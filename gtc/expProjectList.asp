<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
sState = Request("s")
inspector = request("primaryInspector")
clientID=request("clientID")

Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If


On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
If 	inspector = "" then	
	if clientID = 1 then 'this is a special case
		Select Case sState
			Case "All"
				sp = "spGetProjectsAllSpecial"
			
			Case "Active"
				sp = "spGetProjectsActiveSpecial"
			
			Case "Inactive"
				sp = "spGetProjectsInactiveSpecial"
		End Select
	else
		Select Case sState
			Case "All"
				sp = "spGetProjectsAll"
			
			Case "Active"
				sp = "spGetProjectsActive"
			
			Case "Inactive"
				sp = "spGetProjectsInactive"
		End Select
	end if
	
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = sp
	   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
	   .CommandType = adCmdStoredProc   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
				
	Set rs = oCmd.Execute
	Set oCmd = nothing
else
	Select Case sState
		Case "All"
			sp = "spGetProjectsAllInspector"
		
		Case "Active"
			sp = "spGetProjectsActiveInspector"
		
		Case "Inactive"
			sp = "spGetProjectsInactiveInspector"
	End Select
	
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = sp
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, inspector)
	   .CommandType = adCmdStoredProc   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
				
	Set rs = oCmd.Execute
	Set oCmd = nothing
end if


Response.ContentType = "application/vnd.ms-excel"
Response.AddHeader "Content-Disposition", "attachment; filename=projectList" & sState & ".xls" 
if rs.eof <> true then%>
		<table border=1>
			<tr>
				<td><strong>Customer</strong></td>
				<td><strong>Project Name</strong></td>
				<td><strong>Primary Inspector</strong></td>
				<%if sState = "Active" then%>
				<td><b>Last Inspector</b></td>
				<%end if%>
				<td><strong>Bill Rate</strong></td>
				<td><strong>Daily Rate</strong></td>
				<td><strong>Weekly/Post Rain Rate</strong></td>
			</tr>
	<%iBillRate = 0
	idRate = 0
	iwprRate = 0
	do until rs.eof%>
		<tr>
			<td><%=rs("customerName")%></td>
			<td><%=rs("projectName")%></td>
			<td><%=rs("firstName") & " " & rs("lastName")%></td>
			<%if sState = "Active" then
				'get the person who did the last inspection for this project
				Set oCmd = Server.CreateObject("ADODB.Command")

				With oCmd
				   .ActiveConnection = DataConn
				   .CommandText = "spGetLastInspector"
				   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, rs("projectID"))
				   .CommandType = adCmdStoredProc   
				End With

				Set rsLast = oCmd.Execute
				Set oCmd = nothing
			%>
				<td><%=rsLast("firstName") & " " & rsLast("lastName")%></td>
			<%end if%>
			<td>
				<%if isnull(rs("billRate")) then
					iRate = 0
				else
					iRate = rs("billRate")
				end if
				
				if rs("rateType") = 3 then 'lot rate
					'get the number of open lots based on the project
					
					Set oCmd = Server.CreateObject("ADODB.Command")

					With oCmd
					   .ActiveConnection = DataConn
					   .CommandText = "spGetOpenLots"
					   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, trim(rs("projectID")))
					   .CommandType = adCmdStoredProc									   
					End With
					
					
						
					
					'response.Write rs("projectID")	
					Set rsLots = oCmd.Execute
					Set oCmd = nothing
					
					
					if rsLots.eof then
						iNumLots = 0
					else
						dim i
						i = 0
						do until rsLots.eof
							i = i + 1
						rsLots.movenext
						loop
						iNumLots = i
					end if
					
					
					'response.Write iNumLots
					
					icalcRate = iRate * iNumLots
					
				else
					icalcRate = iRate
				end if
				%>
				<%=formatcurrency(icalcRate,2)%>
			</td>
			<td><%=formatcurrency(rs("dRate"),2)%></td>
			<td><%=formatcurrency(rs("wprRate"),2)%></td>
		</tr>
	<%

		
		If IsNull(icalcRate) = False Then'IsNull(rs("billRate")) = False Then
			billRate = billRate + icalcRate
		end if
		If IsNull(rs("dRate")) = False Then
			idRate = idRate + rs("dRate")
		end if
		If IsNull(rs("wprRate")) = False Then
			iwprRate = iwprRate + rs("wprRate")
		end if
							

	
	
	rs.movenext
	loop%>
		<tr><td colspan="6"></td>
		<tr>
			<td colspan="3"><strong>Totals</strong></td>
			<td><strong><%=formatcurrency(billRate,2)%></strong></td>
			<td><strong><%=formatcurrency(idRate,2)%></strong></td>
			<td><strong><%=formatcurrency(iwprRate,2)%></strong></td>
		</tr>
	</table>
<%end if

rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing%>

