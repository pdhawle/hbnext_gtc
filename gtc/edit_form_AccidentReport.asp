<%
clientID = request("clientID")
accidentID = request("accidentID")
'projectID = request("projectID")
'accidentTypeID = request("accidentTypeID")
userID = request("userID")
sState = request("state")

if sState = "" then
	sState = "GA"
end if

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

Set oCmd = Server.CreateObject("ADODB.Command")
		
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetAccidentReport"
   .parameters.Append .CreateParameter("@accidentID", adInteger, adParamInput, 8, accidentID)
   .CommandType = adCmdStoredProc
	   
End With
		
Set rsAccident = oCmd.Execute
Set oCmd = nothing

'get the report based on the type
select case rsAccident("accidentTypeID")
	Case 1 'Vehicular Accident Report
		sp = "spGetAccidentVehicleReport"
		sAccidentType = "Vehicular Accident Report"
	Case 2 'Equipment Damage Report
		sp = "spGetAccidentOtherReport"
		sAccidentType = "Equipment Damage Report"	
	Case 3 'Utility Damage Report
		sp = "spGetAccidentUtilityReport"
		sAccidentType = "Utility Damage Report"
end select

Set oCmd = Server.CreateObject("ADODB.Command")
		
With oCmd
   .ActiveConnection = DataConn
   .CommandText = sp
   .parameters.Append .CreateParameter("@accidentID", adInteger, adParamInput, 8, accidentID)
   .CommandType = adCmdStoredProc
	   
End With
		
Set rs = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
		
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetClientUsers"
   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
   .CommandType = adCmdStoredProc
	   
End With
		
Set rsUsers = oCmd.Execute
Set oCmd = nothing

'get the assigned projects for that user
Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetProjectsActive"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, clientID)
   .CommandType = adCmdStoredProc   
End With
			
Set rsProject = oCmd.Execute
Set oCmd = nothing	

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetStates"
   .CommandType = adCmdStoredProc
   
End With
	
Set rsState = oCmd.Execute
Set oCmd = nothing

If sState <> "" then

	Set oCmd = Server.CreateObject("ADODB.Command")
	
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetCountiesByState"
	   .parameters.Append .CreateParameter("@ID", adVarchar, adParamInput, 50, trim(sState))
	   .CommandType = adCmdStoredProc   
	End With
		
	Set rsCounty = oCmd.Execute
	Set oCmd = nothing

end if

'get a list of the superintendents
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetSupers"
   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
   .CommandType = adCmdStoredProc   
End With
	
Set rsSuper = oCmd.Execute
Set oCmd = nothing
%>
<script language="JavaScript">
<!-- Begin
//function dept_onchange(addCustomer) {
//   document.addCustomer.action = "form.asp?formType=assignCustomer";
//   addCustomer.submit(); 
//}


function type_onchange(addAccidentReport,accidentTypeID,clientID) {
   window.location.href = 'form.asp?formType=addAccidentReport&accidentTypeID='+accidentTypeID+'&clientID='+clientID;
}

function state_onchange(addAccidentReport) {
   document.addAccidentReport.action = "form.asp?formType=addAccidentReport";
   addAccidentReport.submit(); 
}

//function proj_onchange(addAccidentReport,accidentTypeID,clientID,projectID) {
//  document.addAccidentReport.action = "form.asp?formType=addAccidentReport";
//   addAccidentReport.submit();
//}
//  End -->
</script>
<form name="addAccidentReport" method="post" action="process.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Add Accident Report</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table border="0" cellpadding="0" cellspacing="0">
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Accident Type:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td><!-- -->
												<!--<select name="accidentTypeID" onChange="return type_onchange(addAccidentReport,this.getElementsByTagName('option')[this.selectedIndex].value,<%=clientID%>)">
													<option value="">--Select Accident Type---</option>
													<%'do until rsTypes.eof
														'if trim(accidentTypeID) = trim(rsTypes("accidentTypeID")) then%>
															<option selected="selected" value="<%'=rsTypes("accidentTypeID")%>"><%'=rsTypes("accidentType")%></option>
														<%'else%>
															<option value="<%'=rsTypes("accidentTypeID")%>"><%'=rsTypes("accidentType")%></option>
													<%'end if
													'rsTypes.movenext
													'loop%>
												</select>-->
												<%=sAccidentType%>
												<input type="hidden" name="clientID" value="<%=clientID%>" />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										
										
										<%if rsAccident("accidentTypeID") <> "" then
											if rsAccident("accidentTypeID") = 1 then
												sLabel = "Location of Accident"
												
											else
												sLabel = "Project Involved"
											end if%>
											<tr>
												<td valign="top" align="right"><strong><%=sLabel%>:</strong></td>
												<td><img src="images/pix.gif" width="5" height="1"></td>
												<td>
													<input type="text" name="project" size="30" value="<%=rs("project")%>"  />
												</td>
											</tr>
											
											<%'if projectID <> "" then%>
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
												<tr>
													<td colspan="2"></td>
													<td>
														<!--add the approprite form here-->
														<%select case rsAccident("accidentTypeID")%>
															<%case 1 'vehicular company%>
																<!--#include file="inc_edit_form_AccidentVehicular.asp"-->
															<%case 2 'vehicular other%>
																<!--#include file="inc_edit_form_AccidentOther.asp"-->
															<%case 3 'utility%>
																<!--#include file="inc_edit_form_AccidentUtility.asp"-->
																
														<%end select%>
													
													</td>
												</tr>
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
												<tr>
													<td colspan="2"></td>
													<td>
														General Superintendent<br />
														<select name="generalSuper">
															<%do until rsSuper.eof
																if rsAccident("generalSuper") = rsSuper("userID") then%>
																	<option selected="selected" value="<%=rsSuper("userID")%>"><%=rsSuper("firstName")%>&nbsp;<%=rsSuper("lastName")%></option>
																<%else%>
																	<option value="<%=rsSuper("userID")%>"><%=rsSuper("firstName")%>&nbsp;<%=rsSuper("lastName")%></option>
															<%end if
															rsSuper.movenext
															loop%>
														</select>
													</td>
												</tr>
											
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
												<tr>
													<td colspan="3" align="right">
													<%select case rsAccident("accidentTypeID")%>
														<%case 1 'vehicular company%>
															<input type="hidden" name="processType" value="editAccidentVehicular" />
														<%case 2 'vehicular other%>
															<input type="hidden" name="processType" value="editAccidentOther" />
														<%case 3 'utility%>
															<input type="hidden" name="processType" value="editAccidentUtility" />
													<%end select%>
													<input type="hidden" name="accidentID" value="<%=accidentID%>" />
													<input type="submit" value="Save" class="formButton" />
													
													</td>
												</tr>
											<%'end if%>
										<%end if%>
									</table>
								
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<script language="JavaScript" type="text/javascript">

  var frmvalidator  = new Validator("addAccidentReport");
  <%if rsAccident("accidentTypeID") <> "" then%>
  	frmvalidator.addValidation("project","req","Please enter project name");
  <%end if%>
  
  <%if rsAccident("accidentTypeID")=1 then%>
  	frmvalidator.addValidation("driversName","req","Please enter the driver's name");
	frmvalidator.addValidation("unitNumber","req","Please enter the unit number");
	frmvalidator.addValidation("foreman","req","Please enter the foreman's name");
	frmvalidator.addValidation("doingPriorToAccident","req","Describe in detail what you were doing immediately prior to the accident that required your use of the company vehicle?");
	frmvalidator.addValidation("accidentDate","req","Please enter the accident date");
	frmvalidator.addValidation("timeofAccident","req","Please enter the time of the accident");
	frmvalidator.addValidation("describeWhatHappened","req","Please describe what happened");
	frmvalidator.addValidation("reportedBy","req","Please enter who reported accident");
	
  <%end if%>
  
  <%if rsAccident("accidentTypeID")=2 then%>
  	frmvalidator.addValidation("accidentDate","req","Please enter the accident date");
	frmvalidator.addValidation("timeofAccident","req","Please enter the time of the accident");
	frmvalidator.addValidation("foreman","req","Please enter the foreman's name");
	frmvalidator.addValidation("operator","req","Please enter the operator's name");
	frmvalidator.addValidation("unitInvolved","req","Please enter the unit involved");	
  	frmvalidator.addValidation("accidentDescription","req","Please enter the description of the accident");
	frmvalidator.addValidation("accidentCause","req","Please enter the cause of the accident");
	frmvalidator.addValidation("injured","selone_radio","Injured?");
	frmvalidator.addValidation("witnesses","selone_radio","Witnesses?");	
	frmvalidator.addValidation("reportedBy","req","Please enter who reported accident");
  <%end if%>
  
  <%if rsAccident("accidentTypeID")=3 then%>
  	frmvalidator.addValidation("location","req","Please enter the location");
	frmvalidator.addValidation("projectNumber","req","Please enter the job number");
	frmvalidator.addValidation("propertyOwnerName","req","Please enter the utility owner's name");
	frmvalidator.addValidation("incidentDate","req","Please enter the date of the incident");
	frmvalidator.addValidation("timeofIncident","req","Please enter the time of the incident");
	frmvalidator.addValidation("foreman","req","Please enter the foreman");
	frmvalidator.addValidation("operator","req","Please enter the operator");	
	frmvalidator.addValidation("incidentDescription","req","Please describe the incident");
	frmvalidator.addValidation("damagedPropertyDescription","req","Please describe the damaged property");
	frmvalidator.addValidation("sufficientDepthHeight","selone_radio","Were Damaged Utilities at Sufficient Depth or Height?");
	frmvalidator.addValidation("utilitiesShown","selone_radio","Were Utilities shown on Job Specifications?");
	frmvalidator.addValidation("utilityInformed","selone_radio","Had Utility Been Informed to Relocate Equipment?");
	frmvalidator.addValidation("utilityMarked","selone_radio","Were the utilities marked?");
	frmvalidator.addValidation("locateTicketNumber","req","Please enter the locate ticket number");	
	frmvalidator.addValidation("reportedBy","req","Please enter who reported accident");
	
  
  <%end if%>
 // frmvalidator.addValidation("city","req","Please enter the customer's city");
//  frmvalidator.addValidation("zip","req","Please enter the customer's zip");
  //frmvalidator.addValidation("contactName","req","Please enter the contact name");
//  frmvalidator.addValidation("contactPhone","req","Please enter the contact phone");
//  frmvalidator.addValidation("contactFax","req","Please enter the contact fax");
 // frmvalidator.addValidation("contactEmail","req","Please enter the cantact's email address");
 // frmvalidator.addValidation("contactEmail","email");
</script>

<%
rsState.close
set rsState = nothing
DataConn.close
set DataConn = nothing
%>