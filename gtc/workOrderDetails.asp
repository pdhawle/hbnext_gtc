<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%'need customerID, reportID

iWorkOrderID = request("workOrderID")
'reportID = request("reportID")
'customerID = 1
'reportID = 54

Session("LoggedIn") = "True"

Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 

DataConn.Open Session("Connection"), Session("UserID")

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetWorkOrder"
    .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, iWorkOrderID) 'reportID
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsWorkOrder = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetWorkOrderItems"
   .parameters.Append .CreateParameter("@workOrderID", adInteger, adParamInput, 8, iWorkOrderID) 'reportID
   .CommandType = adCmdStoredProc
   
End With

	If Err Then
%>
	<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsItem = oCmd.Execute
Set oCmd = nothing
%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
</head>
<body>
<table width="1000" border="0" cellpadding="0" cellspacing="0">
	<tr bgcolor="#FFFFFF">
		<td colspan="3" bgcolor="#FA702B">
			<img src="images/pix.gif" width="5" height="10">
		</td>
	</tr>
	<tr bgcolor="#ffffff">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width=100% border="0">
										<tr>
											<td valign="top">
												<b>Work Order Date:</b> <%=formatDatetime(rsWorkOrder("dateAdded"),2)%><br>
												<b>Created By:</b> <%=rsWorkOrder("firstName") & " " & rsWorkOrder("lastName")%><br>	
												<b>Approved By:</b> Not Yet Approved<br>	
													
											</td>
											<td><img src=images/pix.gif width=10 height=1></td>
											<td valign="top">
												<b>Customer:</b> <%=rsWorkOrder("customerName")%><br>	
												<b>Division:</b> <%=rsWorkOrder("division")%><br>	
												<b>Project:</b> <%=rsWorkOrder("projectName")%><br>
											</td>
										</tr>
										<tr><td colspan="3"><img src=images/pix.gif width=1 height=20></td></tr>
										<tr>
											<td colspan="3"><strong>General Comments:</strong>&nbsp;<%=rsWorkOrder("generalComments")%></td>
										</tr>
										<tr><td colspan="3"><img src=images/pix.gif width=1 height=20></td></tr>
										<tr>
											<td colspan=3><hr /></td>
										</tr>
										<!--add the items for the actual work order-->
										<tr>
											<td colspan="3">
												<table width="100%" border="0" cellpadding="0" cellspacing="0">
											
													<tr>
														<td><strong>Materials Used</strong></td>
														<td align="center"><strong>Est. Qty</strong></td>
														<td align="center"><strong>Cost / Unit</strong></td>
														<td align="center"><strong>Est. Total</strong></td>
														<td align="center"><strong>Act. Qty</strong></td>
														<td align="center"><strong>Act. Total</strong></td>
														<td align="center"><strong>Warranty/No Charge</strong></td>
													</tr>
													<%blnChange = true
													estTotalAll = 0
													do until rsItem.eof
														If blnChange = true then%>
															<tr class="rowColor">
														<%else%>
															<tr>
														<%end if%>
															<td><%=rsItem("material")%></td>
															<td align="center"><%=formatnumber(rsItem("quantity"),2)%></td>
															<td align="center"><%=formatcurrency(rsItem("cost"),2)%> / <%=rsItem("unit")%></td>
															<td align="center">
																<%estTotal = (rsItem("cost") * rsItem("quantity"))
																response.Write formatcurrency(estTotal,2)
																%>
															</td>
															<td align="center"></td>
															<td width="80" align="center"></td>
															<td align="center">
																<%if rsItem("warrantyNoCharge") = "True" then%>
																	<img src="images/check.gif">
																<%end if%>
															</td>
														</tr>
														
														<%If blnChange = true then%>
															<tr class="rowColor">
														<%else%>
															<tr>
														<%end if%>
															<td colspan="10"><strong>Action Needed:</strong>&nbsp;<%=rsItem("actionNeeded")%></td>
														</tr>
														
														<%If blnChange = true then%>
															<tr class="rowColor">
														<%else%>
															<tr>
														<%end if%>
															<td colspan="10"><strong>Location:</strong>&nbsp;<%=rsItem("location")%></td>
														</tr>
														
														<%If blnChange = true then%>
															<tr class="rowColor">
														<%else%>
															<tr>
														<%end if%>
															<td colspan="10"><strong>Comments:</strong>&nbsp;<%=rsItem("comments")%><br><br></td>
														</tr>
														
													<%
													estTotalAll = (estTotalAll + estTotal)

													'response.Write estTotalAll & "<br>"
													rsItem.movenext
													if blnChange = true then
														blnChange = false
													else
														blnChange = true
													end if
													loop%>
													
													<tr>
														<td colspan="3" align="right"><strong>Est. Totals:</strong></td>
														<td align="center">
															<%=formatcurrency(estTotalAll,2)%>
														</td>
														<td colspan="5"></td>
													</tr>
												</table>
											</td>
										</tr>
										
										<!--end of work order items-->
										<tr><td colspan="3"><img src=images/pix.gif width=1 height=20></td></tr>
										<tr>
											<td colspan="3"><strong>Completed By: __________________________</strong></td>
										</tr>
										<tr>
											<td colspan="3"><strong>Completion Date: __________________________</strong></td>
										</tr>
									</table>	
															
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<tr>
		<td colspan="3" bgcolor="#3C2315">
			<img src="images/pix.gif" width="5" height="10">
		</td>
	</tr>
</table>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>