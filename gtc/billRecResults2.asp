<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
clientID = request("clientID")
smonth = request("month")
syear = request("year")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

if clientID = 1 then 'get NEXT and McBride Accounts. this is a special circumstance
	'get a list of the customers based on the clientID
	Set oCmd = Server.CreateObject("ADODB.Command")
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetCustomersByClientSpecialActive"
	   '.parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
	   .CommandType = adCmdStoredProc   
	End With
else
	'get a list of the customers based on the clientID
	Set oCmd = Server.CreateObject("ADODB.Command")
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetCustomersByClientActive"
	   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
	   .CommandType = adCmdStoredProc   
	End With
end if
Set rsReport = oCmd.Execute
Set oCmd = nothing


%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script type="text/javascript">
    var GB_ROOT_DIR = "<%=sPDFPath%>/greybox/";
</script>
<script type="text/javascript" src="greybox/AJS.js"></script>
<script type="text/javascript" src="greybox/AJS_fx.js"></script>
<script type="text/javascript" src="greybox/gb_scripts.js"></script>
<link href="greybox/gb_styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Billing Reconciliation Report</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<a href="expBillRecResults2.asp?clientID=<%=clientID%>&month=<%=smonth%>&year=<%=syear%>" class="footerLink">Export Data to Excel</a>&nbsp;&nbsp;
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="adminReports.asp" class="footerLink">back to admin reports</a>			
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td></td>
											<td colspan="6">
												<!--Inspector(s): <%'=inspector%><br>
												Project(s): <%'=iProjectID%><br>-->
												<strong><%'=getMonth(smonth) & " " & sYear%></strong><br>
											</td>
										</tr>										
										<tr><td colspan="7"><img src="images/pix.gif" width="1" height="20"></td></tr>
										<tr>
											<td colspan="7">
												<table width="100%" cellpadding="0" cellspacing="0">
													<tr><td colspan="8"><strong><%=getMonth(smonth)%>&nbsp;<%=syear%></strong></td></tr>
													<tr bgcolor="#666666">
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td><span class="searchText">Customer</span></td>
														<td><span class="searchText">Project</span></td>
														<td><span class="searchText">Bill Rate</span></td>
														<td><span class="searchText">Total Reports</span></td>
														<td><span class="searchText">Start Date</span></td>
														<td><span class="searchText">End Date</span></td>
														<td><span class="searchText">Last Inspection</span></td>
													</tr>
													<%if rsReport.eof then%>
														<tr><td></td><td colspan="8">There are no result for the items you chose. Please select again.</td></tr>
													<%else
													iBillTotal = 0
													do until rsReport.eof%>
															<tr class="rowColor">
																<td></td>
																<td colspan="8"><b><%=rsReport("customerName")%></b></td>
															</tr>
															<tr class="rowColor">
																<td></td>
																<td colspan="8">
																	<%if rsReport("advanceBilling") = "True" then%>
																		<em>***Billing in Advance***</em><br>
																	<%else%>
																		<font color="#EE0000"><em>***Billing in Arrears***</em></font><br>
																	<%end if%>
																</td>
															</tr>
															<%
															'get the projects associated with this customer
															Set oCmd = Server.CreateObject("ADODB.Command")
															With oCmd
															   .ActiveConnection = DataConn
															   .CommandText = "spGetProjectandDivisionByCustomerActive"
															   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, rsReport("customerID"))
															   .CommandType = adCmdStoredProc   
															End With
															
															Set rsProject = oCmd.Execute
															Set oCmd = nothing
															
															
															if rsProject.eof then%>
																<tr class="rowColor"><td colspan="2"></td><td colspan="8">There are no active projects listed for this customer</td></tr>
															<%else
																do until rsProject.eof
																
																'get the total number of reports for this project															
																Set oCmd = Server.CreateObject("ADODB.Command")
																	
																With oCmd
																   .ActiveConnection = DataConn
																   .CommandText = "spGetNumReportsByProjectandDate"
																   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, rsProject("projectID"))
																   .parameters.Append .CreateParameter("@month", adInteger, adParamInput, 8, smonth)
																   .parameters.Append .CreateParameter("@year", adInteger, adParamInput, 8, syear)
																   .CommandType = adCmdStoredProc   
																End With																		
																Set rsTot = oCmd.Execute
																Set oCmd = nothing
																%>
																	<tr class="rowColor">
																		<td colspan="2"></td>
																		<td><%'=rsProject("projectID")%><%=rsProject("projectName")%></td>
																		<td>
																			<%
																			
																			if rsProject("endDate") <> "" then
																				if rsProject("endDate") < now() then
																					'zero out the bill rate
																					iBillRate = 0
																				else
																					IBillRate = rsProject("billRate")
																				end if
																			else
																				if rsProject("startDate") > now() then
																					'zero out the bill rate
																					iBillRate = 0
																				else
																					iBillRate = rsProject("billRate")
																				end if
																				'iBillRate = rsProject("billRate")
																			end if
																			response.Write formatCurrency(iBillRate,2)										
																			
																			if isnull(iBillRate) = false then
																				if iBillRate <> "" then
																					'if the project is inactive, don't add to the total
																					If rsProject("isActive") = "True" then
																						iBillTotal = iBillTotal + iBillRate
																					end if
																				end if
																			end if
																			%>
																		</td>
																		<td><%=rsTot("totalNumReports")%></td>
																		<td>
																			<%=rsProject("startDate")%>
																		</td>
																		<td><%=rsProject("endDate")%></td>
																		<td><%=rsProject("lastInspectionDate")%></td>
																	</tr>
																<%
																rsProject.movenext
																loop
															end if%>
															
															<tr><td height="20"></td><td colspan="8"></td></tr>
															
															<%
													
													rsReport.movenext
																
													loop
													rsReport.close
													set rsReport = nothing
													end if%>
													
													<tr>
														<td colspan="2"></td>
														<td align="right"><strong>Total:</strong>&nbsp;</td>
														<td colspan="5"><%=formatcurrency(iBillTotal,2)%></td>
													</tr>
												</table>
											</td>
										</tr>
										
									</table>
															
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>
<%
'rsCustomer.close
rsReport.close
DataConn.close
Set rsCustomer = nothing
Set rsReport = nothing
Set DataConn = nothing
Set oCmd = nothing
%>