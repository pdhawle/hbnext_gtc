<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
sState = Request("s")
inspector = request("primaryInspector")
clientID = session("clientID")



ssort = request("sort")
if ssort = "" then
	columnName = "projectName"
else
	columnName = ssort
end if



Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If


On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

If 	inspector = "" then
	if clientID = 1 then 'this is a special case
		Select Case sState
			Case "All"
				sp = "spGetProjectsAllSpecialSort"
			
			Case "Active"
				sp = "spGetProjectsActiveSpecialSort"
			
			Case "Inactive"
				sp = "spGetProjectsInactiveSpecialSort"
				
			
		End Select
	else
		Select Case sState
			Case "All"
				sp = "spGetProjectsAllSort"
			
			Case "Active"
				sp = "spGetProjectsActiveSort"
			
			Case "Inactive"
				sp = "spGetProjectsInactiveSort"
				
			'Case "ActiveNI"
			
		End Select
	end if
	
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = sp
	   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
	   .parameters.Append .CreateParameter("@columnName", adVarchar, adParamInput, 50, columnName)
	   .CommandType = adCmdStoredProc   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
				
	Set rs = oCmd.Execute
	Set oCmd = nothing
else
	Select Case sState
		Case "All"
			sp = "spGetProjectsAllInspectorSort"
		
		Case "Active"
			sp = "spGetProjectsActiveInspectorSort"
			
		
		Case "Inactive"
			sp = "spGetProjectsInactiveInspectorSort"
	End Select
	
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = sp
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, inspector)
	   .parameters.Append .CreateParameter("@columnName", adVarchar, adParamInput, 50, columnName)
	   .CommandType = adCmdStoredProc   
	End With
				
	Set rs = oCmd.Execute
	Set oCmd = nothing
end if


'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetNextInspectors"
   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
   .CommandType = adCmdStoredProc   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsInspectors = oCmd.Execute
Set oCmd = nothing
%>

<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script language="JavaScript">
<!--
function dept_onchange(projectList) {
   projectList.submit(); 
}
//-->
</script>
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>	
								<%
								Select Case sState
									Case "All"
										sReport = "All Projects"
									
									Case "Active"
										sReport = "Active Projects"
									
									Case "Inactive"
										sReport = "Inactive Projects"
								End Select
								%>							
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - <%=sReport%></span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<a href="listProjects.asp?s=All&primaryInspector=<%=inspector%>" class="footerLink">All Projects</a>&nbsp;&nbsp;
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="listProjects.asp?s=Active&primaryInspector=<%=inspector%>" class="footerLink">Active Projects</a>&nbsp;&nbsp;
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="listProjects.asp?s=Inactive&primaryInspector=<%=inspector%>" class="footerLink">Inactive Projects</a>&nbsp;&nbsp;
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="expProjectList.asp?s=<%=sState%>&primaryInspector=<%=inspector%>&clientID=<%=clientID%>" class="footerLink">Export Data to Excel</a>&nbsp;&nbsp;
						
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<%if sState <> "ActiveNI" then%>
										<tr>
											<td></td>
											<td colspan="10">
								
												<form name="projectList" method="post" action="listProjects.asp?s=<%=sState%>">
												Filter By Primary Inspector:<br>
												<select name="primaryInspector" onChange="return dept_onchange(projectList)">
													<option value="">--all inspectors--</option>
													<%do until rsInspectors.eof
														if trim(rsInspectors("userID")) = trim(inspector) then%>
															<option selected="selected" value="<%=rsInspectors("userID")%>"><%=rsInspectors("firstName") & " " & rsInspectors("lastName")%></option>
														<%else%>
															<option value="<%=rsInspectors("userID")%>"><%=rsInspectors("firstName") & " " & rsInspectors("lastName")%></option>
														<%end if%>
													<%rsInspectors.movenext
													loop%>
												</select>
												</form>
											</td>
										</tr>
										<%end if%>
										<tr><td colspan="10"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr bgcolor="#666666">
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td><span class="searchText">Customer</span></td>
											<td><span class="searchText"><a href="listProjects.asp?s=<%=sState%>&primaryInspector=<%=inspector%>&clientID=<%=clientID%>&sort=projectName" style="color:#721A32;text-decoration:underline;font-weight:bold;">Project Name</a></span></td>
											<td><span class="searchText"><a href="listProjects.asp?s=<%=sState%>&primaryInspector=<%=inspector%>&clientID=<%=clientID%>&sort=lastName" style="color:#721A32;text-decoration:underline;font-weight:bold;">Primary Inspector</a></span></td>
											<%if sState = "Active" then%>
											<td><span class="searchText">Last Inspector</span></td>
											<%end if%>
											<td align="center"><span class="searchText">Rate Type/Frequency</span></td>
											<td align="center"><span class="searchText">Bill Rate</span></td>					
											<td align="center"><span class="searchText">Daily Rate</span></td>
											<td align="center"><span class="searchText">Weekly/Post Rain Rate</span></td>
										</tr>
										<tr><td colspan="10"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<%If rs.eof then%>
											<tr><td></td><td colspan="10">there are no records to display</td></tr>
										<%else
											iBillRate = 0
											idRate = 0
											iwprRate = 0
										
											blnChange = true
											Do until rs.eof
												If blnChange = true then%>
													<tr class="rowColor">
												<%else%>
													<tr>
												<%end if%>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td><a href="customerList.asp?id=<%=left(rs("customerName"),1)%>"><%=rs("customerName")%></a></td>
													<td><a href="form.asp?id=<%=rs("projectID")%>&formType=editProject&customerID=<%=rs("customerID")%>&divisionID=<%=rs("divisionID")%>"><%=rs("projectName")%></a></td>
													<td><%=rs("firstName") & " " & rs("lastName")%></td>
													<%if sState = "Active" then
														'get the person who did the last inspection for this project
														Set oCmd = Server.CreateObject("ADODB.Command")
	
														With oCmd
														   .ActiveConnection = DataConn
														   .CommandText = "spGetLastInspector"
														   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, rs("projectID"))
														   .CommandType = adCmdStoredProc   
														End With
					
														Set rsLast = oCmd.Execute
														Set oCmd = nothing
													%>
														<td><%=rsLast("firstName") & " " & rsLast("lastName")%></td>
													<%end if%>
													<td align="center"><%=rs("rateTypeName")%></td>
													<td align="center">
														<!--calculate the bill rate if it is based on the lot rate-->
														<%if isnull(rs("billRate")) then
															iRate = 0
														else
															iRate = rs("billRate")
														end if
														
														if rs("rateType") = 3 then 'lot rate
															'get the number of open lots based on the project
															
															Set oCmd = Server.CreateObject("ADODB.Command")
							
															With oCmd
															   .ActiveConnection = DataConn
															   .CommandText = "spGetOpenLots"
															   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, trim(rs("projectID")))
															   .CommandType = adCmdStoredProc									   
															End With
															
															
																
															
															'response.Write rs("projectID")	
															Set rsLots = oCmd.Execute
															Set oCmd = nothing
															
															
															if rsLots.eof then
																iNumLots = 0
															else
																dim i
																i = 0
																do until rsLots.eof
																	i = i + 1
																rsLots.movenext
																loop
																iNumLots = i
															end if
															
															
															'response.Write iNumLots
															
															icalcRate = iRate * iNumLots
															
														else
															icalcRate = iRate
														end if
														%>
														<a href="form.asp?id=<%=rs("projectID")%>&formType=editRates&customerID=<%=rs("customerID")%>&divisionID=<%=rs("divisionID")%>"><%=formatcurrency(icalcRate,2)%></a>
													</td>
													<td align="center"><a href="form.asp?id=<%=rs("projectID")%>&formType=editRates&customerID=<%=rs("customerID")%>&divisionID=<%=rs("divisionID")%>"><%=formatcurrency(rs("dRate"),2)%></a></td>
													<td align="center"><a href="form.asp?id=<%=rs("projectID")%>&formType=editRates&customerID=<%=rs("customerID")%>&divisionID=<%=rs("divisionID")%>"><%=formatcurrency(rs("wprRate"),2)%></a></td>
													<%
													If IsNull(icalcRate) = False Then'IsNull(rs("billRate")) = False Then
														billRate = billRate + icalcRate
													end if
													If IsNull(rs("dRate")) = False Then
														idRate = idRate + rs("dRate")
													end if
													If IsNull(rs("wprRate")) = False Then
														iwprRate = iwprRate + rs("wprRate")
													end if
													%>
												</tr>
											<%rs.movenext
											if blnChange = true then
												blnChange = false
											else
												blnChange = true
											end if
											loop
										end if%>
										<tr><td colspan="6"><img src="images/pix.gif" width="1" height="20"></td>
										<tr>
											<td colspan="5" align="right"><strong>Totals&nbsp;&nbsp;</strong></td>
											<td align="center"><strong><%=formatcurrency(billRate,2)%>&nbsp;&nbsp;&nbsp;</strong></td>
											<td align="center"><strong><%=formatcurrency(idRate,2)%></strong></td>
											<td align="center"><strong><%=formatcurrency(iwprRate,2)%></strong></td>
										</tr>
									</table>
									
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>