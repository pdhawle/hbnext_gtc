<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->
<%
reportID = request("reportID")
responsiveActionID = request("responsiveActionID")
referenceNumber = request("referenceNumber")
divisionID=request("divisionID")

'get the projectID based on the report ID
Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")
Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetProjectByReport"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, reportID)
   .CommandType = adCmdStoredProc   
End With
			
Set rs = oCmd.Execute
Set oCmd = nothing

'If Session("LoggedIn") <> "True" Then
'	Response.Redirect("index.asp")
'End If
%>
<html>
<head>
<title><%=sPageTitle%></title>
<link rel="stylesheet" href="includes/main.css" type="text/css">
</head>
<body>
<table align="center" width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3" class="colorBars">
			<img src="images/pix.gif" width="5" height="10">
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Upload Image</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td>
												<strong>Project Name:</strong> <%=rs("projectName")%><br>
												<strong>Report #:</strong> <%=reportID%><br>
												<%if responsiveActionID <> "" then%>
												<strong>Responsive Action #:</strong> <%=responsiveActionID%><br>
												<strong>Reference #:</strong> <%=referenceNumber%><br><br>
												<%end if%>
												Upload an image to this report. Once uploaded, you will be returned to the responsive action list.<br>
												Supported file types: <strong>gif or jpg</strong><br><br>
												
												<strong>PLEASE NOTE:</strong> If you upload more than one image to this responsive action, make sure the file names are <br>different. Otherwise, it <strong>WILL</strong> overwrite the existing image.
											   <FORM METHOD="POST" ENCTYPE="multipart/form-data" ACTION="uploadImgAI.asp?reportID=<%=reportID%>&responsiveActionID=<%=responsiveActionID%>&projectID=<%=rs("projectID")%>&divisionID=<%=divisionID%>">
												  <INPUT TYPE="FILE" SIZE="30" NAME="FILE1" class="formButton"><BR><br>
											   <INPUT TYPE=SUBMIT VALUE="Upload Image" class="formButton">
											   
											   </FORM>
											</td>
										</tr>
									</table>
															
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<tr>
		<td colspan="3" bgcolor="#3C2315">
			<img src="images/pix.gif" width="5" height="10">
		</td>
	</tr>
</table>
</body>
</html>