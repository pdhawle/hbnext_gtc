<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
iCustomerID = request("id")
iParentID = request("parent")

Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If


On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomer"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iCustomerID)
   .CommandType = adCmdStoredProc
   
End With

	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rs = oCmd.Execute
Set oCmd = nothing

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetChildCustomers"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iCustomerID)
   .CommandType = adCmdStoredProc
   
End With

	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsChild = oCmd.Execute
Set oCmd = nothing


If iParentID <> "" then
	'get the parent of the customer if applicable
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetCustomer"
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iParentID)
	   .CommandType = adCmdStoredProc
	   
	End With
	
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
				
	Set rsParent = oCmd.Execute
	Set oCmd = nothing
end if
%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Hierarchy List</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<%sLetter = Left(rs("customerName"),1)%>
						<a href="customerList.asp?id=<%=sLetter%>" class="footerLink">customer list</a>			
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr><td colspan="10"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td colspan="10">
												The company you are viewing information for is in bold.
											</td>
										</tr>
										<tr><td colspan="10"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr bgcolor="#666666">
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td><span class="searchText">Customer Name</span></td>					
											<td><span class="searchText">Contact Name</span></td>
											<td><span class="searchText">Contact Email</span></td>
										</tr>
										<tr><td colspan="10"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<%if iParentID <> "" then%>
											<tr>
												<td><img src="images/pix.gif" width="5" height="1"></td>
												<td>
													<%=rsParent("customerName")%>
												</td>							
												<td><%=rsParent("contactName")%></td>
												<td><a href="mailto:<%=rsParent("contactEmail")%>"><%=rsParent("contactEmail")%></a></td>
											</tr>
										
										<%end if%>
											<tr>
												<td><img src="images/pix.gif" width="5" height="1"></td>
												<td>
													<%if iParentID <> "" then%>
														<img src="images/downArrow.gif">&nbsp;
													<%end if%>
													<strong><%=rs("customerName")%></strong>
												</td>							
												<td><%=rs("contactName")%></td>
												<td><a href="mailto:<%=rs("contactEmail")%>"><%=rs("contactEmail")%></a></td>
											</tr>
											<%
											
											'get the children for this customer
											Do until rsChild.eof%>
												<tr>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td><img src="images/downArrow.gif">&nbsp;<%=rsChild("customerName")%></td>							
													<td><%=rsChild("contactName")%></td>
													<td><a href="mailto:<%=rsChild("contactEmail")%>"><%=rsChild("contactEmail")%></a></td>
												</tr>
											<%rsChild.movenext
											loop%>
									</table>
									
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>