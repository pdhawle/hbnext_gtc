<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

iCustomerID = request("customer")
iDivisionID = request("division")
iProjectID = request("projectID")

dtFrom = request("fromDate")
dtTo = request("toDate")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If


'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetAssignedCustomers"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, Session("ID"))
   .CommandType = adCmdStoredProc   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsCustomer = oCmd.Execute
Set oCmd = nothing



If iCustomerID <> "" then

	If iCustomerID <> "all" then
		'get the division list
		Set oCmd = Server.CreateObject("ADODB.Command")
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetAssignedDivisionsbyCustomer" 'get the divisions based on the customer and user
		   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, Session("ID"))
		   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iCustomerID)
		   .CommandType = adCmdStoredProc   
		End With
			
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
					
		Set rsDivisions = oCmd.Execute
		Set oCmd = nothing
		
	end if
	
end if

If iDivisionID <> "" then
	'get the project list
	Set oCmd = Server.CreateObject("ADODB.Command")
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetAssignedProjectsByDivisionAll" 'get the divisions based on the customer and user
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iDivisionID)
	   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, Session("ID"))
	   .CommandType = adCmdStoredProc   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
				
	Set rsProjects = oCmd.Execute
	Set oCmd = nothing

end if

%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script language="javascript" src="includes/datePicker.js"></script>
<link rel="stylesheet" href="includes/datePicker.css" type="text/css">
<script language="javascript">
<!--
function dept_onchange(reportList) {
   document.reportList.action = "custReportsArchive.asp";
   reportList.submit(); 
}
//-->
</script>
</head>
<body>
<form name="reportList" method="post" action="custReportsArchiveResults.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sCustomerTitle%></span><span class="Header"> - Archive Reports</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<a href="custReports.asp" class="footerLink">back to reports</a>			
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr><td colspan="2"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td>
												Archive Reports<br>
												<select name="customer" onChange="return dept_onchange(reportList)">
													<option value="all">--Select Customer--</option>
													<%do until rsCustomer.eof
														If trim(rsCustomer("customerID")) = trim(iCustomerID) then%>
															<option selected="selected" value="<%=rsCustomer("customerID")%>"><%=rsCustomer("customerName")%></option>
														<%else%>
															<option value="<%=rsCustomer("customerID")%>"><%=rsCustomer("customerName")%></option>
														<%end if
													rsCustomer.movenext
													loop%>
												</select> &nbsp;&nbsp;&nbsp;&nbsp;
												
												<select name="division" onChange="return dept_onchange(reportList)">
													<!--<option value="all">--All Divisions--</option>-->
													<option value="">--All Divisions--</option>										
													<%If iCustomerID <> "" then	
													If iCustomerID <> "all" then									
													do until rsDivisions.eof
														If trim(rsDivisions("divisionID")) = trim(iDivisionID) then%>
															<option selected="selected" value="<%=rsDivisions("divisionID")%>"><%=rsDivisions("division")%></option>
														<%else%>
															<option value="<%=rsDivisions("divisionID")%>"><%=rsDivisions("division")%></option>
														<%end if
													rsDivisions.movenext
													loop
													end if
													end if%>
												</select> 
												<br><br>
												<select name="projectID">
													<%If iDivisionID <> "" then%>	
													<option value="All">--All Projects--</option>
													<%do until rsProjects.eof
														If trim(rsProjects("projectID")) = trim(iProjectID) then%>
															<option selected="selected" value="<%=rsProjects("projectID")%>"><%=rsProjects("projectName")%></option>
														<%else%>
															<option value="<%=rsProjects("projectID")%>"><%=rsProjects("projectName")%></option>
														<%end if															
													rsProjects.movenext
													loop
													end if%>
												</select>&nbsp;&nbsp;&nbsp;&nbsp;
												
												
												<%
												if dtFrom <> "" then
													dtFrom = dtFrom
												else
													dtFrom = formatdatetime(dateadd("d", -7,now()),2)
												end if
												
												if dtTo <> "" then
													dtTo = dtTo
												else
													dtTo = formatdatetime(now(),2)
												end if
												%>
												<strong>From:</strong> <input type="text" name="fromDate" maxlength="10" size="10" value="<%=dtFrom%>"/>&nbsp;<a href="javascript:displayDatePicker('fromDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a> &nbsp;&nbsp;&nbsp;&nbsp;    
												<strong>To:</strong> <input type="text" name="toDate" maxlength="10" size="10" value="<%=dtTo%>"/>&nbsp;<a href="javascript:displayDatePicker('toDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a> &nbsp;&nbsp;&nbsp;&nbsp;
												
												<%If iCustomerID <> "" then%>
													<input type="submit" name="viewReport" value="  View  " class="formButton">
												<%else%>
													<input type="submit" name="viewReport" value="  View  " class="formButton" disabled="disabled">
												<%end if%>
											</td>
										</tr>
									</table>
															
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
</body>
</html>
<%
rsCustomer.close
DataConn.close
Set rsCustomer = nothing
Set DataConn = nothing
Set oCmd = nothing
%>