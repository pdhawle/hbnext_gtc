<%
Set oCmd = Server.CreateObject("ADODB.Command")
		
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCategories"'ByDivision
   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, divisionID)
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsCategories = oCmd.Execute
Set oCmd = nothing

%>

<table cellpadding="0" cellspacing="0" border="0">
	<tr><td></td><td colspan="7" height="10"></td></tr>
	<%'else display the questions
	If rsCategories.eof then%>
		<tr><td></td><td colspan="7">there are no questions to display. please select another customer and project from the dropdown at the top of the page or add questions for this customer via the administrator.</td></tr>
	<%else%>
		<%blnChange = true
		i = 1
		Do until rsCategories.eof
			'response.Write iDivisionID
			%>
			<tr>
				<td><img src="images/pix.gif" width="5" height="0"></td>
				<td>
					<%
	
						Set oCmd = Server.CreateObject("ADODB.Command")

						With oCmd
						   .ActiveConnection = DataConn
						   .CommandText = "spGetWeeklyQuestion"
						   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, rsCategories("categoryID"))
						   .CommandType = adCmdStoredProc
						   
						End With
							
							If Err Then
						%>
							<!--#include file="includes/FatalError.inc"-->
						<%
							End If
									
						Set rsQuestion = oCmd.Execute
						Set oCmd = nothing
						%>
						
						<%if not rsQuestion.eof then%>
						<u><strong><%=rsCategories("category")%></strong></u><br><br>
						<%end if%>
						<table width="800" cellpadding="0" cellspacing="0" border="0">
							<%blnChange = true
							do until rsQuestion.eof
								If blnChange = true then%>
								<tr class="rowColor">
								<%else%>
								<tr>
								<%end if%>
									<td valign="top" width="2">
										<br />&nbsp;<%=i%>. 
									</td>
									<td>
										<br /><%=rsQuestion("question")%><br />
										
										<%iQuesType = rsQuestion("questionType")
										select case iQuesType
										
											Case 1 'No Action Recommended/Add Corrective Action/NA%> 
										  
												<input type="radio" id="answer_&<%=rsQuestion("questionID")%>1" name="answer_&<%=rsQuestion("questionID")%>" value="No Action Recommended" checked="checked" /> No Action Recommended<br />
												<input type="radio" id="answer_&<%=rsQuestion("questionID")%>2" name="answer_&<%=rsQuestion("questionID")%>" value="See Corrective Action Log" onclick="addEvent('<%=i%>','<%=rsQuestion("questionID")%>');"/> Add Remarks<br />
												<input type="radio" id="answer_&<%=rsQuestion("questionID")%>3" name="answer_&<%=rsQuestion("questionID")%>" value="N/A" /> N/A<br /><br />
												
												Comments:<br />
												<textarea cols="30" rows="4" name="comments<%=rsQuestion("questionID")%>"></textarea><br /><br />
												<!--<input type="hidden" value="0" id="theValue<%'=i%>" />
												<a href="javascript:;" onclick="addEvent('<%'=i%>','<%'=rsQuestion("questionID")%>');">Add Comments</a><br /><br />
												<div id="myDiv<%=i%>"> </div><br /><br />-->
												
											<%case 2 'Yes/No/NA %>
												<input type="radio" id="answer_&<%=rsQuestion("questionID")%>1" name="answer_&<%=rsQuestion("questionID")%>" value="Yes" /> Yes<br />
												<input type="radio" id="answer_&<%=rsQuestion("questionID")%>2" name="answer_&<%=rsQuestion("questionID")%>" value="No" checked="checked"/> No<br />
												<input type="radio" id="answer_&<%=rsQuestion("questionID")%>3" name="answer_&<%=rsQuestion("questionID")%>" value="N/A" /> N/A<br /><br />
												
												Comments:<br />
												<textarea cols="30" rows="4" name="comments<%=rsQuestion("questionID")%>"></textarea><br /><br />
												<!--<input type="hidden" value="0" id="theValue<%'=i%>" />
												<a href="javascript:;" onclick="addEvent('<%'=i%>','<%'=rsQuestion("questionID")%>');">Add Comments</a><br /><br />
												<div id="myDiv<%=i%>"> </div><br />-->
												
										<%end select%>
									</td>
								</tr>
						
						<%rsQuestion.movenext
						i=i+1
						if blnChange = true then
							blnChange = false
						else
							blnChange = true
						end if
						loop%>
						
						<tr><td colspan="7" headers="10"></td></tr>
					</table>
					
				</td>
			</tr>
		<%rsCategories.movenext
		loop%>
		</table>
	<%end if%>