<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
</head>
<body>
<form name="openItems" method="post" action="process.asp">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr bgcolor="#FFFFFF">
		<tr>
		<td colspan="3" class="colorBars">
			<img src="images/pix.gif" width="5" height="10">
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page--><br><br>
									<table cellpadding="0" cellspacing="0">
										<tr>
											<td><img src="images/word.gif" border="0">&nbsp;</td>
											<td><a href="upload/NiscayahSafetyTOC(5-19-08).doc" target="_blank">Table of Contents</a></td>
										</tr>
										<tr><td colspan="2" height="5"></td></tr>
										<tr>
											<td><img src="images/word.gif" border="0">&nbsp;</td>
											<td><a href="upload/NiscayahSafetyOrientation(5-19-08).doc" target="_blank">Safety Orientation</a></td>
										</tr>
										<tr><td colspan="2" height="5"></td></tr>
										<tr>
											<td><img src="images/word.gif" border="0">&nbsp;</td>
											<td><a href="upload/NiscayahSafetyPart1(5-19-08).doc" target="_blank">Safety Part 1</a></td>
										</tr>
										<tr><td colspan="2" height="5"></td></tr>
										<tr>
											<td><img src="images/word.gif" border="0">&nbsp;</td>
											<td><a href="upload/NiscayahSafetyPart2(5-19-08).doc" target="_blank">Safety Part 2</a></td>
										</tr>
										<tr><td colspan="2" height="5"></td></tr>
										<tr>
											<td><img src="images/word.gif" border="0">&nbsp;</td>
											<td><a href="upload/NiscayahSafetyPart3(5-19-08).doc" target="_blank">Safety Part 3</a></td>
										</tr>
										<tr><td colspan="2" height="5"></td></tr>
										<tr>
											<td><img src="images/word.gif" border="0">&nbsp;</td>
											<td><a href="upload/NiscayahSafetyPart4(5-19-08).doc" target="_blank">Safety Part 4</a></td>
										</tr>
										<tr><td colspan="2" height="5"></td></tr>
										<tr>
											<td><img src="images/word.gif" border="0">&nbsp;</td>
											<td><a href="upload/NiscayahSafetyPart 5(5-19-08).doc" target="_blank">Safety Part 5</a></td>
										</tr>
										<tr><td colspan="2" height="5"></td></tr>
										<tr>
											<td><img src="images/word.gif" border="0">&nbsp;</td>
											<td><a href="upload/NiscayahSafetyPart6(5-19-08).doc" target="_blank">Safety Part 6</a></td>
										</tr>
									</table>
									
									<br><br>						
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<tr>
		<td colspan="3" bgcolor="#000000">
			<img src="images/pix.gif" width="5" height="10">
		</td>
	</tr>
</table>
</form>
</body>
</html>