<table>
	<tr><td height="20"></td></tr>
	<tr>
		<td align="center">
			<%
			'get the client information
			Set oCmd = Server.CreateObject("ADODB.Command")		
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spGetClient"
			   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
			   .CommandType = adCmdStoredProc
				   
			End With
					
			Set rsClient = oCmd.Execute
			Set oCmd = nothing
		
			%>
			<strong><%=ucase(rsClient("clientName"))%></strong><br />
			ACCIDENT REPORT<br />
			(Other than Licensed Vehicle)
		</td>
	</tr>
	<tr><td height="20"></td></tr>	
	<tr>
		<td>
			
			<table cellpadding="0" cellspacing="0">
				<tr><td colspan="2"><strong>TIME and PLACE	</strong></td>
				<tr>
					<td align="right">Date of Accident:&nbsp;</td>
					<td>
						<input type="text" name="accidentDate" maxlength="10" size="10" value="<%=rs("accidentDate")%>"/>&nbsp;<a href="javascript:displayDatePicker('accidentDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
					</td>
				</tr>
				<tr>
					<td align="right">Time of Accident:&nbsp;</td>
					<td>
						<input id="timeofAccident" name="timeofAccident" type="text"  size=10 maxlength=10 ONBLUR="validateDatePicker(this)" value="<%=rs("timeofAccident")%>">&nbsp;<IMG SRC="images/timepicker.gif" BORDER="0" ALT="Pick a Time!" ONCLICK="selectTime(this,timeofAccident)" STYLE="cursor:hand">
					</td>
				</tr>				
				<tr>
					<td align="right">Foreman:&nbsp;</td>
					<td>
						<input name="foreman" type="text" size=30 maxlength="100" value="<%=rs("foreman")%>"  />
					</td>
				</tr>
				<tr>
					<td align="right">Operator:&nbsp;</td>
					<td>
						<input name="operator" type="text" size=30 maxlength="100" value="<%=rs("operator")%>"  />
					</td>
				</tr>
				<tr>
					<td align="right">Unit Involved:&nbsp;</td>
					<td>
						<input name="unitInvolved" type="text" size=30 maxlength="100" value="<%=rs("unitInvolved")%>"  />
					</td>
				</tr>
				<tr><td colspan="2" height="20"></td>
				<tr><td colspan="2"><strong>COMPLETE DESCRIPTION OF INCIDENT</strong></td>
				<tr>
					<td align="right"></td>
					<td>
						Description of Accident, Loss or Damage<br />
						<textarea name="accidentDescription" rows="5" cols="30"><%=rs("accidentDescription")%></textarea>
					</td>
				</tr>
				<tr><td colspan="2" height="5"></td>
				<tr>
					<td align="right"></td>
					<td>
						Describe Cause of Accident, Loss or Damage<br />
						<textarea name="accidentCause" rows="5" cols="30"><%=rs("accidentCause")%></textarea>
					</td>
				</tr>
				<tr><td colspan="2" height="20"></td>
				<tr><td colspan="2"><strong>INJURED</strong></td>
				<tr>
					<td>&nbsp;</td>
					<td>
						Injured?&nbsp;
						<input type="radio" name="injured" value="Yes" <%=isCheckedRadio(rs("injured"),"Yes")%> />&nbsp;Yes&nbsp;&nbsp;&nbsp;<input type="radio" name="injured" value="No" <%=isCheckedRadio(rs("injured"),"No")%>/>&nbsp;No
					</td>
				</tr>
				<tr>
					<td align="right">Name:&nbsp;</td>
					<td>
						<input name="injuredName1" type="text" size=30 maxlength="50" value="<%=rs("injuredName1")%>"  />
					</td>
				</tr>
				<tr>
					<td align="right">Injury:&nbsp;</td>
					<td>
						<input name="injury1" type="text" size=30  maxlength="100" value="<%=rs("injury1")%>" />
					</td>
				</tr>
				<tr>
					<td align="right">Address:&nbsp;</td>
					<td>
						<input name="injuredAddress1" type="text" size=30 maxlength="100" value="<%=rs("injuredAddress1")%>"  />
					</td>
				</tr>
				<tr>
					<td align="right">Home Phone:&nbsp;</td>
					<td>
						<input name="injuredHomePhone1" type="text" size=10 maxlength="20" value="<%=rs("injuredHomePhone1")%>"  />
					</td>
				</tr>
				<tr>
					<td align="right">Work Phone:&nbsp;</td>
					<td>
						<input name="injuredWorkPhone1" type="text" size=10 maxlength="20" value="<%=rs("injuredWorkPhone1")%>"  />
					</td>
				</tr>
				<tr><td colspan="2" height="10"></td>
				<tr>
					<td align="right">Name:&nbsp;</td>
					<td>
						<input name="injuredName2" type="text" size=30 maxlength="50" value="<%=rs("injuredName2")%>"  />
					</td>
				</tr>
				<tr>
					<td align="right">Injury:&nbsp;</td>
					<td>
						<input name="injury2" type="text" size=30 maxlength="100" value="<%=rs("injury2")%>"  />
					</td>
				</tr>
				<tr>
					<td align="right">Address:&nbsp;</td>
					<td>
						<input name="injuredAddress2" type="text" size=30 maxlength="100" value="<%=rs("injuredAddress2")%>"  />
					</td>
				</tr>
				<tr>
					<td align="right">Home Phone:&nbsp;</td>
					<td>
						<input name="injuredHomePhone2" type="text" size=10 maxlength="20" value="<%=rs("injuredHomePhone2")%>"  />
					</td>
				</tr>
				<tr>
					<td align="right">Work Phone:&nbsp;</td>
					<td>
						<input name="injuredWorkPhone2" type="text" size=10  maxlength="20" value="<%=rs("injuredWorkPhone2")%>" />
					</td>
				</tr>
				<tr><td colspan="2" height="20"></td>
				<tr><td colspan="2"><strong>WITNESSES</strong></td>
				<tr>
					<td>&nbsp;</td>
					<td>
						Witnesses?&nbsp;
						<input type="radio" name="witnesses" value="Yes" <%=isCheckedRadio(rs("witnesses"),"Yes")%> />&nbsp;Yes&nbsp;&nbsp;&nbsp;<input type="radio" name="witnesses" value="No" <%=isCheckedRadio(rs("witnesses"),"No")%>/>&nbsp;No
					</td>
				</tr>
				<tr>
					<td align="right">Name:&nbsp;</td>
					<td>
						<input name="witnessName1" type="text" size=30 maxlength="50" value="<%=rs("witnessName1")%>"  />
					</td>
				</tr>
				<tr>
					<td align="right">Address:&nbsp;</td>
					<td>
						<input name="witnessAddress1" type="text" size=30 maxlength="100" value="<%=rs("witnessAddress1")%>" />
					</td>
				</tr>
				<tr>
					<td align="right">Home Phone:&nbsp;</td>
					<td>
						<input name="witnessHomePhone1" type="text" size=10 maxlength="20" value="<%=rs("witnessHomePhone1")%>"  />
					</td>
				</tr>
				<tr>
					<td align="right">Work Phone:&nbsp;</td>
					<td>
						<input name="witnessWorkPhone1" type="text" size=10 maxlength="20" value="<%=rs("witnessWorkPhone1")%>"  />
					</td>
				</tr>
				<tr><td colspan="2" height="10"></td>
				<tr>
					<td align="right">Name:&nbsp;</td>
					<td>
						<input name="witnessName2" type="text" size=30  maxlength="50" value="<%=rs("witnessName2")%>" />
					</td>
				</tr>
				<tr>
					<td align="right">Address:&nbsp;</td>
					<td>
						<input name="witnessAddress2" type="text" size=30 maxlength="100" value="<%=rs("witnessAddress2")%>"  />
					</td>
				</tr>
				<tr>
					<td align="right">Home Phone:&nbsp;</td>
					<td>
						<input name="witnessHomePhone2" type="text" size=10 maxlength="20" value="<%=rs("witnessHomePhone2")%>"  />
					</td>
				</tr>
				<tr>
					<td align="right">Work Phone:&nbsp;</td>
					<td>
						<input name="witnessWorkPhone2" type="text" size=10 maxlength="20" value="<%=rs("witnessWorkPhone2")%>"  />
					</td>
				</tr>
				<tr><td colspan="2" height="20"></td>
				<tr>
					<td>&nbsp;</td>
					<td>
						Was Incident Reported to Police Department?<br />
						<input type="radio" name="reported" value="Yes" <%=isCheckedRadio(rs("reported"),"Yes")%> />&nbsp;Yes&nbsp;&nbsp;&nbsp;<input type="radio" name="reported" value="No" <%=isCheckedRadio(rs("reported"),"No")%>/>&nbsp;No
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>
						If Yes, which Police Department?<br />
						<input name="policeDepartment" type="text" size=30 maxlength="100" value="<%=rs("policeDepartment")%>"  />
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>
						Date and Name of Police Authority to Whom Reported:<br />
						<input name="dateAndNamePolice" type="text" size=30 maxlength="100" value="<%=rs("dateAndNamePolice")%>" />
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>
						Probable Amount of Entire Loss:<br />
						<input name="lossAmount" type="text" size=5  maxlength="50" value="<%=rs("lossAmount")%>" />
					</td>
				</tr>
				<tr><td colspan="2" height="20"></td>
				<tr><td colspan="2"><strong>PROPERTY DAMAGE</strong></td>
				<tr>
					<td align="right">Owner:&nbsp;</td>
					<td>
						<input name="damageOwner1" type="text" size=30 maxlength="50" value="<%=rs("damageOwner1")%>" />
					</td>
				</tr>
				<tr>
					<td align="right">Address:&nbsp;</td>
					<td>
						<input name="damageAddress1" type="text" size=30 maxlength="100" value="<%=rs("damageAddress1")%>"  />
					</td>
				</tr>
				<tr>
					<td align="right">Home Phone:&nbsp;</td>
					<td>
						<input name="damageHomePhone1" type="text" size=10 maxlength="20" value="<%=rs("damageHomePhone1")%>"  />
					</td>
				</tr>
				<tr>
					<td align="right">Work Phone:&nbsp;</td>
					<td>
						<input name="damageWorkPhone1" type="text" size=10 maxlength="20" value="<%=rs("damageWorkPhone1")%>"  />
					</td>
				</tr>
				<tr><td colspan="2" height="10"></td>
				<tr>
					<td align="right">Name:&nbsp;</td>
					<td>
						<input name="damageOwner2" type="text" size=30 maxlength="50" value="<%=rs("damageOwner2")%>"  />
					</td>
				</tr>
				<tr>
					<td align="right">Address:&nbsp;</td>
					<td>
						<input name="damageAddress2" type="text" size=30  maxlength="100" value="<%=rs("damageAddress2")%>" />
					</td>
				</tr>
				<tr>
					<td align="right">Home Phone:&nbsp;</td>
					<td>
						<input name="damageHomePhone2" type="text" size=10  maxlength="20" value="<%=rs("damageHomePhone2")%>" />
					</td>
				</tr>
				<tr>
					<td align="right">Work Phone:&nbsp;</td>
					<td>
						<input name="damageWorkPhone2" type="text" size=10 maxlength="20" value="<%=rs("damageWorkPhone2")%>"  />
					</td>
				</tr>
				<tr><td colspan="2" height="20"></td>
				<tr>
					<td>&nbsp;</td>
					<td>
						Remarks:<br />
						<textarea name="remarks" rows="5" cols="30"><%=rs("remarks")%></textarea>
					</td>
				</tr>
				<tr><td colspan="2" height="20"></td>
				<tr>
					<td>Reported By:&nbsp;</td>
					<td>
						<input type="text" name="reportedBy" size="30" maxlength="50" value="<%=rs("reportedBy")%>" />
						<!--<select name="userID">

							<option value="<%'=session("ID")%>"><%'=session("name")%></option>
							<%'do until rsUsers.eof
								'if trim(userID) = trim(rsUsers("userID")) then%>
									<option selected="selected" value="<%'=rsUsers("userID")%>"><%'=rsUsers("firstName")%>&nbsp;<%'=rsUsers("lastName")%></option>
								<%'else%>
									<option value="<%'=rsUsers("userID")%>"><%'=rsUsers("firstName")%>&nbsp;<%'=rsUsers("lastName")%></option>
							<%'end if
							'rsUsers.movenext
							'loop%>
						</select>-->
					</td>
				</tr>
				
				
			</table>
	</tr>
</table>
