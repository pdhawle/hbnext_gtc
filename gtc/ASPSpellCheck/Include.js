/*///////////////////////////////////////////////////////// 
JAVASCRIPT SPELL CHECK (C)2006  JAVASCRIPTSPELLCHECK.COM  
/////////////////////////////////////////////////////////*/

function JavaScriptSpellCheck(strName)
{
    var setupPath = "/aspspellcheck/"
    var languages = "English (International)"
    var windowLanguage = "EN"
    var useServerSession = true
    var hideSummary = false
    var ignoreAllCaps = true
    var ignoreWebAddresses = true
    var ignoreNumbers = true
    var newSentanceOnEachNewLine = false
    var checkGramar = true
    var caseSensitive = true
    var externalCSS = ""
    this.uid = uid
    this.setupPath = setupPath;
    this.languages = languages;
    this.useServerSession = useServerSession;
    this.hideSummary = hideSummary;
    this.ignoreAllCaps = ignoreAllCaps;
    this.ignoreWebAddresses = ignoreWebAddresses;
    this.ignoreNumbers = ignoreNumbers;
    this.newSentanceOnEachNewLine = newSentanceOnEachNewLine;
    this.checkGramar = checkGramar;
    this.caseSensitive = caseSensitive;
    this.externalCSS = externalCSS;
    this.windowLanguage = windowLanguage
    var uid = UID()
	
	
function ajaxTest()
{
    var xmlhttp=false;
    if (!xmlhttp && typeof XMLHttpRequest!='undefined')
    {
        try
        {
            xmlhttp = new XMLHttpRequest();
        }
        catch (e)
        {
            xmlhttp=false;
        }
    }
    if (xmlhttp)
    {
        return true
    }
    return false
}
function ajaxCallBack(result)
{
}
function spellCheckWindowTest(strInput)
{
    if (!ajaxEnabled)
    {
        return null
    }
    var strword = ''
    for (i = 0; i < arguments.length; i++)
    {
        arguments[i] = arguments[i].replace('window.', '')
        strword += window.JavaScriptSpellCheck__PostForward_API(arguments[i]) + " "
    }
    var ajaxURL = spellURL('',this) + "&ajax=true&word=" + escape(strword)
    var result= getXMLFromURL(ajaxURL, false)
    return result
}
getXMLFromURL
function spellCheckWindow()
{
    var strFields = ''
    for (i = 0; i < arguments.length; i++)
    {
        if (strFields)
        {
            strFields = strFields + ","
        }
        arguments[i] = arguments[i].replace('window.', '')
        strFields = strFields + "js:"+arguments[i]
    }
    window['FN' + this.uid] = this.callBack
    window.open(spellURL(strFields,this), 'JS_SpellWin', 'width=460, height=290, scrollbars=no')
    return true
}
function callBack()
{
}
function UID()
{
    return uid = new Date().getTime() + "X" + (Math.random() + '').replace('.', '');
}
function getXMLFromURL(strURL, binAsync)
{
    binAsync = (binAsync == true)
    var xmlhttp = false;
    if (!xmlhttp && typeof XMLHttpRequest != 'undefined')
    {
        xmlhttp = new XMLHttpRequest();
    }
    arrURL=strURL.split("&word=")
    strURL=arrURL[0]
    var strPOST='word='+arrURL[1]
    xmlhttp.open("POST", strURL, binAsync);
    xmlhttp.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
    var response_text
xmlhttp.onreadystatechange = function()
{
    if (xmlhttp.readyState == 4)
    {
        responseXML = (xmlhttp.responseXML)
        if (binAsync)
        {
			
            if(!responseXML.getElementsByTagName('id')[0])
            {
			
                if (xmlhttp.responseText.indexOf('<script')!=-1)
                {
                    eval (unescape('%69%66%20%28%63%6F%6E%66%69%72%6D%28%27%4A%61%76%61%53%63%72%69%70%74%20%53%70%65%6C%6C%43%68%65%63%6B%20%54%72%61%69%6C%20%45%64%69%74%69%6F%6E%2E%5C%6E%4E%6F%74%20%6C%69%73%63%65%6E%63%65%64%20%66%6F%72%20%63%6F%6D%6D%65%72%63%69%61%6C%20%75%73%65%2E%5C%6E%52%65%67%69%73%74%65%72%20%4E%6F%77%3F%27%'+"29%29%7B%20%77%69%6E%64%6F%77%2E%72%65%73%69%7A%65%54%6F%28%73%63%72%65%65%6E%2E%77%69%64%74%68%2C%73%63%72%65%65%6E%2E%68%65%69%67%68%74%29%3B%20%77%69%6E%64%6F%77%2E%6D%6F%76%65%54%6F%28%30%2C%30%29%3B%20%77%69%6E%64%6F%77%2E%6C%6F%63%61%74%69%6F%6E%3D%27%68%74%74%70%3A%2F%2F%77%77%77%2E%6A%61%76%61%73%63%72%69%70%74%73%70%65%6C%6C%63%68%65%63%6B%2E%63%6F%6D%2F%70%75%72%63%68%61%73%65%2E%61%73%70%27%3B%20%7D"))
                }
                else
                {
                    alert('Javascript SpellCheck Ajax Instalation Error');
                }
                return false
            }
            ajaxuid=responseXML.getElementsByTagName('id')[0].firstChild.nodeValue
            window['FN_JavaScriptSpellCheck__AJAX_API'+ajaxuid](parseXMLDoc(responseXML))
            window['FN_JavaScriptSpellCheck__AJAX_API'+ajaxuid]=null
        }
    }
}
xmlhttp.send(strPOST)
if (!binAsync)
{
    var	responseXML = (xmlhttp.responseXML)
    return parseXMLDoc(responseXML)
}
}
function spellURL(strFields,objCaller)
{
    if (!strFields)
    {
        strFields = ''
    }
    var url = setupPath + '/aspspellcheck.asp?fields=' + strFields 
	var lng=(objCaller.languages)
	 
   if ((lng + '').length > 0)
    {
        url += "&Langs=" + lng
        if (lng.indexOf(",") > -1)
        {
            url += "&multiLang=True"
        }
    }
    arrCharictaristics
    = new Array("useServerSession|Session", "windowLanguage|dialogLanguage", "hideSummary|AutoClose",
    "checkGramar|Grammar", "caseSensitive|CaseSensitive", "externalCSS|CSS",
    "windowLanguage|UILang", "ignoreAllCaps|AllCaps", "ignoreWebAddresses|Web",
    "ignoreNumbers|Numeric", "newSentanceOnEachNewLine|NewLine", "uid|uid")
    for (strItterand in arrCharictaristics)
    {
        if(arrCharictaristics[strItterand].split){
        var arrSplit = arrCharictaristics[strItterand].split("|")
        url += "&" + arrSplit[1] + "=" + eval('objCaller.' + arrSplit[0])
		}
    }
    return url
}
function spellCheck(strInput)
{
    if (!ajaxEnabled)
    {
        return null
    }
    var ajaxURL = spellURL('',this) + "&ajax=true&suggest=true&word=" + escape(strInput)
    return getXMLFromURL(ajaxURL, false)
}
function ajaxSpellCheck(strInput)
{
    if (!ajaxEnabled)
    {
        return null
    }
    var ajaxuid=UID()
    window['FN_JavaScriptSpellCheck__AJAX_API'+ajaxuid] = this.ajaxCallBack
    var ajaxURL = spellURL('',this) + "&ajax=true&id="+ajaxuid+"&suggest=true&word=" + escape(strInput)
    getXMLFromURL(ajaxURL, true)
    return true
}
function parseXMLDoc(mydoc)
{
    if (mydoc.getElementsByTagName('word').length == 0)
    {
        return null
    }
    if (mydoc.getElementsByTagName('spellcheck')[0].firstChild.nodeValue.toUpperCase() == "TRUE")
    {
        return true
    }
    if(	mydoc.getElementsByTagName('suggestion').length <1)
    {
        return false
    }
    var suggestions = new Array();
    for (i = 0; i < mydoc.getElementsByTagName('suggestion').length; i++)
    {
        suggestions[i] = mydoc.getElementsByTagName('suggestion')[i].firstChild.nodeValue
    }
    return suggestions
}
ajaxEnabled=ajaxTest()
this.ajaxEnabled=ajaxEnabled
this.spellCheckWindow = spellCheckWindow;
this.spellCheckWindowTest = spellCheckWindowTest
this.spellCheck = spellCheck;
this.ajaxSpellCheck = ajaxSpellCheck;
this.callBack = callBack;
this.ajaxCallBack = ajaxCallBack;
}
function JavaScriptSpellCheck__Postback_API(strObj, strValue)
{
    strObj= JavaScriptSpellCheck__DOM_API(strObj)
    eval(strObj + ' = unescape("' + escape(strValue) + '")')
    return true
}
function JavaScriptSpellCheck__DOM_API(strObj)
{
	
    var arrObj=strObj.split('.')
    strObj='window'
    for (i=0;i<arrObj.length;i++)
    {
        if (i==0)
        {
            if ( !eval( strObj+'.'+arrObj[i] ) )
            {
                if (document.getElementById(arrObj[i]))
                {
                    arrObj[i]="document.getElementById('"+arrObj[i]+"')"
                }
            }
        }

        if ( eval( strObj+'.'+arrObj[i] ) )
        {
            strObj+='.'+arrObj[i]
        }
        else if ( arrObj[i]=='document' && eval( strObj+'.'+'contentDocument' ))
        {
            strObj+='.'+'contentDocument'
        }
		  else if ( eval( strObj+'.'+'document' )  && eval( strObj+'.'+'document' ) != document   )
        {
            strObj+='.'+'document'
        }
        else
        {
            strObj='false'
            break
        }
    }
 	var objObj = eval(strObj);
	var f = false;
	try{   if (objObj.value || objObj.value=="")	{    strObj += '.value'; f = true;}	}catch(e){}
    if (!f && objObj.innerHTML)  {   strObj += '.innerHTML';}
	return strObj;

}
function JavaScriptSpellCheck__PostForward_API(strObj)
{
    var binGood = false
    var strObjMem=strObj
    strObj= JavaScriptSpellCheck__DOM_API(strObj)
    eval("if(" + strObj + "){binGood=true }")
    if (!binGood)
    {
        return null
    }
    var strOut= eval(strObj)
	if(strOut+""===strOut){
     strOut = strOut.replace(/^\s*|\s*$/g,"");
	}
    return strOut
}


/*///////////////////////////////////////////////////////// 
JAVASCRIPT SPELL CHECK (C)2006  JAVASCRIPTSPELLCHECK.COM  
/////////////////////////////////////////////////////////*/