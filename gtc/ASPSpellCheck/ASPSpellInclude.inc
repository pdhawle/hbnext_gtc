<%
class AspSpellLink

private  setting_Session, setting_Fields, setting_DictPath, setting_CustomDict, setting_AllCaps, setting_Web  , setting_Numeric  , setting_NewLine  , setting_CSS, setting_Grammar, setting_CaseSensitive ,setting_UILang , setting_Langs, setting_MultiLang, setting_ASPSPELLPATH, setting_AutoClose, setting_strAlert, G_callback


public property let dictionary(strInput)
dim arrLangs, intLangItterand, strLangItterand, intLangCount, strLangs
setting_MultiLang=false
strInput = trim(""&strInput)
if strInput="" then setting_Langs="" : exit property 

arrLangs = split(strInput,",")
intLangCount=0

strLangs="" 
for intLangItterand = 0 to ubound(arrLangs) 
strLangItterand=trim(""&arrLangs(intLangItterand))
if strLangItterand<>"" then intLangCount=intLangCount+1
if strLangs<>"" then strLangs=strLangs&","
strLangs=strLangs&strLangItterand
next
setting_Langs = strLangs
if intLangCount>1 then setting_MultiLang=true
end property

public property get dictionary
activeDictionary=setting_Langs
end property


public property let javaScriptCallback (strInput)
	G_callback=strInput&""
end property

public property get javaScriptCallback
	javaScriptCallback=G_callback
end property


public property let aspSpellPath(strInput)
	setting_ASPSPELLPATH=strInput
end property

public property get aspSpellPath
	aspSpellPath=setting_ASPSPELLPATH
end property

public property let useSession(binFlag)
	setting_Session=(binFlag=true)
end property

public property get useSession
	useSession=setting_Session
end property



public property get doneMessage
	doneMessage=setting_strAlert
end property

public property let doneMessage(strInput)
	setting_strAlert=strInput&""
end property

public property let hideSummary(binFlag)
	setting_AutoClose=(binFlag=true)
end property

public property get hideSummary
	hideSummary=(setting_AutoClose=true)
end property

public property let fields (strInput)
	setting_Fields=(strInput)
end property

public property get fields
	fields=setting_Fields
end property

public property let ignoreAllCaps (binFlag)
	setting_AllCaps=(binFlag=true)
end property

public property get ignoreAllCaps
	ignoreAllCaps=setting_AllCaps
end property

public property let ignoreWebAddresses (binFlag)
	setting_Web=(binFlag=true)
end property

public property get ignoreWebAddresses
	ignoreWebAddresses=setting_Web
end property

public property let ignoreNumbers (binFlag)
	setting_Numeric=(binFlag=true)
end property

public property get ignoreNumbers
	ignoreNumbers=setting_Numeric
end property

public property let newSentanceOnEachNewLine (binFlag) 'LEGACY
	setting_NewLine=(binFlag=true)
end property

public property get newSentanceOnEachNewLine 'LEGACY
	newSentanceOnEachNewLine=setting_NewLine
end property


public property let newSentenceOnEachNewLine (binFlag)
	setting_NewLine=(binFlag=true)
end property

public property get newSentenceOnEachNewLine
	newSentenceOnEachNewLine=setting_NewLine
end property

public property let checkGrammar (binFlag)
	setting_Grammar=(binFlag=true)
end property

public property get checkGrammar 
	checkGrammar=setting_Grammar
end property

public property let checkGramar (binFlag)  'LEGACY
	setting_Grammar=(binFlag=true)
end property

public property get checkGramar  'LEGACY
	checkGramar=setting_Grammar
end property


public property let caseSensitive (binFlag)
	setting_CaseSensitive=(binFlag=true)
end property

public property get caseSensitive
	caseSensitive=setting_CaseSensitive
end property

public property let externalCSS (strInput)
	setting_CSS=strInput
end property

public property get externalCSS
	externalCSS=setting_CSS
end property

public property let dialogLanguage (strInput)
	setting_UILang=strInput
end property

public property get dialogLanguage
	dialogLanguage=setting_UILang
end property



public function url()
	
	dim arr_settings, strItterand, strSettingName, strSettingValue
	arr_settings = array("setting_Session","setting_DictPath","setting_CustomDict","setting_AllCaps","setting_Web","setting_Numeric","setting_NewLine","setting_CSS","setting_Grammar","setting_CaseSensitive","setting_UILang","setting_Langs","setting_MultiLang","setting_AutoClose","setting_strAlert")
	
		
		if right(setting_ASPSPELLPATH,1)<>"/" then setting_ASPSPELLPATH=setting_ASPSPELLPATH&"/"
		if left(setting_ASPSPELLPATH,1)<>"/" then setting_ASPSPELLPATH="/"&setting_ASPSPELLPATH
	
	url=setting_ASPSPELLPATH&"ASPSpellCheck.asp?fields="&setting_Fields
	 
	  
	
	
	for each strItterand in arr_settings
		strSettingName=replace(strItterand,"setting_","")
		strSettingValue=eval(strItterand)
		if ""&strSettingValue<>"" then url=url&"&"&strSettingName&"="&strSettingValue
	next

		
	 url=replace(url,"&","&amp;")
	 
	if len(Trim(G_callback&""))>0 then url=url&"&uid=dcallback__"&server.urlencode(G_callback)

end function 
public function spellingCheck(strWord)
spellingCheck=spellCheck_Mech(strWord,false)
end function 
public function spellingSuggestions(strWord)
spellingSuggestions=spellCheck_Mech(strWord,true)

end function 

private function spellCheck_Mech(strString,binSuggest)
dim ajaxURL
dim arrURL
DIM objXmlHttp
dim strURL
dim strPOST
dim responseXML
dim strXMLTxt
dim strLisc
dim intP
strPOST = "word="&server.urlencode(strString)
	 
strURL = "http://"&request.ServerVariables("HTTP_HOST")&url()&"&ajax=true&id=svr&suggest=true"
strURL = replace(strURL, "&amp;","&")

	SET objXmlHttp = Server.CreateObject("MICROSOFT.XMLHTTP")
	
		call objXmlHttp.open ("POST", strURL, false)
		call objXmlHttp.setRequestHeader ("Content-Type", "application/x-www-form-urlencoded")
	    objXmlHttp.send(strPOST)
	    set responseXML  = objXmlHttp.responseXML
		strXMLTxt = objXmlHttp.responseText
		intP = instr(Lcase(strXMLTxt&""),"<?x")
		if intP>1 then 
		strLisc=left(strXMLTxt,intp-1):  response.write strLisc
		strXMLTxt=right(strXMLTxt,Len(strXMLTxt)-Len(strLisc))  : responseXML.loadXML ( strXMLTxt )
		end if 
		
		
		
	if (responseXML.getElementsByTagName("word").length = 0) then 
			if binSuggest then
				spellCheck_Mech=array()
			else
				spellCheck_Mech=false
			end if
			exit function 
		end if 
		
	if not binSuggest then 	
	 spellCheck_Mech = ( UCASE(responseXML.getElementsByTagName("spellcheck")(0).firstChild.nodeValue) = "TRUE"  	) :	exit function
	end if 
	
		dim  StrSuggestions , i
		
		for i=0 to responseXML.getElementsByTagName("suggestion").length-1 step 1
		if i=0 then StrSuggestions="" else StrSuggestions=StrSuggestions&"~:>@~:>++++"
			StrSuggestions=StrSuggestions& URLDecode(responseXML.getElementsByTagName("suggestion")(i).firstChild.nodeValue)
		next
		spellCheck_Mech=split(StrSuggestions,"~:>@~:>++++")
	
	
end function 

public function linkHTML(linkText,CSSclass)
	if linkText&""="" then linkText="Spell Check"
	if CSSclass&""<>"" then CSSclass=" class="""&CSSclass&""" "
	linkHTML = "<a title="""&linkText&""" href=""#"" "&CSSclass&" onclick=""window.open('"&url()&"','aspSpellWin','width=460, height=290, scrollbars=no');return false;"" >"&linkText&"</a>"
end function 


public function buttonHTML(buttonText,CSSclass)
 buttonHTML = replace (buttonXHTML(buttonText,CSSclass),"/>"," >")
end function 



public function buttonXHTML(buttonText,CSSclass)


	
	if buttonText&""="" then buttonText="Spell Check"
	if CSSclass&""<>"" then CSSclass=" class="""&CSSclass&""" "
	buttonXHTML = "<input type=""button"" alt="""&buttonText&"""  "&CSSclass&" onclick=""window.open('"&url()&"','aspSpellWin','width=460, height=290, scrollbars=no');return false;"" value="""&buttonText&""" />"
end function 




public function imageButtonHTML(buttonImage,rolloverImage, buttonText)
   imageButtonHTML = replace(imageButtonXHTML(buttonImage,rolloverImage, buttonText),"/>"," >")  
end function 

public function imageButtonXHTML(buttonImage,rolloverImage, buttonText)
	if buttonText&""="" then buttonText="Spell Check"
	if rolloverImage="" and buttonImage="" then rolloverImage=setting_ASPSPELLPATH&"Assets/spelliconover.gif"
	if buttonImage&""="" then buttonImage=setting_ASPSPELLPATH&"Assets/spellicon.gif"
	if rolloverImage<> buttonImage then rolloverImage=" onmouseover=""this.src='"&rolloverImage&"'"" onmouseout=""this.src='"&buttonImage&"'""  "
	
	imageButtonXHTML = "<a title="""&buttonText&""" href=""#""  onclick=""window.open('"&url()&"','aspSpellWin','width=460, height=290, scrollbars=no');return false;"" ><img src="""&buttonImage&""" style=""cursor:pointer""   alt="""&buttonText&""" border=""0"" "&rolloverImage&"   /></a>"
end function 


'''''''''''''''LEGACY''''''''''''''''''''

public function addLanguage(strInput)


if ""&strInput="" then exit function 

select case UCASE(TRIM(strInput))
	CASE "EN"
	 strInput="English (International)"
	CASE "ES"
	 strInput="Espagnol"
	CASE "DE"
	 strInput="Deutch"
	CASE "NL"
	 strInput="Nederlands"
	CASE "FR"
	 strInput="Francais"
	CASE "IT"
	 strInput="Italiano"
	CASE "PT"
	 strInput="Portugues"
	CASE ELSE

end select

if setting_Langs&""<>"" then setting_Langs=setting_Langs&"," : setting_MultiLang=true
setting_Langs=setting_Langs&strInput

end function


public function clearLanguages()
setting_Langs="" 
setting_MultiLang=false
end function

public function setMultiLingual()
	setting_MultiLang=true
end function

'''''''''''''''END LEGACY''''''''''''''''''''


Private Function URLDecode(byVal encodedstring)
	Dim strIn, strOut, intPos, strLeft
	Dim strRight, intLoop
	strIn  = encodedstring : strOut = _
		 "" : intPos = Instr(strIn, "+")
	Do While intPos
		strLeft = "" : strRight = ""
		If intPos > 1 then _
			strLeft = Left(strIn, intPos - 1)
		If intPos < len(strIn) then _
			strRight = Mid(strIn, intPos + 1)
		strIn = strLeft & " " & strRight
		intPos = InStr(strIn, "+")
		intLoop = intLoop + 1
	Loop
	intPos = InStr(strIn, "%")
	Do while intPos
		If intPos > 1 then _
			strOut = strOut & _
				Left(strIn, intPos - 1)
		strOut = strOut & _
			Chr(CInt("&H" & _
				mid(strIn, intPos + 1, 2)))
		If intPos > (len(strIn) - 3) then
			strIn = ""
		Else
			strIn = Mid(strIn, intPos + 3)
		End If
		intPos = InStr(strIn, "%")
	Loop
	URLDecode = strOut & strIn
End Function


private sub Class_Initialize() 
setting_Langs=""
G_callback=""
setting_ASPSPELLPATH="/ASPSpellCheck/"
end sub  

 
private sub Class_Terminate() 
end sub  

end class
%>