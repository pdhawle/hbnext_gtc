<%
userID = request("userID")
clientID = request("clientID")
catID = request("catID")

Set DataConn = Server.CreateObject("ADODB.Connection") 	
DataConn.Open Session("Connection"), Session("UserID")

Set oCmd = Server.CreateObject("ADODB.Command")
													
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetUser"
   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)
   .CommandType = adCmdStoredProc
End With
		
Set rsUser = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
													
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetAssignedProjects"
   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)
   .CommandType = adCmdStoredProc
End With
		
Set rsProject = oCmd.Execute
Set oCmd = nothing


Set oCmd = Server.CreateObject("ADODB.Command")
													
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCostCodeCategories"
   .CommandType = adCmdStoredProc
End With
		
Set rsCat = oCmd.Execute
Set oCmd = nothing

if catID <> "" then
	Set oCmd = Server.CreateObject("ADODB.Command")
														
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetCostCodes"
	   .parameters.Append .CreateParameter("@categoryID", adInteger, adParamInput, 8, catID)
	   .CommandType = adCmdStoredProc
	End With
			
	Set rsCodes = oCmd.Execute
	Set oCmd = nothing
end if


%>
<script language="javascript">
<!--
function rept_onchange(addEvent,catID,userID) {
   window.location.href = 'form.asp?formType=addTime&catID='+catID+'&userID='+userID;
}
//-->
</script>
<form name="addEvent" method="post" action="process.asp">

<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sLoginTitle%></span><span class="Header"> - Add Time</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td valign="top" align="right"><strong>Employee Name:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%=rsUser("firstName")%>&nbsp;<%=rsUser("lastName")%>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Category:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="catID" onChange="return rept_onchange(addEvent,this.getElementsByTagName('option')[this.selectedIndex].value,<%=userID%>)">
													<option value="">--select category--</option>
													<%do until rsCat.eof
														if trim(rsCat("categoryID")) = trim(catID) then%>
														<option selected="selected" value="<%=rsCat("categoryID")%>"><%=rsCat("category")%></option>
													<%else%>
														<option value="<%=rsCat("categoryID")%>"><%=rsCat("category")%></option>
													<%end if
													rsCat.movenext
													loop%>
												</select>&nbsp;
												<%if catID = "" then%>
													**select cost code category**
												<%end if%>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<%if catID <> "" then%>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Cost Code:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="costCodeID">
													<%do until rsCodes.eof%>
														<option value="<%=rsCodes("costCodeID")%>"><%=rsCodes("code")%> - <%=rsCodes("type")%></option>
													<%rsCodes.movenext
													loop%>
												</select>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Date:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="date" size="10" maxlength="10" value="<%=formatdatetime(now(),2)%>"/>&nbsp;<a href="javascript:displayDatePicker('date')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Project:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="projectID">
													<option value="0"></option>
													<%do until rsProject.eof%>
														<option value="<%=rsProject("projectID")%>"><%=rsProject("customerName")%>&nbsp;-&nbsp;<%=rsProject("projectName")%></option>
													<%rsProject.movenext
													loop%>
												</select>
											</td>
										</tr>					
										
										
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Hours:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="hours" size="5" maxlength="10"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Billable:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="checkbox" name="billable" />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Comments:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<textarea name="comments" rows="3" cols="23"></textarea>
											</td>
										</tr>										
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td colspan="3" align="right">
												<input type="hidden" name="userID" value="<%=userID%>" />
												<input type="hidden" name="clientID" value="<%=clientID%>" />
												<input type="hidden" name="processType" value="addTime" />
												<input type="submit" value="  Save  " class="formButton"/>
											</td>
										</tr>
										<%end if%>
									</table>
								
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<%if catID <> "" then%>
<script language="JavaScript" type="text/javascript">

  var frmvalidator  = new Validator("addEvent");
  //frmvalidator.addValidation("eventName","req","Please enter an event");
  frmvalidator.addValidation("date","req","Please enter the date");
  frmvalidator.addValidation("hours","req","Please enter the hours");
</script>
<%end if%>