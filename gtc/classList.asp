<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%

customerID = request("customerID")

Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetClasses"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, customerID)
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rs = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomer"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, customerID)
   .CommandType = adCmdStoredProc
   
End With
			
Set rsCust = oCmd.Execute
Set oCmd = nothing



%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<link rel="stylesheet" type="text/css" href="menu/popupmenu.css" />
<script type="text/javascript" src="menu/jquery.min.js"></script>
<script type="text/javascript" src="menu/popupmenu.js"></script>
<script language="JavaScript">
<!--
function confirmDelete(delUrl) {
 if (confirm("Are you sure you wish to delete this class?")) {
    document.location = delUrl;
  }


}
//-->
</script>
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Class List</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<a href="form.asp?formType=addClass&customerID=<%=customerID%>" class="footerLink">add class</a>&nbsp;&nbsp;
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="customerList.asp" class="footerLink">customer list</a>			
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td colspan="9">
												<!--#include file="custDropdown.asp"-->
											</td>
										</tr>
										<tr bgcolor="#666666">
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td><span class="searchText">Class Name</span></td>
											<td><span class="searchText">Open<br />Slots</span></td>
											<td><span class="searchText">Available<br />Slots</span></td>
											<td><span class="searchText">Location</span></td>
											<td><span class="searchText">Students</span></td>
											<td><span class="searchText">Start Date/Time</span></td>
											<td><span class="searchText">End Date/Time</span></td>
											<td><span class="searchText">Est.<br />Cost</span></td>
											<td align="right"><span class="searchText">Action</span></td>
										</tr>
										<tr><td colspan="9"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<%If rs.eof then%>
											<tr><td></td><td colspan="7">there are no records to display</td></tr>
										<%else
											blnChange = true
											
											Do until rs.eof
												iNumAvail = 0
												If blnChange = true then%>
													<tr class="rowColor">
												<%else%>
													<tr>
												<%end if%>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td><%=rs("classType")%></td>
													<td><%=rs("openSlots")%></td>
													<td>
														<%Set oCmd = Server.CreateObject("ADODB.Command")
	
														With oCmd
														   .ActiveConnection = DataConn
														   .CommandText = "spGetNumStudentsAssigned"
														   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, rs("classID"))
														   .CommandType = adCmdStoredProc
														   
														End With
																	
														Set rsNumOpen = oCmd.Execute
														Set oCmd = nothing
														
														iNumAvail = rs("openSlots") - rsNumOpen("numSlots")
														
														response.Write iNumAvail%>
													</td>
													<td><%=rs("location")%></td>
													<td><a href="assignStudents.asp?classID=<%=rs("classID")%>&customerID=<%=customerID%>">Manage/Assign</a></td>
													<td><%=formatDateTime(rs("startDate"),2)%>&nbsp;<%=rs("startTime")%></td>
													<td><%=formatDateTime(rs("endDate"),2)%>&nbsp;<%=rs("endTime")%></td>
													<td>
														<!--run the class through a function to calculate the class cost-->
														<%=calculateClassCost(rs("classID"),rs("classTypeID"))%>
													</td>
													<td align="right">
														<a href="form.asp?classID=<%=rs("classID")%>&formType=editClass&customerID=<%=customerID%>"><img src="images/edit.gif" width="19" height="19" alt="edit" border="0"></a>&nbsp;&nbsp;<input type="image" src="images/remove.gif" width="11" height="13" alt="delete" border="0" onClick="return confirmDelete('process.asp?id=<%=rs("classID")%>&processType=deleteClass&customerID=<%=customerID%>')">&nbsp;
													</td>
													
												</tr>
											<%rs.movenext
											if blnChange = true then
												blnChange = false
											else
												blnChange = true
											end if
											loop
										end if%>
									</table>
									
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing

%>