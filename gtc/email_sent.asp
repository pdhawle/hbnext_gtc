<!--#include file="includes/constants.inc"-->
<%t = request("type")
clientID = request("clientID")
faxType = request("faxType")
emailType = request("emailType")
%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sLoginTitle%></span><span class="Header"> - 
								<%if t = "fax" then%>
									Send Fax
								<%else%>
									Send Email
								
								<%end if%>
								
								</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<%if t = "fax" then%>
										Your fax has been sent successfully. An email will be sent to you confirming the successful fax transmission.<br><br>
										
										<%select case faxType
											case "Report"%>
												<a href="reportList.asp">Report List</a><br>
											<%case else%>
												<a href="quoteList.asp?clientID=<%=clientID%>&init=true">Inspection Quote List</a><br>
										<%end select%>
										
										
									<%else%>
										Your email has been sent successfully.<br><br>
										
										<%select case emailType
			
											case "quote"%>
												<a href="quoteList.asp?clientID=<%=clientID%>">Inspection Quote List</a><br>
											<%case else%>
												<a href="reportList.asp">Report List</a><br>
										<%end select%>
									
									<%end if%>
															
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>