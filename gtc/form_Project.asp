<%
customerID = request("id")
divisionID = request("divisionID")
sState = request("state")

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
'Create command
'get the division name and customer name
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetDivision"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, divisionID)
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsDivision = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomer"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, customerID)
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsCustomer = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetStates"
   .CommandType = adCmdStoredProc   
End With
	
Set rsState = oCmd.Execute
Set oCmd = nothing

If sState <> "" then

	Set oCmd = Server.CreateObject("ADODB.Command")
	
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetCountiesByState"
	   .parameters.Append .CreateParameter("@ID", adVarchar, adParamInput, 50, trim(sState))
	   .CommandType = adCmdStoredProc   
	End With
		
	Set rsCounty = oCmd.Execute
	Set oCmd = nothing

end if

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetRegions"
   .CommandType = adCmdStoredProc
   
End With
	
Set rsRegions = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetRegionalOffices"
   .CommandType = adCmdStoredProc
   
End With
	
Set rsRegionalOffices = oCmd.Execute
Set oCmd = nothing
%>
<script type="text/javascript">
<!--
//function proj_onchange(addProject,stateID,customerID,divisionID) {
 //  window.location.href = 'form.asp?formType=addProject&state='+stateID+'&id='+customerID+'&divisionID='+divisionID;
//}

function proj_onchange(addProject) {
   document.addProject.action = "form.asp?formType=addProject";
   addProject.submit(); 
}
// -->
</script>
<form name="addProject" method="post" action="process.asp">
<input type="hidden" name="id" value="<%=customerID%>" />
<input type="hidden" name="divisionID" value="<%=divisionID%>" />
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Add Project</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td colspan="3">
												<strong>Customer:</strong> <%=rsCustomer("customerName")%><br />
												<strong>Division:</strong> <%=rsDivision("division")%><br /><br />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Project Type:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="projectTypeID" onChange="return proj_onchange(addProject)">
													<option value="0">--select project type--</option>
													<option value="1" <%=isSelected("1",request("projectTypeID"))%>>Substation</option>
													<option value="2" <%=isSelected("2",request("projectTypeID"))%>>Transmission Line</option>
												</select>
												
											</td>
										</tr>				
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Project Name:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="projectName" size="30" tooltipText="Please enter the name of the project." value="<%=request("projectName")%>" />
												
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Project Number:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="projectNumber" size="30" maxlength="50" tooltipText="Please enter the project number. This is optional and mainly used for DOT projects." value="<%=request("projectNumber")%>"/>
												
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>NTU Limit:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="NTULimit" size="10" maxlength="50" value="<%=request("NTULimit")%>"/>
												
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Region:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="regionID">
													<%do until rsRegions.eof%>
														<option <%=isSelected(rsRegions("regionID"),request("regionID"))%> value="<%=rsRegions("regionID")%>"><%=rsRegions("region")%></option>
													<%rsRegions.movenext
													loop%>
												</select>						
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>State:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td><!--,this.getElementsByTagName('option')[this.selectedIndex].value,<%'=customerID%>,<%'=divisionID%>-->
												<select name="state" onChange="return proj_onchange(addProject)">
													<option value="">--select state--</option>
													<%do until rsState.eof
														if sState = rsState("stateID") then%>
															<option selected="selected" value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
														<%else%>
															<option value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
													<%end if
													rsState.movenext
													loop%>
												</select>						
											</td>
										</tr>				
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										
										<script type="text/javascript" src="jquery/jquery.js"></script>
										<script type="text/javascript" src="jquery/jquery.ui.js"></script>
										<script type="text/javascript" src="jquery/jquery.asmselect.js"></script>
										<script type="text/javascript">
											$(document).ready(function() {
												$("select[multiple]").asmSelect({
													addItemTarget: 'bottom',
													animate: true,
													highlight: true,
													sortable: true
												});
												
											}); 
									
										</script>
										<link rel="stylesheet" type="text/css" href="jquery/jquery.asmselect.css" />
										
										
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>County:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%if sState = "" then%>
													Please select a state above.
												<%else%>
													<select id="county" multiple="multiple" name="county" title="select all that apply">
														<%do until rsCounty.eof%>
															<option value="<%=rsCounty("countyID")%>"><%=rsCounty("county")%></option>
														<%rsCounty.movenext
														loop%>
													</select>
												<%end if%>
												
											</td>
										</tr>								
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>City:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="city" size="30" maxlength="100" tooltipText="Please enter the city of where the project is located." value="<%=request("city")%>" />
												
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Regional Office:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="officeID">
													<option></option>
													<%do until rsRegionalOffices.eof%>
														<option <%=isSelected(rsRegionalOffices("officeID"),request("officeID"))%> value="<%=rsRegionalOffices("officeID")%>"><%=rsRegionalOffices("office")%></option>
													<%rsRegionalOffices.movenext
													loop%>
												</select>						
											</td>
										</tr>
										
										
										
										<%if request("projectTypeID") = "2" then%>
											<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
											<tr>
												<td valign="top" align="right"><strong>Latitude Start:</strong></td>
												<td><img src="images/pix.gif" width="5" height="1"></td>
												<td>
													<input type="text" name="latitude" size="20" value="<%=rsProject("latitude")%>" maxlength="20"/>&nbsp;example: 32.81527												
												</td>
											</tr>	
											<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
											<tr>
												<td valign="top" align="right"><strong>Longitude Start:</strong></td>
												<td><img src="images/pix.gif" width="5" height="1"></td>
												<td>
													<input type="text" name="longitude" size="20" value="<%=rsProject("longitude")%>" maxlength="20"/>&nbsp;example: -84.743189												
												</td>
											</tr>
											<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
											<tr>
												<td valign="top" align="right"><strong>Latitude End:</strong></td>
												<td><img src="images/pix.gif" width="5" height="1"></td>
												<td>
													<input type="text" name="latitude2" size="20" value="<%=rsProject("latitude2")%>" maxlength="20"/>&nbsp;example: 32.81527												
												</td>
											</tr>	
											<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
											<tr>
												<td valign="top" align="right"><strong>Longitude End:</strong></td>
												<td><img src="images/pix.gif" width="5" height="1"></td>
												<td>
													<input type="text" name="longitude2" size="20" value="<%=rsProject("longitude2")%>" maxlength="20"/>&nbsp;example: -84.743189												
												</td>
											</tr>
											<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<%else%>
											<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
											<tr>
												<td valign="top" align="right"><strong>Latitude:</strong></td>
												<td><img src="images/pix.gif" width="5" height="1"></td>
												<td>
													<input type="text" name="latitude" size="20" value="<%=rsProject("latitude")%>" maxlength="20"/>&nbsp;example: 32.81527												
													<input type="hidden" name="latitude2" value="" />
													<input type="hidden" name="longitude2" value="" />
												</td>
											</tr>	
											<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
											<tr>
												<td valign="top" align="right"><strong>Longitude:</strong></td>
												<td><img src="images/pix.gif" width="5" height="1"></td>
												<td>
													<input type="text" name="longitude" size="20" value="<%=rsProject("longitude")%>" maxlength="20"/>&nbsp;example: -84.743189												
												</td>
											</tr>
											<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<%end if%>
										
										<tr>
											<td valign="top" align="right"><strong>LDA Date:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="startDate" maxlength="10" size="10" value="<%=request("startDate")%>" />&nbsp;<a href="javascript:displayDatePicker('startDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
												
											</td>
										</tr>				
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>NOT Date:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="endDate" maxlength="10" size="10" value="<%=request("endDate")%>" />&nbsp;<a href="javascript:displayDatePicker('endDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
												
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Auto Email:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%
											'	blnAutoEmail = request("autoEmail")
												
											'	if blnAutoEmail = "" then
											'		blnAutoEmail = "checked"
											'	end if
											'	response.Write blnAutoEmail
												%>
												<input type="checkbox" name="autoEmail" checked="checked"/> *
												
											</td>
										</tr>	
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td colspan="3" align="right">
												<input type="hidden" name="division" value="<%=divisionID%>"/>
												<input type="hidden" name="customer" value="<%=customerID%>"/>
												<input type="hidden" name="processType" value="addProject" />
												<%if sState = "" then%>
													<input type="submit" value="  Save  " class="formButton" disabled="disabled"/>
												<%else%>
													<input type="submit" value="  Save  " class="formButton"/>
												<%end if%>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
									</table>
									<table>
										<tr>
											<td>
												* When checked, emails will automatically be sent to the contacts we have on file for that project when a report is created. Default is manual.<br />
												Note: You must have contacts added to the project in order for the auto email function to work.<br /><br />
												
												Latitude and Longitude are used for locating the project on the map.
											</td>
										</tr>
									</table>
								
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<script language="JavaScript" type="text/javascript">

  var frmvalidator  = new Validator("addProject");
  
  frmvalidator.addValidation("projectTypeID","dontselect=0","Please select a project type");
  frmvalidator.addValidation("projectName","req","Please enter the project name");
  frmvalidator.addValidation("projectNumber","req","Please enter the project number");
  //frmvalidator.addValidation("city","req","Please enter the city that the project is in");
 // frmvalidator.addValidation("county","req","Please enter the county that the project is in");
 // frmvalidator.addValidation("startDate","req","Please enter the date the inspections start");
</script>