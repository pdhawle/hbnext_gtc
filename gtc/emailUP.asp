<!--#include file="includes/constants.inc"-->
<%
bLoggedInInfo = "False"
appLogo = request("appLogo")
refer = request("refer")
msg = request("msg")

if appLogo <> "" then
	Session("appLogo") = appLogo
end if
%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script language="JavaScript" src="includes/validator.js" type="text/javascript"></script>
</head>
<body>
<form name="emailUP" method="post" action="process.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sLoginTitle%></span><span class="Header"> - Email Login Info</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									Please enter your email address and your username and password will be emailed to you.<br /><br />
									<%if msg <> "" then%>
										<strong><%=msg%></strong><br><br>
									<%end if%>
									<input type="text" name="email" size="30" maxlength="100" /><br /><br />
									<input type="hidden" name="refer" value="<%=trim(refer)%>" />
									<input type="hidden" name="appLogo" value="<%=appLogo%>" />
									<input type="hidden" name="processType" value="emailUP" />
									<input type="submit" name="submitEmail" value="Submit" class="formButton"/>
									
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<script language="JavaScript" type="text/javascript">

  var frmvalidator  = new Validator("emailUP");
  frmvalidator.addValidation("email","req","Please enter your email address");
  frmvalidator.addValidation("email","email");
</script>
</body>
</html>