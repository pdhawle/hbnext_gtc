<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%

iProjectID = request("projectID")
inspector = request("inspector")
smonth = request("month")
syear = request("year")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetAgentReportsByMonthandYear"
   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, inspector)
   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, iProjectID)
   .parameters.Append .CreateParameter("@month", adInteger, adParamInput, 8, smonth)
   .parameters.Append .CreateParameter("@year", adInteger, adParamInput, 8, syear)
   .CommandType = adCmdStoredProc   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsReport = oCmd.Execute
Set oCmd = nothing

%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Weekly/Post Rain Reports</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td></td>
											<td colspan="6">
												<strong><%=getMonth(smonth) & " " & sYear%></strong>
											</td>
										</tr>		
										
										
										<tr><td colspan="7"><img src="images/pix.gif" width="1" height="20"></td></tr>
										<tr>
											<td></td>
											<td colspan="6">
												<strong>Weekly/Post Rain Reports</strong>
											</td>
										</tr>
										<tr>
											<td colspan="7">
												<table width="100%" cellpadding="0" cellspacing="0">
													<tr bgcolor="#666666">
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td><span class="searchText">Report ID</span></td>
														<td><span class="searchText">Customer</span></td>
														<td><span class="searchText">Project</span></td>
														<td><span class="searchText">Agent/Inspector</span></td>
														<td><span class="searchText">Inspection Date</span></td>
														<td><span class="searchText">ReportType</span></td>
													</tr>
													<%if rsReport.eof then%>
														<tr><td></td><td colspan="8">There are no result for the items you chose. Please select again.</td></tr>
													<%else
													blnChange = false
										
													do until rsReport.eof
																													
															If blnChange = true then%>
																<tr class="rowColor">
															<%else%>
																<tr>
															<%end if%>
																<td></td>
																<td><%=rsReport("reportID")%></td>
																<td><%=rsReport("customerName")%></td>
																<td><%=rsReport("projectName")%></td>
																<td><%=rsReport("firstName") & " " & rsReport("lastName")%></td>
																<td><%=rsReport("inspectionDate")%></td>
																<td><%=rsReport("reportType")%></td>
															</tr>
															
															<%
															if blnChange = true then
																blnChange = false
															else
																blnChange = true
															end if
													
													rsReport.movenext
																
													loop
													rsReport.close
													set rsReport = nothing
													end if%>
												</table>
											</td>
										</tr>	
										<tr>
											<td></td>
											<td colspan="6">
												<img src="images/pix.gif" width="760" height="1">
											</td>
										</tr>
										<tr><td colspan="7"><img src="images/pix.gif" width="1" height="10"></td></tr>
									</table>
															
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>
<%
rsReport.close
DataConn.close
Set rsCustomer = nothing
Set rsReport = nothing
Set DataConn = nothing
Set oCmd = nothing
%>