<%topicID = request("id")

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetToolboxTopic"
   .parameters.Append .CreateParameter("@topicID", adInteger, adParamInput, 8, topicID)
   .CommandType = adCmdStoredProc   
End With
			
Set rs = oCmd.Execute
Set oCmd = nothing
%>
<form name="editTopic" method="post" action="process.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Edit Toolbox Topic</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td valign="top" align="right"><strong>Topic:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="topic" size="50" value="<%=rs("topic")%>"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<%
										Set fso = CreateObject("Scripting.FileSystemObject")						
										Set folder = fso.GetFolder(server.mappath("topics"))
										'Response.Write folder
										Set filez = folder.Files			
										FileCount = folder.Files.Count
										strPath = "topics/"
										Set objFSO = Server.CreateObject("Scripting.FileSystemObject")
										Set objFolder = objFSO.GetFolder(Server.MapPath(strPath))									
										%>										
										<tr>
											<td valign="top" align="right"><strong>Document:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="document">
												<%For Each objItem In objFolder.Files 
													if trim(objItem.Name) = trim(rs("document")) then%>
														<option selected="selected" value="<%=trim(objItem.Name)%>"><%=trim(objItem.Name)%></option>
													<%else%>
														<option value="<%=trim(objItem.Name)%>"><%=trim(objItem.Name)%></option>
													<%end if%>
												<%next%>
												</select>
												<%Set objItem = Nothing
												Set objFolder = Nothing
												Set objFSO = Nothing%>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="15"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Help Text:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%
													strFormName = "editTopic"
													strTextAreaName = "helpText"
												%>
												<textarea name="helpText" cols="70" rows="10"><%=rs("helpText")%></textarea>
												<script>
													//STEP 2: Replace the textarea (txtContent)
													oEdit1 = new InnovaEditor("oEdit1");
													oEdit1.features=["FullScreen","Preview","Print","Search",
														"Cut","Copy","Paste","PasteWord","PasteText","|","Undo","Redo","|",
														"ForeColor","BackColor","|","Bookmark","Hyperlink","XHTMLSource","BRK",
														"Numbering","Bullets","|","Indent","Outdent","LTR","RTL","|",
														"Image","Flash","Media","|","Table","Guidelines","Absolute","|",
														"Characters","Line","Form","RemoveFormat","ClearAll","BRK",
														"StyleAndFormatting","TextFormatting","ListFormatting","BoxFormatting",
														"ParagraphFormatting","CssText","|",
														"Paragraph","FontName","FontSize","|",
														"Bold","Italic","Underline","Strikethrough","Superscript","Subscript","|",
														"JustifyLeft","JustifyCenter","JustifyRight","JustifyFull"];
													oEdit1.REPLACE("helpText");
												</script>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td colspan="3" align="right">
												<input type="hidden" name="topicID" value="<%=topicID%>" />
												<input type="hidden" name="processType" value="editTopic" />
												<input type="submit" value="  Save  " class="formButton"/>
											</td>
										</tr>
									</table>
								
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<script language="JavaScript" type="text/javascript">

  //var frmvalidator  = new Validator("editTopic");
  //frmvalidator.addValidation("topic","req","Please enter a topic");
</script>