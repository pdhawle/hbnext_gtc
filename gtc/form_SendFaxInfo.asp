<%
infoPackID = request("infoPackID")
customerID = Request("customerID")
customerContactID = Request("customerContactID")
contactFax = request("fax")
'replace the -
contactFax = Replace(contactFax,"-","")
'replace the .
contactFax = Replace(contactFax,".","")
'replace the spaces
contactFax = Replace(contactFax," ","")
contactFax = Replace(contactFax,"(","")
contactFax = Replace(contactFax,")","")
'get the first three numbers
sFax1 = left(contactFax,3)

'get the second three numbers
sFax2 = right(contactFax,7)
sFax2 = left(sFax2,3)

'get the last 4 numbers
sFax3 = right(contactFax,4)

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 	
DataConn.Open Session("Connection"), Session("UserID")

Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetInfoPacksByContact"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, customerContactID)
   .CommandType = adCmdStoredProc   
End With
Set rs = oCmd.Execute
Set oCmd = nothing
%>
<script type="text/javascript" src="includes/autoTab.js"></script>
<form name="sendFax" method="post" action="sendFaxInfo.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Send Fax</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td colspan="3">
												<strong>Customer Name:</strong>&nbsp;<%=rs("customerName")%><br />
												<strong>Contact Name:</strong>&nbsp;<%=rs("contactName")%>&nbsp;<%=rs("contactLastName")%><br /><br />
											</td>
										</tr>
										<tr>	
											<td valign="top"><strong>Country Code:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="countryCode" size="1" value="1"/> United States is 1. This is the default.
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top"><strong>Fax Number:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" onKeyUp="return autoTab(this, 3, event);" name="faxNumber1" size="3" maxlength="3" value="<%=sFax1%>"/>&nbsp;&nbsp;<input type="text" onKeyUp="return autoTab(this, 3, event);" name="faxNumber2" size="3" maxlength="3" value="<%=sFax2%>"/>&nbsp;&nbsp;<input type="text" name="faxNumber3" size="3" maxlength="4" value="<%=sFax3%>"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td colspan="3" align="right">
											
												<input type="hidden" name="faxType" value="<%=faxType%>" />
												<input type="hidden" name="infoPackID" value="<%=infoPackID%>" />
												<input type="hidden" name="customerID" value="<%=customerID%>" />
												<input type="hidden" name="customerContactID" value="<%=customerContactID%>" />
												<input type="submit" value="  Send Fax  " class="formButton"/><br /><br />
												
												*please note: there will be a 20&cent; <br />charge for every fax sent - billed monthly
											</td>
										</tr>
									</table>
								
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<script language="JavaScript" type="text/javascript">

  var frmvalidator  = new Validator("sendFax");
  frmvalidator.addValidation("faxNumber1","req","Please enter the fax number");
  frmvalidator.addValidation("faxNumber2","req","Please enter the fax number");
  frmvalidator.addValidation("faxNumber3","req","Please enter the fax number");
</script>