<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
Dim rs, oCmd, DataConn

divisionID = Request("divisionID")
customerID = Request("customerID")
locateLogID = request("locateLogID")
pg = request("pg")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetUtility"
	.parameters.Append .CreateParameter("@locateLogID", adInteger, adParamInput, 8, locateLogID)
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rs = oCmd.Execute
Set oCmd = nothing
%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<script language="javascript" src="includes/datePicker.js"></script>
<link rel="stylesheet" href="includes/datePicker.css" type="text/css">
<link rel="stylesheet" href="includes/main.css" type="text/css">
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Utility Locate Log</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<a href="form.asp?formType=editUtility&locateLogID=<%=rs("locateLogID")%>&customerID=<%=customerID%>&divisionID=<%=divisionID%>&pg=<%=pg%>" class="footerLink">edit utility locate log item</a>&nbsp;&nbsp;
						<%if pg = "ol" then%>
							<span class="footerLink">|</span>&nbsp;&nbsp;<a href="openLots.asp?id=<%=rs("projectID")%>&divisionID=<%=divisionID%>&customerID=<%=customerID%>" class="footerLink">open lots</a>&nbsp;&nbsp;
						<%else%>
							<span class="footerLink">|</span>&nbsp;&nbsp;<a href="closedLots.asp?id=<%=rs("projectID")%>&divisionID=<%=divisionID%>&customerID=<%=customerID%>" class="footerLink">closed lots</a>&nbsp;&nbsp;
						<%end if%>

					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td bgcolor="#666666"><span class="searchText">&nbsp;Project Name:</span></td>
											<td>&nbsp;</td>
											<td class="rowColor">&nbsp;<%=rs("projectName")%></td>
										</tr>
										<tr><td colspan="7"><img src="images/pix.gif" width="1" height="1"></td></tr>
										<tr>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td bgcolor="#666666"><span class="searchText">&nbsp;Lot Number:</span></td>
											<td>&nbsp;</td>
											<td>&nbsp;<%=rs("lotNumber")%></td>
										</tr>
										<tr><td colspan="7"><img src="images/pix.gif" width="1" height="1"></td></tr>
										<tr>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td bgcolor="#666666"><span class="searchText">&nbsp;Address:</span></td>
											<td>&nbsp;</td>
											<td class="rowColor">&nbsp;<%=rs("address")%></td>
										</tr>
										<tr><td colspan="7"><img src="images/pix.gif" width="1" height="1"></td></tr>
										<tr>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td bgcolor="#666666"><span class="searchText">&nbsp;Confirmation Number:</span></td>
											<td>&nbsp;</td>
											<td>&nbsp;<%=rs("confirmationNumber")%></td>
										</tr>
										<tr><td colspan="7"><img src="images/pix.gif" width="1" height="1"></td></tr>
										<tr>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td bgcolor="#666666"><span class="searchText">&nbsp;Contractor:</span></td>
											<td>&nbsp;</td>
											<td class="rowColor">&nbsp;<%=rs("contractor")%></td>
										</tr>
										<tr><td colspan="7"><img src="images/pix.gif" width="1" height="1"></td></tr>
										<tr>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td bgcolor="#666666"><span class="searchText">&nbsp;Date Begins:</span></td>
											<td>&nbsp;</td>
											<td>&nbsp;<%=rs("dateBegins")%></td>
										</tr>
										<tr><td colspan="7"><img src="images/pix.gif" width="1" height="1"></td></tr>
										<tr>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td bgcolor="#666666"><span class="searchText">&nbsp;Renew By:</span></td>
											<td>&nbsp;</td>
											<td class="rowColor">&nbsp;<%=rs("renewBy")%></td>
										</tr>
										<tr><td colspan="7"><img src="images/pix.gif" width="1" height="1"></td></tr>
										<tr>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td bgcolor="#666666"><span class="searchText">&nbsp;Date Ends:</span></td>
											<td>&nbsp;</td>
											<td>&nbsp;<%=rs("dateEnds")%></td>
										</tr>
						
										<tr><td></td><td></td><td></td><td><img src="images/pix.gif" width="600" height="10"></td></tr>
									</table>
									
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>