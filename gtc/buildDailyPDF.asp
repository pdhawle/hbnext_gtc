<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%'need customerID, reportID

'iDivisionID = request("divisionID")
reportID = request("reportID")

'iDivisionID = 1
'reportID = 129

Session("LoggedIn") = "True"

Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetReportByID"
    .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, reportID) 'reportID
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsReport = oCmd.Execute
Set oCmd = nothing
%>
<html>
<head>
<title>SWSIR</title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/report.css" type="text/css">
</head>
<body>

<table width=650 cellpadding=0 cellspacing=0>
	<tr>
		<td valign=top colspan="2">
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td valign="middle">
						<%if rsReport("logo") = "" then%>
							<img src="images/logo.jpg">
						<%else%>
							<img src="<%=rsReport("logo")%>">
						<%end if%>
					</td>
					<td valign="middle" align="right"><span class="Header">Report #<%=reportID%></span></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=5></td></tr>
	<tr>
		<td colspan=2>
			<table width=650>
				<tr>
					<td valign="top">
						<b>Inspection Type:</b> <%=rsReport("inspectionType")%><br>
						<b>Customer:</b> <%=rsReport("customerName")%><br>
						<b>Project Status:</b> <%=rsReport("projectStatus")%><br>							
					</td>
					<td></td>
					<td valign="top">
						<b>Report Type:</b> <%=rsReport("reportType")%><br>
						<b>Project:</b> <%=rsReport("projectName")%><br>
						<b>Report Date:</b> <%=month(rsReport("inspectionDate")) & "/" & year(rsReport("inspectionDate"))%>
					</td>
				</tr>
				<%if rsReport("comments") <> "" then%>
				<tr>
					<td colspan="3">
						<b>Comments:</b> <%=rsReport("comments")%>
					</td>
				</tr>
				<%end if%>
			</table>
			
		</td>
	</tr>
	<tr>
		<td colspan=2>
			<table width=650 cellpadding="0" cellspacing="0">
					<tr>
						<td colspan=2>
							<table class="borderTable2" width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr bgcolor="#666666">
									<td align="center"><span class="searchText">Date&nbsp;</span></td>
									<td align="center"><span class="searchText">Rainfall</span></td>
									<td align="center"><span class="searchText">Petroleum Storage Area</span></td>
									<td align="center"><span class="searchText">Construction Exits</span></td>
									<td align="center"><span class="searchText">&nbsp;Qualified Inspector&nbsp;</span></td>
								</tr>
								<tr>
									<td class="tCol" align="center">&nbsp;</td>
									<td class="tCol" align="center">Enter Rainfall Amount</td>
									<td class="tCol" align="center">Evidence of Spills or leaks?</td>
									<td class="tCol" align="center">Repairs or corrections needed?</td>
									<td class="tCol" align="center">Signature</td>
								</tr>
								<%Dim counter
								counter=1
								blnChange = true
								for counter = 1 to 31
										If blnChange = true then%>
											<tr class="rowColor">
										<%else%>
											<tr>
										<%end if
										
										'Create command
										Set oCmd = Server.CreateObject("ADODB.Command")
											
										With oCmd
										   .ActiveConnection = DataConn
										   .CommandText = "spGetDailyReport"
											.parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID)
											.parameters.Append .CreateParameter("@dayOfMonth", adInteger, adParamInput, 8, counter)
										   .CommandType = adCmdStoredProc
										   
										End With
										Set rsDaily = oCmd.Execute
										Set oCmd = nothing
										
										'do until rsDaily.eof
											
											if rsDaily.eof then%>
												<td class="tCol" align="center"><%=counter%></td>
												<td class="tCol" align="center">&nbsp;</td>
												<td class="tCol" align="center">&nbsp;</td>
												<td class="tCol" align="center">&nbsp;</td>
												<td class="tCol" align="center">&nbsp;</td>
											<%else%>
												<td class="tCol" align="center"><%=rsDaily("dayOfMonth")%></td>
												<td class="tCol" align="center"><%=formatnumber(rsDaily("rainfallAmount"),2)%></td>
												<td class="tCol" align="center"><%=getAnswer(rsDaily("question1Response"))%></td>
												<td class="tCol" align="center"><%=getAnswer(rsDaily("question2Response"))%></td>
												<td class="tCol" align="center"><%=rsDaily("firstName") & " " & rsDaily("lastName")%></td>
												
											<%end if%>
											 </tr>
										<%								  
									
									if blnChange = true then
										blnChange = false
									else
										blnChange = true
									end if
								next%>
							</table>
							
						</td>
					</tr>
			</table>
		</td>
	</tr>
	<tr><td colspan="2"><img src=images/pix.gif width=1 height=10></td></tr>
	<tr>
		<td colspan="2">
			<em>I certify under the penalty of law that this report and all attachments were prepared
			under my direction or supervision in accordance with a system designed to assure that
			qualified personnel properly gather and evaluate the information submitted. Based on my
			inquiry of the person or persons who manage the system, or those persons directly
			responsible for gathering the information, the information submitted is, to the best of my
			knowledge and belief, true, accurate, and complete. I am aware that there are significant
			penalties for submitting false information, including the possibility of fine and imprisonment
			for knowing violations.</em><br><br>
			
			<strong>Inspectors Signature:</strong> 
			<%if rsReport("sigFileName") <> "" then%> 
				<img src="userUploads/<%=rsReport("userID")%>/<%=rsReport("sigFileName")%>">&nbsp;&nbsp;
			<%else%>
			______________________________ 
			<%end if%>
			&nbsp;&nbsp;<strong>Date:</strong> <%=rsReport("inspectionDate")%>		
		</td>
	</tr>
</table>
</body>
</html>
<%
function getAnswer(resp)
	If resp = "True" Then
		resp = "Yes"
	else
		resp = "No"
	end if
	getAnswer = resp
end function
%>