<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
customerID = Request("customerID")
clientID = request("clientID")
blnVerify = request("v")


Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If


On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 	
DataConn.Open Session("Connection"), Session("UserID")

if blnVerify = "True" then

	Set oCmd = Server.CreateObject("ADODB.Command")
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetInfoPacksByClientUnverified"
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, clientID)
	   .CommandType = adCmdStoredProc   
	End With
				
	Set rs = oCmd.Execute
	Set oCmd = nothing
	
else

	Set oCmd = Server.CreateObject("ADODB.Command")
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetCustomersByClientAll"
	   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
	   .CommandType = adCmdStoredProc   
	End With
				
	Set rsCustomer = oCmd.Execute
	Set oCmd = nothing
	
	if customerID <> "" then
		Set oCmd = Server.CreateObject("ADODB.Command")
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetInfoPacks"
		   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, customerID)
		   .CommandType = adCmdStoredProc   
		End With
					
		Set rs = oCmd.Execute
		Set oCmd = nothing
	End if
	
end if
%>

<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script type="text/javascript">
    var GB_ROOT_DIR = "<%=sPDFPath%>/greybox/";
</script>
<script type="text/javascript" src="greybox/AJS.js"></script>
<script type="text/javascript" src="greybox/AJS_fx.js"></script>
<script type="text/javascript" src="greybox/gb_scripts.js"></script>
<link href="greybox/gb_styles.css" rel="stylesheet" type="text/css" />
<script language="JavaScript">
<!--
function confirmDelete(delUrl) {
 if (confirm("Are you sure you wish to delete this info pack?")) {
    document.location = delUrl;
  }


}

function cust_onchange(infoPackList) {
   document.infoPackList.action = "infoPackList.asp";
   infoPackList.submit(); 
}
//-->
</script>
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Info Pack List</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<%if blnVerify = "True" then%>
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<a href="infoPackList.asp?clientID=<%=clientID%>" class="footerLink">view info packs</a>			
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<%end if%>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<%if blnVerify <> "True" then%>
											<tr>
												<td colspan="10">
												<form name="infoPackList" method="post" action="infoPackList.asp">
													<select name="customerID" onChange="return cust_onchange(infoPackList)">
														<option value="">--Select Customer--</option>	
														<%do until rsCustomer.eof
															if trim(rsCustomer("customerID")) = trim(customerID) then%>
																<option selected="selected" value="<%=rsCustomer("customerID")%>"><%=rsCustomer("customerName")%></option>
															<%else%>
																<option value="<%=rsCustomer("customerID")%>"><%=rsCustomer("customerName")%></option>
														<%end if
														rsCustomer.movenext
														loop%>
													</select>
													<input type="hidden" name="clientID" value="<%=clientID%>">
												</form>
												</td>
											</tr>
											<tr><td colspan="10" height="10"></td></tr>
											
											<tr bgcolor="#666666">
												<td><img src="images/pix.gif" width="5" height="1"></td>
												<td><span class="searchText">Customer Name</span></td>
												<td><span class="searchText">Contact Name</span></td>
												<td><span class="searchText">Contact Email</span></td>
												<td><span class="searchText">Date Sent</span></td>
												<td><span class="searchText">Verified</span></td>
												<td align="center"><span class="searchText">Action</span></td>
											</tr>
											<tr><td colspan="10"><img src="images/pix.gif" width="1" height="10"></td></tr>
											<%if customerID <> "" then%>
												<%If rs.eof then%>
													<tr><td></td><td colspan="10">there are no records to display</td></tr>
												<%else
													blnChange = true
													Do until rs.eof
														If blnChange = true then%>
															<tr class="rowColor">
														<%else%>
															<tr>
														<%end if%>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td><%=rs("customerName")%></td>
															<td><%=rs("contactName")%>&nbsp;<%=rs("contactLastName")%></td>
															<td><a href="mailto:<%=rs("contactEmail")%>"><%=rs("contactEmail")%></a></td>
															<td><%=rs("date")%></td>
															<td>
																<%if rs("isVerified") = "True" then%>
																	<img src="images/check.gif">
																<%end if%>
															</td>
															<td align="center"><a href="downloads/InfoPack_<%=rs("infoPackID")%>.pdf" target="_blank">view/print</a><!--&nbsp;&nbsp;|&nbsp;&nbsp;<a href="form.asp?formType=sendFaxInfo&infoPackID=<%'=rs("infoPackID")%>&customerID=<%'=customerID%>&customerContactID=<%'=customerContactID%>&fax=<%'=rs("fax")%>">fax</a>--></td>
														</tr>
													<%rs.movenext
													if blnChange = true then
														blnChange = false
													else
														blnChange = true
													end if
													loop
												end if%>
											<%else%>
												<tr><td></td><td colspan="10">please select a customer above</td></tr>
											<%end if%>
										<%else%>
											<tr bgcolor="#666666">
												<td><img src="images/pix.gif" width="5" height="1"></td>
												<td><span class="searchText">Customer Name</span></td>
												<td><span class="searchText">Contact Name</span></td>
												<td><span class="searchText">Contact Email</span></td>
												<td><span class="searchText">Date Sent</span></td>
												<td><span class="searchText">Verified</span></td>
												<td align="center"><span class="searchText">Action</span></td>
											</tr>
											<tr><td colspan="10"><img src="images/pix.gif" width="1" height="10"></td></tr>
												<%	blnChange = true
													Do until rs.eof
														If blnChange = true then%>
															<tr class="rowColor">
														<%else%>
															<tr>
														<%end if%>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td><%=rs("customerName")%></td>
															<td><%=rs("contactName")%>&nbsp;<%=rs("contactLastName")%></td>
															<td><a href="mailto:<%=rs("contactEmail")%>"><%=rs("contactEmail")%></a></td>
															<td><%=rs("date")%></td>
															<td>
																<%if rs("isVerified") = "True" then%>
																	<img src="images/check.gif">
																<%end if%>
															</td>
															<td align="center">
																<a href="downloads/InfoPack_<%=rs("infoPackID")%>.pdf" target="_blank">view/print</a>&nbsp;&nbsp;|&nbsp;&nbsp;
																<a href="form.asp?formType=editInfoPack&infoPackID=<%=rs("infoPackID")%>&customerID=<%=rs("customerID")%>&customerContactID=<%=customerContactID%>&clientID=<%=rs("clientID")%>">verify/send</a>&nbsp;&nbsp;|&nbsp;&nbsp;
																<a href=""><input type="image" src="images/remove.gif" width="11" height="13" alt="delete" border="0" onClick="return confirmDelete('process.asp?id=<%=rs("infoPackID")%>&processType=deleteInfoPack&clientID=<%=rs("clientID")%>')"></a>
															</td>
														</tr>
													<%rs.movenext
													if blnChange = true then
														blnChange = false
													else
														blnChange = true
													end if
													loop%>								
										<%end if%>
									</table>
									
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>