<%
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetMonthlyFinalReport"
    .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, iReportID) 'reportID
   .CommandType = adCmdStoredProc
   
End With
			
Set rsMonthly = oCmd.Execute
Set oCmd = nothing
%>
<table cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td colspan="2">
			<strong>Month:</strong>&nbsp;

			<select name="month">
				<option value="1" <%=isSelected(trim(rsMonthly("month")),1)%>>January</option>
				<option value="2" <%=isSelected(trim(rsMonthly("month")),2)%>>February</option>
				<option value="3" <%=isSelected(trim(rsMonthly("month")),3)%>>March</option>
				<option value="4" <%=isSelected(trim(rsMonthly("month")),4)%>>April</option>
				<option value="5" <%=isSelected(trim(rsMonthly("month")),5)%>>May</option>
				<option value="6" <%=isSelected(trim(rsMonthly("month")),6)%>>June</option>
				<option value="7" <%=isSelected(trim(rsMonthly("month")),7)%>>July</option>
				<option value="8" <%=isSelected(trim(rsMonthly("month")),8)%>>August</option>
				<option value="9" <%=isSelected(trim(rsMonthly("month")),9)%>>September</option>
				<option value="10" <%=isSelected(trim(rsMonthly("month")),10)%>>October</option>
				<option value="11" <%=isSelected(trim(rsMonthly("month")),11)%>>November</option>
				<option value="12" <%=isSelected(trim(rsMonthly("month")),12)%>>December</option>
				
			</select>&nbsp;
			<select name="year">
				<option <%=isSelected(trim(rsMonthly("year")),year(now()) - 1)%> value="<%=year(now()) - 1%>"><%=year(now()) - 1%></option>
				<option <%=isSelected(trim(rsMonthly("year")),year(now()))%> value="<%=year(now())%>"><%=year(now())%></option>
				<option <%=isSelected(trim(rsMonthly("year")),year(now()) + 1)%> value="<%=year(now()) + 1%>"><%=year(now()) + 1%></option>
			</select>
		</td>
	</tr>

	<tr><td height="20"></td></tr>
	
	<tr>
		<td colspan="2"><strong>Stormwater Data</strong></td>
	</tr>
	<tr>
		<td colspan="2">
			<table>
				<tr>
					<td>
						<input type="radio" name="stormwaterData" value="1" <%=isCheckedRadio(trim(rsMonthly("stormwaterData")),"1")%> />&nbsp;Enclosed is GTC�s Monthly NPDES Stormwater Monitoring Report. <br />
						<input type="radio" name="stormwaterData" value="2" <%=isCheckedRadio(trim(rsMonthly("stormwaterData")),"2")%> />&nbsp;There were no stormwater samples retrieved at this project site for this reporting month.
					</td>
				</tr>
			</table>
		</td>
	</tr>
	
	<tr><td height="10"></td></tr>
	
	<tr>
		<td colspan="2"><strong>Potential Violations</strong> <em>(Efforts are continuously being made to prevent, correct, and reduce the likelihood of potential permit violations.)</em></td>
	</tr>
	<tr>
		<td colspan="2">
			<table>
				<tr>
					<td>
						<table cellspacing="0" cellpadding="0">
							<tr>
								<td colspan="2"><input type="checkbox" name="potentialViolationsPermit" <%=isChecked(rsMonthly("potentialViolationsPermit"))%> />&nbsp;Following is a list of what may be considered permit violations for the project. </td>
							</tr>
							<tr>
								<td width="25">&nbsp;</td>
								<td>
									<script type="text/javascript" src="jquery/jquery.js"></script>
									<script type="text/javascript" src="jquery/jquery.ui.js"></script>
									<script type="text/javascript" src="jquery/jquery.asmselect.js"></script>
									<script type="text/javascript">
										$(document).ready(function() {
											$("select[multiple]").asmSelect({
												addItemTarget: 'bottom',
												animate: true,
												highlight: true,
												sortable: true
											});
											
										}); 
								
									</script>
									<link rel="stylesheet" type="text/css" href="jquery/jquery.asmselect.css" />
									<%
									vioList = split(rsMonthly("violationList"), ",") 
									For isLoop = LBound(vioList) to UBound(vioList) 
										sVio = Trim(vioList(isLoop))															
										if trim(sVio) = "Failure to conduct daily inspection." then
											blnVio1 = "selected=" & chr(34) & "selected" & chr(34)
										end if															
										if trim(sVio) = "Failure to conduct BMP inspection every 14 days." then
											blnVio2 = "selected=" & chr(34) & "selected" & chr(34)
										end if															
										if trim(sVio) = "Failure to collect daily rainfall data." then
											blnVio3 = "selected=" & chr(34) & "selected" & chr(34)
										end if	
										if trim(sVio) = "Failure to conduct BMP inspection within 24 hours of a 0.5 inch rainfall event." then
											blnVio4 = "selected=" & chr(34) & "selected" & chr(34)
										end if	
										if trim(sVio) = "Exceedence of NTU limit." then
											blnVio5 = "selected=" & chr(34) & "selected" & chr(34)
										end if
										if trim(sVio) = "Failure to collect stormwater sample." then
											blnVio6 = "selected=" & chr(34) & "selected" & chr(34)
										end if
										if trim(sVio) = "Other" then
											blnVio7 = "selected=" & chr(34) & "selected" & chr(34)
										end if														
									next
									%>	
									<select id="violationList" multiple="multiple" name="violationList" title="select all that apply">
										<option value="Failure to conduct daily inspection." <%=blnVio1%>>Failure to conduct daily inspection.</option>
										<option value="Failure to conduct BMP inspection every 14 days." <%=blnVio2%>>Failure to conduct BMP inspection every 14 days.</option>
										<option value="Failure to collect daily rainfall data." <%=blnVio3%>>Failure to collect daily rainfall data.</option>
										<option value="Failure to conduct BMP inspection within 24 hours of a 0.5 inch rainfall event." <%=blnVio4%>>Failure to conduct BMP inspection within 24 hours of a 0.5 inch rainfall event.</option>
										<option value="Exceedence of NTU limit." <%=blnVio5%>>Exceedence of NTU limit.</option>
										<option value="Failure to collect stormwater sample." <%=blnVio6%>>Failure to collect stormwater sample.</option>
										<option value="Other" <%=blnVio7%>>Other</option>
									</select><br />
									
									If other:<br />
									<input type="text" name="violationOther" value="<%=rsMonthly("violationOther")%>" size="50" maxlength="100" /><br /><br />
								</td>
								</td>
							</tr>
						</table>

						
						<input type="checkbox" name="potentialViolationsInfo" <%=isChecked(rsMonthly("potentialViolationsInfo"))%> />&nbsp;Based on the information available to us, we believe that the project site is in compliance with the General Permit.
					</td>
				</tr>
			</table>
		</td>
	</tr>
	
	<tr><td height="10"></td></tr>
	
	<tr>
		<td colspan="2"><%'=trim(rsMonthly("finalStabilization"))%>
			<input type="checkbox" name="finalStabilization" <%=isChecked(rsMonthly("finalStabilization"))%> />&nbsp;Final Stabilization has been acheived
		</td>
	</tr>
	
</table>
