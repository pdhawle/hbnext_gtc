<%
'get info for this customer
Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomer"
   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, rsProjInfo("customerID"))
   .CommandType = adCmdStoredProc   
End With
			
Set rsCustomer = oCmd.Execute
Set oCmd = nothing

%>

<%If iDivisionID <> "" and iProjectID <> "" then%>
	<table cellpadding="0" cellspacing="0" border="0">		
	
		<tr>
			<td><img src="images/pix.gif" width="5" height="0"></td>
			<td>
				<%
					i = 1
					Set oCmd = Server.CreateObject("ADODB.Command")

					With oCmd
					   .ActiveConnection = DataConn
					   .CommandText = "spGetGeneralOSHAQuestion"
					   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iDivisionID)
					   .CommandType = adCmdStoredProc
					   
					End With
						
						If Err Then
					%>
						<!--#include file="includes/FatalError.inc"-->
					<%
						End If
								
					Set rsQuestion = oCmd.Execute
					Set oCmd = nothing
					%>
					
					<%if not rsQuestion.eof then%>
					<br /><strong class="subHeader">General</strong><br>
					<input type="checkbox" name="addGeneralToReport" checked="checked" />&nbsp;Add general items to report (if unchecked, all general items below will be ignored)<br /><br>
					<%end if%>
					<table width="800" cellpadding="0" cellspacing="0" border="0">
						<%blnChange = true
						do until rsQuestion.eof
							If blnChange = true then%>
							<tr class="rowColor">
							<%else%>
							<tr>
							<%end if%>
								<td valign="top">
									<br />&nbsp;<strong><%=i%>.</strong>&nbsp;
								</td>
								<td valign="top">
									<br /><%=rsQuestion("question")%>
									<input type="hidden" name="genquestion<%=i%>" value="<%=rsQuestion("question")%>" /><br />
								</td>
							</tr>
					
					<%rsQuestion.movenext
					i=i+1
					if blnChange = true then
						blnChange = false
					else
						blnChange = true
					end if
					loop%>
					
					
				</table>
				
			</td>
		</tr>
		<tr><td colspan="7" height=5></td></tr>
		<tr>
			<td></td>
			<td colspan="7">&nbsp;&nbsp;
				<input type="hidden" id="idGen" value="<%=i%>">
				<div id="divTxtGen"></div>
			</td>
		</tr>
		<tr><td colspan="7" height=5></td></tr>	
		<tr>
			<td></td>
			<td colspan="7">&nbsp;&nbsp;
				<!--add a new entry to this-->
				<a href="#" onClick="addFormFieldGen(); return false;">+ add new entry</a>
			</td>
		</tr>		
	</table><br /><br />
<%end if%>	
	



<style>
	#myformCust label {
	  font-family: Arial, Helvetica, sans-serif;
	  font-size:11px;
	}	
	#quesListCust {
	  margin: 0px 0px 0px 0px;
	  display: none;
	}
</style>	
<div id="myformCust">
<strong class="subHeader">Client/General Contractor Inspection Information</strong><br /><br />	
<strong><%=rsCustomer("customerName")%></strong>&nbsp;&nbsp;
<input type="radio" name="showQuestionsCust" onclick="javascript: $('#quesListCust').show('fast');" value="show" /> show&nbsp;&nbsp;<input checked="checked" type="radio" name="showQuestionsCust" value="hide" onclick="javascript: $('#quesListCust').hide('fast');" /> hide<br>
<div id="quesListCust">		
<%If iDivisionID <> "" and iProjectID <> "" then%>	
<!--add the OSHA questions for the current customer and any of it's sub-contractors-->
<table cellpadding="0" cellspacing="0" border="0">	
	<tr>
		<td><img src="images/pix.gif" width="5" height="0"></td>
		<td>
			<%
				i = 1
				Set oCmd = Server.CreateObject("ADODB.Command")

				With oCmd
				   .ActiveConnection = DataConn
				   .CommandText = "spGetOSHAQuestion"
				   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iDivisionID)
				   .CommandType = adCmdStoredProc
				   
				End With
							
				Set rsQuestion = oCmd.Execute
				Set oCmd = nothing
				%>
				
				<br><br>
				<table width="800" cellpadding="0" cellspacing="0" border="0">
					<%blnChange = true
					do until rsQuestion.eof
						If blnChange = true then%>
						<tr class="rowColor">
						<%else%>
						<tr>
						<%end if%>
							<td valign="top">
								<br />&nbsp;<strong><%=i%>.</strong>&nbsp;
							</td>
							<td valign="top" align="left">
								<br /><%=rsQuestion("question")%><br />
								<input type="hidden" name="customer_question_question<%=rsQuestion("questionID")%>" value="<%=rsQuestion("question")%>" />
								
								<input type="radio" name="customer_question_answer<%=rsQuestion("questionID")%>" value="1" />&nbsp;<%=rsQuestion("YesAnswer")%><br />
								<input type="radio" name="customer_question_answer<%=rsQuestion("questionID")%>" value="2" />&nbsp;<%=rsQuestion("NoAnswer")%><br />
								<input type="radio" name="customer_question_answer<%=rsQuestion("questionID")%>" value="3"  checked="checked" />&nbsp;<%=rsQuestion("NAAnswer")%><br />
								<input type="radio" name="customer_question_answer<%=rsQuestion("questionID")%>" value="4"/>&nbsp;<%=rsQuestion("NotInspAnswer")%><br />
								
								<a href=javascript:displayCommentPicker('customer_question_comment<%=rsQuestion("questionID")%>')><strong>select comment</strong></a><br />
								<input type="text" name="customer_question_comment<%=rsQuestion("questionID")%>" size="70" />
								<br /><br />
							</td>
						</tr>
				
				<%rsQuestion.movenext
				i=i+1
				if blnChange = true then
					blnChange = false
				else
					blnChange = true
				end if
				loop%>
				
				
			</table>
			
		</td>
	</tr>
	<tr><td colspan="7" height=5></td></tr>
	<tr>
		<td></td>
		<td colspan="7">&nbsp;&nbsp;
			<input type="hidden" id="idCust" value="<%=i%>">
			<div id="divTxtCust"></div>
		</td>
	</tr>	
	<tr><td colspan="7" height=5></td></tr>
	<tr>
		<td></td>
		<td colspan="7">&nbsp;&nbsp;
			<!--add a new entry to this-->
			<a href="#" onClick="addFormFieldCust(); return false;">+ add new entry</a>
		</td>
	</tr>		
</table><br />
</div>
</div>
<%end if%>

<br />
The main issues found during this inspection that may have resulted in OSHA citations are:<br />
<strong>NOTE:</strong> To insert single carraige return, hold the "shift" key while pressing enter.
<script language="JavaScript" type="text/javascript" src="cbrte/html2xhtml.js"></script>
<script language="JavaScript" type="text/javascript" src="cbrte/richtext_compressed.js"></script>
<script language="JavaScript" type="text/javascript">
<!--
function submitRTEForm() {
	updateRTEs();
	return true;
}
initRTE("cbrte/images/", "cbrte/", "", true);
//-->
</script>
<script language="JavaScript" type="text/javascript">
<!--
var rte1 = new richTextEditor('rte1');
//rte1.html = '<%'=sContent%>';
rte1.toggleSrc = false;
rte1.toolbar1 = false;
rte1.build();
//-->
</script>
<br /><br />

<strong>NOTE: </strong>Once the report has been saved, you will have the opportunity to add sub-contractors.<br /><br />

<%'get the users in the system
Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetUserList"
   .CommandType = adCmdStoredProc   
End With
Set rsUsers = oCmd.Execute
Set oCmd = nothing
'add the users to a dropdown for selecting
%>

<script type="text/javascript" src="jquery/jquery.ui.js"></script>
<script type="text/javascript" src="jquery/jquery.asmselect.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$("select[multiple]").asmSelect({
			addItemTarget: 'bottom',
			animate: true,
			highlight: true,
			sortable: true
		});
		
	}); 

</script>
<link rel="stylesheet" type="text/css" href="jquery/jquery.asmselect.css" />
Please select any user(s) that you want this report routed to in order to pre-certify once the report is saved.<br />
<strong>NOTE:</strong>You can also route this report when a sub-contractor is added or edited.<br />															
<select id="routeUsers" multiple="multiple" name="routeUsers" title="select all that apply">
	<%do until rsUsers.eof%>
		<option value="<%=rsUsers("userID")%>"><%=rsUsers("firstName")%>&nbsp;<%=rsUsers("lastName")%></option>
	<%rsUsers.movenext
	loop%>
</select>

