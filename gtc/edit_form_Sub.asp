<%
On Error Resume Next

subID = request("id")
customerID = request("customerID")

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

'Create command for state list
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetStates"
   .CommandType = adCmdStoredProc
   
End With
	
Set rsState = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetSub"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, subID)
   .CommandType = adCmdStoredProc
   
End With
	
Set rsSub = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetSubTypes"
   .CommandType = adCmdStoredProc   
End With
	
Set rsSubTypes = oCmd.Execute
Set oCmd = nothing

%>
<form name="editSub" method="post" action="process.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Add Sub-Contractor</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td valign="top"><strong>Sub-Contractor Name:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="subContractorName" size="30" value="<%=rsSub("subContractorName")%>"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top"><strong>Sub-Contractor Type:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="subContractorType">
													<%do until rsSubTypes.eof
														if trim(rsSubTypes("subType")) = trim(rsSub("subContractorType")) then%>
															<option selected="selected" value="<%=rsSubTypes("subType")%>"><%=rsSubTypes("subType")%></option>
														<%else%>
															<option value="<%=rsSubTypes("subType")%>"><%=rsSubTypes("subType")%></option>
													<%end if
													rsSubTypes.movenext
													loop%>
												</select>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top"><strong>Contact First Name:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="contactFirstName" size="30" value="<%=rsSub("contactFirstName")%>"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top"><strong>Contact Last Name:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="contactLastName" size="30" value="<%=rsSub("contactLastName")%>"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top"><strong>Contact Email:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="contactEmail" size="30" value="<%=rsSub("contactEmail")%>"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>				
										<tr>
											<td valign="top"><strong>Address 1:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="address1" size="30" value="<%=rsSub("address1")%>"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top"><strong>Address 2:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="address2" size="30" value="<%=rsSub("address2")%>" />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top"><strong>City:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="city" size="30" value="<%=rsSub("city")%>"/>
											</td>
										</tr>				
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top"><strong>State:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="state" tooltipText="Select the customer's state.">
													<%do until rsState.eof
														if rsSub("state") = rsState("stateID") then%>
															<option selected="selected" value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
														<%else%>
															<option value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
														<%end if
													rsState.movenext
													loop%>
												</select>&nbsp;&nbsp;<strong>Zip:</strong>&nbsp;<input type="text" name="zip" size="5" value="<%=rsSub("zip")%>" />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top"><strong>Phone:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="phone" size="30" value="<%=rsSub("phone")%>"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top"><strong>Fax:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="fax" size="30" value="<%=rsSub("fax")%>" />
											</td>
										</tr>			
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td colspan="3" align="right">
												<input type="hidden" name="customerID" value="<%=customerID%>" />
												<input type="hidden" name="subContractorID" value="<%=subID%>" />
												<input type="hidden" name="processType" value="editSub" />
												<input type="submit" value="  Save  " class="formButton"/>
											</td>
										</tr>
									</table>
								
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<script language="JavaScript" type="text/javascript">

  var frmvalidator  = new Validator("editSub");
  frmvalidator.addValidation("subContractorName","req","Please enter the Sub-Contractor's name");
 // frmvalidator.addValidation("contactName","req","Please enter the contact's name");
 // frmvalidator.addValidation("contactEmail","req","Please enter the cantact's email address");
  //frmvalidator.addValidation("contactEmail","email");
 // frmvalidator.addValidation("address1","req","Please enter the customer's address");
 // frmvalidator.addValidation("city","req","Please enter the customer's city");
 // frmvalidator.addValidation("zip","req","Please enter the customer's zip code");
 // frmvalidator.addValidation("phone","req","Please enter the customer's phone number");
</script>

<%
rsState.close
set rsState = nothing
DataConn.close
set DataConn = nothing
%>