<%
dim iPrimaryID, rsPrimary

projectID = request("projectID")
iPrimaryID = request("id")

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetPrimaryNOI"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iPrimaryID)
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsPrimary = oCmd.Execute
Set oCmd = nothing	


'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCoverage"
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rs = oCmd.Execute
Set oCmd = nothing

'Create command for state list
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetStates"
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsState = oCmd.Execute
Set oCmd = nothing

'Create command for county list
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCounties"
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsCounty = oCmd.Execute
Set oCmd = nothing

'Create command for NTU Value list
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetNTUValue"
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsNTUValue = oCmd.Execute
Set oCmd = nothing

'Create command for secondary permittee list
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetNumSecPermittees"
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsSP = oCmd.Execute
Set oCmd = nothing
%>

<script language="javascript" type="text/javascript">
<!--
function docheck(){
  if (document.noiPrimaryEdit.estimatedDisturbedAcerage.value > 49)  
  	document.noiPrimaryEdit.locationMap.checked=true;
  if (document.noiPrimaryEdit.estimatedDisturbedAcerage.value < 50)  
  	document.noiPrimaryEdit.locationMap.checked=false;
  if (document.noiPrimaryEdit.estimatedDisturbedAcerage.value > 49) 
	document.noiPrimaryEdit.ESPControlPlan.checked=true;
  if (document.noiPrimaryEdit.estimatedDisturbedAcerage.value < 50) 
	document.noiPrimaryEdit.ESPControlPlan.checked=false;
  if (document.noiPrimaryEdit.estimatedDisturbedAcerage.value > 49) 
	document.noiPrimaryEdit.timingSchedule.checked=true;
  if (document.noiPrimaryEdit.estimatedDisturbedAcerage.value < 50) 
	document.noiPrimaryEdit.timingSchedule.checked=false;
}

function KeepCount() {

	var NewCount = 0
						
	if (document.noiPrimaryEdit.typeConstructionCommercial.checked)
	{NewCount = NewCount + 1}
	
	if (document.noiPrimaryEdit.typeConstructionIndustrial.checked)
	{NewCount = NewCount + 1}
	
	if (document.noiPrimaryEdit.typeConstructionMunicipal.checked)
	{NewCount = NewCount + 1}
	
	if (document.noiPrimaryEdit.typeConstructionLinear.checked)
	{NewCount = NewCount + 1}
	
	if (document.noiPrimaryEdit.typeConstructionUtility.checked)
	{NewCount = NewCount + 1}
	
	if (document.noiPrimaryEdit.typeConstructionResidential.checked)
	{NewCount = NewCount + 1}
	
	if (NewCount == 2)
	{
	alert('Pick Just One Please')
	document.noiPrimaryEdit; return false;
	}
} 

function KeepCount2() {

	var NewCount = 0
						
	if (document.noiPrimaryEdit.IRWTrout.checked)
	{NewCount = NewCount + 1}
	
	if (document.noiPrimaryEdit.IRWWarmWater.checked)
	{NewCount = NewCount + 1}
	
	if (NewCount == 2)
	{
	alert('Pick Just One Please')
	document.noiPrimaryEdit; return false;
	}
} 

function KeepCount3() {

	var NewCount = 0
						
	if (document.noiPrimaryEdit.RWTrout.checked)
	{NewCount = NewCount + 1}
	
	if (document.noiPrimaryEdit.RWWarmWater.checked)
	{NewCount = NewCount + 1}
	
	if (NewCount == 2)
	{
	alert('Pick Just One Please')
	document.noiPrimaryEdit; return false;
	}
} 

function KeepCount4() {
  
	var NewCount = 0
						
	if (document.noiPrimaryEdit.samplingOfOutfall.checked)
	{NewCount = NewCount + 1}
	
	if (document.noiPrimaryEdit.samplingOfRecievingStream.checked)
	{NewCount = NewCount + 1}
	
	if (document.noiPrimaryEdit.troutStream.checked)
	{NewCount = NewCount + 1}
	
	if (NewCount == 2)
	{
	alert('Pick Just One Please')
	document.noiPrimaryEdit; return false;
	}
} 
//-->
</script>

<form name="noiPrimaryEdit" method="post" action="processNOI.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sNOITitle%></span><span class="Header"> - Edit Notice of Intent</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td>
												<strong>State of Georgia</strong><br />
													<strong>Department of Natural Resources</strong><br />
													<strong>Environmental Protection Division</strong><br /><br />

													<a href="instructions_primary.asp" target="_blank">Instructions</a><br><br />
													<strong>For Coverage Under the 2008 Re-Issuance of the NPDES General Permits </strong><br />
													<strong>To Discharge Storm Water Associated With Construction Activity</strong><br><br>
													<strong>PRIMARY PERMITTEE</strong>
											</td>
											<td></td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="20"></td></tr>
										<tr>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td>
												<table border="0" cellpadding="0" cellspacing="0">
													<tr>
														<td valign="top"><strong>Notice Of Intent:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="radio" name="noticeOfIntent" value="1" <%=isCheckedRadio(1,trim(rsPrimary("noticeOfIntent")))%>/>&nbsp;Initial Notification (New Facility/Construction Site)<br />
															<input type="radio" name="noticeOfIntent" value="2" <%=isCheckedRadio(2,trim(rsPrimary("noticeOfIntent")))%>/>&nbsp;Re-Issuance Notification (Existing Facility/Construction Site)<br />
															<input type="radio" name="noticeOfIntent" value="3" <%=isCheckedRadio(3,trim(rsPrimary("noticeOfIntent")))%>/>&nbsp;Change of Information (Applicable only if the NOI was submitted after August 1, 2008)
														</td>
													</tr>												
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="20"></td></tr>
													<tr>
														<td><strong>Coverage Desired:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<select name="coverageDesired">
																<%do until rs.eof%>
																	<%if rsPrimary("coverageDesired") = rs("coverageDesiredID") then%>
																		<option selected="selected" value="<%=rs("coverageDesiredID")%>"><%=rs("coverageDesiredName")%></option>
																	<%else%>
																		<option value="<%=rs("coverageDesiredID")%>"><%=rs("coverageDesiredName")%></option>
																	<%end if%>
																<%rs.movenext
																loop%>
															</select>
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
													<tr>
														<td colspan="3"><strong>I. SITE/OWNER/OPERATOR INFORMATION</strong></td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td><strong>Project Construction Site Name:</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=siteProjectName','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a><br /><img src="images/pix.gif" width="180" height="1"></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="siteProjectName" size="30" value="<%=rsPrimary("siteProjectName")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td valign="top"><strong>GPS Location of Const. Exit:</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=GPSLocation','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<table>
																<tr>
																	<td>
																		Degrees<br /><span class="footer">From 0 to 90</span><br />
																		<input type="text" name="degree1" size="5" maxlength="2" value="<%=rsPrimary("GPSDegree1")%>">
																	</td>
																	<td><img src="images/pix.gif" width="10" height="1"></td>
																	<td>
																		Minutes / Seconds<br /><span class="footer">From 0 to 59</span><br />
																		<input type="text" name="minute1" size="5" maxlength="2" value="<%=rsPrimary("GPSMinute1")%>"> <input type="text" name="second1" maxlength="5" size="5" value="<%=rsPrimary("GPSSecond1")%>"> 
																		<input type="hidden" name="latitude" value="<%=rsPrimary("GPSLat")%>"> North Latitude
																	</td>
																</tr>
																<tr>
																	<td>
																		Degrees<br /><span class="footer">From 0 to 90</span><br />
																		<input type="text" name="degree2" size="5" maxlength="2" value="<%=rsPrimary("GPSDegree2")%>">
																	</td>
																	<td><img src="images/pix.gif" width="10" height="1"></td>
																	<td>
																		Minutes / Seconds<br /><span class="footer">From 0 to 59</span><br />
																		<input type="text" name="minute2" size="5" maxlength="2" value="<%=rsPrimary("GPSMinute2")%>"> <input type="text" name="second2" maxlength="5" size="5" value="<%=rsPrimary("GPSSecond2")%>"> 
																		<input type="hidden" name="longitude" value="<%=sPrimary("GPSLon")%>"> West Longitude
																	</td>
																</tr>
															</table>
															 
															<!-- <input type="text" name="GPSLocationNorth" size="30"> <strong>W</strong> <input type="text" name="GPSLocationWest" size="30">-->
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Construction Site Street Address:</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=projectAddress','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="projectAddress" size="30" value="<%=rsPrimary("projectAddress")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>City(if applicable):</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=projectCity','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="projectCity" size="30" value="<%=rsPrimary("projectCity")%>">&nbsp;&nbsp;
															<strong>County:</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=projectCounty','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a>&nbsp;&nbsp;<select name="projectCounty">
																<%do until rsCounty.eof%>
																	<%if rsCounty("county") = rsPrimary("projectCounty") then%>
																		<option selected="selected" value="<%=rsCounty("county")%>"><%=rsCounty("county")%></option>
																	<%else%>
																		<option value="<%=rsCounty("county")%>"><%=rsCounty("county")%></option>
																	<%end if%>
																<%rsCounty.movenext
																loop%>
															</select>
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Common Development Name:</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=subdivision','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="subdivision" size="30" value="<%=rsPrimary("subdivision")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Owner�s Name:</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=ownerName','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="ownerName" size="30" value="<%=rsPrimary("ownerName")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Address:</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=ownerAddress','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="ownerAddress" size="30" value="<%=rsPrimary("ownerAddress")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>City:</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=ownerCity','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="ownerCity" size="20" value="<%=rsPrimary("ownerCity")%>">&nbsp;&nbsp;<strong>State:</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=ownerState','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a>&nbsp;&nbsp;
															<select name="ownerState">
																<%do until rsState.eof%>
																	<%if rsPrimary("ownerState") = rsState("stateID") then%>
																		<option selected="selected" value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
																	<%else%>
																		<option value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
																	<%end if%>
																<%rsState.movenext
																loop
																rsState.moveFirst%>
															</select>&nbsp;&nbsp;
															<strong>Zip:</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=ownerZip','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a>&nbsp;&nbsp;<input type="text" name="ownerZip" size="10" value="<%=rsPrimary("ownerZip")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td><strong>Duly Authorized Representative:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<input type="text" name="authorizedRep" size="30" value="<%=rsPrimary("authorizedRep")%>">&nbsp;&nbsp;<strong>Phone:</strong>&nbsp;<input type="text" name="authorizedRepPhone" size="30" value="<%=rsPrimary("authorizedRepPhone")%>">
															</td>
														</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Operator�s Name:</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=operatorName','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="operatorName" size="30" value="<%=rsPrimary("operatorName")%>">&nbsp;&nbsp;<strong>Phone:</strong>&nbsp;&nbsp;<input type="text" name="operatorPhone" size="30" value="<%=rsPrimary("operatorPhone")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Address:</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=operatorAddress','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="operatorAddress" size="30" value="<%=rsPrimary("operatorAddress")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>City:</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=operatorCity','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="operatorCity" size="20" value="<%=rsPrimary("operatorCity")%>">&nbsp;&nbsp;<strong>State:</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=operatorState','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a>&nbsp;&nbsp;
															<select name="operatorState">
																<%do until rsState.eof%>
																	<%if rsPrimary("operatorState") = rsState("stateID") then%>
																		<option selected="selected" value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
																	<%else%>
																		<option value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
																	<%end if%>
																<%rsState.movenext
																loop%>
															</select>&nbsp;&nbsp;
															<strong>Zip:</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=operatorZip','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a>&nbsp;&nbsp;<input type="text" name="operatorZip" size="10" value="<%=rsPrimary("operatorZip")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Facility Contact:</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=facilityContact','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="facilityContact" size="30" value="<%=rsPrimary("facilityContact")%>">&nbsp;&nbsp;<strong>Phone:</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=facilityContactPhone','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a>&nbsp;&nbsp;<input type="text" name="facilityContactPhone" size="30" value="<%=rsPrimary("facilityContactPhone")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
													<tr>
														<td colspan="3"><strong>II. SITE ACTIVITY INFORMATION</strong></td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td><strong>Start Date:</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=startDate','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="startDate" size="10" value="<%=rsPrimary("startDate")%>">&nbsp;<a href="javascript:displayDatePicker('startDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>&nbsp;&nbsp;<strong>Completion Date:</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=completionDate','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a>&nbsp;&nbsp;<input type="text" name="completionDate" size="10" value="<%=rsPrimary("completionDate")%>">&nbsp;<a href="javascript:displayDatePicker('completionDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Estimated Disturbed Acreage:</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=estimatedDisturbedAcerage','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="estimatedDisturbedAcerage" size="5" onchange="docheck()" value="<%=rsPrimary("estimatedDisturbedAcerage")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td colspan="3" valign="top">
															<strong>Does the Erosion, Sedimentation and Pollution Control Plan (Plan) provide for disturbing more than 50 acres at any one time for each individual permittee (i.e., primary, secondary or tertiary permittees), or more than 50 contiguous acres total at any one time ?</strong>
														</td>
													</tr>
													<tr>
														<td valign="top" colspan="3">
															<input type="radio" name="disturb50" value="1" <%=isCheckedRadio(1,trim(rsPrimary("disturb50")))%> />&nbsp;YES<br />
															<input type="radio" name="disturb50" value="2" <%=isCheckedRadio(2,trim(rsPrimary("disturb50")))%> />&nbsp;NO<br />
															<input type="radio" name="disturb50" value="3" <%=isCheckedRadio(3,trim(rsPrimary("disturb50")))%> />&nbsp;N/A  - if the Plan was submitted prior to the effective date of the General NPDES Permit No. GAR100001 and No. GAR100003 for Stand Alone and Common <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Development construction activities.<br />
															<input type="radio" name="disturb50" value="4" <%=isCheckedRadio(4,trim(rsPrimary("disturb50")))%> />&nbsp;N/A � if construction activities are covered under the General NPDES Permit No. GAR100002 for Infrastructure construction projects.
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td valign="top"><strong>Type Construction Activity:</strong><br />(please choose only one)</td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="checkbox" name="typeConstructionCommercial" <%=isChecked(rsPrimary("typeConstructionCommercial"))%> onClick="return KeepCount()" />&nbsp;Commercial&nbsp;&nbsp;
															<input type="checkbox" name="typeConstructionIndustrial" <%=isChecked(rsPrimary("typeConstructionIndustrial"))%> onClick="return KeepCount()"/>&nbsp;Industrial&nbsp;&nbsp;
															<input type="checkbox" name="typeConstructionMunicipal" <%=isChecked(rsPrimary("typeConstructionMunicipal"))%> onClick="return KeepCount()"/>&nbsp;Municipal&nbsp;&nbsp;
															<input type="checkbox" name="typeConstructionLinear" <%=isChecked(rsPrimary("typeConstructionLinear"))%> onClick="return KeepCount()"/>&nbsp;Linear<br />
															<input type="checkbox" name="typeConstructionUtility" <%=isChecked(rsPrimary("typeConstructionUtility"))%> onClick="return KeepCount()"/>&nbsp;Utility&nbsp;&nbsp;
															<input type="checkbox" name="typeConstructionResidential" <%=isChecked(rsPrimary("typeConstructionResidential"))%> onClick="return KeepCount()"/>&nbsp;Residential/Subdivision Development
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Number of Secondary Permittees:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<select name="secondaryPermittees">
																<%do until rsSP.eof
																if rsPrimary("secondaryPermittees") = rsSP("secondaryPermittees") then%>
																	<option selected="selected" value="<%=rsSP("secondaryPermittees")%>"><%=rsSP("secondaryPermittees")%></option>							
																<%else%>
																	<option value="<%=rsSP("secondaryPermittees")%>"><%=rsSP("secondaryPermittees")%></option>
																<%end if%>
																<%rsSP.movenext
																loop%>
															</select>
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
													<tr>
														<td colspan="3"><strong>III. RECEIVING WATER INFORMATION</strong></td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td><strong>A. Initial Receiving Water(s):</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=IRWName','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="IRWName" size="30" value="<%=rsPrimary("IRWName")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td valign="top"></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="checkbox" name="IRWTrout" <%=isChecked(rsPrimary("IRWTrout"))%> onClick="return KeepCount2()" />&nbsp;Trout Stream&nbsp;&nbsp;<input type="checkbox" name="IRWWarmWater" <%=isChecked(rsPrimary("IRWWarmWater"))%> onClick="return KeepCount2()" />&nbsp;Warm Water Fisheries Stream
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>B. Municipal Storm Sewer<br />&nbsp;&nbsp;&nbsp; System Owner/Operator:</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=MSSSOwnerOperator','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="MSSSOwnerOperator" size="30" value="<%=rsPrimary("MSSSOwnerOperator")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Name of Receiving Water(s):</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=RWName','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="RWName" size="30" value="<%=rsPrimary("RWName")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td valign="top"></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="checkbox" name="RWTrout" <%=isChecked(rsPrimary("RWTrout"))%> onClick="return KeepCount3()" />&nbsp;Trout Stream&nbsp;&nbsp;<input type="checkbox" name="RWWarmWater"  <%=isChecked(rsPrimary("RWWarmWater"))%> onClick="return KeepCount3()"/>&nbsp;Warm Water Fisheries Stream
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td colspan="3">
															<strong>C.</strong>&nbsp;<input type="checkbox" name="samplingOfRecievingStream" onClick="return KeepCount4()" <%=isChecked(rsPrimary("samplingOfRecievingStream"))%> />&nbsp;Sampling of Receiving Stream(s)&nbsp;&nbsp;<input type="checkbox" name="troutStreamRecieve" onClick="return KeepCount4()" <%=isChecked(rsPrimary("troutStreamRecieve"))%>/>&nbsp;Trout Stream&nbsp;&nbsp;<input type="checkbox" name="warmWaterRecieve" onClick="return KeepCount4()" <%=isChecked(rsPrimary("warmWaterRecieve"))%>/>&nbsp;Warm Water Fisheries Stream
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td colspan="3">
															<strong>D.</strong>&nbsp;<input type="checkbox" name="samplingOfOutfall" onClick="return KeepCount5()" <%=isChecked(rsPrimary("samplingOfOutfall"))%>/>&nbsp;Sampling of Outfall(s)&nbsp;&nbsp;<input type="checkbox" name="troutStream" onClick="return KeepCount5()" <%=isChecked(rsPrimary("troutStream"))%>/>&nbsp;Trout Stream&nbsp;&nbsp;<input type="checkbox" name="warmWater" onClick="return KeepCount5()" <%=isChecked(rsPrimary("warmWater"))%>/>&nbsp;Warm Water Fisheries Stream
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td colspan="3">
															&nbsp;&nbsp;<strong>Number of Outfalls:</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=numberOfOutfalls','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a> <input type="text" name="numberOfOutfalls" size="5"  value="<%=rsPrimary("numberOfOutfalls")%>"/>
															&nbsp;&nbsp;
															<strong>Construction Site Size (acres):</strong>&nbsp;<input type="text" name="constructionSiteSize" size="5" value="<%=rsPrimary("constructionSiteSize")%>" />
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td colspan="3">
															<strong>Appendix B NTU Value:</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=appendixBNTUValue','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a> <select name="appendixBNTUValue">
																<%do until rsNTUValue.eof%>
																	<%if rsNTUValue("ntuValue") = rsPrimary("appendixBNTUValue") then%>
																		<option selected="selected" value="<%=rsNTUValue("ntuValue")%>"><%=rsNTUValue("ntuValue")%></option>
																	<%else%>
																		<option value="<%=rsNTUValue("ntuValue")%>"><%=rsNTUValue("ntuValue")%></option>
																	<%end if%>
																<%rsNTUValue.movenext
																loop%>
															</select>
															&nbsp;&nbsp;<strong>Surface Water Drainage Area:</strong>&nbsp;<a href="#" onClick="MM_openBrWindow('help.asp?topic=SWDrainageArea','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/help.gif" width="14" height="14" border="0"></a> <input type="text" name="SWDrainageArea" size="5" value="<%=rsPrimary("SWDrainageArea")%>"/>
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td colspan="3" valign="top">
															<strong>Does the facility/construction site discharge storm water into an Impaired Stream Segment, or within one (1) linear mile upstream of and within the same watershed as, any portion of an Impaired Stream Segment identified as �not supporting� its designated use(s), as shown on Georgia�s 2008 and subsequent �305(b)/303(d) List Documents (Final)� listed for the criteria violated, �Bio F� (Impaired Fish Community) and/or �Bio M� (Impaired Macroinvertebrate Community), within Category 4a, 4b or 5, and the potential cause is either �NP� (nonpoint source) or �UR� (urban runoff)?</strong>
														</td>
													</tr>
													<tr>
														<td valign="top" colspan="3">
															<input type="radio" name="siteDischargeStormWater" value="1" <%=isCheckedRadio(1,trim(rsPrimary("siteDischargeStormWater")))%>/>&nbsp;YES, Name of Impaired Stream Segment(s):&nbsp;&nbsp;<input type="text" name="impairedStream"  value="<%=rsPrimary("impairedStream")%>"/><br />
															<input type="radio" name="siteDischargeStormWater" value="2" <%=isCheckedRadio(2,trim(rsPrimary("siteDischargeStormWater")))%>/>&nbsp;NO<br />
															<input type="radio" name="siteDischargeStormWater" value="3" <%=isCheckedRadio(3,trim(rsPrimary("siteDischargeStormWater")))%>/>&nbsp;N/A � if the NOI was submitted within 90 days after the effective date of the General NPDES Permit No. GAR100001 and No. GAR100003 for Stand Alone and <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Common Development construction activities.<br />
															<input type="radio" name="siteDischargeStormWater" value="4" <%=isCheckedRadio(4,trim(rsPrimary("siteDischargeStormWater")))%>/>&nbsp;N/A � if the NOI was submitted prior to January 1, 2009 for the General NPDES Permit No. GAR100002 for Infrastructure construction activities.
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td colspan="3" valign="top">
															<strong>Does the facility/construction site discharge storm water into an Impaired Stream Segment where a Total Maximum Daily Load (TMDL) Implementation Plan for �sediment� was finalized at least six (6) months prior to the submittal of the NOI ?</strong>
														</td>
													</tr>
													<tr>
														<td valign="top" colspan="3">
															<input type="radio" name="siteDischargeStormWater2" value="1" <%=isCheckedRadio(1,trim(rsPrimary("siteDischargeStormWater2")))%>/>&nbsp;YES, Name of Impaired Stream Segment(s):&nbsp;&nbsp;<input type="text" name="impairedStream2"  value="<%=rsPrimary("impairedStream2")%>" /><br />
															<input type="radio" name="siteDischargeStormWater2" value="2" <%=isCheckedRadio(2,trim(rsPrimary("siteDischargeStormWater2")))%>/>&nbsp;NO<br />
															<input type="radio" name="siteDischargeStormWater2" value="3" <%=isCheckedRadio(3,trim(rsPrimary("siteDischargeStormWater2")))%>/>&nbsp;N/A � if the NOI was submitted within 90 days after the effective date of the General NPDES Permit No. GAR100001 and No. GAR100003 for Stand Alone and <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Common Development construction activities.<br />
															<input type="radio" name="siteDischargeStormWater2" value="4" <%=isCheckedRadio(4,trim(rsPrimary("siteDischargeStormWater2")))%>/>&nbsp;N/A � if the NOI was submitted prior to January 1, 2009 for the General NPDES Permit No. GAR100002 for Infrastructure construction activities.
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
													<tr>
														<td colspan="3"><strong>IV. ATTACHMENTS. (Check those that apply.)</strong></td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td colspan="3">
															<input type="checkbox" name="locationMap" <%=isChecked(rsPrimary("locationMap"))%>/>&nbsp;Location map showing the receiving stream(s), outfall(s) or combination thereof to be monitored.
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td colspan="3">
															<input type="checkbox" name="ESPControlPlan" <%=isChecked(rsPrimary("ESPControlPlan"))%>/>&nbsp;Erosion, Sedimentation and Pollution Control Plan (if project is greater than 50 acres or if project in areas <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;without local Issuing Authorities regardless of acreage).
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td colspan="3">
															<input type="checkbox" name="writtenAuth" <%=isChecked(rsPrimary("writtenAuth"))%>/>&nbsp;Written authorization from the appropriate EPD District Office if the Plan disturbs more than 50 acres at any one time for each individual permittee (i.e., <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;primary, secondary or tertiary permittees), or more  than 50 contiguous acres total at any one time (applicable only to General NPDES Permits No. <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GAR100001 and No. GAR100003).

														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td colspan="3">
															<input type="checkbox" name="knownSecondaryPermittees" <%=isChecked(rsPrimary("knownSecondaryPermittees"))%>/>&nbsp;List of known secondary permittees.
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td colspan="3">
															<input type="checkbox" name="timingSchedule" <%=isChecked(rsPrimary("timingSchedule"))%>/>&nbsp;Schedule for the timing of the major construction activities.
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
													<tr>
														<td colspan="3"><strong>III. CERTIFICATIONS. (Owner or Operator or both to initial as applicable.)</strong></td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td colspan="3">
															<input type="text" name="certify1" size="5" value="<%=rsPrimary("certify1")%>">&nbsp;I certify that the receiving water(s) or the outfall(s) or a combination of receiving water(s) and
															outfall(s) will be monitored in accordance with the Erosion, Sedimentation and Pollution Control Plan.
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td colspan="3">
															<input type="text" name="certify2" size="5" value="<%=rsPrimary("certify2")%>">&nbsp;I certify that the Erosion, Sedimentation, and Pollution Control Plan (Plan) has been prepared in
															accordance with Part IV of the General NPDES Permit GAR100001, GAR 100002 or GAR 100003, the Plan will
															be implemented, and that such Plan will provide for compliance with this permit.
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td colspan="3">
															<input type="text" name="certify3" size="5" value="<%=rsPrimary("certify3")%>">&nbsp;I certify under penalty of law that this document and all attachments were prepared under my direction
															or supervision in accordance with a system designed to assure that qualified personnel properly gather and
															evaluate the information submitted. Based upon my inquiry of the person or persons who manage the system, or
															those persons directly responsible for gathering the information, the information submitted is, to the best of my
															knowledge and belief, true, accurate, and complete. I am aware that there are significant penalties for submitting
															false information, including the possibility of fine and imprisonment for knowing violations.
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
												</table>
												<table>
													<tr>
														<td valign="top"><strong>Owner�s Printed Name:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td><input type="text" name="ownerPrintedName" size="30" value="<%=rsPrimary("ownerPrintedName")%>">&nbsp;&nbsp;&nbsp;&nbsp;<strong>Title:</strong>&nbsp;&nbsp;<input type="text" name="ownerTitle" size="30" value="<%=rsPrimary("ownerTitle")%>"></td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td valign="top" align="right"><strong>Date:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td colspan="3"><input type="text" name="ownerSignDate" size="10" value="<%=rsPrimary("ownerSignDate")%>">&nbsp;<a href="javascript:displayDatePicker('ownerSignDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a></td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td valign="top"><strong>Operator�s Printed Name:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td><input type="text" name="operatorPrintedName" size="30" value="<%=rsPrimary("operatorPrintedName")%>">&nbsp;&nbsp;&nbsp;&nbsp;<strong>Title:</strong>&nbsp;&nbsp;<input type="text" name="operatorTitle" size="30" value="<%=rsPrimary("operatorTitle")%>"></td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td valign="top" align="right"><strong>Date:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td colspan="3"><input type="text" name="operatorSignDate" size="10" value="<%=rsPrimary("operatorSignDate")%>">&nbsp;<a href="javascript:displayDatePicker('operatorSignDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a></td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
													<tr>
														<td></td>
														<td colspan="2" align="right">
															<input type="hidden" name="projectID" value="<%=projectID%>" />
															<input type="hidden" name="primaryFormID" value="<%=iPrimaryID%>" />
															<input type="hidden" name="processType" value="editPrimary" />
															<input type="submit" value="Update Data" class="formButton"/>
														</td>
													</tr>
												</table>
											</td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
										</tr>
									</table>
								
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<script language="JavaScript" type="text/javascript">

  var frmvalidator  = new Validator("noiPrimaryEdit");
//  frmvalidator.addValidation("siteProjectName","req","Please enter the site project name");
//  frmvalidator.addValidation("degree1","req","Please enter the degrees for latitude");
//  frmvalidator.addValidation("degree1","lessthan=91");
//  frmvalidator.addValidation("minute1","req","Please enter the minutes for latitude");
//  frmvalidator.addValidation("minute1","lessthan=60");
//  frmvalidator.addValidation("second1","req","Please enter the seconds for latitude");
//  frmvalidator.addValidation("second1","lessthan=60");
//  frmvalidator.addValidation("degree2","req","Please enter the degrees for longitude");
//  frmvalidator.addValidation("degree2","lessthan=91");
//  frmvalidator.addValidation("minute2","req","Please enter the minutes for longitude");
//  frmvalidator.addValidation("minute2","lessthan=60");
//  frmvalidator.addValidation("second2","req","Please enter the seconds for longitude");
//  frmvalidator.addValidation("second2","lessthan=60");
//  frmvalidator.addValidation("projectAddress","req","Please enter the street address of the Project");
//  frmvalidator.addValidation("projectCity","req","Please enter the city where the project is located");
//  frmvalidator.addValidation("subdivision","req","Please enter the name of the subdivision");
//  frmvalidator.addValidation("ownerName","req","Please enter the name of the owner");
//  frmvalidator.addValidation("ownerAddress","req","Please enter the street address of the owner");
//  frmvalidator.addValidation("ownerCity","req","Please enter the owner's city");
//  frmvalidator.addValidation("ownerZip","req","Please enter the owner's zip code");
//  frmvalidator.addValidation("operatorName","req","Please enter the name of the operator");
//  frmvalidator.addValidation("operatorPhone","req","Please enter the operator's phone number");
//  frmvalidator.addValidation("operatorAddress","req","Please enter the street address of the operator");
//  frmvalidator.addValidation("operatorCity","req","Please enter the operator's city");
//  frmvalidator.addValidation("operatorZip","req","Please enter the operator's zip code");
//  frmvalidator.addValidation("facilityContact","req","Please enter the facility contact");
//  frmvalidator.addValidation("facilityContactPhone","req","Please enter the facility contact's phone number");
//  frmvalidator.addValidation("startDate","req","Please enter the start date");
//  frmvalidator.addValidation("completionDate","req","Please enter the completion date")  
//  frmvalidator.addValidation("estimatedDisturbedAcerage","req","Please enter the estimated disturbed Acerage")
//  frmvalidator.addValidation("estimatedDisturbedAcerage","numeric")  
//  frmvalidator.addValidation("IRWName","req","Please enter the name of initial receiving water(s)")
//  frmvalidator.addValidation("MSSSOwnerOperator","req","Please enter the municipal storm sewer system owner/operator")
//  frmvalidator.addValidation("RWName","req","Please enter the name of receiving water(s)")  
//  frmvalidator.addValidation("numberOfOutfalls","req","Please enter the number of outfalls")
//  frmvalidator.addValidation("numberOfOutfalls","numeric")  
//  frmvalidator.addValidation("SWDrainageArea","req","Please enter the surface water drainage area")
//  frmvalidator.addValidation("SWDrainageArea","numeric")  
 // frmvalidator.addValidation("certify1","req","Please enter initials")
  //frmvalidator.addValidation("certify2","req","Please enter initials")
  //frmvalidator.addValidation("certify3","req","Please enter initials")
//  frmvalidator.addValidation("ownerPrintedName","req","Please enter the owner's printed name");
//  frmvalidator.addValidation("ownerTitle","req","Please enter the owner's title");
//  frmvalidator.addValidation("ownerSignDate","req","Please enter the date the owner signs the form");
//  frmvalidator.addValidation("operatorPrintedName","req","Please enter the operator's printed name");
//  frmvalidator.addValidation("operatorTitle","req","Please enter the operator's title");
  //frmvalidator.addValidation("operatorSignDate","req","Please enter the date the operator signs the form"); 

</script>
<%
rs.close
rsState.close
set rs = nothing
set rsState = nothing
DataConn.close
set DataConn = nothing
%>