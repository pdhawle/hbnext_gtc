<%
server.ScriptTimeout = 2000
On Error Resume Next

'iCustomerID = request("customer")
iProjectID = request("project")
if request("division") <> "" then
	iDivisionID = request("division")
else
	iDivisionID = 1
end if
iReportTypeID = request("reportType")


'response.Write iDivisionID & "<br>"
'response.Write iProjectID & "<br>"


Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
'get reportTypes
Set oCmd = Server.CreateObject("ADODB.Command")
		
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetUserReportTypes"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, session("ID"))
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsReportType = oCmd.Execute
Set oCmd = nothing

If iReportTypeID <> "" then	
	'Create command for customer list
	'only the customers that are assigned to the user
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetAssignedDivisions"
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, session("ID"))
	   .CommandType = adCmdStoredProc
	   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
		
	Set rsDivisions = oCmd.Execute
	Set oCmd = nothing
end if

If iDivisionID <> "" then
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetAssignedProjectsByDivision"
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iDivisionID)
	   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, session("ID"))
	   .CommandType = adCmdStoredProc   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
				
	Set rsProject = oCmd.Execute
	Set oCmd = nothing


	'Create command for inspection type list
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetInspectionTypesByDivision"
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iDivisionID)
	   .CommandType = adCmdStoredProc
	   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
		
	Set rsInspectionType = oCmd.Execute
	Set oCmd = nothing
	
	'Create command for weather type list
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetWeatherTypesByDivision"
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iDivisionID)
	   .CommandType = adCmdStoredProc
	   
	End With

	
	If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
		
	Set rsWeather = oCmd.Execute
	Set oCmd = nothing
	
	'Create command for weather type list
'	Set oCmd = Server.CreateObject("ADODB.Command")
		
'	With oCmd
'	   .ActiveConnection = DataConn
'	   .CommandText = "spGetProjectStatusByDivision"
'	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iDivisionID)
'	   .CommandType = adCmdStoredProc
'	   
'	End With
'	
'		
'	Set rsStatus = oCmd.Execute
'	Set oCmd = nothing
end if
If iDivisionID <> "" and iProjectID <> "" then
	'Create command for question category list
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetCategories"'ByDivision
	   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, iDivisionID)
	   .CommandType = adCmdStoredProc
	   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
		
	Set rsCategories = oCmd.Execute
	Set oCmd = nothing
end if

If iProjectID <> "" then
	'get the number of open action items
	'Create command for question category list
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetOpenAIByProject"
	   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, iProjectID)
	   .CommandType = adCmdStoredProc
	   
	End With
	
	Set rsAllOpen = oCmd.Execute
	Set oCmd = nothing
	
	
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetProject"
	   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, iProjectID)
	   .CommandType = adCmdStoredProc
	   
	End With
	
	Set rsProjInfo = oCmd.Execute
	Set oCmd = nothing
	
	dteYesterdaysDate = dateadd("d",-1,now())
	dteDate = year(dteYesterdaysDate) & "-" & addZeroToNumber(month(dteYesterdaysDate)) & "-" & addZeroToNumber(day(dteYesterdaysDate)) & "T00:00:00"
	
	'response.Write "date " & dteYesterdaysDate

	iRainfallAmount = getRainfallAmount(iProjectID,dteDate) 
	
	LDADate = rsProjInfo("startDate")

end if

%>
<script type="text/javascript">
<!--

function addEvent(ival,ques)
{
//alert (ival)
//ival = the letter
//ques = the question number
var ni = document.getElementById("myDiv"+ival);
//alert (ni)
var numi = document.getElementById("theValue"+ival);
var num = (document.getElementById("theValue"+ival).value -1)+ 2;
numi.value = num;
var divIdName = "answer"+ival+num;
var newdiv = document.createElement('div');
newdiv.setAttribute("id",divIdName);
//newdiv.innerHTML = "Element Number "+num+" has been added! <a href=\"javascript:;\" onclick=\"removeEvent(\'"+divIdName+"\')\">Remove</a>";
//<br><a href=javascript:onClick=doNew();>Upload Image</a> <input type=text name=tempImage>
//newdiv.innerHTML = "<table><tr><td>Corrective action "+ival+ "-" +num+"&nbsp;&nbsp;<a href=>select answer</a><br><input type=text name=answer_R"+ival+ "-" +num+"@"+ques+" size=40></td><td>Location"+ival+ "-" +num+"<br><input type=text name=location_"+ival+ "-" +num+" size=30></td><td valign=bottom> <a href=\"javascript:;\" onclick=\"removeEvent(\'"+divIdName+"\','"+ival+"\')\">Remove action</a></td></tr></table>";
newdiv.innerHTML = "<table><tr><td>Action item "+ival+ "-" +num+"&nbsp;&nbsp;<a href=javascript:displayAnswerPicker('answer_R"+ival+ "-" +num+"@"+ques+"')>select action</a><br><input type=text name=answer_R"+ival+ "-" +num+"@"+ques+" size=40></td><td>Location"+ival+ "-" +num+"<br><input type=text name=location_"+ival+ "-" +num+" size=30></td><!--<td><input type=checkbox checked name=installed"+ival+ "-" +num+" />&nbsp;In Compliance</td>--><!--<td valign=bottom> <a href=\"javascript:;\" onclick=\"removeEvent(\'"+divIdName+"\','"+ival+"\')\">Remove action</a></td>--></tr><tr><td colspan=3>Proposed Activity<br><input type=text name=proposedActivity"+ival+ "-" +num+" size=60 /></td></tr><tr><td colspan=3>Comments<br><input type=text name=observations"+ival+ "-" +num+" size=60 /></td></tr><tr><td colspan=3>Date Installed<br><input type=text name=dateInstalled"+ival+ "-" +num+" size=10 />&nbsp;<a href=javascript:displayDatePicker('dateInstalled"+ival+ "-" +num+"')><img alt='Pick a date' src='images/date.gif' border=0 width=17 height=16></a></td></tr></table>";

//
//newdiv.innerHTML = "Responsive action needed "+ival+num+" <input type=text name=answer_"+ival+num+"@"+ques+" size=40> <a href=\"javascript:;\" onclick=\"removeEvent(\'"+divIdName+"\','"+ival+"\')\">Remove action</a>";
//alert (newdiv.innerHTML)
ni.appendChild(newdiv);
//if no action recommended is checked, chech the add remarks button
//alert (document.getElementById("answer_&"+ques+"2").value);
//document.getElementById("answer_&"+ques+"2").checked = true;  removed this per request

//check the radio button for "add + ques +  remarks"
//if (document.addReport."answer_&34"[0].checked) {
//      document.addReport."answer_&34"[1].checked=true}
//   else {
//   if (document.addReport."answer_&34"[2].checked) {
//      document.addReport."answer_&34"[1].checked=true}
//   }



//alert (document.addReport.answer_& + ques + [0].value)
//document.addReport.answer_& + ques + ['See Corrective Action Log'].checked=true
//document.addReport.answer_ + ques + ["See Corrective Action Log"].checked = true
//document.addReport.c[0].checked=false
}

function removeEvent(divNum,ival)
{
//alert (divNum)
var d = document.getElementById("myDiv"+ival);
var olddiv = document.getElementById(divNum);
d.removeChild(olddiv);
}

function rept_onchange(addReport,repID) {
   window.location.href = 'form.asp?formType=addReport&reportType='+repID;
}

function dept_onchange(addReport,repID,divID) {
   window.location.href = 'form.asp?formType=addReport&division='+divID+'&reportType='+repID;
}

function proj_onchange(addReport,repID,divID,projID) {
   window.location.href = 'form.asp?formType=addReport&division='+divID+'&project='+projID+'&reportType='+repID;
}


function formControl(submitted) 
{
   if(submitted=="1") 
    {
   addReport.Submit.disabled=true
   document.addReport.submit();
  // alert("Thanks for your comments!")
    }
}

var myWind = ""
function doNew() {
	if (myWind == "" || myWind.closed || myWind.name == undefined) {
		myWind = window.open("uploadImage.asp?project=<%=iProjectID%>","subWindow","HEIGHT=600,WIDTH=1000")
	} else{
		myWind.focus();
	}
}
// -->
</script>
<script type="text/javascript" src="includes/jquery.js"></script>
<form name="addReport" method="post" action="process.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sNPDESTitle%></span><span class="Header"> - Add Report</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td>
												<table border="0" cellpadding="0" cellspacing="0">
													<tr>
														<td><img src="images/pix.gif" width="10" height="1"></td>
														<td valign="top"><span class="required">*</span> <strong>Report Type:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<select name="reportType" onChange="return rept_onchange(addReport,this.getElementsByTagName('option')[this.selectedIndex].value)">
																<option>--Select a report type--</option>
																<%do until rsReportType.eof
																	If trim(rsReportType("reportTypeID")) = trim(iReportTypeID) then%>
																		<option selected="selected" value="<%=rsReportType("reportTypeID")%>" ><%=rsReportType("reportType")%></option>
																	<%else%>
																		<option value="<%=rsReportType("reportTypeID")%>" ><%=rsReportType("reportType")%></option>
																	<%end if
																rsReportType.movenext
																loop%>
															</select>&nbsp;&nbsp;<%if iReportTypeID = "" then%>**please select a report type**<%end if%>
														</td>
													</tr>
													<tr><td colspan="4"><img src="images/pix.gif" width="1" height="5"></td></tr>			
													<tr>
														<td><img src="images/pix.gif" width="10" height="1"></td>
														<td valign="top"><span class="required">*</span> <strong>Division:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<select name="division" onChange="return dept_onchange(addReport,reportType.getElementsByTagName('option')[reportType.selectedIndex].value,this.getElementsByTagName('option')[this.selectedIndex].value)">
																<option>--Select a Division--</option>
																<%If iReportTypeID <> "" then
																do until rsDivisions.eof
																	If trim(rsDivisions("divisionID")) = trim(iDivisionID) then%>
																		<option selected="selected" value="<%=rsDivisions("divisionID")%>" ><%=rsDivisions("division")%></option>
																	<%else%>
																		<option value="<%=rsDivisions("divisionID")%>" ><%=rsDivisions("division")%></option>
																	<%end if
																'iCustomerID = rsDivisions("customerID")
																rsDivisions.movenext
																loop
																end if%>
															</select>&nbsp;&nbsp;
															<%if iReportTypeID <> "" then
															if iDivisionID = "" then%>
																**please select a Division**
															<%end if
															end if%>
														</td>
													</tr>
													<tr><td colspan="4"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td></td>
														<td valign="top"><span class="required">*</span> <strong>Site/Proj. Name:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<select name="project" onChange="return proj_onchange(addReport,reportType.getElementsByTagName('option')[reportType.selectedIndex].value,division.getElementsByTagName('option')[division.selectedIndex].value,this.getElementsByTagName('option')[this.selectedIndex].value)">
																<option>--Select a project--</option>
																<%If iDivisionID <> "" then
																	do until rsProject.eof
																		If trim(rsProject("projectID")) = trim(iProjectID) then%>
																			<option selected="selected" value="<%=rsProject("projectID")%>"><%=rsProject("projectName")%></option>
																		<%else%>
																			<option value="<%=rsProject("projectID")%>"><%=rsProject("projectName")%></option>
																		<%end if
																	rsProject.movenext
																	loop
																end if%>
															</select>&nbsp;&nbsp;
															<%if iDivisionID <> "" then
																if iProjectID = "" then%>
																**please select a project**
															<%end if
															end if%>
														</td>
													</tr>
													
													<%If iProjectID <> "" then%>
														<%if iReportTypeID <> 25 then%>
														<tr><td colspan="4"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td></td>
															<td valign="top"><strong>LDA Date:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<input type="text" name="LDADate" maxlength="10" size="10" value="<%=formatdatetime(LDADate,2)%>"/>&nbsp;<a href="javascript:displayDatePicker('LDADate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
															</td>
														</tr>
														<tr><td colspan="4"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td></td>
															<td valign="top"><strong>NTU Limit:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<%=rsProjInfo("NTULimit")%>
															</td>
														</tr>
														<%end if%>
														<%if rsProjInfo("projectNumber") <> "" then%>
														<tr><td colspan="4"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td></td>
															<td valign="top"><strong>Project Number:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<%=rsProjInfo("projectNumber")%>
															</td>
														</tr>
														<%end if%>
													<%end if%>
													<tr><td colspan="4"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td></td>
														<td valign="top"><strong>Inspector:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=Session("Name")%>
															<input type="hidden" name="inspector" value="<%=Session("ID")%>" />
														</td>
													</tr>
													<!--<tr><td colspan="4"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td></td>
														<td valign="top"><span class="required">*</span> <strong>Contact Number:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%'if request("contactNumber") <> "" then
															'	contactNumber = request("contactNumber")
															'else
															'	contactNumber = Session("cellPhone")
															'end if%>
															<input type="text" name="contactNumber" size="20" value="<%'=contactNumber%>"/>
														</td>
													</tr>-->
													<%if iReportTypeID <> 20 then
													if iReportTypeID <> 3 then
													if iReportTypeID <> 25 then%>
														<tr><td colspan="4"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td></td>
															<td valign="top"><span class="required">*</span> <strong>Inspection Type:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<select name="inspectionType">
																	<%If iDivisionID <> "" then
																	do until rsInspectionType.eof
																		if trim(request("inspectionType")) = trim(rsInspectionType("inspectionTypeID")) then%>
																			<option selected="selected" value="<%=rsInspectionType("inspectionTypeID")%>"><%=rsInspectionType("inspectionType")%></option>
																		<%else%>
																			<option value="<%=rsInspectionType("inspectionTypeID")%>"><%=rsInspectionType("inspectionType")%></option>
																	<%end if
																	rsInspectionType.movenext
																	loop
																	end if%>
																</select>
															</td>
														</tr>
														<!--<tr><td colspan="4"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td></td>
															<td valign="top"><span class="required">*</span> <strong>Project Status:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<select name="projectStatus">
																	<%'If iDivisionID <> "" then
																	'do until rsStatus.eof
																	'	if trim(request("projectStatus")) = trim(rsStatus("projectStatusID")) then%>
																			<option selected="selected" value="<%'=rsStatus("projectStatusID")%>"><%'=rsStatus("projectStatus")%></option>
																		<%'else%>
																			<option value="<%'=rsStatus("projectStatusID")%>"><%'=rsStatus("projectStatus")%></option>
																	<%'end if
																'	rsStatus.movenext
																'	loop
																'	end if%>
																</select>
															</td>
														</tr>-->
													<%end if
													end if
													end if%>
													<tr><td colspan="4"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td></td>
														<td valign="top"><span class="required">*</span> <strong>Inspection Date:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%if request("inspectionDate") <> "" then
																inspectionDate = request("inspectionDate")
															else
																inspectionDate = now()
															end if%>
															<input type="text" name="inspectionDate" maxlength="10" size="10" value="<%=formatdatetime(inspectionDate,2)%>"/>&nbsp;<a href="javascript:displayDatePicker('inspectionDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
														</td>
													</tr>
													<%if iProjectID <> "" then
													if iReportTypeID <> 25 then
													if iReportTypeID <> 2 then
														if iReportTypeID <> 3 then
															if iReportTypeID <> 7 then
																if iReportTypeID <> 20 then
																%>
																<tr><td colspan="4"><img src="images/pix.gif" width="1" height="5"></td></tr>
																<tr>
																	<td></td>
																	<td valign="top"><span class="required">*</span> <strong>Weather Type:</strong></td>
																	<td><img src="images/pix.gif" width="5" height="1"></td>
																	<td>
																		<select name="weatherType">
																			<%If iDivisionID <> "" then
																			do until rsWeather.eof
																				if trim(request("weatherType")) = trim(rsWeather("weatherID")) then%>
																					<option selected="selected" value="<%=rsWeather("weatherID")%>"><%=rsWeather("weatherType")%></option>
																				<%else%>
																					<option value="<%=rsWeather("weatherID")%>"><%=rsWeather("weatherType")%></option>
																			<%end if
																			rsWeather.movenext
																			loop
																			end if%>
																		</select>
																	</td>
																</tr>
																<%end if%>
															<%end if%>
																<tr><td colspan="4"><img src="images/pix.gif" width="1" height="5"></td></tr>
																<tr>
																	<td></td>
																	<td valign="top"><span class="required">*</span> <strong>Rainfall Source:</strong></td>
																	<td><img src="images/pix.gif" width="5" height="1"></td>
																	<td>
																		<select name="rainfallSource">
																			<option value=""></option>
																			<option value="Rainwave">Rainwave</option>
																			<option value="Sampler">Sampler</option>
																			<option value="Rain gauge">Rain gauge</option>
																		</select>
																	</td>
																</tr>
																<tr><td colspan="4"><img src="images/pix.gif" width="1" height="5"></td></tr>
																<tr>
																	<td></td>
																	<td valign="top"><span class="required">*</span> <strong>Rainfall Amount:</strong></td>
																	<td><img src="images/pix.gif" width="5" height="1"></td>
																	<td>
																		<%if request("rainfallAmount") <> "" then
																			rainfallAmount = request("rainfallAmount")
																		else
																			rainfallAmount = iRainfallAmount
																		end if%>
																		<input type="text" name="rainfallAmount" size="2" value="<%=rainfallAmount%>"/> inches (please enter numbers only)
																	</td>
																</tr>
																
															<%if iReportTypeID = 14 or iReportTypeID = 15 then%>
																<input type="hidden" name="personnelPresent" value="" />
																<!--<tr><td colspan="4"><img src="images/pix.gif" width="1" height="5"></td></tr>
																<tr>
																	<td></td>
																	<td valign="top"><span class="required">*</span> <strong>Personnel Present:</strong></td>
																	<td><img src="images/pix.gif" width="5" height="1"></td>
																	<td>
																		<select name="personnelPresent">
																			<option value="No One Present">No One Present</option>
																			<option value="Quality Services Inspector">Quality Services Inspector</option>
																			<option value="Service Resource Inspector">Service Resource Inspector</option>
																			<option value="GPC Inspector">GPC Inspector</option>
																		</select>
																	</td>
																</tr>-->	
															<%end if%>
																
																
															<%if iReportTypeID = 14 or iReportTypeID = 15 then%>
																<tr><td colspan="4"><img src="images/pix.gif" width="1" height="5"></td></tr>
																<tr>
																	<td></td>
																	<td valign="top"><strong>% compliant:</strong></td>
																	<td><img src="images/pix.gif" width="5" height="1"></td>
																	<td>
																		<a href="#" onClick="MM_openBrWindow('openItems.asp?projectID=<%=iProjectID%>','popPrint','scrollbars=yes,resizable=yes,width=1000,height=600')">view open items</a>&nbsp;&nbsp;<br />
																		<!--<a href="percentCompleteChart.asp?projectID=<%'=iProjectID%>" rel="gb_page_fs[]">view chart</a><br>-->
																		<%=rsAllOpen("openAI")%> total open items<br />
																		<%=rsAllOpen("closedAI")%> total corrected items<br />
																		<%
																			iTotalItems = rsAllOpen("closedAI") + rsAllOpen("openAI")
																			iTotalComplete = (rsAllOpen("closedAI")/iTotalItems) * 100
																		if iTotalComplete <> "" then
																			response.Write "<b>" & round(iTotalComplete) & "% corrected</b>"
																		end if
																		%>
																	</td>
																</tr>
																<!--<tr><td colspan="4"><img src="images/pix.gif" width="1" height="5"></td></tr>
																<tr>
																	<td></td>
																	<td valign="top"><strong>Lot management:</strong></td>
																	<td><img src="images/pix.gif" width="5" height="1"></td>
																	<td>
																		<a href="#" onClick="MM_openBrWindow('openLots.asp?ID=<%'=iProjectID%>','popPrint','scrollbars=yes,resizable=yes,width=1000,height=600')">view open/closed lots</a>&nbsp;&nbsp;<br />
																	</td>
																</tr>-->
														<%end if
													end if
													end if
													end if
													end if%>																
													<tr><td colspan="4"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td></td>
														<td valign="top"><strong>Comments:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td valign="top">
															<textarea name="comments" rows="3" cols="30"><%=request("comments")%></textarea>
															Add to blog/journal: 
															<%if iReportTypeID = 10 or iReportTypeID = 19 then 'erosion control checklist%>
																<input checked="checked" type="checkbox" name="addToJournal" />
															<%else%>
																<input type="checkbox" name="addToJournal" />
															<%end if%>
														</td>
													</tr>
													<%if iReportTypeID <> 14 then
													if iReportTypeID <> 3 then
													if iReportTypeID <> 25 then%>
													<tr><td colspan="4"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td></td>
														<td valign="top"><strong>Soil Stabilization<br />Comments:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td valign="top">
															<textarea name="soilStabilizationComments" rows="3" cols="30"><%'=request("soilStabilizationComments")%></textarea>
														</td>
													</tr>
													<%end if
													end if
													end if
													
													if iDivisionID <> "" then
													if iProjectID <> "" then
													if iReportTypeID = 14 or iReportTypeID = 7 or iReportTypeID = 15 then%>
													<tr><td colspan="4"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td></td>
														<td></td>
														<td colspan="2">
															<a href="locationList.asp?projectID=<%=iProjectID%>&customerID=<%=rsProject("customerID")%>&divisionID=<%=iDivisionID%>&red=Report" title="Sequence News" rel="gb_page_fs[]">manage locations</a><br />
															<%Set oCmd = Server.CreateObject("ADODB.Command")
	
															With oCmd
															   .ActiveConnection = DataConn
															   .CommandText = "spGetLocationsByProject"
															   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iProjectID)
															   .CommandType = adCmdStoredProc
															   
															End With
																		
															Set rsLocations = oCmd.Execute
															Set oCmd = nothing
															
															iLoc = 0
															
															if rsLocations.eof then
																iLoc = 0
															else
															
																do until rsLocations.eof
																	iLoc = iLoc + 1
																rsLocations.movenext
																loop
															end if%>
															
															current locations for this project: <strong><%=iLoc%></strong>
														</td>
													</tr>
													<%end if
													end if
													end if%>
													<tr><td colspan="4"><img src="images/pix.gif" width="1" height="30"></td></tr>				
												</table>
												<table width="100%" border="0" cellpadding="0" cellspacing="0">
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td valign="top" colspan="3">
															<%
																Select Case iReportTypeID
																	Case 1
																		sType = "spGetWeeklyQuestion"
																'	Case 3
																'		sType = "spGetMonthlyQuestion"
																	Case 4
																		sType = "spGetPostRainQuestion"
																	Case 8
																		sType = "spGetBiWeeklyQuestion"
																	Case 9
																		sType = "spGetAnnualQuestion"	
																	Case 13
																		sType = "spGetPetroleumQuestion"
																	Case 14 
																		sType = "spGetUtilBWPRQuestion"
																	Case 15
																		sType = "spGetUtilBWPRQuestion"
																		
																	Case 21
																		sType = "spGetWeeklyQuestion"
																	case 25
																		sType = ""
																	Case else
																		sType = "spGetWeeklyQuestion"
																										
																end Select						
																'get the report type to pass in
																'make dynamic?	
																'response.Write iDivisionID & "<br>"						
																If iDivisionID <> "" and iProjectID <> "" then
																	'display the report type
																	'if it is daily display the monthly log
																	Select Case iReportTypeID
									
																		Case 2 'daily report%>
																			<!--#include file="inc_form_daily.asp"-->
																		
																		<%Case 3 'monthly report%>
																			<!--#include file="inc_form_monthly.asp"-->
																				
																		<%case 7 'water sampling report%>	
																			<!--#include file="inc_form_WaterSampling.asp"-->
																			
																		<%case 8 'bi weekly report%>	
																			<!--#include file="inc_form_biWeekly.asp"-->
																			
																		<%case 10 'erosion control checklist%>	
																			<!--#include file="inc_form_ecChecklist.asp"-->
																			
																		<%case 11 'audit%>	
																			<!--#include file="inc_form_Audit.asp"-->
																			
																		<%Case 12 'GDOT daily report%>
																			<!--#include file="inc_form_GDOTDaily.asp"-->
																			
																		<%case 13 'petroleum%>	
																			<!--#include file="inc_form_Petroleum.asp"-->
																			
																		<%case 14 'utility bi weekly%>	
																			<!--#include file="inc_form_UtilBWPR.asp"-->
																		
																		<%case 15 'utility post-rain%>	
																			<!--#include file="inc_form_UtilBWPR.asp"-->
																			
																		<%Case 16 'utility daily report%>
																			<!--#include file="inc_form_Utildaily.asp"-->
																			
																		<%case 19 'monthly erosion control checklist%>	
																			<!--#include file="inc_form_ecChecklistMonthly.asp"-->
																			
																		<%case 20 'GDOT Daily Inspection%>	
																			<!--#include file="inc_form_GDOTDailyInsp.asp"-->
																			
																		<%case 21 'GDOT Monthly%>	
																			<!--#include file="inc_form_GDOTMonthly.asp"-->
																			
																		<%case 25 'Final Monthly%>	
																			<!--#include file="inc_form_FinalMonthly.asp"-->
									
																		<%Case else 'all others - weekly, post-rain, annual%>
																			<!--#include file="inc_form_Weekly.asp"-->
																			
																			
																	<%end select
																else%>
																	<td>there are no questions to display. please select a customer from the dropdown at the top of the page.</td>
																<%end if%>
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td colspan="3" align="right">
															<%If iProjectID <> "" then%>
																<input type="hidden" name="customer" value="<%=iCustomerID%>" />
																<input type="hidden" name="countyID" value="<%=rsProjInfo("countyID")%>" />
																<%select case iReportTypeID
																	case 2 'daily%>
																		<input type="hidden" name="processType" value="addDaily" />
																	<%case 3 'monthly report%>
																		<input type="hidden" name="processType" value="addMonthly" />
																	<%case 7 'water sampling%>
																		<input type="hidden" name="processType" value="addWaterSampling" />
																	<%case 8 'bi weekly%>
																		<input type="hidden" name="processType" value="addBiWeekly" />
																	<%case 10 'EC checklist%>
																		<input type="hidden" name="processType" value="addECChecklist" />
																	<%case 11 'audit%>
																		<input type="hidden" name="processType" value="addAuditReport" />
																	<%case 12 'GDOT Daily%>
																		<input type="hidden" name="processType" value="addGDOTDaily" />
																	<%case 13 'Petroleum%>
																		<input type="hidden" name="processType" value="addPetroleum" />
																	<%case 14 'utility bi weekly%>
																		<input type="hidden" name="processType" value="addUtilBWPR" />
																		<!--this report is like reg weekly but generates different PDF
																		<input type="hidden" name="buildPDFType" value="UtilBWPR" />-->
																	<%case 15 'utility post rain%>
																		<input type="hidden" name="processType" value="addUtilBWPR" />
																		<!--this report is like reg weekly but generates different PDF
																		<input type="hidden" name="buildPDFType" value="UtilBWPR" />-->																		
																	<%case 16 'utility post rain%>
																		<input type="hidden" name="processType" value="addUtilDaily" />
																		
																	<%case 19 'Monthly EC checklist%>
																		<input type="hidden" name="processType" value="addECChecklistMonthly" />
																		
																	<%case 20 'GDOT Daily Inspection%>
																		<input type="hidden" name="processType" value="addGDOTDailyInspection" />
																		
																	<%case 21 'GDOT Daily Inspection%>
																		<input type="hidden" name="processType" value="addGDOTMonthly" />
																		
																	<%case 25 'monthly Final%>
																		<input type="hidden" name="processType" value="addMonthlyFinal" />
																		
																	<%case else 'others%>
																		<input type="hidden" name="processType" value="addReport" />
																		<!--<input type="hidden" name="buildPDFType" value="Weekly" />-->
																<%end select%>
																<a href="contactList.asp?project=<%=iProjectID%>" target="_blank">Manage Project Contacts</a><br /><br />
																<input type="submit" name="Submit" value="  Save  " class="formButton" onClick="formControl(1)"/>
									
															<%end if%>
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
												</table>
											</td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
										</tr>
									</table>
									
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<script language="JavaScript" type="text/javascript">

  var frmvalidator  = new Validator("addReport");
  
  frmvalidator.addValidation("contactNumber","req","Please enter a contact number");
  frmvalidator.addValidation("inspectionDate","req","Please enter the inspection date");
  <%if iProjectID <> "" then
	  if iReportTypeID <> 2 then
	  if iReportTypeID <> 3 then
	  	if iReportTypeID <> 7 then%>
		  frmvalidator.addValidation("rainfallAmount","req","Please enter the rainfall amount");
		  //frmvalidator.addValidation("rainfallAmount","numeric");
	  <%end if
	  end if
	  end if
	  if iReportTypeID = 7 then%>
	  	  //frmvalidator.addValidation("sampleNumber","req","Please enter the sample number");
		  //frmvalidator.addValidation("sampleNumber","numeric");
	  	  //frmvalidator.addValidation("dateSampleTaken","req","Please enter the date the sample was taken");
	  	  //frmvalidator.addValidation("exactLocation","req","Please enter the exact location the sample was taken");
		  //frmvalidator.addValidation("city","req","Please enter the city the sample was taken");
		  //frmvalidator.addValidation("county","req","Please enter the county the sample was taken");
		  //frmvalidator.addValidation("sampledBy","req","Please enter the person who took the sample");
	<%end if
	
	if iReportTypeID = 8 then%>
	  	  
	  	  frmvalidator.addValidation("districtOffice","req","Please enter the district office");
	  	  frmvalidator.addValidation("EPDDivision","req","Please enter the EPD division");
		  frmvalidator.addValidation("address","req","Please enter the EPD division address");		  
		  frmvalidator.addValidation("city","req","Please enter the EPD division city");
		  frmvalidator.addValidation("zip","req","Please enter the EPD division zip code");
		  frmvalidator.addValidation("reportCovering","req","Please enter the dates the report covers");		  
		  frmvalidator.addValidation("county","req","Please enter the county");
		  frmvalidator.addValidation("observation1","req","Please enter your observations");
		  frmvalidator.addValidation("observation2","req","Please enter your observations");
	<%end if
	
	if iReportTypeID = 14 then%>
		<%=validationText%>
		
	<%end if
	if iReportTypeID = 15 then%>
		<%=validationText%>
		
	<%end if
  end if%>
</script>

<%
rsStatus.close
set rsStatus = nothing
rsDivisions.close
set rsDivisions = nothing
rsInspectors.close
set rsInspectors = nothing
rsInspectionType.close
set rsInspectionType = nothing
rsWeather.close
set rsWeather = nothing
rsCategories.close
set rsCategories = nothing
rsQuestion.close
set rsQuestion = nothing

DataConn.close
set DataConn = nothing
%>