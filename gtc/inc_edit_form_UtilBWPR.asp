<table cellpadding="0" cellspacing="0" border="0">
	<%'else display the questions
	If rsCategories.eof then%>
		<tr><td></td><td colspan="7">there are no questions to display. please select another customer and project from the dropdown at the top of the page or add questions for this customer via the administrator.</td></tr>
	<%else%>
		<!--<tr><td></td><td colspan="7"><strong>Note:</strong> If action is needed, select "Add remarks" and add the remarks in the textbox provided. If you need more textboxes, click the "Add BMP" link.<br /><br /></td></tr>-->
		<%blnChange = true
		i = 1
		Do until rsCategories.eof
			'response.Write iDivisionID
			%>
			<tr>
				<td><img src="images/pix.gif" width="5" height="0"></td>
				<td>
					<%
	
						Set oCmd = Server.CreateObject("ADODB.Command")

						With oCmd
						   .ActiveConnection = DataConn
						   .CommandText = sType
						   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, rsCategories("categoryID"))
						   .CommandType = adCmdStoredProc
						   
						End With
							
									
						Set rsQuestion = oCmd.Execute
						Set oCmd = nothing
						%>
						
						<%if not rsQuestion.eof then%>
						<u><strong><%=rsCategories("category")%></strong></u><br><br>
						<%end if%>
						<table width="800" cellpadding="0" cellspacing="0" border="0">
							<%blnChange = true
							do until rsQuestion.eof
							
							
								'reset the num val
								iNumVal = 0
								'get the pre-defined locations for this question and project
								Set oCmd = Server.CreateObject("ADODB.Command")
								With oCmd
								   .ActiveConnection = DataConn
								   .CommandText = "spGetResponsiveActions"
								   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, iReportID)
								   .parameters.Append .CreateParameter("@questionID", adInteger, adParamInput, 8, rsQuestion("questionID"))
								   .CommandType = adCmdStoredProc										   
								End With												
								Set rsLocation = oCmd.Execute
								Set oCmd = nothing
								
								if not rsLocation.eof then
									bAddQuestion = "True"									
								else
									bAddQuestion = "False"
								end if
							
							
								if bAddQuestion = "True" then
							
								If blnChange = true then%>
								<tr class="rowColor">
								<%else%>
								<tr>
								<%end if%>
									<td valign="top">
										<br />&nbsp;<%=i%>. 
									</td>
									<td>
										<br /><%=rsQuestion("question")%><br />
										
										<%										
										
										do until rsLocation.eof
										
											'get the major observations
											
										
											iNumVal = iNumVal + 1%>
											<table><tr><td>Action item <%=i%> - <%=iNumVal%><br><input type=text name="answer_R<%=i%>-<%=iNumVal%>@<%=rsQuestion("questionID")%>" size=40 value="<%=rsLocation("actionNeeded")%>"></td><td>Location<%=i%> - <%=iNumVal%><br><input type=text name="location_<%=i%>-<%=iNumVal%>" size=30 value="<%=rsLocation("location")%>"></td><td><input type="checkbox" name="inspected<%=i%>-<%=iNumVal%>" <%=isChecked(rsLocation("isClosed"))%> />&nbsp;Inspected Location</td></tr>
											<tr><td colspan="3">Proposed Activity<br /><input type="text" name="proposedActivity<%=i%>-<%=iNumVal%>" size="60" value="<%=rsLocation("proposedActivity")%>" /></td></tr>
											<tr><td colspan="3">Comments<br /><input type="text" name="observations<%=i%>-<%=iNumVal%>" size="60" value="<%=rsLocation("majObserv")%>" /></td></tr>
											<tr><td colspan="3">Date Installed<br /><input type="text" name="dateInstalled<%=i%>-<%=iNumVal%>" size="10" value="<%=rsLocation("dateInst")%>" />&nbsp;<a href="javascript:displayDatePicker('dateInstalled<%=i%>-<%=iNumVal%>')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a></td></tr></table>
											<input type="hidden" name="locationID<%=i%>-<%=iNumVal%>" value="<%=rsLocation("locationID")%>" />
											<%validationText = validationText & "frmvalidator.addValidation(""locationID" & i & "-" & iNumVal & """,""shouldselchk=on"",""Please check the inspected location"");"%>
											<!--response.Write rsLocation("location") & "<br>"-->
										<%rsLocation.movenext
										loop
										%>
																					
										<!--<input type="hidden" value="<%'=iNumVal%>" id="theValue<%'=i%>" />
										<a href="javascript:;" onclick="addEvent('<%'=i%>','<%'=rsQuestion("questionID")%>');">Add BMP</a><br /><br />
										<div id="myDiv<%'=i%>"> </div>--><br /><br />
									</td>
									<td valign="top" align="right"><img src="images/pix.gif" width="100" height="1"><br />
										<%if iProjectID <> "" then%><!--questionID=<%'=rsQuestion("questionID")%>&-->
											<a href="#" onClick="MM_openBrWindow('openItems.asp?projectID=<%=iProjectID%>','popPrint','scrollbars=yes,resizable=yes,width=1000,height=600')">view all open items</a>&nbsp;&nbsp;<br /><br />
										<%end if%>
									</td>
								</tr>
						
						<%end if
						
						rsQuestion.movenext
						i=i+1
						if blnChange = true then
							blnChange = false
						else
							blnChange = true
						end if
						loop%>
						

					</table>
					
				</td>
			</tr>
		<%rsCategories.movenext
		loop%>
		<!--<tr><td colspan="5" height="20"></td></tr>
		
		
		
		<tr>
			<td colspan="5">
			
				<%
	'			Set oCmd = Server.CreateObject("ADODB.Command")
	
	'			With oCmd
	'			   .ActiveConnection = DataConn
	'			   .CommandText = "spGetMonthlyRainData"
	'				.parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, iReportID) 'reportID
	'			   .CommandType = adCmdStoredProc
	'			   
	'			End With
	'						
	'			Set rsData = oCmd.Execute
	'			Set oCmd = nothing
	'			
	'			
	'			'create arrays for each item
	'			i = 0
	'			
	'			ReDim edarrRainDateEvent(10)
	'			ReDim edarrRainAmount(10)
	'			ReDim edarrCodeCol1(10)
	'			ReDim edarrCodeCol2(10)
	'			ReDim edarrCodeCol3(10)
	'			ReDim edarrCodeCol4(10)
	'			ReDim edarrCodeCol5(10)
	'			ReDim edarrCodeCol6(10)
	'			ReDim edarrCodeCol7(10)
	'			ReDim edarrCodeCol8(10)
	'			ReDim edarrCodeCol9(10)
	'			ReDim edarrCodeCol10(10)
	'			
	'			do until rsData.eof
	'	
	'				'edarrDateSamplerRemoved(i) = rsData("dateSamplerRemoved")
	'				edarrRainDateEvent(i) = rsData("rainDateEvent")
	'				edarrRainAmount(i) = rsData("rainAmount")
	'				edarrCodeCol1(i) = rsData("codeCol1")
	'				edarrCodeCol2(i) = rsData("codeCol2")
	'				edarrCodeCol3(i) = rsData("codeCol3")
	'				edarrCodeCol4(i) = rsData("codeCol4")
	'				edarrCodeCol5(i) = rsData("codeCol5")
	'				edarrCodeCol6(i) = rsData("codeCol6")
	'				edarrCodeCol7(i) = rsData("codeCol7")
	'				edarrCodeCol8(i) = rsData("codeCol8")
	'				edarrCodeCol9(i) = rsData("codeCol9")
	'				edarrCodeCol10(i) = rsData("codeCol10")
	'			rsData.movenext
	'			i=i+1
	'			loop
				%>
			
				<table>
					
					<tr><td height="20" colspan="3"></td></tr>
				
					<tr>
						<td>Date of >= 0.5<br /> inch event</td>
						<td>Amt. Of<br />Rainfall</td>
						<td colspan="10" bgcolor="#000000">&nbsp;</td>
					</tr>
					<%'for i=1 to 10%>
					<tr>
						<td><input type="text" size="10" class="grayTextBox" name="rainDateEvent<%'=i%>" value="<%'=edarrRainDateEvent(i - 1)%>" />&nbsp;<a href="javascript:displayDatePicker('rainDateEvent<%'=i%>')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a></td>
						<td><input type="text" size="10" class="grayTextBox" name="rainAmount<%'=i%>" value="<%'=edarrRainAmount(i - 1)%>" /></td>
						<td>
							<select class="blueSelect" name="codeCol1<%'=i%>">
								<option></option>
								<option <%'=isSelected("ST-E",trim(edarrCodeCol1(i - 1)))%>>ST-E</option>
								<option <%'=isSelected("ST",trim(edarrCodeCol1(i - 1)))%>>ST</option>
								<option <%'=isSelected("N/A",trim(edarrCodeCol1(i - 1)))%>>N/A</option>	
								<option <%'=isSelected("Q-NO",trim(edarrCodeCol1(i - 1)))%>>Q-NO</option>
								<option <%'=isSelected("Q-ND",trim(edarrCodeCol1(i - 1)))%>>Q-ND</option>
								<option <%'=isSelected("Q-DB",trim(edarrCodeCol1(i - 1)))%>>Q-DB</option>
								<option <%'=isSelected("Q-MS",trim(edarrCodeCol1(i - 1)))%>>Q-MS</option>
								<option <%'=isSelected("Q-SM",trim(edarrCodeCol1(i - 1)))%>>Q-SM</option>
								<option <%'=isSelected("Q-IU",trim(edarrCodeCol1(i - 1)))%>>Q-IU</option>
								<option <%'=isSelected("NBH",trim(edarrCodeCol1(i - 1)))%>>NBH</option>
								
							</select>						
						</td>
						<td>
							<select class="blueSelect" name="codeCol2<%'=i%>">
								<option></option>
								<option <%'=isSelected("ST-E",trim(edarrCodeCol2(i - 1)))%>>ST-E</option>
								<option <%'=isSelected("ST",trim(edarrCodeCol2(i - 1)))%>>ST</option>
								<option <%'=isSelected("N/A",trim(edarrCodeCol2(i - 1)))%>>N/A</option>	
								<option <%'=isSelected("Q-NO",trim(edarrCodeCol2(i - 1)))%>>Q-NO</option>
								<option <%'=isSelected("Q-ND",trim(edarrCodeCol2(i - 1)))%>>Q-ND</option>
								<option <%'=isSelected("Q-DB",trim(edarrCodeCol2(i - 1)))%>>Q-DB</option>
								<option <%'=isSelected("Q-MS",trim(edarrCodeCol2(i - 1)))%>>Q-MS</option>
								<option <%'=isSelected("Q-SM",trim(edarrCodeCol2(i - 1)))%>>Q-SM</option>
								<option <%'=isSelected("Q-IU",trim(edarrCodeCol2(i - 1)))%>>Q-IU</option>
								<option <%'=isSelected("NBH",trim(edarrCodeCol2(i - 1)))%>>NBH</option>
							</select>						
						</td>
						<td>
							<select class="blueSelect" name="codeCol3<%'=i%>">
								<option></option>
								<option <%'=isSelected("ST-E",trim(edarrCodeCol3(i - 1)))%>>ST-E</option>
								<option <%'=isSelected("ST",trim(edarrCodeCol3(i - 1)))%>>ST</option>
								<option <%'=isSelected("N/A",trim(edarrCodeCol3(i - 1)))%>>N/A</option>	
								<option <%'=isSelected("Q-NO",trim(edarrCodeCol3(i - 1)))%>>Q-NO</option>
								<option <%'=isSelected("Q-ND",trim(edarrCodeCol3(i - 1)))%>>Q-ND</option>
								<option <%'=isSelected("Q-DB",trim(edarrCodeCol3(i - 1)))%>>Q-DB</option>
								<option <%'=isSelected("Q-MS",trim(edarrCodeCol3(i - 1)))%>>Q-MS</option>
								<option <%'=isSelected("Q-SM",trim(edarrCodeCol3(i - 1)))%>>Q-SM</option>
								<option <%'=isSelected("Q-IU",trim(edarrCodeCol3(i - 1)))%>>Q-IU</option>
								<option <%'=isSelected("NBH",trim(edarrCodeCol3(i - 1)))%>>NBH</option>
							</select>						
						</td>
						<td>
							<select class="blueSelect" name="codeCol4<%'=i%>">
								<option></option>
								<option <%'=isSelected("ST-E",trim(edarrCodeCol4(i - 1)))%>>ST-E</option>
								<option <%'=isSelected("ST",trim(edarrCodeCol4(i - 1)))%>>ST</option>
								<option <%'=isSelected("N/A",trim(edarrCodeCol4(i - 1)))%>>N/A</option>	
								<option <%'=isSelected("Q-NO",trim(edarrCodeCol4(i - 1)))%>>Q-NO</option>
								<option <%'=isSelected("Q-ND",trim(edarrCodeCol4(i - 1)))%>>Q-ND</option>
								<option <%'=isSelected("Q-DB",trim(edarrCodeCol4(i - 1)))%>>Q-DB</option>
								<option <%'=isSelected("Q-MS",trim(edarrCodeCol4(i - 1)))%>>Q-MS</option>
								<option <%'=isSelected("Q-SM",trim(edarrCodeCol4(i - 1)))%>>Q-SM</option>
								<option <%'=isSelected("Q-IU",trim(edarrCodeCol4(i - 1)))%>>Q-IU</option>
								<option <%'=isSelected("NBH",trim(edarrCodeCol4(i - 1)))%>>NBH</option>
							</select>						
						</td>
						<td>
							<select class="blueSelect" name="codeCol5<%'=i%>">
								<option></option>
								<option <%'=isSelected("ST-E",trim(edarrCodeCol5(i - 1)))%>>ST-E</option>
								<option <%'=isSelected("ST",trim(edarrCodeCol5(i - 1)))%>>ST</option>
								<option <%'=isSelected("N/A",trim(edarrCodeCol5(i - 1)))%>>N/A</option>	
								<option <%'=isSelected("Q-NO",trim(edarrCodeCol5(i - 1)))%>>Q-NO</option>
								<option <%'=isSelected("Q-ND",trim(edarrCodeCol5(i - 1)))%>>Q-ND</option>
								<option <%'=isSelected("Q-DB",trim(edarrCodeCol5(i - 1)))%>>Q-DB</option>
								<option <%'=isSelected("Q-MS",trim(edarrCodeCol5(i - 1)))%>>Q-MS</option>
								<option <%'=isSelected("Q-SM",trim(edarrCodeCol5(i - 1)))%>>Q-SM</option>
								<option <%'=isSelected("Q-IU",trim(edarrCodeCol5(i - 1)))%>>Q-IU</option>
								<option <%'=isSelected("NBH",trim(edarrCodeCol5(i - 1)))%>>NBH</option>
							</select>						
						</td>
						<td>
							<select class="blueSelect" name="codeCol6<%'=i%>">
								<option></option>
								<option <%'=isSelected("ST-E",trim(edarrCodeCol6(i - 1)))%>>ST-E</option>
								<option <%'=isSelected("ST",trim(edarrCodeCol6(i - 1)))%>>ST</option>
								<option <%'=isSelected("N/A",trim(edarrCodeCol6(i - 1)))%>>N/A</option>	
								<option <%'=isSelected("Q-NO",trim(edarrCodeCol6(i - 1)))%>>Q-NO</option>
								<option <%'=isSelected("Q-ND",trim(edarrCodeCol6(i - 1)))%>>Q-ND</option>
								<option <%'=isSelected("Q-DB",trim(edarrCodeCol6(i - 1)))%>>Q-MS</option>
								<option <%'=isSelected("Q-MS",trim(edarrCodeCol6(i - 1)))%>>Q-DB</option>
								<option <%'=isSelected("Q-SM",trim(edarrCodeCol6(i - 1)))%>>Q-SM</option>
								<option <%'=isSelected("Q-IU",trim(edarrCodeCol6(i - 1)))%>>Q-IU</option>
								<option <%'=isSelected("NBH",trim(edarrCodeCol6(i - 1)))%>>NBH</option>
							</select>						
						</td>
						<td>
							<select class="blueSelect" name="codeCol7<%'=i%>">
								<option></option>
								<option <%'=isSelected("ST-E",trim(edarrCodeCol7(i - 1)))%>>ST-E</option>
								<option <%'=isSelected("ST",trim(edarrCodeCol7(i - 1)))%>>ST</option>
								<option <%'=isSelected("N/A",trim(edarrCodeCol7(i - 1)))%>>N/A</option>	
								<option <%'=isSelected("Q-NO",trim(edarrCodeCol7(i - 1)))%>>Q-NO</option>
								<option <%'=isSelected("Q-ND",trim(edarrCodeCol7(i - 1)))%>>Q-ND</option>
								<option <%'=isSelected("Q-DB",trim(edarrCodeCol7(i - 1)))%>>Q-DB</option>
								<option <%'=isSelected("Q-MS",trim(edarrCodeCol7(i - 1)))%>>Q-MS</option>
								<option <%'=isSelected("Q-SM",trim(edarrCodeCol7(i - 1)))%>>Q-SM</option>
								<option <%'=isSelected("Q-IU",trim(edarrCodeCol7(i - 1)))%>>Q-IU</option>
								<option <%'=isSelected("NBH",trim(edarrCodeCol7(i - 1)))%>>NBH</option>
							</select>						
						</td>
						<td>
							<select class="blueSelect" name="codeCol8<%'=i%>">
								<option></option>
								<option <%'=isSelected("ST-E",trim(edarrCodeCol8(i - 1)))%>>ST-E</option>
								<option <%'=isSelected("ST",trim(edarrCodeCol8(i - 1)))%>>ST</option>
								<option <%'=isSelected("N/A",trim(edarrCodeCol8(i - 1)))%>>N/A</option>	
								<option <%'=isSelected("Q-NO",trim(edarrCodeCol8(i - 1)))%>>Q-NO</option>
								<option <%'=isSelected("Q-ND",trim(edarrCodeCol8(i - 1)))%>>Q-ND</option>
								<option <%'=isSelected("Q-DB",trim(edarrCodeCol8(i - 1)))%>>Q-DB</option>
								<option <%'=isSelected("Q-MS",trim(edarrCodeCol8(i - 1)))%>>Q-MS</option>
								<option <%'=isSelected("Q-SM",trim(edarrCodeCol8(i - 1)))%>>Q-SM</option>
								<option <%'=isSelected("Q-IU",trim(edarrCodeCol8(i - 1)))%>>Q-IU</option>
								<option <%'=isSelected("NBH",trim(edarrCodeCol8(i - 1)))%>>NBH</option>
							</select>						
						</td>
						<td>
							<select class="blueSelect" name="codeCol9<%'=i%>">
								<option></option>
								<option <%'=isSelected("ST-E",trim(edarrCodeCol9(i - 1)))%>>ST-E</option>
								<option <%'=isSelected("ST",trim(edarrCodeCol9(i - 1)))%>>ST</option>
								<option <%'=isSelected("N/A",trim(edarrCodeCol9(i - 1)))%>>N/A</option>	
								<option <%'=isSelected("Q-NO",trim(edarrCodeCol9(i - 1)))%>>Q-NO</option>
								<option <%'=isSelected("Q-ND",trim(edarrCodeCol9(i - 1)))%>>Q-ND</option>
								<option <%'=isSelected("Q-DB",trim(edarrCodeCol9(i - 1)))%>>Q-DB</option>
								<option <%'=isSelected("Q-MS",trim(edarrCodeCol9(i - 1)))%>>Q-MS</option>
								<option <%'=isSelected("Q-SM",trim(edarrCodeCol9(i - 1)))%>>Q-SM</option>
								<option <%'=isSelected("Q-IU",trim(edarrCodeCol9(i - 1)))%>>Q-IU</option>
								<option <%'=isSelected("NBH",trim(edarrCodeCol9(i - 1)))%>>NBH</option>
							</select>						
						</td>
						<td>
							<select class="blueSelect" name="codeCol10<%'=i%>">
								<option></option>
								<option <%'=isSelected("ST-E",trim(edarrCodeCol10(i - 1)))%>>ST-E</option>
								<option <%'=isSelected("ST",trim(edarrCodeCol10(i - 1)))%>>ST</option>
								<option <%'=isSelected("N/A",trim(edarrCodeCol10(i - 1)))%>>N/A</option>	
								<option <%'=isSelected("Q-NO",trim(edarrCodeCol10(i - 1)))%>>Q-NO</option>
								<option <%'=isSelected("Q-ND",trim(edarrCodeCol10(i - 1)))%>>Q-ND</option>
								<option <%'=isSelected("Q-DB",trim(edarrCodeCol10(i - 1)))%>>Q-DB</option>
								<option <%'=isSelected("Q-MS",trim(edarrCodeCol10(i - 1)))%>>Q-MS</option>
								<option <%'=isSelected("Q-SM",trim(edarrCodeCol10(i - 1)))%>>Q-SM</option>
								<option <%'=isSelected("Q-IU",trim(edarrCodeCol10(i - 1)))%>>Q-IU</option>
								<option <%'=isSelected("NBH",trim(edarrCodeCol10(i - 1)))%>>NBH</option>
							</select>						
						</td>
					</tr>
					<%'next%>
			
				</table>
			
			
			</td>
		</tr>--->
		
		
		
		
		<tr>
			<td colspan="5">
				<input type="checkbox" name="inCompliance" checked="checked" />&nbsp;Overall the project is in compliance
			</td>
		</tr>
		</table>
	<%end if%>