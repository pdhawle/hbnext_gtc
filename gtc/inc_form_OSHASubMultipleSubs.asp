<%
'get a list of the sub-contractors for this customer
Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetSubContractors"
   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, rsProjInfo("customerID"))
   .CommandType = adCmdStoredProc   
End With
			
Set rsSub = oCmd.Execute
Set oCmd = nothing

'get the subs into the array
do until rsSub.eof
	sSubList = sSubList & chr(34) & rsSub("subContractorName") & " - (" & rsSub("subContractorType") & ")" & chr(34)
rsSub.movenext
if not rsSub.eof then
	sSubList = sSubList & ","
end if
loop

'get info for this customer
Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomer"
   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, rsProjInfo("customerID"))
   .CommandType = adCmdStoredProc   
End With
			
Set rsCustomer = oCmd.Execute
Set oCmd = nothing
%>

<script type='text/javascript' src='jquery/jquery.autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="jquery/jquery.autocomplete.css" />
<script type="text/javascript">
var subs = [
	<%=sSubList%>
];

<%ii = 1
for ii = 1 to 10%>

	$().ready(function() {	
		$("#subName<%=ii%>").autocomplete(subs);
	});
<%next%>
</script>
	
	
<!--add questions for the subs
need to select sub to add questions for-->
<br /><br /><strong class="subHeader">Sub-contractor/Vendor Inspection Information (if applicable)</strong><br />
Enter sub-contractor below<br /><br />
<%
iSub = 1
for iSub = 1 to 10%>
	<!--create a hidden div here until the link is clicked to show it for each sub-contractor-->
	<script type="text/javascript">
		function addFormFieldSub<%=iSub%>() {
		var id = document.getElementById("idSub<%=iSub%>").value;
		$("#divTxtSub<%=iSub%>").append("<span id='row" + id + "'><label for='txt" + id + "'><b>" + id + ".</b>&nbsp;&nbsp<textarea name=sub_question@answer<%=iSub%>_" + id + " id=sub_question@answer<%=iSub%>_" + id + " rows=2 cols=95 class=textArea></textarea>&nbsp;&nbsp<a href='#' onClick='removeFormField(\"#row" + id + "\"); return false;'>Remove</a><span><br><br>");
		
		$('#row' + id).highlightFade({
			speed:1000
		});
		
		id = (id - 1) + 2;
		document.getElementById("idSub<%=iSub%>").value = id;
		}
	
		function removeFormField(idSub<%=iSub%>) {
		//alert (id);
			$(idSub<%=iSub%>).remove();
		}
	</script>
	
	<style>
		#myformSub<%=iSub%> label {
		  font-family: Arial, Helvetica, sans-serif;
		  font-size:11px;
		}
		
		#quesListSub<%=iSub%> {
		  margin: 0px 0px 0px 0px;
		  display: none;
		}
	</style>
	<div id="myformSub<%=iSub%>">
		<div id="content<%=iSub%>"><input type="text" name="subName<%=iSub%>" id="subName<%=iSub%>" size="30" />&nbsp;&nbsp;<input type="radio" name="showQuestionsSub<%=iSub%>" onclick="javascript: $('#quesListSub<%=iSub%>').show('slow');" value="show" /> show&nbsp;&nbsp;<input checked="checked" type="radio" name="showQuestionsSub<%=iSub%>" value="hide" onclick="javascript: $('#quesListSub<%=iSub%>').hide('slow');" /> hide</div><br>
		<div id="quesListSub<%=iSub%>">
			<!--start of the list of questions-->				
			<!--add the OSHA questions for the current customer and any of it's sub-contractors-->
			<table cellpadding="0" cellspacing="0" border="0">	
					<tr>
						<td><img src="images/pix.gif" width="5" height="0"></td>
						<td>
								<%
								i = 1
								Set oCmd = Server.CreateObject("ADODB.Command")
		
								With oCmd
								   .ActiveConnection = DataConn
								   .CommandText = "spGetOSHAQuestion"  'spGetOSHASubQuestion
								   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, rsReport("divisionID"))
								   .CommandType = adCmdStoredProc
								   
								End With
											
								Set rsQuestion = oCmd.Execute
								Set oCmd = nothing
								%>
								
								<%if not rsQuestion.eof then%>
								<br />
								<%end if%>
								<table width="800" cellpadding="0" cellspacing="0" border="0">
									<%blnChange = true
									do until rsQuestion.eof
										If blnChange = true then%>
										<tr class="rowColor">
										<%else%>
										<tr>
										<%end if%>
											<td valign="top">
												<br />&nbsp;<strong><%=i%>.</strong>&nbsp;
											</td>
											<td valign="top">
												<br /><%=rsQuestion("question")%><br />
												<input type="radio" name="sub_question_answer<%=iSub%>_<%=rsQuestion("questionID")%>" value="1" />&nbsp;<%=rsQuestion("YesAnswer")%><br />
												<input type="radio" name="sub_question_answer<%=iSub%>_<%=rsQuestion("questionID")%>" value="2" />&nbsp;<%=rsQuestion("NoAnswer")%><br />
												<input type="radio" name="sub_question_answer<%=iSub%>_<%=rsQuestion("questionID")%>" value="3" checked="checked" />&nbsp;<%=rsQuestion("NAAnswer")%><br />
												<input type="radio" name="sub_question_answer<%=iSub%>_<%=rsQuestion("questionID")%>" value="4" />&nbsp;<%=rsQuestion("NotInspAnswer")%><br />
												
												<a href=javascript:displayCommentPicker('sub_question_comment<%=iSub%>_<%=rsQuestion("questionID")%>')><strong>select comment</strong></a><br />
												<input type="text" name="sub_question_comment<%=iSub%>_<%=rsQuestion("questionID")%>" size="70" /><br />
												<input type="hidden" name="sub_question_question<%=iSub%>_<%=rsQuestion("questionID")%>" value="<%=rsQuestion("question")%>" />
												<input type="hidden" name="sub_question_ID<%=rsQuestion("questionID")%>" value="<%=rsQuestion("questionID")%>" /><br />
											</td>
										</tr>
								
								<%rsQuestion.movenext
								i=i+1
								if blnChange = true then
									blnChange = false
								else
									blnChange = true
								end if
								loop%>
								
								
							</table>
							
						</td>
					</tr>
					<tr>
						<td></td>
						<td colspan="7">&nbsp;&nbsp;
							<input type="hidden" id="idSub<%=iSub%>" value="<%=i%>">
							<div id="divTxtSub<%=iSub%>"></div>
						</td>
					</tr>	
					<tr><td colspan="7" height=5></td></tr>
					<tr>
						<td></td>
						<td colspan="7">&nbsp;&nbsp;
							<!--add a new entry to this-->
							<a href="#" onClick="addFormFieldSub<%=iSub%>(); return false;">+ add new entry</a>
						</td>
					</tr>
				</table>
			
			
			<!--end of the list of questions-->
		</div>
	</div>	
	<br />
<%'rsSub.movenext
'loop
next
%>

<br />
The main issues found during this inspection that may have resulted in OSHA citations are:<br />
<strong>NOTE:</strong> To insert single carraige return, hold the "shift" key while pressing enter.
<script language="JavaScript" type="text/javascript" src="cbrte/html2xhtml.js"></script>
<script language="JavaScript" type="text/javascript" src="cbrte/richtext_compressed.js"></script>
<script language="JavaScript" type="text/javascript">
<!--
function submitRTEForm() {
	updateRTEs();
	return true;
}
initRTE("cbrte/images/", "cbrte/", "", true);
//-->
</script>
<script language="JavaScript" type="text/javascript">
<!--
var rte1 = new richTextEditor('rte1');
//rte1.html = '<%'=sContent%>';
rte1.toggleSrc = false;
rte1.toolbar1 = false;
rte1.build();
//-->
</script>
