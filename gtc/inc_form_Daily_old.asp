<table class="borderTable" width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr bgcolor="#666666">
		<td><span class="searchText">&nbsp;Date</span></td>
		<td align="center"><span class="searchText">Rainfall Amount</span></td>
		<td align="center"><span class="searchText">Petroleum Storage Area <br />Is there any evidence of Spills or leaks?</span></td>
		<td align="center"><span class="searchText">Construction Exits <br />Are any repairs or corrections needed?</span></td>
		<td align="center"><span class="searchText">Inspected By</span></td>
	</tr>
	<%
	Dim counter
	counter=1
	blnChange = true
	for counter = 1 to 31
		If blnChange = true then%>
			<tr class="rowColor">
		<%else%>
			<tr>
		<%end if%>
			<td align="center"><%=counter%></td>
			<td align="center"><input type="text" name="rainfall<%=counter%>" size="3" /></td>
			<td align="center"><select name="question1_<%=counter%>"><option>No</option><option>Yes</option></select></td>
			<td align="center"><select name="question2_<%=counter%>"><option>No</option><option>Yes</option></select></td>
			<td align="center">
				<%
				'get the assigned users for this project
				Set oCmd = Server.CreateObject("ADODB.Command")

				With oCmd
				   .ActiveConnection = DataConn
				   .CommandText = "spGetAssignedUsers"
				   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iProjectID)
				   .CommandType = adCmdStoredProc
				   
				End With
					
					If Err Then
				%>
						<!--#include file="includes/FatalError.inc"-->
				<%
					End If
					
				Set rsInspector = oCmd.Execute
				Set oCmd = nothing
				%>
				<select name="inspector<%=counter%>">
					<option value="">--inspected by--</option>
					<%'rsInspector.movefirst
					do until rsInspector.eof%>
						<option value="<%=rsInspector("userID")%>"><%=rsInspector("lastName") & ", " & rsInspector("firstName")%></option>
					<%rsInspector.movenext
					loop
					%>
				</select>
			</td>
	   </tr>
	<%if blnChange = true then
		blnChange = false
	else
		blnChange = true
	end if
	
	next%>
</table>	