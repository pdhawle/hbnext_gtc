<%@ Language=VBScript %>
<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%On Error Resume Next

projectID = request("projectID")

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")

With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetOpenAIByProject"
   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
   .CommandType = adCmdStoredProc
   
End With
		
Set rs = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")

With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetProject"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, projectID)
   .CommandType = adCmdStoredProc
   
End With
		
Set rsProject = oCmd.Execute
Set oCmd = nothing

sProject = rsProject("customerName") & " - " & rsProject("division") & " - " & rsProject("projectName")
iTotalOpen = rs("openAI")
iTotalClosed = rs("closedAI")

if isNull(iTotalOpen) then
	iTotalOpen = 0
end if

if isNull(iTotalClosed) then
	iTotalClosed = 0
end if
%>

<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<SCRIPT LANGUAGE="Javascript" SRC="includes/FusionCharts.js"></SCRIPT>
<!-- #INCLUDE FILE="Includes/FusionCharts.asp" -->
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr bgcolor="#FFFFFF">
		<tr>
		<td colspan="3" bgcolor="#FA702B">
			<img src="images/pix.gif" width="5" height="10">
		</td>
	</tr>
	<tr>
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td colspan="15">
												<%Dim strXML
												'Generate the graph element
												strXML = "<graph caption='Percent Corrected' subCaption='" & sProject & "' decimalPrecision='0' showNames='1'  numberSuffix=' '  pieSliceDepth='30' formatNumberScale='0' bgColor='E3E3E3'>"
												strXML = strXML & "<set name='Total Open Items' value='" & iTotalOpen & "' color='EE0000' />"
												strXML = strXML & "<set name='Total Corrected Items' value='" & iTotalClosed & "' color='00BB00' />"
												'Finally, close <graph> element
												strXML = strXML & "</graph>"
												
												'Create the chart - Pie 3D Chart with data from strXML
												Call renderChart("charts/FCF_Pie3D.swf", "", strXML, "PercentComplete", 650, 450)
												%>
											</td>
										</tr>
									</table>
															
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<tr>
		<td colspan="3" bgcolor="#3C2315">
			<img src="images/pix.gif" width="5" height="10">
		</td>
	</tr>
</table>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>