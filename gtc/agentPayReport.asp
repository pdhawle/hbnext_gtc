<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%

iProjectID = request("projectID")
inspector = request("inspector")
smonth = request("month")
syear = request("year")

'response.Write iProjectID & "<br>"
'response.Write smonth & "<br>"
'response.Write syear & "<br>"

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

'If iProjectID = "All" Then

'	If inspector = "All" then
		'response.Write "1"
		'all projects fo all inspectors
		'Create command
'		Set oCmd = Server.CreateObject("ADODB.Command")
			
'		With oCmd
'		   .ActiveConnection = DataConn
'		   .CommandText = "spGetAgentPayAllProjectsAllAgents"
'		   .parameters.Append .CreateParameter("@month", adInteger, adParamInput, 8, smonth)
'		   .parameters.Append .CreateParameter("@year", adInteger, adParamInput, 8, syear)
'		   .CommandType = adCmdStoredProc   
'		End With
			
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
					
'		Set rsReport = oCmd.Execute
'		Set oCmd = nothing
	
'	else
		'response.Write "2"
		'all projects fo an inspector
		'Create command
'		Set oCmd = Server.CreateObject("ADODB.Command")
			
'		With oCmd
'		   .ActiveConnection = DataConn
'		   .CommandText = "spGetAgentPayByAgent"
'		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, inspector)
'		   .parameters.Append .CreateParameter("@month", adInteger, adParamInput, 8, smonth)
'		   .parameters.Append .CreateParameter("@year", adInteger, adParamInput, 8, syear)
'		   .CommandType = adCmdStoredProc   
'		End With
			
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
					
'		Set rsReport = oCmd.Execute
'		Set oCmd = nothing
'	end if
'else

'	If inspector = "All" then
		'response.Write "3"
		'all inspectors for a project
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetAgentPayByProject"
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, iProjectID)
		   .parameters.Append .CreateParameter("@month", adInteger, adParamInput, 8, smonth)
		   .parameters.Append .CreateParameter("@year", adInteger, adParamInput, 8, syear)
		   .CommandType = adCmdStoredProc   
		End With
			
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
		Set rsReport = oCmd.Execute
		Set oCmd = nothing
'	else
		'response.Write "4"
		'1 inspector for a project
		'Create command
'		Set oCmd = Server.CreateObject("ADODB.Command")
			
'		With oCmd
'		   .ActiveConnection = DataConn
'		   .CommandText = "spGetAgentPayByProjectandAgent"
'		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, iProjectID)
'		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, inspector)
'		   .parameters.Append .CreateParameter("@month", adInteger, adParamInput, 8, smonth)
'		   .parameters.Append .CreateParameter("@year", adInteger, adParamInput, 8, syear)
'		   .CommandType = adCmdStoredProc   
'		End With
			
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
					
'		Set rsReport = oCmd.Execute
'		Set oCmd = nothing
'	end if

'end if

%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Agent Pay Report</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<!--<a href="expAgentPayReport.asp?projectID=<%'=iProjectID%>&inspector=<%'=inspector%>&month=<%'=smonth%>&year=<%'=syear%>">Export Data to Excel</a>&nbsp;&nbsp;
						<span class="footerLink">|</span>&nbsp;&nbsp;--><a href="chooseAgent.asp" class="footerLink">back to selections</a>			
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td></td>
											<td colspan="6">
												<!--Inspector(s): <%'=inspector%><br>
												Project(s): <%'=iProjectID%><br>-->
												<strong><%=getMonth(smonth) & " " & sYear%></strong><br>
												<strong>Total # of inspection days <%=InspectionDaysInMonth(sYear,smonth)%></strong>
											</td>
										</tr>										
										<tr><td colspan="7"><img src="images/pix.gif" width="1" height="20"></td></tr>
										<tr>
											<td></td>
											<td colspan="6">
												<strong>Weekly/Post Rain Reports</strong><br>
												Click on the agents name to see details of each report done.
											</td>
										</tr>
										<tr>
											<td colspan="7">
												<table width="100%" cellpadding="0" cellspacing="0">
													<tr bgcolor="#666666">
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td><span class="searchText">Customer</span></td>
														<td><span class="searchText">Project</span></td>
														<td><span class="searchText">Agent/Inspector</span></td>
														<td><span class="searchText">Monthly Rate</span></td>
														<td><span class="searchText">Total # Reports</span></td>
														<td><span class="searchText">Comp</span></td>
													</tr>
													<%if rsReport.eof then%>
														<tr><td></td><td colspan="8">There are no result for the items you chose. Please select again.</td></tr>
													<%else
													blnChange = false
													iReportCount = 0
													'iUserOld = ""
													'iUser = ""
													'get the total number of reports for this project
													do until rsReport.eof
														iTotalReports = iTotalReports + rsReport("totalReports")
													rsReport.movenext
													loop
													rsReport.close
													set rsReport = nothing
													
													'open the recordset again to get the data
													Set oCmd = Server.CreateObject("ADODB.Command")
									
													With oCmd
													   .ActiveConnection = DataConn
													   .CommandText = "spGetAgentPayByProject"
													   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, iProjectID)
													   .parameters.Append .CreateParameter("@month", adInteger, adParamInput, 8, smonth)
													   .parameters.Append .CreateParameter("@year", adInteger, adParamInput, 8, syear)
													   .CommandType = adCmdStoredProc   
													End With
														
														If Err Then
													%>
															<!--#include file="includes/FatalError.inc"-->
													<%
														End If
														
													Set rsReport = oCmd.Execute
													Set oCmd = nothing
													
													do until rsReport.eof
																													
															If blnChange = true then%>
																<tr class="reportRowColor">
															<%else%>
																<tr>
															<%end if
																iReportCount = 	iReportCount + 1%>
																<td></td>
																<td><%=rsReport("customerName")%></td>
																<td><%=rsReport("projectName")%></td>
																<td><a href="agentPayDetails.asp?projectID=<%=iProjectID%>&inspector=<%=rsReport("userID")%>&month=<%=smonth%>&year=<%=syear%>" target="_blank"><%=rsReport("firstName") & " " & rsReport("lastName")%></a></td>
																<td>
																	<%response.Write formatcurrency(rsReport("wprRate"),2)%>
																</td>
																<td><%=rsReport("totalReports")%></td>
																<td>
																	<%
																	iPayRate = (rsReport("wprRate") / iTotalReports) * rsReport("totalReports")
																	
																	response.Write formatcurrency(iPayRate,2)
																	
																	'get the monthlyRate
																	iMonthlyRate = rsReport("wprRate")
																	%>
																</td>
															</tr>
															
															<%
															if blnChange = true then
																blnChange = false
															else
																blnChange = true
															end if
													
													rsReport.movenext
																
													loop
													rsReport.close
													set rsReport = nothing
													end if%>
													<tr><td colspan="4"><td><strong>Totals: </strong>&nbsp;</td><td><strong><%=iTotalReports%></strong></td><td><strong><%=formatCurrency(iMonthlyRate,2)%></strong></td>					
												</table>
											</td>
										</tr>
										
										<tr><td colspan="7"><img src="images/pix.gif" width="1" height="20"></td></tr>
										<tr><td></td><td colspan="6"><strong>Daily Reports</strong></td></tr>
										<tr>
											<td colspan="7">
												<table width="100%" cellpadding="0" cellspacing="0">
													<tr bgcolor="#666666">
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td><span class="searchText">Day of Month</span></td>
														<td><span class="searchText">Report ID</span></td>
														<td><span class="searchText">Customer</span></td>
														<td><span class="searchText">Project</span></td>
														<td><span class="searchText">Agent/Inspector</span></td>
														<td><span class="searchText">Monthly Rate</span></td>
														<td><span class="searchText">Comp</span></td>
													</tr>
													<%
													'get the daily reports for the month
													'may be more than one, we need to handle that
													'open the recordset again to get the data
													Set oCmd = Server.CreateObject("ADODB.Command")
									
													With oCmd
													   .ActiveConnection = DataConn
													   .CommandText = "spGetDailyReportByProjectandDate"
													   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, iProjectID)
													   .parameters.Append .CreateParameter("@month", adInteger, adParamInput, 8, smonth)
													   .parameters.Append .CreateParameter("@year", adInteger, adParamInput, 8, syear)
													   .CommandType = adCmdStoredProc   
													End With
														
														If Err Then
													%>
															<!--#include file="includes/FatalError.inc"-->
													<%
														End If
														
													Set rsReport = oCmd.Execute
													Set oCmd = nothing
													
													if rsReport.eof then%>
														<tr><td></td><td colspan="8">There are no result for the items you chose. Please select again.</td></tr>
													<%else
													blnChange = false
													
													do until rsReport.eof
															'get the details of each report
						
															iDailyRate = rsReport("dRate")
															iPayDay = iDailyRate / InspectionDaysInMonth(sYear,smonth)
																														
															If blnChange = true then%>
																<tr class="reportRowColor">
															<%else%>
																<tr>
															<%end if
															if rsReport("reportTypeID") = 2 Then
																iReportCount = 	iReportCount + 1%>
																<td></td>
																<td><%=rsReport("dayOfMonth")%></td>
																<td><%=rsReport("reportID")%></td>
																<td><%=rsReport("customerName")%></td>
																<td><%=rsReport("projectName")%></td>
																<td><%=rsReport("firstName") & " " & rsReport("lastName")%></td>
																<td>
																	<%'put the correct rate here
																	response.Write formatcurrency(rsReport("dRate"),2)
																	%>
																</td>
																<td><%=formatCurrency(iPayDay,2)%></td>
															</tr>
															
															<%
															if blnChange = true then
																blnChange = false
															else
																blnChange = true
															end if
														end if
													
													rsReport.movenext										
													loop
													end if%>
												</table>
											</td>
										</tr>
									</table>
															
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>
<%
'rsCustomer.close
rsReport.close
DataConn.close
Set rsCustomer = nothing
Set rsReport = nothing
Set DataConn = nothing
Set oCmd = nothing
%>