<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
Dim rs, oCmd, DataConn

divisionID = Request("id")
customerID = Request("customerID")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCraneCategories"
    .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, divisionID)
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rs = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomer"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, customerID)
   .CommandType = adCmdStoredProc
   
End With
			
Set rsCust = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetDivision"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, divisionID)
   .CommandType = adCmdStoredProc
   
End With
			
Set rsDiv = oCmd.Execute
Set oCmd = nothing
%>

<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script language="JavaScript">
<!--
function confirmDelete(delUrl) {
 if (confirm("Are you sure you wish to delete this question?")) {
    document.location = delUrl;
  }


}

function confirmRestore(sUrl) {
 if (confirm("Are you sure you wish to restore the default questions and categories? This cannot be undone.")) {
    document.location = sUrl;
  }


}
//-->
</script>
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Question List</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<%If not rs.eof then%>
							<a href="form.asp?formType=addCraneQuestion&customerID=<%=customerID%>&divisionID=<%=divisionID%>" class="footerLink">add question</a>&nbsp;&nbsp;<span class="footerLink">|</span>&nbsp;&nbsp;
						<%end if%>
						<a href="craneCategoryList.asp?id=<%=customerID%>&divisionID=<%=divisionID%>" class="footerLink">question categories</a>&nbsp;&nbsp;
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="customerList.asp" class="footerLink">customer list</a>&nbsp;&nbsp;	
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="divisionList.asp?ID=<%=customerID%>" class="footerLink">division list</a>&nbsp;&nbsp;
						<span class="footerLink">|</span>&nbsp;&nbsp;<a style="cursor:hand;" onClick="return confirmRestore('process.asp?processType=restoreCraneDefaults&customerID=<%=customerID%>&divisionID=<%=divisionID%>&clientID=<%=session("clientID")%>')" class="footerLink">restore defaults</a>			
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td colspan="9">
												<strong>Customer: </strong><%=rsCust("customerName")%><br>
												<strong>Division: </strong><%=rsDiv("division")%><br><br>
											</td>
										</tr>
										<%If rs.eof then%>
											<tr><td></td><td colspan="7">there are no questions or categories to display. <br>please click below to add a category and then add guestions to the category.</td></tr>
										<%else
											bShow = true
											i = 1
											blnChange = true
											Do until rs.eof%>
												<tr>
													<td colspan="9">
														<u><strong><%=rs("category")%></strong></u><br><br>
													</td>
												</tr>
												
														<%
														
															Set oCmd = Server.CreateObject("ADODB.Command")
							
															With oCmd
															   .ActiveConnection = DataConn
															   .CommandText = "spGetCraneQuestion"
															   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, rs("categoryID"))
															   '.parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, divisionID)
															   .CommandType = adCmdStoredProc
															   
															End With
																
																If Err Then
															%>
																	<!--#include file="includes/FatalError.inc"-->
															<%
																End If
																		
															Set rsQuestion = oCmd.Execute
															Set oCmd = nothing%>
															<!--<table width="100%" cellpadding="0" cellspacing="0">-->
																<tr bgcolor="#666666">
																	<td></td>
																	<td><span class="searchText">Question</span></td>
																	<td align="center"><span class="searchText">Tower Crane</span></td>
																	<td><span class="searchText">Edit/Delete</span></td>
																</tr>
																<tr><td colspan="8"><img src="images/pix.gif" width="1" height="10"></td></tr>
															<%blnChange = true
															do until rsQuestion.eof
																If blnChange = true then%>
																	<tr class="rowColor">
																<%else%>
																	<tr>
																<%end if%>
																	<td valign="top" width="1">&nbsp;<%=i%>.&nbsp;&nbsp;</td>
																	<td width="400"><%=rsQuestion("question")%></td>
																	<td align="center">
																		<%If rsQuestion("towerCrane") = "True" Then%>
																			<img src="images/check.gif" width="15" height="15">
																		<%end if%>	
																	</td>
																	<td><a href="form.asp?id=<%=rsQuestion("questionID")%>&formType=editCraneQuestion&customerID=<%=customerID%>&divisionID=<%=divisionID%>"><img src="images/edit.gif" width="19" height="19" alt="edit" border="0"></a>&nbsp;&nbsp;<input type="image" src="images/remove.gif" width="11" height="13" alt="delete" border="0" onClick="return confirmDelete('process.asp?id=<%=rsQuestion("questionID")%>&processType=deleteCraneQuestion&customerID=<%=customerID%>&divisionID=<%=divisionID%>')">&nbsp;</td>
																</tr>
															<%rsQuestion.movenext
															i = i + 1
															if blnChange = true then
																blnChange = false
															else
																blnChange = true
															end if
															loop
														%>
															<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>
														<!--</table>-->
													</td>
												</tr>
											<%rs.movenext
											loop
											rs.movefirst
										end if%>
									</table>
									
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>