<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
server.ScriptTimeout = 2000
'sendExpireAlerts()
'deActivateProjects()
'sevenDayAlert()
'fourteenDayAlert()
'overDueActionItems()
lastInspectedReport()

function lastInspectedReport()

	On Error Resume Next
	
	Set DataConn = Server.CreateObject("ADODB.Connection") 
	DataConn.Open Session("Connection"), Session("UserID")
	
	'get all of the users that are signed up to recieve 7 day alerts
'	Set oCmd = CreateObject("ADODB.Command")	
'	With oCmd
'		   .ActiveConnection = DataConn
'		   .CommandText = "spGetFourteenDayAlertUsers"
'		   .CommandType = adCmdStoredProc
'	End With
'	
'	Set rs = oCmd.Execute
'	Set oCmd = nothing
	
	'for each user, get the projects they are assigned to
'	Do until rs.eof
		sBody = ""
		Set oCmd = CreateObject("ADODB.Command")	
		With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spGetActiveProjectsALL"'spGetAssignedProjectsLastInspectionFD
			   '.parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, rs("userID"))
			   .CommandType = adCmdStoredProc
		End With
		
		Set rsProjects = oCmd.Execute
		Set oCmd = nothing
		
		sBody = sBody & "<table width=100" & chr(37) & "><tr><font face=Arial size=-1><b>Please DO NOT REPLY.</b><br>Below is a list of active projects and the number of days it has been since a BMP report has been generated.<br> You will be getting a daily reminder of the projects that need attention.</font><td colspan=5></td></tr>"
		sBody = sBody & "<tr><td><b><font face=Arial size=-1>Last Inspection Date</font></b></td><td><b><font face=Arial size=-1>Project Name</font></b></td><td><b><font face=Arial size=-1>Customer Name</font></b></td><td><b><font face=Arial size=-1>Days Since Last BMP Report</font></b></td></tr>"
	
		do until rsProjects.eof
			daysOld = datediff("d",rsProjects("lastInspectionDate"),now())
			'If trim(daysOld)  > 14 then
				sBody = sBody & "<tr><td><font face=Arial size=-2>" & rsProjects("lastInspectionDate") & "</font></td><td><font face=Arial size=-2>" & rsProjects("projectName") & "</font></td><td><font face=Arial size=-2>" & rsProjects("customerName") & "</font></td><td><font face=Arial size=-2>" & daysOld & "</font></td></tr>"
			'end if
		rsProjects.movenext
		loop
		Set oCmd = nothing
		rsProjects.close
		set rsProjects = nothing
		
		sBody = sBody & "</table>"
		
		'response.Write "<b>EMAIL: " & rs("email") & "</b><br>"
		'response.Write sBody & "<br><br>"
'*************************************************
		'send the email
		dim oCdoMail, oCdoConf,sConfURL
		Set oCdoMail = CreateObject("CDO.Message")
		Set oCdoConf = CreateObject("CDO.Configuration")

		sConfURL = "http://schemas.microsoft.com/cdo/configuration/"
		with oCdoConf
			.Fields.Item(sConfURL & "sendusing") = 2
			.Fields.Item(sConfURL & "smtpserver") = "localhost"
			.Fields.Item(sConfURL & "smtpserverport") = 25
			.Fields.Update
		 end with
		
		with oCdoMail
			  .From ="support@hbnext.com"
			  .To = "rreynolds@hbnext.com"'"tmiddlebrooks@hbnext.com"'dana.heil@gatrans.com;
			  .bcc = "rreynolds@hbnext.com;mwebb@hbnext.com"
'			  .To =  rs("email")
			end with
'	
		with oCdoMail
		  .Subject = appName & " - Last BMP Inspection Alert - " & date()
		  .TextBody = sBody
		  .HTMLBody = sBody
		end with
	
		oCdoMail.Configuration = oCdoConf
		oCdoMail.Send
		Set oCdoConf = Nothing
		Set oCdoMail = Nothing

'***************************************************
	
'	rs.movenext
'	loop
	
'	Set oCmd = nothing
'	rs.close
'	set rs = nothing
	
	DataConn.close
	set DataConn = nothing
	
	lastInspectedReport = True
end function

function sevenDayAlert()
	dim DataConn, conString, sUserID, oCmd, rs, sBody, daysOld
	On Error Resume Next
	
	Set DataConn = Server.CreateObject("ADODB.Connection") 
	DataConn.Open Session("Connection"), Session("UserID")
	
	'get all of the users that are signed up to recieve 7 day alerts
	Set oCmd = CreateObject("ADODB.Command")	
	With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetSevenDayAlertUsers"
		   .CommandType = adCmdStoredProc
	End With
	
	Set rs = oCmd.Execute
	Set oCmd = nothing
	
	'for each user, get the projects they are assigned to
	Do until rs.eof
		sBody = ""
		Set oCmd = CreateObject("ADODB.Command")	
		With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spGetAssignedProjectsLastInspection"
			   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, rs("userID"))
			   .CommandType = adCmdStoredProc
		End With
		
		Set rsProjects = oCmd.Execute
		Set oCmd = nothing
		
		sBody = sBody & "<table width=100" & chr(37) & "><tr><font face=Arial size=-1><b>Please DO NOT REPLY.</b><br>Below is a list of projects that have not had a report generated within the last seven days.<br> You will be getting a daily reminder of the projects that need attention.</font><td colspan=5></td></tr>"
		sBody = sBody & "<tr><td><b><font face=Arial size=-1>Last Inspection Date</font></b></td><td><b><font face=Arial size=-1>Project Name</font></b></td><td><b><font face=Arial size=-1>Customer Name</font></b></td><td><b><font face=Arial size=-1>Days Since Last Report</font></b></td></tr>"
	
		do until rsProjects.eof
			bSend = "True"
			daysOld = datediff("d",rsProjects("lastInspectionDate"),now())
			If trim(daysOld)  > 7 then
				sBody = sBody & "<tr><td><font face=Arial size=-2>" & rsProjects("lastInspectionDate") & "</font></td><td><font face=Arial size=-2>" & rsProjects("projectName") & "</font></td><td><font face=Arial size=-2>" & rsProjects("customerName") & "</font></td><td><font face=Arial size=-2>" & daysOld & "</font></td></tr>"
			end if
		rsProjects.movenext
		loop
		Set oCmd = nothing
		rsProjects.close
		set rsProjects = nothing
		
		sBody = sBody & "</table>"
		
'		response.Write sBody & "<br>"
'		response.Write rs("email") & "<br>"

		'send the email
		dim oCdoMail, oCdoConf,sConfURL
		Set oCdoMail = CreateObject("CDO.Message")
		Set oCdoConf = CreateObject("CDO.Configuration")
	
		sConfURL = "http://schemas.microsoft.com/cdo/configuration/"
		with oCdoConf
			.Fields.Item(sConfURL & "sendusing") = 2
			.Fields.Item(sConfURL & "smtpserver") = "localhost"
			.Fields.Item(sConfURL & "smtpserverport") = 25
			.Fields.Update
		 end with
	
		with oCdoMail
			  .From ="support@hbnext.com"
			  .To =  rs("email")
			end with
	
		with oCdoMail
		  .Subject = appName & " - Inspection 7 day Alert - " & date()
		  .TextBody = sBody
		  .HTMLBody = sBody
		end with

		oCdoMail.Configuration = oCdoConf
		oCdoMail.Send
		Set oCdoConf = Nothing
		Set oCdoMail = Nothing

	rs.movenext
	loop
	
	Set oCmd = nothing
	rs.close
	set rs = nothing
	
	DataConn.close
	set DataConn = nothing
	
	sevenDayAlert = True

end function


function fourteenDayAlert()

	On Error Resume Next
	
	Set DataConn = Server.CreateObject("ADODB.Connection") 
	DataConn.Open Session("Connection"), Session("UserID")
	
	'get all of the users that are signed up to recieve 7 day alerts
	Set oCmd = CreateObject("ADODB.Command")	
	With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetFourteenDayAlertUsers"
		   .CommandType = adCmdStoredProc
	End With
	
	Set rs = oCmd.Execute
	Set oCmd = nothing
	
	'for each user, get the projects they are assigned to
	Do until rs.eof
		sBody = ""
		Set oCmd = CreateObject("ADODB.Command")	
		With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spGetAssignedProjectsLastInspectionFD"
			   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, rs("userID"))
			   .CommandType = adCmdStoredProc
		End With
		
		Set rsProjects = oCmd.Execute
		Set oCmd = nothing
		
		sBody = sBody & "<table width=100" & chr(37) & "><tr><font face=Arial size=-1><b>Please DO NOT REPLY.</b><br>Below is a list of projects that have not had a report generated within the last fourteen days.<br> You will be getting a daily reminder of the projects that need attention.</font><td colspan=5></td></tr>"
		sBody = sBody & "<tr><td><b><font face=Arial size=-1>Last Inspection Date</font></b></td><td><b><font face=Arial size=-1>Project Name</font></b></td><td><b><font face=Arial size=-1>Customer Name</font></b></td><td><b><font face=Arial size=-1>Days Since Last Report</font></b></td></tr>"
	
		do until rsProjects.eof
			daysOld = datediff("d",rsProjects("lastInspectionDate"),now())
			If trim(daysOld)  > 14 then
				sBody = sBody & "<tr><td><font face=Arial size=-2>" & rsProjects("lastInspectionDate") & "</font></td><td><font face=Arial size=-2>" & rsProjects("projectName") & "</font></td><td><font face=Arial size=-2>" & rsProjects("customerName") & "</font></td><td><font face=Arial size=-2>" & daysOld & "</font></td></tr>"
			end if
		rsProjects.movenext
		loop
		Set oCmd = nothing
		rsProjects.close
		set rsProjects = nothing
		
		sBody = sBody & "</table>"
		
		'response.Write "<b>EMAIL: " & rs("email") & "</b><br>"
		'response.Write sBody & "<br><br>"

		'send the email
		dim oCdoMail, oCdoConf,sConfURL
		Set oCdoMail = CreateObject("CDO.Message")
		Set oCdoConf = CreateObject("CDO.Configuration")

		sConfURL = "http://schemas.microsoft.com/cdo/configuration/"
		with oCdoConf
			.Fields.Item(sConfURL & "sendusing") = 2
			.Fields.Item(sConfURL & "smtpserver") = "localhost"
			.Fields.Item(sConfURL & "smtpserverport") = 25
			.Fields.Update
		 end with
		
		with oCdoMail
			  .From ="support@hbnext.com"
			  .To =  rs("email")
			end with
	
		with oCdoMail
		  .Subject = appName & " - Inspection 14 day Alert - " & date()
		  .TextBody = sBody
		  .HTMLBody = sBody
		end with
	
		oCdoMail.Configuration = oCdoConf
		oCdoMail.Send
		Set oCdoConf = Nothing
		Set oCdoMail = Nothing
	
	rs.movenext
	loop
	
	Set oCmd = nothing
	rs.close
	set rs = nothing
	
	DataConn.close
	set DataConn = nothing
	
	fourteenDayAlert = True
end function

function overDueActionItems()
	On Error Resume Next
	
	Set DataConn = Server.CreateObject("ADODB.Connection") 
	DataConn.Open Session("Connection"), Session("UserID")
	
	'get the users that are able to recieve Open item alerts
	Set oCmd = CreateObject("ADODB.Command")	
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetOverdueActionItemUsers"
	   .CommandType = adCmdStoredProc
	End With
	
	Set rs = oCmd.Execute
	Set oCmd = nothing
	
	
	'for each user, get the projects they are assigned to and the number of open items
	Do until rs.eof
		Set oCmd = CreateObject("ADODB.Command")
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetOverDueActionItems"
		   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, rs("userID"))
		   .CommandType = adCmdStoredProc
		End With
		
		Set rsOI = oCmd.Execute
		Set oCmd = nothing
		
		sBody = sBody & "<table width=100" & chr(37) & "><tr><font face=Arial size=-1><b>Please DO NOT REPLY.</b><br>Below is a list of projects you are assigned to that have open items that are over seven days old.<br>Click on the project name to see the current open items for that project.<br> You will be getting a daily reminder of the projects that need attention.</font><td colspan=5></td></tr>"
		sBody = sBody & "<tr><td><b><font face=Arial size=-1>Project Name</font></b></td><td><b><font face=Arial size=-1>Customer Name</font></b></td><td><b><font face=Arial size=-1>Total Open Items</font></b></td></tr>"

		do until rsOI.eof
			'response.Write rsOI("projectID") & "<br>"

			'get the oldest report date that has an open action item
			Set oCmd = CreateObject("ADODB.Command")
	
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spGetOverDueActionItemsOldestReport"
			   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, rsOI("projectID"))
			   .CommandType = adCmdStoredProc
			End With
 
			
			Set rsDate = oCmd.Execute			
			daysOld = datediff("d",rsDate("oldestDate"),now())

			If trim(daysOld)  > 7 then
				sBody = sBody & "<tr><td><font face=Arial size=-2><a href=" & sURL & "?sURLRed=openItems.asp?projectID=" & rsOI("projectID") & ">" & rsOI("projectName") & "</a></font></td><td><font face=Arial size=-2>" & rsOI("customerName") & "</font></td><td><font face=Arial size=-2>" & rsOI("openItems") & "</font></td></tr>"
			end if
		rsOI.movenext
		loop
		
		sBody = sBody & "</table>"
			
			
		'send the email to the current user
		dim oCdoMail, oCdoConf,sConfURL
		Set oCdoMail = CreateObject("CDO.Message")
		Set oCdoConf = CreateObject("CDO.Configuration")
	
		sConfURL = "http://schemas.microsoft.com/cdo/configuration/"
		with oCdoConf
			.Fields.Item(sConfURL & "sendusing") = 2
			.Fields.Item(sConfURL & "smtpserver") = "localhost"
			.Fields.Item(sConfURL & "smtpserverport") = 25
			.Fields.Update
		 end with
		
		with oCdoMail
		  .From ="support@hbnext.com"
		  .To =  rs("email")'"rreynolds@nextnpdes.com"
		end with
	
		with oCdoMail
		  .Subject = appName & " - Open Items 7 day Alert - " & date()
		  .TextBody = sBody
		  .HTMLBody = sBody
		end with
		
		oCdoMail.Configuration = oCdoConf
		oCdoMail.Send
		Set oCdoConf = Nothing
		Set oCdoMail = Nothing
		
	'response.Write rs("email") & "<br>"
	'response.Write sBody & "<br><br>"
	'clear out the variable for the next user
	sBody = ""	
	rs.movenext
	loop		
		
	DataConn.close
	set DataConn = nothing
	
	overDueActionItems = True
end function

function deActivateProjects()
	
	On Error Resume Next
	
	Set DataConn = Server.CreateObject("ADODB.Connection") 
	DataConn.Open Session("Connection"), Session("UserID")
	
	'get the users that are able to recieve Open item alerts
	Set oCmd = CreateObject("ADODB.Command")	
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetEndingProjects"
	   .CommandType = adCmdStoredProc
	   
	End With
				
	Set rs = oCmd.Execute
	Set oCmd = nothing
	
	do until rs.eof
		'deactivate the project and send emails
		
		Set oCmd = Server.CreateObject("ADODB.Command")	
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetProject"
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, rs("projectID"))
		   .CommandType = adCmdStoredProc
		End With
				
		Set rsProject = oCmd.Execute
		
		Set oCmd = Server.CreateObject("ADODB.Command")	
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeActivateProject"
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, rs("projectID"))
		   .CommandType = adCmdStoredProc
		End With
				
		Set rsProj = oCmd.Execute
		
		
		Set oCmd = Server.CreateObject("ADODB.Command")	
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetAssignedUsersEmail"
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, rs("projectID"))
		   .CommandType = adCmdStoredProc
		End With
				
		Set rsEmail = oCmd.Execute
		
		sSubject = "Project De-activation - " & rsProject("projectName") & " - " & rsProject("customerName")
		sBody = "This email is notification that the above project is no longer active in HB NEXT as of " & rs("endDate") & ".<br>"
		sBody = sBody & "The project was automatically de-activated by the Sequence application based on the end date supplied.<br><br>"
		sBody = sBody & "If you have any questions about this project being de-activated please reply to this email.<br><br>"
		sBody = sBody & "Thanks<br>Customer Support<br>HB NEXT"
		
		do until rsEmail.eof
			'response.Write rsEmail("email") & "<br>"
			'send the email out
			'############################################################################################################
			Set oCdoMail = Server.CreateObject("CDO.Message")
			Set oCdoConf = Server.CreateObject("CDO.Configuration")
			
			sConfURL = "http://schemas.microsoft.com/cdo/configuration/"
			with oCdoConf
			  .Fields.Item(sConfURL & "sendusing") = 2
			  .Fields.Item(sConfURL & "smtpserver") = "localhost"
			  .Fields.Item(sConfURL & "smtpserverport") = 25
			  .Fields.Update
			end with
			
			with oCdoMail
			  .From = "support@hbnext.com"
			  .To = rsEmail("email")
			end with
			
			with oCdoMail
			  .Subject = sSubject
			  .TextBody = sBody
			  .HTMLBody = sBody
			end with
		
			oCdoMail.Configuration = oCdoConf
			oCdoMail.Send
			Set oCdoConf = Nothing
			Set oCdoMail = Nothing
	
			bSent = True			
			'############################################################################################################
		rsEmail.movenext
		loop
	rs.movenext
	loop
	
	deActivateProjects = true
end function

%>