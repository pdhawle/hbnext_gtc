<%
divisionID = Request("divisionID")
projectID = Request("job")
reportType = request("reportType")
Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

'get projects assigned
Set oCmd = Server.CreateObject("ADODB.Command")
		
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetEquipReportTypes"
   .CommandType = adCmdStoredProc   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsType = oCmd.Execute
Set oCmd = nothing

if reportType <> "" then

	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetAssignedDivisions"
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, session("ID"))
	   .CommandType = adCmdStoredProc
	   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
		
	Set rsDivisions = oCmd.Execute
	Set oCmd = nothing
end if


If divisionID <> "" then
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetAssignedProjectsByDivision"
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, divisionID)
	   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, session("ID"))
	   .CommandType = adCmdStoredProc   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
				
	Set rsProjects = oCmd.Execute
	Set oCmd = nothing
end if
%>
<script type="text/javascript">
	<!--
	function submitForm() {
		//form.asp?formType=addWorkOrder&projectID=8
	   addReport.action = 'form.asp?formType=addEquipReport';
	   addReport.submit(); 
	}


function addEvent(ival,ques)
{
var ni = document.getElementById("myDiv"+ival);
//alert (ni)
var numi = document.getElementById("theValue"+ival);
var num = (document.getElementById("theValue"+ival).value -1)+ 2;
numi.value = num;
var divIdName = "answer"+ival+num;
var newdiv = document.createElement('div');
newdiv.setAttribute("id",divIdName);
newdiv.innerHTML = "<table><tr><td>Comment "+ival+ "-" +num+"<br><input type=text name=answer_R"+ival+ "-" +num+"@"+ques+" size=40></td><td valign=bottom> <a href=\"javascript:;\" onclick=\"removeEvent(\'"+divIdName+"\','"+ival+"\')\">Remove Comment</a></td></tr></table>";
ni.appendChild(newdiv);

}

function removeEvent(divNum,ival)
{
//alert (divNum)
var d = document.getElementById("myDiv"+ival);
var olddiv = document.getElementById(divNum);
d.removeChild(olddiv);
}

// -->
</script>
<form name="addReport" method="post" action="process.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sNPDESTitle%></span><span class="Header"> - Add Report</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td><span class="required">*</span> <strong>Report Type:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												
												<select name="reportType" onchange="return submitForm()">
													<option value="">--select report type--</option>
													<%do until rsType.eof
														if trim(reportType) = trim(rsType("equipReportTypeID")) then%>
															<option selected="selected" value="<%=rsType("equipReportTypeID")%>"><%=rsType("equipReportType")%></option>
														<%else%>
															<option value="<%=rsType("equipReportTypeID")%>"><%=rsType("equipReportType")%></option>
													<%end if
													rsType.movenext
													loop%>
												</select>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<%if reportType <> "" then%>
											
											
											<!--this is where the inspection forms will be added-->	
											<%select case reportType%>
												<%case 1%>
														<tr>
															<td><span class="required">*</span> <strong>Customer/Division:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																
																<select name="divisionID" onchange="return submitForm()">
																<option>--Select a customer/Division--</option>
																<%do until rsDivisions.eof
																	If trim(rsDivisions("divisionID")) = trim(divisionID) then%>
																		<option selected="selected" value="<%=rsDivisions("divisionID")%>" ><%=rsDivisions("customerName")%> - <%=rsDivisions("division")%></option>
																	<%else%>
																		<option value="<%=rsDivisions("divisionID")%>" ><%=rsDivisions("customerName")%> - <%=rsDivisions("division")%></option>
																	<%end if
																rsDivisions.movenext
																loop%>
															</select>
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
												
													<%if divisionID <> "" then%>
															<tr>
															<td><span class="required">*</span> <strong>Job/Project:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																
																<select name="job">
																	<option value="">--select project--</option>
																	<%do until rsProjects.eof
																		if trim(projectID) = trim(rsProjects("projectID")) then%>
																			<option selected="selected" value="<%=rsProjects("projectID")%>"><%=rsProjects("projectName")%></option>
																		<%else%>
																			<option value="<%=rsProjects("projectID")%>"><%=rsProjects("projectName")%></option>
																	<%end if
																	rsProjects.movenext
																	loop%>
																</select>
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td><span class="required">*</span> <strong>Month:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<%iMonth = trim(request("month"))
																if iMonth = "" then
																	iMonth = trim(month(date))
																end if
																%>
																<select name="month">
																	<option value="1" <%=isSelected(1,trim(iMonth))%>>January</option>
																	<option value="2" <%=isSelected(2,trim(iMonth))%>>February</option>
																	<option value="3" <%=isSelected(3,trim(iMonth))%>>March</option>
																	<option value="4" <%=isSelected(4,trim(iMonth))%>>April</option>
																	<option value="5" <%=isSelected(5,trim(iMonth))%>>May</option>
																	<option value="6" <%=isSelected(6,trim(iMonth))%>>June</option>
																	<option value="7" <%=isSelected(7,trim(iMonth))%>>July</option>
																	<option value="8" <%=isSelected(8,trim(iMonth))%>>August</option>
																	<option value="9" <%=isSelected(9,trim(iMonth))%>>September</option>
																	<option value="10" <%=isSelected(10,trim(iMonth))%>>October</option>
																	<option value="11" <%=isSelected(11,trim(iMonth))%>>November</option>
																	<option value="12" <%=isSelected(12,trim(iMonth))%>>December</option>
																</select>&nbsp;&nbsp;
																<span class="required">*</span> <strong>Year:</strong>&nbsp;&nbsp;
																<%iYear = request("year")
																if iYear = "" then
																	iYear = year(date)
																end if
																%>
																<select name="year">
																	<option value="<%=year(date)-1%>" <%=isSelected(year(date)-1,iYear)%>><%=year(date)-1%></option>
																	<option value="<%=year(date)%>" <%=isSelected(year(date),iYear)%>><%=year(date)%></option>
																	<option value="<%=year(date)+1%>" <%=isSelected(year(date)+1,iYear)%>><%=year(date) + 1%></option>
																</select>
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td><span class="required">*</span> <strong>Serial #:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<input type="text" name="serialNumber" value="<%=request("serialNumber")%>" size="20" />
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td><strong>Operator:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<%=Session("Name")%>
																<input type="hidden" name="operator" value="<%=Session("ID")%>" />
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td><span class="required">*</span> <strong>Make/Model:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<input type="text" name="makeModel" value="<%=request("makeModel")%>" size="20" />
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td><strong>Address:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<%=Session("JobTitle")%>
																<input type="hidden" name="address" value="<%=Session("JobTitle")%>" />
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="15"></td></tr>
													</table>
													<!--#include file="inc_form_equipmentInspection.asp"-->	
													<%end if%>
													
												<%case 2%>
												
														<tr>
															<td><span class="required">*</span> <strong>Customer/Division:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																
																<select name="divisionID" onchange="return submitForm()">
																<option>--Select a customer/Division--</option>
																<%do until rsDivisions.eof
																	If trim(rsDivisions("divisionID")) = trim(divisionID) then%>
																		<option selected="selected" value="<%=rsDivisions("divisionID")%>" ><%=rsDivisions("customerName")%> - <%=rsDivisions("division")%></option>
																	<%else%>
																		<option value="<%=rsDivisions("divisionID")%>" ><%=rsDivisions("customerName")%> - <%=rsDivisions("division")%></option>
																	<%end if
																rsDivisions.movenext
																loop%>
															</select>
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
												
													<%if divisionID <> "" then%>	
														<tr>
															<td><span class="required">*</span> <strong>Job/Project:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																
																<select name="job">
																	<option value="">--select project--</option>
																	<%do until rsProjects.eof
																		if trim(projectID) = trim(rsProjects("projectID")) then%>
																			<option selected="selected" value="<%=rsProjects("projectID")%>"><%=rsProjects("projectName")%></option>
																		<%else%>
																			<option value="<%=rsProjects("projectID")%>"><%=rsProjects("projectName")%></option>
																	<%end if
																	rsProjects.movenext
																	loop%>
																</select>
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td><span class="required">*</span> <strong>Owner:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<input type="text" name="owner" size="30"value="<%=request("owner")%>" />
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td><span class="required">*</span> <strong>Owners Address:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<input type="text" name="ownersAddress" size="30"value="<%=request("ownersAddress")%>" />
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td><span class="required">*</span> <strong>Jobsite Location:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<input type="text" name="jobsiteLocation" size="30"value="<%=request("jobsiteLocation")%>" />
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td><span class="required">*</span> <strong>Crane Manufacturer:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<input type="text" name="craneManufacturer" size="30"value="<%=request("craneManufacturer")%>" />
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td><span class="required">*</span> <strong>Model Number:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<input type="text" name="modelNumber" size="5"value="<%=request("modelNumber")%>" />
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td><span class="required">*</span> <strong>Serial Number:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<input type="text" name="serialNumber" size="5"value="<%=request("serialNumber")%>" />
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td><span class="required">*</span> <strong>Jib Length:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<input type="text" name="jibLength" size="5"value="<%=request("jibLength")%>" />
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td><span class="required">*</span> <strong>Rated Capacity:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<input type="text" name="ratedCapacity" size="5"value="<%=request("ratedCapacity")%>" />
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td colspan="3"><strong>Test loads applied:</strong></td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td>&nbsp;</td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<table>
																	<tr>
																		<td>
																			<strong>Radius:</strong><br />
																			<input type="text" name="radius" size="5"value="<%=request("radius")%>" />
																		</td>
																		<td>
																			<strong>Proof load:</strong><br />
																			<input type="text" name="proofLoad" size="5"value="<%=request("proofLoad")%>" />
																		</td>
																		<td>
																			<strong>Rated load:</strong><br />
																			<input type="text" name="ratedLoad" size="5"value="<%=request("ratedLoad")%>" />
																		</td>
																	</tr>
																</table>
																
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td valign="top"><strong>Proof Load Desc:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<textarea name="proofLoadDesc" cols="30" rows="5"><%=request("proofLoadDesc")%></textarea>
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
														
														
														<tr>
															<td colspan="3"><strong><strong>LOAD MOMENTS TESTING</strong></strong></td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td>&nbsp;</td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<table>
																	<tr>
																		<td><strong>Tests performed</strong></td>
																		<td><strong>Weight</strong></td>
																		<td><strong>Radius</strong></td>
																		<td><strong>Observations</strong></td>
																	</tr>
																	<tr>
																		<td>90% Trolley Slow Down</td>
																		<td><input type="text" name="testWeight1" size="5" /></td>
																		<td><input type="text" name="testRadius1" size="5" /></td>
																		<td><input type="text" name="testObservation1" size="5" /></td>
																	</tr>
																	<tr>
																		<td>95% Horn</td>
																		<td><input type="text" name="testWeight2" size="5" /></td>
																		<td><input type="text" name="testRadius2" size="5" /></td>
																		<td><input type="text" name="testObservation2" size="5" /></td>
																	</tr>
																	<tr>
																		<td>100% Trolley Out Stop</td>
																		<td><input type="text" name="testWeight3" size="5" /></td>
																		<td><input type="text" name="testRadius3" size="5" /></td>
																		<td><input type="text" name="testObservation3" size="5" /></td>
																	</tr>
																	<tr>
																		<td>100% Hoisting Cut Off</td>
																		<td><input type="text" name="testWeight4" size="5" /></td>
																		<td><input type="text" name="testRadius4" size="5" /></td>
																		<td><input type="text" name="testObservation4" size="5" /></td>
																	</tr>
																</table>
																
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td colspan="3"><strong>Basis for assigned load ratings:</strong></td>
														</tr>
														<tr>
															<td>&nbsp;</td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<textarea name="loadRatingBasis" cols="30" rows="5"><%=request("loadRatingBasis")%></textarea>
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td colspan="3"><strong>Remarks and/or limitations imposed:</strong></td>
														</tr>
														<tr>
															<td>&nbsp;</td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<textarea name="limitationRemarks" cols="30" rows="5"><%=request("limitationRemarks")%></textarea>
															</td>
														</tr>
													</table>
													<!--#include file="inc_form_CraneExam.asp"-->	
													<%end if%>
												<%case else%>
													<tr>
														<td colspan="5">there are no reports for this selection</td>
													</tr>
													</table>
											<%end select%>																	
											<!--end of the inspection form-->
											
											<%if divisionID <> "" then%>
											<table width="830" border="0" cellpadding="0" cellspacing="0">	
												
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
												<tr>
													<td colspan="3" align="right">
														<input type="hidden" name="processType" value="addEquipReport" />
														<input type="submit" value="  Save  " class="formButton"/>
													</td>
												</tr>
											<%end if%>
											</table>
											
									<%else%>	
										</table>
									<%end if%>
								
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>

<script language="JavaScript" type="text/javascript">
<%if divisionID <> "" then
if reportType = 1 then%>
  var frmvalidator  = new Validator("addReport");
  frmvalidator.addValidation("serialNumber","req","Please enter a serial number");
  frmvalidator.addValidation("makeModel","req","Please enter the make/model");
<%end if
end if%>
</script>
