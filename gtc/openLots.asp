<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
Dim rs, oCmd, DataConn

projectID = Request("id")
divisionID = Request("divisionID")
customerID = Request("customerID")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetOpenLots"
	.parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, projectID)
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rs = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetProject"
   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
   .CommandType = adCmdStoredProc   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsInfo = oCmd.Execute
Set oCmd = nothing
%>

<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<script language="javascript" src="includes/datePicker.js"></script>
<link rel="stylesheet" href="includes/datePicker.css" type="text/css">
<link rel="stylesheet" href="includes/main.css" type="text/css">
</head>

<script>
<!-- Begin
var checkflag = "false";
function check(field)
{
 var i;
 if (eval(field[0].checked))
 {
  for (i=0;i<field.length;i++)
    field[i].checked=true;
  LL(field); 
  return "Uncheck All";
 } 
 else
 { 
   for(i=0;i<field.length;i++)
     field[i].checked=false;
   UU(field); 
   return "Check All";
 } 
}
function LL(field){field.disabled=true;}
function UU(field){field.disabled=false;}
function submitOrder(_form)
{
  var _msg  = "None Clicked:\n";
      _msg += "Air, Monitoring, Water, Pollution Prevention checkboxes have ";
      _msg += "been deselected, please select 'All' checkbox.\n";
  var anyClicked=false;
  for(var i=0;i<_form.list.length;i++)
    if(_form.list[i].checked)
	{
	  anyClicked=true;
	  _msg += "true*\n";
    }
	else
	{
	  _msg += "false*\n";
	}
  if(!anyClicked)
    alert(_msg)
//
  var _msg2  = "All Clicked:\n";
      _msg2 += "Air, Monitoring, Water, Pollution Prevention checkboxes have ";
      _msg2 += "been deselected, please select 'All' checkbox.\n";
  var allClicked2=true;
  for(var i=0;i<_form.list.length;i++)
    if(!_form.list[i].checked)
	{
	  allClicked2=false;
	  _msg2 += "false*\n";
    }
	else
	{
	  _msg2 += "true*\n";
	}
  if(allClicked2)
    alert(_msg2)
}


function confirmDelete(delUrl) {
 if (confirm("Are you sure you wish to delete this lot?")) {
    document.location = delUrl;
  }


}


function setDateClose(refID)
{
	var d = new Date();
	var curr_date = d.getDate();
	var curr_month = d.getMonth() + 1;
	var curr_year = d.getFullYear();

	document.getElementById('closedByDate'+refID).value = curr_month + "/" + curr_date + "/" + curr_year;

}
//  End -->
</script>
</head>
<body>
<form name="openLots" method="post" action="process.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Open Lots</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<a href="form.asp?formType=addLots&projectID=<%=projectID%>&divisionID=<%=divisionID%>&customerID=<%=customerID%>" class="footerLink">add lot inventory</a>&nbsp;&nbsp;
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="closedLots.asp?id=<%=projectID%>&divisionID=<%=divisionID%>&customerID=<%=customerID%>" class="footerLink">closed lots</a>&nbsp;&nbsp;
						<%if customerID <> "" then%>
							<span class="footerLink">|</span>&nbsp;&nbsp;<a href="projectList.asp?id=<%=divisionID%>&customerID=<%=customerID%>" class="footerLink">project list</a>&nbsp;&nbsp;
						<%else%>
							<span class="footerLink">|</span>&nbsp;&nbsp;<a href="javascript:window.close();" class="footerLink">close window</a>&nbsp;&nbsp;
						<%end if%>		
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr><td colspan="18">Project: <strong><%=rsInfo("projectName")%></strong></td></tr>
										<tr><td colspan="18"><img src="images/pix.gif" width="1" height="3"></td></tr>
										<tr bgcolor="#666666">
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td><span class="searchText">Lot #</span></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td><span class="searchText">Phase #</span></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td><span class="searchText">Open Date</span></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td align="center"><span class="searchText">Utility Locate Log</span></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td><span class="searchText">Close Lot</span></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td><span class="searchText">Date Closed</span></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td><span class="searchText">Closed By</span></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td align="center"><span class="searchText">Edit Lot</span></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td align="center"><span class="searchText">Delete Lot</span></td>
										</tr>
										<tr><td></td><td colspan="18"><img src="images/pix.gif" width="1" height="10"><!--<INPUT type="checkbox" name="closeItem"  value="Check All" onClick="this.value=check(this.form.closeItem);"> check all--></td></tr>
										<%If rs.eof then%>
											<tr><td></td><td colspan="18">there are no lots to display</td></tr>
										<%else
											blnChange = true
											Do until rs.eof
												If blnChange = true then%>
													<tr class="rowColor">
												<%else%>
													<tr>
												<%end if%>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td><%=rs("lotNumber")%></a></td>
													<td></td>
													<td><%=rs("phase")%></td>
													<td></td>
													<td><%=rs("openDate")%></td>
													<td></td>
													<td align="center">
														<%if isNull(rs("locateLogID")) or rs("locateLogID") = "" Then%>
															<a href="form.asp?formType=addUtility&lotID=<%=rs("lotID")%>&lotNumber=<%=rs("lotNumber")%>&projectName=<%=rsInfo("projectName")%>&customerID=<%=customerID%>&divisionID=<%=divisionID%>&projectID=<%=projectID%>&pg=ol">Add Entry</a>
														<%else%>
															<a href="viewUtility.asp?locateLogID=<%=rs("locateLogID")%>&customerID=<%=customerID%>&divisionID=<%=divisionID%>&projectID=<%=projectID%>&pg=ol">Edit/View Entry</a>
														<%End if%>
													</td>
													<td></td>
													<td><input type="checkbox" name="closeLot" value="<%=rs("lotID")%>" onClick="setDateClose(<%=rs("lotID")%>)"></td>
													<td></td>
													<td width="100"><input type="text" name="closedByDate<%=rs("lotID")%>" maxlength="10" size="8"/>&nbsp;<a href="javascript:displayDatePicker('closedByDate<%=rs("lotID")%>')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a></td>
													<td></td>
													<td>
														<%
														'Create command for contacts
														'brings in the contacts from the project section where they are set up
														Set oCmd = Server.CreateObject("ADODB.Command")
														
														With oCmd
														   .ActiveConnection = DataConn
														   .CommandText = "spGetContactsByProject"
															.parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
														   .CommandType = adCmdStoredProc
														   
														End With
														
															If Err Then
														%>
																<!--#include file="includes/FatalError.inc"-->
														<%
															End If
																	
														Set rsContact = oCmd.Execute
														Set oCmd = nothing
														%>
														<select name="closedBy<%=rs("lotID")%>">
																<option value="<%=session("Name")%>"><%=session("Name")%></option>
															<%do until rsContact.eof%>										
																<option value="<%=rsContact("firstName") & " " & rsContact("lastName")%>"><%=rsContact("firstName") & " " & rsContact("lastName")%></option>
															<%rsContact.movenext
															loop
															rsContact.close
															set rsContact = nothing%>
														</select>
													</td>
													<td></td>
													<td align="center"><a href="form.asp?formType=editLot&lotID=<%=rs("lotID")%>&projectID=<%=projectID%>&customerID=<%=customerID%>&divisionID=<%=divisionID%>"><img src="images/edit.gif" width="19" height="19" alt="Edit" border="0"></a></td>
													<td></td>
													<td align="center"><input type="image" src="images/remove.gif" width="11" height="13" alt="delete" border="0" onClick="return confirmDelete('process.asp?id=<%=rs("lotID")%>&processType=deleteLot&customerID=<%=customerID%>&divisionID=<%=divisionID%>&projectID=<%=projectID%>')"></td>
												</tr>
											<%
											'get the values from all the checkboxes
											sAllVal = sAllVal & rs("lotID")
											
											rs.movenext
											
											if not rs.eof then
												sAllVal = sAllVal & ","
											end if
											
											if blnChange = true then
												blnChange = false
											else
												blnChange = true
											end if
											
											
											
											loop
										end if%>
										<tr>
											<td colspan="18" align="right">
												<input type="hidden" name="projectID" value="<%=projectID%>">
												<input type="hidden" name="divisionID" value="<%=divisionID%>">
												<input type="hidden" name="customerID" value="<%=customerID%>">
												<input type="hidden" name="allBoxes" value="<%=sAllVal%>">
												<input type="hidden" name="processType" value="closeLots" />
												<br><input type="submit" value="Update" class="formButton"/>
											</td>
										</tr>
									</table>
									
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>