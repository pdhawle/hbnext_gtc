<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%

'
' This asp script demonstrates how to send a binary file 
' (such as .PDF, .DOC) from an ASP script, using MS SOAP 3.0
' and the SendfaxEx_2 method within Interfax (Sendfax is 
' commented out, but can be used as well).  As asp has no 
' provisions to reading binary files, we use an ADODB.Stream 
' object to do so.
'

'Option Explicit 
infoPackID = request("infoPackID")
customerID = request("customerID")
customerContactID = request("customerContactID")
'faxType = request("faxType")

'select case faxType
'	Case "Report" 'send the SWSIR report
		sServerName = server.MapPath("downloads/InfoPack_" & infoPackID & ".pdf")
		sSubject = "Info Pack #" & infoPackID
		docFaxed = "InfoPack_" & infoPackID & ".pdf"
'	case else 'send the quote report
'		sServerName = server.MapPath("downloads/quote_" & quoteID & ".pdf")
'		sSubject = "Quote#" & quoteID
'		docFaxed = "quote_" & quoteID & ".pdf"
'end select

sFax = request("faxNumber1") & request("faxNumber2") & request("faxNumber3")
sFaxNumber = "+" & request("countryCode") & sFax
sEmailResponse = Session("Email")
'Const 
FilenametoFax = sServerName

response.Write sFaxNumber & "<br>"


	Dim objSoap, SendFaxResult
	Dim B
	'
	'Read data from a file to a byte-array
	'
	B = ReadBinaryFile(FilenametoFax)
	'
	'Create the SoapClient object    'MS SOAP v3.0
	'
	Set objSoap = Server.CreateObject("MSSOAP.SOAPClient30")

	'Set to True when an Active Server Pages (ASP) application or an ISAPI DLL uses the SoapClient object.
	'
	objSoap.ClientProperty("ServerHTTPRequest") = True 
	'
	'Initializes the SoapClient object with the dfs.wsdl file
	'
	objSoap.mssoapinit("http://ws.interfax.net/dfs.asmx?WSDL")
	'
	'Set to True when a proxy server is to be detected automatically
	'
	objSoap.ConnectorProperty("EnableAutoProxy") = True
	'
	' Invoke the Interfax method
	'
	'SendFaxResult = objSoap.Sendfax("username", "password", "5480015", B , "txt")
	SendFaxResult = objSoap.SendfaxEx_2("sequence", "sequencefax", sFaxNumber, "", B, _
	"pdf", UBOUND(B)+1, Now, 3, "MyCSID", "", "", sSubject, sEmailResponse, _
	"Letter", "Landscape", False, True)
	
	
	'SendFaxResult = objSoap.Sendfax("sequence", "sequencefax", "+17065823133", base64_Text, "pdf")
	
	If CLng(SendFaxResult) > 0 Then 
		Response.Write "Fax submitted. Transaction ID: " & SendFaxResult
		
		'add the details of the fax transmission to the database
		'this will keep track of billing
		
		On Error Resume Next		
		Set DataConn = Server.CreateObject("ADODB.Connection")			
		DataConn.Open Session("Connection"), Session("UserID")
		Set oCmd = Server.CreateObject("ADODB.Command")
	
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddFax"
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, Session("ID"))
		   .parameters.Append .CreateParameter("@faxNumber", adVarChar, adParamInput, 50, sFaxNumber)
		   .parameters.Append .CreateParameter("@subject", adVarChar, adParamInput, 100, sSubject)
		   .parameters.Append .CreateParameter("@docFaxed", adVarChar, adParamInput, 100, docFaxed)
		   .parameters.Append .CreateParameter("@dateFaxed", adDBTimeStamp, adParamInput, 8, now())
		   .CommandType = adCmdStoredProc
		End With
				
		Set rs = oCmd.Execute		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		response.Redirect "email_sent.asp?type=fax"'&clientID=" & clientID & "&faxType=" & faxType
	Else 
		Response.Write "Error sending fax. Return code: " & SendFaxResult
	End If

'*********************************************************

Function ReadBinaryFile(FileName)
  Const adTypeBinary = 1
  
  'Create Stream object
  Dim BinaryStream
  Set BinaryStream = CreateObject("ADODB.Stream")
  
  'Specify stream type - we want To get binary data.
  BinaryStream.Type = adTypeBinary
  
  'Open the stream
  BinaryStream.Open
  
  'Load the file data from disk To stream object
  BinaryStream.LoadFromFile FileName
  
  'Open the stream And get binary data from the object
  ReadBinaryFile = BinaryStream.Read

  'Clean up
  BinaryStream.Close
  Set BinaryStream = Nothing
End Function

 



%>