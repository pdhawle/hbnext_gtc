<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
iUser = request("inspector")
dtFrom = request("fromDate")
dtTo = request("toDate")
'sSort = request("sortBy")

ssort = request("sort")
if ssort = "" then
	columnName = "projectName"
else
	columnName = ssort
end if



If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetAgentRecByDate"
   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, iUser)
   .parameters.Append .CreateParameter("@fromDate", adDBTimeStamp, adParamInput, 8, dtFrom)
   .parameters.Append .CreateParameter("@toDate", adDBTimeStamp, adParamInput, 8, dtTo)
   '.parameters.Append .CreateParameter("@columnName", adVarchar, adParamInput, 50, columnName)
   .CommandType = adCmdStoredProc   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsReport = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetUser"
   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, iUser)
   .CommandType = adCmdStoredProc   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsUser = oCmd.Execute
Set oCmd = nothing
%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Inspector Reconciliation</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<!--<a href="expInspActResults.asp?inspector=<%'=iUser%>&fromDate=<%'=dtFrom%>&toDate=<%'=dtTo%>" class="footerLink">Export Data to Excel</a>&nbsp;&nbsp;
						<span class="footerLink">|</span>&nbsp;&nbsp;--><a href="chooseInspRec.asp" class="footerLink">back to selections</a>			
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0">
										<tr>
											<td></td>
											<td colspan="20">
												Agent: <strong><%=rsUser("firstName")%>&nbsp;<%=rsUser("lastName")%></strong><br>
												Date Range: <strong><%=dtFrom%> - <%=dtTo%></strong>
											</td>
										</tr>
										<tr>
											<td colspan="3"></td>
											<td colspan="5" align="center" bgcolor="#666666"><span class="searchText"><strong>Report Type</strong></span></td>
										</tr>
										<tr bgcolor="#666666" height="35">
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td><span class="searchText">Customer</span></td>
											<td><span class="searchText"><!--<a href="InspRecResults.asp?inspector=<%=iUser%>&fromDate=<%=dtFrom%>&toDate=<%=dtTo%>&sort=projectName" style="color:#721A32;text-decoration:underline;font-weight:bold;">-->Project<!--</a>--></span></td>
											<td align="center"><span class="searchText">Weekly</span></td>
											<td align="center"><span class="searchText">Post Rainfall</span></td>
											<td align="center"><span class="searchText">Daily</span></td>
											<td align="center"><span class="searchText">Monthly</span></td>
											<td align="center"><span class="searchText">Water Sampling</span></td>
										</tr>
								
										<%If rsReport.eof then %>
												<tr><td></td><td colspan="9">there are no reports to display</td></tr>
											<%else
												blnChange = false
												dim i
												i = 0
												Do until rsReport.eof
													If blnChange = true then%>
														<tr class="rowColor">
													<%else%>
														<tr>
													<%end if%>
														<td></td>
														<td><%=rsReport("customerName")%></td>
														<td><%=rsReport("projectName")%></td>
														<td align="center">
															<%=rsReport("numberWeeklyReports")%>
															<%iWeekTotal = iWeekTotal + rsReport("numberWeeklyReports")%>
														</td>
														<td align="center">
															<%=rsReport("numberPRReports")%>
															<%iPRTotal = iPRTotal + rsReport("numberPRReports")%>
														</td>
														<td align="center">
															<%=rsReport("numberDailyReports")%>
															<%iDailyTotal = iDailyTotal + rsReport("numberDailyReports")%>
														</td>
														<td align="center">
															<%=rsReport("numberMonthlyReports")%>
															<%iMonthlyTotal = iMonthlyTotal + rsReport("numberMonthlyReports")%>
														</td>
														<td align="center">
															<%=rsReport("numberWaterReports")%>
															<%iWaterTotal = iWaterTotal + rsReport("numberWaterReports")%>
														</td>
													</tr>
												<%
												
												rsReport.movenext
												if blnChange = true then
													blnChange = false
												else
													blnChange = true
												end if
												i = i + 1
												loop%>
											
											<%end if%>
											<tr><td colspan="8"><img src="images/pix.gif" width="1" height="10"></td></tr>
											<tr>
												<td colspan="3" align="right"><strong>Totals:</strong></td>
												<td align="center"><strong><%=iWeekTotal%></strong></td>
												<td align="center"><strong><%=iPRTotal%></strong></td>
												<td align="center"><strong><%=iDailyTotal%></strong></td>
												<td align="center"><strong><%=iMonthlyTotal%></strong></td>
												<td align="center"><strong><%=iWaterTotal%></strong></td>
											</tr>
										
									</table>
															
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>
<%
'rsCustomer.close
rsReport.close
DataConn.close
Set rsCustomer = nothing
Set rsReport = nothing
Set DataConn = nothing
Set oCmd = nothing
%>