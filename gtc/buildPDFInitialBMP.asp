<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%'need customerID, reportID

reportID = request("reportID")

Session("LoggedIn") = "True"

Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetReportByID"
    .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, reportID) 'reportID
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsReport = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetInitialBMPReport"
    .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, reportID) 'reportID
   .CommandType = adCmdStoredProc
   
End With
			
Set rs = oCmd.Execute
Set oCmd = nothing

%>
<html>
<head>
<title>SWSIR</title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/report.css" type="text/css">
</head>
<body>

<table width=750 cellpadding=0 cellspacing=0>
	<tr>
		<td valign=top colspan="2">
			<table width="750" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td valign="middle">
						<%if rsReport("logo") = "" then%>
							<img src="images/logo.jpg">
						<%else%>
							<img src="<%=rsReport("logo")%>">
						<%end if%>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=10></td></tr>
	<tr>
		<td colspan=2>
			<table width=750>
				<tr>
					<td colspan="3" align="center"><span style="font-size:16px;font-weight:bold;">BMP Inspection By Licensed Design Professional</span></td>
				</tr>
				<tr><td colspan=3><img src=images/pix.gif width=1 height=10></td></tr>
				<tr>
					<td valign="top">
						<b>Project Name:</b> <%=rsReport("projectName")%><br>
												
					</td>
					<td></td>
					<td valign="top">
						<b>Project Number:</b> <%=rsReport("projectNumber")%><br>
					</td>
				</tr>
				<tr>
					<td valign="top" colspan="3">
						<b>Land Disturbance Activity Start Date:</b> <%=rsReport("startDate")%><br>
	
					</td>
				</tr>
				<tr>
					<td valign="top" colspan="3">
						<b>Inspection Date:</b> <%=rsReport("inspectionDate")%><br>
	
					</td>
				</tr>
			</table>
			
		</td>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=20></td></tr>
	<tr>
		<td colspan=2>
			<table width="750" cellpadding="3" cellspacing="0" border="0">
				<tr>
					<td colspan="3">
						I, <u><%=rsReport("firstName")%>&nbsp;<%=rsReport("lastName")%></u> of Georgia Transmission Corporation, am the licensed professional that 
						designed the Erosion, Sedimentation, and Pollution Control Plan (Plan).<br><br>
						
						<!--
						I, <u><%=rsReport("firstName")%>&nbsp;<%=rsReport("lastName")%></u>, <u><%=rs("siteInspected")%></u> inspected the <u><%=rsReport("projectName")%></u> project site, within seven days of<br><br>
						-->
						
						<%if rs("perimControlBMP") = True then%>
							<img src="images/checked.gif">
						<%else%>
							<img src="images/checkbox.gif">
						<%end if%>&nbsp;Perimeter Control BMPs - Installation Date&nbsp;<u><%=rs("perimControlBMPInstallDate")%></u><br><br>
						
						<%if rs("initialSedimentStorage") = True then%>
							<img src="images/checked.gif">
						<%else%>
							<img src="images/checkbox.gif">
						<%end if%>&nbsp;Initial sediment storage requirements - Installation Date&nbsp;<u><%=rs("initialSedimentStorageInstallDate")%></u><br><br>
						
						<%if rs("sedimentStoragePerimControlBMP") = True then%>
							<img src="images/checked.gif">
						<%else%>
							<img src="images/checkbox.gif">
						<%end if%>&nbsp;Sediment storage requirements and perimeter Control BMPs for the initial segment of the linear infrastructure project - Installation Date&nbsp;<u><%=rs("sedimentStoragePerimControlBMPInstallDate")%></u><br><br>
					
						<%if rs("allSedimentBasins") = True then%>
							<img src="images/checked.gif">
						<%else%>
							<img src="images/checkbox.gif">
						<%end if%>&nbsp;All sediment basins within the entire linear infrastructure - Installation Date&nbsp;<u><%=rs("allSedimentBasinsInstallDate")%></u><br><br>
						
						<br><br>
						
						
						<%if rs("inspectionOccurred7") = True then%>
							<img src="images/checked.gif">
						<%else%>
							<img src="images/checkbox.gif">
						<%end if%>&nbsp;This inspection occurred with  seven(7) days of the date checked above.<br><br>
						
						
						<%if rs("bmpInstalledmaintained") = True then%>
							<img src="images/checked.gif">
						<%else%>
							<img src="images/checkbox.gif">
						<%end if%>&nbsp;The Best Management Practices have been installed and are being maintained as designed;<br>
						<u><%=rs("bmpInstalledmaintainedComments")%></u><br><br>
						
						<%if rs("actionItems") = True then%>
							<img src="images/checked.gif">
						<%else%>
							<img src="images/checkbox.gif">
						<%end if%>&nbsp;The following action items were identified regarding the installation and maintenance of Best Management Practices;<br>
						<u><%=rs("actionItemComments")%></u><br><br>
						
					</td>
				</tr>
				
				<tr>
					<td valign="bottom">
						<u><%=rsReport("firstName")%>&nbsp;<%=rsReport("lastName")%></u><br>
						<strong>Name</strong>
					</td>
					<td valign="bottom">
						<%if rsReport("sigFileName") <> "" then%> 
							<u><img src="userUploads/<%=rsReport("userID")%>/<%=rsReport("sigFileName")%>"></u>
						<%else%>
							<u>__________________________</u> 
						<%end if%><br>
						<strong>Signature</strong>
						
					</td>
					<td valign="bottom">
						<u><%=rsReport("inspectionDate")%></u><br>
						<strong>Date</strong>
					</td>
				</tr>
				<tr>
					<td></td>
					<td><img src="images/certSeal.gif"></td>
					<td></td>
				</tr>
				
				
			</table><br>

		</td>
	</tr>
</table>
</body>
</html>