<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->
<%
bLoggedInInfo = "False"
appLogo = request("appLogo")
refer = request("refer")
msg = request("msg")

clientID = request("clientID")

if appLogo <> "" then
	Session("appLogo") = appLogo
end if

Dim rsState, oCmd, DataConn

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

'Create command for state list
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetStates"
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsState = oCmd.Execute
Set oCmd = nothing
%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script language="JavaScript" src="includes/validator.js" type="text/javascript"></script>
<script language="javascript" src="includes/datePicker.js"></script>
<link rel="stylesheet" href="includes/datePicker.css" type="text/css">
</head>
<body>
<form name="addUser" method="post" action="process.asp">
<input type="hidden" name="refer" value="<%=refer%>">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sLoginTitle%></span><span class="Header"> - New User</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td colspan="3">
												Please register to become a new user. All fields are required.<br>
												<%=msg%><br>
											</td>
										</tr>
										<tr>
											<td colspan="3"><strong><u>ABOUT YOU</u></strong></td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td><strong>First Name:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="firstName" size="30" value="<%=request("firstName")%>" tooltipText="Enter your first name in this box.">
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td><strong>Last Name:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="lastName" size="30" value="<%=request("lastName")%>" tooltipText="Enter your last name in this box.">
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td><strong>Company Name:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="company" size="30" value="<%=request("company")%>" tooltipText="Enter the name of your company in this box.">
											</td>
										</tr>	
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td><strong>Job Title:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="jobtitle" size="30" value="<%=request("jobtitle")%>" tooltipText="Enter your job title in this box.">
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td><strong>WECS Cert. #:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="WECSNo" size="10" value="<%=request("WECSNo")%>">&nbsp;&nbsp;<strong>Exp. Date</strong>&nbsp;&nbsp;<input type="text" name="WECSExpDate" maxlength="10" size="10" value="<%=request("WECSExpDate")%>"/>&nbsp;<a href="javascript:displayDatePicker('WECSExpDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td><strong>GASWCC Level 1-A Cert. #:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="GASWCCNo" size="10" value="<%=request("GASWCCNo")%>">&nbsp;&nbsp;<strong>Exp. Date</strong>&nbsp;&nbsp;<input type="text" name="GASWCCExpDate" maxlength="10" size="10" value="<%=request("GASWCCExpDate")%>"/>&nbsp;<a href="javascript:displayDatePicker('GASWCCExpDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top"><strong>Other Certifications:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<textarea name="otherCerts" cols="22" rows="3"><%=request("otherCerts")%></textarea>
											</td>
										</tr>
										
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td><strong>State:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="state" tooltipText="Select the state you will be working in.">
													<%do until rsState.eof%>
														<option value="<%=rsState("stateID")%>"><%=rsState("stateName")%></option>
													<%rsState.movenext
													loop
													rsState.moveFirst%>
												</select>
											</td>
										</tr>	
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td><strong>Office Number:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="officePhone" size="30" value="<%=request("officePhone")%>" tooltipText="Enter your office phone in this box.">
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td><strong>Cell Number:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="cellPhone" size="30" value="<%=request("cellPhone")%>" tooltipText="Enter your cell phone in this box. This is the number that will be printed out on all reports.">
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td><strong>Email Address:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="email" size="30" tooltipText="Enter your email address in this box. All emails to you will be sent to this address."> (will be your username)
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>	
										<tr>
											<td><strong>Password:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="password" name="password" size="30" tooltipText="Enter your desired password in this box. Please write this down for future use.">
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>	
										<tr>
											<td><strong>Password Hint:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="password" name="passwordHint" size="30">
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td><strong>User Type/Industry:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="industry">
													<option selected="selected" value="">Please Select...</option>
													<option value="Customer">Customer</option>
													<option value="DOT Contractor">DOT Contractor</option>
													<option value="Erosion Control Contractor">Erosion Control Contractor</option>
													<option value="Customer">Engineering</option>
													<option value="General Contractor">General Contractor</option>													
													<option value="Land Developer">Land Developer</option>
													<option value="NPDES Inspector">NPDES Inspector</option>
													<option value="Residential Builder">Residential Builder</option>
													<option value="Safety Inspector">Safety Inspector</option>
													<option value="Utility Contractor">Utility Contractor</option>
													<option value="Other">Other</option>
												</select>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td><strong>Interest:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="interest">
													<option selected="selected" value="">Please Select...</option>
													<option value="NOI On Demand">NOI on Demand</option>
													<option value="NPDES On Demand">NPDES on Demand</option>
													<option value="BMP On Demand">BMP on Demand</option>
													<option value="General Inquiry">General Inquiry</option>
													<option value="Product Demo">Product Demo</option>
													<option value="30 Day Trial">30 Day Trial</option>
													<option value="Web Demo">Web Demo</option>
													<option value="Partnership">Partnership</option>
												</select>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
										<!--<tr>
											<td colspan="3"><strong>Check the products you are interested in:</strong></td>
										</tr>
										<tr>
											<td colspan="3"><input type="checkbox" name="NPDESManager">&nbsp;NPDES Manager</td>
										</tr>
										<tr>
											<td colspan="3"><input type="checkbox" name="NOIManager">&nbsp;NOI Manager</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>-->
										<tr>
											<td colspan="3"><strong>Please list the projects/customers <br>you will be working with:</strong></td>
										</tr>
										<tr>
											<td colspan="3">
												<textarea name="associatedProjects" rows="4" cols="25"  tooltipText="Enter any project or customer names that you may be working with. This will give us an idea of what projects to assign you to. We may need more info from you, so we may need to contact you."><%=request("associatedProjects")%></textarea>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										
										<!--<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
										<tr>
											<td colspan="3"><strong><u>YOUR COMPANY</u></strong></td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td><strong>Company Name:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="company" size="30" value="<%=request("company")%>" tooltipText="Enter the name of your company in this box.">
											</td>
										</tr>	
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>-->
										<!--<tr>
											<td><strong>State:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="state" tooltipText="Select the state you will be working in.">
													<%'do until rsState.eof%>
														<option value="<%'=rsState("stateID")%>"><%'=rsState("stateName")%></option>
													<%'rsState.movenext
													'loop
													'rsState.moveFirst%>
												</select>
											</td>
										</tr>	
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td colspan="3"><strong><u>LOGIN INFO</u></strong></td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td><strong>Username:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="userName" size="30" tooltipText="Enter your desired user name in this box. Please write this down for future use.">
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>	
										<tr>
											<td><strong>Password:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="password" name="password" size="30" tooltipText="Enter your desired password in this box. Please write this down for future use.">
											</td>
										</tr>-->
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>				
										<tr>
											<td colspan="3" align="right">
												<input type="hidden" name="clientID" value="<%=clientID%>" />
												<input type="hidden" name="processType" value="addUser" />
												<input type="submit" value="Register" class="formButton"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
									</table>
									
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<script language="JavaScript" type="text/javascript">

  var frmvalidator  = new Validator("addUser");
  frmvalidator.addValidation("firstName","req","Please enter your first name");
  frmvalidator.addValidation("lastName","req","Please enter your last name"); 
  frmvalidator.addValidation("jobtitle","req","Please enter your job title");
  frmvalidator.addValidation("email","req","Please enter your email address");
  frmvalidator.addValidation("email","email");
  frmvalidator.addValidation("officePhone","req","Please enter your office phone number");
  frmvalidator.addValidation("cellPhone","req","Please enter your cell phone number");
  frmvalidator.addValidation("associatedProjects","req","Please enter your projects/customers");
  frmvalidator.addValidation("company","req","Please enter your company's name");  
  //frmvalidator.addValidation("userName","req","Please enter a user name");
  frmvalidator.addValidation("password","req","Please enter a password");
</script>
</body>
</html>
<%
rsState.close
rsCountry.close
set rsState = nothing
set rsCountry = nothing
DataConn.close
set DataConn = nothing
%>