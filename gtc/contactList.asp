<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
projectID = trim(Request("project"))

Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If


On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetAssignedProjects"
   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, session("ID"))
   .CommandType = adCmdStoredProc   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsProjects = oCmd.Execute
Set oCmd = nothing

If projectID <> "" then
	'Create command
	'may need to add user to this because may want to be only for specific user
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetContactsByProject"
	   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, projectID)
	   .CommandType = adCmdStoredProc   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
				
	Set rs = oCmd.Execute
	Set oCmd = nothing
	
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetProject"
	   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, projectID)
	   .CommandType = adCmdStoredProc   
	End With
				
	Set rsProject = oCmd.Execute
	Set oCmd = nothing
end if
%>

<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script language="JavaScript">
<!--
function confirmDelete(delUrl) {
 if (confirm("Are you sure you wish to delete this contact?")) {
    document.location = delUrl;
  }


}

function dept_onchange(contactList) {
   contactList.submit(); 
}
//-->
</script>
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sLoginTitle%></span><span class="Header"> - Project Contact List</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;
						<%if projectID <> "" then%>
							<a href="form.asp?formType=addContact&id=<%=projectID%>" class="footerLink">add new contact</a>&nbsp;&nbsp;
							<span class="footerLink">|</span>&nbsp;&nbsp;<a href="importContacts.asp?projectID=<%=projectID%>&importFrom=project" class="footerLink">import contact(s) from another project</a>&nbsp;&nbsp;
							<span class="footerLink">|</span>&nbsp;&nbsp;<a href="importContacts.asp?projectID=<%=projectID%>&importFrom=userList" class="footerLink">import contact(s) from user list</a>&nbsp;&nbsp;
						<%end if%>			
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td></td>
											<td colspan="8">
											<form name="contactList" method="post" action="contactList.asp">
												<select name="project" onChange="return dept_onchange(contactList)">
													<option value="">--Select Project--</option>
													<%do until rsProjects.eof
													if trim(rsProjects("projectID")) = trim(projectID) then%>
														<option selected="selected" value="<%=rsProjects("projectID")%>"><%=rsProjects("customerName")%> - <%=rsProjects("projectName")%></option>
													<%else%>
														<option value="<%=rsProjects("projectID")%>"><%=rsProjects("customerName")%> - <%=rsProjects("projectName")%></option>
													<%end if
													rsProjects.movenext
													loop%>
												</select>
											</form>
											</td>
										</tr>
										<tr><td colspan="8"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<%If projectID <> "" then%>
										<tr>
											<td colspan="8">
												<strong>Company: </strong><%=rsProject("customerName")%><br>
												<strong>Division: </strong><%=rsProject("division")%><br>
												<strong>Project: </strong><%=rsProject("projectName")%>
											</td>
										</tr>
										<tr><td colspan="8"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<%end if%>
										<tr>
											<td colspan="8">
												<%if projectID <> "" then
													Set oCmd = Server.CreateObject("ADODB.Command")
		
													With oCmd
													   .ActiveConnection = DataConn
													   .CommandText = "spGetProject"
													   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, projectID)
													   .CommandType = adCmdStoredProc   
													End With
														
														If Err Then
													%>
															<!--#include file="includes/FatalError.inc"-->
													<%
														End If
																
													Set rsProject = oCmd.Execute
													Set oCmd = nothing
													
													If rsProject("autoEmail") = "True" then
														Response.Write "Auto email is turned on for this project"
													else
														Response.Write "Auto email is <b>NOT</b> turned on for this project"
													end if
													
												end if%>
											</td>
										</tr>
										
										<tr bgcolor="#666666">
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td><span class="searchText">Contact Name</span></td>
											<td><span class="searchText">Email Address</span></td>
											<td><span class="searchText">Company Name</span></td>	
											<td><span class="searchText">Gets Auto Email</span></td>					
											<td align="center"><span class="searchText">Edit</span></td>
											<td align="center"><span class="searchText">Delete</span></td>
										</tr>
										<tr><td colspan="9"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<%If rs.eof then%>
											<tr><td></td><td colspan="9">there are no records to display</td></tr>
										<%else
											blnChange = true
											Do until rs.eof
												If blnChange = true then%>
													<tr class="rowColor">
												<%else%>
													<tr>
												<%end if%>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td><%=rs("firstName")%>&nbsp;<%=rs("lastName")%></td>							
													<td><a href="mailto:<%=rs("email")%>"><%=rs("email")%></a></td>
													<td><%=rs("companyName")%></td>
													<td align="center">
														<%if rs("getAutoEmail") = "True" then%>
															<img src="images/check.gif">
														<%end if%>
													</td>
													<td align="center"><a href="form.asp?id=<%=rs("contactID")%>&formType=editContact&projectID=<%=projectID%>"><img src="images/edit.gif" width="19" height="19" alt="edit" border="0"></a></td>
													<td align="center"><a href=""><input type="image" src="images/remove.gif" width="11" height="13" alt="delete" border="0" onClick="return confirmDelete('process.asp?id=<%=rs("contactID")%>&processType=deleteContact&projectID=<%=projectID%>')"></a></td>
												</tr>
											<%rs.movenext
											if blnChange = true then
												blnChange = false
											else
												blnChange = true
											end if
											loop
										end if%>
										
										<%if request("imported")="True" then%>
										<tr>
											<td colspan="10">
												<font color="#EE0000">The contacts have been imported.</font>
											</td>
										</tr>
										<%end if%>
									</table>
									
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>