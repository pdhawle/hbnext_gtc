<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->
<%
dim sType, rs
Dim arrX
Dim arrY
Dim arrText
Dim pdf
Dim Doc
sType = Request("processType")

select case stype
	case "addNOT"
		
		projectID = request("projectID")
		primaryFormID = request("primaryFormID")
		coverageDesired= checkNull(request("coverageDesired"))
		siteProjectName= checkNull(request("siteProjectName"))
		GPSDegree1= checkNull(request("degree1"))
		GPSMinute1= checkNull(request("minute1"))
		GPSSecond1= checkNull(request("second1"))
		GPSLat =  checkNull(request("latitude"))
		GPSDegree2= checkNull(request("degree2"))
		GPSMinute2= checkNull(request("minute2"))
		GPSSecond2= checkNull(request("second2"))
		GPSLon =  checkNull(request("longitude"))
		projectAddress= checkNull(request("projectAddress"))
		projectCity= checkNull(request("projectCity"))
		projectCounty= checkNull(request("projectCounty"))
		subdivision= checkNull(request("subdivision"))
		lotNumber= checkNull(request("lotNumber"))
		ownerName= checkNull(request("ownerName"))
		ownerAddress= checkNull(request("ownerAddress"))
		ownerCity= checkNull(request("ownerCity"))
		ownerState= checkNull(request("ownerState"))
		ownerZip= checkNull(request("ownerZip"))
		operatorName= checkNull(request("operatorName"))
		operatorPhone= checkNull(request("operatorPhone"))
		operatorAddress= checkNull(request("operatorAddress"))
		operatorCity= checkNull(request("operatorCity"))
		operatorState= checkNull(request("operatorState"))
		operatorZip= checkNull(request("operatorZip"))
		typePrimary= checkNull(request("typePrimary"))
		typeSecondary= checkNull(request("typeSecondary"))
		typeTertiary= checkNull(request("typeTertiary"))
		facilityContact= checkNull(request("facilityContact"))
		facilityContactPhone= checkNull(request("facilityContactPhone"))
		primaryPermitteeName= checkNull(request("primaryPermitteeName"))
		primaryPermitteePhone= checkNull(request("primaryPermitteePhone"))
		primaryPermitteeAddress= checkNull(request("primaryPermitteeAddress"))
		primaryPermitteeCity= checkNull(request("primaryPermitteeCity"))
		primaryPermitteeState= checkNull(request("primaryPermitteeState"))
		primaryPermitteeZip= checkNull(request("primaryPermitteeZip"))
		secondaryPermittees= checkNull(request("secondaryPermittees"))		
		constructionActivityCompleted= checkNull(request("constructionActivityCompleted"))
		noLongerOwnerOperator= checkNull(request("noLongerOwnerOperator"))		
		typeConstructionCommercial= checkNull(request("typeConstructionCommercial"))
		typeConstructionIndustrial= checkNull(request("typeConstructionIndustrial"))
		typeConstructionMunicipal= checkNull(request("typeConstructionMunicipal"))
		typeConstructionDOT= checkNull(request("typeConstructionDOT"))
		typeConstructionUtility= checkNull(request("typeConstructionUtility"))
		typeConstructionResidential= checkNull(request("typeConstructionResidential"))		
		primaryPermitteeSubdivision= checkNull(request("primaryPermitteeSubdivision"))
		individualLot= checkNull(request("individualLot"))
		individualLotWithinSWDA= checkNull(request("individualLotWithinSWDA"))		
		IRWName= checkNull(request("IRWName"))
		MSSSOwnerOperator= checkNull(request("MSSSOwnerOperator"))
		RWName= checkNull(request("RWName"))
		certify1= checkNull(request("certify1"))
		certify2= checkNull(request("certify2"))
		ownerPrintedName= checkNull(request("ownerPrintedName"))
		ownerTitle= checkNull(request("ownerTitle"))
		ownerSignDate= checkNull(request("ownerSignDate"))
		operatorPrintedName= checkNull(request("operatorPrintedName"))
		operatorTitle= checkNull(request("operatorTitle"))
		operatorSignDate= checkNull(request("operatorSignDate"))
		
		'new fields added for 2008
		commonDevelopmentName= checkNull(request("commonDevelopmentName"))
		constructionStreetAddress= checkNull(request("constructionStreetAddress"))
		authorizedRep= checkNull(request("authorizedRep"))
		authorizedRepPhone= checkNull(request("authorizedRepPhone"))
		primaryPermitteeName2= checkNull(request("primaryPermitteeName2"))
		primaryPermitteePhone2= checkNull(request("primaryPermitteePhone2"))
		primaryPermitteeAddress2= checkNull(request("primaryPermitteeAddress2"))
		primaryPermitteeCity2= checkNull(request("primaryPermitteeCity2"))
		primaryPermitteeState2= checkNull(request("primaryPermitteeState2"))
		primaryPermitteeZip2= checkNull(request("primaryPermitteeZip2"))
		primaryPermitteeName3= checkNull(request("primaryPermitteeName3"))
		primaryPermitteePhone3= checkNull(request("primaryPermitteePhone3"))
		primaryPermitteeAddress3= checkNull(request("primaryPermitteeAddress3"))
		primaryPermitteeCity3= checkNull(request("primaryPermitteeCity3"))
		primaryPermitteeState3= checkNull(request("primaryPermitteeState3"))
		primaryPermitteeZip3= checkNull(request("primaryPermitteeZip3"))
		attach1= checkNull(request("attach1"))
		attach2= checkNull(request("attach2"))
		startDate= checkNull(request("startDate"))
		completionDate= checkNull(request("completionDate"))
		estimatedDisturbedAcerage= checkNull(request("estimatedDisturbedAcerage"))
	
		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If					
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddNOT"
		   .parameters.Append .CreateParameter("@primaryFormID", adInteger, adParamInput, 8, primaryFormID)
		   .parameters.Append .CreateParameter("@coverageDesired", adInteger, adParamInput, 8, coverageDesired)
		   .parameters.Append .CreateParameter("@siteProjectName", adVarChar, adParamInput, 200, siteProjectName)
		   .parameters.Append .CreateParameter("@GPSDegree1", adVarChar, adParamInput, 50, GPSDegree1)
		   .parameters.Append .CreateParameter("@GPSMinute1", adVarChar, adParamInput, 50, GPSMinute1)
		   .parameters.Append .CreateParameter("@GPSSecond1", adVarChar, adParamInput, 50, GPSSecond1)
		   .parameters.Append .CreateParameter("@GPSLat", adVarChar, adParamInput, 50, GPSLat)
		   .parameters.Append .CreateParameter("@GPSDegree2", adVarChar, adParamInput, 50, GPSDegree2)
		   .parameters.Append .CreateParameter("@GPSMinute2", adVarChar, adParamInput, 50, GPSMinute2)
		   .parameters.Append .CreateParameter("@GPSSecond2", adVarChar, adParamInput, 50, GPSSecond2)
		   .parameters.Append .CreateParameter("@GPSLon", adVarChar, adParamInput, 50, GPSLon)
		   .parameters.Append .CreateParameter("@projectAddress", adVarChar, adParamInput, 100, projectAddress)
		   .parameters.Append .CreateParameter("@projectCity", adVarChar, adParamInput, 100, projectCity)
		   .parameters.Append .CreateParameter("@projectCounty", adVarChar, adParamInput, 100, projectCounty)
		   .parameters.Append .CreateParameter("@subdivision", adVarChar, adParamInput, 150, subdivision)
		   .parameters.Append .CreateParameter("@lotNumber", adVarChar, adParamInput, 150, lotNumber)
		   .parameters.Append .CreateParameter("@ownerName", adVarChar, adParamInput, 100, ownerName)		   
		   .parameters.Append .CreateParameter("@ownerAddress", adVarChar, adParamInput, 100, ownerAddress)
		   .parameters.Append .CreateParameter("@ownerCity", adVarChar, adParamInput, 100, ownerCity)
		   .parameters.Append .CreateParameter("@ownerState", adVarChar, adParamInput, 50, ownerState)
		   .parameters.Append .CreateParameter("@ownerZip", adVarChar, adParamInput, 50, ownerZip)
		   .parameters.Append .CreateParameter("@operatorName", adVarChar, adParamInput, 100, operatorName)
		   .parameters.Append .CreateParameter("@operatorPhone", adVarChar, adParamInput, 50, operatorPhone)
		   .parameters.Append .CreateParameter("@operatorAddress", adVarChar, adParamInput, 100, operatorAddress)
		   .parameters.Append .CreateParameter("@operatorCity", adVarChar, adParamInput, 100, operatorCity)
		   .parameters.Append .CreateParameter("@operatorState", adVarChar, adParamInput, 50, operatorState)
		   .parameters.Append .CreateParameter("@operatorZip", adVarChar, adParamInput, 50, operatorZip)
		   .parameters.Append .CreateParameter("@typePrimary", adBoolean, adParamInput, 1, isOn(typePrimary))
		   .parameters.Append .CreateParameter("@typeSecondary", adBoolean, adParamInput, 1, isOn(typeSecondary))
		   .parameters.Append .CreateParameter("@typeTertiary", adBoolean, adParamInput, 1, isOn(typeTertiary))		   
		   .parameters.Append .CreateParameter("@facilityContact", adVarChar, adParamInput, 100, facilityContact)
		   .parameters.Append .CreateParameter("@facilityContactPhone", adVarChar, adParamInput, 50, facilityContactPhone)		   
		   .parameters.Append .CreateParameter("@primaryPermitteeName", adVarChar, adParamInput, 100, primaryPermitteeName)
		   .parameters.Append .CreateParameter("@primaryPermitteePhone", adVarChar, adParamInput, 50, primaryPermitteePhone)
		   .parameters.Append .CreateParameter("@primaryPermitteeAddress", adVarChar, adParamInput, 100, primaryPermitteeAddress)
		   .parameters.Append .CreateParameter("@primaryPermitteeCity", adVarChar, adParamInput, 100, primaryPermitteeCity)
		   .parameters.Append .CreateParameter("@primaryPermitteeState", adVarChar, adParamInput, 50, primaryPermitteeState)
		   .parameters.Append .CreateParameter("@primaryPermitteeZip", adVarChar, adParamInput, 50, primaryPermitteeZip)
		   .parameters.Append .CreateParameter("@secondaryPermittees", adVarChar, adParamInput, 50, secondaryPermittees)	   
		   .parameters.Append .CreateParameter("@constructionActivityCompleted", adBoolean, adParamInput, 1, isOn(constructionActivityCompleted))
		   .parameters.Append .CreateParameter("@noLongerOwnerOperator", adBoolean, adParamInput, 1, isOn(noLongerOwnerOperator))
		   .parameters.Append .CreateParameter("@typeConstructionCommercial", adBoolean, adParamInput, 1, isOn(typeConstructionCommercial))
		   .parameters.Append .CreateParameter("@typeConstructionIndustrial", adBoolean, adParamInput, 1, isOn(typeConstructionIndustrial))
		   .parameters.Append .CreateParameter("@typeConstructionMunicipal", adBoolean, adParamInput, 1, isOn(typeConstructionMunicipal))
		   .parameters.Append .CreateParameter("@typeConstructionDOT", adBoolean, adParamInput, 1, isOn(typeConstructionDOT))
		   .parameters.Append .CreateParameter("@typeConstructionUtility", adBoolean, adParamInput, 1, isOn(typeConstructionUtility))
		   .parameters.Append .CreateParameter("@typeConstructionResidential", adBoolean, adParamInput, 1, isOn(typeConstructionResidential))		   
		   .parameters.Append .CreateParameter("@primaryPermitteeSubdivision", adBoolean, adParamInput, 1, isOn(primaryPermitteeSubdivision))
		   .parameters.Append .CreateParameter("@individualLot", adBoolean, adParamInput, 1, isOn(individualLot))
		   .parameters.Append .CreateParameter("@individualLotWithinSWDA", adBoolean, adParamInput, 1, isOn(individualLotWithinSWDA))		   
		   .parameters.Append .CreateParameter("@IRWName", adVarChar, adParamInput, 100, IRWName)
		   .parameters.Append .CreateParameter("@MSSSOwnerOperator", adVarChar, adParamInput, 100, MSSSOwnerOperator)
		   .parameters.Append .CreateParameter("@RWName", adVarChar, adParamInput, 100, RWName)		   
		   .parameters.Append .CreateParameter("@certify1", adVarChar, adParamInput, 50, certify1)
		   .parameters.Append .CreateParameter("@certify2", adVarChar, adParamInput, 50, certify2)
		   .parameters.Append .CreateParameter("@ownerPrintedName", adVarChar, adParamInput, 100, ownerPrintedName)
		   .parameters.Append .CreateParameter("@ownerTitle", adVarChar, adParamInput, 100, ownerTitle)
		   .parameters.Append .CreateParameter("@ownerSignDate", adDBTimeStamp, adParamInput, 8, ownerSignDate)
		   .parameters.Append .CreateParameter("@operatorPrintedName", adVarChar, adParamInput, 100, operatorPrintedName)
		   .parameters.Append .CreateParameter("@operatorTitle", adVarChar, adParamInput, 100, operatorTitle)
		   .parameters.Append .CreateParameter("@operatorSignDate", adDBTimeStamp, adParamInput, 8, operatorSignDate)
		   'new fields added for 2008
		   .parameters.Append .CreateParameter("@commonDevelopmentName", adVarChar, adParamInput, 100, commonDevelopmentName)
		   .parameters.Append .CreateParameter("@constructionStreetAddress", adVarChar, adParamInput, 100, constructionStreetAddress)
		   .parameters.Append .CreateParameter("@authorizedRep", adVarChar, adParamInput, 100, authorizedRep)
		   .parameters.Append .CreateParameter("@authorizedRepPhone", adVarChar, adParamInput, 50, authorizedRepPhone)
		   .parameters.Append .CreateParameter("@primaryPermitteeName2", adVarChar, adParamInput, 100, primaryPermitteeName2)
		   .parameters.Append .CreateParameter("@primaryPermitteePhone2", adVarChar, adParamInput, 50, primaryPermitteePhone2)
		   .parameters.Append .CreateParameter("@primaryPermitteeAddress2", adVarChar, adParamInput, 100, primaryPermitteeAddress2)
		   .parameters.Append .CreateParameter("@primaryPermitteeCity2", adVarChar, adParamInput, 100, primaryPermitteeCity2)
		   .parameters.Append .CreateParameter("@primaryPermitteeState2", adVarChar, adParamInput, 50, primaryPermitteeState2)
		   .parameters.Append .CreateParameter("@primaryPermitteeZip2", adVarChar, adParamInput, 50, primaryPermitteeZip2)
		   .parameters.Append .CreateParameter("@primaryPermitteeName3", adVarChar, adParamInput, 100, primaryPermitteeName3)
		   .parameters.Append .CreateParameter("@primaryPermitteePhone3", adVarChar, adParamInput, 50, primaryPermitteePhone3)
		   .parameters.Append .CreateParameter("@primaryPermitteeAddress3", adVarChar, adParamInput, 100, primaryPermitteeAddress3)
		   .parameters.Append .CreateParameter("@primaryPermitteeCity3", adVarChar, adParamInput, 100, primaryPermitteeCity3)
		   .parameters.Append .CreateParameter("@primaryPermitteeState3", adVarChar, adParamInput, 50, primaryPermitteeState3)
		   .parameters.Append .CreateParameter("@primaryPermitteeZip3", adVarChar, adParamInput, 50, primaryPermitteeZip3)
		   .parameters.Append .CreateParameter("@attach1", adBoolean, adParamInput, 1, isOn(attach1))
		   .parameters.Append .CreateParameter("@attach2", adBoolean, adParamInput, 1, isOn(attach2))
		   .parameters.Append .CreateParameter("@startDate", adDBTimeStamp, adParamInput, 8, startDate)
		   .parameters.Append .CreateParameter("@completionDate", adDBTimeStamp, adParamInput, 8, completionDate)
		   .parameters.Append .CreateParameter("@estimatedDisturbedAcerage", adVarChar, adParamInput, 50, estimatedDisturbedAcerage)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If

		NOTID = rs("Identity")
		
		Set oCmd = nothing
		rs.close
		Set rs = nothing
		
		
		'create the PDF using the above report ID
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildNOTGAPDF.asp?NOTID=" & NOTID', "landscape=true"
		Filename = Doc.Save( Server.MapPath("downloads/NOI/noticeOfTermination_" & primaryFormID & ".pdf"), True )
		
		'***********create the pdf***********************************
'		select case coverageDesired
'			case 1
'				x = 73
'				y = 611
'			case 2
'				x = 223
'				y = 611
'			case 3
'				x = 397
'				y = 611
			
'		end select
		
'		GPSDegree1= request("degree1")
'		GPSMinute1= request("minute1")
'		GPSSecond1= request("second1")
'		GPSLat =  request("latitude")
'		GPSDegree2= request("degree2")
'		GPSMinute2= request("minute2")
'		GPSSecond2= request("second2")
'		GPSLon =  request("longitude")
		'�
'		GPSLocation = GPSDegree1 & "&deg; " & GPSMinute1 & "' " & GPSSecond1 & "&quot; " & GPSLat & " / " & GPSDegree2 & "&deg; " & GPSMinute2 & "' " & GPSSecond2 & "&quot; " & GPSLon

		'create the PDF and store it in the downloads folder
		'page 1	
'		arrX = Array(500,x,171,246,243,100,377,170,437,153,121,322,437,523,166,480,124,322,437,523,217,325,433,157,443,209,443,121,282,363,492,252,72,289,181,289,361,433,505,181,289,289,289,252,363,252) 'left to right
'		arrY = Array(775,y,563,543,524,505,505,487,487,470,450,450,450,450,430,430,410,410,410,410,388,388,388,367,367,329,329,310,310,310,310,293,251,251,231,231,231,231,231,211,211,191,170,131,112,93) 'up and down
'		arrText = Array(primaryFormID,"x",siteProjectName,GPSLocation,projectAddress,projectCity,projectCounty,subdivision,lotNumber,ownerName,ownerAddress, _
'			ownerCity,ownerState,ownerZip,operatorName,operatorPhone,operatorAddress,operatorCity,operatorState,operatorZip,setx(typePrimary),setx(typeSecondary), _
'			setx(typeTertiary),facilityContact,facilityContactPhone,primaryPermitteeName,primaryPermitteePhone,primaryPermitteeAddress,primaryPermitteeCity, _
'			primaryPermitteeState,primaryPermitteeZip,secondaryPermittees,setx(constructionActivityCompleted),setx(noLongerOwnerOperator),setx(typeConstructionCommercial), _
'			setx(typeConstructionIndustrial),setx(typeConstructionMunicipal),setx(typeConstructionDOT),setx(typeConstructionUtility),setx(typeConstructionResidential), _
'			setx(primaryPermitteeSubdivision),setx(individualLot),setx(individualLotWithinSWDA),IRWName,MSSSOwnerOperator,RWName)
		
		'page2	
'		arrX2 = Array(72,72,187,425,427,200,425,427) 'left to right
'		arrY2 = Array(680,432,295,295,270,232,232,207) 'up and down
'		arrText2 = Array(certify1,certify2,ownerPrintedName,ownerTitle,ownerSignDate,operatorPrintedName,operatorTitle,operatorSignDate)
		
'		Set Pdf = Server.CreateObject("Persits.Pdf")
'		Pdf.RegKey = sRegKey
	
		' Open blank PDF form from file
'		Set Doc = Pdf.OpenDocument( Server.MapPath("pdfTemplates/noticeOfTermination.pdf") )
	
		' Create default font
'		Set Font = Doc.Fonts("Helvetica")
	
		' Obtain the only page's canvas
'		Set Canvas = Doc.Pages(1).Canvas
'		Set Canvas2 = Doc.Pages(2).Canvas
	
		' Create empty param object
'		Set Param = Pdf.CreateParam	
'		Set Param2 = Pdf.CreateParam	
	
		' Go over all items in arrays
'		For j = 0 to UBound(arrX)
	
'			Param("x") = arrX(j)
'			Param("y") = arrY(j) - 263 * i
'			Param("color") = "black"
'			Param("html") = true
'			' Draw text on canvas
'			Page.ResetCoordinates
'			Canvas.DrawText arrText(j), Param, Font
	
'		Next ' j
		
'		j = 0
'		For j = 0 to UBound(arrX2)
'			Param2("x") = arrX2(j)
'			Param2("y") = arrY2(j) - 263 * i
'			Param2("color") = "black"
'			Param("html") = true
'			' Draw text on canvas
'			Page.ResetCoordinates
'			Canvas2.DrawText arrText2(j), Param2, Font
'		Next
		
		' Save document, the Save method returns generated file name
'		Filename = Doc.Save( Server.MapPath("downloads/NOI/noticeOfTermination_" & primaryFormID & ".pdf"), True )
		
		'***********end editing the pdf***********************************

		
		response.Redirect "primaryList.asp?projectID=" & projectID

	case "editNOT"
	
		projectID = request("projectID")
	
		notFormID = request("notFormID")
		primaryFormID = request("primaryFormID")
		coverageDesired= request("coverageDesired")
		siteProjectName= request("siteProjectName")
		GPSDegree1= request("degree1")
		GPSMinute1= request("minute1")
		GPSSecond1= request("second1")
		GPSLat =  request("latitude")
		GPSDegree2= request("degree2")
		GPSMinute2= request("minute2")
		GPSSecond2= request("second2")
		GPSLon =  request("longitude")
		projectAddress= request("projectAddress")
		projectCity= request("projectCity")
		projectCounty= request("projectCounty")
		subdivision= request("subdivision")
		lotNumber= request("lotNumber")
		ownerName= request("ownerName")
		ownerAddress= request("ownerAddress")
		ownerCity= request("ownerCity")
		ownerState= request("ownerState")
		ownerZip= request("ownerZip")
		operatorName= request("operatorName")
		operatorPhone= request("operatorPhone")
		operatorAddress= request("operatorAddress")
		operatorCity= request("operatorCity")
		operatorState= request("operatorState")
		operatorZip= request("operatorZip")
		typePrimary= request("typePrimary")
		typeSecondary= request("typeSecondary")
		typeTertiary= request("typeTertiary")		
		facilityContact= request("facilityContact")
		facilityContactPhone= request("facilityContactPhone")
		primaryPermitteeName= request("primaryPermitteeName")
		primaryPermitteePhone= request("primaryPermitteePhone")
		primaryPermitteeAddress= request("primaryPermitteeAddress")
		primaryPermitteeCity= request("primaryPermitteeCity")
		primaryPermitteeState= request("primaryPermitteeState")
		primaryPermitteeZip= request("primaryPermitteeZip")
		secondaryPermittees= request("secondaryPermittees")		
		constructionActivityCompleted= request("constructionActivityCompleted")
		noLongerOwnerOperator= request("noLongerOwnerOperator")		
		typeConstructionCommercial= request("typeConstructionCommercial")
		typeConstructionIndustrial= request("typeConstructionIndustrial")
		typeConstructionMunicipal= request("typeConstructionMunicipal")
		typeConstructionDOT= request("typeConstructionDOT")
		typeConstructionUtility= request("typeConstructionUtility")
		typeConstructionResidential= request("typeConstructionResidential")		
		primaryPermitteeSubdivision= request("primaryPermitteeSubdivision")
		individualLot= request("individualLot")
		individualLotWithinSWDA= request("individualLotWithinSWDA")		
		IRWName= request("IRWName")
		MSSSOwnerOperator= request("MSSSOwnerOperator")
		RWName= request("RWName")
		certify1= request("certify1")
		certify2= request("certify2")
		ownerPrintedName= request("ownerPrintedName")
		ownerTitle= request("ownerTitle")
		ownerSignDate= request("ownerSignDate")
		operatorPrintedName= request("operatorPrintedName")
		operatorTitle= request("operatorTitle")
		operatorSignDate= request("operatorSignDate")
		
		
		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditNOT"
		   .parameters.Append .CreateParameter("@notFormID", adInteger, adParamInput, 8, notFormID)
		   .parameters.Append .CreateParameter("@coverageDesired", adInteger, adParamInput, 8, coverageDesired)
		   .parameters.Append .CreateParameter("@siteProjectName", adVarChar, adParamInput, 200, siteProjectName)
		   .parameters.Append .CreateParameter("@GPSDegree1", adVarChar, adParamInput, 50, GPSDegree1)
		   .parameters.Append .CreateParameter("@GPSMinute1", adVarChar, adParamInput, 50, GPSMinute1)
		   .parameters.Append .CreateParameter("@GPSSecond1", adVarChar, adParamInput, 50, GPSSecond1)
		   .parameters.Append .CreateParameter("@GPSLat", adVarChar, adParamInput, 50, GPSLat)
		   .parameters.Append .CreateParameter("@GPSDegree2", adVarChar, adParamInput, 50, GPSDegree2)
		   .parameters.Append .CreateParameter("@GPSMinute2", adVarChar, adParamInput, 50, GPSMinute2)
		   .parameters.Append .CreateParameter("@GPSSecond2", adVarChar, adParamInput, 50, GPSSecond2)
		   .parameters.Append .CreateParameter("@GPSLon", adVarChar, adParamInput, 50, GPSLon)
		   .parameters.Append .CreateParameter("@projectAddress", adVarChar, adParamInput, 100, projectAddress)
		   .parameters.Append .CreateParameter("@projectCity", adVarChar, adParamInput, 100, projectCity)
		   .parameters.Append .CreateParameter("@projectCounty", adVarChar, adParamInput, 100, projectCounty)
		   .parameters.Append .CreateParameter("@subdivision", adVarChar, adParamInput, 150, subdivision)
		   .parameters.Append .CreateParameter("@lotNumber", adVarChar, adParamInput, 150, lotNumber)
		   .parameters.Append .CreateParameter("@ownerName", adVarChar, adParamInput, 100, ownerName)		   
		   .parameters.Append .CreateParameter("@ownerAddress", adVarChar, adParamInput, 100, ownerAddress)
		   .parameters.Append .CreateParameter("@ownerCity", adVarChar, adParamInput, 100, ownerCity)
		   .parameters.Append .CreateParameter("@ownerState", adVarChar, adParamInput, 50, ownerState)
		   .parameters.Append .CreateParameter("@ownerZip", adVarChar, adParamInput, 50, ownerZip)
		   .parameters.Append .CreateParameter("@operatorName", adVarChar, adParamInput, 100, operatorName)
		   .parameters.Append .CreateParameter("@operatorPhone", adVarChar, adParamInput, 50, operatorPhone)
		   .parameters.Append .CreateParameter("@operatorAddress", adVarChar, adParamInput, 100, operatorAddress)
		   .parameters.Append .CreateParameter("@operatorCity", adVarChar, adParamInput, 100, operatorCity)
		   .parameters.Append .CreateParameter("@operatorState", adVarChar, adParamInput, 50, operatorState)
		   .parameters.Append .CreateParameter("@operatorZip", adVarChar, adParamInput, 50, operatorZip)
		   .parameters.Append .CreateParameter("@typePrimary", adBoolean, adParamInput, 1, isOn(typePrimary))
		   .parameters.Append .CreateParameter("@typeSecondary", adBoolean, adParamInput, 1, isOn(typeSecondary))
		   .parameters.Append .CreateParameter("@typeTertiary", adBoolean, adParamInput, 1, isOn(typeTertiary))		   
		   .parameters.Append .CreateParameter("@facilityContact", adVarChar, adParamInput, 100, facilityContact)
		   .parameters.Append .CreateParameter("@facilityContactPhone", adVarChar, adParamInput, 50, facilityContactPhone)		   
		   .parameters.Append .CreateParameter("@primaryPermitteeName", adVarChar, adParamInput, 100, primaryPermitteeName)
		   .parameters.Append .CreateParameter("@primaryPermitteePhone", adVarChar, adParamInput, 50, primaryPermitteePhone)
		   .parameters.Append .CreateParameter("@primaryPermitteeAddress", adVarChar, adParamInput, 100, primaryPermitteeAddress)
		   .parameters.Append .CreateParameter("@primaryPermitteeCity", adVarChar, adParamInput, 100, primaryPermitteeCity)
		   .parameters.Append .CreateParameter("@primaryPermitteeState", adVarChar, adParamInput, 50, primaryPermitteeState)
		   .parameters.Append .CreateParameter("@primaryPermitteeZip", adVarChar, adParamInput, 50, primaryPermitteeZip)
		   .parameters.Append .CreateParameter("@secondaryPermittees", adVarChar, adParamInput, 50, secondaryPermittees)	   
		   .parameters.Append .CreateParameter("@constructionActivityCompleted", adBoolean, adParamInput, 1, isOn(constructionActivityCompleted))
		   .parameters.Append .CreateParameter("@noLongerOwnerOperator", adBoolean, adParamInput, 1, isOn(noLongerOwnerOperator))
		   .parameters.Append .CreateParameter("@typeConstructionCommercial", adBoolean, adParamInput, 1, isOn(typeConstructionCommercial))
		   .parameters.Append .CreateParameter("@typeConstructionIndustrial", adBoolean, adParamInput, 1, isOn(typeConstructionIndustrial))
		   .parameters.Append .CreateParameter("@typeConstructionMunicipal", adBoolean, adParamInput, 1, isOn(typeConstructionMunicipal))
		   .parameters.Append .CreateParameter("@typeConstructionDOT", adBoolean, adParamInput, 1, isOn(typeConstructionDOT))
		   .parameters.Append .CreateParameter("@typeConstructionUtility", adBoolean, adParamInput, 1, isOn(typeConstructionUtility))
		   .parameters.Append .CreateParameter("@typeConstructionResidential", adBoolean, adParamInput, 1, isOn(typeConstructionResidential))		   
		   .parameters.Append .CreateParameter("@primaryPermitteeSubdivision", adBoolean, adParamInput, 1, isOn(primaryPermitteeSubdivision))
		   .parameters.Append .CreateParameter("@individualLot", adBoolean, adParamInput, 1, isOn(individualLot))
		   .parameters.Append .CreateParameter("@individualLotWithinSWDA", adBoolean, adParamInput, 1, isOn(individualLotWithinSWDA))		   
		   .parameters.Append .CreateParameter("@IRWName", adVarChar, adParamInput, 100, IRWName)
		   .parameters.Append .CreateParameter("@MSSSOwnerOperator", adVarChar, adParamInput, 100, MSSSOwnerOperator)
		   .parameters.Append .CreateParameter("@RWName", adVarChar, adParamInput, 100, RWName)		   
		   .parameters.Append .CreateParameter("@certify1", adVarChar, adParamInput, 50, certify1)
		   .parameters.Append .CreateParameter("@certify2", adVarChar, adParamInput, 50, certify2)
		   .parameters.Append .CreateParameter("@ownerPrintedName", adVarChar, adParamInput, 100, ownerPrintedName)
		   .parameters.Append .CreateParameter("@ownerTitle", adVarChar, adParamInput, 100, ownerTitle)
		   .parameters.Append .CreateParameter("@ownerSignDate", adDBTimeStamp, adParamInput, 8, ownerSignDate)
		   .parameters.Append .CreateParameter("@operatorPrintedName", adVarChar, adParamInput, 100, operatorPrintedName)
		   .parameters.Append .CreateParameter("@operatorTitle", adVarChar, adParamInput, 100, operatorTitle)
		   .parameters.Append .CreateParameter("@operatorSignDate", adDBTimeStamp, adParamInput, 8, operatorSignDate)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		Set oCmd = nothing
		rs.close
		Set rs = nothing
		
		'create the PDF using the above report ID
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildNOTGAPDF.asp?NOTID=" & notFormID', "landscape=true"
		Filename = Doc.Save( Server.MapPath("downloads/NOI/noticeOfTermination_" & primaryFormID & ".pdf"), True )
		
		'***********edit the pdf***********************************
'		select case coverageDesired
'			case 1
'				x = 73
'				y = 611
'			case 2
'				x = 223
'				y = 611
'			case 3
'				x = 397
'				y = 611
'			
'		end select
'		
'		GPSDegree1= request("degree1")
'		GPSMinute1= request("minute1")
'		GPSSecond1= request("second1")
'		GPSLat =  request("latitude")
'		GPSDegree2= request("degree2")
'		GPSMinute2= request("minute2")
'		GPSSecond2= request("second2")
'		GPSLon =  request("longitude")
'		'�
'		GPSLocation = GPSDegree1 & "&deg; " & GPSMinute1 & "' " & GPSSecond1 & "&quot; " & GPSLat & " / " & GPSDegree2 & "&deg; " & GPSMinute2 & "' " & GPSSecond2 & "&quot; " & GPSLon
'
'		
'		'create the PDF and store it in the downloads folder
'		'page 1	
'		arrX = Array(450,x,171,246,243,100,377,170,437,153,121,322,437,523,166,480,124,322,437,523,217,325,433,157,443,209,443,121,282,363,492,252,72,289,181,289,361,433,505,181,289,289,289,252,363,252) 'left to right
'		arrY = Array(775,y,563,543,524,505,505,487,487,470,450,450,450,450,430,430,410,410,410,410,388,388,388,367,367,329,329,310,310,310,310,293,251,251,231,231,231,231,231,211,211,191,170,131,112,93) 'up and down
'		arrText = Array(primaryFormID,"x",siteProjectName,GPSLocation,projectAddress,projectCity,projectCounty,subdivision,lotNumber,ownerName,ownerAddress, _
'			ownerCity,ownerState,ownerZip,operatorName,operatorPhone,operatorAddress,operatorCity,operatorState,operatorZip,setx(typePrimary),setx(typeSecondary), _
'			setx(typeTertiary),facilityContact,facilityContactPhone,primaryPermitteeName,primaryPermitteePhone,primaryPermitteeAddress,primaryPermitteeCity, _
'			primaryPermitteeState,primaryPermitteeZip,secondaryPermittees,setx(constructionActivityCompleted),setx(noLongerOwnerOperator),setx(typeConstructionCommercial), _
'			setx(typeConstructionIndustrial),setx(typeConstructionMunicipal),setx(typeConstructionDOT),setx(typeConstructionUtility),setx(typeConstructionResidential), _
'			setx(primaryPermitteeSubdivision),setx(individualLot),setx(individualLotWithinSWDA),IRWName,MSSSOwnerOperator,RWName)
''		
'		'page2	
'		arrX2 = Array(72,72,187,425,427,200,425,427) 'left to right
'		arrY2 = Array(680,432,295,295,270,232,232,207) 'up and down
'		arrText2 = Array(certify1,certify2,ownerPrintedName,ownerTitle,ownerSignDate,operatorPrintedName,operatorTitle,operatorSignDate)
'		
'		Set Pdf = Server.CreateObject("Persits.Pdf")
'		Pdf.RegKey = sRegKey
'	
		' Open blank PDF form from file
'		Set Doc = Pdf.OpenDocument( Server.MapPath("pdfTemplates/noticeOfTermination.pdf") )
	
		' Create default font
'		Set Font = Doc.Fonts("Helvetica")
	
		' Obtain the only page's canvas
'		Set Canvas = Doc.Pages(1).Canvas
'		Set Canvas2 = Doc.Pages(2).Canvas
	
		' Create empty param object
'		Set Param = Pdf.CreateParam	
'		Set Param2 = Pdf.CreateParam	
	
		' Go over all items in arrays
'		For j = 0 to UBound(arrX)
	
'			Param("x") = arrX(j)
'			Param("y") = arrY(j) - 263 * i
'			Param("color") = "black"
'			Param("html") = true
'			' Draw text on canvas
'			Page.ResetCoordinates
'			Canvas.DrawText arrText(j), Param, Font
'	
'		Next ' j
		
'		j = 0
'		For j = 0 to UBound(arrX2)
'			Param2("x") = arrX2(j)
'			Param2("y") = arrY2(j) - 263 * i
'			Param2("color") = "black"
'			Param("html") = true
'			' Draw text on canvas
'			Page.ResetCoordinates
'			Canvas2.DrawText arrText2(j), Param2, Font
'		Next
'		
'		' Save document, the Save method returns generated file name
'		Filename = Doc.Save( Server.MapPath("downloads/NOI/noticeOfTermination_" & primaryFormID & ".pdf"), True )
'		
		'***********end editing the pdf***********************************
		
		response.Redirect "primaryList.asp?projectID=" & projectID

	case "addPrimary"
		
		projectID = checkNull(request("projectID"))
		coverageDesired= checkNull(request("coverageDesired"))
		siteProjectName= checkNull(request("siteProjectName"))
		GPSDegree1= checkNull(request("degree1"))
		GPSMinute1= checkNull(request("minute1"))
		GPSSecond1= checkNull(request("second1"))
		GPSLat =  checkNull(request("latitude"))
		GPSDegree2= checkNull(request("degree2"))
		GPSMinute2= checkNull(request("minute2"))
		GPSSecond2= checkNull(request("second2"))
		GPSLon =  checkNull(request("longitude"))
		projectAddress= checkNull(request("projectAddress"))
		projectCity= checkNull(request("projectCity"))
		projectCounty= checkNull(request("projectCounty"))
		subdivision= checkNull(request("subdivision"))
		ownerName= checkNull(request("ownerName"))
		ownerAddress= checkNull(request("ownerAddress"))
		ownerCity= checkNull(request("ownerCity"))
		ownerState= checkNull(request("ownerState"))
		ownerZip= checkNull(request("ownerZip"))
		operatorName= checkNull(request("operatorName"))
		operatorPhone= checkNull(request("operatorPhone"))
		operatorAddress= checkNull(request("operatorAddress"))
		operatorCity= checkNull(request("operatorCity"))
		operatorState= checkNull(request("operatorState"))
		operatorZip= checkNull(request("operatorZip"))
		facilityContact= checkNull(request("facilityContact"))
		facilityContactPhone= checkNull(request("facilityContactPhone"))
		startDate= checkNull(request("startDate"))
		completionDate= checkNull(request("completionDate"))
		estimatedDisturbedAcerage= checkNull(request("estimatedDisturbedAcerage"))
		typeConstructionCommercial= checkNull(request("typeConstructionCommercial"))
		typeConstructionIndustrial= checkNull(request("typeConstructionIndustrial"))
		typeConstructionMunicipal= checkNull(request("typeConstructionMunicipal"))
		typeConstructionLinear= checkNull(request("typeConstructionLinear"))
		typeConstructionUtility= checkNull(request("typeConstructionUtility"))
		typeConstructionResidential= checkNull(request("typeConstructionResidential"))
		secondaryPermittees= checkNull(request("secondaryPermittees"))
		IRWName= checkNull(request("IRWName"))
		IRWTrout= checkNull(request("IRWTrout"))
		IRWWarmWater= checkNull(request("IRWWarmWater"))
		MSSSOwnerOperator= checkNull(request("MSSSOwnerOperator"))
		RWName= checkNull(request("RWName"))
		RWTrout= checkNull(request("RWTrout"))
		RWWarmWater= checkNull(request("RWWarmWater"))
		samplingOfOutfall= checkNull(request("samplingOfOutfall"))
		samplingOfRecievingStream= checkNull(request("samplingOfRecievingStream"))
		troutStream= checkNull(request("troutStream"))
		numberOfOutfalls= checkNull(request("numberOfOutfalls"))
		appendixBNTUValue= checkNull(request("appendixBNTUValue"))
		SWDrainageArea= checkNull(request("SWDrainageArea"))
		locationMap= checkNull(request("locationMap"))
		ESPControlPlan= checkNull(request("ESPControlPlan"))
		knownSecondaryPermittees= checkNull(request("knownSecondaryPermittees"))
		timingSchedule= checkNull(request("timingSchedule"))
		certify1= checkNull(request("certify1"))
		certify2= checkNull(request("certify2"))
		certify3= checkNull(request("certify3"))
		ownerPrintedName= checkNull(request("ownerPrintedName"))
		ownerTitle= checkNull(request("ownerTitle"))
		ownerSignDate= checkNull(request("ownerSignDate"))
		operatorPrintedName= checkNull(request("operatorPrintedName"))
		operatorTitle= checkNull(request("operatorTitle"))
		operatorSignDate= checkNull(request("operatorSignDate"))
		
		'new fields 09/2008
		noticeOfIntent= checkNull(request("noticeOfIntent"))
		authorizedRep= checkNull(request("authorizedRep"))
		authorizedRepPhone= checkNull(request("authorizedRepPhone"))
		disturb50= checkNull(request("disturb50"))
		troutStreamRecieve= checkNull(request("troutStreamRecieve"))
		warmWaterRecieve= checkNull(request("warmWaterRecieve"))
		warmWater= checkNull(request("warmWater"))
		constructionSiteSize= checkNull(request("constructionSiteSize"))
		siteDischargeStormWater= checkNull(request("siteDischargeStormWater"))
		siteDischargeStormWater2= checkNull(request("siteDischargeStormWater2"))
		impairedStream= checkNull(request("impairedStream"))
		impairedStream2= checkNull(request("impairedStream2"))
		writtenAuth= checkNull(request("writtenAuth"))
	
		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If					
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddPrimaryNOI"
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
		   .parameters.Append .CreateParameter("@coverageDesired", adInteger, adParamInput, 8, coverageDesired)
		   .parameters.Append .CreateParameter("@siteProjectName", adVarChar, adParamInput, 200, siteProjectName)
		   .parameters.Append .CreateParameter("@GPSDegree1", adVarChar, adParamInput, 50, GPSDegree1)
		   .parameters.Append .CreateParameter("@GPSMinute1", adVarChar, adParamInput, 50, GPSMinute1)
		   .parameters.Append .CreateParameter("@GPSSecond1", adVarChar, adParamInput, 50, GPSSecond1)
		   .parameters.Append .CreateParameter("@GPSLat", adVarChar, adParamInput, 50, GPSLat)
		   .parameters.Append .CreateParameter("@GPSDegree2", adVarChar, adParamInput, 50, GPSDegree2)
		   .parameters.Append .CreateParameter("@GPSMinute2", adVarChar, adParamInput, 50, GPSMinute2)
		   .parameters.Append .CreateParameter("@GPSSecond2", adVarChar, adParamInput, 50, GPSSecond2)
		   .parameters.Append .CreateParameter("@GPSLon", adVarChar, adParamInput, 50, GPSLon)
		   .parameters.Append .CreateParameter("@projectAddress", adVarChar, adParamInput, 1000, projectAddress)
		   .parameters.Append .CreateParameter("@projectCity", adVarChar, adParamInput, 100, projectCity)
		   .parameters.Append .CreateParameter("@projectCounty", adVarChar, adParamInput, 100, projectCounty)
		   .parameters.Append .CreateParameter("@subdivision", adVarChar, adParamInput, 150, subdivision)
		   .parameters.Append .CreateParameter("@ownerName", adVarChar, adParamInput, 100, ownerName)		   
		   .parameters.Append .CreateParameter("@ownerAddress", adVarChar, adParamInput, 100, ownerAddress)
		   .parameters.Append .CreateParameter("@ownerCity", adVarChar, adParamInput, 100, ownerCity)
		   .parameters.Append .CreateParameter("@ownerState", adVarChar, adParamInput, 50, ownerState)
		   .parameters.Append .CreateParameter("@ownerZip", adVarChar, adParamInput, 50, ownerZip)
		   .parameters.Append .CreateParameter("@operatorName", adVarChar, adParamInput, 100, operatorName)
		   .parameters.Append .CreateParameter("@operatorPhone", adVarChar, adParamInput, 50, operatorPhone)
		   .parameters.Append .CreateParameter("@operatorAddress", adVarChar, adParamInput, 100, operatorAddress)
		   .parameters.Append .CreateParameter("@operatorCity", adVarChar, adParamInput, 100, operatorCity)
		   .parameters.Append .CreateParameter("@operatorState", adVarChar, adParamInput, 50, operatorState)
		   .parameters.Append .CreateParameter("@operatorZip", adVarChar, adParamInput, 50, operatorZip)
		   .parameters.Append .CreateParameter("@facilityContact", adVarChar, adParamInput, 100, facilityContact)
		   .parameters.Append .CreateParameter("@facilityContactPhone", adVarChar, adParamInput, 50, facilityContactPhone)		   
		   .parameters.Append .CreateParameter("@startDate", adDBTimeStamp, adParamInput, 8, startDate)
		   .parameters.Append .CreateParameter("@completionDate", adDBTimeStamp, adParamInput, 8, completionDate)
		   .parameters.Append .CreateParameter("@estimatedDisturbedAcerage", adDouble, adParamInput, , estimatedDisturbedAcerage)
		   .parameters.Append .CreateParameter("@typeConstructionCommercial", adBoolean, adParamInput, 1, isOn(typeConstructionCommercial))
		   .parameters.Append .CreateParameter("@typeConstructionIndustrial", adBoolean, adParamInput, 1, isOn(typeConstructionIndustrial))
		   .parameters.Append .CreateParameter("@typeConstructionMunicipal", adBoolean, adParamInput, 1, isOn(typeConstructionMunicipal))
		   .parameters.Append .CreateParameter("@typeConstructionLinear", adBoolean, adParamInput, 1, isOn(typeConstructionLinear))
		   .parameters.Append .CreateParameter("@typeConstructionUtility", adBoolean, adParamInput, 1, isOn(typeConstructionUtility))
		   .parameters.Append .CreateParameter("@typeConstructionResidential", adBoolean, adParamInput, 1, isOn(typeConstructionResidential))
		   .parameters.Append .CreateParameter("@secondaryPermittees", adVarChar, adParamInput, 50, secondaryPermittees)
		   .parameters.Append .CreateParameter("@IRWName", adVarChar, adParamInput, 100, IRWName)
		   .parameters.Append .CreateParameter("@IRWTrout", adBoolean, adParamInput, 1, isOn(IRWTrout))
		   .parameters.Append .CreateParameter("@IRWWarmWater", adBoolean, adParamInput, 1, isOn(IRWWarmWater))
		   .parameters.Append .CreateParameter("@MSSSOwnerOperator", adVarChar, adParamInput, 100, MSSSOwnerOperator)
		   .parameters.Append .CreateParameter("@RWName", adVarChar, adParamInput, 100, RWName)
		   .parameters.Append .CreateParameter("@RWTrout", adBoolean, adParamInput, 1, isOn(RWTrout))
		   .parameters.Append .CreateParameter("@RWWarmWater", adBoolean, adParamInput, 1, isOn(RWWarmWater))
		   .parameters.Append .CreateParameter("@samplingOfOutfall", adBoolean, adParamInput, 1, isOn(samplingOfOutfall))
		   .parameters.Append .CreateParameter("@samplingOfRecievingStream", adBoolean, adParamInput, 1, isOn(samplingOfRecievingStream))
		   .parameters.Append .CreateParameter("@troutStream", adBoolean, adParamInput, 1, isOn(troutStream))
		   .parameters.Append .CreateParameter("@numberOfOutfalls", adInteger, adParamInput, 8, numberOfOutfalls)
		   .parameters.Append .CreateParameter("@appendixBNTUValue", adInteger, adParamInput, 8, appendixBNTUValue)
		   .parameters.Append .CreateParameter("@SWDrainageArea", adDouble, adParamInput, , SWDrainageArea)
		   .parameters.Append .CreateParameter("@locationMap", adBoolean, adParamInput, 1, isOn(locationMap))
		   .parameters.Append .CreateParameter("@ESPControlPlan", adBoolean, adParamInput, 1, isOn(ESPControlPlan))
		   .parameters.Append .CreateParameter("@knownSecondaryPermittees", adBoolean, adParamInput, 1, isOn(knownSecondaryPermittees))
		   .parameters.Append .CreateParameter("@timingSchedule", adBoolean, adParamInput, 1, isOn(timingSchedule))
		   .parameters.Append .CreateParameter("@certify1", adVarChar, adParamInput, 50, certify1)
		   .parameters.Append .CreateParameter("@certify2", adVarChar, adParamInput, 50, certify2)
		   .parameters.Append .CreateParameter("@certify3", adVarChar, adParamInput, 50, certify3)
		   .parameters.Append .CreateParameter("@ownerPrintedName", adVarChar, adParamInput, 100, ownerPrintedName)
		   .parameters.Append .CreateParameter("@ownerTitle", adVarChar, adParamInput, 100, ownerTitle)
		   .parameters.Append .CreateParameter("@ownerSignDate", adDBTimeStamp, adParamInput, 8, ownerSignDate)
		   .parameters.Append .CreateParameter("@operatorPrintedName", adVarChar, adParamInput, 100, operatorPrintedName)
		   .parameters.Append .CreateParameter("@operatorTitle", adVarChar, adParamInput, 100, operatorTitle)
		   .parameters.Append .CreateParameter("@operatorSignDate", adDBTimeStamp, adParamInput, 8, operatorSignDate)
		   'new fields 09/2008
		   .parameters.Append .CreateParameter("@noticeOfIntent", adInteger, adParamInput, 8, noticeOfIntent)		   
		   .parameters.Append .CreateParameter("@authorizedRep", adVarChar, adParamInput, 100, authorizedRep)
		   .parameters.Append .CreateParameter("@authorizedRepPhone", adVarChar, adParamInput, 50, authorizedRepPhone)
		   .parameters.Append .CreateParameter("@disturb50", adInteger, adParamInput, 8, disturb50)		
		   .parameters.Append .CreateParameter("@troutStreamRecieve", adBoolean, adParamInput, 1, isOn(troutStreamRecieve))
		   .parameters.Append .CreateParameter("@warmWaterRecieve", adBoolean, adParamInput, 1, isOn(warmWaterRecieve))
		   .parameters.Append .CreateParameter("@warmWater", adBoolean, adParamInput, 1, isOn(warmWater))
		   .parameters.Append .CreateParameter("@constructionSiteSize", adVarChar, adParamInput, 50, constructionSiteSize)
		   .parameters.Append .CreateParameter("@siteDischargeStormWater", adInteger, adParamInput, 8, siteDischargeStormWater)
		   .parameters.Append .CreateParameter("@siteDischargeStormWater2", adInteger, adParamInput, 8, siteDischargeStormWater2)
		   .parameters.Append .CreateParameter("@impairedStream", adVarChar, adParamInput, 50, impairedStream)
		   .parameters.Append .CreateParameter("@impairedStream2", adVarChar, adParamInput, 50, impairedStream2)
		   .parameters.Append .CreateParameter("@writtenAuth", adBoolean, adParamInput, 1, isOn(writtenAuth))
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If

		primaryFormID = rs("Identity")
		
		Set oCmd = nothing
		rs.close
		Set rs = nothing
		
		response.Write primaryFormID
		
		
		'create the PDF using the above report ID
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildPrimaryNOIGAPDF.asp?primaryFormID=" & primaryFormID', "landscape=true"
		Filename = Doc.Save( Server.MapPath("downloads/NOI/NOIPrimary_" & primaryFormID & ".pdf"), True )
		
		'***********create the pdf***********************************
'		select case coverageDesired
'			case 1
'				x = 72
'				y = 570
'			case 2
'				x = 216
'				y = 570
'			case 3
'				x = 361
'				y = 570
'			
'		end select
		
'		GPSDegree1= request("degree1")
'		GPSMinute1= request("minute1")
'		GPSSecond1= request("second1")
'		GPSLat =  request("latitude")
'		GPSDegree2= request("degree2")
'		GPSMinute2= request("minute2")
'		GPSSecond2= request("second2")
'		GPSLon =  request("longitude")
		'�
'		GPSLocation = GPSDegree1 & "&deg; " & GPSMinute1 & "' " & GPSSecond1 & "&quot; " & GPSLat & " / " & GPSDegree2 & "&deg; " & GPSMinute2 & "' " & GPSSecond2 & "&quot; " & GPSLon
		'create the PDF and store it in the downloads folder
		'page 1	
'		arrX = Array(500,x, 155, 212,140,150,370,152,140,112,315,430,511,150,435,115,315,430,511,140,435,120,295,493,216,288,360,435,216,288,215,253,144,288,345,230,144,288,108,252,432,190,324,489) 'left to right
'		arrY = Array(765,y, 520,500,480,460,460,440,420,400,400,400,400,380,380,360,360,360,360,340,340,300,300,300,281,281,281,281,271,271,250,210,191,191,170,150,131,131,111,111,111,90,90,90) 'up and down
'		arrText = Array(primaryFormID,"x",siteProjectName,GPSLocation,projectAddress,projectCity,projectCounty,subdivision,ownerName,ownerAddress, _
'			ownerCity,ownerState,ownerZip,operatorName,operatorPhone,operatorAddress,operatorCity,operatorState,operatorZip,facilityContact,facilityContactPhone, _
'			startDate,completionDate,estimatedDisturbedAcerage,setx(typeConstructionCommercial),setx(typeConstructionIndustrial),setx(typeConstructionMunicipal), _
'			setx(typeConstructionLinear),setx(typeConstructionUtility),setx(typeConstructionResidential),secondaryPermittees,IRWName,setx(IRWTrout),setx(IRWWarmWater), _
'			MSSSOwnerOperator,RWName,setx(RWTrout),setx(RWWarmWater),setx(samplingOfOutfall),setx(samplingOfRecievingStream),setx(troutStream),numberOfOutfalls, _
'			appendixBNTUValue,SWDrainageArea)
		
		'page2	
'		arrX2 = Array(125,125,125,125,108,108,108,172,457,457,180,457,459) 'left to right
'		arrY2 = Array(689,658,627,606,551,520,479,387,387,366,335,335,314) 'up and down
'		arrText2 = Array(setx(locationMap),setx(ESPControlPlan),setx(knownSecondaryPermittees),setx(timingSchedule),certify1,certify2,certify3,ownerPrintedName,ownerTitle, _
'			ownerSignDate,operatorPrintedName,operatorTitle,operatorSignDate)
		
'		Set Pdf = Server.CreateObject("Persits.Pdf")
'		Pdf.RegKey = sRegKey
	
		' Open blank PDF form from file
'		Set Doc = Pdf.OpenDocument( Server.MapPath("pdfTemplates/NOIPrimary.pdf") )
	
		' Create default font
'		Set Font = Doc.Fonts("Helvetica")
	
		' Obtain the only page's canvas
'		Set Canvas = Doc.Pages(1).Canvas
'		Set Canvas2 = Doc.Pages(2).Canvas
	
		' Create empty param object
'		Set Param = Pdf.CreateParam	
'		Set Param2 = Pdf.CreateParam	
	
		' Go over all items in arrays
'		For j = 0 to UBound(arrX)
	
'			Param("x") = arrX(j)
'			Param("y") = arrY(j) - 263 * i
'			Param("color") = "black"
'			Param("html") = true
'			' Draw text on canvas
'			Page.ResetCoordinates
'			Canvas.DrawText arrText(j), Param, Font
	
'		Next ' j
		
'		j = 0
'		For j = 0 to UBound(arrX2)
'			Param2("x") = arrX2(j)
'			Param2("y") = arrY2(j) - 263 * i
'			Param2("color") = "black"
'			Param("html") = true
			' Draw text on canvas
'			Page.ResetCoordinates
'			Canvas2.DrawText arrText2(j), Param2, Font
'		Next
		
		' Save document, the Save method returns generated file name
'		Filename = Doc.Save( Server.MapPath("downloads/NOI/NOIPrimary_" & primaryFormID & ".pdf"), True )
		
		'***********end creating the pdf***********************************
		
		response.Redirect "primaryList.asp?projectID=" & projectID
		
	case "editPrimary"
	
		projectID = request("projectID")	
		primaryFormID = request("primaryFormID")
		coverageDesired= request("coverageDesired")
		siteProjectName= request("siteProjectName")
		GPSDegree1= request("degree1")
		GPSMinute1= request("minute1")
		GPSSecond1= request("second1")
		GPSLat =  request("latitude")
		GPSDegree2= request("degree2")
		GPSMinute2= request("minute2")
		GPSSecond2= request("second2")
		GPSLon =  request("longitude")
		projectAddress= request("projectAddress")
		projectCity= request("projectCity")
		projectCounty= request("projectCounty")
		subdivision= request("subdivision")
		ownerName= request("ownerName")
		ownerAddress= request("ownerAddress")
		ownerCity= request("ownerCity")
		ownerState= request("ownerState")
		ownerZip= request("ownerZip")
		operatorName= request("operatorName")
		operatorPhone= request("operatorPhone")
		operatorAddress= request("operatorAddress")
		operatorCity= request("operatorCity")
		operatorState= request("operatorState")
		operatorZip= request("operatorZip")
		facilityContact= request("facilityContact")
		facilityContactPhone= request("facilityContactPhone")
		startDate= request("startDate")
		if startDate = "" then
			startDate = null
		end if
		completionDate= request("completionDate")
		if completionDate = "" then
			completionDate = null
		end if
		estimatedDisturbedAcerage= request("estimatedDisturbedAcerage")
		if estimatedDisturbedAcerage = "" then
			estimatedDisturbedAcerage = null
		end if
		typeConstructionCommercial= request("typeConstructionCommercial")
		typeConstructionIndustrial= request("typeConstructionIndustrial")
		typeConstructionMunicipal= request("typeConstructionMunicipal")
		typeConstructionLinear= request("typeConstructionLinear")
		typeConstructionUtility= request("typeConstructionUtility")
		typeConstructionResidential= request("typeConstructionResidential")
		secondaryPermittees= request("secondaryPermittees")
		IRWName= request("IRWName")
		IRWTrout= request("IRWTrout")
		IRWWarmWater= request("IRWWarmWater")
		MSSSOwnerOperator= request("MSSSOwnerOperator")
		RWName= request("RWName")
		RWTrout= request("RWTrout")
		RWWarmWater= request("RWWarmWater")
		samplingOfOutfall= request("samplingOfOutfall")
		samplingOfRecievingStream= request("samplingOfRecievingStream")
		troutStream= request("troutStream")
		numberOfOutfalls= request("numberOfOutfalls")
		if numberOfOutfalls = "" then
			numberOfOutfalls = null
		end if
		appendixBNTUValue= request("appendixBNTUValue")
		SWDrainageArea= request("SWDrainageArea")
		if SWDrainageArea = "" then
			SWDrainageArea = null
		end if
		locationMap= request("locationMap")
		ESPControlPlan= request("ESPControlPlan")
		knownSecondaryPermittees= request("knownSecondaryPermittees")
		timingSchedule= request("timingSchedule")
		certify1= request("certify1")
		certify2= request("certify2")
		certify3= request("certify3")
		ownerPrintedName= request("ownerPrintedName")
		ownerTitle= request("ownerTitle")
		ownerSignDate= request("ownerSignDate")
		if ownerSignDate = "" then
			ownerSignDate = null
		end if
		operatorPrintedName= request("operatorPrintedName")
		operatorTitle= request("operatorTitle")
		operatorSignDate= request("operatorSignDate")
		if operatorSignDate = "" then
			operatorSignDate = null
		end if
		
		'new fields 09/2008
		noticeOfIntent= checkNull(request("noticeOfIntent"))
		authorizedRep= checkNull(request("authorizedRep"))
		authorizedRepPhone= checkNull(request("authorizedRepPhone"))
		disturb50= checkNull(request("disturb50"))
		troutStreamRecieve= checkNull(request("troutStreamRecieve"))
		warmWaterRecieve= checkNull(request("warmWaterRecieve"))
		warmWater= checkNull(request("warmWater"))
		constructionSiteSize= checkNull(request("constructionSiteSize"))
		siteDischargeStormWater= checkNull(request("siteDischargeStormWater"))
		siteDischargeStormWater2= checkNull(request("siteDischargeStormWater2"))
		impairedStream= checkNull(request("impairedStream"))
		impairedStream2= checkNull(request("impairedStream2"))
		writtenAuth= checkNull(request("writtenAuth"))
		
		
		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditPrimaryNOI"
		   .parameters.Append .CreateParameter("@primaryFormID", adInteger, adParamInput, 8, primaryFormID)
		   .parameters.Append .CreateParameter("@coverageDesired", adInteger, adParamInput, 8, coverageDesired)
		   .parameters.Append .CreateParameter("@siteProjectName", adVarChar, adParamInput, 200, siteProjectName)
		   .parameters.Append .CreateParameter("@GPSDegree1", adVarChar, adParamInput, 50, GPSDegree1)
		   .parameters.Append .CreateParameter("@GPSMinute1", adVarChar, adParamInput, 50, GPSMinute1)
		   .parameters.Append .CreateParameter("@GPSSecond1", adVarChar, adParamInput, 50, GPSSecond1)
		   .parameters.Append .CreateParameter("@GPSLat", adVarChar, adParamInput, 50, GPSLat)
		   .parameters.Append .CreateParameter("@GPSDegree2", adVarChar, adParamInput, 50, GPSDegree2)
		   .parameters.Append .CreateParameter("@GPSMinute2", adVarChar, adParamInput, 50, GPSMinute2)
		   .parameters.Append .CreateParameter("@GPSSecond2", adVarChar, adParamInput, 50, GPSSecond2)
		   .parameters.Append .CreateParameter("@GPSLon", adVarChar, adParamInput, 50, GPSLon)
		   .parameters.Append .CreateParameter("@projectAddress", adVarChar, adParamInput, 1000, projectAddress)
		   .parameters.Append .CreateParameter("@projectCity", adVarChar, adParamInput, 100, projectCity)
		   .parameters.Append .CreateParameter("@projectCounty", adVarChar, adParamInput, 100, projectCounty)
		   .parameters.Append .CreateParameter("@subdivision", adVarChar, adParamInput, 150, subdivision)
		   .parameters.Append .CreateParameter("@ownerName", adVarChar, adParamInput, 100, ownerName)		   
		   .parameters.Append .CreateParameter("@ownerAddress", adVarChar, adParamInput, 100, ownerAddress)
		   .parameters.Append .CreateParameter("@ownerCity", adVarChar, adParamInput, 100, ownerCity)
		   .parameters.Append .CreateParameter("@ownerState", adVarChar, adParamInput, 50, ownerState)
		   .parameters.Append .CreateParameter("@ownerZip", adVarChar, adParamInput, 50, ownerZip)
		   .parameters.Append .CreateParameter("@operatorName", adVarChar, adParamInput, 100, operatorName)
		   .parameters.Append .CreateParameter("@operatorPhone", adVarChar, adParamInput, 50, operatorPhone)
		   .parameters.Append .CreateParameter("@operatorAddress", adVarChar, adParamInput, 100, operatorAddress)
		   .parameters.Append .CreateParameter("@operatorCity", adVarChar, adParamInput, 100, operatorCity)
		   .parameters.Append .CreateParameter("@operatorState", adVarChar, adParamInput, 50, operatorState)
		   .parameters.Append .CreateParameter("@operatorZip", adVarChar, adParamInput, 50, operatorZip)
		   .parameters.Append .CreateParameter("@facilityContact", adVarChar, adParamInput, 100, facilityContact)
		   .parameters.Append .CreateParameter("@facilityContactPhone", adVarChar, adParamInput, 50, facilityContactPhone)		   
		   .parameters.Append .CreateParameter("@startDate", adDBTimeStamp, adParamInput, 8, startDate)
		   .parameters.Append .CreateParameter("@completionDate", adDBTimeStamp, adParamInput, 8, completionDate)
		   .parameters.Append .CreateParameter("@estimatedDisturbedAcerage", adDouble, adParamInput, , estimatedDisturbedAcerage)
		   .parameters.Append .CreateParameter("@typeConstructionCommercial", adBoolean, adParamInput, 1, isOn(typeConstructionCommercial))
		   .parameters.Append .CreateParameter("@typeConstructionIndustrial", adBoolean, adParamInput, 1, isOn(typeConstructionIndustrial))
		   .parameters.Append .CreateParameter("@typeConstructionMunicipal", adBoolean, adParamInput, 1, isOn(typeConstructionMunicipal))
		   .parameters.Append .CreateParameter("@typeConstructionLinear", adBoolean, adParamInput, 1, isOn(typeConstructionLinear))
		   .parameters.Append .CreateParameter("@typeConstructionUtility", adBoolean, adParamInput, 1, isOn(typeConstructionUtility))
		   .parameters.Append .CreateParameter("@typeConstructionResidential", adBoolean, adParamInput, 1, isOn(typeConstructionResidential))
		   .parameters.Append .CreateParameter("@secondaryPermittees", adVarChar, adParamInput, 50, secondaryPermittees)
		   .parameters.Append .CreateParameter("@IRWName", adVarChar, adParamInput, 100, IRWName)
		   .parameters.Append .CreateParameter("@IRWTrout", adBoolean, adParamInput, 1, isOn(IRWTrout))
		   .parameters.Append .CreateParameter("@IRWWarmWater", adBoolean, adParamInput, 1, isOn(IRWWarmWater))
		   .parameters.Append .CreateParameter("@MSSSOwnerOperator", adVarChar, adParamInput, 100, MSSSOwnerOperator)
		   .parameters.Append .CreateParameter("@RWName", adVarChar, adParamInput, 100, RWName)
		   .parameters.Append .CreateParameter("@RWTrout", adBoolean, adParamInput, 1, isOn(RWTrout))
		   .parameters.Append .CreateParameter("@RWWarmWater", adBoolean, adParamInput, 1, isOn(RWWarmWater))
		   .parameters.Append .CreateParameter("@samplingOfOutfall", adBoolean, adParamInput, 1, isOn(samplingOfOutfall))
		   .parameters.Append .CreateParameter("@samplingOfRecievingStream", adBoolean, adParamInput, 1, isOn(samplingOfRecievingStream))
		   .parameters.Append .CreateParameter("@troutStream", adBoolean, adParamInput, 1, isOn(troutStream))
		   .parameters.Append .CreateParameter("@numberOfOutfalls", adInteger, adParamInput, 8, numberOfOutfalls)
		   .parameters.Append .CreateParameter("@appendixBNTUValue", adInteger, adParamInput, 8, appendixBNTUValue)
		   .parameters.Append .CreateParameter("@SWDrainageArea", adDouble, adParamInput, , SWDrainageArea)
		   .parameters.Append .CreateParameter("@locationMap", adBoolean, adParamInput, 1, isOn(locationMap))
		   .parameters.Append .CreateParameter("@ESPControlPlan", adBoolean, adParamInput, 1, isOn(ESPControlPlan))
		   .parameters.Append .CreateParameter("@knownSecondaryPermittees", adBoolean, adParamInput, 1, isOn(knownSecondaryPermittees))
		   .parameters.Append .CreateParameter("@timingSchedule", adBoolean, adParamInput, 1, isOn(timingSchedule))
		   .parameters.Append .CreateParameter("@certify1", adVarChar, adParamInput, 50, certify1)
		   .parameters.Append .CreateParameter("@certify2", adVarChar, adParamInput, 50, certify2)
		   .parameters.Append .CreateParameter("@certify3", adVarChar, adParamInput, 50, certify3)
		   .parameters.Append .CreateParameter("@ownerPrintedName", adVarChar, adParamInput, 100, ownerPrintedName)
		   .parameters.Append .CreateParameter("@ownerTitle", adVarChar, adParamInput, 100, ownerTitle)
		   .parameters.Append .CreateParameter("@ownerSignDate", adDBTimeStamp, adParamInput, 8, ownerSignDate)
		   .parameters.Append .CreateParameter("@operatorPrintedName", adVarChar, adParamInput, 100, operatorPrintedName)
		   .parameters.Append .CreateParameter("@operatorTitle", adVarChar, adParamInput, 100, operatorTitle)
		   .parameters.Append .CreateParameter("@operatorSignDate", adDBTimeStamp, adParamInput, 8, operatorSignDate)
		   'new fields 09/2008
		   .parameters.Append .CreateParameter("@noticeOfIntent", adInteger, adParamInput, 8, noticeOfIntent)		   
		   .parameters.Append .CreateParameter("@authorizedRep", adVarChar, adParamInput, 100, authorizedRep)
		   .parameters.Append .CreateParameter("@authorizedRepPhone", adVarChar, adParamInput, 50, authorizedRepPhone)
		   .parameters.Append .CreateParameter("@disturb50", adInteger, adParamInput, 8, disturb50)		
		   .parameters.Append .CreateParameter("@troutStreamRecieve", adBoolean, adParamInput, 1, isOn(troutStreamRecieve))
		   .parameters.Append .CreateParameter("@warmWaterRecieve", adBoolean, adParamInput, 1, isOn(warmWaterRecieve))
		   .parameters.Append .CreateParameter("@warmWater", adBoolean, adParamInput, 1, isOn(warmWater))
		   .parameters.Append .CreateParameter("@constructionSiteSize", adVarChar, adParamInput, 50, constructionSiteSize)
		   .parameters.Append .CreateParameter("@siteDischargeStormWater", adInteger, adParamInput, 8, siteDischargeStormWater)
		   .parameters.Append .CreateParameter("@siteDischargeStormWater2", adInteger, adParamInput, 8, siteDischargeStormWater2)
		   .parameters.Append .CreateParameter("@impairedStream", adVarChar, adParamInput, 50, impairedStream)
		   .parameters.Append .CreateParameter("@impairedStream2", adVarChar, adParamInput, 50, impairedStream2)
		   .parameters.Append .CreateParameter("@writtenAuth", adBoolean, adParamInput, 1, isOn(writtenAuth))
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		Set oCmd = nothing
		rs.close
		Set rs = nothing
		DataConn.close
		Set DataConn = nothing
		
		
		'create the PDF using the above report ID
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildPrimaryNOIGAPDF.asp?primaryFormID=" & primaryFormID', "landscape=true"
		Filename = Doc.Save( Server.MapPath("downloads/NOI/NOIPrimary_" & primaryFormID & ".pdf"), True )
		
		'***********edit the pdf***********************************
'		select case coverageDesired
'			case 1
'				x = 72
'				y = 570
'			case 2
'				x = 216
'				y = 570
'			case 3
'				x = 361
'				y = 570
'			
'		end select
		
'		GPSLocation = GPSDegree1 & "&deg; " & GPSMinute1 & "' " & GPSSecond1 & "&quot; " & GPSLat & " / " & GPSDegree2 & "&deg; " & GPSMinute2 & "' " & GPSSecond2 & "&quot; " & GPSLon

		
		'create the PDF and store it in the downloads folder
		'page 1	
'		arrX = Array(450,x, 155, 212,140,150,370,152,140,112,315,430,511,150,435,115,315,430,511,140,435,120,295,493,216,288,360,435,216,288,215,253,144,288,345,230,144,288,108,252,432,190,324,489) 'left to right
'		arrY = Array(765,y, 520,500,480,460,460,440,420,400,400,400,400,380,380,360,360,360,360,340,340,300,300,300,281,281,281,281,271,271,250,210,191,191,170,150,131,131,111,111,111,90,90,90) 'up and down
'		arrText = Array(primaryFormID,"x",siteProjectName,GPSLocation,projectAddress,projectCity,projectCounty,subdivision,ownerName,ownerAddress, _
'			ownerCity,ownerState,ownerZip,operatorName,operatorPhone,operatorAddress,operatorCity,operatorState,operatorZip,facilityContact,facilityContactPhone, _
'			startDate,completionDate,estimatedDisturbedAcerage,setx(typeConstructionCommercial),setx(typeConstructionIndustrial),setx(typeConstructionMunicipal), _
'			setx(typeConstructionLinear),setx(typeConstructionUtility),setx(typeConstructionResidential),secondaryPermittees,IRWName,setx(IRWTrout),setx(IRWWarmWater), _
'			MSSSOwnerOperator,RWName,setx(RWTrout),setx(RWWarmWater),setx(samplingOfOutfall),setx(samplingOfRecievingStream),setx(troutStream),numberOfOutfalls, _
'			appendixBNTUValue,SWDrainageArea)
		
		'page2	
'		arrX2 = Array(125,125,125,125,108,108,108,172,457,457,180,457,459) 'left to right
'		arrY2 = Array(689,658,627,606,551,520,479,387,387,366,335,335,314) 'up and down
'		arrText2 = Array(setx(locationMap),setx(ESPControlPlan),setx(knownSecondaryPermittees),setx(timingSchedule),certify1,certify2,certify3,ownerPrintedName,ownerTitle, _
'			ownerSignDate,operatorPrintedName,operatorTitle,operatorSignDate)
		
'		Set Pdf = Server.CreateObject("Persits.Pdf")
'		Pdf.RegKey = sRegKey
	
		' Open blank PDF form from file
'		Set Doc = Pdf.OpenDocument( Server.MapPath("pdfTemplates/NOIPrimary.pdf") )
	
		' Create default font
'		Set Font = Doc.Fonts("Helvetica")
	
		' Obtain the only page's canvas
'		Set Canvas = Doc.Pages(1).Canvas
'		Set Canvas2 = Doc.Pages(2).Canvas
	
		' Create empty param object
'		Set Param = Pdf.CreateParam	
'		Set Param2 = Pdf.CreateParam	
	
		' Go over all items in arrays
'		For j = 0 to UBound(arrX)
'	
'			Param("x") = arrX(j)
'			Param("y") = arrY(j) - 263 * i
'			Param("color") = "black"
'			Param("html") = true
'			' Draw text on canvas
'			Page.ResetCoordinates
'			Canvas.DrawText arrText(j), Param, Font
'	
'		Next ' j
		
'		j = 0
'		For j = 0 to UBound(arrX2)
'			Param2("x") = arrX2(j)
'			Param2("y") = arrY2(j) - 263 * i
'			Param2("color") = "black"
'			Param("html") = true
			' Draw text on canvas
'			Page.ResetCoordinates
'			Canvas2.DrawText arrText2(j), Param2, Font
'		Next
		
		' Save document, the Save method returns generated file name
'		Filename = Doc.Save( Server.MapPath("downloads/NOI/NOIPrimary_" & primaryFormID & ".pdf"), True )
		
		'***********end editing the pdf***********************************
		
		'if they exist already, edit the sub forms
		'check to see if they exist.
		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
			
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
			.ActiveConnection = DataConn
		   .CommandText = "spCheckForUpdate"
		   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, primaryFormID)
		   .CommandType = adCmdStoredProc
		end with
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		feeID = rs("feeFormID")
		notID = rs("notFormID")
	
		Set oCmd = nothing
		rs.close
		Set rs = nothing
		DataConn.close
		Set DataConn = nothing
	
	'############################################### edit fee form ###########################################################################
		'if the fee form exists then update the data and regenerate the pdf
		If feeID <> "" then
			'get the fee form data and update with it along with the data from the primary form
			'will need to map the data
			
			On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
			%>
					<!--#include file="includes/FatalError.inc"-->
			<%
				End If
				
				DataConn.Open Session("Connection"), Session("UserID")
				If Err Then
			%>
					<!--#include file="includes/FatalError.inc"-->
			<%
				End If	
				
			'Create command
			Set oCmd = Server.CreateObject("ADODB.Command")
			If Err Then
			%>
				<!--#include file="includes/FatalError.inc"-->
			<%
			End If
				
			With oCmd
				.ActiveConnection = DataConn
			   .CommandText = "spGetFeeData"
			   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, primaryFormID)
			   .CommandType = adCmdStoredProc
			end with
			
			If Err Then
			%>
				<!--#include file="includes/FatalError.inc"-->
			<%
			End If
					
			Set rs = oCmd.Execute
			If Err Then
			%>
				<!--#include file="includes/FatalError.inc"-->
			<%
			End If
			
			'set the variables here
			feeFormID = rs("feeFormID")
			'primaryFormID = rs("primaryFormID")
			primaryPermittee = ownerName
			permitteeAddress = ownerAddress
			permitteeCity = ownerCity
			permitteeState = ownerState
			permitteeZip = ownerZip
			contactTelephone = facilityContactPhone
			projectName = siteProjectName
			projectAddress = projectAddress
			projectCounty = projectCounty
			projectCity = projectCity
			acresDistirbedLocal = estimatedDisturbedAcerage
			acresDistirbedLocalCost = acresDistirbedLocal * 40
			acresDistirbedNoLocal = rs("acresDistirbedNoLocal")
			acresDistirbedNoLocalCost = rs("acresDistirbedNoLocalCost")
			acresDistirbedExempt = rs("acresDistirbedExempt")
			acresDistirbedExemptCost = rs("acresDistirbedExemptCost")
			totalFees = cint(acresDistirbedLocalCost) + cint(acresDistirbedNoLocalCost) + cint(acresDistirbedExemptCost)
			checkNumber = rs("checkNumber")
			dateSubmitted = rs("dateSubmitted")
			printedName = rs("printedName")
			title = rs("title")
			
		
			Set oCmd = nothing
			rs.close
			Set rs = nothing
			DataConn.close
			Set DataConn = nothing
			
			
			'update the fee form data
			'Open connection and insert into the database
			On Error Resume Next
			
				Set DataConn = Server.CreateObject("ADODB.Connection") 
				If Err Then
			%>
					<!--#include file="includes/FatalError.inc"-->
			<%
				End If
				
				DataConn.Open Session("Connection"), Session("UserID")
				If Err Then
			%>
					<!--#include file="includes/FatalError.inc"-->
			<%
				End If	
			
			'Create command
			Set oCmd = Server.CreateObject("ADODB.Command")
			If Err Then
			%>
				<!--#include file="includes/FatalError.inc"-->
			<%
			End If
				
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spEditFeeForm"
			   .parameters.Append .CreateParameter("@feeFormID", adInteger, adParamInput, 8, feeFormID)
			   .parameters.Append .CreateParameter("@primaryPermittee", adVarChar, adParamInput, 100, primaryPermittee)
			   .parameters.Append .CreateParameter("@permitteeAddress", adVarChar, adParamInput, 100, permitteeAddress)
			   .parameters.Append .CreateParameter("@permitteeCity", adVarChar, adParamInput, 100, permitteeCity)
			   .parameters.Append .CreateParameter("@permitteeState", adVarChar, adParamInput, 50, permitteeState)
			   .parameters.Append .CreateParameter("@permitteeZip", adVarChar, adParamInput, 50, permitteeZip)
			   .parameters.Append .CreateParameter("@contactTelephone", adVarChar, adParamInput, 50, contactTelephone)
			   .parameters.Append .CreateParameter("@projectName", adVarChar, adParamInput, 200, projectName)
			   .parameters.Append .CreateParameter("@projectAddress", adVarChar, adParamInput, 100, projectAddress)
			   .parameters.Append .CreateParameter("@projectCounty", adVarChar, adParamInput, 100, projectCounty)
			   .parameters.Append .CreateParameter("@projectCity", adVarChar, adParamInput, 100, projectCity)
			   .parameters.Append .CreateParameter("@acresDistirbedLocal", adDouble, adParamInput, , acresDistirbedLocal)
			   .parameters.Append .CreateParameter("@acresDistirbedLocalCost", adCurrency, adParamInput, , acresDistirbedLocalCost)
			   .parameters.Append .CreateParameter("@acresDistirbedNoLocal", adDouble, adParamInput, , acresDistirbedNoLocal)
			   .parameters.Append .CreateParameter("@acresDistirbedNoLocalCost", adCurrency, adParamInput, , acresDistirbedNoLocalCost)
			   .parameters.Append .CreateParameter("@acresDistirbedExempt", adDouble, adParamInput, , acresDistirbedExempt)
			   .parameters.Append .CreateParameter("@acresDistirbedExemptCost", adCurrency, adParamInput, , acresDistirbedExemptCost)
			   .parameters.Append .CreateParameter("@totalFees", adCurrency, adParamInput, , totalFees)
			   .parameters.Append .CreateParameter("@checkNumber", adVarChar, adParamInput, 50, checkNumber)
			   .parameters.Append .CreateParameter("@dateSubmitted", adDBTimeStamp, adParamInput, 8, dateSubmitted)
			   .parameters.Append .CreateParameter("@printedName", adVarChar, adParamInput, 100, printedName)
			   .parameters.Append .CreateParameter("@title", adVarChar, adParamInput, 100, title)
			   .CommandType = adCmdStoredProc
			End With
			
			If Err Then
			%>
				<!--#include file="includes/FatalError.inc"-->
			<%
			End If
					
			Set rs = oCmd.Execute
			If Err Then
			%>
				<!--#include file="includes/FatalError.inc"-->
			<%
			End If
			Set oCmd = nothing
			rs.close
			set rs = nothing
			DataConn.close
			Set DataConn = nothing
			
			'***********create the pdf***********************************
			'create the PDF and store it in the downloads folder
			'page 1	
			
		
			arrX = Array(465,163,163,75,164,394,417,364,364,352,485,352,485,352,485,486,410,430,143,430) 'left to right
			arrY = Array(770,518,504,490,449,518,504,462,449,421,421,380,380,339,339,312,283,242,214,214) 'up and down
			arrText = Array(primaryFormID,primaryPermittee,permitteeAddress,permitteeCity & ", " & permitteeState & " " & permitteeZip,contactTelephone, _
				projectName,projectAddress,projectCounty,projectCity,acresDistirbedLocal,formatcurrency(acresDistirbedLocalCost,2), _
				acresDistirbedNoLocal,formatcurrency(acresDistirbedNoLocalCost,2),acresDistirbedExempt,formatCurrency(acresDistirbedExemptCost,2), _
				formatcurrency(totalFees,2),checkNumber,dateSubmitted,printedName,title)
			
			Set Pdf = Server.CreateObject("Persits.Pdf")
			Pdf.RegKey = sRegKey
		
			' Open blank PDF form from file
			Set Doc = Pdf.OpenDocument( Server.MapPath("pdfTemplates/npdesFeeForm.pdf") )
		
			' Create default font
			Set Font = Doc.Fonts("Helvetica")
		
			' Obtain the only page's canvas
			Set Canvas = Doc.Pages(1).Canvas
		
			' Create empty param object
			Set Param = Pdf.CreateParam	
		
			' Go over all items in arrays
			For j = 0 to UBound(arrX)
		
				Param("x") = arrX(j)
				Param("y") = arrY(j) - 263 * i
				Param("color") = "black"
				Param("size") = 10
				Param("html") = true
				' Draw text on canvas
				Page.ResetCoordinates
				Canvas.DrawText arrText(j), Param, Font
		
			Next ' j
	
			' Save document, the Save method returns generated file name
			Filename = Doc.Save( Server.MapPath("downloads/NOI/npdesFeeForm_" & primaryFormID & ".pdf"), True )
			
			'***********end creating the pdf***********************************
			
		end if
	
	'############################################### edit not form ############################################################################	
		'if the not form exists then update the data and regenerate the pdf
		If notID <> "" then
			'get the NOT form data and update with it along with the data from the primary form
			'will need to map the data
			
			On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
			%>
					<!--#include file="includes/FatalError.inc"-->
			<%
				End If
				
				DataConn.Open Session("Connection"), Session("UserID")
				If Err Then
			%>
					<!--#include file="includes/FatalError.inc"-->
			<%
				End If	
				
			'Create command
			Set oCmd = Server.CreateObject("ADODB.Command")
			If Err Then
			%>
				<!--#include file="includes/FatalError.inc"-->
			<%
			End If
				
			With oCmd
				.ActiveConnection = DataConn
			   .CommandText = "spGetNOTData"
			   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, primaryFormID)
			   .CommandType = adCmdStoredProc
			end with
			
			If Err Then
			%>
				<!--#include file="includes/FatalError.inc"-->
			<%
			End If
					
			Set rs = oCmd.Execute
			If Err Then
			%>
				<!--#include file="includes/FatalError.inc"-->
			<%
			End If
			
			'set the NOT variables here
			'map fields from primary
			notFormID = rs("notFormID")
			'primaryFormID = rs("primaryFormID")
			coverageDesired= coverageDesired
			siteProjectName= siteProjectName
			GPSDegree1= GPSDegree1
			GPSMinute1= GPSMinute1
			GPSSecond1= GPSSecond1
			GPSLat =  GPSLat
			GPSDegree2= GPSDegree2
			GPSMinute2= GPSMinute2
			GPSSecond2= GPSSecond2
			GPSLon =  GPSLon
			projectAddress= projectAddress
			projectCity= projectCity
			projectCounty= projectCounty
			subdivision= subdivision
			lotNumber= rs("lotNumber")
			ownerName= ownerName
			ownerAddress= ownerAddress
			ownerCity= ownerCity
			ownerState= ownerState
			ownerZip= ownerZip
			operatorName= operatorName
			operatorPhone= operatorPhone
			operatorAddress= operatorAddress
			operatorCity= operatorCity
			operatorState= operatorState
			operatorZip= operatorZip			
			typePrimary= rs("typePrimary")
			typeSecondary= rs("typeSecondary")
			typeTertiary= rs("typeTertiary")					
			facilityContact= facilityContact
			facilityContactPhone= facilityContactPhone			
			primaryPermitteeName= rs("primaryPermitteeName")
			primaryPermitteePhone= rs("primaryPermitteePhone")
			primaryPermitteeAddress= rs("primaryPermitteeAddress")
			primaryPermitteeCity= rs("primaryPermitteeCity")
			primaryPermitteeState= rs("primaryPermitteeState")
			primaryPermitteeZip= rs("primaryPermitteeZip")			
			secondaryPermittees= secondaryPermittees					
			constructionActivityCompleted= rs("constructionActivityCompleted")
			noLongerOwnerOperator= rs("noLongerOwnerOperator")					
			typeConstructionCommercial= typeConstructionCommercial
			typeConstructionIndustrial= typeConstructionIndustrial
			typeConstructionMunicipal= typeConstructionMunicipal
			typeConstructionDOT= typeConstructionLinear
			typeConstructionUtility= typeConstructionUtility

			typeConstructionResidential= typeConstructionResidential				
			primaryPermitteeSubdivision= rs("primaryPermitteeSubdivision")
			individualLot= rs("individualLot")
			individualLotWithinSWDA= rs("individualLotWithinSWDA")				
			IRWName= IRWName
			MSSSOwnerOperator= MSSSOwnerOperator
			RWName= RWName			
			certify1= rs("certify1")
			certify2= rs("certify2")			
			ownerPrintedName= ownerPrintedName
			ownerTitle= ownerTitle
			ownerSignDate= rs("ownerSignDate")
			operatorPrintedName= operatorPrintedName
			operatorTitle= operatorTitle
			operatorSignDate= rs("operatorSignDate")
			
			
			'Open connection and insert into the database
			On Error Resume Next
			
				Set DataConn = Server.CreateObject("ADODB.Connection") 
				If Err Then
			%>
					<!--#include file="includes/FatalError.inc"-->
			<%
				End If
				
				DataConn.Open Session("Connection"), Session("UserID")
				If Err Then
			%>
					<!--#include file="includes/FatalError.inc"-->
			<%
				End If	
			
			'Create command
			Set oCmd = Server.CreateObject("ADODB.Command")
			If Err Then
			%>
				<!--#include file="includes/FatalError.inc"-->
			<%
			End If
				
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spEditNOT"
			   .parameters.Append .CreateParameter("@notFormID", adInteger, adParamInput, 8, notFormID)
			   .parameters.Append .CreateParameter("@coverageDesired", adInteger, adParamInput, 8, coverageDesired)
			   .parameters.Append .CreateParameter("@siteProjectName", adVarChar, adParamInput, 200, siteProjectName)
			   .parameters.Append .CreateParameter("@GPSDegree1", adVarChar, adParamInput, 50, GPSDegree1)
			   .parameters.Append .CreateParameter("@GPSMinute1", adVarChar, adParamInput, 50, GPSMinute1)
			   .parameters.Append .CreateParameter("@GPSSecond1", adVarChar, adParamInput, 50, GPSSecond1)
			   .parameters.Append .CreateParameter("@GPSLat", adVarChar, adParamInput, 50, GPSLat)
			   .parameters.Append .CreateParameter("@GPSDegree2", adVarChar, adParamInput, 50, GPSDegree2)
			   .parameters.Append .CreateParameter("@GPSMinute2", adVarChar, adParamInput, 50, GPSMinute2)
			   .parameters.Append .CreateParameter("@GPSSecond2", adVarChar, adParamInput, 50, GPSSecond2)
			   .parameters.Append .CreateParameter("@GPSLon", adVarChar, adParamInput, 50, GPSLon)
			   .parameters.Append .CreateParameter("@projectAddress", adVarChar, adParamInput, 100, projectAddress)
			   .parameters.Append .CreateParameter("@projectCity", adVarChar, adParamInput, 100, projectCity)
			   .parameters.Append .CreateParameter("@projectCounty", adVarChar, adParamInput, 100, projectCounty)
			   .parameters.Append .CreateParameter("@subdivision", adVarChar, adParamInput, 150, subdivision)
			   .parameters.Append .CreateParameter("@lotNumber", adVarChar, adParamInput, 150, lotNumber)
			   .parameters.Append .CreateParameter("@ownerName", adVarChar, adParamInput, 100, ownerName)		   
			   .parameters.Append .CreateParameter("@ownerAddress", adVarChar, adParamInput, 100, ownerAddress)
			   .parameters.Append .CreateParameter("@ownerCity", adVarChar, adParamInput, 100, ownerCity)
			   .parameters.Append .CreateParameter("@ownerState", adVarChar, adParamInput, 50, ownerState)
			   .parameters.Append .CreateParameter("@ownerZip", adVarChar, adParamInput, 50, ownerZip)
			   .parameters.Append .CreateParameter("@operatorName", adVarChar, adParamInput, 100, operatorName)
			   .parameters.Append .CreateParameter("@operatorPhone", adVarChar, adParamInput, 50, operatorPhone)
			   .parameters.Append .CreateParameter("@operatorAddress", adVarChar, adParamInput, 100, operatorAddress)
			   .parameters.Append .CreateParameter("@operatorCity", adVarChar, adParamInput, 100, operatorCity)
			   .parameters.Append .CreateParameter("@operatorState", adVarChar, adParamInput, 50, operatorState)
			   .parameters.Append .CreateParameter("@operatorZip", adVarChar, adParamInput, 50, operatorZip)
			   .parameters.Append .CreateParameter("@typePrimary", adBoolean, adParamInput, 1, typePrimary)
			   .parameters.Append .CreateParameter("@typeSecondary", adBoolean, adParamInput, 1, typeSecondary)
			   .parameters.Append .CreateParameter("@typeTertiary", adBoolean, adParamInput, 1, typeTertiary)		   
			   .parameters.Append .CreateParameter("@facilityContact", adVarChar, adParamInput, 100, facilityContact)
			   .parameters.Append .CreateParameter("@facilityContactPhone", adVarChar, adParamInput, 50, facilityContactPhone)		   
			   .parameters.Append .CreateParameter("@primaryPermitteeName", adVarChar, adParamInput, 100, primaryPermitteeName)
			   .parameters.Append .CreateParameter("@primaryPermitteePhone", adVarChar, adParamInput, 50, primaryPermitteePhone)
			   .parameters.Append .CreateParameter("@primaryPermitteeAddress", adVarChar, adParamInput, 100, primaryPermitteeAddress)
			   .parameters.Append .CreateParameter("@primaryPermitteeCity", adVarChar, adParamInput, 100, primaryPermitteeCity)
			   .parameters.Append .CreateParameter("@primaryPermitteeState", adVarChar, adParamInput, 50, primaryPermitteeState)
			   .parameters.Append .CreateParameter("@primaryPermitteeZip", adVarChar, adParamInput, 50, primaryPermitteeZip)
			   .parameters.Append .CreateParameter("@secondaryPermittees", adVarChar, adParamInput, 50, secondaryPermittees)	   
			   .parameters.Append .CreateParameter("@constructionActivityCompleted", adBoolean, adParamInput, 1, constructionActivityCompleted)
			   .parameters.Append .CreateParameter("@noLongerOwnerOperator", adBoolean, adParamInput, 1, noLongerOwnerOperator)
			   .parameters.Append .CreateParameter("@typeConstructionCommercial", adBoolean, adParamInput, 1, isOn(typeConstructionCommercial))
			   .parameters.Append .CreateParameter("@typeConstructionIndustrial", adBoolean, adParamInput, 1, isOn(typeConstructionIndustrial))
			   .parameters.Append .CreateParameter("@typeConstructionMunicipal", adBoolean, adParamInput, 1, isOn(typeConstructionMunicipal))
			   .parameters.Append .CreateParameter("@typeConstructionDOT", adBoolean, adParamInput, 1, isOn(typeConstructionDOT))
			   .parameters.Append .CreateParameter("@typeConstructionUtility", adBoolean, adParamInput, 1, isOn(typeConstructionUtility))
			   .parameters.Append .CreateParameter("@typeConstructionResidential", adBoolean, adParamInput, 1, isOn(typeConstructionResidential))		   
			   .parameters.Append .CreateParameter("@primaryPermitteeSubdivision", adBoolean, adParamInput, 1, primaryPermitteeSubdivision)
			   .parameters.Append .CreateParameter("@individualLot", adBoolean, adParamInput, 1, individualLot)
			   .parameters.Append .CreateParameter("@individualLotWithinSWDA", adBoolean, adParamInput, 1, individualLotWithinSWDA)		   
			   .parameters.Append .CreateParameter("@IRWName", adVarChar, adParamInput, 100, IRWName)
			   .parameters.Append .CreateParameter("@MSSSOwnerOperator", adVarChar, adParamInput, 100, MSSSOwnerOperator)
			   .parameters.Append .CreateParameter("@RWName", adVarChar, adParamInput, 100, RWName)		   
			   .parameters.Append .CreateParameter("@certify1", adVarChar, adParamInput, 50, certify1)
			   .parameters.Append .CreateParameter("@certify2", adVarChar, adParamInput, 50, certify2)
			   .parameters.Append .CreateParameter("@ownerPrintedName", adVarChar, adParamInput, 100, ownerPrintedName)
			   .parameters.Append .CreateParameter("@ownerTitle", adVarChar, adParamInput, 100, ownerTitle)
			   .parameters.Append .CreateParameter("@ownerSignDate", adDBTimeStamp, adParamInput, 8, ownerSignDate)
			   .parameters.Append .CreateParameter("@operatorPrintedName", adVarChar, adParamInput, 100, operatorPrintedName)
			   .parameters.Append .CreateParameter("@operatorTitle", adVarChar, adParamInput, 100, operatorTitle)
			   .parameters.Append .CreateParameter("@operatorSignDate", adDBTimeStamp, adParamInput, 8, operatorSignDate)
			   .CommandType = adCmdStoredProc
			End With
			
			If Err Then
			%>
				<!--#include file="includes/FatalError.inc"-->
			<%
			End If
					
			Set rs = oCmd.Execute
			If Err Then
			%>
				<!--#include file="includes/FatalError.inc"-->
			<%
			End If
			Set oCmd = nothing
			rs.close
			Set rs = nothing
			DataConn.close
			Set DataConn = nothing
			
			'***********edit the pdf***********************************
			select case coverageDesired
				case 1
					x = 73
					y = 611
				case 2
					x = 223
					y = 611
				case 3
					x = 397
					y = 611
				
			end select
	'response.Write noLongerOwnerOperator		
			'create the PDF and store it in the downloads folder
			'page 1	
			arrX = Array(450,x,171,246,243,100,377,170,437,153,121,322,437,523,166,480,124,322,437,523,217,325,433,157,443,209,443,121,282,363,492,252,72,289,181,289,361,433,505,181,289,289,289,252,363,252) 'left to right
			arrY = Array(775,y,563,543,524,505,505,487,487,470,450,450,450,450,430,430,410,410,410,410,388,388,388,367,367,329,329,310,310,310,310,293,251,251,231,231,231,231,231,211,211,191,170,131,112,93) 'up and down
			arrText = Array(primaryFormID,"x",siteProjectName,GPSLocation,projectAddress,projectCity,projectCounty,subdivision,lotNumber,ownerName,ownerAddress, _
				ownerCity,ownerState,ownerZip,operatorName,operatorPhone,operatorAddress,operatorCity,operatorState,operatorZip,setxtf(typePrimary),setxtf(typeSecondary), _
				setxtf(typeTertiary),facilityContact,facilityContactPhone,primaryPermitteeName,primaryPermitteePhone,primaryPermitteeAddress,primaryPermitteeCity, _
				primaryPermitteeState,primaryPermitteeZip,secondaryPermittees,setxtf(constructionActivityCompleted),setxtf(noLongerOwnerOperator),setx(typeConstructionCommercial), _
				setx(typeConstructionIndustrial),setx(typeConstructionMunicipal),setx(typeConstructionDOT),setx(typeConstructionUtility),setx(typeConstructionResidential), _
				setxtf(primaryPermitteeSubdivision),setxtf(individualLot),setxtf(individualLotWithinSWDA),IRWName,MSSSOwnerOperator,RWName)
			
			'page2	
			arrX2 = Array(72,72,187,425,427,200,425,427) 'left to right
			arrY2 = Array(680,432,295,295,270,232,232,207) 'up and down
			arrText2 = Array(certify1,certify2,ownerPrintedName,ownerTitle,ownerSignDate,operatorPrintedName,operatorTitle,operatorSignDate)
			
			Set Pdf = Server.CreateObject("Persits.Pdf")
			Pdf.RegKey = sRegKey
		
			' Open blank PDF form from file
			Set Doc = Pdf.OpenDocument( Server.MapPath("pdfTemplates/noticeOfTermination.pdf") )
		
			' Create default font
			Set Font = Doc.Fonts("Helvetica")
		
			' Obtain the only page's canvas
			Set Canvas = Doc.Pages(1).Canvas
			Set Canvas2 = Doc.Pages(2).Canvas
		
			' Create empty param object
			Set Param = Pdf.CreateParam	
			Set Param2 = Pdf.CreateParam	
		
			' Go over all items in arrays
			For j = 0 to UBound(arrX)
		
				Param("x") = arrX(j)
				Param("y") = arrY(j) - 263 * i
				Param("color") = "black"
				Param("html") = true
				' Draw text on canvas
				Page.ResetCoordinates
				Canvas.DrawText arrText(j), Param, Font
		
			Next ' j
			
			j = 0
			For j = 0 to UBound(arrX2)
				Param2("x") = arrX2(j)
				Param2("y") = arrY2(j) - 263 * i
				Param2("color") = "black"
				Param("html") = true
				' Draw text on canvas
				Page.ResetCoordinates
				Canvas2.DrawText arrText2(j), Param2, Font
			Next
			
			' Save document, the Save method returns generated file name
			Filename = Doc.Save( Server.MapPath("downloads/NOI/noticeOfTermination_" & primaryFormID & ".pdf"), True )
			
			'***********end editing the pdf***********************************		
		end if
	'###########################################################################################################################################		
		
		response.Redirect "primaryList.asp?projectID=" & projectID
		
	case "deletePrimary"
		primaryFormID = Request("id")
	
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If					
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeletePrimaryNOI"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, primaryFormID)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		Set oCmd = nothing
		
		'delete the corresponding pdf from the server
		
		sFile = Server.MapPath("downloads/NOI/NOIPrimary_" & primaryFormID & ".pdf")
		
		Set fso = CreateObject("Scripting.FileSystemObject")
		
		'Delete the file
		fso.DeleteFile sFile, True
		
		'delete the fee for associated with this form
		
		response.Redirect "primaryList.asp"
		
	Case "addSecondary"
	
		projectID = request("projectID")
		primaryFormID = checkNull(request("primaryID"))
		siteProjectName = checkNull(request("siteProjectName"))
		siteLocation = checkNull(request("siteLocation"))
		city = checkNull(request("city"))
		county = checkNull(request("county"))
		subdivision = checkNull(request("subdivision"))
		lotNumber = checkNull(request("lotNumber"))
		secondaryPermitteeName = checkNull(request("secondaryPermitteeName"))
		permitteePhone = checkNull(request("permitteePhone"))
		permitteeAddress = checkNull(request("permitteeAddress"))
		permitteeCity = checkNull(request("permitteeCity"))
		permitteeState = checkNull(request("permitteeState"))
		permitteeZip = checkNull(request("permitteeZip"))
		primaryPermitteeName = checkNull(request("primaryPermitteeName"))
		primaryPermitteePhone = checkNull(request("primaryPermitteePhone"))
		primaryPermitteeAddress = checkNull(request("primaryPermitteeAddress"))
		primaryPermitteeCity = checkNull(request("primaryPermitteeCity"))
		primaryPermitteeState = checkNull(request("primaryPermitteeState"))
		primaryPermitteeZip = checkNull(request("primaryPermitteeZip"))
		facilityContact = checkNull(request("facilityContact"))
		facilityContactPhone = checkNull(request("facilityContactPhone"))
		startDate = checkNull(request("startDate"))
		completionDate = checkNull(request("completionDate"))
		estimatedDisturbedAcerage = checkNull(request("estimatedDisturbedAcerage"))
		typeConstructionCommercial = checkNull(request("typeConstructionCommercial"))
		typeConstructionIndustrial = checkNull(request("typeConstructionIndustrial"))
		typeConstructionMunicipal = checkNull(request("typeConstructionMunicipal"))
		typeConstructionDOT = checkNull(request("typeConstructionDOT"))
		typeConstructionUtility = checkNull(request("typeConstructionUtility"))
		typeConstructionResidential = checkNull(request("typeConstructionResidential"))
		IRWName = checkNull(request("IRWName"))
		IRWTrout = checkNull(request("IRWTrout"))
		IRWWarmWater = checkNull(request("IRWWarmWater"))
		MSSSOwnerOperator = checkNull(request("MSSSOwnerOperator"))
		MSSSTrout = checkNull(request("MSSSTrout"))
		MSSSWarmWater = checkNull(request("MSSSWarmWater"))
		certify1 = checkNull(request("certify1"))
		certify2 = checkNull(request("certify2"))
		printedName = checkNull(request("printedName"))
		title = checkNull(request("title"))
		signDate = checkNull(request("signDate"))
		
		'new fields 09/2008
		noticeOfIntent= checkNull(request("noticeOfIntent"))
		commonDevelopmentName = checkNull(request("commonDevelopmentName"))
		constructionSiteStreetAddress = checkNull(request("constructionSiteStreetAddress"))
		authorizedRep= checkNull(request("authorizedRep"))
		authorizedRepPhone= checkNull(request("authorizedRepPhone"))
		disturb50= checkNull(request("disturb50"))
		RWName= checkNull(request("RWName"))		
		siteDischargeStormWater= checkNull(request("siteDischargeStormWater"))
		siteDischargeStormWater2= checkNull(request("siteDischargeStormWater2"))
		impairedStream= checkNull(request("impairedStream"))
		impairedStream2= checkNull(request("impairedStream2"))
		writtenAuth= checkNull(request("writtenAuth"))
	
		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If					
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddSecondaryNOI"
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
		   .parameters.Append .CreateParameter("@primaryFormID", adInteger, adParamInput, 8, primaryFormID)
		   .parameters.Append .CreateParameter("@siteProjectName", adVarChar, adParamInput, 100, siteProjectName)
		   .parameters.Append .CreateParameter("@siteLocation", adVarChar, adParamInput, 200, siteLocation)
		   .parameters.Append .CreateParameter("@city", adVarChar, adParamInput, 50, city)
		   .parameters.Append .CreateParameter("@county", adVarChar, adParamInput, 50, county)
		   .parameters.Append .CreateParameter("@subdivision", adVarChar, adParamInput, 100, subdivision)
		   .parameters.Append .CreateParameter("@lotNumber", adVarChar, adParamInput, 100, lotNumber)
		   .parameters.Append .CreateParameter("@secondaryPermitteeName", adVarChar, adParamInput, 100, secondaryPermitteeName)		   
		   .parameters.Append .CreateParameter("@permitteePhone", adVarChar, adParamInput, 50, permitteePhone)
		   .parameters.Append .CreateParameter("@permitteeAddress", adVarChar, adParamInput, 100, permitteeAddress)
		   .parameters.Append .CreateParameter("@permitteeCity", adVarChar, adParamInput, 50, permitteeCity)
		   .parameters.Append .CreateParameter("@permitteeState", adVarChar, adParamInput, 50, permitteeState)
		   .parameters.Append .CreateParameter("@permitteeZip", adVarChar, adParamInput, 50, permitteeZip)
		   .parameters.Append .CreateParameter("@primaryPermitteeName", adVarChar, adParamInput, 100, primaryPermitteeName)
		   .parameters.Append .CreateParameter("@primaryPermitteePhone", adVarChar, adParamInput, 50, primaryPermitteePhone)
		   .parameters.Append .CreateParameter("@primaryPermitteeAddress", adVarChar, adParamInput, 100, primaryPermitteeAddress)
		   .parameters.Append .CreateParameter("@primaryPermitteeCity", adVarChar, adParamInput, 50, primaryPermitteeCity)
		   .parameters.Append .CreateParameter("@primaryPermitteeState", adVarChar, adParamInput, 50, primaryPermitteeState)
		   .parameters.Append .CreateParameter("@primaryPermitteeZip", adVarChar, adParamInput, 50, primaryPermitteeZip)
		   .parameters.Append .CreateParameter("@facilityContact", adVarChar, adParamInput, 100, facilityContact)
		   .parameters.Append .CreateParameter("@facilityContactPhone", adVarChar, adParamInput, 50, facilityContactPhone)		   		   
		   .parameters.Append .CreateParameter("@startDate", adDBTimeStamp, adParamInput, 8, startDate)
		   .parameters.Append .CreateParameter("@completionDate", adDBTimeStamp, adParamInput, 8, completionDate)
		   .parameters.Append .CreateParameter("@estimatedDisturbedAcerage", adDouble, adParamInput, , estimatedDisturbedAcerage)
		   .parameters.Append .CreateParameter("@typeConstructionCommercial", adBoolean, adParamInput, 1, isOn(typeConstructionCommercial))
		   .parameters.Append .CreateParameter("@typeConstructionIndustrial", adBoolean, adParamInput, 1, isOn(typeConstructionIndustrial))
		   .parameters.Append .CreateParameter("@typeConstructionMunicipal", adBoolean, adParamInput, 1, isOn(typeConstructionMunicipal))
		   .parameters.Append .CreateParameter("@typeConstructionDOT", adBoolean, adParamInput, 1, isOn(typeConstructionDOT))
		   .parameters.Append .CreateParameter("@typeConstructionUtility", adBoolean, adParamInput, 1, isOn(typeConstructionUtility))
		   .parameters.Append .CreateParameter("@typeConstructionResidential", adBoolean, adParamInput, 1, isOn(typeConstructionResidential))
		   .parameters.Append .CreateParameter("@IRWName", adVarChar, adParamInput, 100, IRWName)
		   .parameters.Append .CreateParameter("@IRWTrout", adBoolean, adParamInput, 1, isOn(IRWTrout))
		   .parameters.Append .CreateParameter("@IRWWarmWater", adBoolean, adParamInput, 1, isOn(IRWWarmWater))
		   .parameters.Append .CreateParameter("@MSSSOwnerOperator", adVarChar, adParamInput, 100, MSSSOwnerOperator)
		   .parameters.Append .CreateParameter("@MSSSTrout", adBoolean, adParamInput, 1, isOn(MSSSTrout))
		   .parameters.Append .CreateParameter("@MSSSWarmWater", adBoolean, adParamInput, 1, isOn(MSSSWarmWater))
		   .parameters.Append .CreateParameter("@certify1", adVarChar, adParamInput, 50, certify1)
		   .parameters.Append .CreateParameter("@certify2", adVarChar, adParamInput, 50, certify2)
		   .parameters.Append .CreateParameter("@printedName", adVarChar, adParamInput, 100, printedName)
		   .parameters.Append .CreateParameter("@title", adVarChar, adParamInput, 100, title)
		   .parameters.Append .CreateParameter("@signDate", adDBTimeStamp, adParamInput, 8, signDate)
		   'new fields 09/2008
		   .parameters.Append .CreateParameter("@noticeOfIntent", adInteger, adParamInput, 8, noticeOfIntent)
		   .parameters.Append .CreateParameter("@commonDevelopmentName", adVarChar, adParamInput, 100, commonDevelopmentName)
		   .parameters.Append .CreateParameter("@constructionSiteStreetAddress", adVarChar, adParamInput, 100, constructionSiteStreetAddress)		   	   
		   .parameters.Append .CreateParameter("@authorizedRep", adVarChar, adParamInput, 100, authorizedRep)
		   .parameters.Append .CreateParameter("@authorizedRepPhone", adVarChar, adParamInput, 50, authorizedRepPhone)
		   .parameters.Append .CreateParameter("@disturb50", adInteger, adParamInput, 8, disturb50)
		   .parameters.Append .CreateParameter("@RWName", adVarChar, adParamInput, 100, RWName)			   
		   .parameters.Append .CreateParameter("@siteDischargeStormWater", adInteger, adParamInput, 8, siteDischargeStormWater)
		   .parameters.Append .CreateParameter("@siteDischargeStormWater2", adInteger, adParamInput, 8, siteDischargeStormWater2)
		   .parameters.Append .CreateParameter("@impairedStream", adVarChar, adParamInput, 50, impairedStream)
		   .parameters.Append .CreateParameter("@impairedStream2", adVarChar, adParamInput, 50, impairedStream2)
		   .parameters.Append .CreateParameter("@writtenAuth", adBoolean, adParamInput, 1, isOn(writtenAuth))
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If

		secondaryFormID = rs("Identity")
		
		Set oCmd = nothing
		rs.close
		Set rs = nothing
		
	'	if primaryFormID = 0 then
	'		uniqueID = secondaryFormID
	'	else
	'		uniqueID = primaryFormID
	'	end if
		
		'***********create the pdf***********************************
		'create the PDF and store it in the downloads folder
		'page 1	
	'	arrX = Array(500,162,230,160,445,162,430,204,440,115,280,360,450,192,440,117,277,360,450,150,474,127,270,471,216,290,350,413,453,504,270,181,289,372,181,288,88,88,260,471,471) 'left to right
	'	arrY = Array(775,568,550,533,533,516,516,499,499,482,482,482,482,464,464,447,447,447,447,431,431,389,389,389,372,372,372,372,372,372,321,304,304,286,269,269,213,183,98,98,79) 'up and down
	'	arrText = Array(uniqueID,siteProjectName,siteLocation,city,county,subdivision,lotNumber,secondaryPermitteeName,permitteePhone,permitteeAddress, _
	'		permitteeCity,permitteeState,permitteeZip,primaryPermitteeName,primaryPermitteePhone,primaryPermitteeAddress,primaryPermitteeCity, _
	'		primaryPermitteeState,primaryPermitteeZip,facilityContact,facilityContactPhone,startDate,completionDate,estimatedDisturbedAcerage, _
	'		setx(typeConstructionCommercial),setx(typeConstructionIndustrial),setx(typeConstructionMunicipal),setx(typeConstructionDOT), _ 
	'		setx(typeConstructionUtility),setx(typeConstructionResidential),IRWName,setx(IRWTrout),setx(IRWWarmWater),MSSSOwnerOperator, _
	'		setx(MSSSTrout),setx(MSSSWarmWater),certify1,certify2,printedName,title,signDate)
	'	
	'	Set Pdf = Server.CreateObject("Persits.Pdf")
	'	Pdf.RegKey = sRegKey
	
		' Open blank PDF form from file
	'	Set Doc = Pdf.OpenDocument( Server.MapPath("pdfTemplates/NOISecondary.pdf") )
	
		' Create default font
	'	Set Font = Doc.Fonts("Helvetica")
	
		' Obtain the only page's canvas
	'	Set Canvas = Doc.Pages(1).Canvas
	
		' Create empty param object
	'	Set Param = Pdf.CreateParam	
	
		' Go over all items in arrays
	'	For j = 0 to UBound(arrX)
	
	'		Param("x") = arrX(j)
	'		Param("y") = arrY(j) - 263 * i
	'		Param("color") = "black"
	'		Param("html") = true
			' Draw text on canvas
	'		Page.ResetCoordinates
	'		Canvas.DrawText arrText(j), Param, Font
	
	'	Next ' j
		
		' Save document, the Save method returns generated file name
	'	if primaryFormID = 0 then
	'		Filename = Doc.Save( Server.MapPath("downloads/NOI/NOISecondary_" & secondaryFormID & ".pdf"), True )
	'	else
	'		Filename = Doc.Save( Server.MapPath("downloads/NOI/NOISecondary_" & primaryFormID & "_" & secondaryFormID & ".pdf"), True )
	'	end if
	
		'create the PDF using the above report ID
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildSecondaryNOIGAPDF.asp?secondaryFormID=" & secondaryFormID', "landscape=true"
		
		if primaryFormID = 0 then
			Filename = Doc.Save( Server.MapPath("downloads/NOI/NOISecondary_" & secondaryFormID & ".pdf"), True )
		else
			Filename = Doc.Save( Server.MapPath("downloads/NOI/NOISecondary_" & primaryFormID & "_" & secondaryFormID & ".pdf"), True )
		end if
		
		'***********end creating the pdf***********************************		
			
		response.Redirect "secondaryList.asp?primaryID=" & primaryFormID & "&projectID=" & projectID
		
	case "editSecondary"
		
		projectID = checkNull(request("projectID"))
		secondaryFormID = checkNull(request("secondaryFormID"))
		primaryFormID = checkNull(request("primaryID"))
		siteProjectName = checkNull(request("siteProjectName"))
		siteLocation = checkNull(request("siteLocation"))
		city = checkNull(request("city"))
		county = checkNull(request("county"))
		subdivision = checkNull(request("subdivision"))
		lotNumber = checkNull(request("lotNumber"))
		secondaryPermitteeName = checkNull(request("secondaryPermitteeName"))
		permitteePhone = checkNull(request("permitteePhone"))
		permitteeAddress = checkNull(request("permitteeAddress"))
		permitteeCity = checkNull(request("permitteeCity"))
		permitteeState = checkNull(request("permitteeState"))
		permitteeZip = checkNull(request("permitteeZip"))
		primaryPermitteeName = checkNull(request("primaryPermitteeName"))
		primaryPermitteePhone = checkNull(request("primaryPermitteePhone"))
		primaryPermitteeAddress = checkNull(request("primaryPermitteeAddress"))
		primaryPermitteeCity = checkNull(request("primaryPermitteeCity"))
		primaryPermitteeState = checkNull(request("primaryPermitteeState"))
		primaryPermitteeZip = checkNull(request("primaryPermitteeZip"))
		facilityContact = checkNull(request("facilityContact"))
		facilityContactPhone = checkNull(request("facilityContactPhone"))
		startDate = checkNull(request("startDate"))
		completionDate = checkNull(request("completionDate"))
		estimatedDisturbedAcerage = checkNull(request("estimatedDisturbedAcerage"))
		typeConstructionCommercial = checkNull(request("typeConstructionCommercial"))
		typeConstructionIndustrial = checkNull(request("typeConstructionIndustrial"))
		typeConstructionMunicipal = checkNull(request("typeConstructionMunicipal"))
		typeConstructionDOT = checkNull(request("typeConstructionDOT"))
		typeConstructionUtility = checkNull(request("typeConstructionUtility"))
		typeConstructionResidential = checkNull(request("typeConstructionResidential"))
		IRWName = checkNull(request("IRWName"))
		IRWTrout = checkNull(request("IRWTrout"))
		IRWWarmWater = checkNull(request("IRWWarmWater"))
		MSSSOwnerOperator = checkNull(request("MSSSOwnerOperator"))
		MSSSTrout = checkNull(request("MSSSTrout"))
		MSSSWarmWater = checkNull(request("MSSSWarmWater"))
		certify1 = checkNull(request("certify1"))
		certify2 = checkNull(request("certify2"))
		printedName = checkNull(request("printedName"))
		title = checkNull(request("title"))
		signDate = checkNull(request("signDate"))
		
		
		'new fields 09/2008
		noticeOfIntent= checkNull(request("noticeOfIntent"))
		commonDevelopmentName = checkNull(request("commonDevelopmentName"))
		constructionSiteStreetAddress = checkNull(request("constructionSiteStreetAddress"))
		authorizedRep= checkNull(request("authorizedRep"))
		authorizedRepPhone= checkNull(request("authorizedRepPhone"))
		disturb50= checkNull(request("disturb50"))
		RWName= checkNull(request("RWName"))		
		siteDischargeStormWater= checkNull(request("siteDischargeStormWater"))
		siteDischargeStormWater2= checkNull(request("siteDischargeStormWater2"))
		impairedStream= checkNull(request("impairedStream"))
		impairedStream2= checkNull(request("impairedStream2"))
		writtenAuth= checkNull(request("writtenAuth"))
	
		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If					
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditSecondaryNOI"
		   .parameters.Append .CreateParameter("@secondaryFormID", adInteger, adParamInput, 8, secondaryFormID)
		   .parameters.Append .CreateParameter("@siteProjectName", adVarChar, adParamInput, 100, siteProjectName)
		   .parameters.Append .CreateParameter("@siteLocation", adVarChar, adParamInput, 200, siteLocation)
		   .parameters.Append .CreateParameter("@city", adVarChar, adParamInput, 50, city)
		   .parameters.Append .CreateParameter("@county", adVarChar, adParamInput, 50, county)
		   .parameters.Append .CreateParameter("@subdivision", adVarChar, adParamInput, 100, subdivision)
		   .parameters.Append .CreateParameter("@lotNumber", adVarChar, adParamInput, 100, lotNumber)
		   .parameters.Append .CreateParameter("@secondaryPermitteeName", adVarChar, adParamInput, 100, secondaryPermitteeName)		   
		   .parameters.Append .CreateParameter("@permitteePhone", adVarChar, adParamInput, 50, permitteePhone)
		   .parameters.Append .CreateParameter("@permitteeAddress", adVarChar, adParamInput, 100, permitteeAddress)
		   .parameters.Append .CreateParameter("@permitteeCity", adVarChar, adParamInput, 50, permitteeCity)
		   .parameters.Append .CreateParameter("@permitteeState", adVarChar, adParamInput, 50, permitteeState)
		   .parameters.Append .CreateParameter("@permitteeZip", adVarChar, adParamInput, 50, permitteeZip)
		   .parameters.Append .CreateParameter("@primaryPermitteeName", adVarChar, adParamInput, 100, primaryPermitteeName)
		   .parameters.Append .CreateParameter("@primaryPermitteePhone", adVarChar, adParamInput, 50, primaryPermitteePhone)
		   .parameters.Append .CreateParameter("@primaryPermitteeAddress", adVarChar, adParamInput, 100, primaryPermitteeAddress)
		   .parameters.Append .CreateParameter("@primaryPermitteeCity", adVarChar, adParamInput, 50, primaryPermitteeCity)
		   .parameters.Append .CreateParameter("@primaryPermitteeState", adVarChar, adParamInput, 50, primaryPermitteeState)
		   .parameters.Append .CreateParameter("@primaryPermitteeZip", adVarChar, adParamInput, 50, primaryPermitteeZip)
		   .parameters.Append .CreateParameter("@facilityContact", adVarChar, adParamInput, 100, facilityContact)
		   .parameters.Append .CreateParameter("@facilityContactPhone", adVarChar, adParamInput, 50, facilityContactPhone)		   		   
		   .parameters.Append .CreateParameter("@startDate", adDBTimeStamp, adParamInput, 8, startDate)
		   .parameters.Append .CreateParameter("@completionDate", adDBTimeStamp, adParamInput, 8, completionDate)
		   .parameters.Append .CreateParameter("@estimatedDisturbedAcerage", adDouble, adParamInput, , estimatedDisturbedAcerage)
		   .parameters.Append .CreateParameter("@typeConstructionCommercial", adBoolean, adParamInput, 1, isOn(typeConstructionCommercial))
		   .parameters.Append .CreateParameter("@typeConstructionIndustrial", adBoolean, adParamInput, 1, isOn(typeConstructionIndustrial))
		   .parameters.Append .CreateParameter("@typeConstructionMunicipal", adBoolean, adParamInput, 1, isOn(typeConstructionMunicipal))
		   .parameters.Append .CreateParameter("@typeConstructionDOT", adBoolean, adParamInput, 1, isOn(typeConstructionDOT))
		   .parameters.Append .CreateParameter("@typeConstructionUtility", adBoolean, adParamInput, 1, isOn(typeConstructionUtility))
		   .parameters.Append .CreateParameter("@typeConstructionResidential", adBoolean, adParamInput, 1, isOn(typeConstructionResidential))
		   .parameters.Append .CreateParameter("@IRWName", adVarChar, adParamInput, 100, IRWName)
		   .parameters.Append .CreateParameter("@IRWTrout", adBoolean, adParamInput, 1, isOn(IRWTrout))
		   .parameters.Append .CreateParameter("@IRWWarmWater", adBoolean, adParamInput, 1, isOn(IRWWarmWater))
		   .parameters.Append .CreateParameter("@MSSSOwnerOperator", adVarChar, adParamInput, 100, MSSSOwnerOperator)
		   .parameters.Append .CreateParameter("@MSSSTrout", adBoolean, adParamInput, 1, isOn(MSSSTrout))
		   .parameters.Append .CreateParameter("@MSSSWarmWater", adBoolean, adParamInput, 1, isOn(MSSSWarmWater))
		   .parameters.Append .CreateParameter("@certify1", adVarChar, adParamInput, 50, certify1)
		   .parameters.Append .CreateParameter("@certify2", adVarChar, adParamInput, 50, certify2)
		   .parameters.Append .CreateParameter("@printedName", adVarChar, adParamInput, 100, printedName)
		   .parameters.Append .CreateParameter("@title", adVarChar, adParamInput, 100, title)
		   .parameters.Append .CreateParameter("@signDate", adDBTimeStamp, adParamInput, 8, signDate)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
		1	<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
	
		Set oCmd = nothing
		rs.close
		Set rs = nothing
		
	'	if primaryFormID = 0 then
	'		uniqueID = secondaryFormID
	'	else
	'		uniqueID = primaryFormID
	'	end if
		'***********create the pdf***********************************
		'create the PDF and store it in the downloads folder
		'page 1	
	'	arrX = Array(450,162,230,160,445,162,430,204,440,115,280,360,450,192,440,117,277,360,450,150,474,127,270,471,216,290,350,413,453,504,270,181,289,372,181,288,88,88,260,471,471) 'left to right
	'	arrY = Array(775,568,550,533,533,516,516,499,499,482,482,482,482,464,464,447,447,447,447,431,431,389,389,389,372,372,372,372,372,372,321,304,304,286,269,269,213,183,98,98,79) 'up and down
	'	arrText = Array(uniqueID,siteProjectName,siteLocation,city,county,subdivision,lotNumber,secondaryPermitteeName,permitteePhone,permitteeAddress, _
	'		permitteeCity,permitteeState,permitteeZip,primaryPermitteeName,primaryPermitteePhone,primaryPermitteeAddress,primaryPermitteeCity, _
	'		primaryPermitteeState,primaryPermitteeZip,facilityContact,facilityContactPhone,startDate,completionDate,estimatedDisturbedAcerage, _
	'		setx(typeConstructionCommercial),setx(typeConstructionIndustrial),setx(typeConstructionMunicipal),setx(typeConstructionDOT), _ 
	'		setx(typeConstructionUtility),setx(typeConstructionResidential),IRWName,setx(IRWTrout),setx(IRWWarmWater),MSSSOwnerOperator, _
	'		setx(MSSSTrout),setx(MSSSWarmWater),certify1,certify2,printedName,title,signDate)
		
	'	Set Pdf = Server.CreateObject("Persits.Pdf")
	'	Pdf.RegKey = sRegKey
	
		' Open blank PDF form from file
	'	Set Doc = Pdf.OpenDocument( Server.MapPath("pdfTemplates/NOISecondary.pdf") )
	
		' Create default font
	'	Set Font = Doc.Fonts("Helvetica")
	
		' Obtain the only page's canvas
	'	Set Canvas = Doc.Pages(1).Canvas
	
		' Create empty param object
	'	Set Param = Pdf.CreateParam	
	
		' Go over all items in arrays
	'	For j = 0 to UBound(arrX)
	'
	'		Param("x") = arrX(j)
	'		Param("y") = arrY(j) - 263 * i
	'		Param("color") = "black"
	'		Param("html") = true
	'		' Draw text on canvas
	'		Page.ResetCoordinates
	'		Canvas.DrawText arrText(j), Param, Font
	'
	'	Next ' j
	
		'create the PDF using the above report ID
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "buildSecondaryNOIGAPDF.asp?secondaryFormID=" & secondaryFormID', "landscape=true"
		
		if primaryFormID = 0 then
			Filename = Doc.Save( Server.MapPath("downloads/NOI/NOISecondary_" & secondaryFormID & ".pdf"), True )
		else
			Filename = Doc.Save( Server.MapPath("downloads/NOI/NOISecondary_" & primaryFormID & "_" & secondaryFormID & ".pdf"), True )
		end if
		
		' Save document, the Save method returns generated file name
	'	if primaryFormID = 0 then
	'		Filename = Doc.Save( Server.MapPath("downloads/NOI/NOISecondary_" & secondaryFormID & ".pdf"), True )
	'	else
	'		Filename = Doc.Save( Server.MapPath("downloads/NOI/NOISecondary_" & primaryFormID & "_" & secondaryFormID & ".pdf"), True )
	'	end if
		
		'***********end creating the pdf***********************************
		
		response.Redirect "secondaryList.asp?primaryID=" & primaryFormID & "&projectID=" & projectID
		
	case "deleteSecondary"
		secondaryFormID = Request("id")
	
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If					
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteSecondaryNOI"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, secondaryFormID)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		Set oCmd = nothing
		
		'delete the corresponding pdf from the server
		
		sFile = Server.MapPath("downloads/NOI/NOISecondary_" & secondaryFormID & ".pdf")
		
		Set fso = CreateObject("Scripting.FileSystemObject")
		
		'Delete the file
		fso.DeleteFile sFile, True
		
		'delete the fee for associated with this form
		
		response.Redirect "secondaryList.asp"
				
	Case "addBlanketSecondary"
		projectID = request("projectID")
		secondaryPermitteeName= request("secondaryPermitteeName")
		permitteePhone= request("permitteePhone")
		permitteeAddress= request("permitteeAddress")
		permitteeCity= request("permitteeCity")
		permitteeState= request("permitteeState")
		permitteeZip= request("permitteeZip")
		subContractorName= request("subContractorName")
		subContractorPhone= request("subContractorPhone")
		subContractorAddress= request("subContractorAddress")
		subContractorCity= request("subContractorCity")
		subContractorState= request("subContractorState")
		subContractorZip= request("subContractorZip")
		facilityContact= request("facilityContact")
		facilityContactPhone= request("facilityContactPhone")
		typeConstructionCommercial= request("typeConstructionCommercial")
		typeConstructionIndustrial= request("typeConstructionIndustrial")
		typeConstructionMunicipal= request("typeConstructionMunicipal")
		typeConstructionDOT= request("typeConstructionDOT")
		typeConstructionUtility= request("typeConstructionUtility")
		typeConstructionResidential= request("typeConstructionResidential")
		certify1= request("certify1")
		certify2= request("certify2")
		permitteePrintedName= request("permitteePrintedName")
		permitteeTitle= request("permitteeTitle")
		signDate= request("signDate")
		
		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddBlanketSecondaryNOI"
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
		   .parameters.Append .CreateParameter("@secondaryPermitteeName", adVarChar, adParamInput, 100, secondaryPermitteeName)
		   .parameters.Append .CreateParameter("@permitteePhone", adVarChar, adParamInput, 50, permitteePhone)
		   .parameters.Append .CreateParameter("@permitteeAddress", adVarChar, adParamInput, 100, permitteeAddress)
		   .parameters.Append .CreateParameter("@permitteeCity", adVarChar, adParamInput, 100, permitteeCity)
		   .parameters.Append .CreateParameter("@permitteeState", adVarChar, adParamInput, 50, permitteeState)
		   .parameters.Append .CreateParameter("@permitteeZip", adVarChar, adParamInput, 50, permitteeZip)
		   .parameters.Append .CreateParameter("@subContractorName", adVarChar, adParamInput, 100, subContractorName)		   
		   .parameters.Append .CreateParameter("@subContractorPhone", adVarChar, adParamInput, 50, subContractorPhone)
		   .parameters.Append .CreateParameter("@subContractorAddress", adVarChar, adParamInput, 100, subContractorAddress)
		   .parameters.Append .CreateParameter("@subContractorCity", adVarChar, adParamInput, 100, subContractorCity)
		   .parameters.Append .CreateParameter("@subContractorState", adVarChar, adParamInput, 50, subContractorState)
		   .parameters.Append .CreateParameter("@subContractorZip", adVarChar, adParamInput, 50, subContractorZip)
		   .parameters.Append .CreateParameter("@facilityContact", adVarChar, adParamInput, 100, facilityContact)
		   .parameters.Append .CreateParameter("@facilityContactPhone", adVarChar, adParamInput, 50, facilityContactPhone)
		   .parameters.Append .CreateParameter("@typeConstructionCommercial", adBoolean, adParamInput, 1, isOn(typeConstructionCommercial))
		   .parameters.Append .CreateParameter("@typeConstructionIndustrial", adBoolean, adParamInput, 1, isOn(typeConstructionIndustrial))
		   .parameters.Append .CreateParameter("@typeConstructionMunicipal", adBoolean, adParamInput, 1, isOn(typeConstructionMunicipal))
		   .parameters.Append .CreateParameter("@typeConstructionDOT", adBoolean, adParamInput, 1, isOn(typeConstructionDOT))
		   .parameters.Append .CreateParameter("@typeConstructionUtility", adBoolean, adParamInput, 1, isOn(typeConstructionUtility))
		   .parameters.Append .CreateParameter("@typeConstructionResidential", adBoolean, adParamInput, 1, isOn(typeConstructionResidential))
		   .parameters.Append .CreateParameter("@certify1", adVarChar, adParamInput, 50, certify1)
		   .parameters.Append .CreateParameter("@certify2", adVarChar, adParamInput, 50, certify2)
		   .parameters.Append .CreateParameter("@permitteePrintedName", adVarChar, adParamInput, 100, permitteePrintedName)
		   .parameters.Append .CreateParameter("@permitteeTitle", adVarChar, adParamInput, 100, permitteeTitle)
		   .parameters.Append .CreateParameter("@signDate", adDBTimeStamp, adParamInput, 8, signDate)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		secondaryFormID = rs("Identity")
		
		Set oCmd = nothing
		
		'***********create the pdf***********************************
		'create the PDF and store it in the downloads folder
		'page 1	
		arrX = Array(205,439,117,278,359,450,205,438,117,278,359,450,149,475,217,290,351,413,454,505,90,90,263,470,466) 'left to right
		arrY = Array(535,535,518,518,518,518,501,501,484,484,484,484,468,468,416,416,416,416,416,416,357,326,231,231,213) 'up and down
		arrText = Array(secondaryPermitteeName,permitteePhone,permitteeAddress,permitteeCity,permitteeState,permitteeZip,subContractorName, _
			subContractorPhone,subContractorAddress,subContractorCity,subContractorState,subContractorZip,facilityContact,facilityContactPhone, _
			setx(typeConstructionCommercial),setx(typeConstructionIndustrial),setx(typeConstructionMunicipal),setx(typeConstructionDOT), _
			setx(typeConstructionUtility),setx(typeConstructionResidential),certify1,certify2,permitteePrintedName,permitteeTitle,signDate)
		
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
	
		' Open blank PDF form from file
		Set Doc = Pdf.OpenDocument( Server.MapPath("pdfTemplates/NOIBlanketSecondary.pdf") )
	
		' Create default font
		Set Font = Doc.Fonts("Helvetica")
	
		' Obtain the only page's canvas
		Set Canvas = Doc.Pages(1).Canvas
	
		' Create empty param object
		Set Param = Pdf.CreateParam	
	
		' Go over all items in arrays
		For j = 0 to UBound(arrX)
	
			Param("x") = arrX(j)
			Param("y") = arrY(j) - 263 * i
			Param("color") = "black"
			Param("size") = 8
			Param("html") = true
			' Draw text on canvas
			Page.ResetCoordinates
			Canvas.DrawText arrText(j), Param, Font
	
		Next ' j

		' Save document, the Save method returns generated file name
		Filename = Doc.Save( Server.MapPath("downloads/NOI/NOIBlanketSecondary_" & secondaryFormID & ".pdf"), True )
		
		'***********end creating the pdf***********************************
		
		response.Redirect "blanketSecondaryList.asp"
		
	Case "editBlanketSecondary"
		secondaryFormID = request("secondaryFormID")
		secondaryPermitteeName= request("secondaryPermitteeName")
		permitteePhone= request("permitteePhone")
		permitteeAddress= request("permitteeAddress")
		permitteeCity= request("permitteeCity")
		permitteeState= request("permitteeState")
		permitteeZip= request("permitteeZip")
		subContractorName= request("subContractorName")
		subContractorPhone= request("subContractorPhone")
		subContractorAddress= request("subContractorAddress")
		subContractorCity= request("subContractorCity")
		subContractorState= request("subContractorState")
		subContractorZip= request("subContractorZip")
		facilityContact= request("facilityContact")
		facilityContactPhone= request("facilityContactPhone")
		typeConstructionCommercial= request("typeConstructionCommercial")
		typeConstructionIndustrial= request("typeConstructionIndustrial")
		typeConstructionMunicipal= request("typeConstructionMunicipal")
		typeConstructionDOT= request("typeConstructionDOT")
		typeConstructionUtility= request("typeConstructionUtility")
		typeConstructionResidential= request("typeConstructionResidential")
		certify1= request("certify1")
		certify2= request("certify2")
		permitteePrintedName= request("permitteePrintedName")
		permitteeTitle= request("permitteeTitle")
		signDate= request("signDate")
		
		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditBlanketSecondaryNOI"
		   .parameters.Append .CreateParameter("@secondaryFormID", adInteger, adParamInput, 8, secondaryFormID)
		   .parameters.Append .CreateParameter("@secondaryPermitteeName", adVarChar, adParamInput, 100, secondaryPermitteeName)
		   .parameters.Append .CreateParameter("@permitteePhone", adVarChar, adParamInput, 50, permitteePhone)
		   .parameters.Append .CreateParameter("@permitteeAddress", adVarChar, adParamInput, 100, permitteeAddress)
		   .parameters.Append .CreateParameter("@permitteeCity", adVarChar, adParamInput, 100, permitteeCity)
		   .parameters.Append .CreateParameter("@permitteeState", adVarChar, adParamInput, 50, permitteeState)
		   .parameters.Append .CreateParameter("@permitteeZip", adVarChar, adParamInput, 50, permitteeZip)
		   .parameters.Append .CreateParameter("@subContractorName", adVarChar, adParamInput, 100, subContractorName)		   
		   .parameters.Append .CreateParameter("@subContractorPhone", adVarChar, adParamInput, 50, subContractorPhone)
		   .parameters.Append .CreateParameter("@subContractorAddress", adVarChar, adParamInput, 100, subContractorAddress)
		   .parameters.Append .CreateParameter("@subContractorCity", adVarChar, adParamInput, 100, subContractorCity)
		   .parameters.Append .CreateParameter("@subContractorState", adVarChar, adParamInput, 50, subContractorState)
		   .parameters.Append .CreateParameter("@subContractorZip", adVarChar, adParamInput, 50, subContractorZip)
		   .parameters.Append .CreateParameter("@facilityContact", adVarChar, adParamInput, 100, facilityContact)
		   .parameters.Append .CreateParameter("@facilityContactPhone", adVarChar, adParamInput, 50, facilityContactPhone)
		   .parameters.Append .CreateParameter("@typeConstructionCommercial", adBoolean, adParamInput, 1, isOn(typeConstructionCommercial))
		   .parameters.Append .CreateParameter("@typeConstructionIndustrial", adBoolean, adParamInput, 1, isOn(typeConstructionIndustrial))
		   .parameters.Append .CreateParameter("@typeConstructionMunicipal", adBoolean, adParamInput, 1, isOn(typeConstructionMunicipal))
		   .parameters.Append .CreateParameter("@typeConstructionDOT", adBoolean, adParamInput, 1, isOn(typeConstructionDOT))
		   .parameters.Append .CreateParameter("@typeConstructionUtility", adBoolean, adParamInput, 1, isOn(typeConstructionUtility))
		   .parameters.Append .CreateParameter("@typeConstructionResidential", adBoolean, adParamInput, 1, isOn(typeConstructionResidential))
		   .parameters.Append .CreateParameter("@certify1", adVarChar, adParamInput, 50, certify1)
		   .parameters.Append .CreateParameter("@certify2", adVarChar, adParamInput, 50, certify2)
		   .parameters.Append .CreateParameter("@permitteePrintedName", adVarChar, adParamInput, 100, permitteePrintedName)
		   .parameters.Append .CreateParameter("@permitteeTitle", adVarChar, adParamInput, 100, permitteeTitle)
		   .parameters.Append .CreateParameter("@signDate", adDBTimeStamp, adParamInput, 8, signDate)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		Set oCmd = nothing
		
		
		'***********edit the pdf***********************************
		'create the PDF and store it in the downloads folder
		'page 1	
		arrX = Array(205,439,117,278,359,450,205,438,117,278,359,450,149,475,217,290,351,413,454,505,90,90,263,470,466) 'left to right
		arrY = Array(535,535,518,518,518,518,501,501,484,484,484,484,468,468,416,416,416,416,416,416,357,326,231,231,213) 'up and down
		arrText = Array(secondaryPermitteeName,permitteePhone,permitteeAddress,permitteeCity,permitteeState,permitteeZip,subContractorName, _
			subContractorPhone,subContractorAddress,subContractorCity,subContractorState,subContractorZip,facilityContact,facilityContactPhone, _
			setx(typeConstructionCommercial),setx(typeConstructionIndustrial),setx(typeConstructionMunicipal),setx(typeConstructionDOT), _
			setx(typeConstructionUtility),setx(typeConstructionResidential),certify1,certify2,permitteePrintedName,permitteeTitle,signDate)
		
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
	
		' Open blank PDF form from file
		Set Doc = Pdf.OpenDocument( Server.MapPath("pdfTemplates/NOIBlanketSecondary.pdf") )
	
		' Create default font
		Set Font = Doc.Fonts("Helvetica")
	
		' Obtain the only page's canvas
		Set Canvas = Doc.Pages(1).Canvas
	
		' Create empty param object
		Set Param = Pdf.CreateParam	
	
		' Go over all items in arrays
		For j = 0 to UBound(arrX)
	
			Param("x") = arrX(j)
			Param("y") = arrY(j) - 263 * i
			Param("color") = "black"
			Param("size") = 8
			Param("html") = true
			' Draw text on canvas
			Page.ResetCoordinates
			Canvas.DrawText arrText(j), Param, Font
	
		Next ' j

		' Save document, the Save method returns generated file name
		Filename = Doc.Save( Server.MapPath("downloads/NOI/NOIBlanketSecondary_" & secondaryFormID & ".pdf"), True )
		
		'***********end editing the pdf***********************************
		
		
		response.Redirect "blanketSecondaryList.asp"
		
	case "deleteBlanketSecondary"
		secondaryFormID = Request("id")
	
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If					
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteBlanketSecondaryNOI"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, secondaryFormID)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		Set oCmd = nothing
		
		'delete the corresponding pdf from the server
		
		sFile = Server.MapPath("downloads/NOI/NOIBlanketSecondary_" & secondaryFormID & ".pdf")
		
		Set fso = CreateObject("Scripting.FileSystemObject")
		
		'Delete the file
		fso.DeleteFile sFile, True
		
		response.Redirect "secondaryList.asp"
		
	Case "sendEmail"
		'need to change
		'sServerPath = "/test6/noi/downloads/"
		projectID = request("projectID")
		sTo = request("emailTo")
		sCC = request("emailCC")
		sBCC = request("emailBCC")
		sSubject = request("subject")
		sBody = request("smessage")
		sAttach = request("attachment1")
		sAttach2 = request("attachment2")
		sAttach3 = request("attachment3")
		
		
		form1 = isOn(request("form1"))
		form2 = isOn(request("form2"))
		form3 = isOn(request("form3"))
		
		If form1 = "True" Then
			insAttach = sAttach
		else
			insAttach = ""
		end if
		
		If form2 = "True" Then
			insAttach2 = sAttach2
		else
			insAttach2 = ""
		end if
		
		If form3 = "True" Then
			insAttach3 = sAttach3
		else
			insAttach3 = ""
		end if
		
		'set for each form type
		If form1 = "True" Then
			if sAttach <> "" then
				sAttach = Server.MapPath(sServerNOIPath & sAttach)
			end if
		End If
		If form2 = "True" Then
			if sAttach2 <> "" then
				sAttach2 = Server.MapPath(sServerNOIPath & sAttach2)
			end if
		End If
		If form3 = "True" Then
			if sAttach3 <> "" then
				sAttach3  = Server.MapPath(sServerNOIPath & sAttach3)
			end if
		End If
		
		'############################################################################################################
		Set oCdoMail = Server.CreateObject("CDO.Message")
  		Set oCdoConf = Server.CreateObject("CDO.Configuration")
		
		sConfURL = "http://schemas.microsoft.com/cdo/configuration/"
	    with oCdoConf
		  .Fields.Item(sConfURL & "sendusing") = 2
		  .Fields.Item(sConfURL & "smtpserver") = "localhost"
		  .Fields.Item(sConfURL & "smtpserverport") = 25
		  .Fields.Update
	    end with
		
		with oCdoMail
		  .From = Session("Email")
		  .To = sTo
		  .CC = sCC
		  .BCC = sBCC
	    end with
		
		with oCdoMail
		  .Subject = sSubject
		  .TextBody = sBody
		  .HTMLBody = sBody
		  If form1 = "True" Then
		  	.AddAttachment (sAttach)
		  end if
		  
		  If form2 = "True" Then
		  	.AddAttachment (sAttach2)
		  end if
		  
		  If form3 = "True" Then
		  	.AddAttachment (sAttach3)
		  end if
		  
	    end with
		
		oCdoMail.Configuration = oCdoConf
	    oCdoMail.Send
	    Set oCdoConf = Nothing
	    Set oCdoMail = Nothing

		bSent = True

		'############################################################################################################
		
		'add the email details to the database		
		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddEmail"
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, Session("ID"))
		   .parameters.Append .CreateParameter("@sendto", adVarChar, adParamInput, 8000, sTo)
		   .parameters.Append .CreateParameter("@cc", adVarChar, adParamInput, 8000, sCC)
		   .parameters.Append .CreateParameter("@bcc", adVarChar, adParamInput, 8000, sBCC)
		   .parameters.Append .CreateParameter("@subject", adVarChar, adParamInput, 8000, sSubject)
		   .parameters.Append .CreateParameter("@message", adVarChar, adParamInput, 8000, sBody)
		   .parameters.Append .CreateParameter("@attach1", adVarChar, adParamInput, 200, insAttach)
		   .parameters.Append .CreateParameter("@attach2", adVarChar, adParamInput, 200, insAttach2)
		   .parameters.Append .CreateParameter("@attach3", adVarChar, adParamInput, 200, insAttach3)
		   .parameters.Append .CreateParameter("@dateSent", adDBTimeStamp, adParamInput, 8, now())
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
	
		'send the user to sent page
		response.Redirect "email_sent.asp"
		
	case "deleteEmail"
		emailID = Request("id")
	
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If					
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spDeleteEmail"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, emailID)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		Set oCmd = nothing
		
		response.Redirect "emailList.asp"
	
	Case "addFeeForm"
		projectID = request("projectID")
		primaryFormID = request("primaryFormID")
		primaryPermittee = request("primaryPermittee")
		permitteeAddress = request("permitteeAddress")
		permitteeCity = request("permitteeCity")
		permitteeState = request("permitteeState")
		permitteeZip = request("permitteeZip")
		contactTelephone = request("contactTelephone")
		projectName = request("projectName")
		projectAddress = request("projectAddress")
		projectCounty = request("projectCounty")
		projectCity = request("projectCity")
		acresDistirbedLocal = request("acresDistirbedLocal")
		acresDistirbedLocalCost = request("acresDistirbedLocalCost")
		acresDistirbedNoLocal = request("acresDistirbedNoLocal")
		acresDistirbedNoLocalCost = request("acresDistirbedNoLocalCost")
		acresDistirbedExempt = request("acresDistirbedExempt")
		acresDistirbedExemptCost = request("acresDistirbedExemptCost")
		totalFees = request("totalFees")
		checkNumber = request("checkNumber")
		dateSubmitted = checknull(request("dateSubmitted"))
		printedName = checknull(request("printedName"))
		title = checknull(request("title"))
		
		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddFeeForm"
		   .parameters.Append .CreateParameter("@primaryFormID", adInteger, adParamInput, 8, primaryFormID)
		   .parameters.Append .CreateParameter("@primaryPermittee", adVarChar, adParamInput, 100, primaryPermittee)
		   .parameters.Append .CreateParameter("@permitteeAddress", adVarChar, adParamInput, 100, permitteeAddress)
		   .parameters.Append .CreateParameter("@permitteeCity", adVarChar, adParamInput, 100, permitteeCity)
		   .parameters.Append .CreateParameter("@permitteeState", adVarChar, adParamInput, 50, permitteeState)
		   .parameters.Append .CreateParameter("@permitteeZip", adVarChar, adParamInput, 50, permitteeZip)
		   .parameters.Append .CreateParameter("@contactTelephone", adVarChar, adParamInput, 50, contactTelephone)
		   .parameters.Append .CreateParameter("@projectName", adVarChar, adParamInput, 200, projectName)
		   .parameters.Append .CreateParameter("@projectAddress", adVarChar, adParamInput, 100, projectAddress)
		   .parameters.Append .CreateParameter("@projectCounty", adVarChar, adParamInput, 100, projectCounty)
		   .parameters.Append .CreateParameter("@projectCity", adVarChar, adParamInput, 100, projectCity)
		   .parameters.Append .CreateParameter("@acresDistirbedLocal", adDouble, adParamInput, , acresDistirbedLocal)
		   .parameters.Append .CreateParameter("@acresDistirbedLocalCost", adCurrency, adParamInput, , acresDistirbedLocalCost)
		   .parameters.Append .CreateParameter("@acresDistirbedNoLocal", adDouble, adParamInput, , acresDistirbedNoLocal)
		   .parameters.Append .CreateParameter("@acresDistirbedNoLocalCost", adCurrency, adParamInput, , acresDistirbedNoLocalCost)
		   .parameters.Append .CreateParameter("@acresDistirbedExempt", adDouble, adParamInput, , acresDistirbedExempt)
		   .parameters.Append .CreateParameter("@acresDistirbedExemptCost", adCurrency, adParamInput, , acresDistirbedExemptCost)
		   .parameters.Append .CreateParameter("@totalFees", adCurrency, adParamInput, , totalFees)
		   .parameters.Append .CreateParameter("@checkNumber", adVarChar, adParamInput, 50, checkNumber)
		   .parameters.Append .CreateParameter("@dateSubmitted", adDBTimeStamp, adParamInput, 8, dateSubmitted)
		   .parameters.Append .CreateParameter("@printedName", adVarChar, adParamInput, 100, printedName)
		   .parameters.Append .CreateParameter("@title", adVarChar, adParamInput, 100, title)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		feeFormID = rs("Identity")
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		'***********create the pdf***********************************
		'create the PDF and store it in the downloads folder
		'page 1	
		
	
		arrX = Array(515,163,163,75,164,394,417,364,364,352,485,352,485,352,485,486,410,430,143,430) 'left to right
		arrY = Array(770,518,504,490,449,518,504,462,449,421,421,380,380,339,339,312,283,242,214,214) 'up and down
		arrText = Array(primaryFormID,primaryPermittee,permitteeAddress,permitteeCity & ", " & permitteeState & " " & permitteeZip,contactTelephone, _
			projectName,projectAddress,projectCounty,projectCity,acresDistirbedLocal,formatcurrency(acresDistirbedLocalCost,2), _
			acresDistirbedNoLocal,formatcurrency(acresDistirbedNoLocalCost,2),acresDistirbedExempt,formatCurrency(acresDistirbedExemptCost,2), _
			formatcurrency(totalFees,2),checkNumber,dateSubmitted,printedName,title)
		
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
	
		' Open blank PDF form from file
		Set Doc = Pdf.OpenDocument( Server.MapPath("pdfTemplates/npdesFeeForm.pdf") )
	

		' Create default font
		Set Font = Doc.Fonts("Helvetica")
	
		' Obtain the only page's canvas
		Set Canvas = Doc.Pages(1).Canvas
	
		' Create empty param object
		Set Param = Pdf.CreateParam	
	
		' Go over all items in arrays
		For j = 0 to UBound(arrX)
	
			Param("x") = arrX(j)
			Param("y") = arrY(j) - 263 * i
			Param("color") = "black"
			Param("size") = 10
			Param("html") = true
			' Draw text on canvas
			Page.ResetCoordinates
			Canvas.DrawText arrText(j), Param, Font
	
		Next ' j

		' Save document, the Save method returns generated file name
		Filename = Doc.Save( Server.MapPath("downloads/NOI/npdesFeeForm_" & primaryFormID & ".pdf"), True )
		
		'***********end creating the pdf***********************************
		
				
		response.Redirect "primaryList.asp?projectID=" & projectID
	
	Case "editFeeForm"
	
		projectID = request("projectID")
	
		feeFormID = request("feeFormID")
		primaryFormID = request("primaryFormID")
		primaryPermittee = request("primaryPermittee")
		permitteeAddress = request("permitteeAddress")
		permitteeCity = request("permitteeCity")
		permitteeState = request("permitteeState")
		permitteeZip = request("permitteeZip")
		contactTelephone = request("contactTelephone")
		projectName = request("projectName")
		projectAddress = request("projectAddress")
		projectCounty = request("projectCounty")
		projectCity = request("projectCity")
		acresDistirbedLocal = request("acresDistirbedLocal")
		acresDistirbedLocalCost = request("acresDistirbedLocalCost")
		acresDistirbedNoLocal = request("acresDistirbedNoLocal")
		acresDistirbedNoLocalCost = request("acresDistirbedNoLocalCost")
		acresDistirbedExempt = request("acresDistirbedExempt")
		acresDistirbedExemptCost = request("acresDistirbedExemptCost")
		totalFees = request("totalFees")
		checkNumber = request("checkNumber")
		dateSubmitted = request("dateSubmitted")
		printedName = request("printedName")
		title = request("title")
		
		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditFeeForm"
		   .parameters.Append .CreateParameter("@feeFormID", adInteger, adParamInput, 8, feeFormID)
		   .parameters.Append .CreateParameter("@primaryPermittee", adVarChar, adParamInput, 100, primaryPermittee)
		   .parameters.Append .CreateParameter("@permitteeAddress", adVarChar, adParamInput, 100, permitteeAddress)
		   .parameters.Append .CreateParameter("@permitteeCity", adVarChar, adParamInput, 100, permitteeCity)
		   .parameters.Append .CreateParameter("@permitteeState", adVarChar, adParamInput, 50, permitteeState)
		   .parameters.Append .CreateParameter("@permitteeZip", adVarChar, adParamInput, 50, permitteeZip)
		   .parameters.Append .CreateParameter("@contactTelephone", adVarChar, adParamInput, 50, contactTelephone)
		   .parameters.Append .CreateParameter("@projectName", adVarChar, adParamInput, 200, projectName)
		   .parameters.Append .CreateParameter("@projectAddress", adVarChar, adParamInput, 100, projectAddress)
		   .parameters.Append .CreateParameter("@projectCounty", adVarChar, adParamInput, 100, projectCounty)
		   .parameters.Append .CreateParameter("@projectCity", adVarChar, adParamInput, 100, projectCity)
		   .parameters.Append .CreateParameter("@acresDistirbedLocal", adDouble, adParamInput, , acresDistirbedLocal)
		   .parameters.Append .CreateParameter("@acresDistirbedLocalCost", adCurrency, adParamInput, , acresDistirbedLocalCost)
		   .parameters.Append .CreateParameter("@acresDistirbedNoLocal", adDouble, adParamInput, , acresDistirbedNoLocal)
		   .parameters.Append .CreateParameter("@acresDistirbedNoLocalCost", adCurrency, adParamInput, , acresDistirbedNoLocalCost)
		   .parameters.Append .CreateParameter("@acresDistirbedExempt", adDouble, adParamInput, , acresDistirbedExempt)
		   .parameters.Append .CreateParameter("@acresDistirbedExemptCost", adCurrency, adParamInput, , acresDistirbedExemptCost)
		   .parameters.Append .CreateParameter("@totalFees", adCurrency, adParamInput, , totalFees)
		   .parameters.Append .CreateParameter("@checkNumber", adVarChar, adParamInput, 50, checkNumber)
		   .parameters.Append .CreateParameter("@dateSubmitted", adDBTimeStamp, adParamInput, 8, dateSubmitted)
		   .parameters.Append .CreateParameter("@printedName", adVarChar, adParamInput, 100, printedName)
		   .parameters.Append .CreateParameter("@title", adVarChar, adParamInput, 100, title)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		'***********create the pdf***********************************
		'create the PDF and store it in the downloads folder
		'page 1	
		
	
		arrX = Array(465,163,163,75,164,394,417,364,364,352,485,352,485,352,485,486,410,430,143,430) 'left to right
		arrY = Array(770,518,504,490,449,518,504,462,449,421,421,380,380,339,339,312,283,242,214,214) 'up and down
		arrText = Array(primaryFormID,primaryPermittee,permitteeAddress,permitteeCity & ", " & permitteeState & " " & permitteeZip,contactTelephone, _
			projectName,projectAddress,projectCounty,projectCity,acresDistirbedLocal,formatcurrency(acresDistirbedLocalCost,2), _
			acresDistirbedNoLocal,formatcurrency(acresDistirbedNoLocalCost,2),acresDistirbedExempt,formatCurrency(acresDistirbedExemptCost,2), _
			formatcurrency(totalFees,2),checkNumber,dateSubmitted,printedName,title)
		
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
	
		' Open blank PDF form from file
		Set Doc = Pdf.OpenDocument( Server.MapPath("pdfTemplates/npdesFeeForm.pdf") )
	
		' Create default font
		Set Font = Doc.Fonts("Helvetica")
	
		' Obtain the only page's canvas
		Set Canvas = Doc.Pages(1).Canvas
	
		' Create empty param object
		Set Param = Pdf.CreateParam	
	
		' Go over all items in arrays
		For j = 0 to UBound(arrX)
	
			Param("x") = arrX(j)
			Param("y") = arrY(j) - 263 * i
			Param("color") = "black"
			Param("size") = 10
			Param("html") = true
			' Draw text on canvas
			Page.ResetCoordinates
			Canvas.DrawText arrText(j), Param, Font
	
		Next ' j

		' Save document, the Save method returns generated file name
		Filename = Doc.Save( Server.MapPath("downloads/NOI/npdesFeeForm_" & primaryFormID & ".pdf"), True )
		
		'***********end creating the pdf***********************************
		
		response.Redirect "primaryList.asp?projectID=" & projectID
		
	Case "setArchive"
		projectID = request("projectID")
		setState = request("set")
		formType = request("fType")
		if fType = "secondary" then
			primaryID = request("id")
		else
			primaryID = request("primaryID")
		end if		
		
		
		if setState = 1 then
			setState = true
		else
			setState = false
		end if
		
		
		
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If	
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		select Case formType
			Case "primary"
				sp = "spSetArchivePrimary"
			Case "secondary"
				sp = "spSetArchiveSecondary"
		end select
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = sp
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, primaryID)
		   .parameters.Append .CreateParameter("@isActive", adBoolean, adParamInput, 1, setState)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		set rs = nothing
		
		select case formType
			case "primary"
				response.Redirect "primaryList.asp?projectID=" & projectID
			case "secondary"
				response.Redirect "secondaryList.asp?primaryID=" & primaryID & "&projectID=" & projectID
		end select
		
	case "addNOTSec"
		
		projectID = request("projectID")
		secondaryFormID = request("secondaryFormID")
		primaryFormID = request("primaryID")
		coverageDesired= request("coverageDesired")
		siteProjectName= request("siteProjectName")
		GPSDegree1= request("degree1")
		GPSMinute1= request("minute1")
		GPSSecond1= request("second1")
		GPSLat =  request("latitude")
		GPSDegree2= request("degree2")
		GPSMinute2= request("minute2")
		GPSSecond2= request("second2")
		GPSLon =  request("longitude")
		projectAddress= request("projectAddress")
		projectCity= request("projectCity")
		projectCounty= request("projectCounty")
		subdivision= request("subdivision")
		lotNumber= request("lotNumber")
		ownerName= request("ownerName")
		ownerAddress= request("ownerAddress")
		ownerCity= request("ownerCity")
		ownerState= request("ownerState")
		ownerZip= request("ownerZip")
		operatorName= request("operatorName")
		operatorPhone= request("operatorPhone")
		operatorAddress= request("operatorAddress")
		operatorCity= request("operatorCity")
		operatorState= request("operatorState")
		operatorZip= request("operatorZip")
		typePrimary= request("typePrimary")
		typeSecondary= request("typeSecondary")
		typeTertiary= request("typeTertiary")		
		facilityContact= request("facilityContact")
		facilityContactPhone= request("facilityContactPhone")
		primaryPermitteeName= request("primaryPermitteeName")
		primaryPermitteePhone= request("primaryPermitteePhone")
		primaryPermitteeAddress= request("primaryPermitteeAddress")
		primaryPermitteeCity= request("primaryPermitteeCity")
		primaryPermitteeState= request("primaryPermitteeState")
		primaryPermitteeZip= request("primaryPermitteeZip")
		secondaryPermittees= request("secondaryPermittees")		
		constructionActivityCompleted= request("constructionActivityCompleted")
		noLongerOwnerOperator= request("noLongerOwnerOperator")		
		typeConstructionCommercial= request("typeConstructionCommercial")
		typeConstructionIndustrial= request("typeConstructionIndustrial")
		typeConstructionMunicipal= request("typeConstructionMunicipal")
		typeConstructionDOT= request("typeConstructionDOT")
		typeConstructionUtility= request("typeConstructionUtility")
		typeConstructionResidential= request("typeConstructionResidential")		
		primaryPermitteeSubdivision= request("primaryPermitteeSubdivision")
		individualLot= request("individualLot")
		individualLotWithinSWDA= request("individualLotWithinSWDA")		
		IRWName= request("IRWName")
		MSSSOwnerOperator= request("MSSSOwnerOperator")
		RWName= request("RWName")
		certify1= request("certify1")
		certify2= request("certify2")
		ownerPrintedName= request("ownerPrintedName")
		ownerTitle= request("ownerTitle")
		ownerSignDate= request("ownerSignDate")
		operatorPrintedName= request("operatorPrintedName")
		operatorTitle= request("operatorTitle")
		operatorSignDate= request("operatorSignDate")
	
		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If					
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spAddNOTSec"
		   .parameters.Append .CreateParameter("@secondaryFormID", adInteger, adParamInput, 8, secondaryFormID)
		   .parameters.Append .CreateParameter("@coverageDesired", adInteger, adParamInput, 8, coverageDesired)
		   .parameters.Append .CreateParameter("@siteProjectName", adVarChar, adParamInput, 200, siteProjectName)
		   .parameters.Append .CreateParameter("@GPSDegree1", adVarChar, adParamInput, 50, GPSDegree1)
		   .parameters.Append .CreateParameter("@GPSMinute1", adVarChar, adParamInput, 50, GPSMinute1)
		   .parameters.Append .CreateParameter("@GPSSecond1", adVarChar, adParamInput, 50, GPSSecond1)
		   .parameters.Append .CreateParameter("@GPSLat", adVarChar, adParamInput, 50, GPSLat)
		   .parameters.Append .CreateParameter("@GPSDegree2", adVarChar, adParamInput, 50, GPSDegree2)
		   .parameters.Append .CreateParameter("@GPSMinute2", adVarChar, adParamInput, 50, GPSMinute2)
		   .parameters.Append .CreateParameter("@GPSSecond2", adVarChar, adParamInput, 50, GPSSecond2)
		   .parameters.Append .CreateParameter("@GPSLon", adVarChar, adParamInput, 50, GPSLon)
		   .parameters.Append .CreateParameter("@projectAddress", adVarChar, adParamInput, 100, projectAddress)
		   .parameters.Append .CreateParameter("@projectCity", adVarChar, adParamInput, 100, projectCity)
		   .parameters.Append .CreateParameter("@projectCounty", adVarChar, adParamInput, 100, projectCounty)
		   .parameters.Append .CreateParameter("@subdivision", adVarChar, adParamInput, 150, subdivision)
		   .parameters.Append .CreateParameter("@lotNumber", adVarChar, adParamInput, 150, lotNumber)
		   .parameters.Append .CreateParameter("@ownerName", adVarChar, adParamInput, 100, ownerName)		   
		   .parameters.Append .CreateParameter("@ownerAddress", adVarChar, adParamInput, 100, ownerAddress)
		   .parameters.Append .CreateParameter("@ownerCity", adVarChar, adParamInput, 100, ownerCity)
		   .parameters.Append .CreateParameter("@ownerState", adVarChar, adParamInput, 50, ownerState)
		   .parameters.Append .CreateParameter("@ownerZip", adVarChar, adParamInput, 50, ownerZip)
		   .parameters.Append .CreateParameter("@operatorName", adVarChar, adParamInput, 100, operatorName)
		   .parameters.Append .CreateParameter("@operatorPhone", adVarChar, adParamInput, 50, operatorPhone)
		   .parameters.Append .CreateParameter("@operatorAddress", adVarChar, adParamInput, 100, operatorAddress)
		   .parameters.Append .CreateParameter("@operatorCity", adVarChar, adParamInput, 100, operatorCity)
		   .parameters.Append .CreateParameter("@operatorState", adVarChar, adParamInput, 50, operatorState)
		   .parameters.Append .CreateParameter("@operatorZip", adVarChar, adParamInput, 50, operatorZip)
		   .parameters.Append .CreateParameter("@typePrimary", adBoolean, adParamInput, 1, isOn(typePrimary))
		   .parameters.Append .CreateParameter("@typeSecondary", adBoolean, adParamInput, 1, isOn(typeSecondary))
		   .parameters.Append .CreateParameter("@typeTertiary", adBoolean, adParamInput, 1, isOn(typeTertiary))		   
		   .parameters.Append .CreateParameter("@facilityContact", adVarChar, adParamInput, 100, facilityContact)
		   .parameters.Append .CreateParameter("@facilityContactPhone", adVarChar, adParamInput, 50, facilityContactPhone)		   
		   .parameters.Append .CreateParameter("@primaryPermitteeName", adVarChar, adParamInput, 100, primaryPermitteeName)
		   .parameters.Append .CreateParameter("@primaryPermitteePhone", adVarChar, adParamInput, 50, primaryPermitteePhone)
		   .parameters.Append .CreateParameter("@primaryPermitteeAddress", adVarChar, adParamInput, 100, primaryPermitteeAddress)
		   .parameters.Append .CreateParameter("@primaryPermitteeCity", adVarChar, adParamInput, 100, primaryPermitteeCity)
		   .parameters.Append .CreateParameter("@primaryPermitteeState", adVarChar, adParamInput, 50, primaryPermitteeState)
		   .parameters.Append .CreateParameter("@primaryPermitteeZip", adVarChar, adParamInput, 50, primaryPermitteeZip)
		   .parameters.Append .CreateParameter("@secondaryPermittees", adVarChar, adParamInput, 50, secondaryPermittees)	   
		   .parameters.Append .CreateParameter("@constructionActivityCompleted", adBoolean, adParamInput, 1, isOn(constructionActivityCompleted))
		   .parameters.Append .CreateParameter("@noLongerOwnerOperator", adBoolean, adParamInput, 1, isOn(noLongerOwnerOperator))
		   .parameters.Append .CreateParameter("@typeConstructionCommercial", adBoolean, adParamInput, 1, isOn(typeConstructionCommercial))
		   .parameters.Append .CreateParameter("@typeConstructionIndustrial", adBoolean, adParamInput, 1, isOn(typeConstructionIndustrial))
		   .parameters.Append .CreateParameter("@typeConstructionMunicipal", adBoolean, adParamInput, 1, isOn(typeConstructionMunicipal))
		   .parameters.Append .CreateParameter("@typeConstructionDOT", adBoolean, adParamInput, 1, isOn(typeConstructionDOT))
		   .parameters.Append .CreateParameter("@typeConstructionUtility", adBoolean, adParamInput, 1, isOn(typeConstructionUtility))
		   .parameters.Append .CreateParameter("@typeConstructionResidential", adBoolean, adParamInput, 1, isOn(typeConstructionResidential))		   
		   .parameters.Append .CreateParameter("@primaryPermitteeSubdivision", adBoolean, adParamInput, 1, isOn(primaryPermitteeSubdivision))
		   .parameters.Append .CreateParameter("@individualLot", adBoolean, adParamInput, 1, isOn(individualLot))
		   .parameters.Append .CreateParameter("@individualLotWithinSWDA", adBoolean, adParamInput, 1, isOn(individualLotWithinSWDA))		   
		   .parameters.Append .CreateParameter("@IRWName", adVarChar, adParamInput, 100, IRWName)
		   .parameters.Append .CreateParameter("@MSSSOwnerOperator", adVarChar, adParamInput, 100, MSSSOwnerOperator)
		   .parameters.Append .CreateParameter("@RWName", adVarChar, adParamInput, 100, RWName)		   
		   .parameters.Append .CreateParameter("@certify1", adVarChar, adParamInput, 50, certify1)
		   .parameters.Append .CreateParameter("@certify2", adVarChar, adParamInput, 50, certify2)
		   .parameters.Append .CreateParameter("@ownerPrintedName", adVarChar, adParamInput, 100, ownerPrintedName)
		   .parameters.Append .CreateParameter("@ownerTitle", adVarChar, adParamInput, 100, ownerTitle)
		   .parameters.Append .CreateParameter("@ownerSignDate", adDBTimeStamp, adParamInput, 8, ownerSignDate)
		   .parameters.Append .CreateParameter("@operatorPrintedName", adVarChar, adParamInput, 100, operatorPrintedName)
		   .parameters.Append .CreateParameter("@operatorTitle", adVarChar, adParamInput, 100, operatorTitle)
		   .parameters.Append .CreateParameter("@operatorSignDate", adDBTimeStamp, adParamInput, 8, operatorSignDate)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		Set rs = nothing
		
		'***********create the pdf***********************************
		select case coverageDesired
			case 1
				x = 73
				y = 611
			case 2
				x = 223
				y = 611
			case 3
				x = 397
				y = 611
			
		end select
		
		GPSDegree1= request("degree1")
		GPSMinute1= request("minute1")
		GPSSecond1= request("second1")
		GPSLat =  request("latitude")
		GPSDegree2= request("degree2")
		GPSMinute2= request("minute2")
		GPSSecond2= request("second2")
		GPSLon =  request("longitude")
		'�
		GPSLocation = GPSDegree1 & "&deg; " & GPSMinute1 & "' " & GPSSecond1 & "&quot; " & GPSLat & " / " & GPSDegree2 & "&deg; " & GPSMinute2 & "' " & GPSSecond2 & "&quot; " & GPSLon

		'create the PDF and store it in the downloads folder
		'page 1	
		arrX = Array(500,x,171,246,243,100,377,170,437,153,121,322,437,523,166,480,124,322,437,523,217,325,433,157,443,209,443,121,282,363,492,252,72,289,181,289,361,433,505,181,289,289,289,252,363,252) 'left to right
		arrY = Array(775,y,563,543,524,505,505,487,487,470,450,450,450,450,430,430,410,410,410,410,388,388,388,367,367,329,329,310,310,310,310,293,251,251,231,231,231,231,231,211,211,191,170,131,112,93) 'up and down
		arrText = Array(primaryFormID,"x",siteProjectName,GPSLocation,projectAddress,projectCity,projectCounty,subdivision,lotNumber,ownerName,ownerAddress, _
			ownerCity,ownerState,ownerZip,operatorName,operatorPhone,operatorAddress,operatorCity,operatorState,operatorZip,setx(typePrimary),setx(typeSecondary), _
			setx(typeTertiary),facilityContact,facilityContactPhone,primaryPermitteeName,primaryPermitteePhone,primaryPermitteeAddress,primaryPermitteeCity, _
			primaryPermitteeState,primaryPermitteeZip,secondaryPermittees,setx(constructionActivityCompleted),setx(noLongerOwnerOperator),setx(typeConstructionCommercial), _
			setx(typeConstructionIndustrial),setx(typeConstructionMunicipal),setx(typeConstructionDOT),setx(typeConstructionUtility),setx(typeConstructionResidential), _
			setx(primaryPermitteeSubdivision),setx(individualLot),setx(individualLotWithinSWDA),IRWName,MSSSOwnerOperator,RWName)
		
		'page2	
		arrX2 = Array(72,72,187,425,427,200,425,427) 'left to right
		arrY2 = Array(680,432,295,295,270,232,232,207) 'up and down
		arrText2 = Array(certify1,certify2,ownerPrintedName,ownerTitle,ownerSignDate,operatorPrintedName,operatorTitle,operatorSignDate)
		
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
	
		' Open blank PDF form from file
		Set Doc = Pdf.OpenDocument( Server.MapPath("pdfTemplates/noticeOfTermination.pdf") )
	
		' Create default font
		Set Font = Doc.Fonts("Helvetica")
	
		' Obtain the only page's canvas
		Set Canvas = Doc.Pages(1).Canvas
		Set Canvas2 = Doc.Pages(2).Canvas
	
		' Create empty param object
		Set Param = Pdf.CreateParam	
		Set Param2 = Pdf.CreateParam	
	
		' Go over all items in arrays
		For j = 0 to UBound(arrX)
	
			Param("x") = arrX(j)
			Param("y") = arrY(j) - 263 * i
			Param("color") = "black"
			Param("html") = true
			' Draw text on canvas
			Page.ResetCoordinates
			Canvas.DrawText arrText(j), Param, Font
	
		Next ' j
		
		j = 0
		For j = 0 to UBound(arrX2)
			Param2("x") = arrX2(j)
			Param2("y") = arrY2(j) - 263 * i
			Param2("color") = "black"
			Param("html") = true
			' Draw text on canvas
			Page.ResetCoordinates
			Canvas2.DrawText arrText2(j), Param2, Font
		Next
		
		if primaryFormID = 0 then
			docID = secondaryFormID
		else
			docID = primaryFormID & "_" & secondaryFormID
		end if
		
		
		' Save document, the Save method returns generated file name
		Filename = Doc.Save( Server.MapPath("downloads/NOI/noticeOfTermination_" & docID & ".pdf"), True )
		
		'***********end editing the pdf***********************************

		
		response.Redirect "secondaryList.asp?primaryID=" & primaryFormID & "&projectID=" & projectID
		
	case "editNOTSec"
		
		projectID = request("projectID")
		notFormID = request("notFormID") 
		secondaryFormID = request("secondaryFormID")
		primaryFormID = request("primaryID")
		coverageDesired= request("coverageDesired")
		siteProjectName= request("siteProjectName")
		GPSDegree1= request("degree1")
		GPSMinute1= request("minute1")
		GPSSecond1= request("second1")
		GPSLat =  request("latitude")
		GPSDegree2= request("degree2")
		GPSMinute2= request("minute2")
		GPSSecond2= request("second2")
		GPSLon =  request("longitude")
		projectAddress= request("projectAddress")
		projectCity= request("projectCity")
		projectCounty= request("projectCounty")
		subdivision= request("subdivision")
		lotNumber= request("lotNumber")
		ownerName= request("ownerName")
		ownerAddress= request("ownerAddress")
		ownerCity= request("ownerCity")
		ownerState= request("ownerState")
		ownerZip= request("ownerZip")
		operatorName= request("operatorName")
		operatorPhone= request("operatorPhone")
		operatorAddress= request("operatorAddress")
		operatorCity= request("operatorCity")
		operatorState= request("operatorState")
		operatorZip= request("operatorZip")
		typePrimary= request("typePrimary")
		typeSecondary= request("typeSecondary")
		typeTertiary= request("typeTertiary")		
		facilityContact= request("facilityContact")
		facilityContactPhone= request("facilityContactPhone")
		primaryPermitteeName= request("primaryPermitteeName")
		primaryPermitteePhone= request("primaryPermitteePhone")
		primaryPermitteeAddress= request("primaryPermitteeAddress")
		primaryPermitteeCity= request("primaryPermitteeCity")
		primaryPermitteeState= request("primaryPermitteeState")
		primaryPermitteeZip= request("primaryPermitteeZip")
		secondaryPermittees= request("secondaryPermittees")		
		constructionActivityCompleted= request("constructionActivityCompleted")
		noLongerOwnerOperator= request("noLongerOwnerOperator")		
		typeConstructionCommercial= request("typeConstructionCommercial")
		typeConstructionIndustrial= request("typeConstructionIndustrial")
		typeConstructionMunicipal= request("typeConstructionMunicipal")
		typeConstructionDOT= request("typeConstructionDOT")
		typeConstructionUtility= request("typeConstructionUtility")
		typeConstructionResidential= request("typeConstructionResidential")		
		primaryPermitteeSubdivision= request("primaryPermitteeSubdivision")
		individualLot= request("individualLot")
		individualLotWithinSWDA= request("individualLotWithinSWDA")		
		IRWName= request("IRWName")
		MSSSOwnerOperator= request("MSSSOwnerOperator")
		RWName= request("RWName")
		certify1= request("certify1")
		certify2= request("certify2")
		ownerPrintedName= request("ownerPrintedName")
		ownerTitle= request("ownerTitle")
		ownerSignDate= request("ownerSignDate")
		operatorPrintedName= request("operatorPrintedName")
		operatorTitle= request("operatorTitle")
		operatorSignDate= request("operatorSignDate")
	
		'Open connection and insert into the database
		On Error Resume Next
		
			Set DataConn = Server.CreateObject("ADODB.Connection") 
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
			
			DataConn.Open Session("Connection"), Session("UserID")
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If					
		
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spEditNOT"
		   .parameters.Append .CreateParameter("@notFormID", adInteger, adParamInput, 8, notFormID)
		   .parameters.Append .CreateParameter("@coverageDesired", adInteger, adParamInput, 8, coverageDesired)
		   .parameters.Append .CreateParameter("@siteProjectName", adVarChar, adParamInput, 200, siteProjectName)
		   .parameters.Append .CreateParameter("@GPSDegree1", adVarChar, adParamInput, 50, GPSDegree1)
		   .parameters.Append .CreateParameter("@GPSMinute1", adVarChar, adParamInput, 50, GPSMinute1)
		   .parameters.Append .CreateParameter("@GPSSecond1", adVarChar, adParamInput, 50, GPSSecond1)
		   .parameters.Append .CreateParameter("@GPSLat", adVarChar, adParamInput, 50, GPSLat)
		   .parameters.Append .CreateParameter("@GPSDegree2", adVarChar, adParamInput, 50, GPSDegree2)
		   .parameters.Append .CreateParameter("@GPSMinute2", adVarChar, adParamInput, 50, GPSMinute2)
		   .parameters.Append .CreateParameter("@GPSSecond2", adVarChar, adParamInput, 50, GPSSecond2)
		   .parameters.Append .CreateParameter("@GPSLon", adVarChar, adParamInput, 50, GPSLon)
		   .parameters.Append .CreateParameter("@projectAddress", adVarChar, adParamInput, 100, projectAddress)
		   .parameters.Append .CreateParameter("@projectCity", adVarChar, adParamInput, 100, projectCity)
		   .parameters.Append .CreateParameter("@projectCounty", adVarChar, adParamInput, 100, projectCounty)
		   .parameters.Append .CreateParameter("@subdivision", adVarChar, adParamInput, 150, subdivision)
		   .parameters.Append .CreateParameter("@lotNumber", adVarChar, adParamInput, 150, lotNumber)
		   .parameters.Append .CreateParameter("@ownerName", adVarChar, adParamInput, 100, ownerName)		   
		   .parameters.Append .CreateParameter("@ownerAddress", adVarChar, adParamInput, 100, ownerAddress)
		   .parameters.Append .CreateParameter("@ownerCity", adVarChar, adParamInput, 100, ownerCity)
		   .parameters.Append .CreateParameter("@ownerState", adVarChar, adParamInput, 50, ownerState)
		   .parameters.Append .CreateParameter("@ownerZip", adVarChar, adParamInput, 50, ownerZip)
		   .parameters.Append .CreateParameter("@operatorName", adVarChar, adParamInput, 100, operatorName)
		   .parameters.Append .CreateParameter("@operatorPhone", adVarChar, adParamInput, 50, operatorPhone)
		   .parameters.Append .CreateParameter("@operatorAddress", adVarChar, adParamInput, 100, operatorAddress)
		   .parameters.Append .CreateParameter("@operatorCity", adVarChar, adParamInput, 100, operatorCity)
		   .parameters.Append .CreateParameter("@operatorState", adVarChar, adParamInput, 50, operatorState)
		   .parameters.Append .CreateParameter("@operatorZip", adVarChar, adParamInput, 50, operatorZip)
		   .parameters.Append .CreateParameter("@typePrimary", adBoolean, adParamInput, 1, isOn(typePrimary))
		   .parameters.Append .CreateParameter("@typeSecondary", adBoolean, adParamInput, 1, isOn(typeSecondary))
		   .parameters.Append .CreateParameter("@typeTertiary", adBoolean, adParamInput, 1, isOn(typeTertiary))		   
		   .parameters.Append .CreateParameter("@facilityContact", adVarChar, adParamInput, 100, facilityContact)
		   .parameters.Append .CreateParameter("@facilityContactPhone", adVarChar, adParamInput, 50, facilityContactPhone)		   
		   .parameters.Append .CreateParameter("@primaryPermitteeName", adVarChar, adParamInput, 100, primaryPermitteeName)
		   .parameters.Append .CreateParameter("@primaryPermitteePhone", adVarChar, adParamInput, 50, primaryPermitteePhone)
		   .parameters.Append .CreateParameter("@primaryPermitteeAddress", adVarChar, adParamInput, 100, primaryPermitteeAddress)
		   .parameters.Append .CreateParameter("@primaryPermitteeCity", adVarChar, adParamInput, 100, primaryPermitteeCity)
		   .parameters.Append .CreateParameter("@primaryPermitteeState", adVarChar, adParamInput, 50, primaryPermitteeState)
		   .parameters.Append .CreateParameter("@primaryPermitteeZip", adVarChar, adParamInput, 50, primaryPermitteeZip)
		   .parameters.Append .CreateParameter("@secondaryPermittees", adVarChar, adParamInput, 50, secondaryPermittees)	   
		   .parameters.Append .CreateParameter("@constructionActivityCompleted", adBoolean, adParamInput, 1, isOn(constructionActivityCompleted))
		   .parameters.Append .CreateParameter("@noLongerOwnerOperator", adBoolean, adParamInput, 1, isOn(noLongerOwnerOperator))
		   .parameters.Append .CreateParameter("@typeConstructionCommercial", adBoolean, adParamInput, 1, isOn(typeConstructionCommercial))
		   .parameters.Append .CreateParameter("@typeConstructionIndustrial", adBoolean, adParamInput, 1, isOn(typeConstructionIndustrial))
		   .parameters.Append .CreateParameter("@typeConstructionMunicipal", adBoolean, adParamInput, 1, isOn(typeConstructionMunicipal))
		   .parameters.Append .CreateParameter("@typeConstructionDOT", adBoolean, adParamInput, 1, isOn(typeConstructionDOT))
		   .parameters.Append .CreateParameter("@typeConstructionUtility", adBoolean, adParamInput, 1, isOn(typeConstructionUtility))
		   .parameters.Append .CreateParameter("@typeConstructionResidential", adBoolean, adParamInput, 1, isOn(typeConstructionResidential))		   
		   .parameters.Append .CreateParameter("@primaryPermitteeSubdivision", adBoolean, adParamInput, 1, isOn(primaryPermitteeSubdivision))
		   .parameters.Append .CreateParameter("@individualLot", adBoolean, adParamInput, 1, isOn(individualLot))
		   .parameters.Append .CreateParameter("@individualLotWithinSWDA", adBoolean, adParamInput, 1, isOn(individualLotWithinSWDA))		   
		   .parameters.Append .CreateParameter("@IRWName", adVarChar, adParamInput, 100, IRWName)
		   .parameters.Append .CreateParameter("@MSSSOwnerOperator", adVarChar, adParamInput, 100, MSSSOwnerOperator)
		   .parameters.Append .CreateParameter("@RWName", adVarChar, adParamInput, 100, RWName)		   
		   .parameters.Append .CreateParameter("@certify1", adVarChar, adParamInput, 50, certify1)
		   .parameters.Append .CreateParameter("@certify2", adVarChar, adParamInput, 50, certify2)
		   .parameters.Append .CreateParameter("@ownerPrintedName", adVarChar, adParamInput, 100, ownerPrintedName)
		   .parameters.Append .CreateParameter("@ownerTitle", adVarChar, adParamInput, 100, ownerTitle)
		   .parameters.Append .CreateParameter("@ownerSignDate", adDBTimeStamp, adParamInput, 8, ownerSignDate)
		   .parameters.Append .CreateParameter("@operatorPrintedName", adVarChar, adParamInput, 100, operatorPrintedName)
		   .parameters.Append .CreateParameter("@operatorTitle", adVarChar, adParamInput, 100, operatorTitle)
		   .parameters.Append .CreateParameter("@operatorSignDate", adDBTimeStamp, adParamInput, 8, operatorSignDate)
		   .CommandType = adCmdStoredProc
		End With
		
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
				
		Set rs = oCmd.Execute
		If Err Then
		%>
			<!--#include file="includes/FatalError.inc"-->
		<%
		End If
		
		Set oCmd = nothing
		rs.close
		Set rs = nothing
		
		'***********create the pdf***********************************
		select case coverageDesired
			case 1
				x = 73
				y = 611
			case 2
				x = 223
				y = 611
			case 3
				x = 397
				y = 611
			
		end select
		
		GPSDegree1= request("degree1")
		GPSMinute1= request("minute1")
		GPSSecond1= request("second1")
		GPSLat =  request("latitude")
		GPSDegree2= request("degree2")
		GPSMinute2= request("minute2")
		GPSSecond2= request("second2")
		GPSLon =  request("longitude")
		'�
		GPSLocation = GPSDegree1 & "&deg; " & GPSMinute1 & "' " & GPSSecond1 & "&quot; " & GPSLat & " / " & GPSDegree2 & "&deg; " & GPSMinute2 & "' " & GPSSecond2 & "&quot; " & GPSLon

		'create the PDF and store it in the downloads folder
		'page 1	
		arrX = Array(500,x,171,246,243,100,377,170,437,153,121,322,437,523,166,480,124,322,437,523,217,325,433,157,443,209,443,121,282,363,492,252,72,289,181,289,361,433,505,181,289,289,289,252,363,252) 'left to right
		arrY = Array(775,y,563,543,524,505,505,487,487,470,450,450,450,450,430,430,410,410,410,410,388,388,388,367,367,329,329,310,310,310,310,293,251,251,231,231,231,231,231,211,211,191,170,131,112,93) 'up and down
		arrText = Array(primaryFormID,"x",siteProjectName,GPSLocation,projectAddress,projectCity,projectCounty,subdivision,lotNumber,ownerName,ownerAddress, _
			ownerCity,ownerState,ownerZip,operatorName,operatorPhone,operatorAddress,operatorCity,operatorState,operatorZip,setx(typePrimary),setx(typeSecondary), _
			setx(typeTertiary),facilityContact,facilityContactPhone,primaryPermitteeName,primaryPermitteePhone,primaryPermitteeAddress,primaryPermitteeCity, _
			primaryPermitteeState,primaryPermitteeZip,secondaryPermittees,setx(constructionActivityCompleted),setx(noLongerOwnerOperator),setx(typeConstructionCommercial), _
			setx(typeConstructionIndustrial),setx(typeConstructionMunicipal),setx(typeConstructionDOT),setx(typeConstructionUtility),setx(typeConstructionResidential), _
			setx(primaryPermitteeSubdivision),setx(individualLot),setx(individualLotWithinSWDA),IRWName,MSSSOwnerOperator,RWName)
		
		'page2	
		arrX2 = Array(72,72,187,425,427,200,425,427) 'left to right
		arrY2 = Array(680,432,295,295,270,232,232,207) 'up and down
		arrText2 = Array(certify1,certify2,ownerPrintedName,ownerTitle,ownerSignDate,operatorPrintedName,operatorTitle,operatorSignDate)
		
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
	
		' Open blank PDF form from file
		Set Doc = Pdf.OpenDocument( Server.MapPath("pdfTemplates/noticeOfTermination.pdf") )
	
		' Create default font
		Set Font = Doc.Fonts("Helvetica")
	
		' Obtain the only page's canvas
		Set Canvas = Doc.Pages(1).Canvas
		Set Canvas2 = Doc.Pages(2).Canvas
	
		' Create empty param object
		Set Param = Pdf.CreateParam	
		Set Param2 = Pdf.CreateParam	
	
		' Go over all items in arrays
		For j = 0 to UBound(arrX)
	
			Param("x") = arrX(j)
			Param("y") = arrY(j) - 263 * i
			Param("color") = "black"
			Param("html") = true
			' Draw text on canvas
			Page.ResetCoordinates
			Canvas.DrawText arrText(j), Param, Font
	
		Next ' j
		
		j = 0
		For j = 0 to UBound(arrX2)
			Param2("x") = arrX2(j)
			Param2("y") = arrY2(j) - 263 * i
			Param2("color") = "black"
			Param("html") = true
			' Draw text on canvas
			Page.ResetCoordinates
			Canvas2.DrawText arrText2(j), Param2, Font
		Next
		
		if primaryFormID = 0 then
			docID = secondaryFormID
		else
			docID = primaryFormID & "_" & secondaryFormID
		end if
		
		
		' Save document, the Save method returns generated file name
		Filename = Doc.Save( Server.MapPath("downloads/NOI/noticeOfTermination_" & docID & ".pdf"), True )
		
		'***********end editing the pdf***********************************

		
		response.Redirect "secondaryList.asp?primaryID=" & primaryFormID & "&projectID=" & projectID

		
	Case else%>
		Your request could not be processed at this time.		  
	
<%end select%>