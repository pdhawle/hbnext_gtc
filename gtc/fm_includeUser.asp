<%
Sub EditDb
On error resume next
  If fs.FileExists(server.mappath(dbfile)) Then
    Response.Redirect dbfile & "?db=" & sFile
  Else
	  Response.Write "No database found" &vbCrLf
	End If
End Sub

Sub CreateNewFolder
On error resume next
%>

<b>Create Folder</b><br><br>
<!--Current folder: <%'=spath%>-->
  <form method="POST" action="<%=scriptname%>?action=newfolder&formType=editUser&path=<%=sPath%>&userID=<%=iUserID%>&sec=docs">
        
  <input name="folder" type="text" value="folder name" size="30">

        <input type="submit" value="Create new folder" name="submit" class="formButtonSmall"> 
</form>
<br>
<%

End Sub

Sub EditFile
On error resume next
  Session("lastpage") = Request.ServerVariables("HTTP_REFERER")
  Set ReadStream = fs.OpenTextFile(server.mappath(sFile))
  filename=request.querystring("file")
  response.write ("<br>")  
  Response.write "currently editing: "
  response.write "<b>"&filename&"</b>"
  response.write "<br>"
  
  Response.Write "<form method=""POST"" action=""" & scriptname & "?action=savefile&path=" & sPath & "&file=" & Request.Querystring("File") & "&overwrite=yes&userID=" & iUserID & ">" &vbCrLf
  Response.Write "<p><textarea rows=""20"" cols=""95"" name=""filestuff"">" & Server.HTMLEncode(ReadStream.ReadAll) & "</textarea></p>" &vbCrLf
response.write "Save file as: <input name=NewFileName type=text size=30>"
  Response.Write "<p><input type=""submit"" value=""Save changes"" name=""submit"" class=formButtonSmall>&nbsp;<input type=""reset"" value=""restore original"" name=""reset""></p>" &vbCrLf
  Response.Write "</form>" &vbCrLf 

  
End Sub

Sub UploadFiles
On error resume next

'for the progress bar
Set UploadProgress = Server.CreateObject("Persits.UploadProgress")
PID = "PID=" & UploadProgress.CreateProgressID()
barref = "framebar.asp?to=10&" & PID

%>
<SCRIPT LANGUAGE="JavaScript">
function ShowProgress()
{
  strAppVersion = navigator.appVersion;
  if (document.MyForm.FILE1.value != "" || document.MyForm.FILE2.value != "" || document.MyForm.FILE3.value != "" || document.MyForm.FILE4.value != "" || document.MyForm.FILE5.value != "" || document.MyForm.FILE6.value != "" || document.MyForm.FILE7.value != "" || document.MyForm.FILE8.value != "" || document.MyForm.FILE9.value != "" || document.MyForm.FILE10.value != "")
  {
    if (strAppVersion.indexOf('MSIE') != -1 && strAppVersion.substr(strAppVersion.indexOf('MSIE')+5,1) > 4)
    {
      winstyle = "dialogWidth=385px; dialogHeight:140px; center:yes";
      window.showModelessDialog('<% = barref %>&b=IE',null,winstyle);
    }
    else
    {
      window.open('<% = barref %>&b=NN','','width=370,height=115', true);
    }
  }
  return true;
}
</SCRIPT> 
<b>Upload file(s)</b><br />
Note: File size limit is 2GB<br /><br />
<!--<br>Current folder:<b> <%'=spath%></b><br>-->
      <form name="MyForm" method="POST" action="uploadUser.asp?userID=<%=iUserID%>&<%=PID%>&sec=docs" enctype="multipart/form-data" OnSubmit="return ShowProgress();">
        <input type="FILE" name="FILE1" size="30" class="formButtonSmall"><br>
		<input type="FILE" name="FILE2" size="30" class="formButtonSmall"><br>
		<input type="FILE" name="FILE3" size="30" class="formButtonSmall"><br>
		<input type="FILE" name="FILE4" size="30" class="formButtonSmall"><br>
		<input type="FILE" name="FILE5" size="30" class="formButtonSmall"><br>
		<input type="FILE" name="FILE6" size="30" class="formButtonSmall"><br>
		<input type="FILE" name="FILE7" size="30" class="formButtonSmall"><br>
		<input type="FILE" name="FILE8" size="30" class="formButtonSmall"><br>
		<input type="FILE" name="FILE9" size="30" class="formButtonSmall"><br>
		<input type="FILE" name="FILE10" size="30" class="formButtonSmall"><br>
			<br>
        <input type="SUBMIT" value="Upload file" name="SUBMIT" class="formButtonSmall">
      </form>
<%
End Sub



Sub CreateFile
On error resume next
  response.write "<b>Create new file</b><br>"
  Session("lastpage") = Request.ServerVariables("HTTP_REFERER") 
  Response.Write "<form method=POST action=" & scriptname & "?action=savefile&formType=editUser&path=" & sPath & "&userID=" & iUserID & "&sec=docs>" &vbCrLf
  Response.Write "<p><input type=""text"" name=""file""></input><font face=""Tahoma""> Give name including extensions.</p>" &vbCrLf
  Response.Write "<p>Contents of file<br><textarea rows=""15"" cols=""60"" name=""newfilestuff""></textarea></p>" &vbCrLf
  Response.Write "<p><input type=""submit"" value=""create file"" name=""submit"" class=formButtonSmall></p>" &vbCrLf
  Response.Write "</form></font>" &vbCrLf
End Sub

Sub SaveFile
On error resume next
  '' the save as a new file routine
  if request.form("NewFileName")<>"" then
  NewFileName=request.form("NewFileName")
  spath=request("path")
    if spath="/" then slashvalue="" else slashvalue="/" end if
  filestuff=request.form("filestuff")
  NewPathFileName= spath&slashvalue&newfilename''creating the right path and filename
    Set fso = CreateObject("Scripting.FileSystemObject")
	Set textStreamObject = fso.CreateTextFile(server.mappath(NewPathFileName),true,false)
	textStreamObject.write filestuff
	Response.Redirect("" & Session("lastpage") & "")
  else
  '' done saving as routine

  If Request.Querystring("overwrite") = "yes" Then
    set fs=Server.CreateObject("Scripting.FileSystemObject")
	Set WriteFile = fs.CreateTextFile(server.mappath(sFile), true)
	WriteFile.Write Request.Form("filestuff")''filestuff''Session("filestuff")
    WriteFile.Close
	Response.Redirect("" & Session("lastpage") & "")
	
  Else
	Session("lastpage") = Request.ServerVariables("HTTP_Referer")
    If fs.FileExists(server.mappath(sFile)) Then
	  'sFile = Replace(sFile, "%20", " ")
      Session("sFile") = sFile
	  spath=request.querystring("path")
      Session("newfilestuff") = Request.Form("newfilestuff")
      Response.Write "<br><p>Filename already exists:  <b>" & sFile & "</b> </p>"
      Response.Write "<UL>"
      Response.Write "<LI><a href=""" & scriptname & "?action=savenewfile&formType=editUser&overwrite=yes&userID=" & iUserID & "&sec=docs>Replace existing file?</a>"
      ' We don't want to lose the information that the typed in the previous form if they decide NOT to overwrite the existing file,
	  ' so we provide a javascript link back that works exactly the same as the browser's back button.
			Response.Write "<LI><a href=""javascript:history.back()"">Back to the previous page</a></LI>"
      Response.Write "</UL>"
    Else 
      Set WriteFile = fs.CreateTextFile(server.mappath(sFile), false)
      WriteFile.Write Request.Form("newfilestuff")
      WriteFile.Close
      Response.Redirect("form.asp?formType=editUser&action=viewfolder&path=" & spath & "&userID=" & iUserID & "&sec=docs")  
    End If
  End If
  end if
End Sub

Sub CreateFolder
On error resume next
 
 Session("lastpage") = request.querystring("path")
 If fs.FolderExists(server.mappath(sFolder)) Then 
   response.write "A folder with the name <b>" & sFolder & "</b> already exists<br>"  
 Else
   fs.CreateFolder(server.mappath(sFolder))
   ''Response.Redirect("" & Session("lastpage") & "")
response.redirect("form.asp?formType=editUser&userID=" & iUserID & "&action=viewfolder&path="&session("lastpage")&"&sec=docs")


 End If
End Sub

Sub DeleteFile
On error resume next
	response.write"<br>"
  If Request.Querystring("commit") <> "yes" Then
    Session("lastpage") = Request.ServerVariables("HTTP_REFERER")
	sFile = Replace(sFile, "%20", " ")	
    Session("sFile") = sFile
    Response.Write "<p>You are about to delete:  " & sFile & ". " 
    If sFileType = "jpg" OR sFileType = "gif" Then
      Response.Write "<p><img src=""http://" & Request.ServerVariables("HTTP_HOST") & sfile & """></p>"
    End If
    Response.Write "<b>This cannot be undone!</b></p>"
    Response.Write "<UL>"
	'response.Write scriptname & "?action=deletefile&path=" & sPath & "&file=" & sFile & "&commit=yes&userID=" & userID
	'response.Write userID
	sFile = Replace(sFile, " ", "%20")
	'response.Write sFile
    Response.Write "<LI><a href=" & scriptname & "?action=deletefile&formType=editUser&commit=yes&userID=" & iUserID & "&path=" & sPath & "&file=" & sFile & "&sec=docs>Continue?</a></LI>"
    Response.Write "<LI><a href=" & Session("lastpage") & ">Stop</a></LI>"
    Response.Write "</UL>"
  Else
  	'response.Write Session("sFile") & "<br>"
	'response.redirect("docManagerCust.asp?projectID=" & request("") & "&action=viewfolder&path="&session("lastpage")&"")
    fs.DeleteFile(server.mappath(Session("sFile")))
    Response.Redirect("" & Session("lastpage") & "")
  End If
End Sub

Sub DeleteFolder
On error resume next
	response.write"<br>"
  If Request.Querystring("commit") <> "yes" Then 
    Session("lastpage") = Request.ServerVariables("HTTP_REFERER")
    Session("sFolder") = sFolder
    Response.Write "<p>You are about to delete: " & sFolder & ". " 
    Response.Write "<b>This cannot be undone!</b></p>"
    Response.Write "<UL>"
    Response.Write "<LI><a href=" & scriptname & "?action=deletefolder&formType=editUser&path=" & sPath & "&folder=" & sFolder &  "&commit=yes&userID=" & iUserID & "&sec=docs>Continue?</a></LI>"
    Response.Write "<LI><a href=" & Session("lastpage") & ">Stop</a></LI>"
    Response.Write "</UL>"
  Else
    Response.Write sPath & "<br>"
    Response.Write sFile & "<br>"
    fs.DeleteFolder(server.mappath(Session("sFolder")))
    Response.Redirect("" & Session("lastpage") & "")
  End If
End Sub

Sub RenameFolder

On error resume next
	response.write"<br>"
	Response.write "<b>Rename folder</b><br>"
  If Request.querystring("commit") <> "yes" Then 
    Session("lastpage") = Request.ServerVariables("HTTP_REFERER")
    Response.Write "<p>You are about to rename the folder named: <b>" & request.querystring("folder") & "</b>" 
%>
<form name="form1" method="post" action="form.asp?formType=editUser&action=RenameFolder&path=<%=spath%>&folder=<%=request.querystring("folder")%>&commit=yes&userID=<%=iUserID%>&sec=docs">
  <input name="NewFolderName" type="text" size="30" value="<%=request.querystring("folder")%>">
  <input type="submit" name="Submit" value="Rename Folder"  class="formButtonSmall">
  <input type="hidden"name="folder" value="<%=request.querystring("folder")%>">
</form>
<%
  Else
	NewFolderName=request.form("NewFolderName")
	sFolder=request.form("folder")
	if spath="/" then slashvalue="" else slashvalue="/" end if
    Set fso = CreateObject("Scripting.FileSystemObject")
	Set folderObject = fso.GetFolder(Server.MapPath(spath&slashvalue&sFolder))
	FolderObject.Name=NewFolderName
	 Set folderObject = Nothing
	 Set fso = Nothing
	  Response.Redirect("" & Session("lastpage") & "") 
   End If
  
End Sub

Sub RenameFile
On error resume next
  response.write"<br>"
  Response.write "<b>Rename file</b><br>"
  If Request("commit") <> "yes" Then 
    Session("lastpage") = Request.ServerVariables("HTTP_REFERER")
    Response.Write "<p>You are about to rename the file named: <b>" & request.querystring("file") & "</b>" 
%>
<form name="form1" method="post" action="form.asp?formType=editUser&action=RenameFile&path=<%=spath%>&folder=<%=request.querystring("folder")%>&commit=yes&userID=<%=iUserID%>&sec=docs">
  <input name="NewFileName" type="text" size="30" value="<%=request.querystring("file")%>">
  <input type="submit" name="Submit" value="Rename file" class="formButtonSmall">
  <input type="hidden" name="filename" value="<%=request.querystring("file") %>">
</form>
<p>
  <%
  Else
  	NewFileName=request.form("NewFileName")
	Sfile=request.form("filename")
	if spath="/" then slashvalue="" else slashvalue="/"
    Set fso = CreateObject("Scripting.FileSystemObject")
	Set FileObject = fso.GetFile(Server.MapPath(spath&slashvalue&sfile))
	FileObject.Name = NewFileName
	Set FilObject = Nothing
	Set fso = Nothing
    Response.Redirect("" & Session("lastpage") & "")  
  End If
  
End Sub

Sub FileTypeUnsupported
On error resume next
  Session("lastpage") = Request.ServerVariables("HTTP_REFERER")
  filename=request.querystring("file")
  response.write ("<br>")  
  Response.write "Currently showing: "
  response.write "<b>"&filename&"</b>"
  response.write "<br>"
  If sFileType = "jpg" OR sFileType = "gif" OR sFileType = "GIF" OR sFileType = "JPG" Then
    Response.Write "<p><img src=""http://" & Request.ServerVariables("HTTP_HOST") & sfile & """></p>"
  else
    Response.Write "<br>The selected file was not recognized.<br><br>"
  End If
  Response.Write "<a href=" & Session("lastpage") & ">back</a>"
End Sub

Sub Size(itemsize)
  Response.Write "<td class=""" & bgcolor & """ align=""center"" valign=""bottom"">" &vbCrLf
  Select case Len(itemsize)
  Case "1", "2", "3" 
    Response.Write itemsize & " bytes"
  Case "4", "5", "6"
    Response.Write Round(itemsize/1000) & " Kb"
  Case "7", "8", "9"
    Response.Write Round(itemsize/1000000) & " Mb"
  End Select
  Response.Write "</td>" &vbCrLf
End Sub

Sub ShowList
 	' Start building the table to display the information we retrieve about the files and folders in the current directory.  
  Response.Write "<table cellpadding=""0"" cellspacing=""0"" border=""0"" bordercolor=""#cccccc"" width=""100%"">" &vbCrLf
  %>
   <tr bgcolor="#666666">
    <td><span class="searchText">&nbsp;Name</span></td>
    <td align="center"><span class="searchText">Type</span></td>
    <td align="center"><span class="searchText">Size</span></td>
    <td align="center"><span class="searchText">Date accessed</span></td>
    <td align="center"><span class="searchText">Action</span></td>
  </tr>
  <tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>
  <%
  'if the folder does not exist, create it dynamically based on the projectID
  'added by RR********************************************************************************************************************************************************************************************
  If not fs.FolderExists(server.mappath(sPath)) Then 
  'response.Write server.mappath(sPath)
  	fs.CreateFolder(server.mappath(sPath))
  end if
  'added by RR********************************************************************************************************************************************************************************************
    ' Use the GetFolder method of the filesystemobject to get the contents of the directory specified in sPath  
  Set fileobject = fs.GetFolder(server.mappath(sPath))
	' Use the SubFolders property to get the folders contained in the directory specified in sPath
  Set foldercollection = fileobject.SubFolders 
    ' Start the code to alternate line colors - just to make the display a little less visually confusing.
  lineid=0
  bgcolor = ""
    bgcolor_off = ""
    bgcolor_on = "rowColor"

  ' Loop through the folders contained in the foldercollection and display their information on the page
  For Each folder in foldercollection 
    ' Apply our alternating line coloring
    If lineid = 0 Then
      bgcolor = bgcolor_off
      lineid = 1
    Else
      bgcolor = bgcolor_on
      lineid = 0
    End if		

		Response.Write "<tr class=""" & bgcolor & """><font face=""verdana"" size=""1"">" &vbCrLf
		If Right(sPath,1)="/" Then
	    Response.Write "<td class=""" & bgcolor & """ align=""left"" valign=""bottom""><img src=images/folder.gif > <a class=fileLink href=""" & scriptname & "?action=viewfolder&sec=docs&formType=editUser&path=" & sPath & folder.name & "&userID=" & iUserID & """>" & folder.name & "</a></td>" & vbCrLf

		Else
		  Response.Write "<td class=""" & bgcolor & """ align=""left"" valign=""bottom""><img src=images/folder.gif > <a class=fileLink href=""" & scriptname & "?action=viewfolder&sec=docs&formType=editUser&path=" & sPath &"/" &folder.name & "&userID=" & iUserID & """>" & folder.name & "</a></td>" & vbCrLf

		End If  

	  Response.Write "<td class=""" & bgcolor & """ align=""center"" valign=""bottom"">folder" 

Call Size(folder.size)
	  Response.Write "<td class=""" & bgcolor & """ align=""center"" valign=""bottom"">" & folder.datelastmodified & "</td>" &vbCrLf
	  Response.Write "<td class=""" & bgcolor & """ align=""center"" valign=""bottom""><a class=fileLink href=""" & scriptname & "?action=RenameFolder&formType=editUser&path=" & sPath & "&folder=" & folder.name & "&userID=" & iUserID & "&sec=docs""><img border=0 alt=rename src=images/rename3.gif></a> <a href=""" & scriptname & "?action=deletefolder&formType=editUser&sec=docs&path=" & sPath & "&folder=" & folder.name & "&userID=" & iUserID & """><img border=0 alt=delete src=images/remove.gif></a></td>" &vbCrLf
    Response.Write "</tr>" &vbCrLf
  Next 
  Set foldercollection=nothing

  ' Use the Files property to get the files contained in the directory specified in sPath
  Set filecollection = fileobject.Files
  
	' Loop through the files contained in the filescollection and dislay their information on the page
	For Each file in filecollection 
    ' Apply our alternating line coloring
    If lineid = 0 Then
      bgcolor = bgcolor_off
      lineid = 1
    Else
      bgcolor = bgcolor_on
      lineid = 0
    End if	
    Response.Write "<tr>" &vbCrLf  
%>
  <%
	if fs.GetExtensionName(file.name)="gif" then image="<img src=images/gif.gif >"
	if fs.GetExtensionName(file.name)="GIF" then image="<img src=images/gif.gif >"
	if fs.GetExtensionName(file.name)="pdf" then image="<img src=images/pdf.gif >"
	if fs.GetExtensionName(file.name)="PDF" then image="<img src=images/pdf.gif >"
	if fs.GetExtensionName(file.name)="css" then image="<img src=images/css.gif >"
	if fs.GetExtensionName(file.name)="CSS" then image="<img src=images/css.gif >"
	if fs.GetExtensionName(file.name)="doc" then image="<img src=images/word.gif >"
	if fs.GetExtensionName(file.name)="DOC" then image="<img src=images/word.gif >"
	if fs.GetExtensionName(file.name)="xls" then image="<img src=images/xls.gif >"
	if fs.GetExtensionName(file.name)="XLS" then image="<img src=images/xls.gif >"
	if fs.GetExtensionName(file.name)="exe" then image="<img src=images/exe.gif >"
	if fs.GetExtensionName(file.name)="EXE" then image="<img src=images/exe.gif >"
	if fs.GetExtensionName(file.name)="zip" then image="<img src=images/zip.gif >"
	if fs.GetExtensionName(file.name)="ZIP" then image="<img src=images/zip.gif >"
	if fs.GetExtensionName(file.name)="jpg" then image="<img src=images/jpg.gif >"
	if fs.GetExtensionName(file.name)="JPG" then image="<img src=images/jpg.gif >"
	if fs.GetExtensionName(file.name)="jpeg" then image="<img src=images/jpg.gif >"
	if fs.GetExtensionName(file.name)="JPEG" then image="<img src=images/jpg.gif >"
	if fs.GetExtensionName(file.name)="htm" then image="<img src=images/htm.gif >"
	if fs.GetExtensionName(file.name)="HTM" then image="<img src=images/htm.gif >"
	if fs.GetExtensionName(file.name)="html" then image="<img src=images/htm.gif >"
	if fs.GetExtensionName(file.name)="HTML" then image="<img src=images/htm.gif >"
	if fs.GetExtensionName(file.name)="swf" then image="<img src=images/swf.gif >"
	if fs.GetExtensionName(file.name)="SWF" then image="<img src=images/swf.gif >"
	if fs.GetExtensionName(file.name)="asp" then image="<img src=images/file.gif >"
	if fs.GetExtensionName(file.name)="ASP" then image="<img src=images/file.gif >"
	if fs.GetExtensionName(file.name)="txt" then image="<img src=images/file.gif >"
	if fs.GetExtensionName(file.name)="TXT" then image="<img src=images/file.gif >"
	if fs.GetExtensionName(file.name)="inc" then image="<img src=images/inc.gif >"
	if fs.GetExtensionName(file.name)="INC" then image="<img src=images/inc.gif >"
	if fs.GetExtensionName(file.name)="js" then image="<img src=images/js.gif >"
	if fs.GetExtensionName(file.name)="JS" then image="<img src=images/js.gif >"
	if fs.GetExtensionName(file.name)="mdb" then image="<img src=images/mdb.gif >"
	if fs.GetExtensionName(file.name)="MDB" then image="<img src=images/mdb.gif >"
	
	if image="" then image= "<img src=images/unknown.gif >"
''Response.Write "<td bgcolor=""" & bgcolor & """ align=""left"" valign=""bottom"">"&image&" <a href=""" & scriptname & "?action=editfile&path=" & sPath & "&file=" & file.name & "&filetype=" & Lcase(fs.GetExtensionName(file.name)) & """>" & file.name & "</a></td>" &vbCrLf
Response.Write "<td class=""" & bgcolor & """ align=""left"" valign=""bottom"">"&image&" <a class=fileLink target=new href=""" & sPath&"/" & file.name &""">" & file.name & "</a></td>" &vbCrLf
	image=""
		Response.Write "<td class=""" & bgcolor & """ align=""center"" valign=""bottom"">" & fs.GetExtensionName(file.name) & "</td>" &vbCrLf
	  Call Size(file.size)
	  Response.Write "<td class=""" & bgcolor & """ align=""center"" valign=""bottom"">" & file.datelastmodified & "</td>" &vbCrLf
	  Response.Write "<td class=""" & bgcolor & """ align=""center"" valign=""bottom""><a class=fileLink href=""" & scriptname & "?action=RenameFile&formType=editUser&path=" & sPath & "&file=" & file.name & "&filetype=" & Lcase(fs.GetExtensionName(file.name)) & "&userID=" & iUserID & "&sec=docs""><img border=0 alt=rename src=images/rename3.gif></a> <a href=""" & scriptname & "?action=deletefile&formType=editUser&path=" & sPath & "&file=" & file.name & "&filetype=" & Lcase(fs.GetExtensionName(file.name)) & "&userID=" & iUserID & "&sec=docs""><img border=0 alt=delete src=images/remove.gif></a></td>" &vbCrLf
    Response.Write "</tr>" &vbCrLf
  Next

  ' We are done displaying information about files and folders in this directory, so close the table.
  Response.Write "</table>" &vbCrLf%>
  <!--show a list of the reports that have been generated for this project-->
  <br /><br /><br />
<%End Sub


Sub DisplayErrors
  Response.Write "U heeft: " & errornum & " niet toegestane akties ondernomen."
	Response.Write "<ul>" & errorcode & "</ul>" & vbCrlf
End Sub
%>


