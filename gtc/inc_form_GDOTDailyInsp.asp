<%
'get the location exits from the last report
Set oCmd = Server.CreateObject("ADODB.Command")

With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetLastLocationExits"
   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, iProjectID)
   .CommandType = adCmdStoredProc										   
End With
		
Set rsLastExit = oCmd.Execute
Set oCmd = nothing

if not rsLastExit.eof then
	sExits = rsLastExit("exitLocations")
end if	

'response.Write 	sAnswer & "<br>"							
%>
<table cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td align="center" colspan="2"><strong><u>CONSTRUCTION EXIT</u></strong></td>
	</tr>
	<tr>
		<td colspan="2">1.&nbsp;Tracking material onto roadway?&nbsp;
			<input type="radio" name="materialOnRoadway" value="No" checked="checked" />&nbsp;No&nbsp;&nbsp;<input type="radio" name="materialOnRoadway" value="Yes" />&nbsp;Yes
		</td>
	</tr>
	<tr><td colspan="2" height="10"></td></tr>
	<tr>
		<td colspan="2">2.&nbsp;Stone consolidated?&nbsp;
			<input type="radio" name="stoneConsolidated" value="No" />&nbsp;No&nbsp;&nbsp;<input type="radio" name="stoneConsolidated" value="Yes" checked="checked" />&nbsp;Yes
		</td>
	</tr>
	<tr><td colspan="2" height="10"></td></tr>
	<tr>
		<td colspan="2">3.&nbsp;Maintenance required?&nbsp;
			<input type="radio" name="maintenanceRequired" value="No" checked="checked" />&nbsp;No&nbsp;&nbsp;<input type="radio" name="maintenanceRequired" value="Yes" />&nbsp;Yes
		</td>
	</tr>
	<tr><td colspan="2" height="10"></td></tr>
	<tr>
		<td valign="top">4.&nbsp;Location of exits:&nbsp;</td>
		<td valign="top">			
			<textarea name="exitLocations" rows="4" cols="30"><%=sExits%></textarea>
		</td>
	</tr>
	<tr><td colspan="2" height="20"></td></tr>
	<tr>
		<td align="center" colspan="2"><strong><u>PETROLEUM PRODUCTS STORAGE/TRANSFER</u></strong></td>
	</tr>
	<tr>
		<td colspan="2">1.&nbsp;Spills/leaks of petroleum products from vehicles?&nbsp;
			<input type="radio" name="spillsVehicle" value="No" checked="checked" />&nbsp;No&nbsp;&nbsp;<input type="radio" name="spillsVehicle" value="Yes" />&nbsp;Yes
		</td>
	</tr>
	<tr><td colspan="2" height="10"></td></tr>
	<tr>
		<td colspan="2">2.&nbsp;Spills/leaks of petroleum products from equipment?&nbsp;
			<input type="radio" name="spillsEquipment" value="No" checked="checked"  />&nbsp;No&nbsp;&nbsp;<input type="radio" name="spillsEquipment" value="Yes"/>&nbsp;Yes
		</td>
	</tr>
	<tr><td colspan="2" height="10"></td></tr>
	<tr>
		<td colspan="2">3.&nbsp;Spills/leaks of petroleum products from storage tanks?&nbsp;
			<input type="radio" name="spillsStorageTanks" value="No" checked="checked" />&nbsp;No&nbsp;&nbsp;<input type="radio" name="spillsStorageTanks" value="Yes" />&nbsp;Yes
		</td>
	</tr>
	<tr><td colspan="2" height="10"></td></tr>
	<tr>
		<td valign="top">4.&nbsp;If the answer to 1, 2, or 3 <br />&nbsp;&nbsp;&nbsp;was yes, describe how <br />&nbsp;&nbsp;&nbsp;the spill/leak was handled:&nbsp;</td>
		<td valign="top">			
			<textarea name="howSpillHandled" rows="4" cols="30"></textarea>
		</td>
	</tr>
</table>	