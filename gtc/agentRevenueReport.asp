<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%

iProjectID = request("projectID")
'inspector = request("inspector")
smonth = request("month")
syear = request("year")
clientID = request("clientID")


If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

'get an array of inspector ids
for i=1 to Request("inspector").count
	listboxvalue=Request.form("inspector").item(i)
	uIDs = uIDs & listboxvalue
	
	if i <> Request("inspector").count then
		uIDs = uIDs & ","
	end if
next

'response.Write uIDs


Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Agent Revenue Report</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<a href="expAgentRevenueReport.asp?projectID=<%=iProjectID%>&inspector=<%=inspector%>&month=<%=smonth%>&year=<%=syear%>&clientID=<%=clientID%>&uIDs=<%=uIDs%>" class="footerLink">Export Data to Excel</a>&nbsp;&nbsp;
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="chooseAgentRevenue.asp" class="footerLink">back to selections</a>			
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="848" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td></td>
											<td colspan="6">
												<strong><%=getMonth(smonth) & " " & sYear%></strong><br>
												<strong>Total # of inspection days <%=InspectionDaysInMonth(sYear,smonth)%></strong>
											</td>
										</tr>										
										<tr><td colspan="7"><img src="images/pix.gif" width="1" height="20"></td></tr>
										<tr>
											<td></td>
											<td colspan="6">
												<strong>Weekly/Post Rain Reports</strong><br>
												Click on the agents name to see details of each report done.
											</td>
										</tr>
										<tr>
											<td colspan="7">
												<table width="848" cellpadding="0" cellspacing="0" border="0">
													<tr bgcolor="#666666">
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td><span class="searchText">Customer</span></td>
														<td width="10"></td>
														<td><span class="searchText">Project Name</span></td>
														<td width="10"></td>
														<!--<td><span class="searchText">Agent/Inspector</span></td>-->
														<td><span class="searchText">Monthly<br>Revenue<br>Rate</span></td>
														<td width="10"></td>
														<td><span class="searchText">Monthly<br>Pay<br>Rate</span></td>
														<td width="10"></td>
														<td><span class="searchText">Total<br>Reports</span></td>
														<td width="10"></td>
														<td><span class="searchText">Total<br>Reports<br>By Insp.</span></td>
														<td width="10"></td>
														<td><span class="searchText">Rate Per<br>Insp.</span></td>
														<td width="10"></td>
														<td><span class="searchText">Agent<br>Pay</span></td>
														<td width="10"></td>
														<td><span class="searchText">Revenue<br>by<br>Inspector</span></td>
														
														<!--<td><span class="searchText">Comp</span></td>-->
													</tr>
													<tr><td colspan="20" height="10"></td></tr>
													<%
													blnChange = false
													iReportCount = 0
													
													for i=1 to Request("inspector").count
														listboxvalue=Request.form("inspector").item(i)
														
														Set oCmd = Server.CreateObject("ADODB.Command")			
														With oCmd
														   .ActiveConnection = DataConn
														   .CommandText = "spGetUser"
														   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, listboxvalue)
														   .CommandType = adCmdStoredProc   
														End With
																	
														Set rsUser = oCmd.Execute
														Set oCmd = nothing
														
														%>

														<tr bgcolor="#666666">
															<td colspan="20">
																<span class="searchText"><strong>Inspector/Agent:&nbsp;<%=rsUser("firstName")%>&nbsp;<%=rsUser("lastName")%></strong></span>
															</td>
														</tr>
														<%Set oCmd = Server.CreateObject("ADODB.Command")
			
														With oCmd
														   .ActiveConnection = DataConn
														   .CommandText = "spGetAgentPayByAgent"
														   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, listboxvalue)
														   .parameters.Append .CreateParameter("@month", adInteger, adParamInput, 8, smonth)
														   .parameters.Append .CreateParameter("@year", adInteger, adParamInput, 8, syear)
														   .CommandType = adCmdStoredProc   
														End With
																	
														Set rsReport = oCmd.Execute
														Set oCmd = nothing
														
														iInspRate = 0
														iTotInspRate = 0
														iTotalReports = 0
														iMonthlyPayRate = 0
														iMonthlyBillRate = 0
														iTotNumRep = 0
														do until rsReport.eof
													
															'get the total number of reports for this project															
															Set oCmd = Server.CreateObject("ADODB.Command")
																
															With oCmd
															   .ActiveConnection = DataConn
															   .CommandText = "spGetNumReportsByProjectandDate"
															   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, rsReport("projectID"))
															   .parameters.Append .CreateParameter("@month", adInteger, adParamInput, 8, smonth)
															   .parameters.Append .CreateParameter("@year", adInteger, adParamInput, 8, syear)
															   .CommandType = adCmdStoredProc   
															End With																		
															Set rsTot = oCmd.Execute
															Set oCmd = nothing
																													
															If blnChange = true then%>
																<tr class="reportRowColor">
															<%else%>
																<tr>
															<%end if
																iReportCount = 	iReportCount + 1%>
																<td></td>
																<td><%=rsReport("customerName")%></td>
																<td width="10"></td>
																<td><%=rsReport("projectName")%></td>
																<td width="10"></td>
																<!--<td><%'=rsReport("firstName") & " " & rsReport("lastName")%></td>-->
																<td>
																	<%
																	'response.Write month(rsReport("startDate")) & "<br>"
																	if trim(smonth) = trim(month(rsReport("startDate"))) then
																		if trim(syear) = trim(year(rsReport("startDate"))) then
												
																			iBillRate = (rsReport("billRate")/21)*rsReport("newStartBillingDays")																			
																																				
																			if rsReport("primaryInspector") = rsReport("userID") then%>
																				<a href="form.asp?id=<%=rsReport("projectID")%>&formType=editRates&customerID=<%=rsReport("customerID")%>&divisionID=<%=rsReport("divisionID")%>" target="_blank"><%response.Write formatcurrency(iBillRate,2)%></a>
																			<%else%>
																				Not PI (<%response.Write formatcurrency(iBillRate,2)%>)
																			<%end if%>	
																			
																																				
																			
																		<%else
																			iBillRate = rsReport("billRate")%>
																			<%'if not the primary inspector, do not display rate
																			if rsReport("primaryInspector") = rsReport("userID") then%>
																				<a href="form.asp?id=<%=rsReport("projectID")%>&formType=editRates&customerID=<%=rsReport("customerID")%>&divisionID=<%=rsReport("divisionID")%>" target="_blank"><%response.Write formatcurrency(iBillRate,2)%></a>
																			<%else%>
																				Not PI (<%response.Write formatcurrency(iBillRate,2)%>)
																			<%end if%>
																		<%end if
																	else
																		iBillRate = rsReport("billRate")%>
																		<%'if not the primary inspector, do not display rate
																		if rsReport("primaryInspector") = rsReport("userID") then%>
																			<a href="form.asp?id=<%=rsReport("projectID")%>&formType=editRates&customerID=<%=rsReport("customerID")%>&divisionID=<%=rsReport("divisionID")%>" target="_blank"><%response.Write formatcurrency(iBillRate,2)%></a>
																		<%else%>
																			Not PI (<%response.Write formatcurrency(iBillRate,2)%>)
																		<%end if%>
																	<%end if%>															
																
																	
																</td>
																<td width="10"></td>
																<td>
																	<a href="form.asp?id=<%=rsReport("projectID")%>&formType=editRates&customerID=<%=rsReport("customerID")%>&divisionID=<%=rsReport("divisionID")%>" target="_blank"><%response.Write formatcurrency(rsReport("wprRate"),2)%></a>
																</td>
																<td width="10"></td>
																<td>
																	<%=rsTot("totalNumReports")%>
																	<%iTotNumRep = iTotNumRep + rsTot("totalNumReports")%>
																</td>
																<td width="10"></td>
																<td>
																	<%=rsReport("totalReports")%>
																	<%iTotalReports = iTotalReports + rsReport("totalReports")%>
																</td>
																<td width="10"></td>
																<%
											
																'get the monthlyRate
																if isNull(rsReport("wprRate")) then																
																else
																	iMonthlyPayRate = iMonthlyPayRate + rsReport("wprRate")
																end if
																
																if rsReport("primaryInspector") = rsReport("userID") then
																	if isNull(iBillRate) then
																	else
																		iMonthlyBillRate = iMonthlyBillRate + iBillRate
																	end if
																end if
																
																%>
																<td>
																	<%
																	'get the rate per inspection
																	if isNull(rsReport("wprRate")) then	
																		iInspRate = 0															
																	else
																		iInspRate = rsReport("wprRate") / rsTot("totalNumReports")
																	end if
																	
																	response.Write formatcurrency(iInspRate,2)
																	%>
																</td>
																<td width="10"></td>
																<td>
																	<%
																	if trim(smonth) = trim(month(rsReport("startDate"))) then
																		if trim(syear) = trim(year(rsReport("startDate"))) then
																			iInspRate = iBillRate/2
																			iTotInspRate = iTotInspRate + iInspRate
																			response.Write formatcurrency(iInspRate,2)
																		else
																			'calculate the pay for the agent
																			iInspRate = iInspRate * rsReport("totalReports")
																			iTotInspRate = iTotInspRate + iInspRate
																			response.Write formatcurrency(iInspRate,2)
																		end if
																		
																	else
																		'calculate the pay for the agent
																		iInspRate = iInspRate * rsReport("totalReports")
																		iTotInspRate = iTotInspRate + iInspRate
																		response.Write formatcurrency(iInspRate,2)
																	end if%>
																</td>
																<td></td>
																<td>
																	<%
																	revByInsp = (iBillRate/rsTot("totalNumReports")) * rsReport("totalReports")
																	totRevByInsp = totRevByInsp + revByInsp
																	response.Write formatcurrency(revByInsp,2)
																	%>
																
																
																</td>
															</tr>
															
															<%
															if blnChange = true then
																blnChange = false
															else
																blnChange = true
															end if
													
														rsReport.movenext
		
														loop
														rsReport.close
														set rsReport = nothing%>	
														
														<tr><td colspan="20" height="3"></td></tr>
														<tr>
															<td colspan="5" align="right"><strong>Totals: </strong>&nbsp;</td>
															<td><strong><%=formatcurrency(iMonthlyBillRate,2)%></strong></td>
															<td width="10"></td>
															<td><strong><%=formatcurrency(iMonthlyPayRate,2)%></strong></td>
															<td width="10"></td>
															<td><strong><%=iTotNumRep%></strong></td>
															<td width="10"></td>
															<td><strong><%=iTotalReports%></strong></td>
															<td></td>
															<td></td>
															<td></td>
															<td><strong><%=formatcurrency(iTotInspRate,2)%></strong></td>
															<td></td>
															<td>
																<strong><%=formatcurrency(totRevByInsp,2)%></strong>
															</td>	
														</tr>												
														<tr><td colspan="20" height="10"></td></tr>
														
													<%next%>
													
													
																	
												</table>
											</td>
										</tr>										
										<tr><td colspan="7"><img src="images/pix.gif" width="1" height="20"></td></tr>										
									
									</table>
									
									
									
									<table width="848" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td></td>
											<td colspan="6">
												<strong>Water Sampling Reports</strong>
											</td>
										</tr>
										<tr>
											<td colspan="7">
												<table width="848" cellpadding="0" cellspacing="0" border="0">
													<tr bgcolor="#666666">
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td><span class="searchText">Customer</span></td>
														<td width="10"></td>
														<td><span class="searchText">Project Name</span></td>
														<td width="10"></td>
														<!--<td><span class="searchText">Agent/Inspector</span></td>-->
														<td><span class="searchText">Water<br>Sampling<br>Rate</span></td>
														<td width="10"></td>
														<td><span class="searchText">Total<br>Samples</span></td>
														<td width="10"></td>
														<td><span class="searchText">Total<br>Samples<br>By Insp.</span></td>
														<td width="10"></td>
														<td><span class="searchText">Agent<br>Pay</span></td>
														<td width="10"></td>
														<td><span class="searchText">Revenue<br>by<br>Inspector</span></td>
														
														<!--<td><span class="searchText">Comp</span></td>-->
													</tr>
													<tr><td colspan="20" height="10"></td></tr>
													<%
													blnChange = false
													iReportCount = 0
													
													for i=1 to Request("inspector").count
														listboxvalue=Request.form("inspector").item(i)
														
														Set oCmd = Server.CreateObject("ADODB.Command")			
														With oCmd
														   .ActiveConnection = DataConn
														   .CommandText = "spGetUser"
														   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, listboxvalue)
														   .CommandType = adCmdStoredProc   
														End With
																	
														Set rsUser = oCmd.Execute
														Set oCmd = nothing
														
														%>

														<tr bgcolor="#666666">
															<td colspan="20">
																<span class="searchText"><strong>Inspector/Agent:&nbsp;<%=rsUser("firstName")%>&nbsp;<%=rsUser("lastName")%></strong></span>
															</td>
														</tr>
														<%Set oCmd = Server.CreateObject("ADODB.Command")
			
														With oCmd
														   .ActiveConnection = DataConn
														   .CommandText = "spGetAgentPayByAgentWS"
														   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, listboxvalue)
														   .parameters.Append .CreateParameter("@month", adInteger, adParamInput, 8, smonth)
														   .parameters.Append .CreateParameter("@year", adInteger, adParamInput, 8, syear)
														   .CommandType = adCmdStoredProc   
														End With
																	
														Set rsReport = oCmd.Execute
														Set oCmd = nothing
														
														iInspRate = 0
														iTotInspRate = 0
														iTotalReports = 0
														iMonthlyPayRate = 0
														iMonthlyBillRate = 0
														iTotNumRep = 0
														do until rsReport.eof
													
															'get the total number of reports for this project															
															Set oCmd = Server.CreateObject("ADODB.Command")
																
															With oCmd
															   .ActiveConnection = DataConn
															   .CommandText = "spGetNumSamplesByProjectandDate"
															   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, rsReport("projectID"))
															   .parameters.Append .CreateParameter("@month", adInteger, adParamInput, 8, smonth)
															   .parameters.Append .CreateParameter("@year", adInteger, adParamInput, 8, syear)
															   .CommandType = adCmdStoredProc   
															End With																		
															Set rsTot = oCmd.Execute
															Set oCmd = nothing
																													
															If blnChange = true then%>
																<tr class="reportRowColor">
															<%else%>
																<tr>
															<%end if
																iReportCount = 	iReportCount + 1%>
																<td></td>
																<td><%=rsReport("customerName")%></td>
																<td width="10"></td>
																<td><%=rsReport("projectName")%></td>
																<td width="10"></td>
																<!--<td><%'=rsReport("firstName") & " " & rsReport("lastName")%></td>-->
																<td>
																	<%
																	'response.Write month(rsReport("startDate")) & "<br>"
																	iBillRate = rsReport("waterSamplingRate")
																	response.Write 	formatcurrency(iBillRate,2)													
																%>
																	
																</td>
																<td width="10"></td>
																<td>
																	<%=rsTot("totalNumReports")%>
																	<%iTotNumRep = iTotNumRep + rsTot("totalNumReports")%>
																</td>
																<td width="10"></td>
																<td>
																	<%=rsReport("totalReports")%>
																	<%iTotalReports = iTotalReports + rsReport("totalReports")%>
																</td>
																<td width="10"></td>
																<td>
																	<%
																	response.Write formatcurrency(0,2)%>
																</td>
																<td></td>
																<td>
																	<%
																	revByInspWS = (iBillRate*rsTot("totalNumReports"))
																	totRevByInspWS = totRevByInspWS + revByInspWS
																	response.Write formatcurrency(revByInspWS,2)
																	%>
																
																
																</td>
															</tr>
															
															<%
															if blnChange = true then
																blnChange = false
															else
																blnChange = true
															end if
													
														rsReport.movenext
		
														loop
														rsReport.close
														set rsReport = nothing%>	
														
														<tr><td colspan="20" height="3"></td></tr>
														<tr>
															<td colspan="5" align="right"><strong>Totals: </strong>&nbsp;</td>
															<td></td>
															<td width="10"></td>
															<td><strong><%=iTotNumRep%></strong></td>
															<td width="10"></td>
															<td><strong><%=iTotalReports%></strong></td>
															<td></td>
															<td></td>
															<td></td>
															<td>
																<strong><%=formatcurrency(totRevByInspWS,2)%></strong>
															</td>	
														</tr>
														
														<tr>
															<td colspan="13" align="right"><strong>Grand Total: </strong>&nbsp;</td>
															<td>
																<strong><%=formatcurrency(totRevByInsp + totRevByInspWS,2)%></strong>
															</td>	
														</tr>													
														<tr><td colspan="20" height="10"></td></tr>
														
													<%next%>
													
													
																	
												</table>
											</td>
										</tr>										
										<tr><td colspan="7"><img src="images/pix.gif" width="1" height="20"></td></tr>										
									
									</table>
															
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>
<%
'rsCustomer.close
rsReport.close
DataConn.close
Set rsCustomer = nothing
Set rsReport = nothing
Set DataConn = nothing
Set oCmd = nothing
%>