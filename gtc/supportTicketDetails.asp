<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
Dim rs, oCmd, DataConn

supportTicketID = Request("supportTicketID")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

Set oCmd = Server.CreateObject("ADODB.Command")	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetSupportTicket"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, supportTicketID)
   .CommandType = adCmdStoredProc   
End With
			
Set rs = oCmd.Execute
Set oCmd = nothing

%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3" class="colorBars">
			<img src="images/pix.gif" width="5" height="10">
		</td>
	</tr>
	<tr>
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td colspan="15">
												<strong><%=rs("firstName")%>&nbsp;<%=rs("lastName")%></strong><br>
												<em><%=rs("dateAdded")%></em><br><br>
												<strong>Importance:</strong> <%=rs("importance")%><br>
												<strong>Category:</strong> <%=rs("category")%><br><br>
												
												<%=rs("message")%><br><br>
												
												<strong>Resolved:</strong> 
												<%If rs("isClosed") = "True" then%>
													<img src="images/check.gif">
												<%end if%><br>
												<strong>Resolved By:</strong> <%=rs("firstNameCB")%>&nbsp;<%=rs("lastNameCB")%><br>
												<strong>Date Resolved:</strong>  <%=rs("dateClosed")%><br><br>
									
												
											<!--	<strong>Responses from support team:</strong><br>
												<%'get the responses from the support team regarding this ticket
											'	Set oCmd = Server.CreateObject("ADODB.Command")	
											'	With oCmd
											'	   .ActiveConnection = DataConn
											'	   .CommandText = "spGetSupportTicketComments"
											'	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, supportTicketID)
											'	   .CommandType = adCmdStoredProc   
											'	End With
															
											'	Set rsResponse = oCmd.Execute
											'	Set oCmd = nothing												
												
												
											'	do until rsResponse.eof%>
													<%'=rsResponse("commentDate")%>
													<%'=rsResponse("supportComments")%><br><br>
												<%'rsResponse.movenext
											'	loop%>-->
											</td>
										</tr>
									</table>
															
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<tr>
		<td colspan="3" bgcolor="#3C2315">
			<img src="images/pix.gif" width="5" height="10">
		</td>
	</tr>
</table>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>