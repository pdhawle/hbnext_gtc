<table>
	<tr><td height="20"></td></tr>
	<tr>
		<td align="center">
			<%
			'get the client information
			Set oCmd = Server.CreateObject("ADODB.Command")		
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spGetClient"
			   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
			   .CommandType = adCmdStoredProc
				   
			End With
					
			Set rsClient = oCmd.Execute
			Set oCmd = nothing
			
			if projectID <> "" then
				if projectID <> "0" then
					Set oCmd = Server.CreateObject("ADODB.Command")		
					With oCmd
					   .ActiveConnection = DataConn
					   .CommandText = "spGetDivisionByProject"
					   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, projectID)
					   .CommandType = adCmdStoredProc
						   
					End With
							
					Set rsDivision = oCmd.Execute
					Set oCmd = nothing
					
				end if
			end if
			
			%>
			<strong><%=ucase(rsClient("clientName"))%></strong><br />
			<strong>VEHICULAR ACCIDENT REPORT</strong>
		</td>
	</tr>
	<tr><td height="20"></td></tr>	
	<tr>
		<td>
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td colspan="5">
						<strong>COMPANY VEHICLE IN ACCIDENT:</strong>
					</td>
				</tr>
				<tr>
					<td align="right">
						Year and Make:&nbsp;
					</td>
					<td>
						<input type="text" name="yearAndMake" size="20" value="<%=rs("yearAndMake")%>"><//>
					</td>
					<td width="20"></td>
					<td align="right">
						Model:&nbsp;
					</td>
					<td>
						<input type="text" name="model" size="10" value="<%=rs("model")%>"/>
					</td>
				</tr>
				<tr>
					<td align="right">
						Driver's Name:&nbsp;
					</td>
					<td>
						<input type="text" name="driversName" size="20" value="<%=rs("driversName")%>"/>
					</td>
					<td></td>
					<td align="right">
						Unit Number:&nbsp;
					</td>
					<td>
						<input type="text" name="unitNumber" size="10" value="<%=rs("unitNumber")%>"/>
					</td>
				</tr>
				<tr>
					<td align="right">
						Division:&nbsp;
					</td>
					<td>
						<%'if request("division") <> "" then
							'division = request("division")
						'else
							'division = rsDivision("division")
						'end if
						%>
						<input type="text" name="division" size="20" value="<%=rs("division")%>"/>
					</td>
					<td></td>
					<td align="right">
						Foreman:&nbsp;
					</td>
					<td>
						<input type="text" name="foreman" size="10" value="<%=rs("foreman")%>"/>
					</td>
				</tr>
			</table><br />
		
			Hours you were on duty before accident:&nbsp;<input type="text" name="hoursOnDuty" size="3" value="<%=rs("hoursOnDuty")%>"/>&nbsp;&nbsp;Years in employ of this Company?&nbsp;<input type="text" name="yearsEmployed" size="3" value="<%=rs("yearsEmployed")%>"/><br />
			How many previous automobile accidents have you had while employed?</strong>&nbsp;<input type="text" name="previousAccidentsEmploy" size="3" value="<%=rs("previousAccidentsEmploy")%>"/><br />
			How many previous automobile accidents this year?&nbsp;<input type="text" name="previousAccidentsYear" size="3" value="<%=rs("previousAccidentsYear")%>"/><br />
			<br />Describe in detail what you were doing immediately prior to the <br />accident that required your use of the company vehicle?<br />
			<textarea name="doingPriorToAccident" rows="5" cols="40"><%=rs("doingPriorToAccident")%></textarea><br /><br />
			
			
			<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td align="right">Date of Accident:&nbsp;</td>
					<td>
						<%'if request("accidentDate") <> "" then
						'	accidentDate = request("accidentDate")
						'else
							'accidentDate = formatdatetime(date(),2)
						'end if
						%>
						<input type="text" name="accidentDate" maxlength="10" size="10" value="<%=rs("accidentDate")%>"/>&nbsp;<a href="javascript:displayDatePicker('accidentDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
					</td>
					<td width="5"></td>
					<td align="right">Day of Week:&nbsp;</td>
					<td><%
					'	if trim(request("dayOfWeek")) = "" then
					'		sDay = getDay(weekDay(Date))
					'	else
					'		sDay = trim(request("dayOfWeek"))
					'	end if
					'response.Write rs("dayOfWeek")
						%>
						<select name="dayOfWeek">
							<option <%=isSelected(rs("dayOfWeek"),"Sunday")%>>Sunday</option>
							<option <%=isSelected(rs("dayOfWeek"),"Monday")%>>Monday</option>
							<option <%=isSelected(rs("dayOfWeek"),"Tuesday")%>>Tuesday</option>
							<option <%=isSelected(rs("dayOfWeek"),"Wednesday")%>>Wednesday</option>
							<option <%=isSelected(rs("dayOfWeek"),"Thursday")%>>Thursday</option>
							<option <%=isSelected(rs("dayOfWeek"),"Friday")%>>Friday</option>
							<option <%=isSelected(rs("dayOfWeek"),"Saturday")%>>Saturday</option>
						</select>
					</td>
					<td width="5"></td>
					<td align="right">Time:&nbsp;</td>
					<td>
						<%'if request("timeofAccident") <> "" then
						'	timeofAccident = request("timeofAccident")
						'else
						'	timeofAccident = "8:00 am"
						'end if
						%>
						<input id="timeofAccident" name="timeofAccident" type="text" value="<%=rs("timeofAccident")%>" size=8 maxlength=8 ONBLUR="validateDatePicker(this)">&nbsp;<IMG SRC="images/timepicker.gif" BORDER="0" ALT="Pick a Time!" ONCLICK="selectTime(this,timeofAccident)" STYLE="cursor:hand">
					</td>
				</tr>
			</table>
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td align="right">State of Accident:&nbsp;</td>
					<td>
						<select name="state" onChange="return state_onchange(addAccidentReport)">
							<option value="">--select state--</option>
							<%do until rsState.eof
								if rs("state") = rsState("stateID") then%>
									<option selected="selected" value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
								<%else%>
									<option value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
							<%end if
							rsState.movenext
							loop%>
						</select>	
					</td>
					<td></td>
					<td align="right">County:&nbsp;</td>
					<td colspan="3">
						<%if sState = "" then%>
							Please select a state above.
						<%else
							%>
							<select name="county">
								<%do until rsCounty.eof
									if rs("county") = rsCounty("countyID") then%>
										<option selected="selected" value="<%=rsCounty("countyID")%>"><%=rsCounty("county")%></option>
									<%else%>
										<option value="<%=rsCounty("countyID")%>"><%=rsCounty("county")%></option>
								<%end if
								rsCounty.movenext
								loop%>
							</select>
						<%end if%>
					</td>
				</tr>
				<tr>
					<td colspan="8">
						<br /><strong>ROAD WHERE ACCIDENT OCCURRED:</strong><br />
						
						<table cellpadding="0" cellspacing="0">
							<tr>
								<td align="right">Give name of street or highway number:&nbsp;</td>
								<td><input type="text" name="streetNumber" size="30" value="<%=rs("streetNumber")%>"/></td>
							</tr>
							<tr>
								<td align="right">Give name of any pertinent intersecting streets:&nbsp;</td>
								<td><input type="text" name="intersectingStreets" size="30" value="<%=rs("intersectingStreets")%>"/></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="8">
						Was the incident reported to the police?&nbsp;<input type="radio" name="reportedToPolice" value="Yes" <%=isCheckedRadio(rs("reportedToPolice"),"Yes")%>/>&nbsp;Yes&nbsp;&nbsp;<input type="radio" name="reportedToPolice" value="No" <%=isCheckedRadio(rs("reportedToPolice"),"No")%> />&nbsp;No&nbsp;&nbsp;<input type="radio" name="reportedToPolice" value="NA" <%=isCheckedRadio(rs("reportedToPolice"),"NA")%> />&nbsp;NA
					</td>
				</tr>
			</table><br /><br />
			
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td colspan="5"><strong>VEHICLE NO. 1</strong></td>
				</tr>
				<tr>
					<td align="right">
						Owner:&nbsp;
					</td>
					<td>
						<input type="text" name="vehicle1Owner" size="30" value="<%=rs("clientName")%>"/>
					</td>
				</tr>
				<tr>
					<td align="right">
						Driver:&nbsp;
					</td>
					<td>
						<input type="text" name="vehicle1Driver" size="30" value="<%=rs("vehicle1Driver")%>"/>
						<!--<select name="userID">
							<%'do until rsUsers.eof
								'if trim(userID) = trim(rsUsers("userID")) then%>
									<option selected="selected" value="<%'=rsUsers("userID")%>"><%'=rsUsers("firstName")%>&nbsp;<%'=rsUsers("lastName")%></option>
								<%'else%>
									<option value="<%'=rsUsers("userID")%>"><%'=rsUsers("firstName")%>&nbsp;<%'=rsUsers("lastName")%></option>
							<%'end if
							'rsUsers.movenext
							'loop%>
						</select>-->
					</td>
				</tr>
				<%'if clientID = 3 then 'cw matthews
				'	sAddress = "1600 Kenview Drive, Marietta, GA 30060"
				'end if
				%>			
				
				<tr>
					<td align="right">
						Address:&nbsp;
					</td>
					<td>
						<input type="text" name="vehicle1Address" size="30" value="<%=rs("vehicle1Address")%>"/>
					</td>
				</tr>
				<tr>
					<td align="right">
						Age:&nbsp;
					</td>
					<td>
						<input type="text" name="vehicle1Age" size="5" value="<%=rs("vehicle1Age")%>"/>&nbsp;&nbsp;Driver�s License Number:&nbsp;<input type="text" name="vehicle1DLNo" size="10" value="<%=rs("vehicle1DLNo")%>"/>
					</td>
				</tr>
				<tr>
					<td align="right">
						Year / Make / Model:&nbsp;
					</td>
					<td>
						<input type="text" name="vehicle1YearMakeModel" size="30" value="<%=rs("vehicle1YearMakeModel")%>"/>
					</td>
				</tr>
				<tr>
					<td align="right">
						Vehicle Damage:&nbsp;
					</td>
					<td>
						<input type="text" name="vehicle1Damage" size="30" value="<%=rs("vehicle1Damage")%>"/>
					</td>
				</tr>
				<tr>
					<td align="right">
						Is Vehicle Drivable?:&nbsp;
					</td>
					<td>
						<input type="text" name="vehicle1Drivable" size="5" value="<%=rs("vehicle1Drivable")%>"/>&nbsp;&nbsp;Approximate Repair Costs:&nbsp;<input type="text" name="vehicle1RepairCost" size="10" value="<%=rs("vehicle1RepairCost")%>"/>
					</td>
				</tr>
			</table><br /><br />
			
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td colspan="5"><strong>VEHICLE NO. 2</strong></td>
				</tr>
				<tr>
					<td align="right">
						Owner:&nbsp;
					</td>
					<td>
						<input type="text" name="vehicle2Owner" size="30" value="<%=rs("vehicle2Owner")%>"/>
					</td>
				</tr>
				<tr>
					<td align="right">
						Driver:&nbsp;
					</td>
					<td>
						<input type="text" name="vehicle2Driver" size="30" value="<%=rs("vehicle2Driver")%>"/>
					</td>
				</tr>
				<tr>
					<td align="right">
						Address:&nbsp;
					</td>
					<td>
						<input type="text" name="vehicle2Address" size="30" value="<%=rs("vehicle2Address")%>"/>
					</td>
				</tr>
				<tr>
					<td align="right">
						Age:&nbsp;
					</td>
					<td>
						<input type="text" name="vehicle2Age" size="5" value="<%=rs("vehicle2Age")%>"/>&nbsp;&nbsp;Driver�s License Number:&nbsp;<input type="text" name="vehicle2DLNo" size="10" value="<%=rs("vehicle2DLNo")%>"/>
					</td>
				</tr>
				<tr>
					<td align="right">
						Year / Make / Model:&nbsp;
					</td>
					<td>
						<input type="text" name="vehicle2YearMakeModel" size="30" value="<%=rs("vehicle2YearMakeModel")%>"/>
					</td>
				</tr>
				<tr>
					<td align="right">
						Vehicle Damage:&nbsp;
					</td>
					<td>
						<input type="text" name="vehicle2Damage" size="30" value="<%=rs("vehicle2Damage")%>"/>
					</td>
				</tr>
				<tr>
					<td align="right">
						Is Vehicle Drivable?:&nbsp;
					</td>
					<td>
						<input type="text" name="vehicle2Drivable" size="5" value="<%=rs("vehicle2Drivable")%>"/>&nbsp;&nbsp;Approximate Repair Costs:&nbsp;<input type="text" name="vehicle2RepairCost" size="10" value="<%=rs("vehicle2RepairCost")%>"/>
					</td>
				</tr>
				<tr>
					<td align="right"><strong>Telephone:</strong>&nbsp;&nbsp;Home:&nbsp;</td>
					<td><input type="text" name="vehicle2HomeNumber" size="10" value="<%=rs("vehicle2HomeNumber")%>"/></td>
				</tr>
				<tr>
					<td align="right">Office:&nbsp;</td>
					<td><input type="text" name="vehicle2OfficeNumber" size="10" value="<%=rs("vehicle2OfficeNumber")%>"/></td>
				</tr>
				<tr>
					<td align="right">Cell:&nbsp;</td>
					<td><input type="text" name="vehicle2CellNumber" size="10" value="<%=rs("vehicle2CellNumber")%>"/></td>
				</tr>
				<tr>
					<td align="right">Insurance Co:&nbsp;</td>
					<td><input type="text" name="vehicle2InsuranceCo" size="30" value="<%=rs("vehicle2InsuranceCo")%>"/></td>
				</tr>
				<tr>
					<td align="right">Policy No:&nbsp;</td>
					<td><input type="text" name="vehicle2PolicyNo" size="30" value="<%=rs("vehicle2PolicyNo")%>"/></td>
				</tr>
			</table><br /><br />
			
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td align="right">Damage to property other than vehicles:&nbsp;</td>
					<td><input type="text" name="otherPropertyDamage" size="30" value="<%=rs("otherPropertyDamage")%>"/></td>
				</tr>
				<tr>
					<td align="right">Approximate Repair Costs:&nbsp;</td>
					<td><input type="text" name="otherPropertyRepairCost" size="30" value="<%=rs("otherPropertyRepairCost")%>"/></td>
				</tr>
			</table><br /><br />
			
			
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td colspan="5"><strong>INJURIES</strong></td>
				</tr>
				<tr>
					<td align="right">
						Name:&nbsp;
					</td>
					<td>
						<input type="text" name="injury1Name" size="30" value="<%=rs("injury1Name")%>"/>
					</td>
					<td width="5"></td>
					<td align="right">
						Address:&nbsp;
					</td>
					<td>
						<input type="text" name="injury1Address" size="30" value="<%=rs("injury1Address")%>"/>
					</td>
				</tr>
				<tr>
					<td align="right">
						Nature of Injuries:&nbsp;
					</td>
					<td>
						<input type="text" name="injury1NatureOfInjury" size="30" value="<%=rs("injury1NatureOfInjury")%>"/>
					</td>
					<td width="5"></td>
					<td align="right">
						Transported by Ambulance From Scene?&nbsp;
					</td>
					<td>
						<select name="injury1Ambulance">
							<option value=""></option>
							<option <%=isSelected(rs("injury1Ambulance"),"No")%>>No</option>
							<option <%=isSelected(rs("injury1Ambulance"),"Yes")%>>Yes</option>
						</select>
					</td>
				</tr>
				<tr><td colspan="5" height="10"></td></tr>
				<tr>
					<td align="right">
						Name:&nbsp;
					</td>
					<td>
						<input type="text" name="injury2Name" size="30" value="<%=rs("injury2Name")%>"/>
					</td>
					<td width="5"></td>
					<td align="right">
						Address:&nbsp;
					</td>
					<td>
						<input type="text" name="injury2Address" size="30" value="<%=rs("injury2Address")%>"/>
					</td>
				</tr>
				<tr>
					<td align="right">
						Nature of Injuries:&nbsp;
					</td>
					<td>
						<input type="text" name="injury2NatureOfInjury" size="30" value="<%=rs("injury2NatureOfInjury")%>"/>
					</td>
					<td width="5"></td>
					<td align="right">
						Transported by Ambulance From Scene?&nbsp;
					</td>
					<td>
						<select name="injury2Ambulance">
							<option value=""></option>
							<option <%=isSelected(rs("injury2Ambulance"),"No")%>>No</option>
							<option <%=isSelected(rs("injury2Ambulance"),"Yes")%>>Yes</option>
						</select>
					</td>
				</tr>
			</table><br /><br />
			
		
			Remarks made by driver and/or occupants of other vehicle (Pedestrian) at scene of accident:<br />
			<textarea name="otherDriverRemarks" rows="5" cols="40"><%=rs("otherDriverRemarks")%></textarea><br /><br />
			
			Remarks, IF ANY, made by you or persons connected with the Company at scene of accident:<br />
			<textarea name="driverRemarks" rows="5" cols="40"><%=rs("driverRemarks")%></textarea><br /><br />
			
			<table width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td>Road Character</td>
					<td width="10"></td>
					<td>Road Service</td>
					<td width="10"></td>
					<td>Road Effect</td>
					<td width="10"></td>
					<td>Light</td>
					<td width="10"></td>
					<td>Weather</td>
					<td width="10"></td>
					<td>Traffic Control</td>
				</tr>
				<tr>
					<td>
						<select name="roadCharacter">
							<option <%=isSelected(rs("roadCharacter"),"Straight Road")%>>Straight Road</option>
							<option <%=isSelected(rs("roadCharacter"),"Curve")%>>Curve</option>
							<option <%=isSelected(rs("roadCharacter"),"Level")%>>Level</option>
							<option <%=isSelected(rs("roadCharacter"),"On Grade")%>>On Grade</option>
							<option <%=isSelected(rs("roadCharacter"),"Hill Crest")%>>Hill Crest</option>
						</select>
					</td>
					<td width="10"></td>
					<td>
						<select name="roadService">
							<option <%=isSelected(rs("roadService"),"Dry")%>>Dry</option>
							<option <%=isSelected(rs("roadService"),"Wet")%>>Wet</option>
							<option <%=isSelected(rs("roadService"),"Muddy")%>>Muddy</option>
							<option <%=isSelected(rs("roadService"),"Snowy")%>>Snowy</option>
							<option <%=isSelected(rs("roadService"),"Ice")%>>Ice</option>
						</select>
					</td>
					<td width="10"></td>
					<td>
						<select name="roadEffect">
							<option <%=isSelected(rs("roadEffect"),"Shoulders")%>>Shoulders</option>
							<option <%=isSelected(rs("roadEffect"),"Holes, bump")%>>Holes, bump</option>
							<option <%=isSelected(rs("roadEffect"),"Loose matter")%>>Loose matter</option>
							<option <%=isSelected(rs("roadEffect"),"Construction")%>>Construction</option>
							<option <%=isSelected(rs("roadEffect"),"No Defects")%>>No Defects</option>
						</select>
					</td>
					<td width="10"></td>
					<td>
						<select name="light">
							<option <%=isSelected(rs("light"),"Daylight")%>>Daylight</option>
							<option <%=isSelected(rs("light"),"Dusk")%>>Dusk</option>
							<option <%=isSelected(rs("light"),"Dawn")%>>Dawn</option>
							<option <%=isSelected(rs("light"),"Dark")%>>Dark</option>
							<option <%=isSelected(rs("light"),"Other")%>>Other</option>
						</select>
					</td>
					<td width="10"></td>
					<td>
						<select name="weather">
							<option <%=isSelected(rs("weather"),"Clear")%>>Clear</option>
							<option <%=isSelected(rs("weather"),"Raining")%>>Raining</option>
							<option <%=isSelected(rs("weather"),"Snowing")%>>Snowing</option>
							<option <%=isSelected(rs("weather"),"Fog")%>>Fog</option>
							<option <%=isSelected(rs("weather"),"Other")%>>Other</option>
						</select>
					</td>
					<td width="10"></td>
					<td>
						<select name="trafficControl">
							<option <%=isSelected(rs("trafficControl"),"Stop sign")%>>Stop sign</option>
							<option <%=isSelected(rs("trafficControl"),"Stop-and-go signal")%>>Stop-and-go signal</option>
							<option <%=isSelected(rs("trafficControl"),"Officer")%>>Officer</option>
							<option <%=isSelected(rs("trafficControl"),"Railroad crossing")%>>Railroad crossing</option>
							<option <%=isSelected(rs("trafficControl"),"No traffic control")%>>No traffic control</option>
						</select>
					</td>
				</tr>
			</table><br /><br />
			
			<strong>WITNESSES:</strong><br />
			<table width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td align="right">Name:&nbsp;</td>
					<td><input type="text" name="witnessName1" size="30" value="<%=rs("witnessName1")%>"/>&nbsp;&nbsp;Phone:&nbsp;<input type="text" name="witnessPhone1" size="10" value="<%=rs("witnessPhone1")%>"/></td>
				</tr>
				<tr>
					<td align="right">Address:&nbsp;</td>
					<td colspan="3"><input type="text" name="witnessAddress1" size="52" value="<%=rs("witnessAddress1")%>"/></td>
				</tr>
				<tr><td colspan="2" height="10"></td></tr>
				<tr>
					<td align="right">Name:&nbsp;</td>
					<td><input type="text" name="witnessName2" size="30" value="<%=rs("witnessName2")%>"/>&nbsp;&nbsp;Phone:&nbsp;<input type="text" name="witnessPhone2" size="10" value="<%=rs("witnessPhone2")%>"/></td>
				</tr>
				<tr>
					<td align="right">Address:&nbsp;</td>
					<td colspan="3"><input type="text" name="witnessAddress2" size="52" value="<%=rs("witnessAddress2")%>"/></td>
				</tr>
				<tr><td colspan="2" height="10"></td></tr>
				<tr>
					<td align="right">Investigating Officer:&nbsp;</td>
					<td><input type="text" name="investigatingOfficer" size="20" value="<%=rs("investigatingOfficer")%>"/>&nbsp;&nbsp;Department:&nbsp;<input type="text" name="department" size="20" value="<%=rs("department")%>"/>&nbsp;&nbsp;Case No:&nbsp;<input type="text" name="caseNo" size="5" value="<%=rs("caseNo")%>"/></td>
				</tr>
			</table><br /><br />
			
			<strong>DESCRIBE WHAT HAPPENED</strong><br />
			Refer to vehicles by number. If third vehicle was involved, please indicate it by giving specific data. <br />Describe any details regarding the incident not 
			already covered in accident report above.<br />
			<textarea name="describeWhatHappened" rows="5" cols="40"><%=rs("describeWhatHappened")%></textarea><br /><br />
			
			Reported By:<br />
			<input type="text" name="reportedBy" size="30" maxlength="50" value="<%=rs("reportedBy")%>" />
			
		</td>
		
	</tr>
</table>
