<%
customerID = request("customerID")
clientID = session("clientID")
studentID = request("studentID")

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")
	
'Create command for customer list
'	Set oCmd = Server.CreateObject("ADODB.Command")
		
'	With oCmd
'	   .ActiveConnection = DataConn
'	   .CommandText = "spGetClassTypes"
'	   .CommandType = adCmdStoredProc   
'	End With
'		
'	Set rsClassTypes = oCmd.Execute
'	Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetStudent"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, studentID)
   .CommandType = adCmdStoredProc
   
End With			
Set rs = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomersByClientAll"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, clientID)
   .CommandType = adCmdStoredProc
   
End With
			
Set rsCust = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetStates"
   .CommandType = adCmdStoredProc   
End With
	
Set rsState = oCmd.Execute
Set oCmd = nothing

%>
<script type="text/javascript">
<!--
//function loc_onchange(addClass,customerID) {
//   document.addClass.action = 'form.asp?formType=addClass&customerID='+customerID;
//   addClass.submit(); 
//}
var isNN = (navigator.appName.indexOf("Netscape")!=-1);
function autoTab(input,len, e) {
var keyCode = (isNN) ? e.which : e.keyCode; 
var filter = (isNN) ? [0,8,9] : [0,8,9,16,17,18,37,38,39,40,46];
if(input.value.length >= len && !containsElement(filter,keyCode)) {
input.value = input.value.slice(0, len);
input.form[(getIndex(input)+1) % input.form.length].focus();
}
function containsElement(arr, ele) {
var found = false, index = 0;
while(!found && index < arr.length)
if(arr[index] == ele)
found = true;
else
index++;
return found;
}
function getIndex(input) {
var index = -1, i = 0, found = false;
while (i < input.form.length && index == -1)
if (input.form[i] == input)index = i;
else i++;
return index;
}
return true;
}
// -->
</script>
<form name="addStudent" method="post" action="process.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Edit Student</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td valign="top" align="right"><strong>Customer:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="customerID">
												<%do until rsCust.eof
													if trim(rs("customerID")) = trim(rsCust("customerID")) then%>
														<option selected="selected" value="<%=rsCust("customerID")%>"><%=rsCust("customerName")%></option>
													<%else%>
														<option value="<%=rsCust("customerID")%>"><%=rsCust("customerName")%></option>
												<%end if
												rsCust.movenext
												loop%>
												</select>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>First Name:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="firstName" maxlength="50" value="<%=rs("firstName")%>" />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Last Name:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="lastName" maxlength="50" value="<%=rs("lastName")%>" />
											</td>
										</tr>
										<tr><t
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Email Address:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="email" maxlength="50" size="30" value="<%=rs("email")%>" />
											</td>
										</tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Address 1:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="address1" maxlength="50" value="<%=rs("address1")%>" />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Address 2:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="address2" maxlength="50" value="<%=rs("address2")%>" />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>City:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="city" maxlength="50" value="<%=rs("city")%>" />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>State:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="state">
														<%do until rsState.eof
															if trim(rs("state")) = trim(rsState("stateID")) then%>
																<option selected="selected" value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
															<%else%>
																<option value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
														<%end if
														rsState.movenext
														loop%>
													</select>&nbsp;<strong>Zip:</strong>&nbsp;<input type="text" name="zip" maxlength="50" size="5" value="<%=rs("zip")%>" />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Office Phone:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%'parse the phone number
												phone = stripPhone(rs("officePhone"))
												
												phone1 = firstThree(phone)
												phone2 = secondThree(phone)
												phone3 = lastFour(phone)%>
												
												<input type="text" name="phone1" size="3" value="<%=phone1%>" maxlength="3" onKeyUp="return autoTab(this, 3, event);"/>&nbsp;
												<input type="text" name="phone2" size="3" value="<%=phone2%>" maxlength="3" onKeyUp="return autoTab(this, 3, event);"/>&nbsp;
												<input type="text" name="phone3" size="3" value="<%=phone3%>" maxlength="4" onKeyUp="return autoTab(this, 4, event);"/>&nbsp;
												ext.&nbsp;<input type="text" name="ext" size="3" value="<%=rs("ext")%>"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Cell Phone:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%cellPhone = stripPhone(rs("cellPhone"))
												
												cellPhone1 = firstThree(cellPhone)
												cellPhone2 = secondThree(cellPhone)
												cellPhone3 = lastFour(cellPhone)
												%>
												<input type="text" name="cellPhone1" size="3" value="<%=cellPhone1%>" maxlength="3" onKeyUp="return autoTab(this, 3, event);"/>&nbsp;
												<input type="text" name="cellPhone2" size="3" value="<%=cellPhone2%>" maxlength="3" onKeyUp="return autoTab(this, 3, event);"/>&nbsp;
												<input type="text" name="cellPhone3" size="3" value="<%=cellPhone3%>" maxlength="4" onKeyUp="return autoTab(this, 4, event);"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td colspan="3" align="right">
												<input type="hidden" name="studentID" value="<%=studentID%>" />
												<input type="hidden" name="clientID" value="<%=clientID%>" />
												<input type="hidden" name="processType" value="editStudent" />
												<input type="submit" value="  Save  " class="formButton"/>
											</td>
										</tr>
									</table>
								
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<script language="JavaScript" type="text/javascript">

  var frmvalidator  = new Validator("addStudent");
  frmvalidator.addValidation("firstName","req","Please enter a first name");
  frmvalidator.addValidation("lastName","req","Please enter a last name");
  frmvalidator.addValidation("email","req","Please enter an email address");
  //frmvalidator.addValidation("location","dontselect=0");
</script>