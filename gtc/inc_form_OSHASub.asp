<%
'get a list of the sub-contractors for this customer
Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetSubContractors"
   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, rsProjInfo("customerID"))
   .CommandType = adCmdStoredProc   
End With
			
Set rsSub = oCmd.Execute
Set oCmd = nothing

'get the subs into the array
do until rsSub.eof
	sSubList = sSubList & chr(34) & rsSub("subContractorName") & " - (" & rsSub("subContractorType") & ")" & chr(34)
rsSub.movenext
if not rsSub.eof then
	sSubList = sSubList & ","
end if
loop

'get info for this customer
Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomer"
   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, rsProjInfo("customerID"))
   .CommandType = adCmdStoredProc   
End With
			
Set rsCustomer = oCmd.Execute
Set oCmd = nothing
%>

<script type='text/javascript' src='jquery/jquery.autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="jquery/jquery.autocomplete.css" />
<script type="text/javascript">
var subs = [
	<%=sSubList%>
];

<%'ii = 1
'for ii = 1 to 10%>

	$().ready(function() {	
		$("#subName").autocomplete(subs);
	});
<%'next%>
</script>
	
	
<!--add questions for the subs
need to select sub to add questions for-->
<br /><br /><strong class="subHeader">Sub-contractor/Vendor Inspection Information</strong><br />
Enter sub-contractor below<br /><br />
<%
'iSub = 1
'for iSub = 1 to 10%>
	<!--create a hidden div here until the link is clicked to show it for each sub-contractor-->
	<script type="text/javascript">
		function addFormFieldSub() {
		var id = document.getElementById("idSub").value;
		$("#divTxtSub").append("<span id='row" + id + "'><label for='txt" + id + "'><b>" + id + ".</b>&nbsp;&nbsp<textarea name=sub_question@answer_" + id + " id=sub_question@answer_" + id + " rows=5 cols=95 class=textArea></textarea>&nbsp;&nbsp<a href='#' onClick='removeFormField(\"#row" + id + "\"); return false;'>Remove</a><span><br><br>");
		
		$('#row' + id).highlightFade({
			speed:1000
		});
		
		id = (id - 1) + 2;
		document.getElementById("idSub").value = id;
		}
	
		function removeFormField(idSub) {
		//alert (id);
			$(idSub).remove();
		}
	</script>
	
	<style>
		#myformSub label {
		  font-family: Arial, Helvetica, sans-serif;
		  font-size:11px;
		}
		
		#quesListSub {
		  margin: 0px 0px 0px 0px;
		  display: none;
		}
	</style>
	<div id="myformSub">
		<div id="content"><input type="text" name="subName" id="subName" size="30" />&nbsp;&nbsp;<input type="radio" name="showQuestionsSub" onclick="javascript: $('#quesListSub').show('slow');" value="show" /> show&nbsp;&nbsp;<input checked="checked" type="radio" name="showQuestionsSub" value="hide" onclick="javascript: $('#quesListSub').hide('slow');" /> hide</div><br>
		<div id="quesListSub">
			<!--start of the list of questions-->				
			<!--add the OSHA questions for the current customer and any of it's sub-contractors-->
			<table cellpadding="0" cellspacing="0" border="0">	
					<tr>
						<td><img src="images/pix.gif" width="5" height="0"></td>
						<td>
								<%
								i = 1
								Set oCmd = Server.CreateObject("ADODB.Command")
		
								With oCmd
								   .ActiveConnection = DataConn
								   .CommandText = "spGetOSHAQuestion"  'spGetOSHASubQuestion
								   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, rsReport("divisionID"))
								   .CommandType = adCmdStoredProc
								   
								End With
											
								Set rsQuestion = oCmd.Execute
								Set oCmd = nothing
								%>
								
								<%if not rsQuestion.eof then%>
								<br />
								<%end if%>
								<table width="800" cellpadding="0" cellspacing="0" border="0">
									<%blnChange = true
									do until rsQuestion.eof
										If blnChange = true then%>
										<tr class="rowColor">
										<%else%>
										<tr>
										<%end if%>
											<td valign="top">
												<br />&nbsp;<strong><%=i%>.</strong>&nbsp;
											</td>
											<td valign="top">
												<br /><%=rsQuestion("question")%><br />
												<input type="radio" name="sub_question_answer_<%=rsQuestion("questionID")%>" value="1" />&nbsp;<%=rsQuestion("YesAnswer")%><br />
												<input type="radio" name="sub_question_answer_<%=rsQuestion("questionID")%>" value="2" />&nbsp;<%=rsQuestion("NoAnswer")%><br />
												<input type="radio" name="sub_question_answer_<%=rsQuestion("questionID")%>" value="3" checked="checked" />&nbsp;<%=rsQuestion("NAAnswer")%><br />
												<input type="radio" name="sub_question_answer_<%=rsQuestion("questionID")%>" value="4" />&nbsp;<%=rsQuestion("NotInspAnswer")%><br />
												
												<a href=javascript:displayCommentPicker('sub_question_comment_<%=rsQuestion("questionID")%>')><strong>select comment</strong></a><br />
												<input type="text" name="sub_question_comment_<%=rsQuestion("questionID")%>" size="70" /><br />
												<input type="hidden" name="sub_question_question_<%=rsQuestion("questionID")%>" value="<%=rsQuestion("question")%>" />
												<input type="hidden" name="sub_question_ID<%=rsQuestion("questionID")%>" value="<%=rsQuestion("questionID")%>" /><br />
											</td>
										</tr>
								
								<%rsQuestion.movenext
								i=i+1
								if blnChange = true then
									blnChange = false
								else
									blnChange = true
								end if
								loop%>
								
								
							</table>
							
						</td>
					</tr>
					<tr>
						<td></td>
						<td colspan="7">&nbsp;&nbsp;
							<input type="hidden" id="idSub" value="<%=i%>">
							<div id="divTxtSub"></div>
						</td>
					</tr>	
					<tr><td colspan="7" height=5></td></tr>
					<tr>
						<td></td>
						<td colspan="7">&nbsp;&nbsp;
							<!--add a new entry to this-->
							<a href="#" onClick="addFormFieldSub(); return false;">+ add new entry</a>
						</td>
					</tr>
				</table>
			
			
			<!--end of the list of questions-->
		</div>
	</div>	
	<br />
<%'rsSub.movenext
'loop
'next
%>


<input type="checkbox" name="addNewSub" />&nbsp;Add another subcontractor when report is saved<br /><br />

<%'get the users in the system
Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetUserList"
   .CommandType = adCmdStoredProc   
End With
Set rsUsers = oCmd.Execute
Set oCmd = nothing
'add the users to a dropdown for selecting
%>

<script type="text/javascript" src="jquery/jquery.ui.js"></script>
<script type="text/javascript" src="jquery/jquery.asmselect.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$("select[multiple]").asmSelect({
			addItemTarget: 'bottom',
			animate: true,
			highlight: true,
			sortable: true
		});
		
	}); 

</script>
<link rel="stylesheet" type="text/css" href="jquery/jquery.asmselect.css" />
Please select any user(s) that you want this report routed to in order to pre-certify once the report is saved.<br />															
<select id="routeUsers" multiple="multiple" name="routeUsers" title="select all that apply">
	<%do until rsUsers.eof%>
		<option value="<%=rsUsers("userID")%>"><%=rsUsers("firstName")%>&nbsp;<%=rsUsers("lastName")%></option>
	<%rsUsers.movenext
	loop%>
</select>
