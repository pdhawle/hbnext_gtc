<!--#include file="includes/constants.inc"-->
<%
If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

customerID = request("customerID")

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomer"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, customerID)
   .CommandType = adCmdStoredProc
   
End With
	
Set rsCustomer = oCmd.Execute
Set oCmd = nothing

%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<%

	'get the settings of the boxes
	Ffile=request.querystring("fileoptions")
	if Ffile="" then Ffile=request.cookies("fileoptions")
		if Ffile="" then 
			response.cookies("fileoptions")="max" 
		else 
			response.cookies("fileoptions")=Ffile 
		end if
	Ffile=request.cookies("fileoptions")
	
'	Fprojects=request.querystring("projectList")
'	if Fprojects="" then Fprojects=request.cookies("projectList")
'		if Fprojects="" then 
'			response.cookies("projectList")="min" 
'		else 
'			response.cookies("projectList")=Fprojects 
'		end if
'	Fprojects=request.cookies("projectList")
	
	'settings of boxes is now loaded.
%>
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sLoginTitle%></span><span class="Header"> - Customer Document Manager</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<strong>Customer: </strong><%=rsCustomer("customerName")%><br>
									<!--<strong>Folder:</strong> <%'=folder.name%>-->
									<table width="98%" align="left" cellpadding="0" cellspacing="0" class="borderTable">
									  <tr> 
										<td colspan="8">
											
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr bgcolor="#FFFFFF"><td colspan="9"><img src="images/pix.gif" width="1" height="10"></td></tr>
											<tr align="left" valign="top"> 
											  <td width="160">
											  
											  
											  <!--main menu for the document manager-->
											  <% if Ffile="min" then %>
												<table class="borderTable" width="155" border="0" align="center" cellpadding="0" cellspacing="0">
												  <tr bgcolor="#295DCE"> 
													<td width="10" bgcolor="#295DCE"> <div align="center"><font color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif"><strong> 
														</strong></font></div></td>
													<td bgcolor="#295DCE"><font color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif"><strong>Files 
													  / Folders</strong></font></td>
													<td width="33"> <div align="right"><font color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif"></font><a href="docManagerCust.asp?fileoptions=max&action=viewfolder&path=<%=request.querystring("path")%>&customerID=<%=customerID%>"><img src="images/arrows_down.gif" width="21" height="22" border="0"></a></div></td>
												  </tr>
												</table>
												<% else %>
											  
												<table class="borderTable" width="155" border="0" align="center" cellpadding="0" cellspacing="0">
												  <tr bgcolor="#295DCE"> 
													<td width="10" bgcolor="#295DCE"> 
													  <div align="center"><font color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif"><strong> 
														</strong></font></div></td>
													<td bgcolor="#295DCE"><font color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif"><strong>Files 
													  / Folders</strong></font></td>
													<td width="33"> <div align="right"><font color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif"></font><a href="docManagerCust.asp?fileoptions=min&action=viewfolder&path=<%=request.querystring("path")%>&customerID=<%=customerID%>"><img src="images/arrows_up.gif"  border="0"></a></div></td>
												  </tr>
												  <tr bgcolor="#EFEFEF"> 
													<td colspan="3">
													  <br><table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr> 
														  <td colspan="2">
															<%if customerID <> "" then%>
																<a class="fileLink" href="docManagerCust.asp?customerID=<%=customerID%>">&nbsp;Document Manager Home</a>
															<%else%>
																&nbsp;Document Manager Home
															<%end if%>
														</td>
														</tr>
													  </table><br>
													  <table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr> 
														  <td width="24%"><div align="center"><img src="images/expl_newfile.gif" width="26" height="21"></div></td>
														  <td width="76%">
															<%if customerID <> "" then%>
																<a class="fileLink" href="docManagerCust.asp?action=newfile&path=<%=request.querystring("path")%>&customerID=<%=customerID%>">Create file</a>
															<%else%>
																Create file
															<%end if%>
														  </td>
														</tr>
													  </table>
													  <table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr> 
														  <td width="24%"><div align="center"><img src="images/nfolder.gif" width="25" height="16"></div></td>
														  <td width="76%">
															<%if customerID <> "" then%>
																<a class="fileLink" href="docManagerCust.asp?action=CreateNewFolder&path=<%=request.querystring("path")%>&customerID=<%=customerID%>">Create folder</a>
															<%else%>
																Create folder
															<%end if%>							  	
														  </td>
														</tr>
													  </table>
													  <br>
													  <table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr> 
														  <td width="24%"><div align="center"><img src="images/upload.gif" ></div></td>
														  <td width="76%">
															<%if customerID <> "" then%>
																<a class="fileLink" href="docManagerCust.asp?action=UploadFiles&path=<%=request.querystring("path")%>&customerID=<%=customerID%>">Upload file(s)</a>
															<%else%>
																Upload file(s)
															<%end if%>
														  </td>
														</tr>
													  </table>
													  <br>
													  <table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr> 
														  <td width="24%"><!--<div align="center"><img src="images/upload.gif" ></div>--></td>
														  <td width="76%">
																<a class="fileLink" href="customerList.asp">Customer List</a>
														  </td>
														</tr>
													  </table>
													  <br> 
													 <!-- <table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr> 
														  <td colspan="2">&nbsp;<a class="fileLink" href="javascript:window.close();">Close Document Manager</a></td>
														</tr>
													  </table>
													  <br>--></td>
												  </tr>
												</table> 
												<% end if %><br>
												 <!--end main menu for the document manager-->
												
											  </td>
											  <td width="5">&nbsp;</td>
											  <td width="80%" height="350" bgcolor="#FFFFFF"> 
												<!--#include file="filemanagercust.asp"-->
											  </td>
											</tr>
										  </table> </td>
									  </tr>
									</table>
															
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>