<%
customerID = request("customerID")
quoteID = request("quoteID")
clientID = request("clientID")
red = request("red")

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")
'Create command for state list
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetStates"
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsState = oCmd.Execute
Set oCmd = nothing

'Create command for customer
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomer"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, customerID)
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsCustomer = oCmd.Execute
Set oCmd = nothing

%>
<script type="text/javascript">
<!--
var isNN = (navigator.appName.indexOf("Netscape")!=-1);
function autoTab(input,len, e) {
var keyCode = (isNN) ? e.which : e.keyCode; 
var filter = (isNN) ? [0,8,9] : [0,8,9,16,17,18,37,38,39,40,46];
if(input.value.length >= len && !containsElement(filter,keyCode)) {
input.value = input.value.slice(0, len);
input.form[(getIndex(input)+1) % input.form.length].focus();
}
function containsElement(arr, ele) {
var found = false, index = 0;
while(!found && index < arr.length)
if(arr[index] == ele)
found = true;
else
index++;
return found;
}
function getIndex(input) {
var index = -1, i = 0, found = false;
while (i < input.form.length && index == -1)
if (input.form[i] == input)index = i;
else i++;
return index;
}
return true;
}
// -->
</script>
<form name="addCustomer" method="post" action="process.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Add Customer Contact</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table border="0" cellpadding="0" cellspacing="0">
										<tr><td colspan="5"><strong>Customer:</strong>&nbsp;<%=rsCustomer("customerName")%></td></tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>First Name:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="contactName" size="30" />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Last Name:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="contactLastName" size="30"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Title/Contact Type:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="contactTitle">
													<option value=""></option>
													<option value="Billing">Billing</option>
													<option value="Controller">Controller</option>
													<option value="CPA">CPA</option>
													<option value="Director">Director</option>
													<option value="EH&S Director">EH&S Director</option>
													<option value="Estimator">Estimator</option>
													<option value="EVP">EVP</option>
													<option value="Executive Director">Executive Director</option>
													<option value="General Manager">General Manager</option>
													<option value="HR">HR</option>
													<option value="IT Manager">IT Manager</option>
													<option value="Office Manager">Office Manager</option>
													<option value="Other">Other</option>
													<option value="Owner">Owner</option>
													<option value="P.E.">P.E.</option>													
													<option value="President">President</option>
													<option value="Principle Consultant">Principle Consultant</option>
													<option value="Project Engineer">Project Engineer</option>
													<option value="Project Manager">Project Manager</option>													
													<option value="Safety Manager">Safety Manager</option>
													<option value="Sales">Sales</option>
													<option value="Senior Estimator">Senior Estimator</option>
													<option value="Senior Pre-Construction Manager">Senior Pre-Construction Manager</option>
													<option value="Senior Project Manager">Senior Project Manager</option>
													<option value="Site Superintendent">Site Superintendent</option>
													<option value="Vice President">Vice President</option>
													<option value="VP Operations">VP Operations</option>
												</select>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Contact Email:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="contactEmail" size="50"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>				
										<tr>
											<td valign="top" align="right"><strong>Address 1:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="address1" size="30" value="<%=rsCustomer("address1")%>" />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Address 2:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="address2" size="30" value="<%=rsCustomer("address2")%>"  />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>City:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="city" size="30" value="<%=rsCustomer("city")%>"  />
											</td>
										</tr>				
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>State:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="state" tooltipText="Select the customer's state.">
													<%do until rsState.eof
														if rsCustomer("state") = rsState("stateID") then%>
															<option selected="selected" value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
														<%else%>
															<option value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
														<%end if%>
													<%rsState.movenext
													loop%>
												</select>&nbsp;&nbsp;<strong>Zip:</strong>&nbsp;<input type="text" name="zip" size="5" value="<%=rsCustomer("zip")%>"  />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Office Phone:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%
												officePhone = stripPhone(rsCustomer("phone"))
												
												officePhone1 = firstThree(officePhone)
												officePhone2 = secondThree(officePhone)
												officePhone3 = lastFour(officePhone)
												%>
												<input type="text" name="officePhone1" size="3" value="<%=officePhone1%>" maxlength="3" onKeyUp="return autoTab(this, 3, event);"/>&nbsp;
												<input type="text" name="officePhone2" size="3" value="<%=officePhone2%>" maxlength="3" onKeyUp="return autoTab(this, 3, event);"/>&nbsp;
												<input type="text" name="officePhone3" size="3" value="<%=officePhone3%>" maxlength="4" onKeyUp="return autoTab(this, 4, event);"/>&nbsp;
												ext.&nbsp;<input type="text" name="extOffice" size="3" value="<%=rsCustomer("ext")%>"/>
												<!--<input type="text" name="phone" size="30" value="<%'=rsCustomer("phone")%>"  />-->
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Direct Line:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%
											'	phone = stripPhone(rsCustomer("phone"))
											'	
											'	phone1 = firstThree(phone)
											'	phone2 = secondThree(phone)
											'	phone3 = lastFour(phone)
												%>
												<input type="text" name="phone1" size="3" value="<%=phone1%>" maxlength="3" onKeyUp="return autoTab(this, 3, event);"/>&nbsp;
												<input type="text" name="phone2" size="3" value="<%=phone2%>" maxlength="3" onKeyUp="return autoTab(this, 3, event);"/>&nbsp;
												<input type="text" name="phone3" size="3" value="<%=phone3%>" maxlength="4" onKeyUp="return autoTab(this, 4, event);"/>&nbsp;
												ext.&nbsp;<input type="text" name="ext" size="3" value="<%=request("ext")%>"/>
												<!--<input type="text" name="phone" size="30" value="<%'=rsCustomer("phone")%>"  />-->
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Cell:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%
												cellPhone = stripPhone(rsCustomer("cellPhone"))
												
												cellPhone1 = firstThree(cellPhone)
												cellPhone2 = secondThree(cellPhone)
												cellPhone3 = lastFour(cellPhone)
												%>
												<input type="text" name="cellPhone1" size="3" value="<%=cellPhone1%>" maxlength="3" onKeyUp="return autoTab(this, 3, event);"/>&nbsp;
												<input type="text" name="cellPhone2" size="3" value="<%=cellPhone2%>" maxlength="3" onKeyUp="return autoTab(this, 3, event);"/>&nbsp;
												<input type="text" name="cellPhone3" size="3" value="<%=cellPhone3%>" maxlength="4" onKeyUp="return autoTab(this, 4, event);"/>
												<!--<input type="text" name="cell" size="30" value="<%'=rsCustomer("cell")%>"  />-->
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Fax:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%
												fax = stripPhone(rsCustomer("fax"))
												
												fax1 = firstThree(fax)
												fax2 = secondThree(fax)
												fax3 = lastFour(fax)
												%>
												<input type="text" name="fax1" size="3" value="<%=fax1%>" maxlength="3" onKeyUp="return autoTab(this, 3, event);"/>&nbsp;
												<input type="text" name="fax2" size="3" value="<%=fax2%>" maxlength="3" onKeyUp="return autoTab(this, 3, event);"/>&nbsp;
												<input type="text" name="fax3" size="3" value="<%=fax3%>" maxlength="4" onKeyUp="return autoTab(this, 4, event);"/>
												<!--<input type="text" name="fax" size="30" value="<%'=rsCustomer("fax")%>"  />-->
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>HB/S Primary Contact:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="checkbox" name="isPrimary" />&nbsp;If selected, the current HB/S primary contact will be de-selected. 
											</td>
										</tr>	
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>HBTC Primary Contact:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="checkbox" name="HBTCContact" />&nbsp;If selected, the current HBTC primary contact will be de-selected.
											</td>
										</tr>		
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td colspan="3" align="right">
												<input type="hidden" name="customerID" value="<%=customerID%>" />
												<input type="hidden" name="red" value="<%=red%>" />
												<input type="hidden" name="clientID" value="<%=clientID%>" />
												<input type="hidden" name="quoteID" value="<%=quoteID%>" />
												<input type="hidden" name="processType" value="addCustomerContact" />
												<input type="submit" value="  Save  " class="formButton"/>
											</td>
										</tr>
									</table>
								
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<script language="JavaScript" type="text/javascript">

  var frmvalidator  = new Validator("addCustomer");
  frmvalidator.addValidation("contactName","req","Please enter the contact's first name");
  frmvalidator.addValidation("contactLastName","req","Please enter the contact's last name");
  //frmvalidator.addValidation("contactEmail","req","Please enter the cantact's email address");
 // frmvalidator.addValidation("contactEmail","email");
 // frmvalidator.addValidation("address1","req","Please enter the customer's address");
  //frmvalidator.addValidation("city","req","Please enter the customer's city");
 // frmvalidator.addValidation("zip","req","Please enter the customer's zip code");
  //frmvalidator.addValidation("phone","req","Please enter the customer's phone number");
</script>

<%
rsState.close
set rsState = nothing
DataConn.close
set DataConn = nothing
%>