<%
On Error Resume Next

primaryID = request("primaryID")
projectID = request("projectID")

if primaryID = "" then
	primaryID = 0
end if

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetAssignedProjects"
   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, session("ID"))
   .CommandType = adCmdStoredProc   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsProjects = oCmd.Execute
Set oCmd = nothing

If projectID <> "" then

	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetCoverage"
	   .CommandType = adCmdStoredProc
	   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
				
	Set rs = oCmd.Execute
	Set oCmd = nothing
	
	'Create command for state list
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetStates"
	   .CommandType = adCmdStoredProc
	   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
		
	Set rsState = oCmd.Execute
	Set oCmd = nothing
	
	'Create command for county list
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetCounties"
	   .CommandType = adCmdStoredProc
	   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
		
	Set rsCounty = oCmd.Execute
	Set oCmd = nothing
	
	'Create command for NTU Value list
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetNTUValue"
	   .CommandType = adCmdStoredProc
	   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
		
	Set rsNTUValue = oCmd.Execute
	Set oCmd = nothing
	
	
	
	If primaryID <> 0 then
		'Create command for primary data
		Set oCmd = Server.CreateObject("ADODB.Command")
		
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetPrimaryNOI"
		   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, primaryID)
		   .CommandType = adCmdStoredProc
		   
		End With
			
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
					
		Set rsPrimary = oCmd.Execute
		Set oCmd = nothing
	End If
	
end if
%>


<script language="javascript" type="text/javascript">
<!--
function KeepCount() {

	var NewCount = 0
						
	if (document.noiSecondary.typeConstructionCommercial.checked)
	{NewCount = NewCount + 1}
	
	if (document.noiSecondary.typeConstructionIndustrial.checked)
	{NewCount = NewCount + 1}
	
	if (document.noiSecondary.typeConstructionMunicipal.checked)
	{NewCount = NewCount + 1}
	
	if (document.noiSecondary.typeConstructionDOT.checked)
	{NewCount = NewCount + 1}
	
	if (document.noiSecondary.typeConstructionUtility.checked)
	{NewCount = NewCount + 1}
	
	if (document.noiSecondary.typeConstructionResidential.checked)
	{NewCount = NewCount + 1}
	
	if (NewCount == 2)
	{
	alert('Pick Just One Please')
	document.noiSecondary; return false;
	}
} 

function KeepCount2() {

	var NewCount = 0
						
	if (document.noiSecondary.IRWTrout.checked)
	{NewCount = NewCount + 1}
	
	if (document.noiSecondary.IRWWarmWater.checked)
	{NewCount = NewCount + 1}
	
	if (NewCount == 2)
	{
	alert('Pick Just One Please')
	document.noiSecondary; return false;
	}
} 

function KeepCount3() {

	var NewCount = 0
						
	if (document.noiSecondary.MSSSTrout.checked)
	{NewCount = NewCount + 1}
	
	if (document.noiSecondary.MSSSWarmWater.checked)
	{NewCount = NewCount + 1}
	
	if (NewCount == 2)
	{
	alert('Pick Just One Please')
	document.noiSecondary; return false;
	}
} 

function dept_onchange(noiSecondary) {
   document.noiSecondary.action = "form.asp?formType=secondary";
   noiSecondary.submit(); 
}
//-->
</script>

<form name="noiSecondary" method="post" action="processNOI.asp" onSubmit="return ValidateDate()">

<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sNOITitle%></span><span class="Header"> - Notice of Intent</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="800" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td colspan="9">
												<!--get a dropdown of all of the assigned projects for the user-->
													&nbsp;&nbsp;&nbsp;&nbsp;Please select a project to create a secondary NOI.<br>
													&nbsp;&nbsp;&nbsp;&nbsp;<select name="projectID" onChange="return dept_onchange(noiSecondary)">
														<option value="">--Select Project--</option>
														<%do until rsProjects.eof
														if trim(rsProjects("projectID")) = trim(ProjectID) then%>
															<option selected="selected" value="<%=rsProjects("projectID")%>"><%=rsProjects("customerName")%> - <%=rsProjects("projectName")%></option>
														<%else%>
															<option value="<%=rsProjects("projectID")%>"><%=rsProjects("customerName")%> - <%=rsProjects("projectName")%></option>
														<%end if
														rsProjects.movenext
														loop
														rsProjects.close
														set rsProjects = nothing%>
													</select>
											</td>
										</tr>
										<%If projectID <> "" then%>
												
											<tr>
												<td><img src="images/pix.gif" width="10" height="1"></td>
												<td><br />
													<a href="instructions_secondary.asp" target="_blank">Instructions</a><br><br />
													<b>State of Georgia<br />
													Department of Natural Resources<br />
													Environmental Protection Division<br /><br />

													For Coverage Under the 2008 Re-Issuance of the<br />
													NPDES General Permits No. GAR100003 To Discharge Storm Water<br />
													Associated With Construction Activity for Common Developments</b><br /><br />

													<strong>SECONDARY PERMITTEE</strong>
												</td>
												<td></td>
											</tr>
											<tr><td colspan="3"><img src="images/pix.gif" width="1" height="20"></td></tr>
											<tr>
												<td><img src="images/pix.gif" width="10" height="1"></td>
												<td>
													<table border="0" cellpadding="0" cellspacing="0">
														<tr>
															<td colspan="3"><strong>Notice Of Intent:</strong><br />
																<input type="radio" name="noticeOfIntent" value="1" checked="checked" />&nbsp;Initial Notification (New Facility/Construction Site)<br />
																<input type="radio" name="noticeOfIntent" value="2" />&nbsp;Re-Issuance Notification (Existing Facility/Construction Site)<br />
																<input type="radio" name="noticeOfIntent" value="3" />&nbsp;Change of Information (Applicable only if the NOI was submitted after August 1, 2008)
															</td>
														</tr>	
														
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
														<tr>
															<td colspan="3"><strong>I. SITE/SECONDARY PERMITTEE INFORMATION</strong></td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
														<tr>
															<td><strong>Project Construction Site Name:</strong><br /><img src="images/pix.gif" width="180" height="1"></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<input type="text" name="siteProjectName" size="30" value="<%=rsPrimary("siteProjectName")%>">
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td><strong>Construction Site Location:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<input type="text" name="siteLocation" size="30" value="<%=rsPrimary("projectAddress")%>">
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td><strong>Subdivision Name:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<%If primaryID <> 0 then%>
																<%=rsPrimary("subdivision")%><input type="hidden" name="subdivision" size="30" value="<%=rsPrimary("subdivision")%>">
																<%else%>
																<input type="text" name="subdivision" size="30" value="<%=rsPrimary("subdivision")%>">
																<%end if%>
																&nbsp;&nbsp;<strong>Lot Number(s):</strong>&nbsp;&nbsp;<input type="text" name="lotNumber" size="30">
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td><strong>Common Development Name:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<input type="text" name="commonDevelopmentName" size="30">
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td><strong>Construction Site Street Address:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<input type="text" name="constructionSiteStreetAddress" size="30">
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td><strong>City(if applicable):</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<%If primaryID <> 0 then%>
																<%=rsPrimary("projectCity")%><input type="hidden" name="city" size="30" value="<%=rsPrimary("projectCity")%>">&nbsp;&nbsp;<strong>County:</strong>
																<%=rsPrimary("projectCounty")%><input type="hidden" name="county" size="30" value="<%=rsPrimary("projectCounty")%>">&nbsp;&nbsp;
																<%else%>
																<input type="text" name="city" size="30" value="<%=rsPrimary("projectCity")%>">&nbsp;&nbsp;<strong>County:</strong>
																<select name="county">
																	<%do until rsCounty.eof%>
																		<%if primaryID <> 0 then
																			if rsCounty("county") = rsPrimary("projectCounty") then%>
																				<option selected="selected" value="<%=rsCounty("county")%>"><%=rsCounty("county")%></option>
																			<%else%>
																				<option value="<%=rsCounty("county")%>"><%=rsCounty("county")%></option>
																			<%end if
																		else%>
																			<option value="<%=rsCounty("county")%>"><%=rsCounty("county")%></option>
																		<%end if%>
																	<%rsCounty.movenext
																	loop
																	rsCounty.moveFirst%>
																</select>&nbsp;&nbsp;
																<%end if%>						
															</td>
														</tr>
														
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td><strong>Secondary Permittee Name:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<input type="text" name="secondaryPermitteeName" size="30" value="<%=rsPrimary("ownerName")%>">&nbsp;&nbsp;<strong>Phone:</strong>&nbsp;&nbsp;<input type="text" name="permitteePhone" size="30">
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td><strong>Address:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<input type="text" name="permitteeAddress" size="30" value="<%=rsPrimary("ownerAddress")%>">
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td><strong>City:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<input type="text" name="permitteeCity" size="20" value="<%=rsPrimary("ownerCity")%>">&nbsp;&nbsp;<strong>State:</strong>&nbsp;&nbsp;
																<select name="permitteeState">
																	<%do until rsState.eof
																		if rsPrimary("ownerState") = rsState("stateID") then%>
																			<option selected="selected" value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
																	<%else%>
																			<option value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
																	<%end if
																	rsState.movenext
																	loop
																	rsState.moveFirst%>
																</select>&nbsp;&nbsp;
																<strong>Zip:</strong>&nbsp;&nbsp;<input type="text" name="permitteeZip" size="10" value="<%=rsPrimary("ownerZip")%>">
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td><strong>Duly Authorized Representative:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<input type="text" name="authorizedRep" size="30" value="<%=rsPrimary("authorizedRep")%>">&nbsp;&nbsp;<strong>Phone:</strong>&nbsp;&nbsp;<input type="text" name="authorizedRepPhone" size="30" value="<%=rsPrimary("authorizedRepPhone")%>">
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td><strong>Facility/Construction Site Contact:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<input type="text" name="facilityContact" size="30" value="<%=rsPrimary("facilityContact")%>">&nbsp;&nbsp;<strong>Phone:</strong>&nbsp;&nbsp;<input type="text" name="facilityContactPhone" size="30" value="<%=rsPrimary("facilityContactPhone")%>">
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td><strong>Primary Permittee Name:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<input type="text" name="primaryPermitteeName" size="30" value="<%=rsPrimary("ownerName")%>">&nbsp;&nbsp;<strong>Phone:</strong>&nbsp;&nbsp;<input type="text" name="primaryPermitteePhone" size="30">
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td><strong>Address:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<input type="text" name="primaryPermitteeAddress" size="30" value="<%=rsPrimary("ownerAddress")%>">
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td><strong>City:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<input type="text" name="primaryPermitteeCity" size="20" value="<%=rsPrimary("ownerCity")%>">&nbsp;&nbsp;<strong>State:</strong>&nbsp;&nbsp;
																<select name="primaryPermitteeState">
																	<%do until rsState.eof
																		if rsPrimary("ownerState") = rsState("stateID") then%>
																			<option selected="selected" value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
																	<%else%>
																			<option value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
																	<%end if
																	rsState.movenext
																	loop%>
																</select>&nbsp;&nbsp;
																<strong>Zip:</strong>&nbsp;&nbsp;<input type="text" name="primaryPermitteeZip" size="10" value="<%=rsPrimary("ownerZip")%>">
															</td>
														</tr>
														
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
														<tr>
															<td colspan="3"><strong>II. CONSTRUCTION SITE ACTIVITY INFORMATION</strong></td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
														<tr>
															<td><strong>Start Date:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td valign="top">
																<input type="text" name="startDate" size="10" value="<%=rsPrimary("startDate")%>">&nbsp;<a href="javascript:displayDatePicker('startDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>&nbsp;&nbsp;&nbsp;&nbsp;<strong>Completion Date:</strong>&nbsp;&nbsp;<input type="text" name="completionDate" size="10" value="<%=rsPrimary("completionDate")%>">&nbsp;<a href="javascript:displayDatePicker('completionDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td><strong>Estimated Disturbed Acreage:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td valign="top">
																<input type="text" name="estimatedDisturbedAcerage" size="5">
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td colspan="3">
																<strong>Will the Secondary Permittee disturb more than 50 acres at any one time?</strong><br />
																<input type="radio" name="disturb50" value="1" checked="checked" />&nbsp;YES<br />
																<input type="radio" name="disturb50" value="2" />&nbsp;NO<br />
																<input type="radio" name="disturb50" value="3" />&nbsp;N/A � if the Plan was submitted prior to the effective date of the General NPDES Permit No. GAR100003 for Common Development construction activities.
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
														<tr>
															<td valign="top"><strong>Construction Activity Type:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<input type="checkbox" name="typeConstructionCommercial" <%=isChecked(rsPrimary("typeConstructionCommercial"))%> onClick="return KeepCount()" />&nbsp;Commercial&nbsp;&nbsp;
																<input type="checkbox" name="typeConstructionIndustrial" <%=isChecked(rsPrimary("typeConstructionIndustrial"))%> onClick="return KeepCount()" />&nbsp;Industrial&nbsp;&nbsp;
																<input type="checkbox" name="typeConstructionMunicipal" <%=isChecked(rsPrimary("typeConstructionMunicipal"))%> onClick="return KeepCount()" />&nbsp;Municipal&nbsp;&nbsp;
																<input type="checkbox" name="typeConstructionDOT" <%=isChecked(rsPrimary("typeConstructionLinear"))%> onClick="return KeepCount()" />&nbsp;DOT<br />
																<input type="checkbox" name="typeConstructionUtility" <%=isChecked(rsPrimary("typeConstructionUtility"))%> onClick="return KeepCount()" />&nbsp;Utility&nbsp;&nbsp;
																<input type="checkbox" name="typeConstructionResidential" <%=isChecked(rsPrimary("typeConstructionResidential"))%> onClick="return KeepCount()" />&nbsp;Residential
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
														<tr>
															<td colspan="3"><strong>III. RECEIVING WATER INFORMATION</strong></td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
														<tr>
															<td><strong>A. Initial Receiving Water(s):</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<input type="text" name="IRWName" size="30" value="<%=rsPrimary("IRWName")%>">
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td valign="top"></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<input type="checkbox" name="IRWTrout" <%=isChecked(rsPrimary("IRWTrout"))%> onClick="return KeepCount2()" />&nbsp;Trout Stream&nbsp;&nbsp;<input type="checkbox" name="IRWWarmWater" <%=isChecked(rsPrimary("IRWWarmWater"))%> onClick="return KeepCount2()" />&nbsp;Warm Water Fisheries Stream
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td><strong>B. Name of MS4 Owner/Operator :</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<input type="text" name="MSSSOwnerOperator" size="30" value="<%=rsPrimary("MSSSOwnerOperator")%>">
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td><strong>Name of Receiving Water(s):</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<input type="text" name="RWName" size="30">
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td valign="top"></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<input type="checkbox" name="MSSSTrout" <%=isChecked(rsPrimary("RWTrout"))%> onClick="return KeepCount3()"/>&nbsp;Trout Stream&nbsp;&nbsp;<input type="checkbox" name="MSSSWarmWater" <%=isChecked(rsPrimary("RWWarmWater"))%> onClick="return KeepCount3()" />&nbsp;Warm Water Fisheries Stream
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
														<tr>
															<td colspan="3">
																<strong>C. Does the facility/construction site discharge storm water into an Impaired Stream Segment, or within one (1) 
																linear mile upstream of and within the same watershed as, any portion of an Impaired Stream Segment identified as �not 
																supporting� its designated use(s), as shown on Georgia�s 2008 and subsequent �305(b)/303(d) List Documents (Final)� listed 
																for the criteria violated, �Bio F� (Impaired Fish Community) and/or �Bio M� (Impaired Macroinvertebrate Community), within 
																Category 4a, 4b or 5, and the potential cause is either �NP� (nonpoint source) or �UR� (urban runoff)?</strong><br />
																<input type="radio" name="siteDischargeStormWater" value="1" checked="checked" />&nbsp;YES, Name of Impaired Stream Segment(s):&nbsp;&nbsp;<input type="text" name="impairedStream" /><br />
																<input type="radio" name="siteDischargeStormWater" value="2" />&nbsp;NO<br />
																<input type="radio" name="siteDischargeStormWater" value="3" />&nbsp;N/A � if the NOI was submitted within 90 days after the effective date of the General NPDES Permit No. GAR100003 for Common Development construction activities.<br />
																<input type="radio" name="siteDischargeStormWater" value="4" />&nbsp;N/A � if the secondary permittees are utility companies and utility contractors and implementing the applicable best management practices detailed in the primary permittee�s Plan.
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="15"></td></tr>
														<tr>
															<td colspan="3" valign="top">
																<strong>D. Does the facility/construction site discharge storm water into an Impaired Stream Segment where a Total Maximum 
																Daily Load (TMDL) Implementation Plan for �sediment� was finalized at least six (6) months prior to the submittal of the 
																NOI?</strong>
															</td>
														</tr>
														<tr>
															<td valign="top" colspan="3">
																<input type="radio" name="siteDischargeStormWater2" value="1" checked="checked" />&nbsp;YES, Name of Impaired Stream Segment(s):&nbsp;&nbsp;<input type="text" name="impairedStream2" /><br />
																<input type="radio" name="siteDischargeStormWater2" value="2" />&nbsp;NO<br />
																<input type="radio" name="siteDischargeStormWater2" value="3" />&nbsp;N/A � if the NOI was submitted within 90 days after the effective date of the General NPDES Permit No. GAR100003 for Common Development construction activities.<br />
																<input type="radio" name="siteDischargeStormWater2" value="4" />&nbsp;N/A � if the secondary permittees are utility companies and utility contractors, and implementing the applicable best management practices detailed in the primary permittee�s Plan.
															</td>
														</tr>
														
														
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
														<tr>
															<td colspan="3"><strong>IV. ATTACHMENTS. (Applicable Only to New Facilities/Construction Sites)</strong></td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
														<tr>
															<td colspan="3">Indicate if the item listed below is attached to this Notice of Intent:</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
														<tr>
															<td colspan="3">
																<input type="checkbox" name="writtenAuth" />&nbsp;Written authorization from the appropriate EPD District Office if the Secondary Permittee plans to disturb more than 50 acres at any one time.
															</td>
														</tr>
		
														
														
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
														<tr>
															<td colspan="3"><strong>IV. CERTIFICATIONS. (Owner or Operator or both to initial as applicable.)</strong></td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
														<tr>
															<td colspan="3">
																<input type="text" name="certify1" size="5">&nbsp;I certify that I will adhere to the Primary Permittee�s Erosion, Sedimentation and Pollutant Control Plan
																(Plan) or the portion of the Plan applicable to my construction activities.
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
														<tr>
															<td colspan="3">
																<input type="text" name="certify2" size="5">&nbsp;I certify under penalty of law that this document and all attachments were prepared under my direction or
																supervision in accordance with a system designed to assure that qualified personnel properly gather and
																evaluate the information submitted. Based upon my inquiry of the person or persons who manage the
																system, or those persons directly responsible for gathering the information, the information submitted is,
																to the best of my knowledge and belief, true, accurate, and complete. I am aware that there are
																significant penalties for submitting false information, including the possibility of fine and imprisonment for
																knowing violations.
															</td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													</table>
													<table>
														<tr>
															<td valign="top"><strong>Secondary Permittee�s Printed Name:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td><input type="text" name="printedName" size="30" value="<%=rsPrimary("ownerName")%>">&nbsp;&nbsp;&nbsp;&nbsp;<strong>Title:</strong>&nbsp;&nbsp;<input type="text" name="title" size="30"></td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td valign="top" align="right"><strong>Date:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td colspan="3"><input type="text" name="signDate" size="10">&nbsp;<a href="javascript:displayDatePicker('signDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a></td>
														</tr>
														<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
														<tr>
															<td colspan="3" align="right">
																<input type="hidden" name="primaryID" value="<%=primaryID%>" />
																<input type="hidden" name="processType" value="addSecondary" />
																<input type="submit" value="Submit Data" class="formButton"/>
															</td>
														</tr>
													</table>
												</td>
												<td><img src="images/pix.gif" width="10" height="1"></td>
											</tr>
										<%end if%>
										
									</table>
								
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<%If projectID <> "" then%>

	<script language="JavaScript" type="text/javascript">
	
	  var frmvalidator  = new Validator("noiSecondary");
	//  frmvalidator.addValidation("siteProjectName","req","Please enter the site project name");
	//  frmvalidator.addValidation("siteLocation","req","Please enter the site location/street address");
	//  frmvalidator.addValidation("city","req","Please enter the city where the project is located");
	//  frmvalidator.addValidation("county","req","Please enter the county where the project is located");
	//  frmvalidator.addValidation("subdivision","req","Please enter the name of the subdivision");
	//  frmvalidator.addValidation("lotNumber","req","Please enter the lot number");
	//  frmvalidator.addValidation("secondaryPermitteeName","req","Please enter the secondary permittee name");
	//  frmvalidator.addValidation("permitteePhone","req","Please enter the secondary permittee's phone number");
	//  frmvalidator.addValidation("permitteeAddress","req","Please enter the secondary permittee's address");
	//  frmvalidator.addValidation("permitteeCity","req","Please enter the secondary permittee's city");
	//  frmvalidator.addValidation("permitteeState","req","Please enter the secondary permittee's state");
//	  frmvalidator.addValidation("permitteeZip","req","Please enter the secondary permittee's zip code");
	//  frmvalidator.addValidation("primaryPermitteeName","req","Please enter the primary permittee name");
	//  frmvalidator.addValidation("primaryPermitteePhone","req","Please enter the primary permittee's phone number");
//	  frmvalidator.addValidation("primaryPermitteeAddress","req","Please enter the primary permittee's address");
//	  frmvalidator.addValidation("primaryPermitteeCity","req","Please enter the primary permittee's city");
	//  frmvalidator.addValidation("primaryPermitteeState","req","Please enter the primary permittee's state");
	 // frmvalidator.addValidation("primaryPermitteeZip","req","Please enter the primary permittee's zip code")  
//	  frmvalidator.addValidation("facilityContact","req","Please enter the facility contact name")
//	  frmvalidator.addValidation("facilityContactPhone","req","Please enter the facility contact phone number") 
//	  frmvalidator.addValidation("startDate","req","Please enter the start date")
//	  frmvalidator.addValidation("completionDate","req","Please enter the completion date")
//	  frmvalidator.addValidation("estimatedDisturbedAcerage","req","Please enter the estimated disturbed acerage")
	  //frmvalidator.addValidation("estimatedDisturbedAcerage","numeric")  
//	  frmvalidator.addValidation("IRWName","req","Please enter the name of Initial Receiving Water(s)")
//	  frmvalidator.addValidation("MSSSOwnerOperator","req","Please enter the Municipal Storm Sewer System Owner/Operator")
//	  frmvalidator.addValidation("certify1","req","Please enter initials")
//	  frmvalidator.addValidation("certify2","req","Please enter initials")
//	  frmvalidator.addValidation("printedName","req","Please enter the secondary permittee's printed name");
//	  frmvalidator.addValidation("title","req","Please enter the secondary permittee's title");
//	  frmvalidator.addValidation("signDate","req","Please enter the date the secondary permittee signs the form");
	
	</script>

<%end if
rs.close
rsState.close
set rs = nothing
set rsState = nothing
DataConn.close
set DataConn = nothing
%>