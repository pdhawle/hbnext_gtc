<table>
	<tr><td height="20"></td></tr>
	<tr>
		<td align="center">
			<%
			'get the client information
			Set oCmd = Server.CreateObject("ADODB.Command")		
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spGetClient"
			   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
			   .CommandType = adCmdStoredProc
				   
			End With
					
			Set rsClient = oCmd.Execute
			Set oCmd = nothing
			
			if projectID <> "" then
				if projectID <> "0" then
					Set oCmd = Server.CreateObject("ADODB.Command")		
					With oCmd
					   .ActiveConnection = DataConn
					   .CommandText = "spGetDivisionByProject"
					   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, projectID)
					   .CommandType = adCmdStoredProc
						   
					End With
							
					Set rsDivision = oCmd.Execute
					Set oCmd = nothing
					
				end if
			end if
			
			%>
			<strong><%=ucase(rsClient("clientName"))%></strong><br />
			<strong>VEHICULAR ACCIDENT REPORT</strong>
		</td>
	</tr>
	<tr><td height="20"></td></tr>	
	<tr>
		<td>
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td colspan="5">
						<strong>COMPANY VEHICLE IN ACCIDENT:</strong>
					</td>
				</tr>
				<tr>
					<td align="right">
						Year and Make:&nbsp;
					</td>
					<td>
						<input type="text" name="yearAndMake" size="20" value="<%=request("yearAndMake")%>" onBlur="document.getElementById('vehicle1YearMakeModel').value=this.value"><//>
					</td>
					<td width="20"></td>
					<td align="right">
						Model:&nbsp;
					</td>
					<td>
						<input type="text" name="model" size="10" value="<%=request("model")%>"/>
					</td>
				</tr>
				<tr>
					<td align="right">
						Driver's Name:&nbsp;
					</td>
					<td>
						<input type="text" name="driversName" size="20" value="<%=request("driversName")%>" onBlur="document.getElementById('vehicle1Driver').value=this.value"/>
					</td>
					<td></td>
					<td align="right">
						Unit Number:&nbsp;
					</td>
					<td>
						<input type="text" name="unitNumber" size="10" value="<%=request("unitNumber")%>"/>
					</td>
				</tr>
				<tr>
					<td align="right">
						Division:&nbsp;
					</td>
					<td>
						<%if request("division") <> "" then
							division = request("division")
						else
							division = rsDivision("division")
						end if
						%>
						<input type="text" name="division" size="20" value="<%=division%>"/>
					</td>
					<td></td>
					<td align="right">
						Foreman:&nbsp;
					</td>
					<td>
						<input type="text" name="foreman" size="10" value="<%=request("foreman")%>"/>
					</td>
				</tr>
			</table><br />
		
			Hours you were on duty before accident:&nbsp;<input type="text" name="hoursOnDuty" size="3" value="<%=request("hoursOnDuty")%>"/>&nbsp;&nbsp;Years in employ of this Company?&nbsp;<input type="text" name="yearsEmployed" size="3" value="<%=request("yearsEmployed")%>"/><br />
			How many previous automobile accidents have you had while employed?</strong>&nbsp;<input type="text" name="previousAccidentsEmploy" size="3" value="<%=request("previousAccidentsEmploy")%>"/><br />
			How many previous automobile accidents this year?&nbsp;<input type="text" name="previousAccidentsYear" size="3" value="<%=request("previousAccidentsYear")%>"/><br />
			<br />Describe in detail what you were doing immediately prior to the <br />accident that required your use of the company vehicle?<br />
			<textarea name="doingPriorToAccident" rows="5" cols="40"><%=request("doingPriorToAccident")%></textarea><br /><br />
			
			
			<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td align="right">Date of Accident:&nbsp;</td>
					<td>
						<%if request("accidentDate") <> "" then
							accidentDate = request("accidentDate")
						else
							accidentDate = formatdatetime(date(),2)
						end if
						%>
						<input type="text" name="accidentDate" maxlength="10" size="10" value="<%=accidentDate%>"/>&nbsp;<a href="javascript:displayDatePicker('accidentDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
					</td>
					<td width="5"></td>
					<td align="right">Day of Week:&nbsp;</td>
					<td><%
						if trim(request("dayOfWeek")) = "" then
							sDay = getDay(weekDay(Date))
						else
							sDay = trim(request("dayOfWeek"))
						end if
						%>
						<select name="dayOfWeek">
							<%if sDay = "Sunday" Then%>
								<option selected="selected">Sunday</option>
							<%else%>
								<option>Sunday</option>
							<%end if%>
							<%if sDay = "Monday" Then%>
								<option selected="selected">Monday</option>
							<%else%>
								<option>Monday</option>
							<%end if%>
							<%if sDay = "Tuesday" Then%>
								<option selected="selected">Tuesday</option>
							<%else%>
								<option>Tuesday</option>
							<%end if%>
							<%if sDay = "Wednesday" Then%>
								<option selected="selected">Wednesday</option>
							<%else%>
								<option>Wednesday</option>
							<%end if%>
							<%if sDay = "Thursday" Then%>
								<option selected="selected">Thursday</option>
							<%else%>
								<option>Thursday</option>
							<%end if%>
							<%if sDay = "Friday" Then%>
								<option selected="selected">Friday</option>
							<%else%>
								<option>Friday</option>
							<%end if%>
							<%if sDay = "Saturday" Then%>
								<option selected="selected">Saturday</option>
							<%else%>
								<option>Saturday</option>
							<%end if%>
						</select>
					</td>
					<td width="5"></td>
					<td align="right">Time:&nbsp;</td>
					<td>
						<%if request("timeofAccident") <> "" then
							timeofAccident = request("timeofAccident")
						else
							timeofAccident = "8:00 am"
						end if
						%>
						<input id="timeofAccident" name="timeofAccident" type="text" value="<%=timeofAccident%>" size=8 maxlength=8 ONBLUR="validateDatePicker(this)">&nbsp;<IMG SRC="images/timepicker.gif" BORDER="0" ALT="Pick a Time!" ONCLICK="selectTime(this,timeofAccident)" STYLE="cursor:hand">
					</td>
				</tr>
			</table>
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td align="right">State of Accident:&nbsp;</td>
					<td>
						<select name="state" onChange="return state_onchange(addAccidentReport)">
							<option value="">--select state--</option>
							<%do until rsState.eof
								if sState = rsState("stateID") then%>
									<option selected="selected" value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
								<%else%>
									<option value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
							<%end if
							rsState.movenext
							loop%>
						</select>	
					</td>
					<td></td>
					<td align="right">County:&nbsp;</td>
					<td colspan="3">
						<%if sState = "" then%>
							Please select a state above.
						<%else%>
							<select name="county">
								<%do until rsCounty.eof%>
									<option value="<%=rsCounty("countyID")%>"><%=rsCounty("county")%></option>
								<%rsCounty.movenext
								loop%>
							</select>
						<%end if%>
					</td>
				</tr>
				<tr>
					<td colspan="8">
						<br /><strong>ROAD WHERE ACCIDENT OCCURRED:</strong><br />
						
						<table cellpadding="0" cellspacing="0">
							<tr>
								<td align="right">Give name of street or highway number:&nbsp;</td>
								<td><input type="text" name="streetNumber" size="30" value="<%=request("streetNumber")%>"/></td>
							</tr>
							<tr>
								<td align="right">Give name of any pertinent intersecting streets:&nbsp;</td>
								<td><input type="text" name="intersectingStreets" size="30" value="<%=request("intersectingStreets")%>"/></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="8">
						Was the incident reported to the police?&nbsp;<input type="radio" name="reportedToPolice" value="Yes"/>&nbsp;Yes&nbsp;&nbsp;<input type="radio" name="reportedToPolice" value="No" />&nbsp;No&nbsp;&nbsp;<input type="radio" name="reportedToPolice" value="NA" checked="checked" />&nbsp;NA
					</td>
				</tr>
			</table><br /><br />
			
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td colspan="5"><strong>VEHICLE NO. 1</strong></td>
				</tr>
				<tr>
					<td align="right">
						Owner:&nbsp;
					</td>
					<td>
						<input type="text" name="vehicle1Owner" size="30" value="<%=rsClient("clientName")%>"/>
					</td>
				</tr>
				<tr>
					<td align="right">
						Driver:&nbsp;
					</td>
					<td>
						<input type="text" name="vehicle1Driver" size="30" value="<%=request("vehicle1Driver")%>"/>
						<!--<select name="userID">
							<%'do until rsUsers.eof
								'if trim(userID) = trim(rsUsers("userID")) then%>
									<option selected="selected" value="<%'=rsUsers("userID")%>"><%'=rsUsers("firstName")%>&nbsp;<%'=rsUsers("lastName")%></option>
								<%'else%>
									<option value="<%'=rsUsers("userID")%>"><%'=rsUsers("firstName")%>&nbsp;<%'=rsUsers("lastName")%></option>
							<%'end if
							'rsUsers.movenext
							'loop%>
						</select>-->
					</td>
				</tr>
				<%if clientID = 3 then 'cw matthews
					sAddress = "1600 Kenview Drive, Marietta, GA 30060"
				end if
				%>			
				
				<tr>
					<td align="right">
						Address:&nbsp;
					</td>
					<td>
						<input type="text" name="vehicle1Address" size="30" value="<%=sAddress%>"/>
					</td>
				</tr>
				<tr>
					<td align="right">
						Age:&nbsp;
					</td>
					<td>
						<input type="text" name="vehicle1Age" size="5" value="<%=request("vehicle1Age")%>"/>&nbsp;&nbsp;Driver�s License Number:&nbsp;<input type="text" name="vehicle1DLNo" size="10" value="<%=request("vehicle1DLNo")%>"/>
					</td>
				</tr>
				<tr>
					<td align="right">
						Year / Make / Model:&nbsp;
					</td>
					<td>
						<input type="text" name="vehicle1YearMakeModel" size="30" value="<%=request("vehicle1YearMakeModel")%>"/>
					</td>
				</tr>
				<tr>
					<td align="right">
						Vehicle Damage:&nbsp;
					</td>
					<td>
						<input type="text" name="vehicle1Damage" size="30" value="<%=request("vehicle1Damage")%>"/>
					</td>
				</tr>
				<tr>
					<td align="right">
						Is Vehicle Drivable?:&nbsp;
					</td>
					<td>
						<input type="text" name="vehicle1Drivable" size="5" value="<%=request("vehicle1Drivable")%>"/>&nbsp;&nbsp;Approximate Repair Costs:&nbsp;<input type="text" name="vehicle1RepairCost" size="10" value="<%=request("vehicle1RepairCost")%>"/>
					</td>
				</tr>
			</table><br /><br />
			
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td colspan="5"><strong>VEHICLE NO. 2</strong></td>
				</tr>
				<tr>
					<td align="right">
						Owner:&nbsp;
					</td>
					<td>
						<input type="text" name="vehicle2Owner" size="30" value="<%=request("vehicle2Owner")%>"/>
					</td>
				</tr>
				<tr>
					<td align="right">
						Driver:&nbsp;
					</td>
					<td>
						<input type="text" name="vehicle2Driver" size="30" value="<%=request("vehicle2Driver")%>"/>
					</td>
				</tr>
				<tr>
					<td align="right">
						Address:&nbsp;
					</td>
					<td>
						<input type="text" name="vehicle2Address" size="30" value="<%=request("vehicle2Address")%>"/>
					</td>
				</tr>
				<tr>
					<td align="right">
						Age:&nbsp;
					</td>
					<td>
						<input type="text" name="vehicle2Age" size="5" value="<%=request("vehicle2Age")%>"/>&nbsp;&nbsp;Driver�s License Number:&nbsp;<input type="text" name="vehicle2DLNo" size="10" value="<%=request("vehicle2DLNo")%>"/>
					</td>
				</tr>
				<tr>
					<td align="right">
						Year / Make / Model:&nbsp;
					</td>
					<td>
						<input type="text" name="vehicle2YearMakeModel" size="30" value="<%=request("vehicle2YearMakeModel")%>"/>
					</td>
				</tr>
				<tr>
					<td align="right">
						Vehicle Damage:&nbsp;
					</td>
					<td>
						<input type="text" name="vehicle2Damage" size="30" value="<%=request("vehicle2Damage")%>"/>
					</td>
				</tr>
				<tr>
					<td align="right">
						Is Vehicle Drivable?:&nbsp;
					</td>
					<td>
						<input type="text" name="vehicle2Drivable" size="5" value="<%=request("vehicle2Drivable")%>"/>&nbsp;&nbsp;Approximate Repair Costs:&nbsp;<input type="text" name="vehicle2RepairCost" size="10" value="<%=request("vehicle2RepairCost")%>"/>
					</td>
				</tr>
				<tr>
					<td align="right"><strong>Telephone:</strong>&nbsp;&nbsp;Home:&nbsp;</td>
					<td><input type="text" name="vehicle2HomeNumber" size="10" value="<%=request("vehicle2HomeNumber")%>"/></td>
				</tr>
				<tr>
					<td align="right">Office:&nbsp;</td>
					<td><input type="text" name="vehicle2OfficeNumber" size="10" value="<%=request("vehicle2OfficeNumber")%>"/></td>
				</tr>
				<tr>
					<td align="right">Cell:&nbsp;</td>
					<td><input type="text" name="vehicle2CellNumber" size="10" value="<%=request("vehicle2CellNumber")%>"/></td>
				</tr>
				<tr>
					<td align="right">Insurance Co:&nbsp;</td>
					<td><input type="text" name="vehicle2InsuranceCo" size="30" value="<%=request("vehicle2InsuranceCo")%>"/></td>
				</tr>
				<tr>
					<td align="right">Policy No:&nbsp;</td>
					<td><input type="text" name="vehicle2PolicyNo" size="30" value="<%=request("vehicle2PolicyNo")%>"/></td>
				</tr>
			</table><br /><br />
			
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td align="right">Damage to property other than vehicles:&nbsp;</td>
					<td><input type="text" name="otherPropertyDamage" size="30" value="<%=request("otherPropertyDamage")%>"/></td>
				</tr>
				<tr>
					<td align="right">Approximate Repair Costs:&nbsp;</td>
					<td><input type="text" name="otherPropertyRepairCost" size="30" value="<%=request("otherPropertyRepairCost")%>"/></td>
				</tr>
			</table><br /><br />
			
			
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td colspan="5"><strong>INJURIES</strong></td>
				</tr>
				<tr>
					<td align="right">
						Name:&nbsp;
					</td>
					<td>
						<input type="text" name="injury1Name" size="30" value="<%=rsClient("injury1Name")%>"/>
					</td>
					<td width="5"></td>
					<td align="right">
						Address:&nbsp;
					</td>
					<td>
						<input type="text" name="injury1Address" size="30" value="<%=rsClient("injury1Address")%>"/>
					</td>
				</tr>
				<tr>
					<td align="right">
						Nature of Injuries:&nbsp;
					</td>
					<td>
						<input type="text" name="injury1NatureOfInjury" size="30" value="<%=rsClient("injury1NatureOfInjury")%>"/>
					</td>
					<td width="5"></td>
					<td align="right">
						Transported by Ambulance From Scene?&nbsp;
					</td>
					<td>
						<select name="injury1Ambulance">
							<option value=""></option>
							<%if trim(request("injury1Ambulance")) = "No" Then%>
								<option selected="selected">No</option>
							<%else%>
								<option>No</option>
							<%end if%>
							<%if trim(request("injury1Ambulance")) = "Yes" Then%>
								<option selected="selected">Yes</option>
							<%else%>
								<option>Yes</option>
							<%end if%>
						</select>
					</td>
				</tr>
				<tr><td colspan="5" height="10"></td></tr>
				<tr>
					<td align="right">
						Name:&nbsp;
					</td>
					<td>
						<input type="text" name="injury2Name" size="30" value="<%=rsClient("injury2Name")%>"/>
					</td>
					<td width="5"></td>
					<td align="right">
						Address:&nbsp;
					</td>
					<td>
						<input type="text" name="injury2Address" size="30" value="<%=rsClient("injury2Address")%>"/>
					</td>
				</tr>
				<tr>
					<td align="right">
						Nature of Injuries:&nbsp;
					</td>
					<td>
						<input type="text" name="injury2NatureOfInjury" size="30" value="<%=rsClient("injury2NatureOfInjury")%>"/>
					</td>
					<td width="5"></td>
					<td align="right">
						Transported by Ambulance From Scene?&nbsp;
					</td>
					<td>
						<select name="injury2Ambulance">
							<option value=""></option>
							<%if trim(request("injury2Ambulance")) = "No" Then%>
								<option selected="selected">No</option>
							<%else%>
								<option>No</option>
							<%end if%>
							<%if trim(request("injury2Ambulance")) = "Yes" Then%>
								<option selected="selected">Yes</option>
							<%else%>
								<option>Yes</option>
							<%end if%>
						</select>
					</td>
				</tr>
			</table><br /><br />
			
		
			Remarks made by driver and/or occupants of other vehicle (Pedestrian) at scene of accident:<br />
			<textarea name="otherDriverRemarks" rows="5" cols="40"><%=otherDriverRemarks%></textarea><br /><br />
			
			Remarks, IF ANY, made by you or persons connected with the Company at scene of accident:<br />
			<textarea name="driverRemarks" rows="5" cols="40"><%=driverRemarks%></textarea><br /><br />
			
			<table width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td>Road Character</td>
					<td width="10"></td>
					<td>Road Service</td>
					<td width="10"></td>
					<td>Road Effect</td>
					<td width="10"></td>
					<td>Light</td>
					<td width="10"></td>
					<td>Weather</td>
					<td width="10"></td>
					<td>Traffic Control</td>
				</tr>
				<tr>
					<td>
						<select name="roadCharacter">
							<%if trim(request("roadCharacter")) = "Straight Road" Then%>
								<option selected="selected">Straight Road</option>
							<%else%>
								<option>Straight Road</option>
							<%end if%>
							<%if trim(request("roadCharacter")) = "Curve" Then%>
								<option selected="selected">Curve</option>
							<%else%>
								<option>Curve</option>
							<%end if%>
							<%if trim(request("roadCharacter")) = "Level" Then%>
								<option selected="selected">Level</option>
							<%else%>
								<option>Level</option>
							<%end if%>
							<%if trim(request("roadCharacter")) = "On Grade" Then%>
								<option selected="selected">On Grade</option>
							<%else%>
								<option>On Grade</option>
							<%end if%>
							<%if trim(request("roadCharacter")) = "Hill Crest" Then%>
								<option selected="selected">Hill Crest</option>
							<%else%>
								<option>Hill Crest</option>
							<%end if%>
						</select>
					</td>
					<td width="10"></td>
					<td>
						<select name="roadService">
							<%if trim(request("roadService")) = "Dry" Then%>
								<option selected="selected">Dry</option>
							<%else%>
								<option>Dry</option>
							<%end if%>
							<%if trim(request("roadService")) = "Wet" Then%>
								<option selected="selected">Wet</option>
							<%else%>
								<option>Wet</option>
							<%end if%>
							<%if trim(request("roadService")) = "Muddy" Then%>
								<option selected="selected">Muddy</option>
							<%else%>
								<option>Muddy</option>
							<%end if%>
							<%if trim(request("roadService")) = "Snowy" Then%>
								<option selected="selected">Snowy</option>
							<%else%>
								<option>Snowy</option>
							<%end if%>
							<%if trim(request("roadService")) = "Ice" Then%>
								<option selected="selected">Ice</option>
							<%else%>
								<option>Ice</option>
							<%end if%>
						</select>
					</td>
					<td width="10"></td>
					<td>
						<select name="roadEffect">
							<%if trim(request("roadEffect")) = "Shoulders" Then%>
								<option selected="selected">Shoulders</option>
							<%else%>
								<option>Shoulders</option>
							<%end if%>
							<%if trim(request("roadEffect")) = "Holes, bump" Then%>
								<option selected="selected">Holes, bump</option>
							<%else%>
								<option>Holes, bump</option>
							<%end if%>
							<%if trim(request("roadEffect")) = "Loose matter" Then%>
								<option selected="selected">Loose matter</option>
							<%else%>
								<option>Loose matter</option>
							<%end if%>
							<%if trim(request("roadEffect")) = "Construction" Then%>
								<option selected="selected">Construction</option>
							<%else%>
								<option>Construction</option>
							<%end if%>
							<%if trim(request("roadEffect")) = "No Defects" Then%>
								<option selected="selected">No Defects</option>
							<%else%>
								<option>No Defects</option>
							<%end if%>
						</select>
					</td>
					<td width="10"></td>
					<td>
						<select name="light">
							<%if trim(request("light")) = "Daylight" Then%>
								<option selected="selected">Daylight</option>
							<%else%>
								<option>Daylight</option>
							<%end if%>
							<%if trim(request("light")) = "Dusk" Then%>
								<option selected="selected">Dusk</option>
							<%else%>
								<option>Dusk</option>
							<%end if%>
							<%if trim(request("light")) = "Dawn" Then%>
								<option selected="selected">Dawn</option>
							<%else%>
								<option>Dawn</option>
							<%end if%>
							<%if trim(request("light")) = "Dark" Then%>
								<option selected="selected">Dark</option>
							<%else%>
								<option>Dark</option>
							<%end if%>
							<%if trim(request("light")) = "Other" Then%>
								<option selected="selected">Other</option>
							<%else%>
								<option>Other</option>
							<%end if%>
						</select>
					</td>
					<td width="10"></td>
					<td>
						<select name="weather">
							<%if trim(request("weather")) = "Clear" Then%>
								<option selected="selected">Clear</option>
							<%else%>
								<option>Clear</option>
							<%end if%>
							<%if trim(request("weather")) = "Raining" Then%>
								<option selected="selected">Raining</option>
							<%else%>
								<option>Raining</option>
							<%end if%>
							<%if trim(request("weather")) = "Snowing" Then%>
								<option selected="selected">Snowing</option>
							<%else%>
								<option>Snowing</option>
							<%end if%>
							<%if trim(request("weather")) = "Fog" Then%>
								<option selected="selected">Fog</option>
							<%else%>
								<option>Fog</option>
							<%end if%>
							<%if trim(request("weather")) = "Other" Then%>
								<option selected="selected">Other</option>
							<%else%>
								<option>Other</option>
							<%end if%>
						</select>
					</td>
					<td width="10"></td>
					<td>
						<select name="trafficControl">
							<%if trim(request("trafficControl")) = "Stop sign" Then%>
								<option selected="selected">Stop sign</option>
							<%else%>
								<option>Stop sign</option>
							<%end if%>
							<%if trim(request("trafficControl")) = "Stop-and-go signal" Then%>
								<option selected="selected">Stop-and-go signal</option>
							<%else%>
								<option>Stop-and-go signal</option>
							<%end if%>
							<%if trim(request("trafficControl")) = "Officer" Then%>
								<option selected="selected">Officer</option>
							<%else%>
								<option>Officer</option>
							<%end if%>
							<%if trim(request("trafficControl")) = "Railroad crossing" Then%>
								<option selected="selected">Railroad crossing</option>
							<%else%>
								<option>Railroad crossing</option>
							<%end if%>
							<%if trim(request("trafficControl")) = "No traffic control" Then%>
								<option selected="selected">No traffic control</option>
							<%else%>
								<option>No traffic control</option>
							<%end if%>
						</select>
					</td>
				</tr>
			</table><br /><br />
			
			<strong>WITNESSES:</strong><br />
			<table width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td align="right">Name:&nbsp;</td>
					<td><input type="text" name="witnessName1" size="30" value="<%=request("witnessName1")%>"/>&nbsp;&nbsp;Phone:&nbsp;<input type="text" name="witnessPhone1" size="10" value="<%=request("witnessPhone1")%>"/></td>
				</tr>
				<tr>
					<td align="right">Address:&nbsp;</td>
					<td colspan="3"><input type="text" name="witnessAddress1" size="52" value="<%=request("witnessAddress1")%>"/></td>
				</tr>
				<tr><td colspan="2" height="10"></td></tr>
				<tr>
					<td align="right">Name:&nbsp;</td>
					<td><input type="text" name="witnessName2" size="30" value="<%=request("witnessName2")%>"/>&nbsp;&nbsp;Phone:&nbsp;<input type="text" name="witnessPhone2" size="10" value="<%=request("witnessPhone2")%>"/></td>
				</tr>
				<tr>
					<td align="right">Address:&nbsp;</td>
					<td colspan="3"><input type="text" name="witnessAddress2" size="52" value="<%=request("witnessAddress2")%>"/></td>
				</tr>
				<tr><td colspan="2" height="10"></td></tr>
				<tr>
					<td align="right">Investigating Officer:&nbsp;</td>
					<td><input type="text" name="investigatingOfficer" size="20" value="<%=request("investigatingOfficer")%>"/>&nbsp;&nbsp;Department:&nbsp;<input type="text" name="department" size="20" value="<%=request("department")%>"/>&nbsp;&nbsp;Case No:&nbsp;<input type="text" name="caseNo" size="5" value="<%=request("caseNo")%>"/></td>
				</tr>
			</table><br /><br />
			
			<strong>DESCRIBE WHAT HAPPENED</strong><br />
			Refer to vehicles by number. If third vehicle was involved, please indicate it by giving specific data. <br />Describe any details regarding the incident not 
			already covered in accident report above.<br />
			<textarea name="describeWhatHappened" rows="5" cols="40"><%=request("describeWhatHappened")%></textarea><br /><br />
			
			Reported By:<br />
			<input type="text" name="reportedBy" size="30" maxlength="50" />
			
		</td>
		
	</tr>
</table>
