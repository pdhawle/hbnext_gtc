<!--#include file="includes/constants.inc"-->
<%

userID = request("userID")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If
%>
<html>
<head>
<title><%=sPageTitle%></title>
<link rel="stylesheet" href="includes/main.css" type="text/css">
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Upload Signature</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td>
												Upload an image of your signature. Once uploaded, you will be returned to edit your account.<br>
												Recommended file dimensions: 150px width x 40px height or smaller.<br>
												Supported file types: <strong>gif, jpg, or tiff</strong>
											   <FORM METHOD="POST" ENCTYPE="multipart/form-data" ACTION="uploadSig.asp?userID=<%=userID%>">
												  <INPUT TYPE="FILE" SIZE="30" NAME="FILE1" class="formButton"><BR><br>
											   <INPUT TYPE=SUBMIT VALUE="Upload Signature" class="formButton">
											   
											   </FORM>
											</td>
										</tr>
									</table>
															
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>