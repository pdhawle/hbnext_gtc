<%
dim iUserID,rsUser

iUserID = request("id")
isUser = False
sec = request("sec")

'response.Write iUserID & "<br>"
'response.Write isUser & "<br>"
if sec = "" then
	sec = "acctInfo"
end if

sMsgAcctInfo=request("sMsgAcctInfo")
sMsgResume=request("sMsgResume")
sMsgJobhistory=request("sMsgJobhistory")
sMsgEducation=request("sMsgEducation")
sMsgCerts=request("sMsgCerts")

sSearch = request("s")

if iUserID = "" then
	isUser = True
	iUserID = Session("ID")
end if

'response.Write isUser

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetUser"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iUserID)
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsUser = oCmd.Execute
Set oCmd = nothing

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetUserProject"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iUserID)
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsUserProject = oCmd.Execute
Set oCmd = nothing

'Create command for state list
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetStates"
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsState = oCmd.Execute
Set oCmd = nothing


'Create command for customer list
Set oCmd = Server.CreateObject("ADODB.Command")

If session("superAdmin") = "True" Then	
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetAvailableProjects"
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iUserID)
	   .CommandType = adCmdStoredProc
	   
	End With
else
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetAvailableProjectsForClient"
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iUserID)
	   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, session("clientID"))
	   .CommandType = adCmdStoredProc
	   
	End With
end if
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsAvailProjects = oCmd.Execute
Set oCmd = nothing



'Create command for customer list
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetAvailableReports"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iUserID)
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsAvailReports = oCmd.Execute
Set oCmd = nothing

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetUserReportTypes"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iUserID)
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsUserReportType = oCmd.Execute
Set oCmd = nothing


'*new
Set oCmd = Server.CreateObject("ADODB.Command")	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCerts"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iUserID)
   .CommandType = adCmdStoredProc
   
End With
			
Set rsCert = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetEducationList"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iUserID)
   .CommandType = adCmdStoredProc
   
End With
			
Set rsEdu = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetJobHistory"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iUserID)
   .CommandType = adCmdStoredProc
   
End With
			
Set rsJob = oCmd.Execute
Set oCmd = nothing
%>

<!--*****************for the tabs-->

<script language="javascript" SRC="includes/common.js"></script>
<script LANGUAGE="javascript">
<!--
	
function confirmDeleteCert(delUrl) {
 if (confirm("Are you sure you wish to delete this certification?")) {
	document.location = delUrl;
  }


}

function confirmDeleteEdu(delUrl) {
 if (confirm("Are you sure you wish to delete this school?")) {
	document.location = delUrl;
  }


}

function confirmDeleteJob(delUrl) {
 if (confirm("Are you sure you wish to delete this company?")) {
	document.location = delUrl;
  }


}



// determine browser type
var browser = (navigator.userAgent.toLowerCase().indexOf("msie") != -1)?"ie":"ns";

function MoveUp()
{
	ShiftListSelections(document.editUser.allprojects, false);
}

function MoveDown()
{
	ShiftListSelections(document.editUser.allprojects, true);
	
}

/* 
** Add the selected fields from the freefields listbox into the
** usedfields listbox, then remove them from the freefields listbox
*/
function AddFields() {
	//alert("test");
	//document.editUser.project
	MoveSelectedListItems(document.editUser.project,document.editUser.allprojects,false);
	//Force a refresh to keeps netscape happy.
	//if (browser=="ns") history.go(0);
}

/* This function does the opposite of Addfields, but ensures the free list is alphabetical */
function RemoveFields() {
	//document.editUser.allprojects
	//document.editUser.project
	//alert(document.editUser.allprojects..options.length)
	MoveSelectedListItems(document.editUser.allprojects,document.editUser.project,true);
	//Force a refresh to keeps netscape happy.
	//if (browser=="ns") history.go(0);
}

function SelectAll(sel)
{
	for (var x=0; x<sel.length; x++){sel.options[x].selected=true;}
}

function selectAll(box) 
{     
	for(var i=0; i<box.length; i++) 
	{     
		box[i].selected = true;     
	}     
}


//add the reporttypes
function AddFieldsRT() {
	MoveSelectedListItems(document.editUser.reports,document.editUser.allreports,false);
	//Force a refresh to keeps netscape happy.
	//if (browser=="ns") history.go(0);
}

/* This function does the opposite of Addfields, but ensures the free list is alphabetical */
function RemoveFieldsRT() {
	MoveSelectedListItems(document.editUser.allreports,document.editUser.reports,true);
	//Force a refresh to keeps netscape happy.
	//if (browser=="ns") history.go(0);
}


function confirmDeleteSig(delUrl) {
 if (confirm("Are you sure you wish to delete this signature?")) {
    document.location = delUrl;
  }


}


//-->
</script>

<script type="text/javascript" src="ckeditor/ckeditor.js"></script>

<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Edit User Information</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
							&nbsp;&nbsp;<a href="form.asp?id=<%=iUserID%>&formType=editUser&sec=acctInfo" class="footerLink">Account Info</a>&nbsp;&nbsp;
							<span class="footerLink">|</span>&nbsp;&nbsp;<a href="form.asp?id=<%=iUserID%>&formType=editUser&sec=resume" class="footerLink">Resume</a>&nbsp;&nbsp;
							<span class="footerLink">|</span>&nbsp;&nbsp;<a href="form.asp?id=<%=iUserID%>&formType=editUser&sec=jobhistory" class="footerLink">Job History</a>&nbsp;&nbsp;
							<span class="footerLink">|</span>&nbsp;&nbsp;<a href="form.asp?id=<%=iUserID%>&formType=editUser&sec=education" class="footerLink">Education</a>&nbsp;&nbsp;
							<span class="footerLink">|</span>&nbsp;&nbsp;<a href="form.asp?id=<%=iUserID%>&formType=editUser&sec=certs" class="footerLink">Certifications</a>&nbsp;&nbsp;
							<span class="footerLink">|</span>&nbsp;&nbsp;<a href="form.asp?id=<%=iUserID%>&formType=editUser&sec=docs" class="footerLink">Documents</a>&nbsp;&nbsp;
						<%if session("Admin") = "True" then%>
							<span class="footerLink">|</span>&nbsp;&nbsp;<a href="resumeSearch.asp" class="footerLink">search resumes</a>&nbsp;&nbsp;
							<span class="footerLink">|</span>&nbsp;&nbsp;<a href="userList.asp" class="footerLink">user list</a>&nbsp;&nbsp;	
						<%end if%>
						
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									
									<!--<div id="myTab">
										<ul>
											<li><a href="#acctInfo"><span>Account Info</span></a></li>
											<li><a href="#resume"><span>Resume</span></a></li>
											<li><a href="#jobhistory"><span>Job History</span></a></li>
											<li><a href="#education"><span>Education</span></a></li>
											<li><a href="#certs"><span>Certifications</span></a></li>
											<li><a href="#docs"><span>Documents</span></a></li>
										</ul>-->
									
									
									
										<%select case sec
										
										case "acctInfo"%>
											<form name="editUser" method="post" action="process.asp">
											<table border="0" cellpadding="0" cellspacing="0">
												<tr>
													<td colspan="3">
														<%if sMsgAcctInfo = "True" then%>
															<font color="#EE0000"><strong>Your changes have been made</font></span>
														<%else%>
															<span class="redText">&nbsp;</span>
														<%end if%>
													</td>
												</tr>				
												<tr><td colspan="3" height="5"></td></tr>
												<tr>
													<td align="right"><strong>First Name:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<input type="text" name="firstName" size="30" value="<%=rsUser("firstName")%>" tooltipText="Enter your first name in this box.">
													</td>
												</tr>
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
												<tr>
													<td align="right"><strong>Last Name:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<input type="text" name="lastName" size="30" value="<%=rsUser("lastName")%>" tooltipText="Enter your last name in this box.">
													</td>
												</tr>
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
												<tr>
													<td align="right"><strong>Job Title:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<input type="text" name="jobtitle" size="30" value="<%=rsUser("jobtitle")%>" tooltipText="Enter your job title in this box.">
													</td>
												</tr>
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
												<tr>
													<td align="right"><strong>WECS Cert. #:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<input type="text" name="WECSNo" size="10" value="<%=rsUser("WECSNo")%>">&nbsp;&nbsp;<strong>Exp. Date</strong>&nbsp;&nbsp;<input type="text" name="WECSExpDate" maxlength="10" size="10" value="<%=rsUser("WECSExpDate")%>"/>&nbsp;<a href="javascript:displayDatePicker('WECSExpDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
													</td>
												</tr>
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
												<tr>
													<td align="right"><strong>GASWCC Level 1-A Cert. #:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<input type="text" name="GASWCCNo" size="10" value="<%=rsUser("GASWCCNo")%>">&nbsp;&nbsp;<strong>Exp. Date</strong>&nbsp;&nbsp;<input type="text" name="GASWCCExpDate" maxlength="10" size="10" value="<%=rsUser("GASWCCExpDate")%>"/>&nbsp;<a href="javascript:displayDatePicker('GASWCCExpDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
													</td>
												</tr>
												<!--<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
												<tr>
													<td valign="top" align="right"><strong>Level 1-A Cert Image:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<%'if rsUser("level1AFileName") <> "" then%>
															<img src="userUploads/<%'=rsUser("userID")%>/1Acert/<%'=rsUser("level1AFileName")%>"><br />
															<%'=rsUser("level1AFileName")%>
														<%'else
															'response.Write "You currently do not have a level 1-A certification card uploaded"
														'end if%>											
														<br /><a href="uploadLevel1A.asp?userID=<%'=iUserID%>">upload new level 1-A card image</a><br /><br />
													</td>
												</tr>-->
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
												<tr>
													<td valign="top" align="right"><strong>Other Certifications:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<textarea name="otherCerts" cols="22" rows="3"><%=rsUser("otherCerts")%></textarea>
													</td>
												</tr>
												
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
												<tr>
													<td align="right"><strong>Email Address:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<input type="text" name="email" size="30" value="<%=rsUser("email")%>" tooltipText="Enter your email address in this box. All emails to you will be sent to this address.">
													</td>
												</tr>
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
												<tr>
													<td align="right"><strong>Office Number:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<input type="text" name="officePhone" size="30" value="<%=rsUser("officePhone")%>" tooltipText="Enter your office phone in this box.">
													</td>
												</tr>
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
												<tr>
													<td align="right"><strong>Cell Number:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<input type="text" name="cellPhone" size="30" value="<%=rsUser("cellPhone")%>" tooltipText="Enter your cell phone in this box. This is the number that will be printed out on all reports.">
													</td>
												</tr>
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
												<tr>
													<td align="right"><strong>Company Name:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<input type="text" name="company" size="30" value="<%=rsUser("company")%>" tooltipText="Enter the name of your company in this box.">
													</td>
												</tr>
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
												<tr>
													<td align="right"><strong>State:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<select name="state" tooltipText="Select the state you will be working in.">
															<%do until rsState.eof
																If rsState("stateID") = rsUser("state") then%>
																	<option selected="selected" value="<%=rsState("stateID")%>"><%=rsState("stateName")%></option>
																<%else%>	
																	<option value="<%=rsState("stateID")%>"><%=rsState("stateName")%></option>
																<%end if
															rsState.movenext
															loop
															rsState.moveFirst%>
														</select>
													</td>
												</tr>	
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
												<tr>
													<td align="right"><strong>Application Logo:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<input type="text" name="appLogo" size="30" value="<%=rsUser("appLogo")%>" maxlength="150">
													</td>
												</tr>
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
												<tr>
													<td valign="top" align="right"><strong>Signature Image:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<%if rsUser("sigFileName") <> "" then%>
															<img src="userUploads/<%=rsUser("userID")%>/<%=rsUser("sigFileName")%>"><br />
															<%=rsUser("sigFileName")%>&nbsp;<em>(actual size on reports)</em>
														<%else
															response.Write "You currently do not have a signature image uploaded"
														end if%>											
														<br /><br />
														<%if rsUser("sigFileName") <> "" then%>
															<a onClick="return confirmDeleteSig('process.asp?id=<%=rsUser("userID")%>&processType=deleteSignature')" style="cursor:pointer;">delete signature</a>&nbsp;|&nbsp;
														<%end if%>
														<a href="uploadSignature.asp?userID=<%=iUserID%>">upload new signature</a><br />
														<em>(dynamically signs reports with the provided signature image)</em><br /><br />
													</td>
												</tr>
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
												<tr>
													<td valign="top" align="right"><strong>Email Signature:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<%
														'	Dim strFormNames
														'	Dim strTextAreaNames
														'	strFormNames = "myForm"
														'	strTextAreaNames = "emailSignature"
														%>
														<!--<textarea name="emailSignature" cols="60" rows="10"><%'=rsUser("emailSignature")%></textarea>
														<script>
															//STEP 2: Replace the textarea (txtContent)
															oEdit1 = new InnovaEditor("oEdit1");
															oEdit1.features=["FullScreen","Preview","Print","Search",
																"Cut","Copy","Paste","PasteWord","PasteText","|","Undo","Redo","|",
																"ForeColor","BackColor","|","Bookmark","Hyperlink","XHTMLSource","BRK",
																"Numbering","Bullets","|","Indent","Outdent","LTR","RTL","|",
																"Image","Flash","Media","|","Table","Guidelines","Absolute","|",
																"Characters","Line","Form","RemoveFormat","ClearAll","BRK",
																"StyleAndFormatting","TextFormatting","ListFormatting","BoxFormatting",
																"ParagraphFormatting","CssText","|",
																"Paragraph","FontName","FontSize","|",
																"Bold","Italic","Underline","Strikethrough","Superscript","Subscript","|",
																"JustifyLeft","JustifyCenter","JustifyRight","JustifyFull"];
															oEdit1.REPLACE("emailSignature");
														</script>-->
														<textarea cols="80" id="emailSignature" name="emailSignature" rows="10"><%=rsUser("emailSignature")%></textarea>
														<script type="text/javascript">
															CKEDITOR.replace( 'emailSignature' );											
														</script>
													</td>
												</tr>
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
												<%if isUser = False then%>
												<tr>
													<td align="right"><strong>Active:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<input type="checkbox" name="isActive" <%=isChecked(rsUser("isActive"))%> />
													</td>
												</tr>
												<!--<tr><td colspan="3"><img src="images/pix.gif" width="1" height="15"></td></tr>
												<tr>
													<td align="right"><strong>OSHA Manager:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<input type="checkbox" name="OSHAManager" <%'=isChecked(rsUser("OSHAManager"))%> />
													</td>
												</tr>-->
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="15"></td></tr>
												<tr>
													<td align="right"><strong>Read Only:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<input type="checkbox" name="customerAdmin" <%=isChecked(rsUser("customerAdmin"))%> />
														<input type="hidden" name="customerReports" value="" />
														<input type="hidden" name="openCorrectedLog" value="" />
														<input type="hidden" name="projectDocuments" value="" />
														<input type="hidden" name="OSHAManager" value="" />
														<input type="hidden" name="noiUser" value="" />
														<input type="hidden" name="canApprove" value="" />
														<input type="hidden" name="deleteWorkOrder" value="" />
														<input type="hidden" name="closeWorkOrder" value="" />
														<input type="hidden" name="receiveSevenDayAlerts" value="" />
														<input type="hidden" name="viewQuotes" value="" />												
														<input type="hidden" name="empType" value="" />
														
													</td>
												</tr>
												<!--<tr class="rowColor"><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
												<tr class="rowColor">
													<td align="right"><strong>Customer Reports:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<input type="checkbox" name="customerReports" <%'=isChecked(rsUser("customerReports"))%> /> customer must be checked to see this
													</td>
												</tr>
												<tr class="rowColor"><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
												<tr class="rowColor">
													<td align="right"><strong>Open Corrected Log:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<input type="checkbox" name="openCorrectedLog" <%'=isChecked(rsUser("openCorrectedLog"))%> /> customer must be checked to see this
													</td>
												</tr>
												<tr class="rowColor"><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
												<tr class="rowColor">
													<td align="right"><strong>Project Documents:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<input type="checkbox" name="projectDocuments" <%'=isChecked(rsUser("projectDocuments"))%> /> customer or inspector must be checked to see this&nbsp;
													</td>
												</tr>-->
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="15"></td></tr>
												<tr>
													<td align="right"><strong>Create/Edit:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<input type="checkbox" name="inspector" <%=isChecked(rsUser("inspector"))%> />
													</td>
												</tr>
												<!--<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
												<tr class="rowColor">
													<td align="right"><strong>NOI User:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<input type="checkbox" name="noiUser" <%'=isChecked(rsUser("noiUser"))%> />
													</td>
												</tr>-->
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
												<tr>
													<td align="right"><strong>Single Project User:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<input type="checkbox" name="isSingleProject" <%=isChecked(rsUser("isSingleProject"))%> /> Is only assigned to a single project
													</td>
												</tr>	
												
												<!--<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
												<tr class="rowColor">
													<td align="right"><strong>Can Approve WOs:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<input type="checkbox" name="canApprove" <%'=isChecked(rsUser("canApprove"))%> /> user will be able to approve work orders
													</td>
												</tr>
												<tr class="rowColor"><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
												<tr class="rowColor">
													<td align="right"><strong>Can Delete WOs:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<input type="checkbox" name="deleteWorkOrder" <%'=isChecked(rsUser("deleteWorkOrder"))%> /> user will be able to delete work orders
													</td>
												</tr>
												<tr class="rowColor"><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
												<tr class="rowColor">
													<td align="right"><strong>Can Close WOs:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<input type="checkbox" name="closeWorkOrder" <%'=isChecked(rsUser("closeWorkOrder"))%> /> user will be able to close work orders
													</td>
												</tr>-->														
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
												<tr>
													<td align="right"><strong>Recieve Open Item Alert:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<input type="checkbox" name="recieveOIAlerts" <%=isChecked(rsUser("recieveOIAlerts"))%> />
													</td>
												</tr>
												<!--<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
												<tr>
													<td align="right"><strong>Recieve 7 day Alert:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<input type="checkbox" name="receiveSevenDayAlerts" <%'=isChecked(rsUser("receiveSevenDayAlerts"))%> />
													</td>
												</tr>-->
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
												<tr>
													<td align="right"><strong>Recieve 14 day alert:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<input type="checkbox" name="receiveFourteenDayAlerts" <%=isChecked(rsUser("receiveFourteenDayAlerts"))%> />
													</td>
												</tr>
												<!--<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
												<tr class="rowColor">
													<td align="right"><strong>Can View/Create Quotes:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<input type="checkbox" name="viewQuotes" <%'=isChecked(rsUser("viewQuotes"))%> /> user will be able to view and create inspection quotes
													</td>
												</tr>
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
												<tr class="rowColor">
													<td align="right"><strong>User Type:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<select name="empType">
															<option value="0" <%'=isSelected(rsUser("empType"), "0")%>></option>
															<option value="1" <%'=isSelected(rsUser("empType"), "1")%>>Division VP</option>
															<option value="2" <%'=isSelected(rsUser("empType"), "2")%>>Foreman</option>
															<option value="3" <%'=isSelected(rsUser("empType"), "3")%>>General Superintendent</option>
															<option value="4" <%'=isSelected(rsUser("empType"), "4")%>>HR</option>
														</select> used for the accident reports
													</td>
												</tr>-->
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="20"></td></tr>
											</table>
											<table border="0" cellpadding="0" cellspacing="0">
												<tr>
													<td valign="top" align="right"><strong>Projects:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
																				
														<table width="100%" border="0" cellspacing="0" cellpadding="0"> 
															<tr valign="top"> 
																<td> 
																	<table class="rowColor" border="0"> 
																		<tr><td colspan="3">&nbsp;&nbsp;Note: To select multiple projects hold down the Ctrl key and select the items you want.&nbsp;&nbsp;</td></tr>
																		<tr> 
																			<td> 
																				&nbsp;&nbsp;<strong>All Available Projects</strong><br />
																				&nbsp;&nbsp;<select name="allprojects" id="allprojects" multiple="multiple" size="12">
																					<%do until rsAvailProjects.eof%>
																						<option value="<%=rsAvailProjects("projectID")%>"><%=rsAvailProjects("customerName")%> - <%=rsAvailProjects("division")%> - <%=rsAvailProjects("projectName")%></option>
																					<%rsAvailProjects.movenext
																					loop
																					rsAvailProjects.moveFirst%>
																				</select>
																				<br /><br />
																				<div align="center">
																					<a href="#acctInfo" onClick="AddFields();return false;"><img src="images/arrowup.gif" alt="Move Selected List 2 Fields to List 1" border="0" width="22" height="12"></a>&nbsp;&nbsp;&nbsp; 
																					<a href="#acctInfo" onClick="RemoveFields();return false;"><img src="images/arrowdown.gif" alt="Move Selected List 1 Fields to List 2" border="0" width="22" height="12"></a> 
																				</div>
																				<br /><br />
																				&nbsp;&nbsp;<strong>Projects Assigned</strong><br />
																				&nbsp;&nbsp;<select name="project" id="project" multiple="multiple" size="12">
																					<%do until rsUserProject.eof%>
																							<option value="<%=rsUserProject("projectID")%>"><%=rsUserProject("customerName")%> - <%=rsUserProject("division")%> - <%=rsUserProject("projectName")%></option>
																					<%rsUserProject.movenext
																					loop
																					rsUserProject.moveFirst%>
																				</select><br /><br />
																			</td>
																		</tr> 
																	</table>
																</td>
															</tr> 
														</table>
														
													</td>
												</tr>				
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>									
												<tr>
													<td valign="top" align="right"><strong>Reports:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
																				
														<table width="100%" border="0" cellspacing="0" cellpadding="0"> 
															<tr valign="top"> 
																<td> 
																	<table class="rowColor" border="0" cellpadding="0" cellspacing="0"> 
																		<tr><td colspan="3">&nbsp;&nbsp;Note: To select multiple reports hold down the Ctrl key and select the items you want.&nbsp;&nbsp;</td></tr>
																		<tr> 
																			<td colspan="3">
																				&nbsp;&nbsp;<strong>All Available Reports</strong><br />
																				&nbsp;&nbsp;<select name="allreports" id="allreports" multiple="multiple" size="12">
																					<%do until rsAvailReports.eof%>
																						<option value="<%=rsAvailReports("reportTypeID")%>"><%=rsAvailReports("reportType")%></option>
																					<%rsAvailReports.movenext
																					loop
																					rsAvailReports.moveFirst%>
																				</select><br /><br />
		
																				<div align="center">
																					<a href="#" onClick="AddFieldsRT();return false;"><img src="images/arrowup.gif" alt="Move Selected List 2 Fields to List 1" border="0" width="22" height="12"></a>&nbsp;&nbsp;&nbsp; 
																					<a href="#" onClick="RemoveFieldsRT();return false;"><img src="images/arrowdown.gif" alt="Move Selected List 1 Fields to List 2" border="0" width="22" height="12"></a> 
																				</div><br /><br />
																				&nbsp;&nbsp;<strong>Reports Assigned</strong><br />
																				&nbsp;&nbsp;<select name="reports" id="reports" multiple="multiple" size="12">
																					<%do until rsUserReportType.eof%>
																							<option value="<%=rsUserReportType("reportTypeID")%>"><%=rsUserReportType("reportType")%></option>
																					<%rsUserReportType.movenext
																					loop
																					rsUserReportType.moveFirst%>
																				</select><br /><br />
																			</td>
																		</tr> 
																	</table>
																</td>
															</tr> 
														</table>
														
													</td>
												</tr>				
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
												
												
												<%end if%>
												<tr>
													<td align="right"><strong>Username:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<input type="text" name="userName" size="30" value="<%=rsUser("userName")%>">
													</td>
												</tr>
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>	
												<tr>
													<td align="right"><strong>Password:</strong></td>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td>
														<input type="password" name="password" size="30" value="<%=rsUser("password")%>">
													</td>
												</tr>
												<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
												<tr>
													<td colspan="3" align="right">
														<input type="hidden" name="isUser" value="<%=isUser%>" />
														<input type="hidden" name="userID" value="<%=iUserID%>" />
														<input type="hidden" name="processType" value="editUser" />
														<input type="submit" value="  Save  " class="formButton" onClick="selectAll(document.editUser.project);selectAll(document.editUser.reports);"/>
													</td>
												</tr>
											</table>
											</form>
										
										<%case "resume"%>
									
									
											<form name="editUserResume" method="post" action="process.asp">
											<table width="100%" border="0" cellpadding="0" cellspacing="0">
												<tr>
													<td colspan="3">
														<%if sMsgResume = "True" then%>
															<font color="#EE0000"><strong>Your changes have been made</font></span>
														<%else%>
															<span class="redText">&nbsp;</span>
														<%end if%>
													</td>
												</tr>				
												<tr><td colspan="3" height="5"></td></tr>
												<tr>
													<td>
														<textarea cols="80" id="txtresume" name="txtresume" rows="40"><%=rsUser("sresume")%></textarea>
														<script type="text/javascript">
														//<![CDATA[
															CKEDITOR.replace( 'txtresume' );											
														//]]>
														</script>
													</td>
												</tr>
												<tr><td colspan="3" height="10"></td></tr>
												<tr>
													<td colspan="3" align="right">
														<input type="hidden" name="isUser" value="<%=isUser%>" />
														<input type="hidden" name="userID" value="<%=iUserID%>" />
														<input type="hidden" name="processType" value="editResume" />
														<input type="submit" value="  Save  " class="formButton"/>
													</td>
												</tr>
											</table>
											</form>
										
										<%case "jobhistory"%>
										
											<table width="100%" border="0" cellpadding="0" cellspacing="0">
												<tr>
													<td colspan="3">
														<%if sMsgJobhistory = "True" then%>
															<font color="#EE0000"><strong>Your changes have been made</font></span>
														<%else%>
															<span class="redText">&nbsp;</span>
														<%end if%>
													</td>
												</tr>
												<tr><td colspan="3" height="5"></td></tr>
												<tr>
													<td colspan="3">
														<strong><u>My Job History</u></strong><br /><br />
														<table width="100%" cellpadding="0" cellspacing="0">
															<tr bgcolor="#666666">
																<td class="searchText">&nbsp;Company</td>
																<td class="searchText">Title</td>
																<td class="searchText">Responsibilities</td>
																<td class="searchText">Start Date</td>
																<td class="searchText">End Date</td>
																<td class="searchText" align="center">Action</td>
															</tr>
															<%if rsJob.eof then%>
																<tr><td colspan="4">you do not have any job history</td></tr>
															<%else%>
																
																<%blnChange = true
																do until rsJob.eof
																	If blnChange = true then%>
																		<tr class="rowColor">
																	<%else%>
																		<tr>
																	<%end if%>
																		<td>&nbsp;<%=rsJob("company")%></td>
																		<td>&nbsp;<%=rsJob("title")%></td>
																		<td>&nbsp;<%=rsJob("responsibilities")%></td>
																		<td><%=rsJob("startDate")%></td>
																		<td><%=rsJob("endDate")%></td>
																		<td align="center"><input type="image" src="images/delete.gif" width="8" height="9" alt="delete" border="0" onClick="return confirmDeleteJob('process.asp?id=<%=rsJob("jobID")%>&processType=deleteJobHistory&isUser=<%=isUser%>&userID=<%=iUserID%>')"></td>
																	</tr>
																<%rsJob.movenext
																if blnChange = true then
																	blnChange = false
																else
																	blnChange = true
																end if
																loop%>
															<%end if%>
														</table>
													</td>
												</tr>				
												<tr><td colspan="3" height="45"></td></tr>
												<form name="editUser" method="post" action="process.asp">
												<tr>
													<td colspan="3">
														<strong><u>Add New Job</u></strong>
													</td>
												</tr>
												<tr>
													<td align="right"><strong>Company:</strong></td>
													<td width=5></td>
													<td>
														<input type="text" name="company" size="50" value="" maxlength="150">
													</td>
												</tr>
												<tr><td colspan="3" height="5"></td></tr>
												<tr>
													<td align="right"><strong>Title:</strong></td>
													<td width=5></td>
													<td>
														<input type="text" name="title" size="50" value="" maxlength="150">
													</td>
												</tr>
												<tr>
													<td align="right" valign="top"><strong>Responsibilities:</strong></td>
													<td width=5></td>
													<td>
														<textarea name="responsibilities" rows="5" cols="50"></textarea>
													</td>
												</tr>
												<tr><td colspan="3" height="5"></td></tr>
												<tr><td colspan="3" height="5"></td></tr>
												<tr>
													<td align="right"><strong>Start Date:</strong></td>
													<td width=5></td>
													<td>
														<input type="text" name="startDate" maxlength="10" size="10" value=""/>&nbsp;<a href="javascript:displayDatePicker('startDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
													</td>
												</tr>
												<tr><td colspan="3" height="5"></td></tr>
												<tr>
													<td align="right"><strong>End Date:</strong></td>
													<td width=5></td>
													<td>
														<input type="text" name="endDate" maxlength="10" size="10" value=""/>&nbsp;<a href="javascript:displayDatePicker('endDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
													</td>
												</tr>
												<tr><td colspan="3" height="10"></td></tr>
												<tr>
													<td colspan="3" align="right">
														<input type="hidden" name="isUser" value="<%=isUser%>" />
														<input type="hidden" name="userID" value="<%=iUserID%>" />
														<input type="hidden" name="processType" value="addJobHistory" />
														<input type="submit" value="  Save  " class="formButton"/>
													</td>
												</tr>
											</table>
											</form>
											
										<%case "education"%>
										
										
											<table width="100%" border="0" cellpadding="0" cellspacing="0">
												<tr>
													<td colspan="3">
														<%if sMsgEducation = "True" then%>
															<font color="#EE0000"><strong>Your changes have been made</font></span>
														<%else%>
															<span class="redText">&nbsp;</span>
														<%end if%>
													</td>
												</tr>
												<tr><td colspan="3" height="5"></td></tr>
												<tr>
													<td colspan="3">
														<strong><u>My Education</u></strong><br /><br />
														<table width="100%" cellpadding="0" cellspacing="0">
															<tr bgcolor="#666666">
																<td class="searchText">&nbsp;School</td>
																<td class="searchText">Start Date</td>
																<td class="searchText">End Date</td>
																<td class="searchText" align="center">Action</td>
															</tr>
															<%if rsEdu.eof then%>
																<tr><td colspan="4">you do not have any schools listed</td></tr>
															<%else%>
																
																<%blnChange = true
																do until rsEdu.eof
																	If blnChange = true then%>
																		<tr class="rowColor">
																	<%else%>
																		<tr>
																	<%end if%>
																		<td>&nbsp;<%=rsEdu("school")%></td>
																		<td><%=rsEdu("startDate")%></td>
																		<td><%=rsEdu("endDate")%></td>
																		<td align="center"><input type="image" src="images/delete.gif" width="8" height="9" alt="delete" border="0" onClick="return confirmDeleteEdu('process.asp?id=<%=rsEdu("educationID")%>&processType=deleteEducation&isUser=<%=isUser%>&userID=<%=iUserID%>')"></td>
																	</tr>
																<%rsEdu.movenext
																if blnChange = true then
																	blnChange = false
																else
																	blnChange = true
																end if
																loop%>
															<%end if%>
														</table>
													</td>
												</tr>				
												<tr><td colspan="3" height="45"></td></tr>
												<form name="editUserEducation" method="post" action="process.asp">
												<tr>
													<td colspan="3">
														<strong><u>Add New School</u></strong>
													</td>
												</tr>
												<tr>
													<td align="right"><strong>School:</strong></td>
													<td width=5></td>
													<td>
														<input type="text" name="school" size="50" value="" maxlength="150">
													</td>
												</tr>
												<tr><td colspan="3" height="5"></td></tr>
												<tr>
													<td align="right"><strong>Start Date:</strong></td>
													<td width=5></td>
													<td>
														<input type="text" name="startDateEdu" maxlength="10" size="10" value=""/>&nbsp;<a href="javascript:displayDatePicker('startDateEdu')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
													</td>
												</tr>
												<tr><td colspan="3" height="5"></td></tr>
												<tr>
													<td align="right"><strong>End Date:</strong></td>
													<td width=5></td>
													<td>
														<input type="text" name="endDateEdu" maxlength="10" size="10" value=""/>&nbsp;<a href="javascript:displayDatePicker('endDateEdu')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
													</td>
												</tr>
												<tr><td colspan="3" height="10"></td></tr>
												<tr>
													<td colspan="3" align="right">
														<input type="hidden" name="isUser" value="<%=isUser%>" />
														<input type="hidden" name="userID" value="<%=iUserID%>" />
														<input type="hidden" name="processType" value="addEducation" />
														<input type="submit" value="  Save  " class="formButton"/>
													</td>
												</tr>
											</table>
											</form>
										
										<%case "certs"%>
											
											<table width="100%" border="0" cellpadding="0" cellspacing="0">
												<tr>
													<td colspan="3">
														<%if sMsgCerts = "True" then%>
															<font color="#EE0000"><strong>Your changes have been made</font></span>
														<%else%>
															<span class="redText">&nbsp;</span>
														<%end if%>
													</td>
												</tr>
												<tr><td colspan="3" height="5"></td></tr>
												<tr>
													<td colspan="3">
														<strong><u>My Certifications</u></strong><br /><br />
														<table width="100%" cellpadding="0" cellspacing="0">
															<tr bgcolor="#666666">
																<td class="searchText">&nbsp;Certification</td>
																<td class="searchText">Certification Number</td>
																<td class="searchText">Date Passed</td>
																<td class="searchText">Expiration Date</td>
																<td class="searchText" align="center">Upload/View Card</td>
																<td class="searchText" align="center">Action</td>
															</tr>
															<%if rsCert.eof then%>
																<tr><td colspan="4">you do not have any certifications listed</td></tr>
															<%else%>
																
																<%blnChange = true
																do until rsCert.eof
																	If blnChange = true then%>
																		<tr class="rowColor">
																	<%else%>
																		<tr>
																	<%end if%>
																		<td>&nbsp;<%=rsCert("certification")%></td>
																		<td><%=rsCert("certificationNumber")%></td>
																		<td><%=rsCert("datePassed")%></td>
																		<td><%=rsCert("expirationDate")%></td>
											
																		<td align="center">
																			<a href="uploadLevel1A.asp?userID=<%=iUserID%>&certID=<%=rsCert("certificationID")%>">upload</a>&nbsp;&nbsp;
																			<%if rsCert("cardImage") <> "" then%>
																			|&nbsp;&nbsp;<a href="userUploads/<%=iUserID%>/<%=rsCert("certificationID")%>/<%=rsCert("cardImage")%>" title="<%=rsCert("certification")%>" rel="gb_image[]">view</a>
																			<%end if%>
																		</td>
																		<td align="center"><input type="image" src="images/delete.gif" width="8" height="9" alt="delete" border="0" onClick="return confirmDeleteCert('process.asp?id=<%=rsCert("certificationID")%>&processType=deleteCertification&isUser=<%=isUser%>&userID=<%=iUserID%>')"></td>
																	</tr>
																<%rsCert.movenext
																if blnChange = true then
																	blnChange = false
																else
																	blnChange = true
																end if
																loop%>
															<%end if%>
														</table>
													</td>
												</tr>				
												<tr><td colspan="3" height="45"></td></tr>
												<form name="editUserCerts" method="post" action="process.asp">
												<tr>
													<td colspan="3">
														<strong><u>Add New Certification</u></strong>
													</td>
												</tr>
												<tr>
													<td align="right"><strong>Certification:</strong></td>
													<td width=5></td>
													<td>
														<input type="text" name="certification" size="50" value="" maxlength="150">
													</td>
												</tr>
												<tr><td colspan="3" height="5"></td></tr>
												<tr>
													<td align="right"><strong>Certification Number:</strong></td>
													<td width=5></td>
													<td>
														<input type="text" name="certificationNumber" size="50" value="" maxlength="150">
													</td>
												</tr>
												<tr><td colspan="3" height="5"></td></tr>
												<tr>
													<td align="right"><strong>Date passed:</strong></td>
													<td width=5></td>
													<td>
														<input type="text" name="datePassed" maxlength="10" size="10" value=""/>&nbsp;<a href="javascript:displayDatePicker('datePassed')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
													</td>
												</tr>
												<tr><td colspan="3" height="5"></td></tr>
												<tr>
													<td align="right"><strong>Expiration Date:</strong></td>
													<td width=5></td>
													<td>
														<input type="text" name="expirationDate" maxlength="10" size="10" value=""/>&nbsp;<a href="javascript:displayDatePicker('expirationDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
													</td>
												</tr>
												<tr><td colspan="3" height="10"></td></tr>
												<tr>
													<td colspan="3" align="right">
														<input type="hidden" name="isUser" value="<%=isUser%>" />
														<input type="hidden" name="userID" value="<%=iUserID%>" />
														<input type="hidden" name="processType" value="addCertification" />
														<input type="submit" value="  Save  " class="formButton"/>
													</td>
												</tr>
											</table>
											</form>
											
										<%case "docs"%>
											<%
												'get the settings of the boxes
												Ffile=request.querystring("fileoptions")
												if Ffile="" then Ffile=request.cookies("fileoptions")
													if Ffile="" then 
														response.cookies("fileoptions")="max" 
													else 
														response.cookies("fileoptions")=Ffile 
													end if
												Ffile=request.cookies("fileoptions")
											%>
											
											
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr bgcolor="#FFFFFF"><td colspan="9"><img src="images/pix.gif" width="1" height="10"></td></tr>
											<tr align="left" valign="top"> 
											  <td width="160">
											  
											  <% if Ffile="min" then %>
												<table class="borderTable" width="155" border="0" align="center" cellpadding="0" cellspacing="0">
												  <tr bgcolor="#295DCE"> 
													<td width="10" bgcolor="#295DCE"> <div align="center"><font color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif"><strong> 
														</strong></font></div></td>
													<td bgcolor="#295DCE"><font color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif"><strong>Files 
													  / Folders</strong></font></td>
													<td width="33"> <div align="right"><font color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif"></font><a href="form.asp?formType=editUser&fileoptions=max&action=viewfolder&path=<%=request.querystring("path")%>&userID=<%=iUserID%>&sec=docs"><img src="images/arrows_down.gif" width="21" height="22" border="0"></a></div></td>
												  </tr>
												</table>
												<% else %>
											  
												<table class="borderTable" width="155" border="0" align="center" cellpadding="0" cellspacing="0">
												  <tr bgcolor="#295DCE"> 
													<td width="10" bgcolor="#295DCE"> 
													  <div align="center"><font color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif"><strong> 
														</strong></font></div></td>
													<td bgcolor="#295DCE"><font color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif"><strong>Files 
													  / Folders</strong></font></td>
													<td width="33"> <div align="right"><font color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif"></font><a href="form.asp?formType=editUser&fileoptions=min&action=viewfolder&path=<%=request.querystring("path")%>&userID=<%=iUserID%>&sec=docs"><img src="images/arrows_up.gif"  border="0"></a></div></td>
												  </tr>
												  <tr bgcolor="#EFEFEF"> 
													<td colspan="3">
													  <br><table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr> 
														  <td colspan="2">
															<a class="fileLink" href="form.asp?formType=editUser&userID=<%=iUserID%>&sec=docs">&nbsp;Document Manager Home</a>
														</td>
														</tr>
													  </table><br>
													  <table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr> 
														  <td width="24%"><div align="center"><img src="images/expl_newfile.gif" width="26" height="21"></div></td>
														  <td width="76%">
															<a class="fileLink" href="form.asp?formType=editUser&action=newfile&path=<%=request.querystring("path")%>&userID=<%=iUserID%>&sec=docs">Create file</a>
														  </td>
														</tr>
													  </table>
													  <table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr> 
														  <td width="24%"><div align="center"><img src="images/nfolder.gif" width="25" height="16"></div></td>
														  <td width="76%">
																<a class="fileLink" href="form.asp?formType=editUser&action=CreateNewFolder&path=<%=request.querystring("path")%>&userID=<%=iUserID%>&sec=docs">Create folder</a>
														  </td>
														</tr>
													  </table>
													  <br>
													  <table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr> 
														  <td width="24%"><div align="center"><img src="images/upload.gif" ></div></td>
														  <td width="76%">
																<a class="fileLink" href="form.asp?formType=editUser&action=UploadFiles&path=<%=request.querystring("path")%>&userID=<%=iUserID%>&sec=docs">Upload file(s)</a>
														  </td>
														</tr>
													  </table>
													  <br> 
													 </td>
												  </tr>
												</table> 
												<% end if %><br>
												
											  </td>
											  <td width="5">&nbsp;</td>
											  <td width="80%" height="350" bgcolor="#FFFFFF"> 
												<!--#include file="filemanageruser.asp"-->
											  </td>
											</tr>
										  </table>			
											
											
										<%end select%>
								
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
<!--<script language="JavaScript" type="text/javascript">

  var frmvalidator  = new Validator("editUser");
  frmvalidator.addValidation("firstName","req","Please enter your first name");
  frmvalidator.addValidation("lastName","req","Please enter your last name"); 
  frmvalidator.addValidation("jobtitle","req","Please enter your job title");
  frmvalidator.addValidation("email","req","Please enter your email address");
  frmvalidator.addValidation("email","email");
  frmvalidator.addValidation("officePhone","req","Please enter your office phone number");
  frmvalidator.addValidation("cellPhone","req","Please enter your cell phone number");
  frmvalidator.addValidation("company","req","Please enter your company's name");  
  frmvalidator.addValidation("userName","req","Please enter a user name");
  frmvalidator.addValidation("password","req","Please enter a password");
</script>-->

<%
rsUser.close
set rsUser = nothing
rsState.close
rsCountry.close
set rsState = nothing
set rsCountry = nothing
DataConn.close
set DataConn = nothing
%>