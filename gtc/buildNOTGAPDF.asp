<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
NOTID = request("NOTID")

Session("LoggedIn") = "True"

Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetNOT"
    .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, NOTID) 'NOTID
   .CommandType = adCmdStoredProc
   
End With
			
Set rsNOI = oCmd.Execute
Set oCmd = nothing
%>
<html>
<head>
<title>SWSIR</title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/report.css" type="text/css">
</head>
<body>

<table width=650 cellpadding=0 cellspacing=0>
	<tr>
		<td align="right">
			<table width="130" border="0" cellpadding="2" cellspacing="1" style="border-color:#000000;border-style:solid;border-width:thin;">
				<tr><td height="10">&nbsp;</td></tr>
			</table>
			For Official Use Only
		</td>
	</tr>
	<tr>
		<td align="center">
			<strong>NOTICE OF TERMINATION<br><br>

			VERSION 2008<br><br>
			
			State of Georgia<br>
			Department of Natural Resources<br>
			Environmental Protection Division<br><br>
			
			To Cease Coverage Under the NPDES General Permits<br>
			To Discharge Storm Water Associated With Construction Activity<br>

		</td>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=20></td></tr>
	<tr>
		<td>
			<strong>I.	PERMIT TYPE (Check Only One):</strong><br><br>
			<%
			select case rsNOI("coverageDesired")
				Case 1%>
					<img src="images/checked.gif">&nbsp;GAR100001 - Stand Alone<br><img src="images/checkbox.gif">&nbsp;GAR100002 - Infrastructure<br><img src="images/checkbox.gif">&nbsp;GAR100003 - Common Development
				<%case 2%>
					<img src="images/checkbox.gif">&nbsp;GAR100001 - Stand Alone<br><img src="images/checked.gif">&nbsp;GAR100002 - Infrastructure<br><img src="images/checkbox.gif">&nbsp;GAR100003 - Common Development
				<%case 3%>
					<img src="images/checkbox.gif">&nbsp;GAR100001 - Stand Alone<br><img src="images/checkbox.gif">&nbsp;GAR100002 - Infrastructure<br><img src="images/checked.gif">&nbsp;GAR100003 - Common Development
			<%end select%>
		</td>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=15></td></tr>
	<tr>
		<td>
			<strong>II.	SITE / PERMITTEE INFORMATION</strong><br><br>
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td colspan="2">Project Construction Site Name:&nbsp;<u><%=rsNOI("siteProjectName")%></u></td>
				</tr>
				<tr><td colspan="2" height="5"></td></tr>
				<tr>
					<td colspan="2">GPS Location of Construction Exit (<em>degrees/minutes/seconds</em>):</td>
				</tr>
				<tr><td colspan="2" height="5"></td></tr>
				<tr>
					<td colspan="2" align="center">Latitude <u><%=rsNOI("GPSDegree1")%></u>&deg; <u><%=rsNOI("GPSMinute1")%></u>' <u><%=rsNOI("GPSSecond1")%></u>&quot;&nbsp;&nbsp;&nbsp;Longitude <u><%=rsNOI("GPSDegree2")%></u>&deg; <u><%=rsNOI("GPSMinute2")%></u>' <u><%=rsNOI("GPSSecond2")%></u>&quot; </td>
				</tr>
				<tr><td colspan="2" height="5"></td></tr>
				<tr>
					<td colspan="2">Construction Site Location (<em>information must be sufficient to accurately locate the construction site</em>):<br><u><%=rsNOI("projectAddress")%></u></td>
				</tr>
				<tr><td colspan="2" height="5"></td></tr>
				<tr>
					<td colspan="2">Subdivision Name (<em>if applicable</em>):&nbsp;<u><%=rsNOI("subdivision")%></u>&nbsp;&nbsp;&nbsp;Lot Number(s) (<em>if applicable</em>):&nbsp;<u><%=rsNOI("lotNumber")%></u></td>
				</tr>
				<tr><td colspan="2" height="5"></td></tr>
				<tr>
					<td colspan="2">Common Development Name (<em>applicable only to General NPDES Permit No. GAR100003</em>):<br><u><%=rsNOI("commonDevelopmentName")%></u></td>
				</tr>
				<tr><td colspan="2" height="5"></td></tr>
				<tr>
					<td colspan="2">Construction Site Street Address:&nbsp;<u><%=rsNOI("constructionStreetAddress")%></u></td>
				</tr>
				<tr><td colspan="2" height="5"></td></tr>				
				<tr>
					<td colspan="2">City (<em>applicable if the site is located within the jurisdictional boundaries of the city</em>):&nbsp;<u><%=rsNOI("projectCity")%></u></td>
				</tr>
				<tr><td colspan="2" height="5"></td></tr>
				<tr>
					<td colspan="2">County:&nbsp;<u><%=rsNOI("projectCounty")%></u></td>
				</tr>

				<tr><td colspan="2" height="5"></td></tr>
				<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td>
									Owner�s Name:&nbsp;<u><%=rsNOI("ownerName")%></u>
								</td>
								<td></td>
								<td>
									<!--Phone:&nbsp;<u><%'=rsNOI("ownerPhone")%></u>-->
								</td>
							</tr>
							<tr><td colspan="3" height="5"></td></tr>
							<tr>
								<td>
									Address:&nbsp;<u><%=rsNOI("ownerAddress")%></u>&nbsp;&nbsp;City:&nbsp;<u><%=rsNOI("ownerCity")%></u>
								</td>
								<td></td>
								<td>
									State:&nbsp;<u><%=rsNOI("ownerState")%></u>&nbsp;&nbsp;Zip Code:&nbsp;<u><%=rsNOI("ownerZip")%></u>
								</td>
							</tr>
							<tr><td colspan="3" height="5"></td></tr>
							<tr>
								<td>
									Duly Authorized Representative (<em>optional</em>):&nbsp;<u><%=rsNOI("authorizedRep")%></u>
								</td>
								<td></td>
								<td>
									Phone:&nbsp;<u><%=rsNOI("authorizedRepPhone")%></u>
								</td>
							</tr>
							<tr><td colspan="3" height="5"></td></tr>
							<tr>
								<td>
									Operator�s Name (<em>optional</em>):&nbsp;<u><%=rsNOI("operatorName")%></u>
								</td>
								<td></td>
								<td>
									Phone:&nbsp;<u><%=rsNOI("operatorPhone")%></u>
								</td>
							</tr>
							<tr><td colspan="3" height="5"></td></tr>
							<tr>
								<td>
									Address:&nbsp;<u><%=rsNOI("operatorAddress")%></u>&nbsp;&nbsp;City:&nbsp;<u><%=rsNOI("operatorCity")%></u>
								</td>
								<td></td>
								<td>
									State:&nbsp;<u><%=rsNOI("operatorState")%></u>&nbsp;&nbsp;Zip Code:&nbsp;<u><%=rsNOI("operatorZip")%></u>
								</td>
							</tr>
							<tr><td colspan="3" height="5"></td></tr>
							<tr>
								<td>
									Facility/Construction Site Contact:&nbsp;<u><%=rsNOI("facilityContact")%></u>
								</td>
								<td></td>
								<td>
									Phone:&nbsp;<u><%=rsNOI("facilityContactPhone")%></u>
								</td>
							</tr>
						</table>
					
					</td>
				</tr>
	

				
			</table>
		</td>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=15></td></tr>
	<tr>
		<td>
			<strong>TYPE OF PERMITTEE (<em>Check Only One and Complete</em>):</strong><br><br>
			<table width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td colspan="3">
						<%typePrimary = checkNullValue(rsNOI("typePrimary"))%>
						<%if typePrimary = "True" then%>
							<img src="images/checked.gif">
						<%else%>
							<img src="images/checkbox.gif">
						<%end if%>&nbsp;<em><strong>Primary Permittee</strong></em>
					</td>
				</tr>
				<tr><td colspan="3" height="5"></td></tr>
				<tr>
					<td>
						Primary Permittee's Name:&nbsp;<u><%=rsNOI("primaryPermitteeName")%></u>
					</td>
					<td></td>
					<td>
						 Phone:&nbsp;<u><%=rsNOI("primaryPermitteePhone")%></u>
					</td>
				</tr>
				<tr><td colspan="3" height="5"></td></tr>
				<tr>
					<td>
						Address:&nbsp;<u><%=rsNOI("primaryPermitteeAddress")%></u>&nbsp;&nbsp;City:&nbsp;<u><%=rsNOI("primaryPermitteeCity")%></u>
					</td>
					<td></td>
					<td>
						State:&nbsp;<u><%=rsNOI("primaryPermitteeState")%></u>&nbsp;&nbsp;Zip Code:&nbsp;<u><%=rsNOI("primaryPermitteeZip")%></u>
					</td>
				</tr>
				<tr><td colspan="3" height="10"></td></tr>
				<tr>
					<td colspan="3">
						<%attach1 = checkNullValue(rsNOI("attach1"))%>
						<%if attach1 = "True" then%>
							<img src="images/checked.gif">
						<%else%>
							<img src="images/checkbox.gif">
						<%end if%>&nbsp;<strong>Attached to this Notice of Termination</strong> - Listing of the legal name, complete address <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;and  telephone number for each secondary permittee at the 
						site for which this NOT <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;is submitted (applicable only to General NPDES Permit No. GAR100003).

					</td>
				</tr>
				<tr><td colspan="3" height="10"></td></tr>
				<tr>
					<td colspan="3">
						<%attach2 = checkNullValue(rsNOI("attach2"))%>
						<%if attach2 = "True" then%>
							<img src="images/checked.gif">
						<%else%>
							<img src="images/checkbox.gif">
						<%end if%>&nbsp;<strong>Attached to this Notice of Termination</strong> - Listing of the legal name, complete address and 
						telephone number for the legal title holders for each remaining undeveloped lot(s) at the site
						for which this NOT is submitted (applicable only to General NPDES Permit No. GAR100003).
					</td>
				</tr>
				<tr><td colspan="3" height="10"></td></tr>
				<tr>
					<td colspan="3">
						<%typeSecondary = checkNullValue(rsNOI("typeSecondary"))%>
						<%if typeSecondary = "True" then%>
							<img src="images/checked.gif">
						<%else%>
							<img src="images/checkbox.gif">
						<%end if%>&nbsp;<em><strong>Secondary Permittee</strong>  (applicable only to General NPDES Permit No. GAR100003)</em>
					</td>
				</tr>
				<tr><td colspan="3" height="5"></td></tr>
				<tr>
					<td>
						Primary Permittee's Name:&nbsp;<u><%=rsNOI("primaryPermitteeName2")%></u>
					</td>
					<td></td>
					<td>
						 Phone:&nbsp;<u><%=rsNOI("primaryPermitteePhone2")%></u>
					</td>
				</tr>
				<tr><td colspan="3" height="5"></td></tr>
				<tr>
					<td>
						Address:&nbsp;<u><%=rsNOI("primaryPermitteeAddress2")%></u>&nbsp;&nbsp;City:&nbsp;<u><%=rsNOI("primaryPermitteeCity2")%></u>
					</td>
					<td></td>
					<td>
						State:&nbsp;<u><%=rsNOI("primaryPermitteeState2")%></u>&nbsp;&nbsp;Zip Code:&nbsp;<u><%=rsNOI("primaryPermitteeZip2")%></u>
					</td>
				</tr>
				<tr><td colspan="3" height="10"></td></tr>
				<tr>
					<td colspan="3">
						<%typeTertiary = checkNullValue(rsNOI("typeTertiary"))%>
						<%if typeTertiary = "True" then%>
							<img src="images/checked.gif">
						<%else%>
							<img src="images/checkbox.gif">
						<%end if%>&nbsp;<em><strong>Tertiary Permittee</strong>  (applicable only to General NPDES Permit No. GAR100003)</em>
					</td>
				</tr>
				<tr><td colspan="3" height="5"></td></tr>
				<tr>
					<td>
						Primary Permittee's Name:&nbsp;<u><%=rsNOI("primaryPermitteeName3")%></u>
					</td>
					<td></td>
					<td>
						 Phone:&nbsp;<u><%=rsNOI("primaryPermitteePhone3")%></u>
					</td>
				</tr>
				<tr><td colspan="3" height="5"></td></tr>
				<tr>
					<td>
						Address:&nbsp;<u><%=rsNOI("primaryPermitteeAddress3")%></u>&nbsp;&nbsp;City:&nbsp;<u><%=rsNOI("primaryPermitteeCity3")%></u>
					</td>
					<td></td>
					<td>
						State:&nbsp;<u><%=rsNOI("primaryPermitteeState3")%></u>&nbsp;&nbsp;Zip Code:&nbsp;<u><%=rsNOI("primaryPermitteeZip3")%></u>
					</td>
				</tr>
				<tr><td colspan="3" height="10"></td></tr>
			</table>
		</td>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=15></td></tr>
	<tr>
		<td>
			<strong>III.	SITE ACTIVITY INFORMATION</strong><br><br>
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td>Start Date <em>(month/day/year)</em>:&nbsp;<u><%=rsNOI("startDate")%></u></td>
					<td width="50"></td>
					<td>Completion Date <em>(month/day/year)</em>:&nbsp;<u><%=rsNOI("completionDate")%></u></td>
				</tr>
				<tr><td colspan="3" height="5"></td></tr>
				<tr>
					<td colspan="3">Disturbed Acreage:&nbsp;<u><%=rsNOI("estimatedDisturbedAcerage")%></u></td>
				</tr>
				<tr><td colspan="2" height="5"></td></tr>
				<tr>
					<td colspan="3">
						<em>Check Only One</em>:<br>
						<%constructionActivityCompleted = checkNullValue(rsNOI("constructionActivityCompleted"))%>
						<%if constructionActivityCompleted = "True" then%>
							<img src="images/checked.gif">
						<%else%>
							<img src="images/checkbox.gif">
						<%end if%>&nbsp;Construction Activity Ceased and Final Stabilization Completed<br>
						
						<%noLongerOwnerOperator = checkNullValue(rsNOI("noLongerOwnerOperator"))%>
						<%if noLongerOwnerOperator = "True" then%>
							<img src="images/checked.gif">
						<%else%>
							<img src="images/checkbox.gif">
						<%end if%>&nbsp;No Longer Owner and/or Operator of Facility/Construction Site
						
					</td>
				</tr>
				<tr><td colspan="2" height="15"></td></tr>
				<tr>
					<td valign="top">Construction Activity Type:</td>
					<td colspan="2">
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>
									<%if rsNOI("typeConstructionCommercial") = "True" then%>
										<img src="images/checked.gif">&nbsp;Commercial
									<%else%>
										<img src="images/checkbox.gif">&nbsp;Commercial
									<%end if%>									
								</td>
								<td width="40"></td>
								<td>
									<%if rsNOI("typeConstructionIndustrial") = "True" then%>
										<img src="images/checked.gif">&nbsp;Industrial
									<%else%>
										<img src="images/checkbox.gif">&nbsp;Industrial
									<%end if%>									
								</td>
								<td width="40"></td>
								<td>
									<%if rsNOI("typeConstructionMunicipal") = "True" then%>
										<img src="images/checked.gif">&nbsp;Municipal
									<%else%>
										<img src="images/checkbox.gif">&nbsp;Municipal
									<%end if%>										
								</td>
							</tr>							
							<tr>
								<td>
									<%if rsNOI("typeConstructionUtility") = "True" then%>
										<img src="images/checked.gif">&nbsp;Utility
									<%else%>
										<img src="images/checkbox.gif">&nbsp;Utility
									<%end if%>									
								</td>
								<td></td>
								<td colspan="3">									
									<%if rsNOI("typeConstructionResidential") = "True" then%>
										<img src="images/checked.gif">&nbsp;Residential
									<%else%>
										<img src="images/checkbox.gif">&nbsp;Residential
									<%end if%>	
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="2" height="15"></td></tr>
				<tr>
					<td colspan="4">Name of Initial Receiving Water(s):&nbsp;<u><%=rsNOI("IRWName")%></u></td>
				</tr>
				<tr><td colspan="2" height="5"></td></tr>
				<tr>
					<td colspan="4">Name of MS4 Owner/Operator (<em>if applicable</em>):&nbsp;<u><%=rsNOI("MSSSOwnerOperator")%></u></td>
				</tr>
				<tr><td colspan="2" height="5"></td></tr>
				<tr>
					<td colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Name of Receiving Water(s):&nbsp;<u><%=rsNOI("RWName")%></u></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=15></td></tr>
	<tr>
		<td>
			<strong>IV.	CERTIFICATIONS (Owner or Operator or Both to Initial as Applicable)</strong><br><br>
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td valign="top">
						<%if rsNOI("certify1") <> "" then%>
							<u><%=rsNOI("certify1")%></u>
						<%else%>
							___
						<%end if%>	
					</td>
					<td width="10"></td>
					<td valign="top">
						I certify under penalty of law that either: (a) all storm water discharges associated with construction activity from the portion of the construction activity where 
						I was an Owner or Operator have ceased or have been eliminated; (b) all storm water discharges associated with construction activity from the identified site that are 
						authorized by General NPDES Permit number indicated in Section I of this form have ceased; (c) I am no longer an Owner or Operator at the construction site and a new 
						Owner or Operator has assumed operational control for those portions of the construction site where I previously had ownership or operational control; and/or if I am 
						a primary permittee filing this Notice of Termination under Part VI.A.2. of General NPDES Permit No. GAR100003, I will notify by written correspondence with return 
						receipt certified mail (or similar service) to the subsequent legal title holder of each remaining lot(s) that these lot Owners or Operators will become tertiary 
						permittees for purposes of General NPDES Permit No. GAR100003.  I understand that by submitting this Notice of Termination, that I am no longer authorized to 
						discharge storm water associated with construction activity by the general permit, and that discharging pollutants in storm water associated with construction 
						activity to waters of Georgia is unlawful under the Georgia Water Quality Control Act and the Clean Water Act where the discharge is not authorized by a NPDES 
						permit.
					</td>
				</tr>
				<tr><td colspan="4" height="10"></td></tr>
				<tr>
					<td valign="top">
						<%if rsNOI("certify2") <> "" then%>
							<u><%=rsNOI("certify2")%></u>
						<%else%>
							___
						<%end if%>	
					</td>
					<td width="10"></td>
					<td valign="top">
						 I certify under penalty of law that this document and all attachments were prepared under my direction or supervision in accordance with a system designed to 
						 assure that certified personnel properly gather and evaluate the information submitted.  Based upon my inquiry of the person or persons who manage the system, 
						 or those persons directly responsible for gathering the information, the information submitted is, to the best of my knowledge and belief, true, accurate and 
						 complete.  I am aware that there are significant penalties for submitting false information, including the possibility of fines and imprisonment for knowing 
						 violations.
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=15></td></tr>
	<tr>
		<td>
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td>Owner�s Printed Name:&nbsp;<u><%=rsNOI("ownerPrintedName")%></u></td>
					<td width="100"></td>
					<td>Title:&nbsp;<u><%=rsNOI("ownerTitle")%></u></td>
				</tr>
				<tr><td colspan="3" height="10"></td></tr>
				<tr>
					<td>Signature:&nbsp;__________________</td>
					<td></td>
					<td>Date:&nbsp;<u><%=rsNOI("ownerSignDate")%></u></td>
				</tr>
				<tr><td colspan="3" height="15"></td></tr>
				<tr>
					<td>Operator�s Printed Name:&nbsp;<u><%=rsNOI("operatorPrintedName")%></u></td>
					<td></td>
					<td>Title:&nbsp;<u><%=rsNOI("operatorTitle")%></u></td>
				</tr>
				<tr><td colspan="3" height="10"></td></tr>
				<tr>
					<td>Signature:&nbsp;__________________</td>
					<td></td>
					<td>Date:&nbsp;<u><%=rsNOI("operatorSignDate")%></u></td>
				</tr>
			</table>
			   								  			
		</td>
	</tr>
</table>
</body>
</html>