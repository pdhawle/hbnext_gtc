<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
clientID = request("clientID")
dtFrom = request("fromDate")
dtTo = request("toDate")


If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")
	
'Create command
if clientID = 1 then
Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetNewStartsByDateSpecial"
   .parameters.Append .CreateParameter("@fromDate", adDBTimeStamp, adParamInput, 8, dtFrom)
   .parameters.Append .CreateParameter("@toDate", adDBTimeStamp, adParamInput, 8, dtTo)
   '.parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
   .CommandType = adCmdStoredProc   
End With
else
Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetNewStartsByDate"
   .parameters.Append .CreateParameter("@fromDate", adDBTimeStamp, adParamInput, 8, dtFrom)
   .parameters.Append .CreateParameter("@toDate", adDBTimeStamp, adParamInput, 8, dtTo)
   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
   .CommandType = adCmdStoredProc   
End With
end if
			
Set rsReport = oCmd.Execute
Set oCmd = nothing



Response.ContentType = "application/vnd.ms-excel"
Response.AddHeader "Content-Disposition", "attachment; filename=NewStartReport.xls" 
if rsReport.eof <> true then%>
		<table border=1>
			<tr><td colspan="5"><strong>Date Range: <%=dtFrom%> - <%=dtTo%></strong></td></tr>
			<tr>
				<td><strong>Customer</strong></td>
				<td><strong>Project</strong></td>
				<td><strong>Account Manager</strong></td>
				<td><strong>StartDate</strong></td>
				<td><strong>Primary Inspector</strong></td>
				<td><strong>Monthly Rate</strong></td>
				<td><strong>Activation Fee</strong></td>
				<td><strong>Billing Days</strong></td>
				<td><strong>Prorated Amount</strong></td>
				<td><strong>Total Billed</strong></td>
			</tr>
	<%dim i
	i = 0
	irate = 0
	iFee = 0
	do until rsReport.eof%>
		<tr>
			<td><%=rsReport("customerName")%></td>
			<td><%=rsReport("projectName")%></td>
			<td><%=rsReport("fName") & " " & rsReport("lName")%></td>
			<td><%=rsReport("startDate")%></td>
			<td><%=rsReport("firstName") & " " & rsReport("lastName")%></td>
			<td>
				<%
				If not isnull(rsReport("billRate")) then
					irate = irate + rsReport("billRate")
				end if 
				response.Write formatcurrency(rsReport("billRate"),2)%>
			</td>
			<td>
				<%
				If not isnull(rsReport("activationFee")) then
					iFee = iFee + rsReport("activationFee")
				end if 
				response.Write formatcurrency(rsReport("activationFee"),2)%>
			</td>
			<td><%=rsReport("newStartBillingDays")%></td>
			<td>
				<%'calculate the amount
				iProRate = (rsReport("billRate")/21)*rsReport("newStartBillingDays")
				response.Write formatcurrency(iProRate,2)
				If not isnull(iProRate) then
					iTotProRate = iTotProRate + iProRate
				end if
				
				%>
			</td>
			<td>
				<%iBilled = iProRate + rsReport("activationFee") + rsReport("billRate")
				response.Write formatcurrency(iBilled,2)
				If not isnull(iBilled) then
					iBilledTotal = iBilledTotal + iBilled
				end if
				%>
			</td>
		</tr>
	<%
	rsReport.movenext
	i = i + 1
	loop%>
		<tr>
			<td colspan="5" align="right"><strong>Totals:</strong>&nbsp;&nbsp;</td>
			<td><%=formatcurrency(irate,2)%></td>
			<td><%=formatcurrency(iFee,2)%></td>
			<td>&nbsp;</td>
			<td><%=formatcurrency(iTotProRate,2)%></td>
			<td><%=formatcurrency(iBilledTotal,2)%></td>
		</tr>
	</table>
<%end if

rsReport.close
DataConn.close
Set rsCustomer = nothing
Set rsReport = nothing
Set DataConn = nothing
Set oCmd = nothing%>

