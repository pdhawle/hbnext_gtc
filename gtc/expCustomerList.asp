<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
clientID = request("clientID")
If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")
	
'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetAllCustomersByClient"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, clientID)
   .CommandType = adCmdStoredProc   
End With
			
Set rs = oCmd.Execute
Set oCmd = nothing


Response.ContentType = "application/vnd.ms-excel"
Response.AddHeader "Content-Disposition", "attachment; filename=reconciliationReport.xls" %>
<table border=1>
	<tr>
		<td><strong>Customer Name</strong></td>
		<td><strong>Address 1</strong></td>
		<td><strong>Address 2</strong></td>
		<td><strong>City</strong></td>
		<td><strong>State</strong></td>
		<td><strong>Zip</strong></td>
		<td><strong>County</strong></td>
	</tr>
<%
do until rs.eof%>
	<tr>
		<td><%=rs("customerName")%></td>
		<td><%=rs("address1")%></td>
		<td><%=rs("address2")%></td>
		<td><%=rs("city")%></td>
		<td><%=rs("state")%></td>
		<td><%=rs("zip")%></td>
		<td><%=rs("county")%></td>
	</tr>
<%
rs.movenext
loop%>
</table>
<%

rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing%>

