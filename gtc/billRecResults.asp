<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
clientID = request("clientID")
smonth = request("month")
syear = request("year")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

if clientID = 1 then 'get NEXT and McBride Accounts. this is a special circumstance
	'get a list of the customers based on the clientID
	Set oCmd = Server.CreateObject("ADODB.Command")
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetCustomersByClientSpecial"
	   '.parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
	   .CommandType = adCmdStoredProc   
	End With
else
	'get a list of the customers based on the clientID
	Set oCmd = Server.CreateObject("ADODB.Command")
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetCustomersByClient"
	   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
	   .CommandType = adCmdStoredProc   
	End With
end if
Set rsReport = oCmd.Execute
Set oCmd = nothing


%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script type="text/javascript">
    var GB_ROOT_DIR = "<%=sPDFPath%>/greybox/";
</script>
<script type="text/javascript" src="greybox/AJS.js"></script>
<script type="text/javascript" src="greybox/AJS_fx.js"></script>
<script type="text/javascript" src="greybox/gb_scripts.js"></script>
<link href="greybox/gb_styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Billing Reconciliation Report</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<a href="expBillRecResults.asp?clientID=<%=clientID%>" class="footerLink">Export Data to Excel</a>&nbsp;&nbsp;
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="adminReports.asp" class="footerLink">back to admin reports</a>			
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td></td>
											<td colspan="6">
												<!--Inspector(s): <%'=inspector%><br>
												Project(s): <%'=iProjectID%><br>-->
												<strong><%'=getMonth(smonth) & " " & sYear%></strong><br>
											</td>
										</tr>										
										<tr><td colspan="7"><img src="images/pix.gif" width="1" height="20"></td></tr>
										<tr>
											<td colspan="7">
												<table width="100%" cellpadding="0" cellspacing="0">
													<tr><td colspan="8">* Last Inspection date is only listed if the date is more than 10 days old.</td></tr>
													<tr bgcolor="#666666">
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td><span class="searchText">Customer</span></td>
														<td><span class="searchText">Project</span></td>
														<td><span class="searchText">Bill Rate</span></td>
														<td><span class="searchText">1st Inspection Date</span></td>
														<td><span class="searchText">*Last Inspection Date</span></td>
														<td><span class="searchText">Project End Date</span></td>
														<td><span class="searchText">Active</span></td>
													</tr>
													<%if rsReport.eof then%>
														<tr><td></td><td colspan="8">There are no result for the items you chose. Please select again.</td></tr>
													<%else
													iBillTotal = 0
													do until rsReport.eof%>
															<tr class="rowColor">
																<td></td>
																<td colspan="8"><b><%=rsReport("customerName")%></b>&nbsp;-&nbsp;<a href="billingComments.asp?customerID=<%=rsReport("customerID")%>" title="Billing Comments for <%=rsReport("customerName")%>" rel="gb_page_center[820, 500]">billing comments</a></td>
															</tr>
															<tr class="rowColor">
																<td></td>
																<td colspan="8">
																	<%if rsReport("advanceBilling") = "True" then%>
																		<em>***Billing in Advance***</em><br>
																	<%else%>
																		<font color="#EE0000"><em>***Billing in Arrears***</em></font><br>
																	<%end if%>
																	<%if rsReport("billName") <> "" then%>
																		<%=rsReport("billName")%><br>
																	<%end if%>
																	<%if rsReport("billEmail") <> "" then%>
																		<a href="mailto:<%=rsReport("billEmail")%>"><%=rsReport("billEmail")%></a><br>
																	<%end if%>
																	<%if rsReport("billPhone") <> "" then%>
																		<%=rsReport("billPhone")%><br>
																	<%end if%>
																	<%if rsReport("billStreet") <> "" then%>
																		<%=rsReport("billStreet")%><br>
																	<%end if%>
																	<%if rsReport("billSuite") <> "" then%>
																		Suite&nbsp;<%=rsReport("billSuite")%><br>
																	<%end if%>
																	<%if rsReport("billCity") <> "" then%>
																		<%=rsReport("billCity")%>,&nbsp;<%=rsReport("billState")%>&nbsp;<%=rsReport("billZip")%><br>
																	<%end if%>																	
																	<%if rsReport("billMethodFax") = "True" then%>
																		<strong>Fax</strong><br>
																	<%end if%>
																	<%if rsReport("billMethodEmail") = "True" then%>
																		<strong>Email</strong><br>
																	<%end if%>
																	<%if rsReport("billMethodMail") = "True" then%>
																		<strong>Mail</strong><br>
																	<%end if%>
																	<%if rsReport("acctManager") <> "" then%>
																		<strong>Acct Mgr:</strong> <%=rsReport("acctManager")%><br>
																	<%end if%>
																</td>
															</tr>
															<%
															'get the projects associated with this customer
															Set oCmd = Server.CreateObject("ADODB.Command")
															With oCmd
															   .ActiveConnection = DataConn
															   .CommandText = "spGetProjectandDivisionByCustomer"
															   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, rsReport("customerID"))
															   .CommandType = adCmdStoredProc   
															End With
															
															Set rsProject = oCmd.Execute
															Set oCmd = nothing
															
															
															if rsProject.eof then%>
																<tr class="reportRowColor"><td colspan="2"></td><td colspan="8">There are no projects listed for this customer</td></tr>
															<%else
																do until rsProject.eof%>
																	<tr class="reportRowColor">
																		<td colspan="2"></td>
																		<td><%=rsProject("projectName")%></td>
																		<td>
																			<%
																			
																			if rsProject("endDate") <> "" then
																				'zero out the bill rate
																				iBillRate = 0
																			else
																				iBillRate = rsProject("billRate")
																			end if
																			response.Write formatCurrency(iBillRate,2)										
																			
																			if isnull(iBillRate) = false then
																				if iBillRate <> "" then
																					'if the project is inactive, don't add to the total
																					If rsProject("isActive") = "True" then
																						iBillTotal = iBillTotal + iBillRate
																					end if
																				end if
																			end if
																			
																		'	response.Write formatCurrency(rsProject("billRate"),2)
																		'		if isnull(rsProject("billRate")) = false then
																		'			if rsProject("billRate") <> "" then
																		'				'if the project is inactive, don't add to the total
																		'				If rsProject("isActive") = "True" then
																		'					iBillTotal = iBillTotal + rsProject("billRate")
																		'				end if
																		'			end if
																		'		end if
																			%>
																		</td>
																		<td><%=rsProject("firstInspectionDate")%></td>
																		<td>
																			<%If datediff("d",rsProject("lastInspectionDate"),date()) > 10 then																			
																				response.Write "<b><font color=red>* " & rsProject("lastInspectionDate") & "</font>"
																			end if
																			%>
																		</td>
																		<td><%=rsProject("endDate")%></td>
																		<td>
																			<%'=rsProject("isActive")%>
																			<%if rsProject("isActive") = "True" then%>
																				<img src="images/check.gif">
																			<%end if%>
																		</td>
																	</tr>
																<%
																rsProject.movenext
																loop
															end if%>
															
															<tr><td height="20"></td><td colspan="8"></td></tr>
															
															<%
													
													rsReport.movenext
																
													loop
													rsReport.close
													set rsReport = nothing
													end if%>
													
													<tr>
														<td colspan="2"></td>
														<td align="right"><strong>Total:</strong>&nbsp;</td>
														<td colspan="5"><%=formatcurrency(iBillTotal,2)%></td>
													</tr>
												</table>
											</td>
										</tr>
										
									</table>
															
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>
<%
'rsCustomer.close
rsReport.close
DataConn.close
Set rsCustomer = nothing
Set rsReport = nothing
Set DataConn = nothing
Set oCmd = nothing
%>