<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
Dim rs, oCmd, DataConn

projectID = Request("projectID")
divisionID  = request("divisionID")
stype = Request("type")
dtFrom = request("from")
dtTo = request("to")



'If Session("LoggedIn") <> "True" Then
'	Response.Redirect("index.asp")
'End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

'Create command

Select Case stype
	Case "Open"

		Set oCmd = Server.CreateObject("ADODB.Command")	
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetOpenItemsByProject"
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
		   .CommandType = adCmdStoredProc
		   
		End With
		
		Set rs = oCmd.Execute
		Set oCmd = nothing
		
	Case "Corrected"
		Set oCmd = Server.CreateObject("ADODB.Command")	
		if dtTo <> "" then			
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spGetClosedItemsByProjectandDate"
			   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
			   .parameters.Append .CreateParameter("@fromDate", adDBTimeStamp, adParamInput, 8, dtFrom)
			   .parameters.Append .CreateParameter("@toDate", adDBTimeStamp, adParamInput, 8, dtTo)
			   .CommandType = adCmdStoredProc
			   
			End With
			
			Set rs = oCmd.Execute
			Set oCmd = nothing
			
		else
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spGetClosedItemsByProject"
			   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
			   .CommandType = adCmdStoredProc			   
			End With
			
			Set rs = oCmd.Execute
			Set oCmd = nothing
		end if
		
	Case "Legend"
		'just print out the legend for the questions		
	
End select
	
		


Set oCmd = Server.CreateObject("ADODB.Command")	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCategoriesByDivision"
   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, divisionID)
   .CommandType = adCmdStoredProc   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsCategory = oCmd.Execute
Set oCmd = nothing


Set oCmd = Server.CreateObject("ADODB.Command")	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomerByProject"
   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
   .CommandType = adCmdStoredProc   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsInfo = oCmd.Execute
Set oCmd = nothing

%>

<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="<%=sPDFPath%>includes/main.css" type="text/css">
</head>
<body>
<table class="borderTable" width="900" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="2" valign=top><br>
			<%if rsInfo("logo") = "" then%>
				&nbsp;<img src="<%=sPDFPath%>images/logoTemp.gif">
			<%else%>
				&nbsp;<img src="<%=rsInfo("logo")%>">
			<%end if%>
		</td>
	</tr>
	<tr>
		<td width="20">&nbsp;</td>
		<td>
			<table class="borderTable" width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr><td colspan="15" height="10">&nbsp;</td></tr>
				<tr>
					<td></td><td colspan="15"><span class="grayHeader"><%=sNPDESTitle%></span><span class="Header"> - <%=stype%> Items Log</span></td>
				</tr>
				<tr><td colspan="15" height="10">&nbsp;</td></tr>
				<tr><td></td><td colspan="5">Customer: <strong><%=rsInfo("customerName")%></strong></td></tr>
				<tr><td></td><td colspan="5">Project: <strong><%=rsInfo("projectName")%></strong></td></tr>
				<tr><td colspan="15" height="3">&nbsp;</td></tr>
				<%if stype <> "Legend" then%>
					<tr bgcolor="#666666">
						<td width="5">&nbsp;</td>
						<td><span class="searchText">ReportID</span></td>
						<td width="10">&nbsp;</td>
						<td><span class="searchText">Report Date</span><br><img src="images/pix.gif" width="60" height="1"></td>
						<td width="10">&nbsp;</td>
						<td><span class="searchText">Ref#</span></td>
						<td width="10">&nbsp;</td>
						<td><span class="searchText">Corrective Action Needed</span></td>
						<td width="10">&nbsp;</td>
						<td><span class="searchText">Proposed Activity</span></td>
						<td width="10">&nbsp;</td>
						<td><span class="searchText">Location</span></td>
						<td width="10">&nbsp;</td>
						<td><span class="searchText">Date Corrected</span></td>
						<td width="10">&nbsp;</td>
						<td><span class="searchText">Corrected By</span></td>
					</tr>
					<tr><td colspan="15"></td><td height="10">&nbsp;</td></tr>
					<%If rs.eof then%>
						<tr><td></td><td colspan="15">there are no records to display</td></tr>
					<%else
						blnChange = true
						Do until rs.eof
							If blnChange = true then%>
								<tr bgcolor="#DDDDDD">
							<%else%>
								<tr>
							<%end if%>
								<td width="5">&nbsp;</td>
								<td><%=rs("reportID")%></td>
								<td></td>
								<td><%=rs("inspectionDate")%></td>
								<td></td>
								<td><%=rs("reportID") & "-" & rs("referenceNumber")%></td>
								<td></td>
								<td><%=rs("actionNeeded")%></td>
								<td></td>
								<td><%=rs("proposedActivity")%></td>
								<td></td>
								<td><%=rs("location")%></td>
								<td></td>
								<td><%=rs("addressedByDate")%></td>
								<td></td>
								<td>
									<%=rs("addressedBy")%>
								</td>
							</tr>
							<%
							if rs("closeComments") <> "" then
								If blnChange = true then%>
									<tr class="rowColor">
								<%else%>
									<tr>
								<%end if%>
									<td colspan="20">
										&nbsp;&nbsp;<strong>Comments:</strong>&nbsp;<%=trim(rs("closeComments"))%>&nbsp;&nbsp;
									</td>
								</tr>
							<%end if%>
						<%
		
						rs.movenext
						if blnChange = true then
							blnChange = false
						else
							blnChange = true
						end if
						
						
						
						loop
					end if
				
				else%>
				
				
					<tr><td width="5">&nbsp;</td><td colspan="15" height="10">&nbsp;</td></tr>
					<tr><td></td><td colspan="15">
						<strong>LEGEND/LEYENDA</strong><br>
						
						Example/Ejemplo: 1000-1-2<br>
						1000 = Report ID/Informe de identificaci�n<br>
						<strong>1 = Question/Pregunta</strong> (This corresponds to the numbers below/Esto corresponde a los siguientes n�meros)<br>
						2 = Item Number/N�mero de la partida
					</td></tr>
					<tr><td></td><td colspan="15" height="10">&nbsp;</td></tr>
					<tr><td></td><td colspan="15">
						<%'get the categories and questions
						i = 1
						Do until rsCategory.eof
							response.Write "<b>" & rsCategory("category") & "</b><br>"
							
							Set oCmd = Server.CreateObject("ADODB.Command")
		
							With oCmd
							   .ActiveConnection = DataConn
							   .CommandText = "spGetQuestion"
							   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, rsCategory("categoryID"))
							   '.parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, divisionID)
							   .CommandType = adCmdStoredProc
							   
							End With
								
								If Err Then
							%>
									<!--#include file="includes/FatalError.inc"-->
							<%
								End If
										
							Set rsQuestion = oCmd.Execute
							Set oCmd = nothing%>
							<table>
							<%do until rsQuestion.eof%>
								<tr>
									<td valign="top"><strong><%=i%></strong>&nbsp;</td>
									<td>
										<%=rsQuestion("question")%>
										
										<%if rsQuestion("secondLanguage") <> "" then
											response.Write " - " & rsQuestion("secondLanguage")
										else
											response.Write "&nbsp;"
										end if%>
									</td>
								</tr>
							<%rsQuestion.movenext
							i = i + 1
							loop%>
							</table>
							
						<%rsCategory.movenext
						response.Write "<br>"
						loop
						%>
					</td></tr>
				<%end if%>
				<tr><td></td><td colspan="15" height="10">&nbsp;</td></tr>
			</table>
		</td>
		<td width="20">&nbsp;</td>
	</tr>
	<tr><td colspan="4" height="20">&nbsp;</td></tr>
</table>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>
