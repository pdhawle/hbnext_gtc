<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->
<%

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Change Log</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td><strong>Below are the changes, updates and bug fixes.</strong></td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td>
												<strong>February 2008</strong><br><br>
												<strong>02/28/2008</strong> - Added a calendar to the My Tools section so the user can add, edit, delete and share events with other users.<br />
												<strong>02/27/2008</strong> - Added a "My Tools" section in the header to list all of the user tools.<br />
												<strong>02/18/2008</strong> - Built in admin rights for certain users so they can control own users, customers, projects, etc.<br />
												<strong>02/07/2008</strong> - Made changes to the open item alert. If a user is signed up to receive OI alers, they will receive a daily email listing projects that have open items over 7 days old.<br /><br>
											
												<strong>January 2008</strong><br><br>
												<strong>01/31/2008</strong> - Added a seven day alert system for the open items.<br />
												<strong>01/27/2008</strong> - Edited the EC Checklist to keep a running tab of checklist items. Took away edit feature. Can edit items when new report is added.<br />
												<strong>01/22/2008</strong> - Added new design to the application.<br />
												<strong>01/17/2008</strong> - Added ability to record multiple water samples on one report<br />
												<strong>01/11/2008</strong> - Updated the document manager to allow for uploads up to 2MB. Also added progress bar to upload feature.<br />
												<strong>01/10/2008</strong> - Added all U.S. counties to the database so when editing or adding a project, the state and county can be added to the project.<br />
												<strong>01/09/2008</strong> - Added custom titles to each page in the application.<br />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
									</table>
															
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>