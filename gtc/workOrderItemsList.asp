<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
Dim rs, oCmd, DataConn
'response.Write session("canApprove")
projectID = Request("projectID")
clientID = request("clientID")
workOrderID = request("workOrderID")
projName = request("projName")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If


'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetWorkOrderItems"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, workOrderID)
   .CommandType = adCmdStoredProc   
End With
			
Set rs = oCmd.Execute
Set oCmd = nothing


%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script language="javascript" src="includes/datePicker.js"></script>
<link rel="stylesheet" href="includes/datePicker.css" type="text/css">
<script language="JavaScript" src="includes/validator.js" type="text/javascript"></script>
<script type="text/javascript">
    var GB_ROOT_DIR = "<%=sPDFPath%>/greybox/";
</script>
<script type="text/javascript" src="greybox/AJS.js"></script>
<script type="text/javascript" src="greybox/AJS_fx.js"></script>
<script type="text/javascript" src="greybox/gb_scripts.js"></script>
<link href="greybox/gb_styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
<form name="workOrderList" method="post" action="process.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sNPDESTitle%></span><span class="Header"> - Work Orders Items List</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">						
						&nbsp;&nbsp;<a href="workOrdersList.asp?projectID=<%=projectID%>&clientID=<%=clientID%>" class="footerLink">work orders list</a>				
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr><td colspan="20"><strong>Project:</strong> <%=projName%></td></tr>
										<tr><td colspan="20"><strong>Note:</strong> Once item(s) are checked and approved, the work order and PDF will be generated.<br>You will not be able to come back to this screen.<br>Items are checked and dated for your convenience.</td></tr>
										<tr><td colspan="20"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr bgcolor="#666666">
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td><span class="searchText">Item ID</span></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td><span class="searchText">Action Needed</span></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td><span class="searchText">Location</span></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td><span class="searchText">Material</span></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td><span class="searchText">Qty / Price</span></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td><span class="searchText">Approve</span></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td><span class="searchText">Date Approved</span></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td><span class="searchText">Approved By</span></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td><span class="searchText">Activity/Code</span></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td><span class="searchText">Comments</span></td>
										</tr>
											<%if rs.eof then%>
												<tr><td colspan="20">&nbsp;There are no work order items for this project.</td></tr>
											<%else
												blnChange = true
												do until rs.eof
													If blnChange = true then%>
														<tr class="rowColor">
													<%else%>
														<tr>
													<%end if%>
													<td></td>
													<td><%=rs("workOrderItemID")%></td>
													<td></td>
													<%'if rs("actionNeeded") <> "" then%>
														<td><%=rs("actionNeeded")%></td>
														<td></td>
														<td><%=rs("location")%></td>
														<td></td>
													<%'else%>
														<!--<td><%'=rs("actionNeeded")%></td>
														<td></td>
														<td><%'=rs("location")%></td>
														<td></td>-->
													<%'end if%>
													<td><%=rs("material")%></td>
													<td></td>
													<td><%=rs("quantity")%> @ <%=formatcurrency(rs("cost"),2)%>/<%=rs("unit")%></td>
													<td></td>
													<td align="center">
														<input type="checkbox" name="approveWorkOrderItem" value="<%=rs("workOrderItemID")%>" checked="checked">
													</td>
													<td></td>
													<td>
														<input type="text" name="dateApproved<%=rs("workOrderItemID")%>" maxlength="10" size="8" value="<%=formatDateTime(now(),2)%>"/>&nbsp;<a href="javascript:displayDatePicker('dateApproved<%=rs("workOrderItemID")%>')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
													</td>
													<td></td>
													<td>
														<%
														'Create command for contacts
														'brings in the contacts from the project section where they are set up
														Set oCmd = Server.CreateObject("ADODB.Command")
														
														With oCmd
														   .ActiveConnection = DataConn
														   .CommandText = "spGetAssignedUsers"
															.parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
														   .CommandType = adCmdStoredProc
														   
														End With
																	
														Set rsUsers = oCmd.Execute
														Set oCmd = nothing
														%>
														<select name="approvedBy<%=rs("workOrderItemID")%>">
																<option value="<%=session("Name")%>"><%=session("Name")%></option>
															<%do until rsUsers.eof%>										
																<option value="<%=rsUsers("firstName") & " " & rsUsers("lastName")%>"><%=rsUsers("firstName") & " " & rsUsers("lastName")%></option>
															<%rsUsers.movenext
															loop%>
														</select>
													</td>
													<td></td>
													<td><input type="text" name="activityCode<%=rs("workOrderItemID")%>" size="10" maxlength="100"/></td>
													<td></td>
													<td><input type="text" name="customerComments<%=rs("workOrderItemID")%>" size="15"/></td>
												</tr>
											
												<%
												'get the values from all the checkboxes
												sAllVal = sAllVal & rs("workOrderItemID")
												
												rs.movenext
												
												if not rs.eof then
													sAllVal = sAllVal & ","
												end if
												
												if blnChange = true then
													blnChange = false
												else
													blnChange = true
												end if
												loop
												
												rs.close
												set rs = nothing%>
										<%end if%>
							
										<tr>
											<td colspan="25" align="right">
												<input type="hidden" name="projectID" value="<%=projectID%>">
												<input type="hidden" name="workOrderID" value="<%=workOrderID%>">
												<input type="hidden" name="clientID" value="<%=clientID%>">
												<input type="hidden" name="allBoxes" value="<%=sAllVal%>">
												<input type="hidden" name="processType" value="approveWorkOrderItems" />
												<br><input type="submit" value="Update" class="formButton"/>
											</td>
										</tr>
										
									</table>
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>

<%'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetWorkOrderItems"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, workOrderID)
   .CommandType = adCmdStoredProc   
End With
			
Set rs = oCmd.Execute
Set oCmd = nothing%>

<script language="JavaScript" type="text/javascript">

  var frmvalidator  = new Validator("workOrderList");
  <%do until rs.eof%>
  frmvalidator.addValidation("activityCode<%=rs("workOrderItemID")%>","req","Please enter an activity code");
  <%rs.movenext
  loop%>
</script>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>