<%
On Error Resume Next

'iCustomerID = request("customer")
iProjectID = request("project")
iDivisionID = request("division")
iReportTypeID = request("reportType")



Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")
If iReportTypeID <> "" then		
	'Create command for customer list
	'only the customers that are assigned to the user
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetAssignedDivisions"
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, session("ID"))
	   .CommandType = adCmdStoredProc
	   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
		
	Set rsDivisions = oCmd.Execute
	Set oCmd = nothing
end if

If iDivisionID <> "" then
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetAssignedProjectsByDivision"
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iDivisionID)
	   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, session("ID"))
	   .CommandType = adCmdStoredProc   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
				
	Set rsProject = oCmd.Execute
	Set oCmd = nothing


end if


If iProjectID <> "" then
	'get the number of open action items
	'Create command for question category list
'	Set oCmd = Server.CreateObject("ADODB.Command")
		
'	With oCmd
'	   .ActiveConnection = DataConn
'	   .CommandText = "spGetOpenAIByProject"
'	   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, iProjectID)
'	   .CommandType = adCmdStoredProc
'	   
'	End With
	
'	Set rsAllOpen = oCmd.Execute
'	Set oCmd = nothing
	
	
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetProject"
	   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, iProjectID)
	   .CommandType = adCmdStoredProc
	   
	End With
	
	Set rsProjInfo = oCmd.Execute
	Set oCmd = nothing

end if

%>
<script type="text/javascript">
<!--

function addEvent(ival,ques)
{
//alert (ival)
//ival = the letter
//ques = the question number
var ni = document.getElementById("myDiv"+ival);
//alert (ni)
var numi = document.getElementById("theValue"+ival);
var num = (document.getElementById("theValue"+ival).value -1)+ 2;
numi.value = num;
var divIdName = "answer"+ival+num;
var newdiv = document.createElement('div');
newdiv.setAttribute("id",divIdName);
//newdiv.innerHTML = "Element Number "+num+" has been added! <a href=\"javascript:;\" onclick=\"removeEvent(\'"+divIdName+"\')\">Remove</a>";
//<br><a href=javascript:onClick=doNew();>Upload Image</a> <input type=text name=tempImage>
//newdiv.innerHTML = "<table><tr><td>Corrective action "+ival+ "-" +num+"&nbsp;&nbsp;<a href=>select answer</a><br><input type=text name=answer_R"+ival+ "-" +num+"@"+ques+" size=40></td><td>Location"+ival+ "-" +num+"<br><input type=text name=location_"+ival+ "-" +num+" size=30></td><td valign=bottom> <a href=\"javascript:;\" onclick=\"removeEvent(\'"+divIdName+"\','"+ival+"\')\">Remove action</a></td></tr></table>";
newdiv.innerHTML = "<table><tr><td>Corrective action "+ival+ "-" +num+"&nbsp;&nbsp;<a href=javascript:displayAnswerPicker('answer_R"+ival+ "-" +num+"@"+ques+"')>select action</a><br><input type=text name=answer_R"+ival+ "-" +num+"@"+ques+" size=40></td><td>Location"+ival+ "-" +num+"<br><input type=text name=location_"+ival+ "-" +num+" size=30></td><td valign=bottom> <a href=\"javascript:;\" onclick=\"removeEvent(\'"+divIdName+"\','"+ival+"\')\">Remove action</a></td></tr></table>";

//
//newdiv.innerHTML = "Responsive action needed "+ival+num+" <input type=text name=answer_"+ival+num+"@"+ques+" size=40> <a href=\"javascript:;\" onclick=\"removeEvent(\'"+divIdName+"\','"+ival+"\')\">Remove action</a>";
//alert (newdiv.innerHTML)
ni.appendChild(newdiv);
//if no action recommended is checked, chech the add remarks button
//alert (document.getElementById("answer_&"+ques+"2").value);
//document.getElementById("answer_&"+ques+"2").checked = true;  removed this per request

//check the radio button for "add + ques +  remarks"
//if (document.addReport."answer_&34"[0].checked) {
//      document.addReport."answer_&34"[1].checked=true}
//   else {
//   if (document.addReport."answer_&34"[2].checked) {
//      document.addReport."answer_&34"[1].checked=true}
//   }



//alert (document.addReport.answer_& + ques + [0].value)
//document.addReport.answer_& + ques + ['See Corrective Action Log'].checked=true
//document.addReport.answer_ + ques + ["See Corrective Action Log"].checked = true
//document.addReport.c[0].checked=false
}

function removeEvent(divNum,ival)
{
//alert (divNum)
var d = document.getElementById("myDiv"+ival);
var olddiv = document.getElementById(divNum);
d.removeChild(olddiv);
}

function rept_onchange(addReport,repID) {
   window.location.href = 'form.asp?formType=addOSHAReport&reportType='+repID;
}

function dept_onchange(addReport,repID,divID) {
   window.location.href = 'form.asp?formType=addOSHAReport&division='+divID+'&reportType='+repID;
}

function proj_onchange(addReport,repID,divID,projID) {
   window.location.href = 'form.asp?formType=addOSHAReport&division='+divID+'&project='+projID+'&reportType='+repID;
}


function formControl(submitted) 
{
   if(submitted=="1") 
    {
   addReport.Submit.disabled=true
   document.addReport.submit();
  // alert("Thanks for your comments!")
    }
}

var myWind = ""
function doNew() {
	if (myWind == "" || myWind.closed || myWind.name == undefined) {
		myWind = window.open("uploadImage.asp?project=<%=iProjectID%>","subWindow","HEIGHT=600,WIDTH=1000")
	} else{
		myWind.focus();
	}
}





function addFormFieldGen() {
	var id = document.getElementById("idGen").value;
	//alert (id);
	$("#divTxtGen").append("<span id='row" + id + "'><label for='txt" + id + "'><b>" + id + ".</b>&nbsp;&nbsp;<textarea name=genquestion" + id + " id=genquestion" + id + " rows=2 cols=95 class=textArea></textarea>&nbsp;&nbsp<a href='#' onClick='removeFormFieldGeneral(\"#row" + id + "gen\"); return false;'>Remove</a><span><br><br>");
	
	
	$('#row' + id + 'gen').highlightFade({
		speed:1000
	});
	
	id = (id - 1) + 2;
	document.getElementById("idGen").value = id;
}

function removeFormFieldGeneral(idGen) {
alert (idGen);
	$(idGen + 'gen').remove();
}



function addFormFieldCust() {
	var id = document.getElementById("idCust").value;
	$("#divTxtCust").append("<span id='row" + id + "'><label for='txt" + id + "'><b>" + id + ".</b>&nbsp;&nbsp<textarea name=customer_question@answer" + id + " id=customer_question@answer" + id + " rows=2 cols=95 class=textArea></textarea>&nbsp;&nbsp<a href='#' onClick='removeFormFieldCust(\"#row" + id + "\"); return false;'>Remove</a><span><br><br>");
	
	
	$('#row' + id).highlightFade({
		speed:1000
	});
	
	id = (id - 1) + 2;
	document.getElementById("idCust").value = id;
}

function removeFormFieldCust(idCust) {
//alert (id);
	$(idCust).remove();
}
// -->
</script>
<form name="addReport" method="post" action="process.asp" onsubmit="return submitRTEForm();">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sNPDESTitle%></span><span class="Header"> - Add Mock OSHA/EPA Report</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td>
												<table border="0" cellpadding="0" cellspacing="0">
													<tr>
														<td><img src="images/pix.gif" width="10" height="1"></td>
														<td valign="top" align="right"><span class="required">*</span> <strong>Report Type:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<select name="reportType" onChange="return rept_onchange(addReport,this.getElementsByTagName('option')[this.selectedIndex].value)">
																<option>--Select a report type--</option>
																<%if iReportTypeID = "1" then%>
																	<option value="1" selected="selected">Mock OSHA</option>
																<%else%>
																	<option value="1">Mock OSHA</option>
																<%end if%>
																<!--<%'if iReportTypeID = "2" then%>
																	<option value="2" selected="selected">Mock EPA</option>
																<%'else%>
																	<option value="2">Mock EPA</option>
																<%'end if%>-->
															</select>&nbsp;&nbsp;<%if iReportTypeID = "" then%>**please select a report type**<%end if%>
														</td>
													</tr>
													<tr><td colspan="4"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><img src="images/pix.gif" width="10" height="1"></td>
														<td valign="top" align="right"><span class="required">*</span> <strong>Customer/Division:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<select name="division" onChange="return dept_onchange(addReport,reportType.getElementsByTagName('option')[reportType.selectedIndex].value,this.getElementsByTagName('option')[this.selectedIndex].value)">
																<option>--Select a customer/Division--</option>
																<%If iReportTypeID <> "" then
																do until rsDivisions.eof
																	If trim(rsDivisions("divisionID")) = trim(iDivisionID) then%>
																		<option selected="selected" value="<%=rsDivisions("divisionID")%>" ><%=rsDivisions("customerName")%> - <%=rsDivisions("division")%></option>
																	<%else%>
																		<option value="<%=rsDivisions("divisionID")%>" ><%=rsDivisions("customerName")%> - <%=rsDivisions("division")%></option>
																	<%end if
																'iCustomerID = rsDivisions("customerID")
																rsDivisions.movenext
																loop
																end if%>
															</select>&nbsp;&nbsp;
															<%if iReportTypeID <> "" then
															if iDivisionID = "" then%>
																**please select a customer/Division**
															<%end if
															end if%>
														</td>
													</tr>
													<tr><td colspan="4"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td></td>
														<td valign="top" align="right"><span class="required">*</span> <strong>Job Site Name:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<select name="project" onChange="return proj_onchange(addReport,reportType.getElementsByTagName('option')[reportType.selectedIndex].value,division.getElementsByTagName('option')[division.selectedIndex].value,this.getElementsByTagName('option')[this.selectedIndex].value)">
																<option>--Select a project--</option>
																<%If iDivisionID <> "" then
																	do until rsProject.eof
																		If trim(rsProject("projectID")) = trim(iProjectID) then%>
																			<option selected="selected" value="<%=rsProject("projectID")%>"><%=rsProject("projectName")%></option>
																		<%else%>
																			<option value="<%=rsProject("projectID")%>"><%=rsProject("projectName")%></option>
																		<%end if
																	rsProject.movenext
																	loop
																end if%>
															</select>&nbsp;&nbsp;
															<%if iDivisionID <> "" then
																if iProjectID = "" then%>
																**please select a project**
															<%end if
															end if%>
														</td>
													</tr>
													<tr><td colspan="4"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td></td>
														<td valign="top" align="right"><strong>Inspector:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=Session("Name")%>
															<input type="hidden" name="inspector" value="<%=Session("ID")%>" />
														</td>
													</tr>													
													<tr><td colspan="4"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td></td>
														<td valign="top" align="right"><span class="required">*</span> <strong>Report Date:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%if request("reportDate") <> "" then
																reportDate = request("reportDate")
															else
																reportDate = now()
															end if%>
															<input type="text" name="reportDate" maxlength="10" size="10" value="<%=formatdatetime(reportDate,2)%>"/>&nbsp;<a href="javascript:displayDatePicker('reportDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
														</td>
													</tr>
													<tr><td colspan="4"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td></td>
														<td valign="top" align="right"><span class="required">*</span> <strong>Inspection Date:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%if request("inspectionDate") <> "" then
																inspectionDate = request("inspectionDate")
															else
																inspectionDate = now()
															end if%>
															<input type="text" name="inspectionDate" maxlength="10" size="10" value="<%=formatdatetime(inspectionDate,2)%>"/>&nbsp;<a href="javascript:displayDatePicker('inspectionDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
														</td>
													</tr>
													<tr><td colspan="4"><img src="images/pix.gif" width="1" height="5"></td></tr>													
													<tr>
														<td></td>
														<td valign="top" align="right"><span class="required">*</span> <strong>Jobsite Status:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td valign="top">
															<textarea name="jobsiteStatus" rows="3" cols="70" class="textArea"><%=request("jobsiteStatus")%></textarea>
														</td>
													</tr>
													<tr><td colspan="4"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td></td>
														<td valign="top" align="right"><span class="required">*</span> <strong>Person Attending Walk-Around:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td valign="top">
															<input type="text" name="attendWalkAround" size="30" maxlength="50" />
														</td>
													</tr>
													<tr><td colspan="4"><img src="images/pix.gif" width="1" height="30"></td></tr>			
												</table>
												<table width="100%" border="0" cellpadding="0" cellspacing="0">
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td valign="top" colspan="3">
															<%If iDivisionID <> "" and iProjectID <> "" then
															
																Select Case iReportTypeID									
																	Case 1 'Mock OSHA%>
																		<!--#include file="inc_form_OSHA.asp"-->
																	<%case 2 'Mock EPA%>
																		<!--#include file="inc_form_EPA.asp"-->									
																	
																<%end select
																																			
															else%>
																there are no questions to display. please select a customer from the dropdown at the top of the page.
															<%end if%>
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td colspan="3" align="right">
															<%If iProjectID <> "" then%>
																<input type="hidden" name="customerID" value="<%=rsProjInfo("customerID")%>" />
																<!--<input type="hidden" name="reportType" value="<%'=iReportTypeID%>" />-->
																<%select case iReportTypeID
																	case 1 'Mock OSHA%>
																		<input type="hidden" name="processType" value="addOSHAReport" />
																	<%case 2 'Mock EPA%>
																		<input type="hidden" name="processType" value="addEPAReport" />
																<%end select%>
																 <!--onClick="formControl(1)"-->
																<input type="submit" name="Submit" value="  Save  " class="formButton"/>									
															<%end if%>
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
												</table>
											</td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
										</tr>
									</table>
									
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<!--<script language="JavaScript" type="text/javascript">

  //var frmvalidator  = new Validator("addReport");
  
  //frmvalidator.addValidation("contactNumber","req","Please enter a contact number");
 // frmvalidator.addValidation("reportDate","req","Please enter the report date");
 // frmvalidator.addValidation("inspectionDate","req","Please enter the inspection date");
 // frmvalidator.addValidation("jobsiteStatus","req","Please enter the jobsiteStatus");
 // frmvalidator.addValidation("attendWalkAround","req","Please enter the person that attended the walkaround with you.");
  
</script>-->

<%
rsStatus.close
set rsStatus = nothing
rsDivisions.close
set rsDivisions = nothing
rsInspectors.close
set rsInspectors = nothing
rsInspectionType.close
set rsInspectionType = nothing
rsWeather.close
set rsWeather = nothing
rsCategories.close
set rsCategories = nothing
rsQuestion.close
set rsQuestion = nothing

DataConn.close
set DataConn = nothing
%>