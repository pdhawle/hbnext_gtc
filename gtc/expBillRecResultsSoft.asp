<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
clientID = request("clientID")
smonth = request("month")
syear = request("year")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomersByClientSpecialActiveSoft"
   '.parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
   .CommandType = adCmdStoredProc   
End With

Set rsReport = oCmd.Execute
Set oCmd = nothing


Response.ContentType = "application/vnd.ms-excel"
Response.AddHeader "Content-Disposition", "attachment; filename=billingReconciliationReportSoftware.xls" 
%>

<!--start of content for the page-->
<table border="1">
	<tr>
		<td colspan="6"><strong>BILLING RECONCILIATION REPORT</strong></td>
	</tr>
	<tr bgcolor="#666666">
		<td><b style="color:#FFFFFF">Customer</b></td>
		<td><b style="color:#FFFFFF">Project</b></td>
		<td><b style="color:#FFFFFF">Bill Rate</b></td>
		<td><b style="color:#FFFFFF">Start Date</b></td>
		<td><b style="color:#FFFFFF">End Date</b></td>
	</tr>
	<%if rsReport.eof then%>
		<tr bgcolor="#DDDDDD"><td colspan="6">There are no result for the items you chose. Please select again.</td></tr>
	<%else
	iBillTotal = 0
	do until rsReport.eof%>
			<tr>
				<td colspan="6"><b><%=rsReport("customerName")%></b></td>
			</tr>
			<tr>
				<td colspan="6">
					<%if rsReport("advanceBilling") = "True" then%>
						<em>***Billing in Advance***</em><br>
					<%else%>
						<font color="#EE0000"><em>***Billing in Arrears***</em></font><br>
					<%end if%>
				</td>
			</tr>
			<%
			'get the projects associated with this customer
			Set oCmd = Server.CreateObject("ADODB.Command")
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spGetProjectandDivisionByCustomerActive"
			   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, rsReport("customerID"))
			   .CommandType = adCmdStoredProc   
			End With
			
			Set rsProject = oCmd.Execute
			Set oCmd = nothing
			
			
			if rsProject.eof then%>
				<tr><td colspan="6">There are no active projects listed for this customer</td></tr>
			<%else
				do until rsProject.eof
				
				'get the total number of reports for this project															
				Set oCmd = Server.CreateObject("ADODB.Command")
					
				With oCmd
				   .ActiveConnection = DataConn
				   .CommandText = "spGetNumReportsByProjectandDate"
				   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, rsProject("projectID"))
				   .parameters.Append .CreateParameter("@month", adInteger, adParamInput, 8, smonth)
				   .parameters.Append .CreateParameter("@year", adInteger, adParamInput, 8, syear)
				   .CommandType = adCmdStoredProc   
				End With																		
				Set rsTot = oCmd.Execute
				Set oCmd = nothing
				%>
					<tr bgcolor="#E3E3E3">
						<td></td>
						<td><%=rsProject("projectName")%></td>
						<td>
							<%
							
							if rsProject("endDate") <> "" then
								if rsProject("endDate") < now() then
									'zero out the bill rate
									iBillRate = 0
								else
									iBillRate = rsProject("billRate")
								end if
							else
								if rsProject("startDate") > now() then
									'zero out the bill rate
									iBillRate = 0
								else
									iBillRate = rsProject("billRate")
								end if
							end if
							response.Write formatCurrency(iBillRate,2)										
							
							if isnull(iBillRate) = false then
								if iBillRate <> "" then
									'if the project is inactive, don't add to the total
									If rsProject("isActive") = "True" then
										iBillTotal = iBillTotal + iBillRate
									end if
								end if
							end if
							%>
						</td>
						<td>
							<%=rsProject("startDate")%>
						</td>
						<td><%=rsProject("endDate")%></td>
					</tr>
				<%
				rsProject.movenext
				loop
			end if%>						
			<%
	
	rsReport.movenext
				
	loop
	rsReport.close
	set rsReport = nothing
	end if%>
	
	<tr bgcolor="#666666">
		<td></td>
		<td align="right"><strong style="color:#FFFFFF">Total:</strong>&nbsp;</td>
		<td colspan="6" align="left"><span style="color:#FFFFFF"><%=formatcurrency(iBillTotal,2)%></span></td>
	</tr>
</table>
						
<!--end of content for the page-->

<%
'rsCustomer.close
rsReport.close
DataConn.close
Set rsCustomer = nothing
Set rsReport = nothing
Set DataConn = nothing
Set oCmd = nothing
%>