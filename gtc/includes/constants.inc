<%
	Const adBinary = 128
	Const adVarBinary = 204
	Const adChar = 129
	Const adVarChar = 200
	Const adDBDate = 133
	Const adDBTimeStamp = 135
	Const adNumeric = 131
	Const adDouble = 5
	Const adSingle = 4
	Const adInteger = 3
	Const adSmallInt = 2
	Const adUnsignedTinyInt = 17
	Const adBoolean = 11
	Const adLongVarChar = 201
	Const adLongVarBinary = 205
	Const adCurrency = 6
	Const adDecimal = 14
	
	Const adParamInput = 1   
	Const adParamOutput = 2   
	Const adParamInputOutput = 3   
	Const adParamReturnValue = 4   
	
	Const adCmdUnspecified = -1   
	Const adCmdText = 1   
	Const adCmdTable = 2   
	Const adCmdStoredProc = 4   
	Const adCmdUnknown = 8

	Const adStateOpen = 1  
	
	adOpenForwardOnly = 0
	adOpenStatic = 3
	adOpenDynamic = 2
	adOpenKeyset = 1
	 
	
	Const sPageTitle = "On-Demand Records Management Application"
	Const sLoginTitle = "On-Demand Records Management Application"
	Const sNPDESTitle = "On-Demand NPDES"
	Const sNOITitle = "On-Demand NOI"
	Const sSupportTitle = "On-Demand Support"
	Const sDashboardTitle = "On-Demand Dashboard"
	Const sCustomerTitle = "On-Demand Customer"
	Const sRegKey = "IWezOtkuJbgFIAc213KacCXF1SPlaqa88SMPErXV8YBrW8eu0db3McxwOYLMQz9OqLDhk5qriOuo"
	Const sRegKeyUpload = "63809-42042-09133"
	Const sServerPath = "/gtc/downloads/"
	Const sServerOIPath = "/gtc/downloads/openItems/"
	sServerURL = "http://localhost/gtc/index.asp"
	const sPDFPath = "http://localhost/gtc/"
	const sServerNOIPath = "/gtc/downloads/NOI/"
	
	
	'for the alerts
	appName = "On-Demand Records Management"
	sURL = "http://localhost/gtc/index.asp"
%>