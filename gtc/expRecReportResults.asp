<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
iCustomerID = request("customer")
iDivisionID = request("division")
iProjectID = request("projectID")
dtFrom = request("fromDate")
dtTo = request("toDate")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomer"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iCustomerID)
   .CommandType = adCmdStoredProc   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsCustomer = oCmd.Execute
Set oCmd = nothing

if iProjectID = "All" then
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetRecReportByDivision" 'by division
	   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, iDivisionID)
	   .parameters.Append .CreateParameter("@fromDate", adDBTimeStamp, adParamInput, 8, dtFrom)
	   .parameters.Append .CreateParameter("@toDate", adDBTimeStamp, adParamInput, 8, dtTo)
	   .CommandType = adCmdStoredProc   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
				
	Set rsReport = oCmd.Execute
	Set oCmd = nothing
	
else
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetRecReportByProject" 'specific project
	   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, iProjectID)
	   .parameters.Append .CreateParameter("@fromDate", adDBTimeStamp, adParamInput, 8, dtFrom)
	   .parameters.Append .CreateParameter("@toDate", adDBTimeStamp, adParamInput, 8, dtTo)
	   .CommandType = adCmdStoredProc   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
				
	Set rsReport = oCmd.Execute
	Set oCmd = nothing
end if


Response.ContentType = "application/vnd.ms-excel"
Response.AddHeader "Content-Disposition", "attachment; filename=reconciliationReport.xls" 
if rsReport.eof <> true then%>
		<table border=1>
			<tr><td colspan="5"><strong>Customer: <%=rsCustomer("customerName")%></strong></td></tr>
			<tr><td colspan="5"><strong>Date Range: <%=dtFrom%> - <%=dtTo%></strong></td></tr>
			<tr>
				<td><strong>Report ID</strong></td>
				<td><strong>Division</strong></td>
				<td><strong>Project</strong></td>
				<td><strong>Report Date</strong></td>
				<td><strong>Report Type</strong></td>
			</tr>
	<%dim i
	i = 0
	do until rsReport.eof%>
		<tr>
			<td><%=rsReport("reportID")%></td>
			<td><%=rsReport("division")%></td>
			<td><%=rsReport("projectName")%></td>
			<td><%=rsReport("inspectionDate")%></td>
			<td><%=rsReport("reportType")%></td>
		</tr>
	<%
	rsReport.movenext
	i = i + 1
	loop%>
		<tr>
			<td colspan="5" align="center"><strong>Total Reports:&nbsp;<%=i%></strong></td>
		</tr>
	</table>
<%end if

rsReport.close
DataConn.close
Set rsCustomer = nothing
Set rsReport = nothing
Set DataConn = nothing
Set oCmd = nothing%>

