<table>
	<tr><td height="20"></td></tr>
	<tr>
		<td align="center">
			<%
			'get the client information
			Set oCmd = Server.CreateObject("ADODB.Command")		
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spGetClient"
			   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
			   .CommandType = adCmdStoredProc
				   
			End With
					
			Set rsClient = oCmd.Execute
			Set oCmd = nothing
			
			if projectID <> "" then
				if projectID <> "0" then
					
					Set oCmd = Server.CreateObject("ADODB.Command")		
					With oCmd
					   .ActiveConnection = DataConn
					   .CommandText = "spGetProject"
					   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, projectID)
					   .CommandType = adCmdStoredProc
						   
					End With
							
					Set rsProjectInfo = oCmd.Execute
					Set oCmd = nothing
				end if
			end if
		
			%>
			<strong><%=ucase(rsClient("clientName"))%></strong><br />
			INCIDENT REPORT<br />
			(Utility Damage)

		</td>
	</tr>
	<tr><td height="20"></td></tr>	
	<tr>
		<td>
			
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td align="right">Location:&nbsp;</td>
					<td>
						<input type="text" name="location" maxlength="200" size="30" value="<%=rs("location")%>"/>
					</td>
				</tr>
				<tr>
					<td align="right">Project#:&nbsp;</td>
					<td>
						<input type="text" name="projectNumber" maxlength="50" size="30" value="<%=rs("projectNumber")%>"/>
					</td>
				</tr>
				<tr><td colspan="2" height="10"></td></tr>
				<tr><td colspan="2"><strong>UTILITY OWNER</strong></td></tr>
				<tr>
					<td align="right">Name:&nbsp;</td>
					<td>
						<input type="text" name="propertyOwnerName" maxlength="50" size="30" value="<%=rs("propertyOwnerName")%>"/>
					</td>
				</tr>
				<tr>
					<td align="right">Address:&nbsp;</td>
					<td>
						<input type="text" name="propertyOwnerAddress" maxlength="100" size="30" value="<%=rs("propertyOwnerAddress")%>"/>
					</td>
				</tr>
				<tr>
					<td align="right">Phone No.:&nbsp;</td>
					<td>
						<input type="text" name="propertyOwnerPhone" maxlength="20" size="10" value="<%=rs("propertyOwnerPhone")%>"/>
					</td>
				</tr>
			
				<tr><td colspan="2" height="20"></td>
				<tr><td colspan="2"><strong>TIME</strong></td>
				<tr>
					<td align="right">Date of Incident:&nbsp;</td>
					<td>
						<input type="text" name="incidentDate" maxlength="10" size="10" value="<%=rs("incidentDate")%>"/>&nbsp;<a href="javascript:displayDatePicker('incidentDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
					</td>
				</tr>
				<tr>
					<td align="right">Time of Incident:&nbsp;</td>
					<td>
						<input id="timeofAccident" name="timeofIncident" type="text" value="<%=rs("timeofIncident")%>" size=10 maxlength=10 ONBLUR="validateDatePicker(this)">&nbsp;<IMG SRC="images/timepicker.gif" BORDER="0" ALT="Pick a Time!" ONCLICK="selectTime(this,timeofIncident)" STYLE="cursor:hand">
					</td>
				</tr>
				<tr>
					<td align="right">Foreman:&nbsp;</td>
					<td>
						<input name="foreman" type="text" size=30 maxlength="50" value="<%=rs("foreman")%>"  />
					</td>
				</tr>
				<tr>
					<td align="right">Operator:&nbsp;</td>
					<td>
						<input name="operator" type="text" size=30 maxlength="50" value="<%=rs("operator")%>"  />
					</td>
				</tr>
				<tr><td colspan="2" height="20"></td>
				<tr><td colspan="2"><strong>COMPLETE DESCRIPTION OF INCIDENT</strong></td>
				<tr>
					<td align="right"></td>
					<td>
						Description of Incident<br />
						<textarea name="incidentDescription" rows="5" cols="30"><%=rs("incidentDescription")%></textarea>
					</td>
				</tr>
				<!--<tr><td colspan="2" height="5"></td>
				<tr>
					<td align="right">Job No.:&nbsp;</td>
					<td>
						<input type="text" name="jobNumber" maxlength="50" size="30" value="<%'=rsProjectInfo("projectNumber")%>"/>
					</td>
				</tr>	-->			
				<tr><td colspan="2" height="20"></td>
				<tr><td colspan="2"><strong>DESCRIPTION OF DAMAGED PROPERTY</strong></td>
				<tr>
					<td align="right"></td>
					<td>
						Description of Damaged Property<br />
						<textarea name="damagedPropertyDescription" rows="5" cols="30"><%=rs("damagedPropertyDescription")%></textarea>
					</td>
				</tr>
				<tr><td colspan="2" height="20"></td>
				<tr><td colspan="2"><strong>WITNESSES</strong></td>
				<tr>
					<td align="right">Crew Present:&nbsp;</td>
					<td>
						<input name="crewPresent" type="text" size=30 maxlength="100" value="<%=rs("crewPresent")%>"  />
					</td>
				</tr>
				<tr>
					<td align="right">Officials Present:&nbsp;</td>
					<td>
						<input name="officialsPresent" type="text" size=30 maxlength="100" value="<%=rs("officialsPresent")%>"  />
					</td>
				</tr>
				<tr>
					<td align="right">Other Witnesses:&nbsp;</td>
					<td>
						<input name="otherWitnesses" type="text" size=30 maxlength="100" value="<%=rs("otherWitnesses")%>"  />
					</td>
				</tr>
				<tr><td colspan="2" height="20"></td>
				<tr><td colspan="2"><strong>INVESTIGATION CONDUCTED</strong></td>
				<tr>
					<td>&nbsp;</td>
					<td>
						Were Damaged Utilities at Sufficient Depth or Height?<br />
						<input type="radio" name="sufficientDepthHeight" value="Yes" <%=isCheckedRadio(rs("sufficientDepthHeight"),"Yes")%>/>&nbsp;Yes&nbsp;&nbsp;&nbsp;<input type="radio" name="sufficientDepthHeight" value="No" <%=isCheckedRadio(rs("sufficientDepthHeight"),"No")%> />&nbsp;No
					</td>
				</tr>
				<tr><td colspan="2" height="5"></td>
				<tr>
					<td>&nbsp;</td>
					<td>
						Were Utilities shown on Job Specifications?<br />
						<input type="radio" name="utilitiesShown" value="Yes" <%=isCheckedRadio(rs("utilitiesShown"),"Yes")%>/>&nbsp;Yes&nbsp;&nbsp;&nbsp;<input type="radio" name="utilitiesShown" value="No" <%=isCheckedRadio(rs("utilitiesShown"),"No")%> />&nbsp;No
					</td>
				</tr>
				<tr><td colspan="2" height="5"></td>
				
				<tr>
					<td>&nbsp;</td>
					<td>
						Had Utility Been Informed to Relocate Equipment?<br />
						<input type="radio" name="utilityInformed" value="Yes" <%=isCheckedRadio(rs("utilityInformed"),"Yes")%>/>&nbsp;Yes&nbsp;&nbsp;&nbsp;<input type="radio" name="utilityInformed" value="No" <%=isCheckedRadio(rs("utilityInformed"),"No")%> />&nbsp;No
					</td>
				</tr>				
				<tr><td colspan="2" height="5"></td>
				<tr>
					<td>&nbsp;</td>
					<td>
						Were the utilities marked?<br />
						<input type="radio" name="utilityMarked" value="Yes" <%=isCheckedRadio(rs("utilityMarked"),"Yes")%>/>&nbsp;Yes&nbsp;&nbsp;&nbsp;<input type="radio" name="utilityMarked" value="No" <%=isCheckedRadio(rs("utilityMarked"),"No")%> />&nbsp;No
					</td>
				</tr>
				<tr><td colspan="2" height="5"></td>	
				<tr>
					<td align="right">Locate Ticket #:&nbsp;</td>
					<td>
						<input name="locateTicketNumber" type="text" size=30 maxlength="100" value="<%=rs("locateTicketNumber")%>"   />
					</td>
				</tr>
				<tr><td colspan="2" height="5"></td>		
				<!--<tr>
					<td>&nbsp;</td>
					<td>
						By Whom?<br />
						<input name="byWhom" type="text" size=30 maxlength="100"  />
					</td>
				</tr>
				<tr><td colspan="2" height="5"></td>			
				<tr>
					<td>&nbsp;</td>
					<td>
						CURRENT LOCATE TICKET NO.:<br />
						<input name="locateTicketNumber" type="text" size=30 maxlength="100"  />
					</td>
				</tr>-->
				<tr>
					<td align="right"></td>
					<td>
						Describe Conversations with Utility Representatives<br />
						<textarea name="describeConversation" rows="5" cols="30"><%=rs("describeConversation")%></textarea>
					</td>
				</tr>
				<tr><td colspan="2" height="20"></td>
				<tr><td colspan="2"><strong>COST OF INCIDENT TO COMPANY</strong></td>
				<tr>
					<td align="right"></td>
					<td>
						Describe in Detail the Damages and Delays Caused <br />by the Incident, including Equipment, Labor and Overhead:<br />
						<textarea name="describeDamageAndDelays" rows="5" cols="30"><%=rs("describeDamageAndDelays")%></textarea>
					</td>
				</tr>
				<tr><td colspan="2" height="20"></td>
				<tr><td colspan="2"><strong>FOREMAN SIGNATURE</strong></td>
				<tr>
					<td>Reported By:&nbsp;</td>
					<td>
						<input type="text" name="reportedBy" size="30" maxlength="50" value="<%=rs("reportedBy")%>" />
						<!--<select name="userID">
							
							<option value="<%'=session("ID")%>"><%'=session("name")%></option>
							<%'do until rsUsers.eof
								'if trim(userID) = trim(rsUsers("userID")) then%>
									<option selected="selected" value="<%'=rsUsers("userID")%>"><%'=rsUsers("firstName")%>&nbsp;<%'=rsUsers("lastName")%></option>
								<%'else%>
									<option value="<%'=rsUsers("userID")%>"><%'=rsUsers("firstName")%>&nbsp;<%'=rsUsers("lastName")%></option>
							<%'end if
							'rsUsers.movenext
							'loop%>
						</select>-->
					</td>
				</tr>
			</table>
	</tr>
</table>
