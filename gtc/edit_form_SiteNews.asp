<%
siteNewsID = request("ID")
clientID = request("clientID")


On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

Set oCmd = Server.CreateObject("ADODB.Command")	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetSiteNewsItem"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, siteNewsID)
   .CommandType = adCmdStoredProc   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rs = oCmd.Execute
Set oCmd = nothing
%>

<form name="editSiteNews" method="post" action="process.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sLoginTitle%></span><span class="Header"> - Add Sequence News</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Headline:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="headline" size="30" maxlength="100" value="<%=rs("headline")%>"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top"><span class="required">*</span> <strong>News Story:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<textarea name="story" rows="7" cols="53"><%=rs("story")%></textarea>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td align="right"><strong>Publish:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="checkbox"  name="isPublished" <%=isChecked(rs("isPublished"))%>/>
											</td>
										</tr>
										<%if Session("superAdmin") = True then%>
											<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
											<tr>
												<td align="right"><strong>Make Public:</strong></td>
												<td><img src="images/pix.gif" width="5" height="1"></td>
												<td>
													<input type="checkbox"  name="isPublic" <%=isChecked(rs("isPublic"))%>/>
												</td>
											</tr>
										<%else%>
											<input type="hidden" name="isPublic" value="" />
										<%end if%>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td colspan="3" align="right">
												<input type="hidden" name="siteNewsID" value="<%=siteNewsID%>" />
												<input type="hidden" name="clientID" value="<%=clientID%>" />
												<input type="hidden" name="processType" value="editSiteNews" />
												<input type="submit" value="  Save  " class="formButton"/>
											</td>
										</tr>
									</table>
								
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<script language="JavaScript" type="text/javascript">

  var frmvalidator  = new Validator("editSiteNews");
  frmvalidator.addValidation("headline","req","Please enter an event");
  frmvalidator.addValidation("story","req","Please enter the news story");
</script>