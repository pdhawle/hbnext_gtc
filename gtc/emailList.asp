<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->

<%
Dim rs, oCmd, DataConn

sView = request("sView")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

select case sView
	case "archived"
		sp = "spGetArchivedEmailList"
	case "faxes"
		sp = "spGetFaxList"
	case else
		sp = "spGetEmailList"
end select

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")

With oCmd
   .ActiveConnection = DataConn
   .CommandText = sp
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, Session("ID"))
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rs = oCmd.Execute
Set oCmd = nothing
%>

<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script language="JavaScript">
<!--
function confirmDelete(delUrl) {
 if (confirm("Are you sure you wish to delete this item?")) {
    document.location = delUrl;
  }


}

function confirmArchive(arUrl) {
 if (confirm("Are you sure you wish to archive this item?")) {
    document.location = arUrl;
  }


}
//-->
</script>
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sLoginTitle%></span><span class="Header"> - 
								<%select case sView
									case "archived"%>
										Archived Emails
									<%case "faxes"%>
										Faxes
									<%case else%>
										Sent Emails
								<%end select%>
								</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;						
						<a href="emailList.asp" class="footerLink">sent emails</a>&nbsp;<span class="footerLink">|</span>&nbsp;
						<a href="emailList.asp?sView=archived" class="footerLink">archived emails</a>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
									
										<%Select Case sView%>
										
											<%case "faxes"%>
												
												<tr bgcolor="#666666">
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td><span class="searchText">Faxed To</span></td>
													<td><span class="searchText">Subject</span></td>
													<td><span class="searchText">Date Faxed</span></td>
													<td align="center"><span class="searchText">Action</span></td>
												</tr>
												<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>
												<%If rs.eof then%>
													<tr><td></td><td colspan="4">there are no records to display</td></tr>
												<%else
													blnChange = true
													Do until rs.eof
														If blnChange = true then%>
															<tr class="rowColor">
														<%else%>
															<tr>
														<%end if%>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td><%=rs("faxNumber")%></td>
															<td><a href="emailDetails.asp?id=<%=rs("faxID")%>"><%=rs("subject")%></a></td>
															<td><%=rs("dateFaxed")%></td>
															<td align="center"><a href="downloads/<%=rs("docFaxed")%>" target="_blank">view</a></td>
														</tr>
													<%rs.movenext
													if blnChange = true then
														blnChange = false
													else
														blnChange = true
													end if
													loop
												end if%>
												
											<%case else%>
											
												<tr bgcolor="#666666">
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td><span class="searchText">Sent To</span></td>
													<td><span class="searchText">Subject</span></td>
													<td><span class="searchText">Date Sent</span></td>
													<%if sView = "" Then%>
													<td align="center"><span class="searchText">Archive</span></td>
													<%end if%>
													<td align="center"><span class="searchText">Delete</span></td>
												</tr>
												<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>
												<%If rs.eof then%>
													<tr><td></td><td colspan="4">there are no records to display</td></tr>
												<%else
													blnChange = true
													Do until rs.eof
														If blnChange = true then%>
															<tr class="rowColor">
														<%else%>
															<tr>
														<%end if%>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td><%=rs("sendTo")%></td>
															<td><a href="emailDetails.asp?id=<%=rs("emailID")%>"><%=rs("subject")%></a></td>
															<td><%=rs("dateSent")%></td>
															<%if sView = "" Then%>
															<td align="center"><input type="image" src="images/folder.gif" width="16" height="13" alt="archive" border="0" onClick="return confirmArchive('process.asp?id=<%=rs("emailID")%>&processType=archiveEmail')"></td>
															<%end if%>
															<td align="center"><input type="image" src="images/remove.gif" width="11" height="13" alt="delete" border="0" onClick="return confirmDelete('process.asp?id=<%=rs("emailID")%>&processType=deleteEmail')"></td>
														</tr>
													<%rs.movenext
													if blnChange = true then
														blnChange = false
													else
														blnChange = true
													end if
													loop
												end if%>
											
										<%end select%>
										
										
										
										
									</table>
									
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>