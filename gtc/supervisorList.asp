<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
userID = request("userID")

Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If


On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 	
DataConn.Open Session("Connection"), Session("UserID")

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetSupervisors"
   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)
   .CommandType = adCmdStoredProc
   
End With
			
Set rs = oCmd.Execute
Set oCmd = nothing

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetUser"
   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)
   .CommandType = adCmdStoredProc
   
End With
			
Set rsUser = oCmd.Execute
Set oCmd = nothing

%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Supervisor List</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<a href="userList.asp" class="footerLink">user list</a>&nbsp;&nbsp;
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="form.asp?formType=assignSupervisor&userID=<%=userID%>&clientID=<%=session("clientID")%>" class="footerLink">assign supervisor(s)</a>&nbsp;&nbsp;			
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr><td colspan="10"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td colspan="10">
												<strong>User:</strong> <%=rsUser("firstName")%>&nbsp;<%=rsUser("lastName")%>
											</td>
										</tr>
										<tr><td colspan="10"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr bgcolor="#666666">
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td><span class="searchText">Supervisor</span></td>					
											<td><span class="searchText">Email</span></td>
										</tr>
										<tr><td colspan="10"><img src="images/pix.gif" width="1" height="10"></td></tr>
											<%If rs.eof then%>
											<tr><td></td><td colspan="2">there are no supervisors assigned to this user</td></tr>
											<%else
											blnChange = true
											'get the supervisors for this user
											Do until rs.eof
												If blnChange = true then%>
													<tr class="rowColor">
												<%else%>
													<tr>
												<%end if%>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td><%=rs("firstName")%>&nbsp;<%=rs("lastName")%></td>							
													<td><a href="mailto:<%=rs("email")%>"><%=rs("email")%></a></td>
												</tr>
											<%rs.movenext
											if blnChange = true then
												blnChange = false
											else
												blnChange = true
											end if
											loop
										end if%>
									</table>
									
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>