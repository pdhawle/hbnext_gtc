<%
quoteID = request("quoteID")
customerQuoteID = request("customerQuoteID")
userID = request("userID")
customerID = request("customerID")
clientID = request("clientID")
'divisionID = request("divisionID")


On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")
	
'Create command
'get the division name and customer name
'Set oCmd = Server.CreateObject("ADODB.Command")
	
'With oCmd
'   .ActiveConnection = DataConn
'   .CommandText = "spGetDivisionsByCustomer"
'   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, customerID)
'   .CommandType = adCmdStoredProc
   
'End With
			
'Set rsDivision = oCmd.Execute
'Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetQuoteCustomer"
   .parameters.Append .CreateParameter("@customerQuoteID", adInteger, adParamInput, 8, customerQuoteID)
   .CommandType = adCmdStoredProc   
End With
			
Set rsQuote = oCmd.Execute
Set oCmd = nothing

sState = rsQuote("projectState")
if sState = "" then
	sState = request("projectState")
end if

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomer"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, customerID)
   .CommandType = adCmdStoredProc
   
End With
	
Set rsCustomer = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetStates"
   .CommandType = adCmdStoredProc   
End With
	
Set rsState = oCmd.Execute
Set oCmd = nothing

If sState <> "" then

	Set oCmd = Server.CreateObject("ADODB.Command")
	
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetCountiesByState"
	   .parameters.Append .CreateParameter("@ID", adVarchar, adParamInput, 50, trim(sState))
	   .CommandType = adCmdStoredProc   
	End With
		
	Set rsCounty = oCmd.Execute
	Set oCmd = nothing

end if

Set oCmd = Server.CreateObject("ADODB.Command")	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetUser"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, userID)
   .CommandType = adCmdStoredProc   
End With
	
Set rsUser = oCmd.Execute
Set oCmd = nothing

%>
<script type="text/javascript">
<!--
//function proj_onchange(addProject,stateID,customerID,divisionID) {
 //  window.location.href = 'form.asp?formType=addProject&state='+stateID+'&id='+customerID+'&divisionID='+divisionID;
//}

function proj_onchange(addPLS) {
   document.addPLS.action = "form.asp?formType=addPLS";
   addPLS.submit(); 
}
 -->
</script>
<form name="addPLS" method="post" action="process.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Project Launch Information</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table border="0" cellpadding="0" cellspacing="0">
										<!--<tr>
											<td valign="top" colspan="3"><strong>CUSTOMER/CLIENT INFORMATION</strong></td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>-->
										<tr>
											<td valign="top" align="right"><strong>Customer/Client:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%=rsCustomer("customerName")%>	
												<input type="hidden" name="customerID" value="<%=customerID%>" />
												<input type="hidden" name="quoteID" value="<%=quoteID%>" />
												<input type="hidden" name="customerQuoteID" value="<%=customerQuoteID%>" />
												<input type="hidden" name="userID" value="<%=userID%>" />
												<input type="hidden" name="clientID" value="<%=clientID%>" />
												<input type="hidden" name="name" value="<%=rsUser("firstName") & " " & rsUser("lastName")%>" />
												<input type="hidden" name="email" value="<%=rsUser("email")%>" />
												<input type="hidden" name="customerName" value="<%=rsCustomer("customerName")%>" />											
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<!--<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Permit holder:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="radio" name="permitHolder" value="Primary" />&nbsp;Primary&nbsp;&nbsp;<input type="radio" name="permitHolder" value="Secondary" />&nbsp;Secondary											
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Office Phone:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="billPhone" value="<%'=rsCustomer("billPhone")%>" size="10" />											
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Cell Phone:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>	
												<input type="text" name="billCell" value="<%'=rsCustomer("billMobile")%>" size="10" />											
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Fax:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>	
												<input type="text" name="billFax" value="<%'=rsCustomer("billFax")%>" size="10" />											
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Email:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>	
												<input type="text" name="billEmail" value="<%'=rsCustomer("billFax")%>" size="30" />											
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Billing Address:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>	
												<input type="text" name="billAddress" value="<%'=rsCustomer("billFax")%>" size="30" />											
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>City:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>	
												<input type="text" name="billCity" value="<%'=rsCustomer("billFax")%>" size="30" />											
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>State:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>	
												<select name="billState">
													<option value="">--select state--</option>
													<%'do until rsState.eof
														'if sState = rsState("stateID") then%>
															<option selected="selected" value="<%'=rsState("stateID")%>"><%'=rsState("stateID")%></option>
														<%'else%>
															<option value="<%'=rsState("stateID")%>"><%'=rsState("stateID")%></option>
													<%'end if
													'rsState.movenext
													'loop
													'srState.movefirst%>
												</select>&nbsp;&nbsp;<b>Zip:</b>&nbsp;<input type="text" name="billZip" value="<%'=rsCustomer("billFax")%>" size="3" />									
											</td>
										</tr>										
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Physical Address:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>	
												<input type="text" name="physicalAddress" value="<%'=rsCustomer("billFax")%>" size="30" />											
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>City:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>	
												<input type="text" name="physicalCity" value="<%'=rsCustomer("billFax")%>" size="30" />											
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>State:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>	
												<select name="physicalState">
													<option value="">--select state--</option>
													<%'do until rsState.eof
														'if sState = rsState("stateID") then%>
															<option selected="selected" value="<%'=rsState("stateID")%>"><%'=rsState("stateID")%></option>
														<%'else%>
															<option value="<%'=rsState("stateID")%>"><%'=rsState("stateID")%></option>
													<%'end if
													'rsState.movenext
													'loop
													'srState.movefirst%>
												</select>&nbsp;&nbsp;<b>Zip:</b>&nbsp;<input type="text" name="physicalZip" value="<%'=rsCustomer("billFax")%>" size="3" />									
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="15"></td></tr>
										<tr>
											<td valign="top" colspan="3"><strong>SITE/PROJECT INFORMATION</strong></td>
										</tr>										
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Division:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="divisionID">
													<%'do until rsDivision.eof%>
														<option value="<%'=rsDivision("divisionID")%>"><%'=rsDivision("division")%></option>
													<%'rsDivision.movenext
													'loop%>
												</select>&nbsp;If default, will need to edit under dashboard.												
											</td>
										</tr>-->												
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Project Name:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%
												projectName = rsQuote("projectName")
												if projectName = "" then
													projectName = request("projectName")
												end if
												%>
												<input type="text" name="projectName" size="30" value="<%=projectName%>" />
												
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Project Address:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%
												projectAddress = rsQuote("projectAddress")
												if projectAddress = "" then
													projectAddress = request("projectAddress")
												end if
												%>	
												<input type="text" name="projectAddress" value="<%=projectAddress%>" size="30" />											
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>City:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>	
												<%
												projectCity = rsQuote("projectCity")
												if projectCity = "" then
													projectCity = request("projectCity")
												end if
												%>	
												<input type="text" name="projectCity" value="<%=projectCity%>" size="30" />											
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>State:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>	
												<select name="projectState" onChange="return proj_onchange(addPLS)">
													<option value="">--select state--</option>
													<%do until rsState.eof
														if sState = rsState("stateID") then%>
															<option selected="selected" value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
														<%else%>
															<option value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
													<%end if
													rsState.movenext
													loop
													srState.movefirst%>
												</select>&nbsp;&nbsp;<span class="required">*</span> <b>Zip:</b>&nbsp;
												<%
												projectZip = rsQuote("projectZip")
												if projectZip = "" then
													projectZip = request("projectZip")
												end if
												%>
												<input type="text" name="projectZip" value="<%=projectZip%>" size="5" />									
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>County:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%if sState = "" then%>
													Please select a state above.
												<%else%>
													<select name="projectCounty" tooltipText="Please select the county of where the project is located.">
														<%do until rsCounty.eof
															if trim(rsQuote("county"))=trim(rsCounty("countyID")) then%>
															<option selected="selected" value="<%=rsCounty("countyID")%>"><%=rsCounty("county")%></option>
															<%else%>
															<option value="<%=rsCounty("countyID")%>"><%=rsCounty("county")%></option>
														<%end if
														rsCounty.movenext
														loop%>
													</select>
												<%end if%>
												
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Latitude:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="latitude" size="20" value="<%=request("latitude")%>" maxlength="20"/>&nbsp;example: 32.81527												
											</td>
										</tr>	
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Longitude:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="longitude" size="20" value="<%=request("longitude")%>" maxlength="20"/>&nbsp;example: -84.743189												
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Directions:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<textarea name="projectDirections" cols="30" rows="3"><%=request("projectDirections")%></textarea>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Acres/Phases:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="acresPhases" size="20" value="<%=request("acresPhases")%>"/>												
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Stage:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="radio" name="stage" value="existing" />&nbsp;existing&nbsp;&nbsp;
												<input type="radio" name="stage" value="clearing" />&nbsp;clearing&nbsp;&nbsp;
												<input type="radio" name="stage" value="grading" />&nbsp;grading&nbsp;&nbsp;
												<input type="radio" name="stage" value="utilities" />&nbsp;utilities&nbsp;&nbsp;
												<input type="radio" name="stage" value="construction" />&nbsp;construction&nbsp;&nbsp;												
											</td>
										</tr>										
										
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Contact Name:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%
												contactName = rsQuote("contactName")
												if contactName = "" then
													contactName = request("contactName")
												end if
												%>	
												<input type="text" name="contactName" maxlength="50" value="<%=contactName%>" size="30"/>
												
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Contact Number:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%
												contactPhone = rsQuote("contactPhone")
												if contactPhone = "" then
													contactPhone = request("contactPhone")
												end if
												%>
												<input type="text" name="contactPhone" maxlength="50" value="<%=contactPhone%>" size="30"/>
												
											</td>
										</tr>				
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Contact Cell:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%
												contactCell = rsQuote("contactCell")
												if contactCell = "" then
													contactCell = request("contactCell")
												end if
												%>
												<input type="text" name="contactCell" maxlength="50" value="<%=contactCell%>" size="30"/>
												
											</td>
										</tr>	
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Contact Fax:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%
												contactFax = rsQuote("contactFax")
												if contactFax = "" then
													contactFax = request("contactFax")
												end if
												%>
												<input type="text" name="contactFax" maxlength="50" value="<%=contactFax%>" size="30"/>
												
											</td>
										</tr>	
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Contact Email:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%
												contactEmail = rsQuote("contactEmail")
												if contactEmail = "" then
													contactEmail = request("contactEmail")
												end if
												%>
												<input type="text" name="contactEmail" value="<%=contactEmail%>" size="50"/>
												
											</td>
										</tr>			
										
										<!--<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top"><span class="required">*</span> <strong>Project Start Date:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="startDate" maxlength="10" size="10" tooltipText="Please enter the date that inspections will start on this project." value="<%=request("startDate")%>" />&nbsp;<a href="javascript:displayDatePicker('startDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
												
											</td>
										</tr>-->
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td colspan="3" align="right">
												<input type="hidden" name="processType" value="addPLS" />
												<%if sState = "" then%>
													<input type="submit" value="  Save  " class="formButton" disabled="disabled"/>
												<%else%>
													<input type="submit" value="  Save  " class="formButton"/>
												<%end if%>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
									</table>
								
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<script language="JavaScript" type="text/javascript">

  var frmvalidator  = new Validator("addPLS");
  frmvalidator.addValidation("projectName","req","Please enter the project name");
  frmvalidator.addValidation("projectAddress","req","Please enter the address for the project");
  frmvalidator.addValidation("projectCity","req","Please enter the city that the project is in");
  frmvalidator.addValidation("projectZip","req","Please enter the project zip code");
  frmvalidator.addValidation("projectDirections","req","Please enter the general directions to the project");
  frmvalidator.addValidation("stage","selone_radio","Please select the stage of the project");
  frmvalidator.addValidation("contactName","req","Please enter the contact name");
  frmvalidator.addValidation("contactPhone","req","Please enter the contact phone number");
  frmvalidator.addValidation("contactCell","req","Please enter the contact cell number");
  frmvalidator.addValidation("contactFax","req","Please enter the contact fax number");
  frmvalidator.addValidation("contactEmail","req","Please enter the contact email address");
</script>