<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->
<!--#include file="letterArray.asp"-->

<%
iDivisionID = request("divisionID")
reportID = request("reportID")

Session("LoggedIn") = "True"

Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 	
DataConn.Open Session("Connection"), Session("UserID")
'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetReportByID"
    .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, reportID) 'reportID
   .CommandType = adCmdStoredProc
   
End With			
Set rsReport = oCmd.Execute
Set oCmd = nothing

%>
<html>
<head>
<title>Storm Water Site Inspection Report</title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/reportswsir.css" type="text/css">
</head>
<body>

<table border="0" width=650 cellpadding=0 cellspacing=0>
	<tr>		
		<td valign="top" width="450">
			<span class="Header">Storm Water Site Evaluation Report</span><br>
			<strong>Evaluation Date:</strong>&nbsp;<u><%=rsReport("inspectionDate")%></u><br>
			<strong>Division:</strong>&nbsp;<u><%=rsReport("division")%></u><br>
			<strong>Site Name:</strong>&nbsp;<u><%=rsReport("projectName")%></u><br>
			<strong>Project Number:</strong>&nbsp;<u><%=rsReport("projectNumber")%></u>
		</td>
		<td valign=top align="right">
			<%if rsReport("logo") = "" then%>
				<img src="images/logo.jpg">
			<%else%>
				<img src="<%=rsReport("logo")%>">
			<%end if%>
		</td>
	</tr>
	<tr><td colspan="2"><img src=images/pix.gif width=1 height=20></td></tr>
	<tr>
		<td colspan="2">
			<div align="center"><span class="Header">Open Action Item Log</span><br><br>
			<%
			'get the responsive action items from this report
			'
			Set oCmd = Server.CreateObject("ADODB.Command")

			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spGetResponsiveActionItems"
			   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID) 'reportID
			   .CommandType = adCmdStoredProc
			   
			End With
						
			Set rsAction = oCmd.Execute
			Set oCmd = nothing
			%>
			<table class="borderTable2" width="650" cellpadding="0" cellspacing="0">
				<tr><td colspan="6" align="center" class="tCol"><strong>For action items found during this evaluation</strong></td></tr>
				<tr>
					<td align="center" class="tCol"><strong>Ref#</strong><br><img src=images/pix.gif width=60 height=1></td>
					<td align="center" class="tCol"><strong>Deficiency(Action Item)</strong></td>
					<td align="center" class="tCol"><strong>Location</strong></td>
					<td align="center" class="tCol"><strong>Addressed By</strong></td>
					<td align="center" class="tCol"><strong>Date</strong></td>					
					<td align="center" class="tCol" width="150"><strong>Action Taken</strong></td>
				</tr>				
				<%
				If rsAction.eof then
					response.Write "<tr><td class=tCol colspan=6 align=center>There are no corrective action items for this report.</td></tr>"
				else
					do until rsAction.eof%>
					<tr>
						<td align="center" valign="top" class="tCol"><%=rsAction("referenceNumber")%>&nbsp;</td>
						<td align="left" valign="top" class="tCol"><%=rsAction("actionNeeded")%>&nbsp;</td>
						<td align="center" valign="top" class="tCol"><%=rsAction("location")%>&nbsp;</td>
						<td align="center" valign="top" class="tCol"><%=rsAction("addressedBy")%>&nbsp;</td>
						<td align="left" valign="top" class="tCol" width="60"><%=rsAction("addressedByDate")%>&nbsp;</td>
						<td align="center" valign="top" class="tCol">&nbsp;</td>
					</tr>
					<%rsAction.movenext
					loop
				end if%>
				<tr>
					<td valign="top" colspan="6" class="tCol" height="40">
						&nbsp;<strong>Additional Comments:</strong>&nbsp;<%=rsReport("comments")%>
					</td>
				</tr>
			</table>
			
		</td>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=20></td></tr>
	<tr>
		<td colspan="2">
			<%
			'get the responsive action items from this report
			Set oCmd = Server.CreateObject("ADODB.Command")
	
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spGetOpenItemsPrevReports"
			   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, rsReport("projectID")) 'projectID
			   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID) 'reportID
			   .CommandType = adCmdStoredProc				   
			End With
							
			Set rsPrev = oCmd.Execute
			Set oCmd = nothing
			%>
			<table class="borderTable2" width="650" cellpadding="0" cellspacing="0">
				<tr><td colspan="5" align="center" class="tCol"><strong>Uncompleted Open Items From Prior Evaluations</strong></td></tr>
				<tr>
					<td align="center" class="tCol"><strong>Ref#</strong><br><img src=images/pix.gif width=60 height=1></td>
					<td align="center" class="tCol"><strong>Deficiency(Action Item)</strong></td>
					<td align="center" class="tCol"><strong>Location</strong></td>
					<td align="center" class="tCol"><strong>Evaluation Date</strong></td>					
					<td align="center" class="tCol" width="150"><strong>Explanation</strong></td>
				</tr>				
				<%
				If rsPrev.eof then
					response.Write "<tr><td colspan=5 align=center class=tCol>There are no open action items for this report.</td></tr>"
				else
					do until rsPrev.eof%>
					<tr>
				
						<td align="center" valign="top" class="tCol"><%=rsPrev("referenceNumber")%>&nbsp;</td>
						<td valign="top" class="tCol"><%=rsPrev("actionNeeded")%>&nbsp;</td>
						<td valign="top" class="tCol"><%=rsPrev("location")%>&nbsp;</td>
						<td align="center" valign="top" class="tCol"><%=rsPrev("inspectionDate")%>&nbsp;</td>
						<td align="center" valign="top" class="tCol"><%=rsPrev("explanation")%>&nbsp;</td>
					</tr>
					<%rsPrev.movenext
					loop
				end if%>
			</table>
			
		</td>
	</tr>
	<tr><td colspan="2" height="20"></td></tr>
	<tr>
		<td colspan="2">
			<table width="100%">
				<tr>
					<td colspan="3">
						<em>I certify under the penalty of law that this document and all attachments were prepared
						under my direction or supervision in accordance with a system designed to assure that
						qualified personnel properly gather and evaluate the information submitted. Based on my
						inquiry of the person or persons who manage the system, or those persons directly
						responsible for gathering the information, the information submitted is, to the best of my
						knowledge and belief, true, accurate, and complete. I am aware that there are significant
						penalties for submitting false information, including the possibility of fine and imprisonment
						for knowing violations.</em>
					</td>
				</tr>	
				<tr>
					<td>
						<u><%=rsReport("firstName")%>&nbsp;<%=rsReport("lastName")%></u><br>
						<strong>Name and Title of Inspector</strong>
					</td>
					<td>
						<%if rsReport("sigFileName") <> "" then%> 
							<u><img src="userUploads/<%=rsReport("userID")%>/<%=rsReport("sigFileName")%>"></u>
						<%else%>
							<u>__________________________</u> 
						<%end if%><br>
						<strong>Signature of Inspector</strong>
					</td>
					<td>
						<u><%=rsReport("inspectionDate")%></u><br>
						<strong>Date</strong>
					</td>
				</tr>
			</table>
		</td>		
	</tr>
	<tr><td colspan="2" height="20"></td></tr>
	<tr>
		<td colspan="2">
			<strong>Information to Permittee:</strong>  In order to maintain compliance with the terms and conditions of Stormwater General Permit NCG010000, corrective actions 
			identified in this Evaluation Form must be addressed within the timeframe specified by the permit. Please note corrective actions taken on the Stormwater 
			Inspection Form and sign where indicated.
		</td>
	</tr>
</table>
</body>
</html>