<script language="javascript">
<!--
function addSample() {
   document.addReport.action = "form.asp?formType=addReport#jumpto";
   document.addReport.submit(); 
}

//-->
</script>
<%
iDCount = request("count")
if iDCount = "" then
	iDCount = 0
end if
%>
<table width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td width="10"></td>
		<td>
			<input type="hidden" name="dataCount" value="<%=iDCount%>" />
			<table bgcolor="#DDDDDD" class="borderTable" cellpadding="3" cellspacing="0">
				<tr>
					<td colspan="2">
						<strong>Defaults and Number of Samples</strong><br />
						Please enter the information below and click the "load samples" button. The info below will pre-populate the samples.
					</td>
				</tr>
				<tr>
					<td align="right">Number of Samples: </td>
					<td><input type="text" name="count" size="5" value="<%=iDCount%>" /></td>
				</tr>
				<!--<tr>
					<td align="right">City: </td>
					<td><input type="text" name="refCity" size="30" value="<%'=request("refCity")%>" /></td>
				</tr>
				<tr>
					<td align="right">County: </td>
					<td><input type="text" name="refCounty" size="30" value="<%'=request("refCounty")%>" /></td>
				</tr>-->
				<tr>
					<td align="right">Date Sample Was Taken: </td>
					<td>
						<%if request("refDateSampleTaken") = "" then
							refDateSampleTaken = formatdatetime(now(),2)
						else
							refDateSampleTaken = request("refDateSampleTaken")
						end if%>
						<input type="text" name="refDateSampleTaken" maxlength="10" size="10" value="<%=refDateSampleTaken%>"/>&nbsp;<a href="javascript:displayDatePicker('refDateSampleTaken')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
					</td>
				</tr>
				<tr>
					<td align="right">Sampling Method: </td>
					<td><input type="checkbox" name="refIsAutomatic" <%=requestIsChecked(request("refIsAutomatic"))%> />&nbsp;Automatic &nbsp;<input type="checkbox" name="refIsManual" <%=requestIsChecked(request("refIsManual"))%> />&nbsp;Manual (Grab) </td>
				</tr>
				<tr>
					<td align="right">Sampled By: </td>
					<td>
						<%if request("refSampledBy") = "" then
							refSampledBy = Session("Name")
						else
							refSampledBy = request("refSampledBy")
						end if%>
						<input type="text" name="refSampledBy" size="30" value="<%=refSampledBy%>" />
					</td>
				</tr>
				<tr>
					<td align="right">Date of Analysis: </td>
					<td>
						<%if request("refAnalysisDate") = "" then
							refAnalysisDate = formatdatetime(now(),2)
						else
							refAnalysisDate = request("refAnalysisDate")
						end if%>
						<input type="text" name="refAnalysisDate" maxlength="10" size="10" value="<%=refAnalysisDate%>"/>&nbsp;<a href="javascript:displayDatePicker('refAnalysisDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
					</td>
				</tr>
				<tr>
					<td align="right">Analyzed By: </td>
					<td><input type="text" name="refAnalysisBy" size="30" value="<%=request("refAnalysisBy")%>" /></td>
				</tr>
				<tr>
					<td align="right">Analytical Method: </td>
					<td><input type="text" name="refAnalysisMethod" size="30" value="EPA 180.1" /></td>
				</tr>
				<!--<tr>
					<td align="right">Calibration Date: </td>
					<td>
						<input type="text" name="refCalibrationDate" maxlength="10" size="10" value="<%'=request("refCalibrationDate")%>"/>&nbsp;<a href="javascript:displayDatePicker('refCalibrationDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>&nbsp;
						<strong>Time: </strong>&nbsp;<input type="text" name="refCalibrationTime" size="5" maxlength="50" value="<%'=request("refCalibrationTime")%>" />
					</td>
				</tr>-->
				<tr>
					<td colspan="2" align="right"><input type="button" name="refresh" value="Load Samples" class="formButton" onclick="return addSample()"/></td>
				</tr>		
			</table>
		</td>
	</tr>
	<tr>
		<td><img src="images/pix.gif" width="1" height="10"></td>
	</tr>
</table>
<%
'set the number of samples based on the iDCount
for iNum = 1 to iDCount
%>
<a name="jumpto"></a>
	<table width="100%" class="borderTable" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td>
				<table cellpadding="0" cellspacing="0" border="0">
					<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
					<tr>
						<td><img src="images/pix.gif" width="5" height="0"></td>
						<td align="right"><span class="required">*</span> <strong>Sample #:</strong>&nbsp;</td>
						<td>
							
							<input type="text" name="sampleNumber<%=iNum%>" size="3" value="<%=iNum%>" />
						</td>
					</tr>
					<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
					<tr>
						<td></td>
						<td align="right"><span class="required">*</span> <strong>Date Sample Was Taken:</strong>&nbsp;</td>
						<td>
							<%'if request("dateSampleTaken" & iNum) = "" then
							'	dSampleTaken = formatdatetime(now(),2)
							'else
							'	dSampleTaken = request("dateSampleTaken" & iNum)
							'end if%>
							<input type="text" name="dateSampleTaken<%=iNum%>" maxlength="10" size="10" value="<%=request("refDateSampleTaken")%>"/>&nbsp;<a href="javascript:displayDatePicker('dateSampleTaken<%=iNum%>')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
						</td>
					</tr>
					<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
					<tr><td></td><td colspan="2"><strong><u>Sampling Point</u></strong></td></tr>
					<tr>
						<td></td>
						<td></td>
						<td>
							<table>
								<tr>
									<td align="right">
										<span class="required">*</span> <strong>Exact Location:</strong>&nbsp;
									</td>
									<td>
										<%
										if iProjectID <> "" then
										'get the location list for this project
										Set oCmd = Server.CreateObject("ADODB.Command")

										With oCmd
										   .ActiveConnection = DataConn
										   .CommandText = "spGetLocationsByProjectWS"
										   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, iProjectID) 'reportID
										   .CommandType = adCmdStoredProc
										   
										End With
													
										Set rsLoc = oCmd.Execute
										Set oCmd = nothing%>
										
										<select name="exactLocation<%=iNum%>">
											<option></option>
										<%do until rsLoc.eof%>
											<option value="<%=rsLoc("location")%>"><%=handleApostropheDisplay(rsLoc("location"))%></option>
										<%rsLoc.movenext
										loop%>
										</select>
										<%end if%>
									</td>
								</tr>
								<tr>
									<td align="right">
										<strong>Rain Amount:</strong>&nbsp;
									</td>
									<td>
										<input type="text" name="rain<%=iNum%>" maxlength="50" size="10" value="0.0" />
									</td>
								</tr>
								<!--<tr>
									<td align="right">
										<span class="required">*</span> <strong>City:</strong>&nbsp;
									</td>
									<td>
										<input type="text" name="city<%'=iNum%>" maxlength="100" value="<%'=request("refCity")%>" />
									</td>
								</tr>
								<tr>
									<td align="right">
										<span class="required">*</span> <strong>County:</strong>&nbsp;
									</td>
									<td>
										<input type="text" name="county<%'=iNum%>" maxlength="100" value="<%'=request("refCounty")%>" />
									</td>
								</tr>-->
							</table>													
						</td>
					</tr>
					<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
					<tr>
						<td><img src="images/pix.gif" width="5" height="0"></td>
						<td align="right"><strong>Time Sampled:</strong>&nbsp;</td>
						<td>
							<input type="text" name="timeSampled<%=iNum%>" size="5" maxlength="50" value="<%=request("timeSampled" & iNum)%>" /> 
							
							&nbsp;<input type="radio" name="sampleMethod<%=iNum%>" value="0" />&nbsp;Automatic 
							&nbsp;<input type="radio" name="sampleMethod<%=iNum%>" value="1" />&nbsp;Manual (Grab) 
						</td>
					</tr>
					<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
					<tr>
						<td><img src="images/pix.gif" width="5" height="0"></td>
						<td align="right"><span class="required">*</span> <strong>Sampled By:</strong>&nbsp;</td>
						<td>
							<input type="text" name="sampledBy<%=iNum%>" maxlength="150" value="<%=request("refSampledBy")%>" />
						</td>
					</tr>
					<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
				</table>
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td class="colorBars"></td>
					</tr>
				</table>
				<table cellpadding="0" cellspacing="0" border="0">
					<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
					<tr><td></td><td colspan="2"><strong><u>Analysis</u></strong></td></tr>
					<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
					<tr>
						<td><img src="images/pix.gif" width="5" height="0"></td>
						<td align="right"><strong>Date:</strong>&nbsp;</td>
						<td>
							<input type="text" name="analysisDate<%=iNum%>" maxlength="10" size="10" value="<%=request("refAnalysisDate")%>"/>&nbsp;<a href="javascript:displayDatePicker('analysisDate<%=iNum%>')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>&nbsp;
							<strong>Time: </strong>&nbsp;<input type="text" name="analysisTime<%=iNum%>" size="5" maxlength="50" value="<%=request("analysisTime" & iNum)%>" />
						</td>
					</tr>
					<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
					<tr>
						<td><img src="images/pix.gif" width="5" height="0"></td>
						<td align="right"><strong>By:</strong>&nbsp;</td>
						<td>
							<input type="text" name="analysisBy<%=iNum%>" maxlength="150" value="<%=request("refAnalysisBy")%>"/>
						</td>
					</tr>
					<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
					<tr>
						<td><img src="images/pix.gif" width="5" height="0"></td>
						<td align="right"><strong>Method:</strong>&nbsp;</td>
						<td>
							<input type="text" name="analysisMethod<%=iNum%>" maxlength="150" value="<%=request("refAnalysisMethod")%>" />
						</td>
					</tr>
					<!--<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
					<tr>
						<td><img src="images/pix.gif" width="5" height="0"></td>
						<td align="right"><strong>Calibration Date:</strong>&nbsp;</td>
						<td>
							<input type="text" name="calibrationDate<%'=iNum%>" maxlength="10" size="10" value="<%'=request("refCalibrationDate")%>"/>&nbsp;<a href="javascript:displayDatePicker('calibrationDate<%'=iNum%>')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>&nbsp;
							<strong>Time: </strong>&nbsp;<input type="text" name="calibrationTime<%'=iNum%>" size="5" maxlength="50" value="<%'=request("refCalibrationTime")%>" />
						</td>
					</tr>-->
					<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
					<tr>
						<td><img src="images/pix.gif" width="5" height="0"></td>
						<td align="right"><strong>Results (NTU):</strong>&nbsp;</td>
						<td>
							<input type="text" name="resultsNTU<%=iNum%>" size="5"  maxlength="50" value="<%=request("resultsNTU" & iNum)%>"/>
						</td>
					</tr>
					<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
				</table>
			</td>
		</tr>
	</table><br />
<%
next
%>
<div align="right">
<input type="checkbox" name="isBillable" />&nbsp;Billable
</div>