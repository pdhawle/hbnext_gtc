<%
clientID = request("clientID")
'projectID = request("projectID")
'accidentTypeID = request("accidentTypeID")
'userID = request("userID")
'sState = request("state")

'if sState = "" then
'	sState = "GA"
'end if

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")


Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetStates"
   .CommandType = adCmdStoredProc
   
End With
	
Set rsState = oCmd.Execute
Set oCmd = nothing

'If sState <> "" then

	Set oCmd = Server.CreateObject("ADODB.Command")
	
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetCountiesByState"
	   .parameters.Append .CreateParameter("@ID", adVarchar, adParamInput, 50, "GA")
	   .CommandType = adCmdStoredProc   
	End With
		
	Set rsCounty = oCmd.Execute
	Set oCmd = nothing

'end if
%>
<form name="addWorkComp" method="post" action="process.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Add Workers Compensation</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table border="0" cellpadding="0" cellspacing="0">
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Board Claim No.</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="boardClaimNo" size="10"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Employee Last Name</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="empLastName" size="30"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Employee First Name</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="empFirstName" size="30"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Employee Middle Initial</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="empMI" size="3"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Social Security Number</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="ssn" size="10"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Date of Injury</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="injuryDate" maxlength="10" size="10"/>&nbsp;<a href="javascript:displayDatePicker('injuryDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="15"></td></tr>
										<tr><td colspan="3"><strong>IDENTIFYING INFORMATION</strong></td></tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr><td colspan="3"><strong>EMPLOYEE</strong></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Sex</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="radio" name="empSex" value="Male" />&nbsp;Male&nbsp;&nbsp;<input type="radio" name="empSex" value="Female" />&nbsp;Female
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Birthdate</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="birthDate" maxlength="10" size="10"/>&nbsp;<a href="javascript:displayDatePicker('birthDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Phone Number</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="empPhone" size="10"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Employee Email</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="empEmail" size="30"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Address</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="empAddress" size="30"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>City</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="empCity" size="30"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>State</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="empState">
													<option value="">--select state--</option>
													<%do until rsState.eof%>
														<option value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
													<%rsState.movenext
													loop
													rsState.movefirst%>
												</select>&nbsp;&nbsp;<strong>Zip</strong>&nbsp;<input type="text" name="empZip" size="3"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr><td colspan="3"><strong>EMPLOYER</strong></td></tr>
										<%'set the below fields if the client is CW Matthews
										If clientID = 3 then
											employerName = "CW Matthews Contracting Company"
											employerAddress = "PO Drawer 970"
											employerCity = "Marietta"
											employerState = "GA"
											employerZip = "30061"
											employerPhone = "770.422.7520"
											employerEmail = "lorit@cwmatthews.com"
											
											claimName = "Lori Tidwell"
											claimPhone = "770.422.7520"
											claimEmail = "lorit@cwmatthews.com"
											claimAddress = "PO Drawer 970"
											claimCity = "Marietta"
											claimState = "GA"
											claimZip = "30061"
										end if
										%>
										<tr>
											<td valign="top" align="right"><strong>Name</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="employerName" size="30" value="<%=employerName%>"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Address</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="employerAddress" size="30" value="<%=employerAddress%>"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>City</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="employerCity" size="30" value="<%=employerCity%>"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>State</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="employerState">
													<option value="">--select state--</option>
													<%do until rsState.eof
														if employerState = rsState("stateID") then%>
															<option selected="selected" value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
														<%else%>
															<option value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
													<%end if
													rsState.movenext
													loop
													rsState.movefirst%>
												</select>&nbsp;&nbsp;<strong>Zip</strong>&nbsp;<input type="text" name="employerZip" size="3" value="<%=employerZip%>"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Phone Number</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="employerPhone" size="30" value="<%=employerPhone%>"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Employer Email</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="employerEmail" size="30" value="<%=employerEmail%>"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>NAICS Code</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="NAICSCode" size="5"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Nature of Business</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="natureOfBusiness" size="30"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Employer FEIN</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="employerFEIN" size="5"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr><td colspan="3"><strong>INSURER/SELF-INSURER</strong></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Name</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="insurerName" size="30" value="<%=employerName%>"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Insurer/Self-Insurer FEIN</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="insurerFEIN" size="5"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Insurer/Self-Insurer File #</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="insurerFileNumber" size="5"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr><td colspan="3"><strong>CLAIMS OFFICE</strong></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Name</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="claimName" size="30" value="<%=claimName%>"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Claims Office FEIN</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="claimFEIN" size="5"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Claims Office Phone</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="claimPhone" size="10" value="<%=claimPhone%>"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Claims Office Email</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="claimEmail" size="10" value="<%=claimEmail%>"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>SBWC ID #</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="claimSBWC" size="5"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Address</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="claimAddress" size="10" value="<%=claimAddress%>"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>City</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="claimCity" size="30" value="<%=claimCity%>"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>State</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="claimState">
													<option value="">--select state--</option>
													<%do until rsState.eof
														if claimState = rsState("stateID") then%>
															<option selected="selected" value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
														<%else%>
															<option value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
													<%end if
													rsState.movenext
													loop
													rsState.movefirst%>
												</select>&nbsp;&nbsp;<strong>Zip</strong>&nbsp;<input type="text" name="claimZip" size="3" value="<%=claimZip%>"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr><td colspan="3"><strong>EMPLOYMENT/WAGE</strong></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Date Hired By Employer</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="dateHired" maxlength="10" size="10"/>&nbsp;<a href="javascript:displayDatePicker('dateHired')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Job Classified Code #</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="jobCodeNumber" size="10"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong># Days Work per week</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="numberDaysWork" size="10"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Wage rate at time of injury or disease</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="radio" name="wageRate" value="Per Hour" />&nbsp;Per Hour&nbsp;&nbsp;<input type="radio" name="wageRate" value="Per Day" />&nbsp;Per Day&nbsp;&nbsp;<input type="radio" name="wageRate" value="Per Week" />&nbsp;Per Week&nbsp;&nbsp;<input type="radio" name="wageRate" value="Per Month" />&nbsp;Per Month
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Insurer Type Code</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="radio" name="insurerCode" value="I - Insurer" />&nbsp;I - Insurer&nbsp;&nbsp;<input type="radio" name="insurerCode" value="S - Self-Insurer" />&nbsp;S - Self-Insurer&nbsp;&nbsp;<input type="radio" name="insurerCode" value="G - Guarantee Fund" />&nbsp;G - Guarantee Fund
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>List normally scheduled days off</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="daysOff" size="30"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr><td colspan="3"><strong>INJURY/ILLNESS & MEDICAL</strong></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Time of Injury</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input id="injuryTime" name="injuryTime" type="text" size=8 maxlength=8 ONBLUR="validateDatePicker(this)">&nbsp;<IMG SRC="images/timepicker.gif" BORDER="0" ALT="Pick a Time!" ONCLICK="selectTime(this,injuryTime)" STYLE="cursor:hand">
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>County of Injury</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="injuryCounty">
													<%do until rsCounty.eof%>
														<option value="<%=rsCounty("countyID")%>"><%=rsCounty("county")%></option>
													<%rsCounty.movenext
													loop%>
												</select>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Date Employer had Knowledge of disability</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="dateEmployerKnowledge" maxlength="10" size="10"/>&nbsp;<a href="javascript:displayDatePicker('dateEmployerKnowledge')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Date Employee failed to work full day</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="dateEmployeeFullDay" maxlength="10" size="10"/>&nbsp;<a href="javascript:displayDatePicker('dateEmployeeFullDay')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Did employee receive full pay on daye of injury?</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="radio" name="receiveFullPay" value="Yes" />&nbsp;Yes&nbsp;&nbsp;<input type="radio" name="receiveFullPay" value="No" />&nbsp;No
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Did injury occur on employer premises?</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="radio" name="occurOnPremises" value="Yes" />&nbsp;Yes&nbsp;&nbsp;<input type="radio" name="occurOnPremises" value="No" />&nbsp;No
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Type of Injury</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="injuryType" size="30"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Body Part Affected</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="bodyPartAffected" size="30"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>How injury or illness/abnormal health cond. occurred</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<textarea rows="3" cols="30" name="howInjuryOccurred"></textarea>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Treating Physician (name and address)</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="physician" size="30"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Initial Treatment given</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="radio" name="treatmentGiven" value="None" />&nbsp;None<br />
												<input type="radio" name="treatmentGiven" value="Minor: By Employer" />&nbsp;Minor: By Employer<br />
												<input type="radio" name="treatmentGiven" value="Minor: Clinical Hospital" />&nbsp;Minor: Clinical Hospital<br />
												<input type="radio" name="treatmentGiven" value="Emergency Room" />&nbsp;Emergency Room<br />
												<input type="radio" name="treatmentGiven" value="Hospitalized > 24 hours" />&nbsp;Hospitalized > 24 hours
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Hospital/Treating Facility (name and address)</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="hospital" size="30"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>If returned to work give date</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="dateReturned" maxlength="10" size="10"/>&nbsp;<a href="javascript:displayDatePicker('dateReturned')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Returned at what wage</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="returnedWage" size="5"/>&nbsp;per week
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>If fatal, enter complete date of death</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="dateOfDeath" maxlength="10" size="10"/>&nbsp;<a href="javascript:displayDatePicker('dateOfDeath')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="15"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Report prepared by</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="preparedBy" size="30"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Telephone Number</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="preparedByPhone" size="30"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Date of Report</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="reportDate" maxlength="10" size="10"/>&nbsp;<a href="javascript:displayDatePicker('reportDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="15"></td></tr>
										
											
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td colspan="3" align="right">
												<input type="hidden" name="clientID" value="<%=clientID%>" />
												<input type="hidden" name="processType" value="addWorkComp" />
												<input type="submit" value="Save" class="formButton" />													
											</td>
										</tr>
									</table>
								
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<script language="JavaScript" type="text/javascript">

  var frmvalidator  = new Validator("addWorkComp");
  	frmvalidator.addValidation("empLastName","req","Please enter the employees last name");
	frmvalidator.addValidation("empFirstName","req","Please enter the employees first name");
	frmvalidator.addValidation("injuryDate","req","Please enter the date of the injury");
	frmvalidator.addValidation("injuryTime","req","Please enter the time of the injury");
	frmvalidator.addValidation("injuryType","req","Please enter the type of injury");
	frmvalidator.addValidation("bodyPartAffected","req","Please enter the body part affected");
	frmvalidator.addValidation("howInjuryOccurred","req","Please describe how the injury occurred");
	frmvalidator.addValidation("preparedBy","req","Who prepared the report?");
	
	//frmvalidator.addValidation("unitNumber","req","Please enter the unit number");
	//frmvalidator.addValidation("foreman","req","Please enter the foreman's name");
	//frmvalidator.addValidation("doingPriorToAccident","req","Describe in detail what you were doing immediately prior to the accident that required your use of the company vehicle?");
	//frmvalidator.addValidation("accidentDate","req","Please enter the accident date");
	//frmvalidator.addValidation("timeofAccident","req","Please enter the time of the accident");
	//frmvalidator.addValidation("describeWhatHappened","req","Please describe what happened");
	//frmvalidator.addValidation("reportedBy","req","Please enter who reported accident");
	

</script>

<%
rsState.close
set rsState = nothing
DataConn.close
set DataConn = nothing
%>