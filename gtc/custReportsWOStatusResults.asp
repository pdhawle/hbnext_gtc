<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%

iCustomerID = request("customer")
iDivisionID = request("division")
iProjectID = request("projectID")
dtFrom = request("fromDate")
dtTo = request("toDate")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

	
If iProjectID = "All" then
	'select the whole division
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetWOByDateandDivision" 'specific customer and division
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iCustomerID)
	   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, iDivisionID)
	   .parameters.Append .CreateParameter("@fromDate", adDBTimeStamp, adParamInput, 8, dtFrom)
	   .parameters.Append .CreateParameter("@toDate", adDBTimeStamp, adParamInput, 8, dtTo)
	   .CommandType = adCmdStoredProc   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
				
	Set rsReport = oCmd.Execute
	Set oCmd = nothing
else
	'select by just the project
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetWOByDateandProject" 'specific project
	   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, iProjectID)
	   .parameters.Append .CreateParameter("@fromDate", adDBTimeStamp, adParamInput, 8, dtFrom)
	   .parameters.Append .CreateParameter("@toDate", adDBTimeStamp, adParamInput, 8, dtTo)
	   .CommandType = adCmdStoredProc   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
				
	Set rsReport = oCmd.Execute
	Set oCmd = nothing
end if

%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sCustomerTitle%></span><span class="Header"> - Work Order Status</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<!--<a href="expCustReportsWOStatusResults.asp?customer=<%'=iCustomerID%>&division=<%'=iDivisionID%>&fromDate=<%'=dtFrom%>&toDate=<%'=dtTo%>" class="footerLink">Export Data to Excel</a>&nbsp;&nbsp;
						<span class="footerLink">|</span>&nbsp;&nbsp;--><a href="custReportsWOStatus.asp" class="footerLink">back to selections</a>			
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0">
										<tr bgcolor="#666666">
											<td colspan="6"></td>
											<td align="center" colspan="2"><span class="searchText">Current Status</span></td>
										</tr>
										<tr bgcolor="#666666">
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td><span class="searchText">Work Order ID</span></td>
											<td><span class="searchText">Customer</span></td>
											<td><span class="searchText">Division</span></td>
											<td><span class="searchText">Project</span></td>
											<td><span class="searchText">Create Date</span></td>
											<td align="center"><span class="searchText">Approved</span></td>
											<td align="center"><span class="searchText">Closed</span></td>
										</tr>
								
										<%if iCustomerID <> "" and iDivisionID <> "" then
											If rsReport.eof then %>
												<tr><td></td><td colspan="7">there are no work orders to display</td></tr>
											<%else
												blnChange = false
												Do until rsReport.eof
													If blnChange = true then%>
														<tr class="rowColor">
													<%else%>
														<tr>
													<%end if%>
														<td></td>
														<td><a href="downloads/workorder_<%=rsReport("workOrderID")%>.pdf" target="_blank"><%=rsReport("workOrderID")%></a></td>
														<td><%=rsReport("customerName")%></td>
														<td><%=rsReport("division")%></td>
														<td><%=rsReport("projectName")%></td>
														<td><%=rsReport("dateAdded")%></td>
														<td align="center">
															<%if rsReport("isApproved") = "True" then%>
																<img src="images/check.gif">
															<%end if%>
														</td>
														<td align="center">
															<%if rsReport("isClosed") = "True" then%>
																<img src="images/check.gif">
															<%end if%>
														</td>
													</tr>
												<%
											
												rsReport.movenext
												if blnChange = true then
													blnChange = false
												else
													blnChange = true
												end if
												loop%>
												
												<tr><td colspan="10"><img src="images/pix.gif" width="1" height="10"></td></tr>
											
											<%end if
										end if%>
										
									</table>
															
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>