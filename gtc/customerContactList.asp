<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->
<%

customerID = request("id")

Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 	
DataConn.Open Session("Connection"), Session("UserID")

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomerContacts"
   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)
   .CommandType = adCmdStoredProc   
End With
Set rs = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomer"
   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)
   .CommandType = adCmdStoredProc   
End With
Set rsCust = oCmd.Execute
Set oCmd = nothing
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<link rel="stylesheet" type="text/css" href="menu/popupmenu.css" />
<script type="text/javascript" src="menu/jquery.min.js"></script>
<script type="text/javascript" src="menu/popupmenu.js"></script>
<script language="JavaScript">
<!--
function confirmDelete(delUrl) {
 if (confirm("Are you sure you wish to delete this contact?")) {
    document.location = delUrl;
  }


}
//-->
</script>
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Customer Contact List</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<a href="form.asp?formType=addCustomerContact&customerID=<%=customerID%>" class="footerLink">add contact</a>&nbsp;&nbsp;
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="customerList.asp?id=<%=left(rsCust("customerName"),1)%>" class="footerLink">customer list</a>&nbsp;&nbsp;	
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="form.asp?formType=addInfoPack&clientID=<%=session("clientID")%>&customer=<%=customerID%>" class="footerLink">add info pack</a>&nbsp;&nbsp;			
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr><td colspan="10"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr><td colspan="10">
											<!--#include file="custDropdown.asp"-->
										</td></tr>
										<tr bgcolor="#666666">
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td valign="bottom"><span class="searchText">Contact Name</span></td>
											<td valign="bottom"><span class="searchText">Title</span></td>
											<td valign="bottom"><span class="searchText">Direct line</span></td>
											<td valign="bottom"><span class="searchText">Email</span></td>
											<td valign="bottom" align="center"><span class="searchText">Info Packs</span></td>
											<td valign="bottom" align="center"><span class="searchText">HB/S<br />Primary<br />Contact</span></td>
											<td valign="bottom" align="center"><span class="searchText">HBTC<br />Primary<br />Contact</span></td>
											<td valign="bottom" align="center"><span class="searchText">Edit</span></td>
											<td valign="bottom" align="center"><span class="searchText">Delete</span></td>
										</tr>
										<tr><td colspan="10"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<%If rs.eof then%>
											<tr><td></td><td colspan="10">there are no contacts listed for this customer</td></tr>
										<%else
										
											blnChange = true
											Do until rs.eof
											
												Set oCmd = Server.CreateObject("ADODB.Command")
												With oCmd
												   .ActiveConnection = DataConn
												   .CommandText = "spGetInfoPacksByContact"
												   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, rs("customerContactID"))
												   .CommandType = adCmdStoredProc   
												End With
															
												Set rsInfo = oCmd.Execute
												Set oCmd = nothing
												
												if not rsInfo.eof then
													blnInfo = "True"
												else
													blnInfo = "False"
												end if
											
												If blnChange = true then%>
													<tr class="rowColor">
												<%else%>
													<tr>
												<%end if%>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td><a href="form.asp?customerID=<%=customerID%>&formType=editCustomerContact&customerContactID=<%=rs("customerContactID")%>"><%=rs("contactName")%>&nbsp;<%=rs("contactLastName")%></a></td>
													<td><%=rs("contactTitle")%></td>
													<td>
														<%
														if rs("phone") <> "" then
															phone = stripPhone(rs("phone"))
															'add the dots
															phoneAdj1 = addDotFirstThree(phone)
															phoneAdj2 =addDotSecondThree(phone)
															phoneAdj3 = lastFour(phone)
															
															phone = phoneAdj1 & phoneAdj2 & phoneAdj3
															%>
															<%=phone%>
															<%if rs("ext") <> "" then%>
																ext. <%=rs("ext")%>
															<%end if%>
														<%elseif rs("cell") <> "" then
															cell = stripPhone(rs("cell"))
															'add the dots
															cellAdj1 = addDotFirstThree(cell)
															cellAdj2 =addDotSecondThree(cell)
															cellAdj3 = lastFour(cell)
															
															cell = cellAdj1 & cellAdj2 & cellAdj3
															%>
															<%=cell%>
														<%else
															office = stripPhone(rs("officePhone"))
															'add the dots
															officeAdj1 = addDotFirstThree(office)
															officeAdj2 =addDotSecondThree(office)
															officeAdj3 = lastFour(office)
															
															office = officeAdj1 & officeAdj2 & officeAdj3
															%>
															<%=office%>
														<%end if%>
													</td>
													<td><a href="mailto:<%=rs("contactEmail")%>"><%=rs("contactEmail")%></a></td>
													<td align="center">
														<%if blnInfo = "True" then%>
														<a href="infoPackListContact.asp?customerContactID=<%=rs("customerContactID")%>&customerID=<%=customerID%>">list info packs</a>
														<%end if%>
													</td>
													<td align="center">
														<%if rs("isPrimary") = "True" then%>
															<img src="images/check.gif">
														<%end if%>
													</td>
													<td align="center">
														<%if rs("HBTCContact") = "True" then%>
															<img src="images/check.gif">
														<%end if%>
													</td>
													<td align="center"><a href="form.asp?customerID=<%=customerID%>&formType=editCustomerContact&customerContactID=<%=rs("customerContactID")%>"><img src="images/edit.gif" width="19" height="19" alt="edit" border="0"></a></td>
													<td align="center"><a href=""><input type="image" src="images/remove.gif" width="11" height="13" alt="delete" border="0" onClick="return confirmDelete('process.asp?id=<%=rs("customerContactID")%>&processType=deleteCustomerContact&customerID=<%=customerID%>')"></a></td>
												</tr>
											<%rs.movenext
											if blnChange = true then
												blnChange = false
											else
												blnChange = true
											end if
											loop
										end if%>
									</table>
									
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>