<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If


Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetProjectsAll"
   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, session("clientID"))
   .CommandType = adCmdStoredProc   
End With
			
Set rsProjects = oCmd.Execute
Set oCmd = nothing



%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script language="javascript" src="includes/datePicker.js"></script>
<link rel="stylesheet" href="includes/datePicker.css" type="text/css">
</head>
<body>
<form name="reportList" method="post" action="journalActResults.asp" >
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Journal Activity</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td>
												Select project and date range to see journal activity.<br>
												<select name="projectID">
													<option value="all">--all projects--</option>
													<%do until rsProjects.eof
														if trim(rsProjects("projectID")) = trim(projectID) then%>
															<option selected="selected" value="<%=rsProjects("projectID")%>"><%=rsProjects("projectName")%></option>
														<%else%>
															<option value="<%=rsProjects("projectID")%>"><%=rsProjects("projectName")%></option>
														<%end if%>
													<%rsProjects.movenext
													loop%>
												</select>&nbsp;&nbsp;&nbsp;&nbsp;
												
												From: <input type="text" name="fromDate" maxlength="10" size="10" value="<%=formatdatetime(dateadd("d", -7,now()),2)%>"/>&nbsp;<a href="javascript:displayDatePicker('fromDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a> &nbsp;&nbsp;&nbsp;&nbsp;    
												To: <input type="text" name="toDate" maxlength="10" size="10" value="<%=formatdatetime(now(),2)%>"/>&nbsp;<a href="javascript:displayDatePicker('toDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a> &nbsp;&nbsp;&nbsp;&nbsp;
												
												<input type="submit" value=" Select " class="formButton"> 
											</td>
										</tr>
									</table>
		
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
</body>
</html>
<%
rsProjects.close
DataConn.close
Set rsProjects = nothing
Set DataConn = nothing
Set oCmd = nothing
%>