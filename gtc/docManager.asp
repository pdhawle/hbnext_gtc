<!--#include file="includes/constants.inc"-->
<!--#include file="includes/functions.asp"-->
<%
If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

iProjectID = request("projectID")



%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<%
	'get the project name for the header
	On Error Resume Next

	Set DataConn = Server.CreateObject("ADODB.Connection") 
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
		
	DataConn.Open Session("Connection"), Session("UserID")
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
	
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetAssignedProjects"
	   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, session("ID"))
	   .CommandType = adCmdStoredProc   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
				
	Set rsProjects = oCmd.Execute
	Set oCmd = nothing
	
	if iProjectID <> "" then
		Set oCmd = Server.CreateObject("ADODB.Command")
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetProject"
		   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iProjectID)
		   .CommandType = adCmdStoredProc   
		End With
					
		Set rsProject = oCmd.Execute
		Set oCmd = nothing
	end if
	

	'get the settings of the boxes
	Ffile=request.querystring("fileoptions")
	if Ffile="" then Ffile=request.cookies("fileoptions")
		if Ffile="" then 
			response.cookies("fileoptions")="max" 
		else 
			response.cookies("fileoptions")=Ffile 
		end if
	Ffile=request.cookies("fileoptions")
	
	Fprojects=request.querystring("projectList")
	if Fprojects="" then Fprojects=request.cookies("projectList")
		if Fprojects="" then 
			response.cookies("projectList")="min" 
		else 
			response.cookies("projectList")=Fprojects 
		end if
	Fprojects=request.cookies("projectList")
	
	'settings of boxes is now loaded.
%>
<script language="JavaScript">
<!--
function dept_onchange(docManager) {
   docManager.submit(); 
}
//-->
</script>
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sLoginTitle%></span><span class="Header"> - Document Manager</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="98%" align="left" cellpadding="0" cellspacing="0" class="borderTable">
									  <tr> 
										<td colspan="8">
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr bgcolor="#FFFFFF"><td colspan="9"><img src="images/pix.gif" width="1" height="10"></td></tr>
											<tr bgcolor="#FFFFFF">
												<td colspan="9">
													<!--get a dropdown of all of the assigned projects for the user-->
													<form name="docManager" method="post" action="docManager.asp">
														&nbsp;&nbsp;&nbsp;&nbsp;Please select a project to manage it's documents.<br>
														&nbsp;&nbsp;&nbsp;&nbsp;<select name="projectID" onChange="return dept_onchange(docManager)">
															<option value="">--Select Project--</option>
															<%do until rsProjects.eof
															if trim(rsProjects("projectID")) = trim(iProjectID) then%>
																<option selected="selected" value="<%=rsProjects("projectID")%>"><%=rsProjects("customerName")%> - <%=rsProjects("projectName")%></option>
															<%else%>
																<option value="<%=rsProjects("projectID")%>"><%=rsProjects("customerName")%> - <%=rsProjects("projectName")%></option>
															<%end if
															rsProjects.movenext
															loop
															rsProjects.close
															set rsProjects = nothing%>
														</select>
														<%'if Session("inspector") = "True" then
														'	if rsProject("PLSGenerated") = True then
														'		sPLS = "<a href=downloads/PLS_" & iProjectID & ".pdf target=_blank>view PLS</a>"
														'	else
														'		sPLS = "NA"
														'	end if%>
															<br><br>
															<%sCurrPath = Request.Querystring("path")
															
															if sCurrPath <> "" then
																'remove the extra path info
																sCurrPath = replace(sCurrPath,"/downloads/","")
																sCurrPath = replace(sCurrPath,"project_" & iProjectID & "/","")
															else
																sCurrPath = "Root folder"
															end if%>
															&nbsp;&nbsp;&nbsp;&nbsp;<strong>Current Folder: <%=sCurrPath%></strong>
														<%'end if%>
													</form>
													
													
												</td>
											</tr>
											<tr bgcolor="#FFFFFF"><td colspan="9"><img src="images/pix.gif" width="1" height="5"></td></tr>
											<tr align="left" valign="top"> 
											  <td width="160">
											  
											  
											  <!--main menu for the document manager-->
											  <% if Ffile="min" then %>
												<table class="borderTable" width="155" border="0" align="center" cellpadding="0" cellspacing="0">
												  <tr bgcolor="#295DCE"> 
													<td width="10" bgcolor="#295DCE"> <div align="center"><font color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif"><strong> 
														</strong></font></div></td>
													<td bgcolor="#295DCE"><font color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif"><strong>Files 
													  / Folders</strong></font></td>
													<td width="33"> <div align="right"><font color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif"></font><a href="docManager.asp?fileoptions=max&action=viewfolder&path=<%=request.querystring("path")%>&projectID=<%=iProjectID%>"><img src="images/arrows_down.gif" width="21" height="22" border="0"></a></div></td>
												  </tr>
												</table>
												<% else %>
											  
												<table class="borderTable" width="155" border="0" align="center" cellpadding="0" cellspacing="0">
												  <tr bgcolor="#295DCE"> 
													<td width="10" bgcolor="#295DCE"> 
													  <div align="center"><font color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif"><strong> 
														</strong></font></div></td>
													<td bgcolor="#295DCE"><font color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif"><strong>Files 
													  / Folders</strong></font></td>
													<td width="33"> <div align="right"><font color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif"></font><a href="docManager.asp?fileoptions=min&action=viewfolder&path=<%=request.querystring("path")%>&projectID=<%=iProjectID%>"><img src="images/arrows_up.gif"  border="0"></a></div></td>
												  </tr>
												  <tr bgcolor="#EFEFEF"> 
													<td colspan="3">
													  <br><table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr> 
														  <td colspan="2">
															<%if iProjectID <> "" then%>
																<a class="fileLink" href="docManager.asp?projectID=<%=iProjectID%>">&nbsp;Document Manager Home</a>
															<%else%>
																&nbsp;Document Manager Home
															<%end if%>
														</td>
														</tr>
													  </table><br>
													  <table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr> 
														  <td width="24%"><div align="center"><img src="images/expl_newfile.gif" width="26" height="21"></div></td>
														  <td width="76%">
															<%if iProjectID <> "" then%>
																<a class="fileLink" href="docManager.asp?action=newfile&path=<%=request.querystring("path")%>&projectID=<%=iProjectID%>">Create file</a>
															<%else%>
																Create file
															<%end if%>
														  </td>
														</tr>
													  </table>
													  <table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr> 
														  <td width="24%"><div align="center"><img src="images/nfolder.gif" width="25" height="16"></div></td>
														  <td width="76%">
															<%if iProjectID <> "" then%>
																<a class="fileLink" href="docManager.asp?action=CreateNewFolder&path=<%=request.querystring("path")%>&projectID=<%=iProjectID%>">Create folder</a>
															<%else%>
																Create folder
															<%end if%>							  	
														  </td>
														</tr>
													  </table>
													  <br>
													  <table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr> 
														  <td width="24%"><div align="center"><img src="images/upload.gif" ></div></td>
														  <td width="76%">
															<%if iProjectID <> "" then%>
																<a class="fileLink" href="docManager.asp?action=UploadFiles&path=<%=request.querystring("path")%>&projectID=<%=iProjectID%>">Upload file(s)</a>
															<%else%>
																Upload file(s)
															<%end if%>
														  </td>
														</tr>
													  </table>
													  <br> 
													 <!-- <table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr> 
														  <td colspan="2">&nbsp;<a class="fileLink" href="javascript:window.close();">Close Document Manager</a></td>
														</tr>
													  </table>
													  <br>--></td>
												  </tr>
												</table> 
												<% end if %><br>
												 <!--end main menu for the document manager-->
												
												
												<!--main menu for the project list-->
											  <% if Fprojects="min" then %>
												<table class="borderTable" width="155" border="0" align="center" cellpadding="0" cellspacing="0">
												  <tr bgcolor="#295DCE"> 
													<td width="10" bgcolor="#295DCE"> <div align="center"><font color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif"><strong> 
														</strong></font></div></td>
													<td bgcolor="#295DCE"><font color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif"><strong>My Projects</strong></font></td>
													<td width="33"> <div align="right"><font color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif"></font><a href="docManager.asp?projectList=max&action=viewfolder&path=<%=request.querystring("path")%>&projectID=<%=iProjectID%>"><img src="images/arrows_down.gif" width="21" height="22" border="0"></a></div></td>
												  </tr>
												</table>
												<% else %>					  
												<table class="borderTable" width="155" border="0" align="center" cellpadding="0" cellspacing="0">
												  <tr bgcolor="#295DCE"> 
													<td width="10" bgcolor="#295DCE"> 
													  <div align="center"><font color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif"><strong> 
														</strong></font></div></td>
													<td bgcolor="#295DCE"><font color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif"><strong>My Projects</strong></font></td>
													<td width="33"> <div align="right"><font color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif"></font><a href="docManager.asp?projectList=min&action=viewfolder&path=<%=request.querystring("path")%>&projectID=<%=iProjectID%>"><img src="images/arrows_up.gif"  border="0"></a></div></td>
												  </tr>
												  <tr bgcolor="#EFEFEF"> 
													<td colspan="3">
														<%'Create command
														Set oCmd = Server.CreateObject("ADODB.Command")
															
														With oCmd
														   .ActiveConnection = DataConn
														   .CommandText = "spGetAssignedProjects"
														   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, session("ID"))
														   .CommandType = adCmdStoredProc   
														End With
															
															If Err Then
														%>
																<!--#include file="includes/FatalError.inc"-->
														<%
															End If
																	
														Set rsProjects = oCmd.Execute
														Set oCmd = nothing
														do until rsProjects.eof%>
															  <table border="0" cellspacing="0" cellpadding="0">
																<tr> 
																  <td>&nbsp;</td>
																  <td><a href="docManager.asp?projectID=<%=rsProjects("projectID")%>"><%=rsProjects("projectName")%></a></td>
																</tr>
															  </table>
														<%rsProjects.movenext
														loop%>		  
													  <br></td>
												  </tr>
												</table> 
												<% end if %><br>
												 <!--end main menu for the project list-->
												
												
											  </td>
											  <td width="5">&nbsp;</td>
											  <td width="80%" height="350" bgcolor="#FFFFFF"> 
												<!--#include file="filemanager.asp" -->
											  </td>
											</tr>
										  </table> </td>
									  </tr>
									</table>
															
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>