<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
Dim rs, oCmd, DataConn

projectID = Request("projectID")
clientID = Request("clientID")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If


'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetProcessedWorkOrders"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, projectID)
   .CommandType = adCmdStoredProc   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rs = oCmd.Execute
Set oCmd = nothing


%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script language="javascript" src="includes/datePicker.js"></script>
<link rel="stylesheet" href="includes/datePicker.css" type="text/css">
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sNPDESTitle%></span><span class="Header"> - Closed Work Orders List</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">						
						&nbsp;&nbsp;<a href="workOrdersList.asp?projectID=<%=projectID%>&clientID=<%=clientID%>" class="footerLink">view open work orders</a>&nbsp;&nbsp;		
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="closedWorkOrdersList.asp?projectID=<%=projectID%>&clientID=<%=clientID%>" class="footerLink">view closed work orders</a>&nbsp;&nbsp;			
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td colspan="15">
												This is where the work orders are listed after the invoice has been paid.
											</td>
										</tr>
										<tr bgcolor="#666666">
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td><span class="searchText">WorkOrderID</span></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td><span class="searchText">Project</span></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td><span class="searchText">Date Processed</span></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td><span class="searchText">Processed By</span></td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td align="center"><span class="searchText">Action</span></td>
										</tr>
										<%if projectID <> "" then
											if rs.eof then%>
												<tr><td colspan="15">&nbsp;There are no processed work orders for this project.</td></tr>
											<%else
												blnChange = true
												do until rs.eof
													If blnChange = true then%>
														<tr class="rowColor">
													<%else%>
														<tr>
													<%end if%>
													<td></td>
													<td><%=rs("workOrderID")%></td>
													<td></td>
													<td><%=rs("projectName")%></td>
													<td></td>
													<td><%=rs("dateProcessed")%></td>
													<td></td>
													<td><%=rs("processedBy")%></td>
													<td></td>
													<td align="center"><a href="downloads/workOrder_<%=rs("workOrderID")%>.pdf" target="_blank">view/print</a></td>
												</tr>
											
												<%
											
												rs.movenext
											
												if blnChange = true then
													blnChange = false
												else
													blnChange = true
												end if
												loop
											end if%>
										<%end if%>
				
									</table>
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>