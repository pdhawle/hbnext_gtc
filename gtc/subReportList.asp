<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
Dim rs, oCmd, DataConn

reportID = request("reportID")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetOSHASubsByID"
	.parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, reportID)
   .CommandType = adCmdStoredProc   
End With
			
Set rs = oCmd.Execute
Set oCmd = nothing
%>

<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Report Subcontractor List</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<a href="form.asp?formType=addOSHASub&reportID=<%=reportID%>" class="footerLink">add sub</a>&nbsp;&nbsp;
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="OSHAReportList.asp?findReport=<%=reportID%>" class="footerLink">report list</a>&nbsp;&nbsp;	
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="downloads/Report_<%=reportID%>Ent.pdf" target="_blank" class="footerLink">view entire report (unbroken)</a>&nbsp;&nbsp;
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="downloads/Report_<%=reportID%>.pdf" target="_blank" class="footerLink">view entire report</a>&nbsp;&nbsp;
						<!--<span class="footerLink">|</span>&nbsp;&nbsp;<a href="downloads/Report_<%'=reportID%>_GC.pdf" target="_blank" class="footerLink">view GC portion</a>&nbsp;&nbsp;-->
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<%If rs.eof then%>
											<tr><td colspan="7">there are no subcontractors to display</td></tr>
										<%else
											blnChange = true
											Do until rs.eof
												If blnChange = true then%>
													<tr class="rowColor">
												<%else%>
													<tr>
												<%end if%>
													<td>
									
														&nbsp;&nbsp;<%=rs("subName")%>
														
													</td>
													<td align="right">
														<a href="downloads/Report_<%=reportID%>_<%=rs("subID")%>.pdf" target="_blank">view sub report</a>&nbsp;&nbsp;|
														&nbsp;&nbsp;<a href="form.asp?formType=editOSHASub&subID=<%=rs("subID")%>&reportID=<%=reportID%>">edit</a>&nbsp;&nbsp;
													</td>
												</tr>
											<%rs.movenext
											if blnChange = true then
												blnChange = false
											else
												blnChange = true
											end if
											loop
										end if%>
									</table>
															
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>