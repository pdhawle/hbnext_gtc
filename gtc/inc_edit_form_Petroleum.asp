<table cellpadding="0" cellspacing="0" border="0">
	<%'else display the questions
	If rsCategories.eof then%>
		<tr><td></td><td colspan="7">there are no questions to display. please select another customer and project from the dropdown at the top of the page or add questions for this customer via the administrator.</td></tr>
	<%else%>
		<%blnChange = true
		i = 1
		Do until rsCategories.eof
			%>
			<tr>
				<td><img src="images/pix.gif" width="5" height="0"></td>
				<td>
					<%
																		
						Set oCmd = Server.CreateObject("ADODB.Command")

						With oCmd
						   .ActiveConnection = DataConn
						   .CommandText = sType
						   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, rsCategories("categoryID"))
						   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, iReportID)
						   .CommandType = adCmdStoredProc
						   
						End With
							
							If Err Then
						%>
							<!--#include file="includes/FatalError.inc"-->
						<%
							End If
									
						Set rsQuestion = oCmd.Execute
						Set oCmd = nothing
						%>
						
						<%if not rsQuestion.eof then%>
						<u><strong><%=rsCategories("category")%></strong></u><br><br>
						<%end if%>
						<table width="800" cellpadding="0" cellspacing="0" border="0">
							<%blnChange = true
							do until rsQuestion.eof
								If blnChange = true then%>
								<tr class="rowColor">
								<%else%>
								<tr>
								<%end if%>
									<td width="15" valign="top">
										<br />&nbsp;<%=i%>. 
									</td>
									<td align="left">
										<br /><%=rsQuestion("question")%><br /><br />
										<select name="answer<%=rsQuestion("questionID")%>">
											<!--<option value="">--select answer--</option>-->
											<%if rsQuestion("answer") = "Provide Response Below" Then%>
												<option selected="selected" value="Provide Response Below">Provide Response Below</option>
											<%else%>
												<option value="Provide Response Below">Provide Response Below</option>
											<%end if%>
											<%if rsQuestion("answer") = "Yes" Then%>
												<option selected="selected" value="Yes">Yes</option>
											<%else%>
												<option value="Yes">Yes</option>
											<%end if%>
											<%if rsQuestion("answer") = "Yes (explain below)" Then%>
												<option selected="selected" value="Yes (explain below)">Yes (explain below)</option>
											<%else%>
												<option value="Yes (explain below)">Yes (explain below)</option>
											<%end if%>
											<%if rsQuestion("answer") = "No" Then%>
												<option selected="selected" value="No">No</option>
											<%else%>
												<option value="No">No</option>
											<%end if%>
											<%if rsQuestion("answer") = "No (explain below)" Then%>
												<option selected="selected" value="No (explain below)">No (explain below)</option>
											<%else%>
												<option value="No (explain below)">No (explain below)</option>
											<%end if%>
										</select><br /><br />
										
										Remarks<br />
										<textarea name="remark<%=rsQuestion("questionID")%>" cols="40" rows="3"><%=rsQuestion("remark")%></textarea><br /><br />
										
									</td>
									
								</tr>
						
						<%rsQuestion.movenext
						i=i+1
						if blnChange = true then
							blnChange = false
						else
							blnChange = true
						end if
						loop%>
						

					</table>
					
				</td>
			</tr>
		<%rsCategories.movenext
		loop%>
		</table>
	<%end if%>