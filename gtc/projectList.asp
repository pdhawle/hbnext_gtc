<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
customerID = Request("customerID")
divisionID = Request("id")

Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If


On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetProjectsByDivision"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, divisionID)
   .CommandType = adCmdStoredProc   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rs = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomer"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, customerID)
   .CommandType = adCmdStoredProc
   
End With
			
Set rsCust = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetDivision"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, divisionID)
   .CommandType = adCmdStoredProc
   
End With
			
Set rsDiv = oCmd.Execute
Set oCmd = nothing
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script type="text/javascript">
    var GB_ROOT_DIR = "<%=sPDFPath%>/greybox/";
</script>
<script type="text/javascript" src="greybox/AJS.js"></script>
<script type="text/javascript" src="greybox/AJS_fx.js"></script>
<script type="text/javascript" src="greybox/gb_scripts.js"></script>
<link href="greybox/gb_styles.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="menu/popupmenu.css" />
<script type="text/javascript" src="menu/jquery.min.js"></script>
<script type="text/javascript" src="menu/popupmenu.js"></script>
<script language="JavaScript">
<!--
function confirmDelete(delUrl) {
 if (confirm("Are you sure you wish to delete this project?")) {
    document.location = delUrl;
  }


}
//-->
</script>
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Project List</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<a href="form.asp?formType=addProject&id=<%=customerID%>&divisionID=<%=divisionID%>" class="footerLink">add project</a>&nbsp;&nbsp;
						<!--<span class="footerLink">|</span>&nbsp;&nbsp;<a href="customerList.asp" class="footerLink">customer list</a>&nbsp;&nbsp;-->
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="divisionList.asp?ID=<%=customerID%>" class="footerLink">division list</a>			
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td colspan="9">
												<strong>Division: </strong><%=rsDiv("division")%><br><br>
												
												<!--#include file="custDropdown.asp"-->
												
											</td>
										</tr>
										<tr bgcolor="#666666">
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td><span class="searchText">Project Name</span></td>
											<!--<td><span class="searchText">Customer/Client</span></td>
											<td><span class="searchText">Division</span></td>-->
											<td align="center"><span class="searchText">Contacts</span></td>
											<td align="center"><span class="searchText">Locations</span></td>
											<!--<td align="center"><span class="searchText">Streets</span></td>
											<td align="center"><span class="searchText">Cranes</span></td>
											<td align="center"><span class="searchText">Lots</span></td>-->	
											<!--<td align="center"><span class="searchText">Rates</span></td>
											<td align="center"><span class="searchText">Alerts</span></td>-->
											<td align="center"><span class="searchText">Docs/Reports</span></td>
											<td align="center"><span class="searchText">NOI</span></td>
											<!--<td align="center"><span class="searchText">Quote</span></td>
											<td align="center"><span class="searchText">PLS</span></td>-->
											<td align="center"><span class="searchText">Map</span></td>	
											<td align="center"><span class="searchText">% Complete</span></td>				
											<td align="center"><span class="searchText">Edit</span></td>
											<!--<td align="center"><span class="searchText">Delete</span></td>-->
											<td align="center"><span class="searchText">Active</span></td>
										</tr>
										<tr><td colspan="10"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<%If rs.eof then%>
											<tr><td></td><td colspan="10">there are no records to display</td></tr>
										<%else
											blnChange = true
											Do until rs.eof
												If blnChange = true then%>
													<tr class="rowColor">
												<%else%>
													<tr>
												<%end if%>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td><%=rs("projectName")%></td>
													<!--<td><%'=rs("customerName")%></td>
													<td><%'=rs("division")%></td>-->
													<td align="center"><a href="contactList.asp?project=<%=rs("projectID")%>" title="Project Contacts" rel="gb_page_fs[]">List</a></td>
													<td align="center"><a href="locationList.asp?projectID=<%=rs("projectID")%>&customerID=<%=customerID%>&divisionID=<%=divisionID%>">Manage</a></td>
													<!--<td align="center"><a href="streetList.asp?id=<%'=rs("projectID")%>&customerID=<%'=customerID%>&divisionID=<%'=divisionID%>">List</a></td>
													<td align="center"><a href="craneList.asp?projectID=<%'=rs("projectID")%>&customerID=<%'=customerID%>&divisionID=<%'=divisionID%>">List</a></td>
													<td align="center"><a href="openLots.asp?id=<%'=rs("projectID")%>&customerID=<%'=customerID%>&divisionID=<%'=divisionID%>">Manage</a></td>	-->						
													<!--<td align="center"><a href="form.asp?id=<%'=rs("projectID")%>&formType=editRates&customerID=<%'=customerID%>&divisionID=<%'=divisionID%>">view/edit</a></td>
													<td align="center"><a href="form.asp?id=<%'=rs("projectID")%>&formType=editAlerts&customerID=<%'=customerID%>&divisionID=<%'=divisionID%>">view/edit</a></td>-->
													<td align="center"><a href="docManager.asp?projectID=<%=rs("projectID")%>">view</a></td>
													<td align="center">
														<%'check to see if there is an NOI for this project														
														Set oCmd = Server.CreateObject("ADODB.Command")	
														With oCmd
														   .ActiveConnection = DataConn
														   .CommandText = "spCheckForNOI"
														   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, rs("projectID"))
														   .CommandType = adCmdStoredProc   
														End With																	
														Set rsNOI = oCmd.Execute
														Set oCmd = nothing
														
														if not rsNOI.eof then%>
															<img src="images/check.gif">
														<%end if%>
													</td>
													
												<!--	<%'if rs("PLSGenerated") = "True" Then
														'get the winning quote for this project
													'	Set oCmd = Server.CreateObject("ADODB.Command")	
													'	With oCmd
													'	   .ActiveConnection = DataConn
													'	   .CommandText = "spGetAwardedQuote"
													'	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, rs("quoteID"))
													'	   .CommandType = adCmdStoredProc   
													'	End With																	
													'	Set rsQuote = oCmd.Execute
													'	Set oCmd = nothing%>
														<td align="center"><a href="downloads/quote_<%'=rsQuote("inspectionQuoteCustomerID")%>.pdf" target="_blank">view</a></td>
													<%'else%>
														<td align="center"></td>
													<%'end if%>
													
													<%'if rs("PLSGenerated") = "True" Then%>
														<td align="center"><a href="downloads/PLS_<%'=rs("projectID")%>.pdf" target="_blank">view</a></td>
													<%'else%>
														<td align="center"></td>
													<%'end if%>-->
													<td align="center"><a href="projectMap.asp?projectID=<%=rs("projectID")%>" title="<%=rs("projectName")%>" rel="gb_page[600, 520]">view</a></td>													
													<td align="center"><a href="percentCompleteChart.asp?projectID=<%=rs("projectID")%>" title="<%=rs("projectName")%>" rel="gb_page_fs[]">view</a></td>
													<td align="center"><a href="form.asp?id=<%=rs("projectID")%>&formType=editProject&customerID=<%=customerID%>&divisionID=<%=divisionID%>"><img src="images/edit.gif" width="19" height="19" alt="edit" border="0"></a></td>
													<!--<td align="center"><a href=""><input type="image" src="images/remove.gif" width="11" height="13" alt="delete" border="0" onClick="return confirmDelete('process.asp?id=<%'=rs("projectID")%>&processType=deleteProject&customerID=<%'=customerID%>&divisionID=<%'=divisionID%>')"></a></td>-->
													<td align="center">	
														<%If rs("isActive") = "True" Then%>
															<img src="images/check.gif" width="15" height="15">
														<%end if%>									
													</td>
												</tr>
											<%rs.movenext
											if blnChange = true then
												blnChange = false
											else
												blnChange = true
											end if
											loop
										end if%>
									</table>
									
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>