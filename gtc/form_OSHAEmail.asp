<%
reportID = request("reportID")
projName = request("projName")
iProjectID = trim(request("projectID"))
reportType = request("repType")
custName = request("custName")
divisionID=request("divisionID")
quoteID=request("quoteID")
sEmail = request("contactEmail")
clientID = request("clientID")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

sAttach = "report_" & reportID & "Ent.pdf"
sAttach2 = "report_" & reportID & ".pdf"


On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")
'get the user's signature, if available
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetUser"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, Session("ID"))
   .CommandType = adCmdStoredProc
   
End With
			
Set rsUser = oCmd.Execute
Set oCmd = nothing

'get the sub contractor reports
'	Set oCmd = Server.CreateObject("ADODB.Command")
'		
'	With oCmd
'	   .ActiveConnection = DataConn
'	   .CommandText = "spGetOSHASubsByID"
'		.parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, reportID)
'	   .CommandType = adCmdStoredProc   
'	End With
'				
'	Set rsSubs = oCmd.Execute
'	Set oCmd = nothing

%>
<SCRIPT LANGUAGE="JavaScript">
var myWind = ""
function doNew() {
	if (myWind == "" || myWind.closed || myWind.name == undefined) {
		myWind = window.open("addressBook.asp?project=<%=iProjectID%>","subWindow","HEIGHT=600,WIDTH=1000,scrollbars=yes")
	} else{
		myWind.focus();
	}
}
</SCRIPT>
<form action="process.asp" method="post" name="myForm">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sNPDESTitle%></span><span class="Header"> - Send OSHA/EPA Email</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td>
												<strong>Project Name:</strong> <%=projName%>
											</td>
											<td></td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td>
												<table border="0" cellpadding="0" cellspacing="0">
													<tr>
														<td colspan="3">
															<strong>Check below to send as an attachment or click to print.</strong> <br />
															<%select case reportType
																case 1
																	reportType = "Mock OSHA"
																case 2
																	reportType = "Mock EPA"
															end select%>
															<input type="checkbox" name="reportPDFUnb" checked="checked"/> <strong><a href="downloads/<%=sAttach%>" target="_blank">Unbroken Report #<%=reportID%>  (<%=reportType%>)</a></strong><br />
															<input type="checkbox" name="reportPDF"/> <strong><a href="downloads/<%=sAttach2%>" target="_blank">Separated Report #<%=reportID%>  (<%=reportType%>)</a></strong><br />
															<br />
															Separate multiple recipients' email addresses with a semi-colon (;). 
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td><input type="button" name="storage" value="To:.." onClick="doNew()" class="formButton"></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="emailTo" size="50" value="<%=sEmail%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>CC:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="emailCC" size="50">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Bcc:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="emailBCC" size="50">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>	
													<tr>
														<td><strong>Subject:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="subject" size="50" value="<%=custName & " - " & projName & " - " & reportType & " - " & date()%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>			
													<tr>
														<td valign="top"><strong>Message:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%
																strFormName = "myForm"
																strTextAreaName = "smessage"
															%>
															<%'if reportType <> "Daily" then response.Write sOIBody end if%>
															<textarea name="smessage" cols="70" rows="10"><br /><br><%=rsUser("emailSignature")%></textarea>
															<script>
																//STEP 2: Replace the textarea (txtContent)
																oEdit1 = new InnovaEditor("oEdit1");
																oEdit1.features=["FullScreen","Preview","Print","Search",
																	"Cut","Copy","Paste","PasteWord","PasteText","|","Undo","Redo","|",
																	"ForeColor","BackColor","|","Bookmark","Hyperlink","XHTMLSource","BRK",
																	"Numbering","Bullets","|","Indent","Outdent","LTR","RTL","|",
																	"Image","Flash","Media","|","Table","Guidelines","Absolute","|",
																	"Characters","Line","Form","RemoveFormat","ClearAll","BRK",
																	"StyleAndFormatting","TextFormatting","ListFormatting","BoxFormatting",
																	"ParagraphFormatting","CssText","|",
																	"Paragraph","FontName","FontSize","|",
																	"Bold","Italic","Underline","Strikethrough","Superscript","Subscript","|",
																	"JustifyLeft","JustifyCenter","JustifyRight","JustifyFull"];
																oEdit1.REPLACE("smessage");
															</script>
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td colspan="3" align="right">
															<input type="hidden" name="clientID" value="<%=clientID%>" />
															<input type="hidden" name="reportID" value="<%=reportID%>" />
															<input type="hidden" name="processType" value="sendOSHAEmail" />
															<input type="hidden" name="attachment1" value="<%=sAttach%>" />
															<input type="hidden" name="projName" value="<%=projName%>" />
															<input type="submit" value="Send Email" class="formButton"/>
														</td>
													</tr>
												</table>
											</td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
										</tr>
									</table>
									
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>