<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
Dim rs, oCmd, DataConn

classID = Request("classID")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

Set oCmd = Server.CreateObject("ADODB.Command")	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetClass"
   .parameters.Append .CreateParameter("@eventID", adInteger, adParamInput, 8, classID)
   .CommandType = adCmdStoredProc   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rs = oCmd.Execute
Set oCmd = nothing

%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
</head>
<body>
<form name="openItems" method="post" action="process.asp">
<table bgcolor="#FFFFFF" width="900" border="0" cellpadding="0" cellspacing="0">
	<tr bgcolor="#FFFFFF">
		<td colspan="3" class="colorBars">
			&nbsp;&nbsp;<a href="assignStudents.asp?classID=<%=rs("classID")%>&customerID=<%=rs("customerID")%>" class="footerLink">manage/assign students</a>&nbsp;&nbsp;
		</td>
	</tr>
	<tr>
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td align="right">
												<strong>Customer:</strong>&nbsp;
											</td>
											<td>
												<%if rs("customerID") = 0 then
													response.Write "Open Class"
												else
													response.Write rs("customerName")
												end if%>
											</td>
										</tr>
										<tr><td align="right"><strong>Class:</strong>&nbsp;</td><td><%=rs("classType")%></td></tr>
										<tr><td align="right"><strong>Class Size:</strong>&nbsp;</td><td>Up to <%=rs("classSize")%></td></tr>
										<tr><td align="right"><strong>Open Slots:</strong>&nbsp;</td><td><%=rs("openSlots")%></td></tr>
										<tr>
											<td align="right"><strong>Available Slots:</strong>&nbsp;</td>
											<td>
												<%Set oCmd = Server.CreateObject("ADODB.Command")
	
												With oCmd
												   .ActiveConnection = DataConn
												   .CommandText = "spGetNumStudentsAssigned"
												   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, rs("classID"))
												   .CommandType = adCmdStoredProc
												   
												End With
															
												Set rsNumOpen = oCmd.Execute
												Set oCmd = nothing
												
												iNumAvail = rs("openSlots") - rsNumOpen("numSlots")
												
												response.Write iNumAvail%>
											</td>
										</tr>
										<tr><td align="right"><strong># Student Manuals:</strong>&nbsp;</td><td><%=rs("numberManuals")%></td></tr>
										<tr><td align="right"><strong>Start Date/Time:</strong>&nbsp;</td><td><%=rs("startDate")%>&nbsp;<%=rs("startTime")%></td></tr>
										<tr><td align="right"><strong>End Date/Time:</strong>&nbsp;</td><td><%=rs("endDate")%>&nbsp;<%=rs("endTime")%></td></tr>
										<tr>
											<td align="right"><strong>Evening/Split Class:</strong>&nbsp;</td>
											<td>
												<%if rs("eveningClass") = "True" then%>
													Yes
												<%else%>
													No
												<%end if%>
											</td>
										</tr>
										<tr>
											<td align="right"><strong>Saturday Class:</strong>&nbsp;</td>
											<td>
												<%if rs("saturdayClass") = "True" then%>
													Yes
												<%else%>
													No
												<%end if%>
											</td>
										</tr>
										<tr>
											<td align="right"><strong>Association Discount:</strong>&nbsp;</td>
											<td>
												<%if rs("associationDiscount") = "True" then%>
													Yes
												<%else%>
													No
												<%end if%>
											</td>
										</tr>
										<tr><td align="right"><strong>Discount Amount:</strong>&nbsp;</td><td><%=formatcurrency(rs("associationDiscountAmt"),2)%></td></tr>
										<tr><td align="right"><strong>Location:</strong>&nbsp;</td><td><%=rs("location")%></td></tr>
										<tr><td align="right"><strong>Address 1:</strong>&nbsp;</td><td><%=rs("address1")%></td></tr>
										<tr><td align="right"><strong>Address 2:</strong>&nbsp;</td><td><%=rs("address2")%></td></tr>
										<tr><td align="right"><strong>City:</strong>&nbsp;</td><td><%=rs("city")%></td></tr>
										<tr><td align="right"><strong>State:</strong>&nbsp;</td><td><%=rs("state")%></td></tr>
										<tr><td align="right"><strong>Zip:</strong>&nbsp;</td><td><%=rs("zip")%></td></tr>
									</table><br>
									
									
									<%
									Set oCmd = Server.CreateObject("ADODB.Command")
	
									With oCmd
									   .ActiveConnection = DataConn
									   .CommandText = "spGetStudentClass"
									   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, classID)
									   .CommandType = adCmdStoredProc
									   
									End With
												
									Set rsStudentClass = oCmd.Execute
									Set oCmd = nothing
									%>
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td colspan="9">
												<b>Assigned Students</b>												
											</td>
										</tr>
										<tr bgcolor="#666666">
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td><span class="searchText">Student Name</span></td>
											<td><span class="searchText">Email</span></td>
											<td align="center"><span class="searchText">Attend</span></td>
											<td align="center"><span class="searchText">Remove</span></td>
										</tr>
										<tr><td colspan="9"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<%If rsStudentClass.eof then%>
											<tr><td></td><td colspan="7">there are no students assigned to this class</td></tr>
										<%else
											blnChange = true
											Do until rsStudentClass.eof
												If blnChange = true then%>
													<tr class="rowColor">
												<%else%>
													<tr>
												<%end if%>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td><%=rsStudentClass("firstName")%>&nbsp;<%=rsStudentClass("lastName")%></td>
													<td><a href="mailto:<%=rsStudentClass("email")%>"><%=rsStudentClass("email")%></a></td>
													<td align="center" valign="middle">
														<%if rsStudentClass("attended") = "False" then%>
															<a href="process.asp?processType=attendClass&studentID=<%=rsStudentClass("studentID")%>&classID=<%=classID%>&customerID=<%=customerID%>&clientID=<%=clientID%>&sType=true&sRefer=cal"><img src="images/checkbox.gif" border="0"></a>
														<%else%>
															<a href="process.asp?processType=attendClass&studentID=<%=rsStudentClass("studentID")%>&classID=<%=classID%>&customerID=<%=customerID%>&clientID=<%=clientID%>&sType=false&sRefer=cal"><img src="images/checked.gif" border="0"></a>
														<%end if%>
													</td>
													<td align="center" valign="middle">
														<a href="process.asp?processType=removeStudent&studentID=<%=rsStudentClass("studentID")%>&classID=<%=classID%>&customerID=<%=customerID%>&clientID=<%=clientID%>&sRefer=cal"><img src="images/remove.gif" border="0"></a>
													</td>

												</tr>
											<%rsStudentClass.movenext
											if blnChange = true then
												blnChange = false
											else
												blnChange = true
											end if
											loop
										end if%>
									</table>
															
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<tr>
		<td colspan="3" bgcolor="#3C2315">
			<img src="images/pix.gif" width="5" height="10">
		</td>
	</tr>
</table>
</form>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>