<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
clientID=request("clientID")
iCustomerID = request("customer")
iDivisionID = request("division")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 	
DataConn.Open Session("Connection"), Session("UserID")

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetAllCustomersForClient"
   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
   .CommandType = adCmdStoredProc   
End With
			
Set rsCustomer = oCmd.Execute
Set oCmd = nothing



If iCustomerID <> "" then
	'get the division list
	Set oCmd = Server.CreateObject("ADODB.Command")
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetDivisionsByCustomer" 'get the divisions based on the customer
	  ' .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, Session("ID"))
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iCustomerID)
	   .CommandType = adCmdStoredProc   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
				
	Set rsDivisions = oCmd.Execute
	Set oCmd = nothing
	
end if


If iDivisionID <> "" then
	'get the project list
	Set oCmd = Server.CreateObject("ADODB.Command")
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetProjectsByDivision" 'get the divisions based on the customer and user
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iDivisionID)
	   '.parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, Session("ID"))
	   .CommandType = adCmdStoredProc   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
				
	Set rsProjects = oCmd.Execute
	Set oCmd = nothing

end if
%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script language="javascript" src="includes/datePicker.js"></script>
<link rel="stylesheet" href="includes/datePicker.css" type="text/css">
<script language="javascript">
<!--
function dept_onchange(reportList) {
   document.reportList.action = "chooseRecWO.asp";
   reportList.submit(); 
}

function sendme() 
{ 
    window.open("","myNewWin","scrollbars=yes,resizable=yes,width=1000,height=800"); 
    var a = window.setTimeout("document.reportList.submit();",100); 
} 

//-->
</script>
</head>
<body>
<form name="reportList" method="post" action="recWOReportResults.asp" >
<input type="hidden" name="clientID" value="<%=clientID%>">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Work Order Reconciliation</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td>
												Select project to run reconciliation report on.<br>
												<select name="customer" onChange="return dept_onchange(reportList)">
													<option value="">--Select Customer--</option>	
													<%do until rsCustomer.eof
														If trim(rsCustomer("customerID")) = trim(iCustomerID) then%>
															<option selected="selected" value="<%=rsCustomer("customerID")%>"><%=rsCustomer("customerName")%></option>
														<%else%>
															<option value="<%=rsCustomer("customerID")%>"><%=rsCustomer("customerName")%></option>
														<%end if
													rsCustomer.movenext
													loop%>
												</select> &nbsp;&nbsp;&nbsp;&nbsp;
												
												<select name="division" onChange="return dept_onchange(reportList)">
													<option value="">--Select Division--</option>										
													<%If iCustomerID <> "" then	
													do until rsDivisions.eof
														If trim(rsDivisions("divisionID")) = trim(iDivisionID) then%>
															<option selected="selected" value="<%=rsDivisions("divisionID")%>"><%=rsDivisions("division")%></option>
														<%else%>
															<option value="<%=rsDivisions("divisionID")%>"><%=rsDivisions("division")%></option>
														<%end if
													rsDivisions.movenext
													loop
													end if%>
												</select><br><br>
												
												<select name="projectID">
													<%If iDivisionID <> "" then%>	
													<option value="All">--All Projects--</option>
													<%do until rsProjects.eof%>
															<option value="<%=rsProjects("projectID")%>"><%=rsProjects("projectName")%></option>
													<%rsProjects.movenext
													loop
													end if%>
												</select> &nbsp;&nbsp;&nbsp;&nbsp;
												
												<select name="openClosed">
													<option value="False">Open</option>
													<option value="True">Closed</option>
												</select>&nbsp;&nbsp;&nbsp;&nbsp;
												
											<!--	From: <input type="text" name="fromDate" maxlength="10" size="10" value="<%'=formatdatetime(dateadd("d", -7,now()),2)%>"/>&nbsp;<a href="javascript:displayDatePicker('fromDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a> &nbsp;&nbsp;&nbsp;&nbsp;    
												To: <input type="text" name="toDate" maxlength="10" size="10" value="<%'=formatdatetime(now(),2)%>"/>&nbsp;<a href="javascript:displayDatePicker('toDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a> &nbsp;&nbsp;&nbsp;&nbsp;
											-->
											
												<%If iCustomerID <> "" and iDivisionID <> "" then%>
													<input type="submit" value=" Select " class="formButton"> 
												<%else%>
													<input type="submit" value=" Select " class="formButton" disabled="disabled"> 
												<%end if%>
											</td>
										</tr>
									</table>
															
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
</body>
</html>
<%
rsCustomer.close
DataConn.close
Set rsCustomer = nothing
Set DataConn = nothing
Set oCmd = nothing
%>