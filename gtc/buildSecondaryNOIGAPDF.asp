<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
secondaryFormID = request("secondaryFormID")

Session("LoggedIn") = "True"

Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetSecondaryNOI"
    .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, secondaryFormID) 'secondaryFormID
   .CommandType = adCmdStoredProc
   
End With
			
Set rsNOI = oCmd.Execute
Set oCmd = nothing
%>
<html>
<head>
<title>SWSIR</title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/report.css" type="text/css">
</head>
<body>

<table width=650 cellpadding=0 cellspacing=0>
	<tr>
		<td align="right">
			<table width="130" border="0" cellpadding="2" cellspacing="1" style="border-color:#000000;border-style:solid;border-width:thin;">
				<tr><td height="10">&nbsp;</td></tr>
			</table>
			For Official Use Only
		</td>
	</tr>
	<tr>
		<td align="center">
			<strong>NOTICE OF INTENT<br><br>

			VERSION 2008<br><br>
			
			State of Georgia<br>
			Department of Natural Resources<br>
			Environmental Protection Division<br>
			
			For Coverage Under the 2008 Re-Issuance of the<br>
			NPDES General Permits No. GAR100003 To Discharge Storm Water<br>
			Associated With Construction Activity for Common Developments<br><br>
			
			SECONDARY PERMITTEE</strong>

		</td>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=20></td></tr>
	<tr>
		<td>
			<strong>NOTICE OF INTENT (Check Only One):</strong><br><br>
			<%select case rsNOI("noticeOfIntent")
				Case 1%>
					<table cellpadding="0" cellspacing="0">
						<tr>
							<td><img src="images/checked.gif">&nbsp;&nbsp;</td>
							<td>Initial Notification (New Facility/Construction Site)</td>
						</tr>
						<tr>
							<td><img src="images/checkbox.gif">&nbsp;</td>
							<td>Re-Issuance Notification (Existing Facility/Construction Site)</td>
						</tr>
						<tr>
							<td><img src="images/checkbox.gif">&nbsp;</td>
							<td>Change of Information (Applicable only if the NOI was submitted after August 1, 2008)</td>
						</tr>
					</table>
				<%case 2%>
					<table cellpadding="0" cellspacing="0">
						<tr>
							<td><img src="images/checkbox.gif">&nbsp;</td>
							<td>Initial Notification (New Facility/Construction Site)</td>
						</tr>
						<tr>
							<td><img src="images/checked.gif">&nbsp;&nbsp;</td>
							<td>Re-Issuance Notification (Existing Facility/Construction Site)</td>
						</tr>
						<tr>
							<td><img src="images/checkbox.gif">&nbsp;</td>
							<td>Change of Information (Applicable only if the NOI was submitted after August 1, 2008)</td>
						</tr>
					</table>
				<%case 3%>
					<table cellpadding="0" cellspacing="0">
						<tr>
							<td><img src="images/checkbox.gif">&nbsp;</td>
							<td>Initial Notification (New Facility/Construction Site)</td>
						</tr>
						<tr>
							<td><img src="images/checkbox.gif">&nbsp;</td>
							<td>Re-Issuance Notification (Existing Facility/Construction Site)</td>
						</tr>
						<tr>
							<td><img src="images/checked.gif">&nbsp;&nbsp;</td>
							<td>Change of Information (Applicable only if the NOI was submitted after August 1, 2008)</td>
						</tr>
					</table>
			<%end select%>
		</td>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=15></td></tr>
	<tr>
		<td>
			<strong>I.	SITE/SECONDARY PERMITTEE INFORMATION</strong><br><br>
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td colspan="2">Project Construction Site Name:&nbsp;<u><%=rsNOI("siteProjectName")%></u></td>
				</tr>
				<tr><td colspan="2" height="5"></td></tr>
				<tr>
					<td colspan="2">Construction Site Location (<em>information must be sufficient to accurately locate the construction site</em>): <u><%=rsNOI("siteLocation")%></u></td>
				</tr>
				<tr><td colspan="2" height="5"></td></tr>
				<tr>
					<td colspan="2">Subdivision Name (I):&nbsp;<u><%=rsNOI("subdivision")%></u>&nbsp;&nbsp;&nbsp;Lot Number(s) (<em>if applicable</em>):&nbsp;<u><%=rsNOI("lotNumber")%></u></td>
				</tr>
				<tr><td colspan="2" height="5"></td></tr>
				<tr>
					<td colspan="2">Common Development Name:&nbsp;<u><%=rsNOI("commonDevelopmentName")%></u></td>
				</tr>
				<tr><td colspan="2" height="5"></td></tr>
				<tr>
					<td colspan="2">Construction Site Street Address:&nbsp;<u><%=rsNOI("constructionSiteStreetAddress")%></u></td>
				</tr>
				<tr><td colspan="2" height="5"></td></tr>
				<tr>
					<td colspan="2">City (<em>if site is located within jurisdictional boundaries of municipality</em>):&nbsp;<u><%=rsNOI("city")%></u></td>
				</tr>
				<tr><td colspan="2" height="5"></td></tr>
				<tr><td colspan="2">County:&nbsp;<u><%=rsNOI("county")%></u></td></tr>
				<tr><td colspan="2" height="5"></td></tr>
				<tr>
					<td colspan="2">Secondary Permittee�s Name:&nbsp;<u><%=rsNOI("secondaryPermitteeName")%></u>&nbsp;&nbsp;&nbsp;Phone:&nbsp;<u><%=rsNOI("permitteePhone")%></u></td>
				</tr>
				<tr><td colspan="2" height="5"></td></tr>
				<tr>
					<td colspan="2">Address:&nbsp;<u><%=rsNOI("permitteeAddress")%></u>&nbsp;&nbsp;&nbsp;City:&nbsp;<u><%=rsNOI("permitteeCity")%></u>&nbsp;&nbsp;&nbsp;State:&nbsp;<u><%=rsNOI("permitteeState")%></u>&nbsp;&nbsp;&nbsp;Zip Code:&nbsp;<u><%=rsNOI("permitteeZip")%></u></td>
				</tr>
				<tr><td colspan="2" height="5"></td></tr>
				<tr>
					<td colspan="2">Duly Authorized Representative (<em>optional</em>):&nbsp;<u><%=rsNOI("authorizedRep")%></u>&nbsp;&nbsp;&nbsp;Phone:&nbsp;<u><%=rsNOI("authorizedRepPhone")%></u></td>
				</tr>
				<tr><td colspan="2" height="5"></td></tr>
				<tr>
					<td colspan="2">Facility/Construction Site Contact:&nbsp;<u><%=rsNOI("facilityContact")%></u>&nbsp;&nbsp;&nbsp;Phone:&nbsp;<u><%=rsNOI("facilityContactPhone")%></u></td>
				</tr>
				<tr><td colspan="2" height="5"></td></tr>
				<tr>
					<td colspan="2">Primary Permittee�s Name:&nbsp;<u><%=rsNOI("primaryPermitteeName")%></u>&nbsp;&nbsp;&nbsp;Phone:&nbsp;<u><%=rsNOI("primaryPermitteePhone")%></u></td>
				</tr>
				<tr><td colspan="2" height="5"></td></tr>
				<tr>
					<td colspan="2">Address:&nbsp;<u><%=rsNOI("primaryPermitteeAddress")%></u>&nbsp;&nbsp;&nbsp;City:&nbsp;<u><%=rsNOI("primaryPermitteeCity")%></u>&nbsp;&nbsp;&nbsp;State:&nbsp;<u><%=rsNOI("primaryPermitteeState")%></u>&nbsp;&nbsp;&nbsp;Zip Code:&nbsp;<u><%=rsNOI("primaryPermitteeZip")%></u></td>
				</tr>
				<tr><td colspan="2" height="5"></td></tr>
			</table>
		</td>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=15></td></tr>
	<tr>
		<td>
			<strong>II.	CONSTRUCTION SITE ACTIVITY INFORMATION</strong><br><br>
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td>Start Date <em>(month/day/year)</em>:&nbsp;<u><%=rsNOI("startDate")%></u></td>
					<td width="50"></td>
					<td>Completion Date <em>(month/day/year)</em>:&nbsp;<u><%=rsNOI("completionDate")%></u></td>
				</tr>
				<tr><td colspan="3" height="5"></td></tr>
				<tr>
					<td colspan="3">Estimated Disturbed Acreage (<em>acres, to the nearest tenth (1/10th) acre</em>):&nbsp;<u><%=rsNOI("estimatedDisturbedAcerage")%></u></td>
				</tr>
				<tr><td colspan="2" height="5"></td></tr>
				<tr>
					<td colspan="3">
						Will the Secondary Permittee disturb more than 50 acres at any one time? (<em>Check Only One</em>):<br>
						
						<%select case rsNOI("disturb50")
							Case 1%>
								<img src="images/checked.gif">&nbsp;YES<br>
								<img src="images/checkbox.gif">&nbsp;NO<br>
								<img src="images/checkbox.gif">&nbsp;N/A � if the Plan was submitted prior to the effective date of the General NPDES <br>&nbsp;&nbsp;&nbsp;&nbsp;Permit No. GAR100003 for Common Development construction activities.<br>
							
							<%case 2%>
								<img src="images/checkbox.gif">&nbsp;YES<br>
								<img src="images/checked.gif">&nbsp;NO<br>
								<img src="images/checkbox.gif">&nbsp;N/A � if the Plan was submitted prior to the effective date of the General NPDES <br>&nbsp;&nbsp;&nbsp;&nbsp;Permit No. GAR100003 for Common Development construction activities.<br>
							<%case 3%>
								<img src="images/checkbox.gif">&nbsp;YES<br>
								<img src="images/checkbox.gif">&nbsp;NO<br>
								<img src="images/checked.gif">&nbsp;N/A � if the Plan was submitted prior to the effective date of the General NPDES <br>&nbsp;&nbsp;&nbsp;&nbsp;Permit No. GAR100003 for Common Development construction activities.<br>
						<%end select%>
					</td>
				</tr>
				<tr><td colspan="2" height="15"></td></tr>
				<tr>
					<td valign="top">Construction Activity Type:</td>
					<td colspan="2">
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>
									<%typeConstructionCommercial = checkNullValue(rsNOI("typeConstructionCommercial"))%>
									<%if typeConstructionCommercial = "True" then%>
										<img src="images/checked.gif">&nbsp;Commercial
									<%else%>
										<img src="images/checkbox.gif">&nbsp;Commercial
									<%end if%>									
								</td>
								<td width="40"></td>
								<td>
									<%typeConstructionIndustrial = checkNullValue(rsNOI("typeConstructionIndustrial"))%>
									<%if typeConstructionIndustrial = "True" then%>
										<img src="images/checked.gif">&nbsp;Industrial
									<%else%>
										<img src="images/checkbox.gif">&nbsp;Industrial
									<%end if%>									
								</td>
								<td width="40"></td>
								<td>
									<%typeConstructionMunicipal = checkNullValue(rsNOI("typeConstructionMunicipal"))%>
									<%if typeConstructionMunicipal = "True" then%>
										<img src="images/checked.gif">&nbsp;Municipal
									<%else%>
										<img src="images/checkbox.gif">&nbsp;Municipal
									<%end if%>										
								</td>
							</tr>							
							<tr>
								<td>
									<%typeConstructionLinear = checkNullValue(rsNOI("typeConstructionLinear"))%>
									<%if typeConstructionLinear = "True" then%>
										<img src="images/checked.gif">&nbsp;Linear
									<%else%>
										<img src="images/checkbox.gif">&nbsp;Linear
									<%end if%>										
								</td>
								<td></td>
								<td>
									<%typeConstructionUtility = checkNullValue(rsNOI("typeConstructionUtility"))%>
									<%if typeConstructionUtility = "True" then%>
										<img src="images/checked.gif">&nbsp;Utility
									<%else%>
										<img src="images/checkbox.gif">&nbsp;Utility
									<%end if%>									
								</td>
								<td></td>
								<td>			
									<%typeConstructionResidential = checkNullValue(rsNOI("typeConstructionResidential"))%>						
									<%if typeConstructionResidential = "True" then%>
										<img src="images/checked.gif">&nbsp;Residential
									<%else%>
										<img src="images/checkbox.gif">&nbsp;Residential
									<%end if%>	
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=15></td></tr>
	<tr>
		<td>
			<strong>III.	RECEIVING WATER INFORMATION</strong><br><br>
			<table width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td colspan="4">A.&nbsp;&nbsp;Name of Initial Receiving Water(s):&nbsp;<u><%=rsNOI("IRWName")%></u></td>
				</tr>
				<tr><td colspan="4" height="5"></td></tr>
				<tr>
					<td colspan="4" align="center">
						<%if rsNOI("IRWTrout") = "True" then%>
							<img src="images/checked.gif">&nbsp;Trout Stream
						<%else%>
							<img src="images/checkbox.gif">&nbsp;Trout Stream
						<%end if%>&nbsp;&nbsp;&nbsp;
						<%if rsNOI("IRWWarmWater") = "True" then%>
							<img src="images/checked.gif">&nbsp;Warm Water Fisheries Stream
						<%else%>
							<img src="images/checkbox.gif">&nbsp;Warm Water Fisheries Stream
						<%end if%>
					</td>
				</tr>
				<tr><td colspan="4" height="5"></td></tr>
				<tr>
					<td colspan="4">B.&nbsp;&nbsp;Name of MS4 Owner/Operator (if applicable):&nbsp;<u><%=rsNOI("MSSSOwnerOperator")%></u></td>
				</tr>
				<tr><td colspan="4" height="5"></td></tr>
				<tr>
					<td colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Name of Receiving Water(s):&nbsp;<u><%=rsNOI("RWName")%></u></td>
				</tr>
				<tr><td colspan="4" height="5"></td></tr>
				<tr>
					<td colspan="4" align="center">
						<%if rsNOI("MSSSTrout") = "True" then%>
							<img src="images/checked.gif">&nbsp;Trout Stream
						<%else%>
							<img src="images/checkbox.gif">&nbsp;Trout Stream
						<%end if%>&nbsp;&nbsp;&nbsp;
						<%if rsNOI("MSSSWarmWater") = "True" then%>
							<img src="images/checked.gif">&nbsp;Warm Water Fisheries Stream
						<%else%>
							<img src="images/checkbox.gif">&nbsp;Warm Water Fisheries Stream
						<%end if%>
					</td>
				</tr>
				<tr><td colspan="4" height="5"></td></tr>
				<tr>
					<td colspan="4">
						<table cellpadding="0" cellspacing="0">
							<tr>
								<td valign="top">C.&nbsp;&nbsp;</td>
								<td>
									Does the facility/construction site discharge storm water into an Impaired Stream Segment, or within one (1) linear mile upstream of and within 
									the same watershed as, any portion of an Impaired Stream Segment identified as �not supporting� its designated use(s), as shown on Georgia�s 
									2008 and subsequent �305(b)/303(d) List Documents (Final)� listed for the criteria violated, �Bio F� (Impaired Fish Community) and/or �Bio M� 
									(Impaired Macroinvertebrate Community), within Category 4a, 4b or 5, and the potential cause is either �NP� (nonpoint source) or 
									�UR� (urban runoff)?  (<em>Check Only One</em>):<br>
									
									<%select case rsNOI("siteDischargeStormWater")
										Case 1%>
											<img src="images/checked.gif">&nbsp;YES, Name of Impaired Stream Segment(s):&nbsp;<u><%=rsNOI("impairedStream")%></u><br>
											<img src="images/checkbox.gif">&nbsp;NO<br>
											<img src="images/checkbox.gif">&nbsp;N/A � if the NOI was submitted within 90 days after the effective date of the General <br>&nbsp;&nbsp;&nbsp;&nbsp;NPDES Permit No. GAR100003 for Common Development construction activities.<br>
											<img src="images/checkbox.gif">&nbsp;N/A � if the secondary permittees are utility companies and utility contractors <br>&nbsp;&nbsp;&nbsp;&nbsp;and implementing the applicable best management practices detailed in the primary <br>&nbsp;&nbsp;&nbsp;&nbsp;permittee�s Plan.<br>
											
										<%case 2%>
											<img src="images/checkbox.gif">&nbsp;YES, Name of Impaired Stream Segment(s):<br>
											<img src="images/checked.gif">&nbsp;NO<br>
											<img src="images/checkbox.gif">&nbsp;N/A � if the NOI was submitted within 90 days after the effective date of the General <br>&nbsp;&nbsp;&nbsp;&nbsp;NPDES Permit No. GAR100003 for Common Development construction activities.<br>
											<img src="images/checkbox.gif">&nbsp;N/A � if the secondary permittees are utility companies and utility contractors <br>&nbsp;&nbsp;&nbsp;&nbsp;and implementing the applicable best management practices detailed in the primary <br>&nbsp;&nbsp;&nbsp;&nbsp;permittee�s Plan.<br>
										<%case 3%>
											<img src="images/checkbox.gif">&nbsp;YES, Name of Impaired Stream Segment(s):<br>
											<img src="images/checkbox.gif">&nbsp;NO<br>
											<img src="images/checked.gif">&nbsp;N/A � if the NOI was submitted within 90 days after the effective date of the General <br>&nbsp;&nbsp;&nbsp;&nbsp;NPDES Permit No. GAR100003 for Common Development construction activities.<br>
											<img src="images/checkbox.gif">&nbsp;N/A � if the secondary permittees are utility companies and utility contractors <br>&nbsp;&nbsp;&nbsp;&nbsp;and implementing the applicable best management practices detailed in the primary <br>&nbsp;&nbsp;&nbsp;&nbsp;permittee�s Plan.<br>
										<%case 4%>
											<img src="images/checkbox.gif">&nbsp;YES, Name of Impaired Stream Segment(s):<br>
											<img src="images/checkbox.gif">&nbsp;NO<br>
											<img src="images/checkbox.gif">&nbsp;N/A � if the NOI was submitted within 90 days after the effective date of the General <br>&nbsp;&nbsp;&nbsp;&nbsp;NPDES Permit No. GAR100003 for Common Development construction activities.<br>
											<img src="images/checked.gif">&nbsp;N/A � if the secondary permittees are utility companies and utility contractors <br>&nbsp;&nbsp;&nbsp;&nbsp;and implementing the applicable best management practices detailed in the primary <br>&nbsp;&nbsp;&nbsp;&nbsp;permittee�s Plan.<br>
									<%end select%>
								</td>
							</tr>
							<tr><td colspan="4" height="10"></td></tr>
							<tr>
								<td valign="top">D.&nbsp;&nbsp;</td>
								<td>
									Does the facility/construction site discharge storm water into an Impaired Stream Segment where a Total Maximum Daily Load (TMDL) Implementation 
									Plan for �sediment� was finalized at least six (6) months prior to the submittal of the NOI? (<i>Check Only One</i>):<br>
									
									<%select case rsNOI("siteDischargeStormWater2")
										Case 1%>
											<img src="images/checked.gif">&nbsp;YES, Name of Impaired Stream Segment(s):&nbsp;<u><%=rsNOI("impairedStream2")%></u><br>
											<img src="images/checkbox.gif">&nbsp;NO<br>
											<img src="images/checkbox.gif">&nbsp;N/A � if the NOI was submitted within 90 days after the effective date of the General <br>&nbsp;&nbsp;&nbsp;&nbsp;NPDES Permit No. GAR100003 for Common Development construction activities.<br>
											<img src="images/checkbox.gif">&nbsp;N/A � if the secondary permittees are utility companies and utility contractors, and <br>&nbsp;&nbsp;&nbsp;&nbsp;implementing the applicable best management practices detailed in the primary <br>&nbsp;&nbsp;&nbsp;&nbsp;permittee�s Plan.<br>
											
										<%case 2%>
											<img src="images/checkbox.gif">&nbsp;YES, Name of Impaired Stream Segment(s):<br>
											<img src="images/checked.gif">&nbsp;NO<br>
											<img src="images/checkbox.gif">&nbsp;N/A � if the NOI was submitted within 90 days after the effective date of the General <br>&nbsp;&nbsp;&nbsp;&nbsp;NPDES Permit No. GAR100003 for Common Development construction activities.<br>
											<img src="images/checkbox.gif">&nbsp;N/A � if the secondary permittees are utility companies and utility contractors, and <br>&nbsp;&nbsp;&nbsp;&nbsp;implementing the applicable best management practices detailed in the primary <br>&nbsp;&nbsp;&nbsp;&nbsp;permittee�s Plan.<br>
										<%case 3%>
											<img src="images/checkbox.gif">&nbsp;YES, Name of Impaired Stream Segment(s):<br>
											<img src="images/checkbox.gif">&nbsp;NO<br>
											<img src="images/checked.gif">&nbsp;N/A � if the NOI was submitted within 90 days after the effective date of the General <br>&nbsp;&nbsp;&nbsp;&nbsp;NPDES Permit No. GAR100003 for Common Development construction activities.<br>
											<img src="images/checkbox.gif">&nbsp;N/A � if the secondary permittees are utility companies and utility contractors, and <br>&nbsp;&nbsp;&nbsp;&nbsp;implementing the applicable best management practices detailed in the primary <br>&nbsp;&nbsp;&nbsp;&nbsp;permittee�s Plan.<br>
										<%case 4%>
											<img src="images/checkbox.gif">&nbsp;YES, Name of Impaired Stream Segment(s):<br>
											<img src="images/checkbox.gif">&nbsp;NO<br>
											<img src="images/checkbox.gif">&nbsp;N/A � if the NOI was submitted within 90 days after the effective date of the General <br>&nbsp;&nbsp;&nbsp;&nbsp;NPDES Permit No. GAR100003 for Common Development construction activities.<br>
											<img src="images/checked.gif">&nbsp;N/A � if the secondary permittees are utility companies and utility contractors, and <br>&nbsp;&nbsp;&nbsp;&nbsp;implementing the applicable best management practices detailed in the primary <br>&nbsp;&nbsp;&nbsp;&nbsp;permittee�s Plan.<br>
									<%end select%>
								</td>
							</tr>
						</table>
						
					</td>
				</tr>
				<tr><td colspan="4" height="5"></td></tr>
			</table>
		</td>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=15></td></tr>
	<tr>
		<td>
			<strong>IV.	ATTACHMENTS (Applicable Only to New Facilities/Construction Sites)</strong><br><br>
			Indicate if the items listed below are attached to this Notice of Intent:<br><br>
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td width="20"></td>
					<td valign="top">
						<%writtenAuth = checkNullValue(rsNOI("writtenAuth"))%>
						<%if writtenAuth = "True" then%>
							<img src="images/checked.gif">
						<%else%>
							<img src="images/checkbox.gif">
						<%end if%>	
					</td>
					<td width="5"></td>
					<td valign="top">
						Written authorization from the appropriate EPD District Office if the Plan disturbs more than 50 acres  
						at any one time for each individual permittee (i.e., primary, secondary or tertiary permittees), or more  than 50 
						contiguous acres total at any one time (applicable only to General NPDES Permits No. GAR100001 and No. GAR100003).
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=15></td></tr>
	<tr>
		<td>
			<strong>V.	CERTIFICATIONS (Owner or Operator or Both to Initial as Applicable)</strong><br><br>
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td width="20"></td>
					<td valign="top">
						<%if rsNOI("certify1") <> "" then%>
							<u><%=rsNOI("certify1")%></u>
						<%else%>
							___
						<%end if%>	
					</td>
					<td width="5"></td>
					<td valign="top">
						I certify that I will adhere to the Primary Permittee�s Erosion, Sedimentation and Pollution Control Plan (Plan) or the portion of the 
						Plan applicable to my construction activities.
					</td>
				</tr>
				<tr><td colspan="4" height="5"></td></tr>
				<tr>
					<td width="20"></td>
					<td valign="top">
						<%if rsNOI("certify2") <> "" then%>
							<u><%=rsNOI("certify2")%></u>
						<%else%>
							___
						<%end if%>	
					</td>
					<td width="5"></td>
					<td valign="top">
						I certify under penalty of law that this document and all attachments were prepared under my direction or supervision in accordance with a system 
						designed to assure that certified personnel properly gather and evaluate the information submitted.  Based upon my inquiry of the person or persons 
						who manage the system, or those persons directly responsible for gathering the information, the information submitted is, to the best of my knowledge 
						and belief, true, accurate and complete.  I am aware that there are significant penalties for submitting false information, including the possibility 
						of fines and imprisonment for knowing violations.
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=15></td></tr>
	<tr>
		<td>
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td>Secondary Permittee�s Printed Name:&nbsp;<u><%=rsNOI("printedName")%></u></td>
					<td width="100"></td>
					<td>Title:&nbsp;<u><%=rsNOI("title")%></u></td>
				</tr>
				<tr><td colspan="3" height="10"></td></tr>
				<tr>
					<td>Signature:&nbsp;__________________</td>
					<td></td>
					<td>Date:&nbsp;<u><%=rsNOI("signDate")%></u></td>
				</tr>
			</table>
			   								  			
		</td>
	</tr>
</table>
</body>
</html>