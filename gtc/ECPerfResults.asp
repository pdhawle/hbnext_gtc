<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
iUser = request("inspector")
iIssue = request("issue")
dtFrom = request("fromDate")
dtTo = request("toDate")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
if iUser = "all" then
	if iIssue = "all" then
	
	'get all of the issues for all of the inspectors that are based on my client id
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetAllIssuesAllUsers"
		   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, session("clientID"))
		   .parameters.Append .CreateParameter("@fromDate", adDBTimeStamp, adParamInput, 8, dtFrom)
		   .parameters.Append .CreateParameter("@toDate", adDBTimeStamp, adParamInput, 8, dtTo)
		   .CommandType = adCmdStoredProc   
		End With
					
		Set rsReport = oCmd.Execute
		Set oCmd = nothing

	else
		'get specific issue based on all inspectors
		Set oCmd = Server.CreateObject("ADODB.Command")
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetSpecificIssuesAllUsers"
		   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, session("clientID"))
		   .parameters.Append .CreateParameter("@performanceIssueID", adInteger, adParamInput, 8, iIssue)
		   .parameters.Append .CreateParameter("@fromDate", adDBTimeStamp, adParamInput, 8, dtFrom)
		   .parameters.Append .CreateParameter("@toDate", adDBTimeStamp, adParamInput, 8, dtTo)
		   .CommandType = adCmdStoredProc   
		End With
					
		Set rsReport = oCmd.Execute
		Set oCmd = nothing
	end if
	
else
	if iIssue = "all" then
		'get all issues based on specific inspector
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetAllIssuesSpecificUser"
		   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, session("clientID"))
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, iUser)
		   .parameters.Append .CreateParameter("@fromDate", adDBTimeStamp, adParamInput, 8, dtFrom)
		   .parameters.Append .CreateParameter("@toDate", adDBTimeStamp, adParamInput, 8, dtTo)
		   .CommandType = adCmdStoredProc   
		End With
					
		Set rsReport = oCmd.Execute
		Set oCmd = nothing
	else
		'get specific issues based on specific inspector
		Set oCmd = Server.CreateObject("ADODB.Command")
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetSpecificIssueSpecificUser"
		   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, session("clientID"))
		   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, iUser)
		   .parameters.Append .CreateParameter("@performanceIssueID", adInteger, adParamInput, 8, iIssue)
		   .parameters.Append .CreateParameter("@fromDate", adDBTimeStamp, adParamInput, 8, dtFrom)
		   .parameters.Append .CreateParameter("@toDate", adDBTimeStamp, adParamInput, 8, dtTo)
		   .CommandType = adCmdStoredProc   
		End With
					
		Set rsReport = oCmd.Execute
		Set oCmd = nothing
	
	end if
end if

%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - EC performance Tracking</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<!--<a href="expInspActResults.asp?inspector=<%=iUser%>&fromDate=<%=dtFrom%>&toDate=<%=dtTo%>" class="footerLink">Export Data to Excel</a>&nbsp;&nbsp;
						<span class="footerLink">|</span>&nbsp;&nbsp;--><a href="chooseECPerf.asp" class="footerLink">back to selections</a>			
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0">
										<tr><td></td><td colspan="5">Date Range: <strong><%=dtFrom%> - <%=dtTo%></strong></td></tr>
										<tr bgcolor="#666666">
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td><span class="searchText">Date Entered</span></td>
											<td><span class="searchText">Issue</span></td>
											<td><span class="searchText">Issue Description</span></td>
											<td><span class="searchText">Project</span></td>
											<td><span class="searchText">Inspector</span></td>
										</tr>
								
										<%If rsReport.eof then%>
												<tr><td></td><td colspan="9">there are no reports to display</td></tr>
											<%else
												blnChange = false
												dim i
												i = 0
												Do until rsReport.eof
													If blnChange = true then%>
														<tr class="rowColor">
													<%else%>
														<tr>
													<%end if%>
														<td></td>
														<td><%=rsReport("dateAdded")%></td>
														<td><%=rsReport("issue")%></td>
														<td><%=rsReport("describe")%></td>
														<td><%=rsReport("projectName")%></td>
														<td><%=rsReport("firstName") & " " & rsReport("lastName")%></td>
													</tr>
												<%
												
												rsReport.movenext
												if blnChange = true then
													blnChange = false
												else
													blnChange = true
												end if
												i = i + 1
												loop%>
												
												<tr><td colspan="10"><img src="images/pix.gif" width="1" height="10"></td></tr>
												<tr>
													<td colspan="9" align="right"><strong>Total Issues:&nbsp;<%=i%>&nbsp;&nbsp;&nbsp;</strong></td>
												</tr>
												
											<%end if%>
										
									</table>
															
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>
<%
'rsCustomer.close
rsReport.close
DataConn.close
Set rsCustomer = nothing
Set rsReport = nothing
Set DataConn = nothing
Set oCmd = nothing
%>