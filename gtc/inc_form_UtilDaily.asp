<%
'	dim blnMonth
	
	blnMonth = request("vm")
	
	if blnMonth = "true" then
		blnMonth = True
	else
		blnMonth = False
	end if
	
%>
<table>
	<tr>
		<td>
			<%if blnMonth = False then%>
				&nbsp;<a href="form.asp?formType=addReport&division=<%=iDivisionID%>&project=<%=iProjectID%>&reportType=<%=iReportTypeID%>&vm=true">view entire month</a>
			<%else%>
				&nbsp;<a href="form.asp?formType=addReport&division=<%=iDivisionID%>&project=<%=iProjectID%>&reportType=<%=iReportTypeID%>&vm=false">view current day</a>
			<%end if%>
		</td>
	</tr>
</table><br />
<table class="borderTable" width="830" cellpadding="0" cellspacing="0" border="0">
	<tr bgcolor="#666666">
		<td><span class="searchText">&nbsp;Date</span></td>
		<td align="center"><span class="searchText">Rainfall Amount<BR />(Inches)</span></td>
		<td align="center"><span class="searchText">Construction<br /> Exit<br />Functioning<br /> Properly*</span></td>
		<td align="center"><span class="searchText">Fuel &<br> Equipment<BR /> Storage<br>in Compliance</span></td>
		<td align="center"><span class="searchText">Time</span></td>
		<td align="center"><span class="searchText">Comments</span></td>
		<td align="center"><span class="searchText">Reported By</span></td>
	</tr>
	<%
	'Dim counter
	counter=1
	blnChange = true
	'if the user is not administrator, then only show today
	If blnMonth = False then
	'put only today's line item in and just put the person's name%>
		<tr class="rowColor">
			<td align="center"><%=day(date())%></td>
				<td align="center"><input type="text" name="rainfall<%=day(date())%>" size="3" /></td>
				<td align="center"><select name="question1_<%=day(date())%>"><option>Yes</option><option>No</option></select></td>
				<td align="center"><select name="question2_<%=day(date())%>"><option>Yes</option><option>No</option></select></td>
				<td align="center"><input id="time<%=day(date())%>" name="time<%=day(date())%>" type="text" value="8:00 am" size=8 maxlength=8 ONBLUR="validateDatePicker(this)">&nbsp;<IMG SRC="images/timepicker.gif" BORDER="0" ALT="Pick a Time!" ONCLICK="selectTime(this,time<%=day(date())%>)" STYLE="cursor:hand"></td>
				<td align="center"><input type="text" name="comments<%=day(date())%>" size="30" maxlength="250" /></td>
				<td align="center"><%=session("name")%><input type="hidden" name="inspector<%=day(date())%>" value="<%=session("ID")%>" /></td>
		</tr>
	<%else
		for counter = 1 to 31
			If blnChange = true then%>
				<tr class="rowColor">
			<%else%>
				<tr>
			<%end if%>
				<td align="center"><%=counter%></td>
				<td align="center"><input type="text" name="rainfall<%=counter%>" size="3" /></td>
				<td align="center"><select name="question1_<%=counter%>"><option>Yes</option><option>No</option></select></td>
				<td align="center"><select name="question2_<%=counter%>"><option>Yes</option><option>No</option></select></td>
				<td align="center"><input id="time<%=counter%>" name="time<%=counter%>" type="text" value="8:00 am" size=8 maxlength=8 ONBLUR="validateDatePicker(this)">&nbsp;<IMG SRC="images/timepicker.gif" BORDER="0" ALT="Pick a Time!" ONCLICK="selectTime(this,time<%=counter%>)" STYLE="cursor:hand"></td>
				<td align="center"><input type="text" name="comments<%=counter%>" size="30"  maxlength="250"/></td>
				<td align="center">
					<%
					'get the assigned users for this project
					Set oCmd = Server.CreateObject("ADODB.Command")
	
					With oCmd
					   .ActiveConnection = DataConn
					   .CommandText = "spGetAssignedUsers"
					   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iProjectID)
					   .CommandType = adCmdStoredProc
					   
					End With
						
						
					Set rsInspector = oCmd.Execute
					Set oCmd = nothing
					%>
					<select name="inspector<%=counter%>">
						<option value="">--inspected by--</option>
						<%'rsInspector.movefirst
						do until rsInspector.eof%>
							<option value="<%=rsInspector("userID")%>"><%=rsInspector("lastName") & ", " & rsInspector("firstName")%></option>
						<%rsInspector.movenext
						loop
						%>
					</select>
				</td>
		   </tr>
		<%if blnChange = true then
			blnChange = false
		else
			blnChange = true
		end if
		
		next
	end if%>

</table><br />
<input type="checkbox" name="inCompliance" checked="checked" />&nbsp;Overall the project is in compliance