<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->
<%
Dim rs, rsState, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")
'get the user's signature, if available
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetUser"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, Session("ID"))
   .CommandType = adCmdStoredProc
   
End With
			
Set rsUser = oCmd.Execute
Set oCmd = nothing

'get the assigned users for the list below
%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script language="JavaScript" src="includes/validator.js" type="text/javascript"></script>
<script language=JavaScript src='scripts/innovaeditor.js'></script>
</head>
<body>
<form action="process.asp" method="post" name="myForm">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Email Users</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td><strong>From:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%=session("name")%>&nbsp;(<%=session("email")%>)
												<input type="hidden" name="from" value="<%=session("email")%>" />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td><strong>User List:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="userList">
													<option value="AllUsers">All Users</option>
													<option value="All">All Active Users</option>
													<option value="Administrators">Administrators</option>
													<option value="Inspectors">Inspectors</option>	
													<option value="NOI">NOI Users</option>								
													<!--<option value="proj specific">proj specific</option>-->
													<!--add this later if needed for project specific emails-->
												</select>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>			
										<tr>
											<td valign="top"><strong>Message:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%
													Dim strFormName
													Dim strTextAreaName
													strFormName = "myForm"
													strTextAreaName = "smessage"
												%>
												<%'if reportType <> "Daily" then response.Write sOIBody end if%>
												<textarea name="smessage" cols="70" rows="10"><br /><br><%=rsUser("emailSignature")%></textarea>
												<script>
													//STEP 2: Replace the textarea (txtContent)
													oEdit1 = new InnovaEditor("oEdit1");
													oEdit1.features=["FullScreen","Preview","Print","Search",
														"Cut","Copy","Paste","PasteWord","PasteText","|","Undo","Redo","|",
														"ForeColor","BackColor","|","Bookmark","Hyperlink","XHTMLSource","BRK",
														"Numbering","Bullets","|","Indent","Outdent","LTR","RTL","|",
														"Image","Flash","Media","|","Table","Guidelines","Absolute","|",
														"Characters","Line","Form","RemoveFormat","ClearAll","BRK",
														"StyleAndFormatting","TextFormatting","ListFormatting","BoxFormatting",
														"ParagraphFormatting","CssText","|",
														"Paragraph","FontName","FontSize","|",
														"Bold","Italic","Underline","Strikethrough","Superscript","Subscript","|",
														"JustifyLeft","JustifyCenter","JustifyRight","JustifyFull"];
													oEdit1.REPLACE("smessage");
												</script>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td colspan="3" align="right">
												<input type="hidden" name="processType" value="sendUsersEmails" />
												<input type="submit" value="Submit Email" class="formButton"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
									</table>
									
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
</body>
</html>