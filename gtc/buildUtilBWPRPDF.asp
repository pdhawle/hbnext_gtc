<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%'need customerID, reportID

iDivisionID = request("divisionID")
reportID = request("reportID")
'customerID = 1
'reportID = 54

Session("LoggedIn") = "True"

Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetReportByID"
    .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, reportID) 'reportID
   .CommandType = adCmdStoredProc
   
End With
			
Set rsReport = oCmd.Execute
Set oCmd = nothing

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCategories"'ByDivision
    .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, iDivisionID)
   .CommandType = adCmdStoredProc
   
End With
			
Set rsCategories = oCmd.Execute
Set oCmd = nothing
%>
<html>
<head>
<title>SWSIR</title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/report.css" type="text/css">
</head>
<body>

<table width=650 cellpadding=0 cellspacing=0>
	<tr>
		<td valign=top colspan="2">
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td valign="middle">
						<%if rsReport("logo") = "" then%>
							<img src="images/logo.jpg">
						<%else%>
							<img src="<%=rsReport("logo")%>">
						<%end if%>
					</td>
					<td valign="middle" align="right"><b>Report #<%=reportID%></b></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=20></td></tr>
	<tr>
		<td colspan=2>
			<table width=650>
				<tr>
					<td width="325" valign="top">
						<b>Project Name:</b> <%=rsReport("projectName")%><br>
					</td>
					<td></td>
					<td width="325" valign="top">
						<!--<b>Initial Inspection Date:</b>  -->
						<%
					'	Set oCmd = Server.CreateObject("ADODB.Command")	
					'	With oCmd
					'	   .ActiveConnection = DataConn
					'	   .CommandText = "spGetInitialInspectionDate"
					'	   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, rsReport("projID"))
					'	   .CommandType = adCmdStoredProc							   
					'	End With
					'
					'	Set rsInit = oCmd.Execute
					'	Set oCmd = nothing
						
					'	response.Write rsInit("inspectionDate")%>
					</td>
				</tr>
				
				<tr>
					<td valign="top">
						<b>Project Location (County(s)):</b> 
						
						<%sCounty = split(rsReport("county"), ",")
						if sCounty <> "" then
							For isLoop = LBound(sCounty) to UBound(sCounty)
								'get the county
								Set oCmd = Server.CreateObject("ADODB.Command")	
								With oCmd
								   .ActiveConnection = DataConn
								   .CommandText = "spGetCounty"
								   .parameters.Append .CreateParameter("@countyID", adInteger, adParamInput, 8, trim(sCounty(isLoop)))
								   .CommandType = adCmdStoredProc							   
								End With
							
								Set rsCounty = oCmd.Execute
								Set oCmd = nothing
								
								do until rsCounty.eof
									response.Write rsCounty("county")
								rsCounty.movenext
								loop
								
								if isLoop <> UBound(sCounty) then
									response.Write ", "
								end if 
							next
						end if
						%>
					</td>
					<td></td>
					<td valign="top">
						<b>LDA Start Date:</b> <%=rsReport("startDate")%><br>
					</td>
				</tr>
				<tr>
					<td valign="top">
							
						
						<b>Inspecting Company's Name:</b> <%=rsReport("company")%><br>
					</td>
					<td></td>
					<td>
						<b>Qualified Inspector:</b> <%=rsReport("lastName")%>, <%=rsReport("firstname")%><br>
					</td>
				</tr>
				<tr>
					<td valign="top">
						<strong>Rainfall Amount:</strong> <%=formatnumber(rsReport("rainfallAmount"),1)%> 
						<%if rsReport("rainfallAmount") > 1 then%>
							inches
						<%else%>
							inch
						<%end if%><br> 
					</td>
					<td></td>
					<td valign="top">
						
						<b>Date of the Inspection:</b> <%=rsReport("inspectionDate")%><br>	

					</td>
				</tr>
				<tr>
					<td valign="top">
						<strong>Phase:</strong> <%=rsReport("phase")%> 
					</td>
					<td></td>
					<td valign="top">
						
						

					</td>
				</tr>
			</table>
			
		</td>
	</tr>
	<tr><td colspan="2"><img src=images/pix.gif width=1 height=20></td></tr>
	<tr>
		<td colspan="2">
			<%
			'get the responsive action items from this report
			'
			Set oCmd = Server.CreateObject("ADODB.Command")

			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spGetResponsiveActionItemsUtil"
			   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID) 'reportID
			   .CommandType = adCmdStoredProc
			   
			End With
						
			Set rsAction = oCmd.Execute
			Set oCmd = nothing
			%>
			<strong>Action Items From This Inspection</strong><br>
			<table border="1" width="650" cellpadding="0" cellspacing="0">
				<!--<tr><td colspan="6" align="center" class="tCol"><strong>For action items found during this inspection</strong></td></tr>-->
				<tr>
					<td align="center" width="20"><strong>Item ID</strong></td>
					<td><strong>Location</strong></td>
					<td><strong>Proposed Activity</strong></td>
					<td><strong>BMP/Sensitive Area</strong></td>
					<td width="200"><strong>Comments</strong></td>
					<td width="50"><strong>Action Item</strong></td>
					<!--<td width="50"><strong>Date Installed</strong></td>
					<td align="center"><strong>Comp.</strong></td>
					<td align="center"><strong>Date Fixed</strong></td>
					<td><strong>Comments</strong></td>-->
				</tr>				
				<%
				If rsAction.eof then
					response.Write "<tr><td colspan=5 align=center>There are no corrective action items for this report.</td></tr>"
				else
					do until rsAction.eof%>
					<tr>
						<td valign="top" align="center"><%=rsAction("referenceNumber")%>&nbsp;</td>
						<td valign="top"><%=rsAction("location")%>&nbsp;</td>
						<td valign="top"><%=rsAction("proposedActivity")%>&nbsp;</td>
						<td valign="top"><%=rsAction("question")%>&nbsp;</td>
						<td valign="top"><%=rsAction("majorObservations")%>&nbsp;</td>
						<td align="left" valign="top"><%=rsAction("actionNeeded")%>&nbsp;</td>
						<!--<td valign="top"><%'=rsAction("dateInstalled")%>&nbsp;</td>
						<td valign="top" align="center">
							<%'response.Write rsAction("installedCorrectly")
						'	if rsAction("installedCorrectly") = "True" then %>
								Y
							<%'else%>
								N
							<%'end if%>
						</td>						
						<td align="center" valign="top"><%'=formatDatetime(rsAction("addressedByDate"),2)%>&nbsp;</td>
						<td align="left" valign="top"><%'=rsAction("closeComments")%>&nbsp;</td>-->	
					</tr>
					<%rsAction.movenext
					loop
				end if%>
			</table>
			<br>
			
			
			<%
			'get the responsive action items from this report
			'
			Set oCmd = Server.CreateObject("ADODB.Command")

			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spGeMajorObservations"
			   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, rsReport("projectID")) 'reportID
			   .CommandType = adCmdStoredProc
			   
			End With
						
			Set rsOtherCom = oCmd.Execute
			Set oCmd = nothing
			%>
			<strong>Locations Inspected</strong><br>
			<table border="1" width="650" cellpadding="0" cellspacing="0">
				<!--<tr><td colspan="6" align="center" class="tCol"><strong>For action items found during this inspection</strong></td></tr>-->
				<tr>
					<!--<td align="center" width="20"><strong>Item ID</strong></td>-->
					<td><strong>Location</strong></td>
					<td><strong>Proposed Activity</strong></td>
					<td><strong>BMP/Sensitive Area</strong></td>
					<td><strong>Comments</strong></td>
					<td><strong>Date Installed</strong></td>
				</tr>				
				<%
				If rsOtherCom.eof then
					response.Write "<tr><td colspan=5 align=center>There are no other comments on this report.</td></tr>"
				else
					do until rsOtherCom.eof%>
					<tr>
						<!--<td valign="top" align="center"><%'=rsOtherCom("referenceNumber")%>&nbsp;</td>-->
						<td valign="top"><%=rsOtherCom("location")%>&nbsp;</td>
						<td valign="top"><%=rsOtherCom("proposedActivity")%>&nbsp;</td>
						<td valign="top"><%=rsOtherCom("question")%>&nbsp;</td>
						<td valign="top"><%=rsOtherCom("majorObservations")%>&nbsp;</td>
						<td valign="top"><%=rsOtherCom("dateInstalled")%>&nbsp;</td>
					</tr>
					<%rsOtherCom.movenext
					loop
				end if%>
			</table>
			<br>
			
			<%
			'get the responsive action items from this report
			Set oCmd = Server.CreateObject("ADODB.Command")
	
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spGetOpenItemsPrevReports"
			   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, rsReport("projectID")) 'projectID
			   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID) 'reportID
			   .CommandType = adCmdStoredProc				   
			End With
							
			Set rsPrev = oCmd.Execute
			Set oCmd = nothing
			%>
			<strong>Uncompleted Open Items From Prior Inspections</strong><br>
			<table border="1" width="650" cellpadding="0" cellspacing="0">
				<tr>
					<td align="center" width="20"><strong>Item ID</strong></td>
					<td><strong>Location</strong></td>
					<td><strong>Proposed Activity</strong></td>
					<td><strong>BMP/Sensitive Area</strong></td>
					<!--<td width="200"><strong>Comments</strong></td>-->
					<td width="50"><strong>Action Item</strong></td>
					<td width="200"><strong>Action Item Comments</strong></td>
					<!--<td width="50"><strong>Date Installed</strong></td>-->
				</tr>				
				<%
				If rsPrev.eof then
					response.Write "<tr><td colspan=6 align=center>There are no corrective action items for this report.</td></tr>"
				else
					do until rsPrev.eof%>
					<tr>
						<td valign="top" align="center"><%=rsPrev("referenceNumber")%>&nbsp;</td>
						<td valign="top"><%=rsPrev("location")%>&nbsp;</td>
						<td valign="top"><%=rsPrev("proposedActivity")%>&nbsp;</td>
						<td valign="top"><%=rsPrev("question")%>&nbsp;</td>
						<!--<td valign="top"><%'=rsPrev("majorObservations")%>&nbsp;</td>-->
						<td align="left" valign="top"><%=rsPrev("actionNeeded")%>&nbsp;</td>
						<td align="left" valign="top"><%=rsPrev("comments")%>&nbsp;</td>
						<!--<td valign="top"><%'=rsPrev("dateInstalled")%>&nbsp;</td>	-->
					</tr>
					<%rsPrev.movenext
					loop
				end if%>
			</table><br>
			
			<%
			
		'	response.Write rsReport("projectID") & "<br>"
		'	response.Write rsReport("lastInspectionDate") & "<br>"
		'	response.Write rsReport("inspectionDate") & "<br>"
			
			'get the responsive action items from this report
			Set oCmd = Server.CreateObject("ADODB.Command")
	
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spGetClosedItemsThisReport"
			   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, rsReport("projectID")) 'projectID
			   .parameters.Append .CreateParameter("@lastInspectionDate", adDBTimeStamp, adParamInput, 8, rsReport("lastInspectionDate"))
			   .parameters.Append .CreateParameter("@inspectionDate", adDBTimeStamp, adParamInput, 8, rsReport("inspectionDate"))
			   .CommandType = adCmdStoredProc				   
			End With
							
			Set rsPrevClosed = oCmd.Execute
			Set oCmd = nothing
			%>
			<strong>Completed Open Items From Prior Inspection</strong><br>
			<table border="1" width="650" cellpadding="0" cellspacing="0">
				<tr>
					<td align="center" width="20"><strong>Item ID</strong></td>
					<td><strong>Location</strong></td>
					<td><strong>Proposed Activity</strong></td>
					<td><strong>BMP/Sensitive Area</strong></td>
					<!--<td width="200"><strong>Comments</strong></td>-->
					<td width="50"><strong>Action Item</strong></td>
					<td width="50"><strong>Date Addressed</strong></td>
					<td width="200"><strong>Action Item Comments</strong></td>
				</tr>				
				<%
				If rsPrevClosed.eof then
					response.Write "<tr><td colspan=7 align=center>There are no corrective action items for this report.</td></tr>"
				else
					do until rsPrevClosed.eof
						'if rsReport("inspectionDate") = rsPrevClosed("addressedByDate") then%>
					<tr>
						<td valign="top" align="center"><%=rsPrevClosed("referenceNumber")%>&nbsp;</td>
						<td valign="top"><%=rsPrevClosed("location")%>&nbsp;</td>
						<td valign="top"><%=rsPrevClosed("proposedActivity")%>&nbsp;</td>
						<td valign="top"><%=rsPrevClosed("question")%>&nbsp;</td>
						<!--<td valign="top"><%'=rsPrevClosed("majorObservations")%>&nbsp;</td>-->
						<td align="left" valign="top"><%=rsPrevClosed("actionNeeded")%>&nbsp;</td>
						<td valign="top"><%=rsPrevClosed("addressedByDate")%>&nbsp;</td>
						<td align="left" valign="top"><%=rsPrevClosed("comments")%>&nbsp;</td>
					</tr>
					<%'end if
					rsPrevClosed.movenext
					loop
				end if%>
			</table>
		</td>
	</tr>
	<!---<tr><td colspan="2"><img src=images/pix.gif width=1 height=20></td></tr>
	
	
	
	<tr>
		<td colspan=2 align="center"><strong>Water Quality Monitoring</strong></td></tr>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=5></td></tr>
	
	<tr>
		<td colspan=2>
			<%'get the data
	'		Set oCmd = Server.CreateObject("ADODB.Command")
	'
	'		With oCmd
	'		   .ActiveConnection = DataConn
	'		   .CommandText = "spGetMonthlyRainData"
	'			.parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, reportID) 'reportID
	'		   .CommandType = adCmdStoredProc
	'		   
	'		End With
	'					
	'		Set rsData = oCmd.Execute
	'		Set oCmd = nothing
	'		
	'		'create arrays for each item
	'		i = 0
'	'		Dim arrSamplerID(10)
'			Dim arrDateEndClearingGrubbing(10)			
'			Dim arrDateSampleTakenAfterClearGrub(10)
'			Dim arrDateEndGrading(10)
'			Dim arrDateAfterEndGradingorNinety(10)
			
'			Dim arrSamplingRequired(10)
'			Dim arrSamplingComplete(10)
	'		Dim arrRainDateEvent(10)
	'		Dim arrRainAmount(10)
	'		Dim arrCodeCol1(10)
	'		Dim arrCodeCol2(10)
	'		Dim arrCodeCol3(10)
	'		Dim arrCodeCol4(10)
	'		Dim arrCodeCol5(10)
	'		Dim arrCodeCol6(10)
	'		Dim arrCodeCol7(10)
	'		Dim arrCodeCol8(10)
	'		Dim arrCodeCol9(10)
	'		Dim arrCodeCol10(10)
	'		
	'		do until rsData.eof
'				arrSamplerID(i) = rsData("samplerID")
'				arrDateEndClearingGrubbing(i) = rsData("dateEndClearingGrubbing")
'				arrDateSampleTakenAfterClearGrub(i) = rsData("dateSampleTakenAfterClearGrub")
'				arrDateEndGrading(i) = rsData("dateEndGrading")
'				arrDateAfterEndGradingorNinety(i) = rsData("dateAfterEndGradingorNinety")
'				
'				arrSamplingRequired(i) = rsData("samplingRequired")
'				arrSamplingComplete(i) = rsData("samplingComplete")
	'			arrRainDateEvent(i) = rsData("rainDateEvent")
	'			arrRainAmount(i) = rsData("rainAmount")
	'			arrCodeCol1(i) = rsData("codeCol1")
	'			arrCodeCol2(i) = rsData("codeCol2")
	'			arrCodeCol3(i) = rsData("codeCol3")
	'			arrCodeCol4(i) = rsData("codeCol4")
	'			arrCodeCol5(i) = rsData("codeCol5")
	'			arrCodeCol6(i) = rsData("codeCol6")
	'			arrCodeCol7(i) = rsData("codeCol7")
	'			arrCodeCol8(i) = rsData("codeCol8")
	'			arrCodeCol9(i) = rsData("codeCol9")
	'			arrCodeCol10(i) = rsData("codeCol10")
	'		rsData.movenext
	'		i=i+1
	'		loop
			%>
			<table width="100%" class="borderTable2" cellpadding="3" cellspacing="0" border="0">-->
				<!--<tr>
					<td width="120" class="tCol" colspan="2">
						<span class="smallCopy">Sampler ID<br>(See CMP for ID)</span>
					</td>
					<%'for i = 0 to 9%>
						<td class="tCol" bgcolor="#CCCCCC" align="center">
							<span class="smallCopy"><%'=arrSamplerID(i)%>&nbsp;</span>
						</td>
					<%'next%>
				</tr>
				<tr>
					<td width="120" class="tCol" colspan="2">
						<span class="smallCopy">Date end clearing and grubbing</span>
					</td>
					<%'for i = 0 to 9%>
						<td class="tCol" bgcolor="#CCCCCC" align="center">
							<span class="smallCopy"><%'=arrDateEndClearingGrubbing(i)%>&nbsp;</span>
						</td>
					<%'next%>
				</tr>
				<tr>
					<td width="120" class="tCol" colspan="2">
						<span class="smallCopy">Date sample taken after clearing/grubbing.</span>
					</td>
					<%'for i = 0 to 9%>
						<td class="tCol" bgcolor="#CCCCCC" align="center">
							<span class="smallCopy"><%'=arrDateSampleTakenAfterClearGrub(i)%>&nbsp;</span>
						</td>
					<%'next%>
				</tr>
				<tr>
					<td width="120" class="tCol" colspan="2">
						<span class="smallCopy">Date of end of grading.</span>
					</td>
					<%'for i = 0 to 9%>
						<td class="tCol" bgcolor="#CCCCCC" align="center">
							<span class="smallCopy"><%'=arrDateEndGrading(i)%>&nbsp;</span>
						</td>
					<%'next%>
				</tr>
				<tr>
					<td width="120" class="tCol" colspan="2">
						<span class="smallCopy">Date sample take after end or grading or within 90 days of first sampling event.</span>
					</td>
					<%'for i = 0 to 9%>
						<td class="tCol" bgcolor="#CCCCCC" align="center">
							<span class="smallCopy"><%'=arrDateAfterEndGradingorNinety(i)%>&nbsp;</span>
						</td>
					<%'next%>
				</tr>
				<tr>
					<td width="120" class="tCol" colspan="2">
						<span class="smallCopy">Is sampling required for this project per the ES&PC Plans?</span>

					</td>
					<%'for i = 0 to 9%>
						<td class="tCol" bgcolor="#CCCCCC" align="center">
							<span class="smallCopy"><%'=arrSamplingRequired(i)%>&nbsp;</span>
						</td>
					<%'next%>
				</tr>
				<tr>
					<td width="120" class="tCol" colspan="2">
						<span class="smallCopy">Sampling complete?</span>

					</td>
					<%'for i = 0 to 9%>
						<td class="tCol" bgcolor="#CCCCCC" align="center">
							<span class="smallCopy"><%'=arrSamplingComplete(i)%>&nbsp;</span>
						</td>
					<%'next%>
				</tr>
			-->
		<!--	<tr>
				<td class="tCol"><span class="smallCopy">Date of >= 0.5<br /> inch event</span></td>
				<td class="tCol"><span class="smallCopy">Amt. Of<br />Rainfall</span></td>
				<td class="tCol" colspan="10" bgcolor="#000000">&nbsp;</td>
			</tr>	
			<%'for i = 0 to 0%>
			<tr>
				<td class="tCol" bgcolor="#CCCCCC" align="center">
					<span class="smallCopy"><%'=arrRainDateEvent(i)%>&nbsp;</span>
				</td>
				<td class="tCol" bgcolor="#CCCCCC" align="center">
					<span class="smallCopy"><%'=arrRainAmount(i)%>&nbsp;</span>
				</td>
				<td class="tCol" bgcolor="#CCFFFF" align="center">
					<span class="smallCopy"><%'=arrCodeCol1(i)%>&nbsp;</span>
				</td>
				<td class="tCol" bgcolor="#CCFFFF" align="center">
					<span class="smallCopy"><%'=arrCodeCol2(i)%>&nbsp;</span>
				</td>
				<td class="tCol" bgcolor="#CCFFFF" align="center">
					<span class="smallCopy"><%'=arrCodeCol3(i)%>&nbsp;</span>
				</td>
				<td class="tCol" bgcolor="#CCFFFF" align="center">
					<span class="smallCopy"><%'=arrCodeCol4(i)%>&nbsp;</span>
				</td>
				<td class="tCol" bgcolor="#CCFFFF" align="center">
					<span class="smallCopy"><%'=arrCodeCol5(i)%>&nbsp;</span>
				</td>
				<td class="tCol" bgcolor="#CCFFFF" align="center">
					<span class="smallCopy"><%'=arrCodeCol6(i)%>&nbsp;</span>
				</td>
				<td class="tCol" bgcolor="#CCFFFF" align="center">
					<span class="smallCopy"><%'=arrCodeCol7(i)%>&nbsp;</span>
				</td>
				<td class="tCol" bgcolor="#CCFFFF" align="center">
					<span class="smallCopy"><%'=arrCodeCol8(i)%>&nbsp;</span>
				</td>
				<td class="tCol" bgcolor="#CCFFFF" align="center">
					<span class="smallCopy"><%'=arrCodeCol9(i)%>&nbsp;</span>
				</td>
				<td class="tCol" bgcolor="#CCFFFF" align="center">
					<span class="smallCopy"><%'=arrCodeCol10(i)%>&nbsp;</span>
				</td>
			</tr>
			<%'next%>
			</table>
		</td>
	</tr>-->
	
	
	<tr><td colspan="2"><img src=images/pix.gif width=1 height=20></td></tr>
	<!--<tr><td colspan="2"><font color="#FF0000">Photo document deficiencies and retain in permanent file.</font></td></tr>
	<tr><td colspan="2"><img src=images/pix.gif width=1 height=20></td></tr>-->
	<tr>
		<td colspan="2">
			<%if rsReport("inCompliance") = "True" then%>
				<img src="images/checked.gif">
			<%else%>
				<img src="images/checkbox.gif">
			<%end if%>&nbsp;Overall the project is in compliance<br><br>
			
			"I certify under the penalty of law that this report and all attachments were prepared
			under my direction or supervision in accordance with a system designed to assure that
			certified personnel properly gather and evaluate the information submitted. Based on my
			inquiry of the person or persons who manage the system, or those persons directly
			responsible for gathering the information, the information submitted is, to the best of my
			knowledge and belief, true, accurate, and complete. I am aware that there are significant
			penalties for submitting false information, including the possibility of fine and imprisonment
			for knowing violations"</u><br><br>
			
			<strong>Signature:</strong> 
			<%if rsReport("sigFileName") <> "" then%> 
				<img src="userUploads/<%=rsReport("userID")%>/<%=rsReport("sigFileName")%>">&nbsp;&nbsp;
			<%else%>
			______________________________ 
			<%end if%> <br><br>
			<strong>Date:</strong> <%=rsReport("inspectionDate")%><br>
			Erosion Control Inspector
			
						
		</td>
	</tr>
	<tr><td colspan="2"><img src=images/pix.gif width=1 height=140></td></tr>
	
</table>
</body>
</html>