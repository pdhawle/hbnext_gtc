<html>
<head>
<title>On-Demand NOI</title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
</head>
<body>
<table bgcolor="#FFFFFF" class="borderTable" width="400" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td bgcolor="#FA702B"><img src="images/pix.gif" width="25" height="1"></td>
					<td bgcolor="#FA702B" valign="top">
						<%if Session("appLogo") <> "" then%>
							<img src="<%=Session("appLogo")%>" border="0"/>
						<%else%>
							<a href="http://next-sequence.com"><img src="images/logo.gif" width="110" height="100" border="0"/></a>
						<%end if%>
						<img src="images/sequence.gif" width="171" height="100" /><img src="images/pix.gif" width="151" height="1">
					</td>
				</tr>
				<tr><td colspan="8"><img src="images/pix.gif" width="1" height="10"></td></tr>
			</table>
		</td>
	</tr>
	<tr>
		<td><img src="images/pix.gif" width="20" height="1"></td>
		<td>
			<table width="800" border="0" cellpadding="0" cellspacing="0">
				<tr align="center">
					<td colspan="3">
						<span class="Header">Instructions</span><br />
						<strong>Blanket Notice of Intent - <em>Secondary Permittee</em><br />
						For Storm Water Discharges<br />
						Associated With Construction Activity<br />
						To Be Covered Under The NPDES General Permit<br />
						Who must file a Notice of Intent (NOI) Form</strong><br />
					</td>
				</tr>
				<tr><td><img src="images/pix.gif" width="1" height="30" /></td></tr>
				<tr>
					<td><img src="images/pix.gif" width="10" height="1" /></td>
					<td>
						This Notice of Intent must be typed. Any NOI that contains illegible information will not be accepted, will be returned, and the site
						will not be granted Permit coverage. All information on this NOI must be submitted to be a valid Notice. Any information requested
						on the NOI that is not applicable to the owner and operator or to the site must be marked �N/A�.<br /><br />
						
						The Owner and Operator of an activity that has a discharge of storm water from a site where construction activities occur must
						apply for a National Pollutant Discharge Elimination System (NPDES) Permit. The Georgia Environmental Protection Division has
						issued this General NPDES Permit for storm water discharges from construction activities with an effective date of August 12,
						2003. The Permit is available is available for review at EPD�s offices and on EPD�s web page at www.dnr.state.ga.us/dnr/environ/.
						It is highly recommended that the permittee read and understand the terms and conditions of the Permit prior to submitting a NOI
						for coverage under this Permit. Contact EPD at the Regional Office or District Office shown on the next page for assistance in
						completing this NOI.<br /><br />
						
						<strong>Where to file NOI Forms</strong> -- The NOI and attachments must be sent to the Regional Office or District Office shown on the next
						page. Please submit only the first page of this document; do not submit the instructions or addresses.<br /><br />
						
						<strong>Section I. Site / Secondary Permittee Information</strong><br />
						Enter the information required. The site/project name is the physical location of the construction activity. Should the site lack a
						street address, sufficiently describe the facility location so that it can be found by district personnel. If additional space is needed,
						attach the description to the notice.<br /><br />
						
						The facility contact is the person who the secondary permittee has assigned the responsibility for the daily on-site operational
						control. Please do not leave any blanks in this section.<br /><br />
						
						<strong>Section II. Site Activity Information</strong><br />
						The start date and completion date are expected for the construction activity for which this NOI is applicable.<br /><br />
						
						Estimated disturbed acreage is the total number of acres, to the nearest 1/10 acre, that will be disturbed under this NOI
						(this includes disturbances by the secondary permittee(s).)<br /><br />
						
						<strong>Section III. Receiving Water Information</strong><br />
						If the facility discharges storm water directly or indirectly (but not through a MS4) to the receiving water(s), enter the name(s) of the
						receiving water(s) and indicate whether the water(s) is a trout stream or a warm water fisheries stream. Attach to this notice a
						written description and a map of the location of the receiving water(s).<br /><br />
						
						If the storm water discharges to a municipal separate storm sewer system (MS4), enter the name of the operator of the MS4 (e.g.,
						city name or county name) and the name of the receiving water at the point of discharge from the MS4. A MS4 is defined as a
						conveyance or system of conveyances (including: roads with drainage systems, municipal streets, catch basins, curbs, gutters,
						ditches, man-made channels, or storm drains) that is owned or operated by a city or county which is designed or used for collecting
						or conveying storm water. It may be necessary to contact the city or county that operates the MS4 to determine the name of the
						receiving waters. Indicate whether the receiving water(s) is a trout stream or a warm water fisheries stream.<br /><br />
						
						<strong>Section IV. Certifications</strong><br />
						All applicants must sign this certification. Permittees shall initial next to the applicable certification statements on the line
						provided. Federal and State statutes provide specific requirements as to whom is authorized to sign Notice of Intents. Signing of a
						Notice of Intent by others is not a valid submittal. Please be aware Federal and State statues provide severe penalties for
						submitting false information on this application form. Federal and State regulations require this application to be signed as follows:<br /><br />
						
						- For a corporation: by a responsible corporate officer;<br />
						- For a partnership or sole proprietorship: by a general partner or the proprietor; or<br />
						- For a municipality, state, Federal or other public facility: by either a principal executive officer or ranking elected official.<br /><br />
					</td>
					<td><img src="images/pix.gif" width="10" height="1" /></td>
				</tr>
				<tr><td><img src="images/pix.gif" width="1" height="30" /></td></tr>
				<tr align="center">
					<td colspan="3">
						<strong>GEORGIA EPD DISTRICT OFFICES</strong>
					</td>
				</tr>
				<tr><td><img src="images/pix.gif" width="1" height="30" /></td></tr>
				<tr>
					<td><img src="images/pix.gif" width="10" height="1" /></td>
					<td>
						All Notice of Intents, Notice of Terminations, Erosion, Sedimentation and Pollution Control Plans, Comprehensive
						Monitoring Programs, certifications, reports, and any other information shall be sent to the following District offices of
						EPD:<br /><br />
						
						<strong>A. For facilities/sites located in the following counties:</strong> Bibb, Bleckley, Chattahoochee, Crawford, Dooly, Harris,
						Houston, Jones, Lamar, Macon, Marion, Meriwether, Monroe, Muscogee, Peach, Pike, Pulaski, Schley, Talbot, Taylor,
						Troup, Twiggs, Upson<br /><br />
						<table>
							<tr>
								<td valign="top">Information shall be submitted to:</td>
								<td><img src="images/pix.gif" width="10" height="1" /></td>
								<td>
									West Central District Office<br />
									Georgia Environmental Protection Division<br />
									2640 Shurling Drive<br />
									Macon, GA 31211-3576<br />
									(478) 751-6612<br />
								</td>
							</tr>
						</table><br />
						
						<strong>B. For facilities/sites located in the following counties:</strong> Burke, Columbia, Emanuel, Glascock, Jefferson, Jenkins,
						Johnson, Laurens, McDuffie, Montgomery, Richmond, Screven, Treutlen, Warren, Washington, Wheeler, Wilkinson<br /><br />
						<table>
							<tr>
								<td valign="top">Information shall be submitted to:</td>
								<td><img src="images/pix.gif" width="10" height="1" /></td>
								<td>
									East Central District Office<br />
									Georgia Environmental Protection Division<br />
									1885-A Tobacco Road<br />
									Augusta, GA 30906-8825<br />
									(706) 792-7744<br />
								</td>
							</tr>
						</table><br />
						
						<strong>C. For facilities/sites located in the following counties:</strong> Baldwin, Banks, Barrow, Butts, Clarke, Elbert, Franklin,
						Greene, Hall, Hancock, Hart, Jackson, Jasper, Lincoln, Madison, Morgan, Newton, Oconee, Oglethorpe, Putnam,
						Stephens, Taliaferro, Walton, Wilkes<br /><br />
						<table>
							<tr>
								<td valign="top">Information shall be submitted to:</td>
								<td><img src="images/pix.gif" width="10" height="1" /></td>
								<td>
									Northeast District Office<br />
									Georgia Environmental Protection Division<br />
									745 Gaines School Road<br />
									Athens, GA 30605-3129<br />
									(706) 369-6376<br />
								</td>
							</tr>
						</table><br />
						
						<strong>D. For facilities/sites located in the following counties:</strong> Clayton, Coweta, DeKalb, Fayette, Gwinnett, Heard,
						Henry, Rockdale, Spalding<br /><br />
						<table>
							<tr>
								<td valign="top">Information shall be submitted to:</td>
								<td><img src="images/pix.gif" width="10" height="1" /></td>
								<td>
									Mountain District - Atlanta Satellite<br />
									Georgia Environmental Protection Division<br />
									4244 International Parkway, Suite 114<br />
									Atlanta, GA 30354-3906<br />
									(404) 362-2671<br />
								</td>
							</tr>
						</table><br />
						
						<strong>E. For facilities/sites located in the following counties:</strong> Bartow, Carroll, Catoosa, Chattooga, Cherokee, Cobb,
						Dade, Dawson, Douglas, Fannin, Floyd, Forsyth, Fulton, Gilmer, Gordon, Habersham, Haralson, Lumpkin, Murray,
						Paulding, Pickens, Polk, Rabun, Towns, Union, Walker, White, Whitfield<br /><br />
						<table>
							<tr>
								<td valign="top">Information shall be submitted to:</td>
								<td><img src="images/pix.gif" width="10" height="1" /></td>
								<td>
									Mountain District - Cartersville Office<br />
									Georgia Environmental Protection Division<br />
									P.O. Box 3250<br />
									Cartersville, GA 30120-1705<br />
									(770) 387-4900<br />
								</td>
							</tr>
						</table><br />
						
						<strong>F. For facilities/sites located in the following counties:</strong> Appling, Atkinson, Bacon, Brantley, Bryan, Bulloch,
						Camden, Candler, Charlton, Chatham, Clinch, Coffee, Effingham, Evans, Glynn, Jeff Davis, Liberty, Long, McIntosh,
						Pierce, Tattnall, Toombs, Ware, Wayne<br /><br />
						<table>
							<tr>
								<td valign="top">Information shall be submitted to:</td>
								<td><img src="images/pix.gif" width="10" height="1" /></td>
								<td>
									Coastal District- Brunswick Office<br />
									Georgia Environmental Protection Division<br />
									One Conservation Way<br />
									Brunswick, GA 31520-8687<br />
									(912) 264-7284<br />
								</td>
							</tr>
						</table><br />
						
						<strong>G. For facilities/sites located in the following counties:</strong> Baker, Ben Hill, Berrien, Brooks, Calhoun, Clay, Colquitt,
						Cook, Crisp, Decatur, Dodge, Dougherty, Early, Echols, Grady, Irwin, Lanier, Lee, Lowndes, Miller, Mitchell, Quitman,
						Randolph, Seminole, Stewart, Sumter, Telfair, Terrell, Thomas, Tift, Turner, Webster, Wilcox, Worth<br /><br />
						<table>
							<tr>
								<td valign="top">Information shall be submitted to:</td>
								<td><img src="images/pix.gif" width="10" height="1" /></td>
								<td>
									Southwest District Office<br />
									Georgia Environmental Protection Division<br />
									2024 Newton Road<br />
									Albany, GA 31701-3576<br />
									912) 430-4144<br />
								</td>
							</tr>
						</table><br />
					</td>
					<td><img src="images/pix.gif" width="10" height="1" /></td>
				</tr>
			</table>
		</td>
		<td><img src="images/pix.gif" width="20" height="1"></td>
	</tr>
</table>
</body>
</html>
