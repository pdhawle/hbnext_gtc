<%
infoPackID = request("infoPackID")
clientID = request("clientID")
customerID = request("customer")
sdate = request("date")
contactID = request("contactID")
userID = request("userID")
openingParagraph = request("openingParagraph")

'only admins can send info packs
blnVerify = request("v")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 	
DataConn.Open Session("Connection"), Session("UserID")

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomer"
   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, customerID)
   .CommandType = adCmdStoredProc   
End With
			
Set rsCustomer = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomerContact"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, contactID)
   .CommandType = adCmdStoredProc   
End With
			
Set rs = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetUser"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, userID)
   .CommandType = adCmdStoredProc   
End With
			
Set rsUser = oCmd.Execute
Set oCmd = nothing

%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script language="javascript" src="includes/datePicker.js"></script>
<link rel="stylesheet" href="includes/datePicker.css" type="text/css">
</head>
<body>
<form name="addInfoPack" method="post" action="process.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Preview Info Pack</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td width="30"><td>
											<td>
												<table width="100%" cellpadding="0" cellspacing="0">
													<tr>
														<td valign="top">
															<img src="images/hbslogo.gif"><br>
															<span class="greyCopy">1255 Lakes Parkway, Suite 385<br>
															Lawrenceville, GA  30043<br>
															(P) 678-336-1357 &bull; (F) 678-336-1358</span>
														</td>
														<td valign="top" align="right">
															<img src="images/logoprint.gif">
														</td>
													</tr>
												</table><br><br>
												
												<%=sdate%><br><br>
												
												<%=rs("contactName")%>&nbsp;<%=rs("contactLastName")%><br>
												<%=rs("customerName")%><br>
												<%=rs("address1")%>
												<%if rs("address2") <> "" then
													response.Write ", " & rs("address2") & "<br>"
												else
													response.Write "<br>"
												end if%>
												<%=rs("city")%>,&nbsp;<%=rs("state")%>&nbsp;<%=rs("zip")%><br><br>
												
												Dear <%=trim(rs("contactName"))%>,<br><br>
												
												<%=openingParagraph%><br><br>
												
												<strong>HB/Sequence - NEXT (HB/S)</strong> is pleased to submit this information package about our services.  Our 
												Goal is to provide you with your needed results and a �Return on Investment� from our NPDES Compliance Inspections, 
												Software and Consulting Services. Our Mission is to provide you, your company and your employees with the <em>Best Value</em> 
												in inspections, software and consulting services.  We understand and teach the importance and the balance of managing 
												<strong>B</strong>udget, <strong>S</strong>chedule, <strong>Q</strong>uality, <strong>S</strong>afety, & 
												<strong>P</strong>eople (<strong>B-S-Q-S & P</strong>) to everyone in your business.  We are here to supplement and 
												complement your efforts, to assist you and to provide support to your program without unnecessary �overhead�.<br><br>
												
												<strong>HB/S</strong> Inspectors and Consultants are state certified and trained to provide detailed and accurate inspections and 
												reports. Our inspectors have a minimum certification of Georgia Level 1A, level 1B up to Georgia Level II Design 
												professional, and CPESC (Certified Professional in Erosion and Sediment Control). <br><br>

												We have been involved in EPA and EPD site inspections and enforcement actions, managing NPDES site compliance for bank 
												owned properties, stream bank restoration, commercial projects, national home builders and DOT projects.<br><br>
												
												Our knowledge, experience, and attention to detail enables us to help our customers manage risk and reduce the possibility 
												of violations and fines.<br><br>
												
												<strong>HB/S</strong> stays current with governmental standards and industry practices by working with a widely diversified 
												client base, and through association with organizations such as SWCC, EPA / EPD, IECA, OSHA, MSHA, CEFGA, NCCER, NUCA, GUCA, 
												ABC, AGC, ASSE, and Medic First Aid.  We maintain current authorizations to conduct training classes from these organizations 
												and we serve on various boards, committees, and advisory panels.<br><br>
												
												<strong>HB/S</strong> can provide specific pricing and/or RFP responses upon request.  An estimate with specific pricing 
												information will be provided to the client in advance of any services.<br><br>
												
												<strong>Inspections and Reporting Services</strong><br><br>
												<table cellpadding="0" cellspacing="0">
													<tr>
														<td valign="top" width="15"></td>
														<td>Daily rainfall, petroleum storage, and construction entrance inspections</td>
													</tr>
													<tr>
														<td valign="top" width="15"></td>
														<td>Weekly and post rain</td>
													</tr>
													<tr>
														<td valign="top" width="15"></td>
														<td>Infrastructure - 14 day and post rain</td>
													</tr>
													<tr>
														<td valign="top" width="15"></td>
														<td>Violations summaries</td>
													</tr>
													<tr>
														<td valign="top" width="15"></td>
														<td>Monthly inspections</td>
													</tr>
													<tr>
														<td valign="top" width="15"></td>
														<td>Storm water sampling and analysis</td>
													</tr>
												</table><br>
												
												<strong>Consulting Services</strong><br><br>

												Our consulting services can be purchased by the hour with a four hour minimum, per day, or by the half day or 
												full day, or by the week depending on your needs.<br><br>
												
												The following are Consultant services:<br><br>
												
												<table cellpadding="0" cellspacing="0">
													<tr>
														<td valign="top" width="15"></td>
														<td>NPDES Program Analysis</td>
													</tr>
													<tr>
														<td valign="top" width="15"></td>
														<td>Program Development</td>
													</tr>
													<tr>
														<td valign="top" width="15"></td>
														<td>Site Audits / Plan Updates</td>
													</tr>
													<tr>
														<td valign="top" width="15"></td>
														<td>�Mock� EPD / EPA Site Inspections</td>
													</tr>
												</table><br>
												
												The following are regular Senior Consultant services:<br><br>
												<table cellpadding="0" cellspacing="0">
													<tr>
														<td valign="top" width="15"></td>
														<td>Plan review and amendment</td>
													</tr>
													<tr>
														<td valign="top" width="15"></td>
														<td>NPDES Inspection and Consultation</td>
													</tr>
													<tr>
														<td valign="top" width="15"></td>
														<td>Pre-Construction site assessments</td>
													</tr>
												</table><br>

												The following are Legal Consultant or Engineer Consultant services:<br><br>
												<table cellpadding="0" cellspacing="0">
													<tr>
														<td valign="top" width="15"></td>
														<td>NPDES Legal Consultation</td>
													</tr>
													<tr>
														<td valign="top" width="15"></td>
														<td>Engineering or Design Consultation</td>
													</tr>
													<tr>
														<td valign="top" width="15"></td>
														<td>Expert Witness, Case Deposition, Court or Arbitration</td>
													</tr>
												</table><br>
												
												<strong>Training Classes</strong><br><br>

												We offer a wide variety of training classes, some of which are listed below.  Please let us know if you 
												need a special class. We tailor classes for clients regularly.  We also offer monthly �OPEN� Classes for 
												companies with only a few employees who need training.<br><br>
												
												NPDES (National Pollutant Discharge Elimination System)<br>
												Level 1A & 1B Class and Exam<br>
												Level 1A & 1B Recertification<br><br>
												
												We also offer OSHA, safety awareness, and competent person safety training courses.<br><br>
												
												Most of our classes can be scheduled on a Saturday, or after regular working hours, during evenings, or split over multiple 
												days for an additional charge of $100.00 per �split� or evening class, and $200.00 per Saturday class.<br><br>
												
												Saturday classes require a non-refundable deposit of $500.00. The deposit will be applied to the final invoice. If the deposit 
												is not received at the <strong>HB/S</strong> office at least ten days prior to the day of the class, the class may not be scheduled.<br><br>
												
												A laminated student class card is generally issued upon satisfactory completion of most classes. <strong>HB/S</strong> will provide sign-in 
												sheets, evaluation sheets, and tests (if applicable).<br><br>
												
												<strong>Equipment and Supplies</strong><br><br>
												
												<%=rs("customerName")%> is to provide training facility, tables, chairs, pens, highlighters, similar supplies, and audio-visual 
												equipment.  Lunch, breakfast or �break� items (if applicable) and any special request items are provided by <%=rs("customerName")%>.  
												<strong>HB/S</strong> can coordinate and arrange for these items upon request, for an additional fee.<br><br>

												<strong>HB/S</strong> can provide audio-visual equipment upon request at $50.00 per day. The <strong>HB/S</strong> onsite training center in 
												Lawrenceville, GA is available upon request at $200.00 per day plus audio-visual equipment rental.<br><br>
												
												<strong>Sequence Software (Field Information Management Solution)</strong><br><br>

												Sequence is an Internet based Field Information Management Solution that helps organizations manage field level information 
												and processes more efficiently.   With Sequence, companies can capture field level data, automatically distribute it to 
												designated project, corporate and third party personnel, manage alerts / notifications, and retain the data in a central 
												secure data center.<br><br>
												
												Sequence can coexist with existing project management, scheduling, accounting and human resource software. <br><br> 
												
												Sequence is built on a SaaS (Software as a Service) methodology that allows a customer to pay only for the services used. <br><br>
												
												<em>�If you are considering hiring a software developer or purchasing a departmental software solution to improve a business process, 
												I would recommend that you talk to HB/S before you make a final decision.� Dale Hoffman � Workplace Compliance Manager - KB Home</em><br><br>
												
												<em>�We manage all of our DOT Field level environmental, accident and workers compensation reports through the Sequence platform.  
												Sequence was easy to implement, it required minimal training and it is easy to use.� Brian Oden � Environmental and Safety 
												Manager � CW Matthews Contracting</em><br><br>
												
												<strong>Additional Notes</strong><br><br>

												Scheduled services may require a retainer to �hold the day�.  The retainer will be applied to the final invoice, which is due 
												and payable upon completion of services.<br><br>
												
												Inspection services require a $250.00 Site activation and set up fee. The fee will include permit box installed onsite, logbook, 
												rain gauge, document review, and three years record retention.<br><br>
												
												A Handling Fee of $200.00 will be charged for any scheduled services that are canceled and/or re-scheduled within five days 
												of the event. At the discretion of <strong>HB/S</strong>, this Handling Fee will be waived if the scheduled time slot can be 
												otherwise utilized.<br><br>
												
												None of the prices in this proposal cover any travel, lodging, meals, shipping, postage, copying, or similar expenses.  
												Mileage is billed from our office at the current �IRS� standard mileage rates.  All pre-paid expenses are due when incurred.<br><br>
												
												Client agrees that during the term of the Engagement and for one year after termination of the Engagement, client will not offer 
												employment or non-employee contract positions, either directly or through other indirect means, to Consultants or other employees 
												or agents of <strong>HB/S</strong>. Client agrees that it will also take reasonable steps to ensure that its own clients to whom 
												it is providing services, or to whom <strong>HB/S</strong> employees or agents have been introduced will be bound by the same 
												restrictions.<br><br>

												<strong>HB/S</strong> agrees to hold all client information confidential.<br><br>
												
												<strong>Contacts and References</strong><br><br>

												<strong>HB/S</strong> Inspectors, Consultants, Software Developers and resources will be engaged as applicable to your 
												exact needs.  Our office is open Monday - Friday, 8am to 5pm.<br><br>
												
												
												In closing, <strong>HB/S</strong> has a proven track record of �results� with our services.  We offer a few references here.  
												Additional references are available upon request.<br><br>
												
												<table width="500" cellpadding="0" cellspacing="0">
													<tr>
														<td>KB Home</td>
														<td>Dale Hoffman</td>
														<td>720-323-7066</td>
													</tr>
													<tr>
														<td>CW Matthews</td>
														<td>Brian Oden</td>
														<td>404-226-0106</td>
													</tr>
													<tr>
														<td>Windswept Organix</td>
														<td>Kevin Stumpff</td>
														<td>602-882-1636</td>
													</tr>
													<tr>
														<td>Bank of North Georgia</td>
														<td>Joseph Sumner</td>
														<td>770-349-5323</td>
													</tr>
													<tr>
														<td>S&ME Engineering</td>
														<td>Michelle Logut</td>
														<td>919-801-5897</td>
													</tr>
													<tr>
														<td>Georgia Power</td>
														<td>Greg Gilreath</td>
														<td>404-608-5799</td>
													</tr>
												</table><br>
												
												We appreciate the opportunity to be of service to you and <%=rs("customerName")%> and we look forward to working with you in 
												the future.  If you have any questions, need additional pricing information, or would like to schedule our services, please 
												contact us at our office at (678) 336-1357 or visit our website at 
												<a href="http://www.next-sequence.com" target="_blank">www.next-sequence.com</a>.<br><br>
												
												Sincerely,<br><br> 
												
												<%if rsUser("sigFileName") <> "" then%> 
													<img src="userUploads/<%=rsUser("userID")%>/<%=rsUser("sigFileName")%>"><br>
												<%else%>
													<br><br>
												<%end if%>
												<%=rsUser("firstName")%>&nbsp;<%=rsUser("lastName")%><br>
												<br>						
												<%=rsUser("cellPhone")%><br>
												<%=rsUser("email")%><br>

												<strong>HB/Sequence - NEXT</strong>


											</td>
											<td width="30"><td>
										</tr>
										
										
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td colspan="3" align="right">
												<table>
													<%if blnVerify = "True" Then%>
														<tr><td><input type="radio" name="emailPack" value="emailPack" checked="checked">&nbsp;</td><td align="left">Email pack to selected contact</td></tr>
														<tr><td><input type="radio" name="emailPack" value="faxPack">&nbsp;</td><td align="left">Fax pack to selected contact</td></tr>
													<%else%>
														
													<%end if%>
												</table>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td colspan="3" align="right">
												<input type="hidden" name="verifiedBy" value="<%=session("ID")%>"/>
												<input type="hidden" name="userID" value="<%=userID%>"/>
												<input type="hidden" name="customerID" value="<%=customerID%>"/>
												<input type="hidden" name="clientID" value="<%=clientID%>"/>
												<input type="hidden" name="contactID" value="<%=contactID%>"/>
												<input type="hidden" name="openingParagraph" value="<%=openingParagraph%>"/>
												<input type="hidden" name="date" value="<%=sdate%>"/>
												
												<%if blnVerify = "True" Then%>
													<input type="hidden" name="infoPackID" value="<%=infoPackID%>"/>
													<input type="hidden" name="processType" value="verifyInfoPack" />
													<INPUT type="button" value=" Go Back " onClick="history.back()" class="formButton">&nbsp;&nbsp;&nbsp;<input type="submit" value="Finish/Send" class="formButton"/>
												<%else%>
													<input type="hidden" name="processType" value="addInfoPack" />
													<input type="submit" value="Save" class="formButton"/>
												<%end if%>
											</td>
										</tr>
										
									</table>
								
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<script language="JavaScript" type="text/javascript">

  var frmvalidator  = new Validator("addInfoPack");
 // frmvalidator.addValidation("numberOfLots","req","Please enter the number of lots you need");
 // frmvalidator.addValidation("numberOfLots","numeric");
 // frmvalidator.addValidation("startingLotNumber","req","Please enter the starting lot number");
 // frmvalidator.addValidation("startingLotNumber","numeric");
</script>
<%
rsPhase.close
set rsPhase = nothing
DataConn.close
set DataConn = nothing
%>
