<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
projectID = trim(Request("projectID"))
selectedProject = trim(Request("project"))
importFrom = request("importFrom")

Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If


On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")
	
if importFrom = "project" then
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetAssignedProjects"
	   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, session("ID"))
	   .CommandType = adCmdStoredProc   
	End With
				
	Set rsProjects = oCmd.Execute
	Set oCmd = nothing
	
	If selectedProject <> "" then
		'Create command
		'may need to add user to this because may want to be only for specific user
		Set oCmd = Server.CreateObject("ADODB.Command")
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetContactsByProject"
		   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, selectedProject)
		   .CommandType = adCmdStoredProc   
		End With
					
		Set rs = oCmd.Execute
		Set oCmd = nothing
	end if
else
	If session("superAdmin") = "True" Then
		Set oCmd = Server.CreateObject("ADODB.Command")	
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetUserList"
		   .CommandType = adCmdStoredProc   
		End With
					
		Set rs = oCmd.Execute
		Set oCmd = nothing
	else
	
		Set oCmd = Server.CreateObject("ADODB.Command")	
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetUserListForClient"
		   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, session("clientID"))
		   .CommandType = adCmdStoredProc   
		End With
					
		Set rs = oCmd.Execute
		Set oCmd = nothing
	
	end if
end if


'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetProject"
   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
   .CommandType = adCmdStoredProc   
End With
			
Set rsProject = oCmd.Execute
Set oCmd = nothing
%>

<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script language="JavaScript">
<!--
function dept_onchange(contactList) {
   document.contactList.action = "importContacts.asp";
   contactList.submit(); 
}
//-->
</script>
</head>
<body>
<form name="contactList" method="post" action="process.asp">
<input type="hidden" name="projectID" value="<%=projectID%>">
<input type="hidden" name="processType" value="importContacts" />
<input type="hidden" name="importFrom" value="<%=importFrom%>">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sLoginTitle%></span><span class="Header"> - Import Contacts</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr><td colspan="8"><b>Import Contacts to:</b> <%=rsProject("projectName")%></td></tr>
										<tr><td colspan="8"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<%if importFrom = "project" then%>
										<tr>
											<td></td>
											<td colspan="8" valign="top">
											
												
												Select the project you want to import from:<br>
												<select name="project" onChange="return dept_onchange(contactList)">
													<option value="">--Select Project--</option>
													<%do until rsProjects.eof
													if trim(rsProjects("projectID")) = trim(selectedProject) then%>
														<option selected="selected" value="<%=rsProjects("projectID")%>"><%=rsProjects("customerName")%> - <%=rsProjects("projectName")%></option>
													<%else%>
														<option value="<%=rsProjects("projectID")%>"><%=rsProjects("customerName")%> - <%=rsProjects("projectName")%></option>
													<%end if
													rsProjects.movenext
													loop%>
												</select>											
											</td>
										</tr>
										<tr><td colspan="8"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<%end if%>
										<tr bgcolor="#666666">
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td><span class="searchText">Contact Name</span></td>
											<td><span class="searchText">Email Address</span></td>
											<td><span class="searchText">Company Name</span></td>
											<%if importFrom = "project" then%>	
											<td><span class="searchText">Gets Auto Email</span></td>
											<%end if%>					
											<td align="center"><span class="searchText">Import</span></td>
										</tr>
										<tr><td colspan="9"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<%If rs.eof then%>
											<tr><td></td><td colspan="9">there are no records to display</td></tr>
										<%else
											blnChange = true
											Do until rs.eof
												If blnChange = true then%>
													<tr class="rowColor">
												<%else%>
													<tr>
												<%end if%>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td><%=rs("firstName")%>&nbsp;<%=rs("lastName")%></td>							
													<td><a href="mailto:<%=rs("email")%>"><%=rs("email")%></a></td>
													<%if importFrom = "project" then%>
													<td><%=rs("companyName")%></td>
													<%else%>
													<td><%=rs("company")%></td>
													<%end if%>
													<%if importFrom = "project" then%>
													<td align="center">
														<%if rs("getAutoEmail") = "True" then%>
															<img src="images/check.gif">
														<%end if%>
													</td>
													<%end if%>
													<td align="center">
														<%if importFrom = "project" then%>
															<input type="checkbox" name="import" value="<%=rs("contactID")%>">
														<%else%>
															<input type="checkbox" name="import" value="<%=rs("userID")%>">
														<%end if%>
													</td>
												</tr>
											<%
											'get the values from all the checkboxes
											if importFrom = "project" then
												sAllVal = sAllVal & rs("contactID")
											else
												sAllVal = sAllVal & rs("userID")
											end if
											
											rs.movenext
											
											if not rs.eof then
												sAllVal = sAllVal & ","
											end if
											
											if blnChange = true then
												blnChange = false
											else
												blnChange = true
											end if
											loop
										end if%>
										<tr><td colspan="8" height="10"></td></tr>
										<tr>
											<td colspan="8" align="right">
												<input type="hidden" name="allBoxes" value="<%=sAllVal%>">
												<input type="submit" name="importContact" value="Import" class="formButton">
											</td>
										</tr>
									</table>
									
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>