<%
clientID = request("clientID")
sState = request("projectState")
quoteID = request("id")

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetQuote"
   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, quoteID)
   .CommandType = adCmdStoredProc   
End With

Set rs = oCmd.Execute
Set oCmd = nothing

'Create command for state list
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetStates"
   .CommandType = adCmdStoredProc
   
End With
	
Set rsState = oCmd.Execute
Set oCmd = nothing

if rs("projectState") <> "" then
	if sState = "" then
		sState = rs("projectState")
	end if
else
	sState = sState							
end if

If sState <> "" then

	Set oCmd = Server.CreateObject("ADODB.Command")
	
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetCountiesByState"
	   .parameters.Append .CreateParameter("@ID", adVarchar, adParamInput, 50, trim(sState))
	   .CommandType = adCmdStoredProc   
	End With
		
	Set rsCounty = oCmd.Execute
	Set oCmd = nothing

end if


Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetUserListForClient"
   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
   .CommandType = adCmdStoredProc   
End With

Set rsUsers = oCmd.Execute
Set oCmd = nothing

do until rsUsers.eof
	sUserList = sUserList & chr(34) & rsUsers("firstName") & " " & rsUsers("lastName") & chr(34)
rsUsers.movenext
if not rsUsers.eof then
	sUserList = sUserList & ","
end if
loop

%>
<script type="text/javascript" src="jquery/jquery.js"></script>
<script type='text/javascript' src='jquery/jquery.autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="jquery/jquery.autocomplete.css" />
<script type="text/javascript">
var users = [
	<%=sUserList%>
];
$().ready(function() {	
	$("#leadGeneratedBy").autocomplete(users);
});


function proj_onchange(addQuote) {
   document.addQuote.action = "form.asp?formType=addQuote";
   addQuote.submit(); 
}
</script>

<form name="addQuote" method="post" action="process.asp" autocomplete="off">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sNPDESTitle%></span><span class="Header"> - Edit Quote</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table border="0" cellpadding="0" cellspacing="0">									
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Bid Date:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="bidDate" maxlength="10" size="10" value="<%=rs("bidDate")%>"/>&nbsp;<a href="javascript:displayDatePicker('bidDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Project Name:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="projectName" size="30" value="<%=rs("projectName")%>" />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>Address:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="projectAddress" size="30" value="<%=rs("projectAddress")%>" />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><span class="required">*</span> <strong>City:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="projectCity" size="30" value="<%=rs("projectCity")%>" />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>State:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="projectState" onChange="return proj_onchange(addQuote)">
													<option value="">--select state--</option>
													<%do until rsState.eof
														if sState = rsState("stateID") then%>
															<option selected="selected" value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
														<%else%>
															<option value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
													<%end if
													rsState.movenext
													loop%>
												</select>&nbsp;&nbsp;<strong>Zip:</strong>&nbsp;<input type="text" name="projectZip" size="5" value="<%=rs("projectZip")%>"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>County:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%if sState = "" then%>
													Please select a state above.
												<%else%>
													<select name="county">
														<%do until rsCounty.eof
															if trim(rs("county")) = trim(rsCounty("countyID")) then%>
																<option selected="selected" value="<%=rsCounty("countyID")%>"><%=rsCounty("county")%></option>
															<%else%>
																<option value="<%=rsCounty("countyID")%>"><%=rsCounty("county")%></option>
														<%end if
														rsCounty.movenext
														loop%>
													</select>
												<%end if%>
												
											</td>
										</tr>																					
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Project Type:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="projectType">
													<option <%=isSelected(rs("projectType"),"Land Development")%>>Land Development</option>
													<option <%=isSelected(rs("projectType"),"Linear")%>>Linear</option>													
													<option <%=isSelected(rs("projectType"),"Lot")%>>Lot</option>
													<option <%=isSelected(rs("projectType"),"Vertical Construction")%>>Vertical Construction</option>
												</select>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Project Size:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" id="projectSize" name="projectSize" size="30" value="<%=rs("projectSize")%>" maxlength="50" />
											</td>
										</tr>
										
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Lead Generated By:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<div id="content">
													<input type="text" id="leadGeneratedBy" name="leadGeneratedBy" size="30" value="<%=rs("leadGeneratedBy")%>" maxlength="50" />
												</div>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Lead Source:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="leadSource">
													<option <%=isSelected(rs("leadSource"),"LDI")%>>LDI</option>
													<option <%=isSelected(rs("leadSource"),"Reed Connect")%>>Reed Connect</option>													
													<option <%=isSelected(rs("leadSource"),"Newspaper")%>>Newspaper</option>
													<option <%=isSelected(rs("leadSource"),"Customer")%>>Customer</option>
													<option <%=isSelected(rs("leadSource"),"Employee")%>>Employee</option>
													<option <%=isSelected(rs("leadSource"),"Other")%>>Other</option>
												</select>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="15"></td></tr>
										<tr>
											<td valign="top" colspan="3"><strong>24-Hour Site Contact</strong></td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Name:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="TFHourName" size="30" value="<%=rs("TFHourName")%>" maxlength="50" />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Phone Number:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="TFHourPhone" size="20" value="<%=rs("TFHourPhone")%>" maxlength="20" />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td colspan="3" align="right">
												<input type="hidden" name="quoteID" value="<%=quoteID%>" />
												<input type="hidden" name="clientID" value="<%=clientID%>" />
												<input type="hidden" name="processType" value="editQuote" />
												<input type="submit" value="  Save  " class="formButton"/>
											</td>
										</tr>
									</table>
								
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<script language="JavaScript" type="text/javascript">

  var frmvalidator  = new Validator("addQuote");
  frmvalidator.addValidation("bidDate","req","Please enter the bid date");
  frmvalidator.addValidation("projectName","req","Please enter the project name");
  frmvalidator.addValidation("projectAddress","req","Please enter the project address");
  frmvalidator.addValidation("projectCity","req","Please enter the project city");
  frmvalidator.addValidation("projectSize","req","Please enter the project size");
  frmvalidator.addValidation("leadGeneratedBy","req","Who generated the lead for this quote?");
  
  
//  frmvalidator.addValidation("contactEmail","email");
//  frmvalidator.addValidation("address1","req","Please enter the customer's address");
//  frmvalidator.addValidation("city","req","Please enter the customer's city");
//  frmvalidator.addValidation("zip","req","Please enter the customer's zip code");
//  frmvalidator.addValidation("phone","req","Please enter the customer's phone number");
</script>

<%
rsState.close
set rsState = nothing
DataConn.close
set DataConn = nothing
%>