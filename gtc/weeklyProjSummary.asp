<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
projectID = trim(Request("project"))

Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If


On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetAssignedProjects"
   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, session("ID"))
   .CommandType = adCmdStoredProc   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsProjects = oCmd.Execute
Set oCmd = nothing

If projectID <> "" then
	'Create command
	'may need to add user to this because may want to be only for specific user
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetWeeklySummaryList"
	   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, projectID)
	   .CommandType = adCmdStoredProc   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
				
	Set rs = oCmd.Execute
	Set oCmd = nothing
end if
%>

<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script language="JavaScript">
<!--
function confirmDelete(delUrl) {
 if (confirm("Are you sure you wish to delete this summary item?")) {
    document.location = delUrl;
  }


}

function dept_onchange(summaryList) {
   document.summaryList.action = "weeklyProjSummary.asp";
   summaryList.submit(); 
}

//-->
</script>
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sLoginTitle%></span><span class="Header"> - Weekly Project Summary List</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<!--<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;
						<%'if projectID <> "" then%>
							<a href="form.asp?formType=addContact&id=<%'=projectID%>" class="footerLink">add contact</a>
						<%'end if%>			
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>-->
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td></td>
											<td colspan="8">
											<form name="summaryList" method="post" action="process.asp">
												<select name="project" onChange="return dept_onchange(summaryList)">
													<option value="">--Select Project--</option>
													<%do until rsProjects.eof
													if trim(rsProjects("projectID")) = trim(projectID) then%>
														<option selected="selected" value="<%=rsProjects("projectID")%>"><%=rsProjects("customerName")%> - <%=rsProjects("projectName")%></option>
													<%else%>
														<option value="<%=rsProjects("projectID")%>"><%=rsProjects("customerName")%> - <%=rsProjects("projectName")%></option>
													<%end if
													rsProjects.movenext
													loop%>
												</select>
											
											</td>
										</tr>
										<tr><td colspan="8"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<%if projectID <> "" then%>
											<tr>
												<td></td>
												<td colspan="8">
													Add a weekly summary for this project.<br>
													<textarea name="summary" cols="40" rows="10"></textarea>
													<input type="hidden" name="processType" value="addWeeklySummary">
													<input type="submit" value="Save" class="formButton">
												</td>
											</tr>
										<%end if%>
										</form>
										<tr><td colspan="8"><img src="images/pix.gif" width="1" height="30"></td></tr>
										<tr bgcolor="#666666">												
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td valign="top" width="100"><span class="searchText">Date Entered</span></td>
											<td width="10"></td>
											<td valign="top"><span class="searchText">Summary Entry</span></td>
											<td width="20"></td>							
										</tr>
										<%If rs.eof then%>
											<tr><td></td><td colspan="9">there are no records to display</td></tr>
										<%else
											blnChange = false
											Do until rs.eof
												If blnChange = true then%>
													<tr class="rowColor">
												<%else%>
													<tr>
												<%end if%>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td valign="top"><%=formatdatetime(rs("dateEntered"),2)%></td>
													<td></td>
													<td valign="top"><%=rs("summary")%></td>	
													<td></td>						
													<!--<td align="center"><a href="form.asp?id=<%'=rs("contactID")%>&formType=editContact&projectID=<%'=projectID%>"><img src="images/edit.gif" width="19" height="19" alt="edit" border="0"></a></td>
													<td align="center"><a href=""><input type="image" src="images/remove.gif" width="11" height="13" alt="delete" border="0" onClick="return confirmDelete('process.asp?id=<%'=rs("contactID")%>&processType=deleteContact&projectID=<%'=projectID%>')"></a></td>-->
												</tr>
												<tr><td colspan="8" height="10"></td></tr>
											<%rs.movenext
											if blnChange = true then
												blnChange = false
											else
												blnChange = true
											end if
											loop
										end if%>
									</table>
									
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>