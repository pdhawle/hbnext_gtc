<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%'need customerID, reportID

quoteID = request("quoteID")
customerQuoteID = request("customerQuoteID")
userID = request("userID")

Session("LoggedIn") = "True"
Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 	
DataConn.Open Session("Connection"), Session("UserID")

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetQuoteforPDF"
    .parameters.Append .CreateParameter("@quoteID", adInteger, adParamInput, 8, quoteID)
	.parameters.Append .CreateParameter("@customerQuoteID", adInteger, adParamInput, 8, customerQuoteID)
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsQuote = oCmd.Execute
Set oCmd = nothing

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetUser"
    .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, userID)
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsUser = oCmd.Execute
Set oCmd = nothing
%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
</head>
<style>
	body, table, td, span, div, p {
	font-family: Arial, Helvetica, sans-serif;
	font-size:12px;
}
</style>
<body>
<table class="borderTable" width="600" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="4">
			<table border="0" cellpadding="0" cellspacing="0">
				<tr bgcolor="#FFFFFF">
					<td><img src="images/quoteHeaderBW.gif" width="615" height="70" /></td>
				</tr>
				<tr bgcolor="#FFFFFF"><td><img src="images/pix.gif" width="1" height="10"></td></tr>
			</table>
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left">
									<span class="Header">Project Name: <%=rsQuote("projectName")%></span><br><br>
									<table cellpadding="0" cellspacing="0">
										<tr><td><strong>Location:</strong></td><td>&nbsp;<%=rsQuote("projectAddress")%></td></tr>
										<tr><td><strong>County:</strong></td><td>&nbsp;<%=rsQuote("countyName")%></td></tr>
										<tr><td><strong>City:</strong></td><td>&nbsp;<%=rsQuote("projectCity")%></td></tr>
										<tr><td><strong>State:</strong></td><td>&nbsp;<%=rsQuote("projectState")%></td></tr>
										<tr><td><strong>Zip:</strong></td><td>&nbsp;<%=rsQuote("projectZip")%></td></tr>
									</table><br>
									
									<strong>Quote: NPDES and Water Quality Monitoring</strong><br>
									<strong>Bid Date: <%=formatDateTime(rsQuote("bidDate"),2)%></strong>
								</td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5" height="10">
					</td>
				</tr>
				<!--end of nav bars for the sub pages-->
				
				<tr>
					<td colspan="5">
						<table width="100%">
							<tr>								
								<td valign="middle" align="left">
									<table width="100%" cellpadding="0" cellspacing="0">
										<tr>
											<td valign="top">
												<strong><%=rsQuote("customerName")%></strong><br>
												<%=rsQuote("address")%><br>
												<%=rsQuote("city")%>,&nbsp;<%=rsQuote("state")%>&nbsp;<%=rsQuote("zip")%>
											</td>
											<td>
												<table cellpadding="0" cellspacing="0">
													<tr><td><strong>Estimator: </strong></td><td>&nbsp;<%=rsQuote("contactName")%></td></tr>
													<tr><td><strong>Phone: </strong></td><td>&nbsp;<%=rsQuote("contactPhone")%></td></tr>
													<tr><td><strong>Fax: </strong></td><td>&nbsp;<%=rsQuote("contactFax")%></td></tr>
													<tr><td><strong>Email: </strong></td><td>&nbsp;<%=rsQuote("contactEmail")%></td></tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>						
					</td>
				</tr>				
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<table width="600" cellpadding="0" cellspacing="0" border="0">
										<tr><td colspan="20"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<%
										cEach = 0
										cMonthly = 0
										cOneTime = 0
										cQuarterly = 0
										%>	
										<tr bgcolor="#666666">
											<td></td>
											<td align="center"><span class="searchText">One Time Fee</span></td>
											<td align="center"><span class="searchText">Monthly</span></td>
											<td align="center"><span class="searchText">Each</span></td>
											<td align="center"><span class="searchText">Quarterly</span></td>
										</tr>
										<tr>
											<td colspan="4">&nbsp;<strong><u>Inspections</u></strong></td>
										</tr>
										<tr class="rowColor">
											<td>&nbsp;<strong>Weekly Inspection</strong></td>
											<td></td>
											<td align="center">
												<%if rsQuote("wFreq") = "Monthly" then
													response.Write formatCurrency(rsQuote("wRate"),2)
													cMonthly = formatcurrency(cMonthly + rsQuote("wRate"),2)
												end if%>&nbsp;
											</td>
											<td align="center">
												<%if rsQuote("wFreq") = "Each" then
													response.Write formatCurrency(rsQuote("wRate"),2)
													cEach = formatcurrency(cEach + rsQuote("wRate"),2)
												end if%>&nbsp;
											</td>
											<td></td>
										</tr>
										<tr>
											<td>&nbsp;<strong>Post-Rain Inspection</strong></td>
											<td></td>
											<td align="center">
												<%if rsQuote("prFreq") = "Monthly" then
													response.Write formatCurrency(rsQuote("prRate"),2)
													cMonthly = formatcurrency(cMonthly + rsQuote("prRate"),2)
												end if%>&nbsp;
											</td>
											<td align="center">
												<%if rsQuote("prFreq") = "Each" then
													response.Write formatCurrency(rsQuote("prRate"),2)
													cEach = formatcurrency(cEach + rsQuote("prRate"),2)
												end if%>&nbsp;
											</td>
											<td></td>
										</tr>
										<tr class="rowColor">
											<td>&nbsp;<strong>Weekly and Post Rain Inspection</strong></td>
											<td></td>
											<td align="center">
												<%if rsQuote("wprFreq") = "Monthly" then
													response.Write formatCurrency(rsQuote("wprRate"),2)
													cMonthly = formatcurrency(cMonthly + rsQuote("wprRate"),2)
												end if%>&nbsp;
											</td>
											<td align="center">
												<%if rsQuote("wprFreq") = "Each" then
													response.Write formatCurrency(rsQuote("wprRate"),2)
													cEach = formatcurrency(cEach + rsQuote("wprRate"),2)
												end if%>&nbsp;
											</td>
											<td></td>
										</tr>
										<tr>
											<td>&nbsp;<strong>Weekly, Post Rain and Daily Inspection</strong></td>
											<td></td>
											<td align="center">
												<%if rsQuote("wprdFreq") = "Monthly" then
													response.Write formatCurrency(rsQuote("wprdRate"),2)
													cMonthly = formatcurrency(cMonthly + rsQuote("wprdRate"),2)
												end if%>&nbsp;
											</td>
											<td align="center">
												<%if rsQuote("wprdFreq") = "Each" then
													response.Write formatCurrency(rsQuote("wprdRate"),2)
													cEach = formatcurrency(cEach + rsQuote("wprdRate"),2)
												end if%>&nbsp;
											</td>
											<td></td>
										</tr>
										<tr class="rowColor">
											<td>&nbsp;<strong>Daily Rain, Petroleum and CO Pad Insp.</strong></td>
											<td></td>
											<td align="center">
												<%if rsQuote("dFreq") = "Monthly" then
													response.Write formatCurrency(rsQuote("dRate"),2)
													cMonthly = formatcurrency(cMonthly + rsQuote("dRate"),2)
												end if%>&nbsp;
											</td>
											<td align="center">
												<%if rsQuote("dFreq") = "Each" then
													response.Write formatCurrency(rsQuote("dRate"),2)
													cEach = formatcurrency(cEach + rsQuote("dRate"),2)
												end if%>&nbsp;
											</td>
											<td></td>
										</tr>
										<tr>
											<td colspan="5">&nbsp;<strong>Daily Rain, Petroleum and CO Pad Log - included</strong></td>
										</tr>
										<tr><td colspan="6" height="10"></td>
										<tr>
											<td colspan="5">&nbsp;<strong><u>Water Sampling</u></strong></td>
										</tr>
										<tr class="rowColor">
											<td colspan="5">&nbsp;<strong>4 Water Samples and Analysis - included</strong></td>
										</tr>
										<tr>
											<td>&nbsp;<strong>Additional Water Samples and Analysis (If Needed)***</strong></td>
											<td></td>
											<td align="center"></td>
											<td align="center">
												<%response.Write formatCurrency(rsQuote("waterSampRate"),2)
												cEach = formatcurrency(cEach + rsQuote("waterSampRate"),2)%>&nbsp;
												
											</td>
											<td></td>
										</tr>
										<tr><td colspan="5"><em>***Note*** non-compliance or cited violation requires samples until compliant</em></td>
										<tr><td colspan="5" height="10"></td>
										<tr>
											<td colspan="4">&nbsp;<strong><u>Site Audits</u></strong></td>
										</tr>
										<tr class="rowColor">
											<td>&nbsp;<strong>BMP Performance Audit and Estimate</strong></td>
											<td></td>
											<td align="center"></td>
											<td align="center">
												<%response.Write formatCurrency(rsQuote("bmpPerfAudit"),2)
												cEach = formatcurrency(cEach + rsQuote("bmpPerfAudit"),2)%>&nbsp;
											</td>
											<td></td>
										</tr>
										<tr><td colspan="6" height="10"></td>
										<tr>
											<td colspan="5">&nbsp;<strong><u>NEXT Sequence License</u></strong></td>
										</tr>
										<tr class="rowColor">
											<td>&nbsp;<strong>Single Project</strong></td>
											<td></td>
											<td align="center">
												<%if rsQuote("licSingleFreq") = "Monthly" then
													response.Write formatCurrency(rsQuote("licSingle"),2)
													cMonthly = formatcurrency(cMonthly + rsQuote("licSingle"),2)
												end if%>&nbsp;
											</td>
											<td align="center">
												<%if rsQuote("licSingleFreq") = "Each" then
													response.Write formatCurrency(rsQuote("licSingle"),2)
													cEach = formatcurrency(cEach + rsQuote("licSingle"),2)
												end if%>&nbsp;
											</td>
											<td></td>
										</tr>
										<tr>
											<td>&nbsp;<strong>Multiple Project</strong></td>
											<td></td>
											<td align="center">
												<%if rsQuote("licMultipleFreq") = "Monthly" then
													response.Write formatCurrency(rsQuote("licMultiple"),2)
													cMonthly = formatcurrency(cMonthly + rsQuote("licMultiple"),2)
												end if%>&nbsp;
											</td>
											<td align="center"></td>
											<td align="center">
												<%if rsQuote("licMultipleFreq") = "Quarterly" then
													response.Write formatCurrency(rsQuote("licMultiple"),2)
													cQuarterly = formatcurrency(cQuarterly + rsQuote("licMultiple"),2)
												end if%>&nbsp;
											</td>
										</tr>
										<tr><td colspan="6" height="10"></td>
										<tr>
											<td colspan="5">&nbsp;<strong><u>Site Setup</u></strong></td>
										</tr>
										<tr class="rowColor">
											<td>&nbsp;<strong>Activation</strong></td>
											<td align="center">
												<%response.Write formatCurrency(rsQuote("siteActivationFee"),2)
												cOneTime = formatcurrency(cOneTime + rsQuote("siteActivationFee"),2)
												%>&nbsp;</td>
											<td align="center"></td>
											<td align="center"></td>
											<td></td>
										</tr>
										<tr><td colspan="5"><em>includes job box, field records manual, rain gauge, document review, and 3 year records retention</em></td>
										<tr><td colspan="5" height="10"></td>
										<tr bgcolor="#666666">
											<td align="right"><span class="searchText">Totals:</span>&nbsp;&nbsp;</td>
											<td align="center"><span class="searchText"><%=formatCurrency(cOneTime,2)%>&nbsp;</span></td>
											<td align="center"><span class="searchText"><%=formatCurrency(cMonthly,2)%></span></td>
											<td align="center"><span class="searchText"><%=formatCurrency(cEach,2)%>&nbsp;</span></td>
											<td align="center"><span class="searchText"><%=formatCurrency(cQuarterly,2)%>&nbsp;</span></td>
										</tr>
									</table>
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5" height="10"></td></tr>
				<tr>
					<td colspan="6">
						By signing below you agree to the terms and conditions outlined in this proposal.<br><br>
						<table cellpadding="0" cellspacing="0">
							<tr>
								<td>
									____________________________<br>
									<strong><%=rsQuote("customerName")%></strong>
								</td>
								<td width="50"></td>
								<td>
									____________________________<br>
									<strong>Date</strong>
								</td>
							</tr>
						</table>
						
					</td>
				</tr>
				<tr><td colspan="5" height="5"></td></tr>
				<tr>
					<td colspan="5">
						<table width="100%">
							<tr>								
								<td valign="middle" align="left">
									Thanks for the opportunity to quote this project.<br><br>
									<%=rsUser("firstName")%>&nbsp;<%=rsUser("lastName")%><br>
									<%=rsUser("cellPhone")%><br>
									<%=rsUser("Email")%><br><br>
									
									<%if rsUser("clientID") = 1 then 'this is for NEXT%>
										The proven leader in on-demand records and data management, NEXT Sequence allows customers to take control of their critical inspection 
										data and reports. NEXT Sequence is the nations 1st real-time web based NPDES data, document management and collaboration solution. We 
										have become the new standard by delivering the most innovative technology and making it as easy as possible to capture, distribute and 
										manage field inspection information. Our products combine award-winning functionality, proven integration, point-and-click customization, 
										mobile capabilities, and a simple to use interface into one manageable solution.
										
									<%end if%>
									<br><br>
									<span class="footer">Quote ID: <%=rsQuote("quoteID")%></span>
								</td>
							</tr>
						</table>						
					</td>
				</tr>	
				
				
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="50"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<tr>
		<td colspan="3"><img src="images/quoteFooterBW.gif" width="615" height="29"><!--<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr><td colspan="4"><img src="images/reportFooterBarTop.gif" width="609" height="27"></td></tr>
				<tr><td width="45"><img src="images/reportFooterBarLeft.gif" width="45" height="39"></td><td bgcolor="#000000" align="center" width="600"><span class="footerLink"><strong>Web -  www.next-sequence.com</strong>&nbsp;&nbsp;&nbsp;<strong>Ph -  770.554.6509</strong>&nbsp;&nbsp;&nbsp;<strong>F -  770.554.6532</strong><br><em><strong>The Power of Instant Access to Critical Data</strong></em></span></td><td><img src="images/reportFooterBarRight.gif" width="29" height="39"></td></tr>
			</table>--></td>
	</tr>
	</table>
	<table width="300" cellpadding="0" cellspacing="0" border="0">
		<tr align="left">
			<td><img src="images/poweredBy.gif" width="186" height="40" /></td>
		</tr>
	</table>
</table>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>