<%

'get info for this customer
'Set oCmd = Server.CreateObject("ADODB.Command")
'With oCmd
'   .ActiveConnection = DataConn
'   .CommandText = "spGetCustomer"
'   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, rsProjInfo("customerID"))
'   .CommandType = adCmdStoredProc   
'End With
			
'Set rsCustomer = oCmd.Execute
'Set oCmd = nothing

If iDivisionID <> "" and iProjectID <> "" then
	'Create command for question category list
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetEPACategories"'ByDivision
	   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, iDivisionID)
	   .CommandType = adCmdStoredProc
   
	End With		
	Set rsCategories = oCmd.Execute
	Set oCmd = nothing
end if%>


<style>
	#myformCust label {
	  font-family: Arial, Helvetica, sans-serif;
	  font-size:11px;
	}	
	#quesListCust {
	  margin: 0px 0px 0px 0px;
	  display: none;
	}
</style>
	
<div id="myformCust">
<input type="radio" name="showQuestionsCust" onclick="javascript: $('#quesListCust').show('fast');" value="show" /> show questions&nbsp;&nbsp;<input checked="checked" type="radio" name="showQuestionsCust" value="hide" onclick="javascript: $('#quesListCust').hide('fast');" /> hide questions<br>
<div id="quesListCust">		
<%If iDivisionID <> "" and iProjectID <> "" then%>	
<%Do until rsCategories.eof%>
	<table cellpadding="0" cellspacing="0" border="0">
		<tr><td colspan="10" height="15"></td></tr>
		<tr><td></td><td colspan="5"><strong><%=rsCategories("category")%></strong></td></tr>	
		<tr>
			<td><img src="images/pix.gif" width="5" height="0"></td>
			<td valign="top">
				<%
					i = 1
					Set oCmd = Server.CreateObject("ADODB.Command")
	
					With oCmd
					   .ActiveConnection = DataConn
					   .CommandText = "spGetEPAQuestion"
					   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, rsCategories("categoryID"))
					   .CommandType = adCmdStoredProc
					   
					End With
								
					Set rsQuestion = oCmd.Execute
					Set oCmd = nothing
					%>
					
					<table width="800" cellpadding="0" cellspacing="0" border="0">
						<%blnChange = true
						do until rsQuestion.eof
							If blnChange = true then%>
							<tr class="rowColor">
							<%else%>
							<tr>
							<%end if%>
								<td valign="top">
									<br />&nbsp;<strong><%=i%>.</strong>&nbsp;
								</td>
								<td valign="top" align="left">
									<br /><%=rsQuestion("question")%><br />
									<input type="hidden" name="customer_question_question<%=rsQuestion("questionID")%>" value="<%=rsQuestion("question")%>" />
									<input type="hidden" name="customer_question_category<%=rsCategories("categoryID")%>" value="<%=rsCategories("categoryID")%>" />
									
									<input type="radio" name="customer_question_answer<%=rsQuestion("questionID")%>" value="1" />&nbsp;<%=rsQuestion("YesAnswer")%><br />
									<input type="radio" name="customer_question_answer<%=rsQuestion("questionID")%>" value="2" />&nbsp;<%=rsQuestion("NoAnswer")%><br />
									<input type="radio" name="customer_question_answer<%=rsQuestion("questionID")%>" value="3"  checked="checked" />&nbsp;<%=rsQuestion("NAAnswer")%><br />
									<input type="radio" name="customer_question_answer<%=rsQuestion("questionID")%>" value="4"/>&nbsp;<%=rsQuestion("NotInspAnswer")%><br />
									
									<a href=javascript:displayCommentPicker('customer_question_comment<%=rsQuestion("questionID")%>')><strong>select comment</strong></a><br />
									<input type="text" name="customer_question_comment<%=rsQuestion("questionID")%>" size="70" />
									<br /><br />
								</td>
							</tr>
					
					<%rsQuestion.movenext
					i=i+1
					if blnChange = true then
						blnChange = false
					else
						blnChange = true
					end if
					loop%>
					
					
				</table>
				
			</td>
		</tr>
		<!--<tr>
			<td></td>
			<td colspan="7">&nbsp;&nbsp;
				<input type="hidden" id="idCust" value="<%=i%>">
				<div id="divTxtCust"></div>
			</td>
		</tr>	
		<tr><td colspan="7" height=5></td></tr>
		<tr>
			<td></td>
			<td colspan="7">&nbsp;&nbsp;
				
				<a href="#" onClick="addFormFieldCust(); return false;">+ add new entry</a>
			</td>
		</tr>-->		
	</table>
	<%rsCategories.movenext
	loop%>
	
	
	</div>
	</div>
<%end if%>
	
<br />
Additional items of concern:<br />
<strong>NOTE:</strong> To insert single carraige return, hold the "shift" key while pressing enter.
<script language="JavaScript" type="text/javascript" src="cbrte/html2xhtml.js"></script>
<script language="JavaScript" type="text/javascript" src="cbrte/richtext_compressed.js"></script>
<script language="JavaScript" type="text/javascript">
<!--
function submitRTEForm() {
	updateRTEs();
	return true;
}
initRTE("cbrte/images/", "cbrte/", "", true);
//-->
</script>
<script language="JavaScript" type="text/javascript">
<!--
var rte1 = new richTextEditor('rte1');
//rte1.html = '<%'=sContent%>';
rte1.toggleSrc = false;
rte1.toolbar1 = false;
rte1.build();
//-->
</script>

