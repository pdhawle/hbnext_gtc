<html>
<head>
<title>On-Demand NOI</title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
</head>
<body>
<table bgcolor="#FFFFFF" class="borderTable" width="400" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td bgcolor="#FA702B"><img src="images/pix.gif" width="25" height="1"></td>
					<td bgcolor="#FA702B" valign="top">
						<%if Session("appLogo") <> "" then%>
							<img src="<%=Session("appLogo")%>" border="0"/>
						<%else%>
							<a href="http://next-sequence.com"><img src="images/logo.gif" width="110" height="100" border="0"/></a>
						<%end if%>
						<img src="images/sequence.gif" width="171" height="100" /><img src="images/pix.gif" width="151" height="1">
					</td>
				</tr>
				<tr><td colspan="8"><img src="images/pix.gif" width="1" height="10"></td></tr>
			</table>
		</td>
	</tr>
	<tr>
		<td><img src="images/pix.gif" width="20" height="1"></td>
		<td>
			<table width="800" border="0" cellpadding="0" cellspacing="0">
				<tr align="center">
					<td colspan="3">
						<span class="Header">Instructions</span><br />
						<strong>Notice of Termination (NOT) For Storm Water Discharges<br />
						Associated With Construction Activity<br /><br>
						NPDES General Permit</strong><br />
					</td>
				</tr>
				<tr><td><img src="images/pix.gif" width="1" height="30" /></td></tr>
				<tr>
					<td><img src="images/pix.gif" width="10" height="1" /></td>
					<td>
						W<strong>ho must file a Notice of Termination (NOT) Form</strong><br /><br />
						
						This Notice of Termination must be typed. Any NOT that contains illegible information will not be accepted. All
						information on this NOT must be submitted to be a valid Notice. Any information requested on the NOT that is not
						applicable to the owner or operator or the site must be marked �N/A�.<br /><br />
						
						When the facility/site has been finally stabilized and all storm water discharges from construction activities
						authorized by the NPDES General Permit number indicated in Section I of this form have ceased or when the
						Owner/Operator of the site changes, the Owner/Operator of the facility/site must submit a Notice of Termination
						(NOT). Final Stabilization means that all soil disturbing activities at the site have been completed, and that for
						unpaved areas and areas not covered by permanent structures, 100% of the soil surface is uniformly covered in
						permanent vegetation with a density of 70% or greater, or equivalent permanent stabilization measures (such as
						the use of rip rap, gabions, permanent mulches or geotextiles) have been used. Permanent vegetation shall
						consist of: planted trees, shrubs, perennial vines; a crop of perennial vegetation appropriate for the time of year
						and region; or a crop of annual vegetation and a seeding of target crop perennials appropriate for the region. Final
						stabilization applies to each phase of construction.<br /><br />
						
						<strong>Where to file NOT Forms</strong> - The NOT and attachments must be sent to the Regional Office or District Office
						shown on the next page. Please submit only the first two pages of this document plus your attachments, if necessary.<br /><br />
						
						<strong>Section I. Permit Type</strong> - Indicate the NPDES General Permit number for which this form is being submitted.<br /><br />
						
						<strong>Section II. Site / Permittee Information</strong><br /><br />
						
						Enter the information required. The site/project name is the physical location of the construction activity. Should
						the site lack a street address, sufficiently describe the facility location so that it can be found by district personnel. <strong>Should
						additional space be needed, attach the description to the notice.</strong><br><br>
						
						The facility contact is the person who the Owner and Operator has assigned the responsibility for the daily on-site
						operational control. Please do not leave any blanks in this section.<br><br>
						
						<strong>Section III. Site Activity Information</strong><br><br>
						
						Indicate by marking the appropriate block whether this NOT is submitted due to completion of the construction
						activity or a change in the Owner and Operator of the construction activity.<br><br>
						
						Mark the appropriate box to indicate the class of construction activity that was conducted at the site. For
						residential sites, also mark the block as either a subdivision development or an individual lot.<br><br>
						
						<strong>Section IV. Certifications</strong><br><br>
						
						All applicants must sign this Notice. Permittees shall initial next to the applicable certification statements on the
						line provided. Federal and State statutes provide specific requirements as to whom is authorized to sign Notice of
						Terminations. Signing of a Notice of Termination by others is not a valid submittal. Please be aware Federal and
			
						State statues provide severe penalties for submitting false information on this application form. Federal and State
						regulations require this application to be signed as follows:<br><br>
						
						- For a corporation: by a responsible corporate officer;<br />
						- For a partnership or sole proprietorship: by a general partner or the proprietor; or<br />
						- For a municipality, state, Federal or other public facility: by either a principal executive officer or ranking elected official.<br /><br />
						
					</td>
					<td><img src="images/pix.gif" width="10" height="1" /></td>
				</tr>
				<tr><td><img src="images/pix.gif" width="1" height="30" /></td></tr>
				<tr align="center">
					<td colspan="3">
						<strong>GEORGIA EPD DISTRICT OFFICES</strong>
					</td>
				</tr>
				<tr><td><img src="images/pix.gif" width="1" height="30" /></td></tr>
				<tr>
					<td><img src="images/pix.gif" width="10" height="1" /></td>
					<td>
						All Notice of Intents, Notice of Terminations, Erosion, Sedimentation and Pollution Control Plans, Comprehensive
						Monitoring Programs, certifications, reports, and any other information shall be sent to the following District offices of
						EPD:<br /><br />
						
						<strong>A. For facilities/sites located in the following counties:</strong> Bibb, Bleckley, Chattahoochee, Crawford, Dooly, Harris,
						Houston, Jones, Lamar, Macon, Marion, Meriwether, Monroe, Muscogee, Peach, Pike, Pulaski, Schley, Talbot, Taylor,
						Troup, Twiggs, Upson<br /><br />
						<table>
							<tr>
								<td valign="top">Information shall be submitted to:</td>
								<td><img src="images/pix.gif" width="10" height="1" /></td>
								<td>
									West Central District Office<br />
									Georgia Environmental Protection Division<br />
									2640 Shurling Drive<br />
									Macon, GA 31211-3576<br />
									(478) 751-6612<br />
								</td>
							</tr>
						</table><br />
						
						<strong>B. For facilities/sites located in the following counties:</strong> Burke, Columbia, Emanuel, Glascock, Jefferson, Jenkins,
						Johnson, Laurens, McDuffie, Montgomery, Richmond, Screven, Treutlen, Warren, Washington, Wheeler, Wilkinson<br /><br />
						<table>
							<tr>
								<td valign="top">Information shall be submitted to:</td>
								<td><img src="images/pix.gif" width="10" height="1" /></td>
								<td>
									East Central District Office<br />
									Georgia Environmental Protection Division<br />
									1885-A Tobacco Road<br />
									Augusta, GA 30906-8825<br />
									(706) 792-7744<br />
								</td>
							</tr>
						</table><br />
						
						<strong>C. For facilities/sites located in the following counties:</strong> Baldwin, Banks, Barrow, Butts, Clarke, Elbert, Franklin,
						Greene, Hall, Hancock, Hart, Jackson, Jasper, Lincoln, Madison, Morgan, Newton, Oconee, Oglethorpe, Putnam,
						Stephens, Taliaferro, Walton, Wilkes<br /><br />
						<table>
							<tr>
								<td valign="top">Information shall be submitted to:</td>
								<td><img src="images/pix.gif" width="10" height="1" /></td>
								<td>
									Northeast District Office<br />
									Georgia Environmental Protection Division<br />
									745 Gaines School Road<br />
									Athens, GA 30605-3129<br />
									(706) 369-6376<br />
								</td>
							</tr>
						</table><br />
						
						<strong>D. For facilities/sites located in the following counties:</strong> Clayton, Coweta, DeKalb, Fayette, Gwinnett, Heard,
						Henry, Rockdale, Spalding<br /><br />
						<table>
							<tr>
								<td valign="top">Information shall be submitted to:</td>
								<td><img src="images/pix.gif" width="10" height="1" /></td>
								<td>
									Mountain District - Atlanta Satellite<br />
									Georgia Environmental Protection Division<br />
									4244 International Parkway, Suite 114<br />
									Atlanta, GA 30354-3906<br />
									(404) 362-2671<br />
								</td>
							</tr>
						</table><br />
						
						<strong>E. For facilities/sites located in the following counties:</strong> Bartow, Carroll, Catoosa, Chattooga, Cherokee, Cobb,
						Dade, Dawson, Douglas, Fannin, Floyd, Forsyth, Fulton, Gilmer, Gordon, Habersham, Haralson, Lumpkin, Murray,
						Paulding, Pickens, Polk, Rabun, Towns, Union, Walker, White, Whitfield<br /><br />
						<table>
							<tr>
								<td valign="top">Information shall be submitted to:</td>
								<td><img src="images/pix.gif" width="10" height="1" /></td>
								<td>
									Mountain District - Cartersville Office<br />
									Georgia Environmental Protection Division<br />
									P.O. Box 3250<br />
									Cartersville, GA 30120-1705<br />
									(770) 387-4900<br />
								</td>
							</tr>
						</table><br />
						
						<strong>F. For facilities/sites located in the following counties:</strong> Appling, Atkinson, Bacon, Brantley, Bryan, Bulloch,
						Camden, Candler, Charlton, Chatham, Clinch, Coffee, Effingham, Evans, Glynn, Jeff Davis, Liberty, Long, McIntosh,
						Pierce, Tattnall, Toombs, Ware, Wayne<br /><br />
						<table>
							<tr>
								<td valign="top">Information shall be submitted to:</td>
								<td><img src="images/pix.gif" width="10" height="1" /></td>
								<td>
									Coastal District- Brunswick Office<br />
									Georgia Environmental Protection Division<br />
									One Conservation Way<br />
									Brunswick, GA 31520-8687<br />
									(912) 264-7284<br />
								</td>
							</tr>
						</table><br />
						
						<strong>G. For facilities/sites located in the following counties:</strong> Baker, Ben Hill, Berrien, Brooks, Calhoun, Clay, Colquitt,
						Cook, Crisp, Decatur, Dodge, Dougherty, Early, Echols, Grady, Irwin, Lanier, Lee, Lowndes, Miller, Mitchell, Quitman,
						Randolph, Seminole, Stewart, Sumter, Telfair, Terrell, Thomas, Tift, Turner, Webster, Wilcox, Worth<br /><br />
						<table>
							<tr>
								<td valign="top">Information shall be submitted to:</td>
								<td><img src="images/pix.gif" width="10" height="1" /></td>
								<td>
									Southwest District Office<br />
									Georgia Environmental Protection Division<br />
									2024 Newton Road<br />
									Albany, GA 31701-3576<br />
									(912) 430-4144<br />
								</td>
							</tr>
						</table><br />
					</td>
					<td><img src="images/pix.gif" width="10" height="1" /></td>
				</tr>
			</table>

		</td>
		<td><img src="images/pix.gif" width="20" height="1"></td>
	</tr>
</table>
</body>
</html>
