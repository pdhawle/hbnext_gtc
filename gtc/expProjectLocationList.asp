<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
inspector = request("primaryInspector")
clientID=request("clientID")

Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If


On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")
	
If 	inspector = "" then

	
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetProjectsActive"'sp
	   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
	   .CommandType = adCmdStoredProc   
	End With
				
	Set rs = oCmd.Execute
	Set oCmd = nothing
else

	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetProjectsActiveInspector"
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, inspector)
	   .CommandType = adCmdStoredProc   
	End With
				
	Set rs = oCmd.Execute
	Set oCmd = nothing
end if


'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetNextInspectors"
   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
   .CommandType = adCmdStoredProc   
End With
			
Set rsInspectors = oCmd.Execute
Set oCmd = nothing


Response.ContentType = "application/vnd.ms-excel"
Response.AddHeader "Content-Disposition", "attachment; filename=projectList" & sState & ".xls" 
if rs.eof <> true then%>
		<table border=1>
			<tr>
				<td><strong>Customer</strong></td>
				<td><strong>Project Name</strong></td>
				<td><strong>Primary Inspector</strong></td>
				<td><strong>City</strong></td>
				<td><strong>State</strong></td>
				<td><strong>County</strong></td>
			</tr>
	<%iBillRate = 0
	idRate = 0
	iwprRate = 0
	do until rs.eof%>
		<tr>
			<td><%=rs("customerName")%></td>
			<td><%=rs("projectName")%></td>
			<td><%=rs("firstName") & " " & rs("lastName")%></td>
			<td><%=rs("city")%></td>
			<td><%=rs("state")%></td>
			<td><%=rs("countyName")%></td>
		</tr>
	<%
	rs.movenext
	loop%>
	</table>
<%end if

rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing%>

