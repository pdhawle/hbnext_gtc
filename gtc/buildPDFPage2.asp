<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->
<!--#include file="letterArray.asp"-->

<%'need customerID, reportID

iDivisionID = request("divisionID")
reportID = request("reportID")

Session("LoggedIn") = "True"

Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 	
DataConn.Open Session("Connection"), Session("UserID")
'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetReportByID"
    .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, reportID) 'reportID
   .CommandType = adCmdStoredProc
   
End With			
Set rsReport = oCmd.Execute
Set oCmd = nothing

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCategories"'ByDivision
    .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, iDivisionID)
   .CommandType = adCmdStoredProc
   
End With
			
Set rsCategories = oCmd.Execute
Set oCmd = nothing
%>
<html>
<head>
<title>Storm Water Site Inspection Report</title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/reportswsir.css" type="text/css">
</head>
<body>

<table border="0" width=750 cellpadding=0 cellspacing=0>
	<tr>		
		<td valign="top" width="400">
			<span class="Header">Storm Water Site Inspection Report</span><br>
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td><strong>Report Date:</strong>&nbsp;</td>
					<td><u><%=rsReport("inspectionDate")%></u></td>
				</tr>
				<tr>
					<td><strong>Customer:</strong>&nbsp;</td>
					<td><u><%=rsReport("customerName")%></u></td>
				</tr>
				<tr>
					<td><strong>Division:</strong>&nbsp;</td>
					<td><u><%=rsReport("division")%></u></td>
				</tr>
				<tr>
					<td><strong>Site Name:</strong>&nbsp;</td>
					<td><u><%=rsReport("projectName")%></u></td>
				</tr>
			</table>
		</td>
		<td valign=top align="right">
			<%if rsReport("logo") = "" then%>
				<img src="images/logo.jpg">
			<%else%>
				<img src="<%=rsReport("logo")%>">
			<%end if%>
		</td>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=20></td></tr>
	<tr>
		<td colspan="2">
			<div align="center"><span class="Header">Corrective Action Log and Punch List - Report #<%=reportID%></span><br><br>
			<%
			'get the responsive action items from this report
			'
			Set oCmd = Server.CreateObject("ADODB.Command")

			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spGetResponsiveActionItems"
			   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID) 'reportID
			   .CommandType = adCmdStoredProc
			   
			End With
						
			Set rsAction = oCmd.Execute
			Set oCmd = nothing
			%>
			<table class="borderTable2" width="750" cellpadding="0" cellspacing="0">
				<tr><td colspan="6" align="center" class="tCol"><strong>For action items found during this inspection</strong></td></tr>
				<tr>
					<td align="center" class="tCol"><strong>Ref#</strong><br><img src=images/pix.gif width=60 height=1></td>
					<td align="center" class="tCol"><strong>Deficiency(Action Item)</strong></td>
					<td align="center" class="tCol"><strong>Location</strong></td>
					<td align="center" class="tCol"><strong>Addressed By</strong></td>
					<td align="center" class="tCol"><strong>Date</strong></td>					
					<td align="center" class="tCol" width="150"><strong>Action Taken</strong></td>
				</tr>				
				<%
				If rsAction.eof then
					response.Write "<tr><td class=tCol colspan=6 align=center>There are no corrective action items for this report.</td></tr>"
				else
					do until rsAction.eof%>
					<tr>
						<td align="center" valign="top" class="tCol"><%=reportID & "-" & rsAction("referenceNumber")%>&nbsp;</td>
						<td align="left" valign="top" class="tCol"><%=rsAction("actionNeeded")%>&nbsp;</td>
						<td align="center" valign="top" class="tCol"><%=rsAction("location")%>&nbsp;</td>
						<td align="center" valign="top" class="tCol"><%=rsAction("addressedBy")%>&nbsp;</td>
						<td align="left" valign="top" class="tCol" width="60"><%=rsAction("addressedByDate")%>&nbsp;</td>
						<td align="center" valign="top" class="tCol">&nbsp;</td>
					</tr>
					<%rsAction.movenext
					loop
				end if%>
			</table>
			
		</td>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=20></td></tr>
	<tr>
		<td colspan="2">
			<%
			'get the responsive action items from this report
			Set oCmd = Server.CreateObject("ADODB.Command")
	
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spGetOpenItemsPrevReports"
			   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, rsReport("projectID")) 'projectID
			   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID) 'reportID
			   .CommandType = adCmdStoredProc				   
			End With
							
			Set rsPrev = oCmd.Execute
			Set oCmd = nothing
			%>
			<table class="borderTable2" width="750" cellpadding="0" cellspacing="0">
				<tr><td colspan="5" align="center" class="tCol"><strong>Uncompleted Open Items From Prior Inspections</strong></td></tr>
				<tr>
					<td align="center" class="tCol" width="60"><strong>Ref#</strong></td>
					<td align="center" class="tCol"><strong>Deficiency(Action Item)</strong></td>
					<td align="center" class="tCol"><strong>Location</strong></td>
					<td align="center" class="tCol"><strong>Inspection Date</strong></td>					
					<td align="center" class="tCol" width="150"><strong>Explanation</strong></td>
				</tr>				
				<%
				If rsPrev.eof then
					response.Write "<tr><td colspan=5 align=center class=tCol>There are no open action items for this report.</td></tr>"
				else
					do until rsPrev.eof%>
					<tr>
				
						<td align="center" valign="top" class="tCol"><%=rsPrev("reportID") & "-" & rsPrev("referenceNumber")%>&nbsp;</td>
						<td valign="top" class="tCol"><%=rsPrev("actionNeeded")%>&nbsp;</td>
						<td valign="top" class="tCol"><%=rsPrev("location")%>&nbsp;</td>
						<td align="center" valign="top" class="tCol"><%=rsPrev("inspectionDate")%>&nbsp;</td>
						<td align="center" valign="top" class="tCol"><%=rsPrev("comments")%>&nbsp;</td>
					</tr>
					<%rsPrev.movenext
					loop
				end if%>
			</table>
			
		</td>
	</tr>
</table>
</body>
</html>