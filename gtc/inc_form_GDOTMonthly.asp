<table cellpadding="0" cellspacing="0" border="0">
	<%'else display the questions
	If rsCategories.eof then%>
		<tr><td></td><td colspan="7">there are no questions to display. please select another customer and project from the dropdown at the top of the page or add questions for this customer via the administrator.</td></tr>
	<%else%>
		<tr>
		<td colspan="7"><br />
			<%
			on error resume next
			
			'get the latest EC report
			Set oCmd = Server.CreateObject("ADODB.Command")
					
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spGetLatestMonthDOTReport"
			   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iProjectID)
			   .CommandType = adCmdStoredProc
			   
			End With
				
			Set rsLast = oCmd.Execute
			Set oCmd = nothing
			
			If request("reportNumber") <> "" then
				iRepNum = request("reportNumber")
			else
				'if there is no report number in the previous report, get the total number and post here
				'get the report number from the previous report
				
				Set oCmd = Server.CreateObject("ADODB.Command")
			
				With oCmd
				   .ActiveConnection = DataConn
				   .CommandText = "spGetLatestReportNumber"
				   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, rsLast("reportID"))
				   '.parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, iProjectID)
				   .CommandType = adCmdStoredProc
				   
				End With
					
				Set rsReportNumber = oCmd.Execute
				Set oCmd = nothing
				
				If Err Then
		%>
			3<!--#include file="includes/FatalError.inc"-->
		<%
			End If
				
				if isnull(rsReportNumber("reportNumber")) then
					'get the count of the report previously entered
					Set oCmd = Server.CreateObject("ADODB.Command")
		
					With oCmd
					   .ActiveConnection = DataConn
					   .CommandText = "spGetDOTMonthlyCount"
						.parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, iProjectID) 'reportID
					   .CommandType = adCmdStoredProc
					   
					End With							
					Set rsCount = oCmd.Execute
					Set oCmd = nothing
					
					iRepNum = rsCount("reportNumber")
				else
					'set the report number to be the last in the DB
					iRepNum = rsReportNumber("reportNumber")
				end if
			end if
			
			
			
			if iRepNum = 0 then
				iRepNum = 1
			end if
			%>
			&nbsp;&nbsp;<strong>Report Number:</strong>&nbsp;<input type="text" name="reportNumber" size="3" value="<%=iRepNum%>"/>&nbsp;this is the number that will be displayed at the top of the report
		</td>
	</tr>
	
		<tr><td></td><td colspan="7"><strong>Note:</strong> In order to speed up inspections, the "No action recommended" item is checked by default. If action is needed, select "Add remarks" and add the remarks in the textbox provided. If you need more textboxes, click the "Add corrective action needed" link.<br /><br /></td></tr>
		<%blnChange = true
		
		i = 1
		Do until rsCategories.eof
			'response.Write iDivisionID
			%>
			<tr>
				<td><img src="images/pix.gif" width="5" height="0"></td>
				<td>
					<%
						
						Set oCmd = Server.CreateObject("ADODB.Command")

						With oCmd
						   .ActiveConnection = DataConn
						   .CommandText = sType
						   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, rsCategories("categoryID"))
						   .CommandType = adCmdStoredProc
						   
						End With
							
																
						Set rsQuestion = oCmd.Execute
						Set oCmd = nothing
						%>
						
						<%if not rsQuestion.eof then%>
						<u><strong><%=rsCategories("category")%></strong></u><br><br>
						<%end if%>
						<table width="800" cellpadding="0" cellspacing="0" border="0">
							<%blnChange = true
							do until rsQuestion.eof
								If blnChange = true then%>
								<tr class="rowColor">
								<%else%>
								<tr>
								<%end if%>
									<td valign="top">
										<br />&nbsp;<%=i%>. 
									</td>
									<td>
										<br /><%=rsQuestion("question")%><br />
										
										<%iQuesType = rsQuestion("questionType")
										select case iQuesType
										
											Case 1 'No Action Recommended/Add Corrective Action/NA%> 
										  
												<input type="radio" id="answer_&<%=rsQuestion("questionID")%>1" name="answer_&<%=rsQuestion("questionID")%>" value="No Action Recommended" checked="checked" /> No Action Recommended<br />
												<input type="radio" id="answer_&<%=rsQuestion("questionID")%>2" name="answer_&<%=rsQuestion("questionID")%>" value="See Corrective Action Log" onclick="addEvent('<%=i%>','<%=rsQuestion("questionID")%>');"/> Add Remarks<br />
												<input type="radio" id="answer_&<%=rsQuestion("questionID")%>3" name="answer_&<%=rsQuestion("questionID")%>" value="N/A" /> N/A<br /><br />
												
												<input type="hidden" value="0" id="theValue<%=i%>" />
												<a href="javascript:;" onclick="addEvent('<%=i%>','<%=rsQuestion("questionID")%>');">Add Corrective Action</a><br /><br />
												<div id="myDiv<%=i%>"> </div><br /><br />
												
											<%case 2 'Yes/No/NA %>
												<input type="radio" id="answer_&<%=rsQuestion("questionID")%>1" name="answer_&<%=rsQuestion("questionID")%>" value="Yes" checked="checked" /> Yes<br />
												<input type="radio" id="answer_&<%=rsQuestion("questionID")%>2" name="answer_&<%=rsQuestion("questionID")%>" value="No"/> No<br />
												<input type="radio" id="answer_&<%=rsQuestion("questionID")%>3" name="answer_&<%=rsQuestion("questionID")%>" value="N/A" /> N/A<br /><br />
												
												<input type="hidden" value="0" id="theValue<%=i%>" />
												<a href="javascript:;" onclick="addEvent('<%=i%>','<%=rsQuestion("questionID")%>');">Add Corrective Action</a><br /><br />
												<div id="myDiv<%=i%>"> </div><br />
												
										<%end select%>
									</td>
									<td valign="top" align="right"><img src="images/pix.gif" width="100" height="1"><br />
										<%if iProjectID <> "" then%>
											<a href="#" onClick="MM_openBrWindow('openItems.asp?questionID=<%=rsQuestion("questionID")%>&projectID=<%=iProjectID%>','popPrint','scrollbars=yes,resizable=yes,width=1000,height=600')">view open items</a>&nbsp;&nbsp;<br /><br />
											<!--<a href="openItems.asp?questionID=<%'=rsQuestion("questionID")%>&projectID=<%'=iProjectID%>">view open items</a>&nbsp;&nbsp;<br /><br />-->
											
											<%
												'get the percent complete for each question
												Set oCmd = Server.CreateObject("ADODB.Command")

												With oCmd
												   .ActiveConnection = DataConn
												   .CommandText = "spGetOpenAIByQuestion"
												   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, iProjectID)
												   .parameters.Append .CreateParameter("@questionID", adInteger, adParamInput, 8, rsQuestion("questionID"))
												   .CommandType = adCmdStoredProc
												   
												End With
													
															
												Set rsQuestionComplete = oCmd.Execute
												Set oCmd = nothing
												
												iOpen = rsQuestionComplete("openAI")
												if isNull(iOpen) then
													iOpen = 0
												end if
												iClosed = rsQuestionComplete("closedAI")
												if isNull(iClosed) then
													iClosed = 0
												end if
												
												iTotItem = iOpen + iClosed
												
												If iTotItem <> 0 then
													iQuestionComplete = (iClosed/iTotItem) * 100
												else
													iQuestionComplete = 100
												end if
											
											%>
											
											open items: <%=iOpen%>&nbsp;&nbsp;<br />
											corrected items: <%=iClosed%>&nbsp;&nbsp;<br />
											
											<b>corrected: <%=round(iQuestionComplete)%>%</b>&nbsp;&nbsp;
										<%end if%>
									</td>
								</tr>
						
						<%rsQuestion.movenext
						i=i+1
						if blnChange = true then
							blnChange = false
						else
							blnChange = true
						end if
						loop%>
						

					</table>
					
				</td>
			</tr>
		<%rsCategories.movenext
		loop%>
		</table>
	<%end if%>