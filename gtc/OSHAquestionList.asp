<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
Dim rs, oCmd, DataConn

divisionID = Request("id")
customerID = Request("customerID")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection")	
DataConn.Open Session("Connection"), Session("UserID")

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomer"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, customerID)
   .CommandType = adCmdStoredProc
   
End With
			
Set rsCust = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetDivision"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, divisionID)
   .CommandType = adCmdStoredProc
   
End With
			
Set rsDiv = oCmd.Execute
Set oCmd = nothing

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<link rel="stylesheet" type="text/css" href="menu/popupmenu.css" />
<script type="text/javascript" src="menu/jquery.min.js"></script>
<script type="text/javascript" src="menu/popupmenu.js"></script>
<script language="JavaScript">
<!--
function confirmDelete(delUrl) {
 if (confirm("Are you sure you wish to delete this question?")) {
    document.location = delUrl;
  }


}

function confirmRestore(sUrl) {
 if (confirm("Are you sure you wish to restore the default OSHA questions? This cannot be undone.")) {
    document.location = sUrl;
  }


}
//-->
</script>
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Question List</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<%If not rs.eof then%>
							<a href="form.asp?formType=addOSHAQuestion&customerID=<%=customerID%>&divisionID=<%=divisionID%>" class="footerLink">add question</a>&nbsp;&nbsp;
						<%end if%>
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="customerList.asp" class="footerLink">customer list</a>&nbsp;&nbsp;	
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="divisionList.asp?ID=<%=customerID%>" class="footerLink">division list</a>	&nbsp;&nbsp;
						<span class="footerLink">|</span>&nbsp;&nbsp;<a style="cursor:hand;" onClick="return confirmRestore('process.asp?processType=restoreOSHADefaults&customerID=<%=customerID%>&divisionID=<%=divisionID%>&clientID=<%=session("clientID")%>')" class="footerLink">restore defaults</a>					
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td colspan="9">
												<strong>Division: </strong><%=rsDiv("division")%><br><br>
												
												<!--#include file="custDropdown.asp"-->
											</td>
										</tr>
										<%
										'get the OSHA questions
										Set oCmd = Server.CreateObject("ADODB.Command")
											
										With oCmd
										   .ActiveConnection = DataConn
										   .CommandText = "spGetGeneralOSHAQuestion"
											.parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, divisionID)
										   .CommandType = adCmdStoredProc
										   
										End With
													
										Set rs = oCmd.Execute
										Set oCmd = nothing
										%>
										<!--general reminders-->
										<tr><td colspan="5"><strong>General Reminders</strong></td></tr>
										<tr><td height="5"></td></tr>
										<tr bgcolor="#666666">
											<td></td>
											<td><span class="searchText">Question</span></td>
											<td align="center"><span class="searchText">General Reminder</span></td>
											<!--<td align="center"><span class="searchText">Sub-Contractor Question</span></td>-->
											<td align="right"><span class="searchText">Edit/Delete</span></td>
										</tr>
										<tr><td colspan="8"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<%If rs.eof then%>
											<tr><td></td><td colspan="7">there are no questions to display.</td></tr>
										<%else	
											i = 1											
											blnChange = true
											do until rs.eof
												If blnChange = true then%>
													<tr class="rowColor">
												<%else%>
													<tr>
												<%end if%>
													<td valign="top">&nbsp;<%=i%>.&nbsp;&nbsp;</td>
													<td width="400"><%=rs("question")%></td>
													<td align="center">
														<%If rs("GeneralReminder") = "True" Then%>
															<img src="images/check.gif" width="15" height="15">
														<%end if%>	
													</td>
													<!--<td align="center">
														<%'If rs("subQuestion") = "True" Then%>
															<img src="images/check.gif" width="15" height="15">
														<%'end if%>	
													</td>-->
													<td align="right"><a href="form.asp?id=<%=rs("questionID")%>&formType=editOSHAQuestion&customerID=<%=customerID%>&divisionID=<%=divisionID%>"><img src="images/edit.gif" width="19" height="19" alt="edit" border="0"></a>&nbsp;&nbsp;<input type="image" src="images/remove.gif" width="11" height="13" alt="delete" border="0" onClick="return confirmDelete('process.asp?id=<%=rs("questionID")%>&processType=deleteOSHAQuestion&customerID=<%=customerID%>&divisionID=<%=divisionID%>')">&nbsp;</td>
												</tr>
											<%
											i = i + 1
											if blnChange = true then
												blnChange = false
											else
												blnChange = true
											end if
											rs.movenext
											loop%>
										<%end if%>
										
										
										<tr><td height="15"></td></tr>
										
										<%
										'get the OSHA questions
										Set oCmd = Server.CreateObject("ADODB.Command")
											
										With oCmd
										   .ActiveConnection = DataConn
										   .CommandText = "spGetOSHAQuestion"
											.parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, divisionID)
										   .CommandType = adCmdStoredProc
										   
										End With
													
										Set rs = oCmd.Execute
										Set oCmd = nothing
										%>
										<!--Customer Questions-->
										<tr><td colspan="5"><strong>Company/Client Questions</strong></td></tr>
										<tr><td height="5"></td></tr>
										<tr bgcolor="#666666">
											<td></td>
											<td><span class="searchText">Question</span></td>
											<td align="center"><span class="searchText">General Reminder</span></td>
											<!--<td align="center"><span class="searchText">Sub-Contractor Question</span></td>-->
											<td align="right"><span class="searchText">Edit/Delete</span></td>
										</tr>
										<tr><td colspan="8"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<%If rs.eof then%>
											<tr><td></td><td colspan="7">there are no questions to display.</td></tr>
										<%else	
											i = 1											
											blnChange = true
											do until rs.eof
												If blnChange = true then%>
													<tr class="rowColor">
												<%else%>
													<tr>
												<%end if%>
													<td valign="top">&nbsp;<%=i%>.&nbsp;&nbsp;</td>
													<td width="400"><%=rs("question")%></td>
													<td align="center">
														<%If rs("GeneralReminder") = "True" Then%>
															<img src="images/check.gif" width="15" height="15">
														<%end if%>	
													</td>
													<!--<td align="center">
														<%'If rs("subQuestion") = "True" Then%>
															<img src="images/check.gif" width="15" height="15">
														<%'end if%>	
													</td>-->
													<td align="right"><a href="form.asp?id=<%=rs("questionID")%>&formType=editOSHAQuestion&customerID=<%=customerID%>&divisionID=<%=divisionID%>"><img src="images/edit.gif" width="19" height="19" alt="edit" border="0"></a>&nbsp;&nbsp;<input type="image" src="images/remove.gif" width="11" height="13" alt="delete" border="0" onClick="return confirmDelete('process.asp?id=<%=rs("questionID")%>&processType=deleteOSHAQuestion&customerID=<%=customerID%>&divisionID=<%=divisionID%>')">&nbsp;</td>
												</tr>
											<%
											i = i + 1
											if blnChange = true then
												blnChange = false
											else
												blnChange = true
											end if
											rs.movenext
											loop%>
										<%end if%>
										
										
										
										<!--<tr><td height="15"></td></tr>-->
										
										<%
										'get the OSHA questions
							'			Set oCmd = Server.CreateObject("ADODB.Command")
											
							'			With oCmd
							'			   .ActiveConnection = DataConn
							'			   .CommandText = "spGetOSHASubQuestion"
							'				.parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, divisionID)
							'			   .CommandType = adCmdStoredProc
										   
							'			End With
							'						
							'			Set rs = oCmd.Execute
							'			Set oCmd = nothing
										%>
										<!--Sub Questions-->
										<!--<tr><td colspan="5"><strong>Sub-Contractor Questions</strong></td></tr>
										<tr><td height="5"></td></tr>
										<tr bgcolor="#666666">
											<td></td>
											<td><span class="searchText">Question</span></td>
											<td align="center"><span class="searchText">General Reminder</span></td>
											<td align="center"><span class="searchText">Sub-Contractor Question</span></td>
											<td align="right"><span class="searchText">Edit/Delete</span></td>
										</tr>
										<tr><td colspan="8"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<%'If rs.eof then%>
											<tr><td></td><td colspan="7">there are no questions to display.</td></tr>
										<%'else	
										'	i = 1											
										'	blnChange = true
										'	do until rs.eof
										'		If blnChange = true then%>
													<tr class="rowColor">
												<%'else%>
													<tr>
												<%'end if%>
													<td valign="top">&nbsp;<%'=i%>.&nbsp;&nbsp;</td>
													<td width="400"><%'=rs("question")%></td>
													<td align="center">
														<%'If rs("GeneralReminder") = "True" Then%>
															<img src="images/check.gif" width="15" height="15">
														<%'end if%>	
													</td>
													<td align="center">
														<%'If rs("subQuestion") = "True" Then%>
															<img src="images/check.gif" width="15" height="15">
														<%'end if%>	
													</td>
													<td align="right"><a href="form.asp?id=<%'=rs("questionID")%>&formType=editOSHAQuestion&customerID=<%'=customerID%>&divisionID=<%'=divisionID%>"><img src="images/edit.gif" width="19" height="19" alt="edit" border="0"></a>&nbsp;&nbsp;<input type="image" src="images/remove.gif" width="11" height="13" alt="delete" border="0" onClick="return confirmDelete('process.asp?id=<%'=rs("questionID")%>&processType=deleteOSHAQuestion&customerID=<%'=customerID%>&divisionID=<%'=divisionID%>')">&nbsp;</td>
												</tr>
											<%
										'	i = i + 1
										'	if blnChange = true then
										'		blnChange = false
										'	else
										'		blnChange = true
										'	end if
										'	rs.movenext
										'	loop%>
										<%'end if%>-->
									</table>
									
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>