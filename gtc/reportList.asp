<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%

sMan = request("man")
clientID = session("clientID")

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

reportID = request("findReport")






'if the user is assigned to a single project
'get the project, customer, and division and populate the dropdowns and list
If Session("singleProjectUser") = "True" then
	'get the assigned project
	'spGetSingleProjectInfo
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
	'may need to change later
	'may want to only display divisions
	'that sre assigned to the current user
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetSingleProjectInfo"
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, Session("ID"))
	   .CommandType = adCmdStoredProc   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
				
	Set rsSingle = oCmd.Execute
	Set oCmd = nothing
	
	iDivisionID = rsSingle("divisionID")
	'reportID = rsSingle("reportID")
	projectID = rsSingle("projectID")
	
else

	iDivisionID = request("division")
	projectID = request("project")

end if	

if iDivisionID = "" then
	iDivisionID = 1
end if

if reportID <> "" then
	projectID = ""
end if



ssort = request("sort")
if ssort = "" then
	columnName = "reportID"
else
	columnName = ssort
end if



If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
'may need to change later
'may want to only display divisions
'that sre assigned to the current user
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetAssignedDivisions"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, Session("ID"))
   .CommandType = adCmdStoredProc   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsDivision = oCmd.Execute
Set oCmd = nothing

	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

If iDivisionID <> "" then
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetAssignedProjectsByDivision"
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iDivisionID)
	   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, Session("ID"))
	   .CommandType = adCmdStoredProc   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
				
	Set rsProject = oCmd.Execute
	Set oCmd = nothing
end if
	

	
if reportID <> "" then
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetReportByID"
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, reportID)
	   .CommandType = adCmdStoredProc   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
				
	Set rsReportByID = oCmd.Execute
	Set oCmd = nothing
	
	Set oCmd = Server.CreateObject("ADODB.Command")
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetProject"
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8,rsReportByID("projectID"))
	   .CommandType = adCmdStoredProc   
	End With
				
	Set rsProjInfo = oCmd.Execute
	Set oCmd = nothing
	
end if


if projectID <> "" then
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetReportListByProjectSort"
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, projectID)
	   .parameters.Append .CreateParameter("@columnName", adVarchar, adParamInput, 50, columnName)
	   .CommandType = adCmdStoredProc   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
				
	Set rsReportByProject = oCmd.Execute
	Set oCmd = nothing
	
	'get the customer
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetCustomerByProject"
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, projectID)
	   .CommandType = adCmdStoredProc   
	End With
				
	Set rsCustomer = oCmd.Execute
	Set oCmd = nothing
	
	Set oCmd = Server.CreateObject("ADODB.Command")
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetProject"
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, projectID)
	   .CommandType = adCmdStoredProc   
	End With
				
	Set rsProjInfo = oCmd.Execute
	Set oCmd = nothing
	
end if
	

Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetClient"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, clientID)
   .CommandType = adCmdStoredProc   
End With
			
Set rsClient = oCmd.Execute
Set oCmd = nothing



%>

<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script type="text/javascript" src="includes/rounded-corners.js"></script>
<script type="text/javascript" src="includes/form-field-tooltip.js"></script>
<script type="text/javascript">
    var GB_ROOT_DIR = "<%=sPDFPath%>/greybox/";
</script>
<script type="text/javascript" src="greybox/AJS.js"></script>
<script type="text/javascript" src="greybox/AJS_fx.js"></script>
<script type="text/javascript" src="greybox/gb_scripts.js"></script>
<link href="greybox/gb_styles.css" rel="stylesheet" type="text/css" />
</head>
<script language="javascript">
<!--
function dept_onchange(reportList) {
   reportList.submit(); 
}

function confirmDelete(delUrl) {
 if (confirm("Are you sure you wish to delete this report?")) {
    document.location = delUrl;
  }


}

function confirmSign(sUrl) {
 if (confirm("Are you sure you wish to sign this report. The report will be signed by the person currently logged in.")) {
    document.location = sUrl;
  }


}
//-->
</script>
</head>
<body>
<form name="reportList" method="post" action="reportList.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sNPDESTitle%></span><span class="Header"> - Report List</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr class="colorBars">
											<td colspan="6" valign="middle">
												&nbsp;<span class="searchText">Find report by ID:</span>&nbsp;&nbsp;<input type="text" name="findReport" size="5" tooltipText="To search for a report by it's ID number, enter it here.">&nbsp;&nbsp;<input type="submit" name="search" value="Search" class="formButton">
											</td>
											<td align="right">
												<%if sMan = "" then%>
												<a href="form.asp?formType=addReport" class="footerLink">Add New Report</a>&nbsp;&nbsp;
												<span class="footerLink">|</span>&nbsp;&nbsp;<a href="chooseProj.asp?customer=<%=rsCustomer("customerID")%>&division=<%=iDivisionID%>" class="footerLink">Open Items Punch List</a>&nbsp;&nbsp;
												<span class="footerLink">|</span>&nbsp;&nbsp;<a href="docManager.asp?projectID=<%=projectID%>" class="footerLink">View Project Documents</a>&nbsp;&nbsp;
												<%end if%>
											</td>
										</tr>
										<tr>
											<td></td>
											<td colspan="7">
												<table>
													<tr><td colspan="2"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr><td colspan="2">Please select a division and a project to view reports.</td></tr>
													<tr><td colspan="2"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td valign="top" align="right">
															<strong>Division:</strong> 
														</td>
														<td>
															<select name="division" onChange="return dept_onchange(reportList)" tooltipText="Select the customer and division here.">
																<option value="">--Select a division--</option>
																<%do until rsDivision.eof
																	If trim(rsDivision("divisionID")) = trim(iDivisionID) then%>
																		<option selected="selected" value="<%=rsDivision("divisionID")%>"><%=rsDivision("division")%></option>
																	<%else%>
																		<option value="<%=rsDivision("divisionID")%>"><%=rsDivision("division")%></option>
																	<%end if
																rsDivision.movenext
																loop%>
															</select>
														</td>
													</tr>
													<tr>
														<td valign="top" align="right">
															<strong>Project:</strong> 
														</td>
														<td>
															<select name="project">
																<option value="">--Select a project--</option>
																<%If iDivisionID <> "" then
																	do until rsProject.eof
																		If trim(rsProject("projectID")) = trim(projectID) then%>
																			<option selected="selected" value="<%=rsProject("projectID")%>"><%=rsProject("projectName")%></option>
																		<%else%>
																			<option value="<%=rsProject("projectID")%>"><%=rsProject("projectName")%></option>
																		<%end if
																	rsProject.movenext
																	loop
																end if%>
															</select>
														</td>
													</tr>
													<tr><td colspan="2"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td colspan="2" align="right">
															<input type="submit" name="viewReports" value="View Reports" class="formButton">
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr><td colspan="7"><img src="images/pix.gif" width="1" height="20"></td></tr>
										<tr>
											<td colspan="7">
												<%if reportID <> "" then
													if rsProjInfo("projectID") <> "" then%>
														NOT Date: <b>
														<%if rsProjInfo("endDate") <> "" then
															response.Write rsProjInfo("endDate")
														else
															response.Write "N/A"
														end if%>
														</b>
													<%end if
												else
												
													if projectID <> "" then%>
														NOT Date: <b>
														<%if rsProjInfo("endDate") <> "" then
															response.Write rsProjInfo("endDate")
														else
															response.Write "N/A"
														end if%>
														</b>
													<%end if
												end if%>
												
												<br>
												<span style="color:#FF0000">&#9873;</span> denotes modification is needed
												<table width="100%" cellpadding="0" cellspacing="0">
													<tr bgcolor="#666666" height="35">
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td></td>
														<td><span class="searchText"><a href="reportList.asp?man=<%=sMan%>&findReport=<%=reportID%>&division=<%=iDivisionID%>&project=<%=projectID%>&sort=reportID" style="color:#721A32;text-decoration:underline;font-weight:bold;">Report ID</a></span></td>
														<td><span class="searchText"><a href="reportList.asp?man=<%=sMan%>&findReport=<%=reportID%>&division=<%=iDivisionID%>&project=<%=projectID%>&sort=inspectionDate" style="color:#721A32;text-decoration:underline;font-weight:bold;">Date</a></span></td>
														<td><span class="searchText">Division</span></td>
														<td><span class="searchText"><a href="reportList.asp?man=<%=sMan%>&findReport=<%=reportID%>&division=<%=iDivisionID%>&project=<%=projectID%>&sort=projectName" style="color:#721A32;text-decoration:underline;font-weight:bold;">Project</a></span></td>
														<td><span class="searchText"><a href="reportList.asp?man=<%=sMan%>&findReport=<%=reportID%>&division=<%=iDivisionID%>&project=<%=projectID%>&sort=reportType" style="color:#721A32;text-decoration:underline;font-weight:bold;">Report Type</a></span></td>
														<td><span class="searchText"><a href="reportList.asp?man=<%=sMan%>&findReport=<%=reportID%>&division=<%=iDivisionID%>&project=<%=projectID%>&sort=inspectionType" style="color:#721A32;text-decoration:underline;font-weight:bold;">Inspection Type</a></span></td>
														<td align="center"><span class="searchText">Action</span></td>
														<td><span class="searchText">&nbsp;<!--initial report here--></span></td>
													</tr>
													
													
													<%if reportID <> "" then
														If rsReportByID.eof then %>
															<tr><td></td><td colspan="7">there are no reports to display</td></tr>
														<%else
															Do until rsReportByID.eof%>
																<tr>
																	<td></td>
																	<td>
																		<%if rsReportByID("isFlagged") = True then%>
																			<span style="color:#FF0000">&#9873;</span>
																		<%end if%>
																	</td>
																	<td><%=rsReportByID("reportID")%></td>
																	<td><%=rsReportByID("inspectionDate")%></td>
																	<td><%=rsReportByID("division")%></td>
																	<td><%=rsReportByID("projectName")%></td>
																	<td><%=rsReportByID("reportType")%></td>
																	<td>
																		<%If not isnull(rsReportByID("inspectionType")) then
																			response.Write rsReportByID("inspectionType")
																		else
																			response.Write "N/A"
																		end if%>
																	</td>
																	<td align="center">
																		<%'if rsReportByID("reportTypeID") <> 10 then
																			'if rsReportByID("reportTypeID") <> 20 then
																			'	if rsReportByID("reportTypeID") <> 2 and rsReportByID("reportTypeID") <> 16 and rsReportByID("reportTypeID") <> 12 then
																			'		If trim(rsReportByID("userID")) = trim(session("ID")) then 'if the user created the report, he/she can edit it
																						'get the date difference between today and the report date
																						
																							select case rsReportByID("reportTypeID")
																								case 7
																									iNumDays = 3
																								case 14
																									iNumDays = 3
																								case 15
																									iNumDays = 3
																								case 26
																									iNumDays = 0
																								Case 3
																									iNumDays = 365 
																								case else
																									iNumDays = 45
																							end select
																							dteExpire = DateDiff("d",rsReportByID("dateAdded"),Date)
																							if dteExpire < iNumDays then 'if the report is more than 2 days old, don't let them edit%>
																								<a href="form.asp?formType=editReport&reportID=<%=rsReportByID("reportID")%>">edit</a> | 
																							<%end if%>
																						
																					<%'end if%>
																				<%'else%>
																					<!--<a href="form.asp?formType=editReport&reportID=<%'=rsReportByID("reportID")%>">edit</a> | -->
																				<%'end if%>
																			<%'end if%>
																		<%'end if%>
																		<a href="downloads/swsir_<%=rsReportByID("reportID")%>.pdf" target="_blank">view</a> | 
																		<a href="form.asp?formType=sendEmail&reportID=<%=rsReportByID("reportID")%>&projName=<%=rsReportByID("projectName")%>&repType=<%=rsReportByID("reportType")%>&custName=<%=rsReportByID("customerName")%>&projectID=<%=rsReportByID("projectID")%>&divisionID=<%=rsReportByID("divisionID")%>">email</a>
																		<%
																	'	If rsClient("attachPhotos") = "True" then
																			'check to see if there are open items for this report
																	'		Set oCmd = Server.CreateObject("ADODB.Command")
																	'		With oCmd
																	'		   .ActiveConnection = DataConn
																	'		   .CommandText = "spGetNumAI"
																	'		   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, rsReportByID("reportID"))
																	'		   .CommandType = adCmdStoredProc   
																	'		End With
																	'					
																	''		Set rsAI = oCmd.Execute
																		'	Set oCmd = nothing
																			
																	'		if rsAI("NumAI") > 0 then
																			%>
																			 | <a href="uploadImage.asp?reportID=<%=rsReportByID("reportID")%>&divisionID=<%=rsReportByID("divisionID")%>"  title="" rel="gb_page_fs[]">view upload images</a>
																			<%'end if
																	'	end if%>
																		<%if dteExpire < 31 then 'if the report is more than 30 days old, don't let them delete%>																							
																		 | <a style="cursor:hand" onClick="return confirmDelete('process.asp?id=<%=rsReportByID("reportID")%>&divisionID=<%=rsReportByID("divisionID")%>&projectID=<%=rsReportByID("projectID")%>&reportType=<%=rsReportByID("reportTypeID")%>&processType=deleteReport')">delete</a>
																		<%end if%>
																		<%if rsReportByID("reportTypeID") = 25 then%>
																		 | <a style="cursor:pointer;" onClick="return confirmSign('process.asp?processType=signReport&reportID=<%=rsReportByID("reportID")%>&divisionID=<%=rsReportByID("divisionID")%>&reportType=<%=rsReportByID("reportTypeID")%>&userID=<%=session("ID")%>')">sign</a>
																		<%end if%>
																		
																		
																		<%if Session("Admin") = "True" then%>
																		<%if rsReportByID("isFlagged") = True then%>
																			 | <a href="process.asp?processType=flagReport&fType=unFlag&reportID=<%=rsReportByID("reportID")%>&userID=<%=session("ID")%>">un-flag</a>
																		<%else%>
																			 | <a href="process.asp?processType=flagReport&fType=Flag&reportID=<%=rsReportByID("reportID")%>&userID=<%=session("ID")%>">flag</a>
																		<%end if%>
																		<%end if%>
																		
																		<%=checkForAttachments(rsReportByID("reportID"),rsReportByID("projectID"))%>
																	</td>
																	<td>&nbsp;</td>
																</tr>
															<%rsReportByID.movenext
															loop
														end if
													end if%>
													
													<%if projectID <> "" then
														If rsReportByProject.eof then %>
															<tr><td></td><td colspan="7">there are no reports to display</td></tr>
														<%else
															blnChange = false
															Do until rsReportByProject.eof
																If blnChange = true then%>
																	<tr class="rowColor">
																<%else%>
																	<tr>
																<%end if%>
																	<td></td>
																	<td>
																		<%if rsReportByProject("isFlagged") = True then%>
																			<span style="color:#FF0000">&#9873;</span>
																		<%end if%>
																	</td>
																	<td><%=rsReportByProject("reportID")%></td>
																	<td><%=rsReportByProject("inspectionDate")%></td>
																	<td><%=rsReportByProject("division")%></td>
																	<td><%=rsReportByProject("projectName")%></td>
																	<td><%=rsReportByProject("reportType")%></td>
																	<td>
																		<%If not isnull(rsReportByProject("inspectionType")) then
																			response.Write rsReportByProject("inspectionType")
																		else
																			response.Write "N/A"
																		end if%>
																	</td>
																	<td align="center">
																		<%'if rsReportByProject("reportTypeID") <> 10 then
																			'if rsReportByProject("reportTypeID") <> 20 then
																				'if rsReportByProject("reportTypeID") <> 2 or rsReportByProject("reportTypeID") <> 16 or rsReportByProject("reportTypeID") <> 12 then
																				'	If trim(rsReportByProject("userID")) = trim(session("ID")) then 'if the user created the report, he/she can edit it
																						
																							'get the date difference between today and the report date
																							select case rsReportByProject("reportTypeID")
																								case 7
																									iNumDays = 3
																								case 14
																									iNumDays = 3
																								case 15
																									iNumDays = 3
																								case 26
																									iNumDays = 0
																								Case 3
																									iNumDays = 365
																								case else
																									iNumDays = 45
																							end select
																							dteExpire = DateDiff("d",rsReportByProject("dateAdded"),Date)
																							if dteExpire < iNumDays then 'if the report is more than 2 days old, don't let them edit%>
																								<a href="form.asp?formType=editReport&reportID=<%=rsReportByProject("reportID")%>">edit</a> | 
																							<%end if%>
																							
																						
																					<%'end if%>
																				<%'else%>
																					<!--<a href="form.asp?formType=editReport&reportID=<%'=rsReportByProject("reportID")%>">edit</a> | -->
																				<%'end if%>
																			<%'end if%>																			
																		<%'end if%>
																		<a href="downloads/swsir_<%=rsReportByProject("reportID")%>.pdf" target="_blank">view</a> | 
																		<!--<a href="buildPDF.asp?reportID=<%'=rsReportByProject("reportID")%>&divisionID=<%'=iDivisionID%>" target="_blank">html</a> | -->
																		<a href="form.asp?formType=sendEmail&reportID=<%=rsReportByProject("reportID")%>&projName=<%=rsReportByProject("projectName")%>&repType=<%=rsReportByProject("reportType")%>&custName=<%=rsReportByProject("customerName")%>&projectID=<%=rsReportByProject("projectID")%>&divisionID=<%=rsReportByProject("divisionID")%>">email</a>
																		<%
																	'	If rsClient("attachPhotos") = "True" then
																			'check to see if there are open items for this report
																	'		Set oCmd = Server.CreateObject("ADODB.Command")
																	'		With oCmd
																	'		   .ActiveConnection = DataConn
																	'		   .CommandText = "spGetNumAI"
																	'		   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, rsReportByProject("reportID"))
																	'		   .CommandType = adCmdStoredProc   
																	'		End With
																	'					
																	'		Set rsAI = oCmd.Execute
																	'		Set oCmd = nothing
																	'		
																	'		if rsAI("NumAI") > 0 then
																			%>
																			 | <a href="uploadImage.asp?reportID=<%=rsReportByProject("reportID")%>&divisionID=<%=rsReportByProject("divisionID")%>"  title="" rel="gb_page_fs[]">view upload images</a>
																			<%'end if
																	'	end if%>
																		<%if dteExpire < 31 then 'if the report is more than 30 days old, don't let them delete%>	
																		 | <a style="cursor:hand" onClick="return confirmDelete('process.asp?id=<%=rsReportByProject("reportID")%>&processType=deleteReport&divisionID=<%=rsReportByProject("divisionID")%>&projectID=<%=rsReportByProject("projectID")%>&reportType=<%=rsReportByProject("reportTypeID")%>')">delete</a>
																		<%end if%>
																		<%if rsReportByProject("reportTypeID") = 25 then%>
																		 | <a style="cursor:pointer;" onClick="return confirmSign('process.asp?processType=signReport&reportID=<%=rsReportByProject("reportID")%>&divisionID=<%=rsReportByProject("divisionID")%>&reportType=<%=rsReportByProject("reportTypeID")%>&userID=<%=session("ID")%>')">sign</a>
																		<%end if%>
																		
																		<%if Session("Admin") = "True" then%>
																		<%if rsReportByProject("isFlagged") = True then%>
																			 | <a href="process.asp?processType=flagReport&fType=unFlag&reportID=<%=rsReportByProject("reportID")%>&divisionID=<%=rsReportByProject("divisionID")%>&projectID=<%=rsReportByProject("projectID")%>&userID=<%=session("ID")%>">un-flag</a>
																		<%else%>
																			 | <a href="process.asp?processType=flagReport&fType=Flag&reportID=<%=rsReportByProject("reportID")%>&divisionID=<%=rsReportByProject("divisionID")%>&projectID=<%=rsReportByProject("projectID")%>&userID=<%=session("ID")%>">flag</a>
																		<%end if%>
																		<%end if%>
																		
																		<%=checkForAttachments(rsReportByProject("reportID"),rsReportByProject("projectID"))%>
																	</td>
																	<td></td>
																</tr>
															<%rsReportByProject.movenext
															if blnChange = true then
																blnChange = false
															else
																blnChange = true
															end if
															loop
														end if
													end if%>
													
													
												</table>
											</td>
										</tr>
									</table>
									
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
</body>
</html>
<%
rsDivision.close
rsProject.close
DataConn.close
Set rsDivision = nothing
Set rsProject = nothing
Set DataConn = nothing
Set oCmd = nothing
%>