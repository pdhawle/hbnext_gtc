<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
userID = request("userID")
dtFrom = request("fromDate")
dtTo = request("toDate")
exclude = request("exclude")


If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")
	
'Create command
if exclude = "on" then
	sp="spGetInspectionsByUserAndDateNoDaily"
else
	sp="spGetInspectionsByUserAndDate"
end if

Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = sp
   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)
   .parameters.Append .CreateParameter("@fromDate", adDBTimeStamp, adParamInput, 8, dtFrom)
   .parameters.Append .CreateParameter("@toDate", adDBTimeStamp, adParamInput, 8, dtTo)   
   .CommandType = adCmdStoredProc   
End With
			
Set rsReport = oCmd.Execute
Set oCmd = nothing


Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetUser"
   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)  
   .CommandType = adCmdStoredProc   
End With
			
Set rsUser = oCmd.Execute
Set oCmd = nothing




Response.ContentType = "application/vnd.ms-excel"
Response.AddHeader "Content-Disposition", "attachment; filename=NewStartReport.xls" 
if rsReport.eof <> true then%>
		<table border=1>
			<tr><td colspan="5"><strong>Inspector: <%=rsUser("firstName")%>&nbsp;<%=rsUser("lastName")%></strong></td></tr>
			<tr><td colspan="5"><strong>Date Range: <%=dtFrom%> - <%=dtTo%></strong></td></tr>
			<tr>
				<td><strong>ReportID</strong></td>
				<td><strong>Customer</strong></td>
				<td><strong>Project</strong></td>
				<td><strong>Report Type</strong></td>
				<td><strong>Pay Rate</strong></td>
				<td><strong>Report Date</strong></td>
			</tr>
	<%dim i
	i = 0
	do until rsReport.eof%>
		<tr>
			<td><%=rsReport("reportID")%></td>
			<td><%=rsReport("customerName")%></td>
			<td><%=rsReport("projectName")%></td>
			<td><%=rsReport("reportType")%></td>
			<td><%=formatcurrency(rsReport("wprRate"),2)%></td>
			<td><%=rsReport("inspectionDate")%></td>
		</tr>
	<%
	rsReport.movenext
	i = i + 1
	loop%>
		<tr>
			<td colspan="4" align="right"><strong>Total Reports:</strong>&nbsp;&nbsp;</td>
			<td><%=i%></td>
		</tr>
	</table>
<%end if

rsReport.close
DataConn.close
Set rsCustomer = nothing
Set rsReport = nothing
Set DataConn = nothing
Set oCmd = nothing%>

