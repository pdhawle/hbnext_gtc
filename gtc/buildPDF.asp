<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->
<!--#include file="letterArray.asp"-->

<%'need customerID, reportID

iDivisionID = request("divisionID")
reportID = request("reportID")

Session("LoggedIn") = "True"

Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 	
DataConn.Open Session("Connection"), Session("UserID")
'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetReportByID"
    .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, reportID) 'reportID
   .CommandType = adCmdStoredProc
   
End With			
Set rsReport = oCmd.Execute
Set oCmd = nothing

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCategories"'ByDivision
    .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, iDivisionID)
   .CommandType = adCmdStoredProc
   
End With
			
Set rsCategories = oCmd.Execute
Set oCmd = nothing
%>
<html>
<head>
<title>Storm Water Site Inspection Report</title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/reportswsir.css" type="text/css">
</head>
<body>

<table border="0" width=750 cellpadding=0 cellspacing=0>
	<tr>		
		<td valign="top" width="400">
			<span class="Header">Storm Water Site Inspection Report<br>Report #<%=reportID%></span><br>
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td><strong>Report Date:</strong>&nbsp;</td>
					<td><u><%=rsReport("inspectionDate")%></u></td>
				</tr>
				<tr>
					<td><strong>Customer:</strong>&nbsp;</td>
					<td><u><%=rsReport("customerName")%></u></td>
				</tr>
				<tr>
					<td><strong>Division:</strong>&nbsp;</td>
					<td><u><%=rsReport("division")%></u></td>
				</tr>
				<tr>
					<td><strong>Site Name:</strong>&nbsp;</td>
					<td><u><%=rsReport("projectName")%></u></td>
				</tr>
			</table>
		</td>
		<td valign=top align="right">
			<%if rsReport("logo") = "" then%>
				<img src="images/logo.jpg">
			<%else%>
				<img src="<%=rsReport("logo")%>">
			<%end if%>
		</td>
	</tr>
	<tr><td colspan=2><img src=images/pix.gif width=1 height=20></td></tr>
	<tr>
		<td colspan=2>
			<table width=750>
				<tr>
					<td><strong>Qualified Inspector:</strong>&nbsp;<u><%=rsReport("firstName")%>&nbsp;<%=rsReport("lastName")%></u></td>
					<td><strong>Inspection Type:</strong>&nbsp;<u><%=rsReport("inspectionType")%></u></td>
					<td>
						<strong>Last Inspection Date:</strong>&nbsp; 
						<%if rsReport("lastInspectionDate") <> "" then
							response.Write "<u>" & rsReport("lastInspectionDate") & "</u>"
						else
							response.Write "<u>N/A</u>"
						end if
						%>
					</td>
				</tr>
				<tr>
					<td><strong>Company:</strong>&nbsp;<u><%=rsReport("company")%></u></td>
					<td><strong>Project Status:</strong>&nbsp;<u><%=rsReport("projectStatus")%></u></td>
					<td><strong>Weather:</strong>&nbsp;<u><%=rsReport("weatherType")%></u></td>
					
				</tr>
				<tr>
					<td><strong>Phone #:</strong>&nbsp;<u><%=rsReport("contactNumber")%></u></td>
					<td><strong>Report Type:</strong>&nbsp;<u><%=rsReport("reportType")%></u></td>
					<td>
						<strong>Rainfall Amount:</strong>&nbsp;<u><%=rsReport("rainfallAmount")%>&nbsp;
						<%if rsReport("rainfallAmount") > 1 then%>
							inches
						<%else%>
							inch
						<%end if%></u>
					</td>
				</tr>
				<%if left(trim(rsReport("inspectionType")),3) = "Lot" then
					Set oCmd = Server.CreateObject("ADODB.Command")
	
					With oCmd
					   .ActiveConnection = DataConn
					   .CommandText = "spGetOpenLots"
						.parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, rsReport("projectID"))
					   .CommandType = adCmdStoredProc
					   
					End With
						
						If Err Then
					%>
							<!--#include file="includes/FatalError.inc"-->
					<%
						End If
								
					Set rsLots = oCmd.Execute
					Set oCmd = nothing%>
					<tr>
						<td colspan="3"><strong>Open Lots: </strong>
							<%If rsLots.eof then
								response.Write "There are currently no open lots."
							else
								do until rsLots.eof
									response.Write "<u>" & rsLots("lotNumber") & "</u>"
								rsLots.movenext
								if not rsLots.eof then
									response.Write ", "
								end if
								loop
							end if%>
						</td>
					</tr>
				<%end if%>
				<tr><td colspan="3" headers="5"></td></tr>
				<tr>
					<td colspan="3"><strong>Additional Comments:</strong>&nbsp;<%=rsReport("comments")%></strong></td>
				</tr>		
			</table>
			
		</td>
	</tr>
	<tr>
		<td colspan=2>
			<table border="0" width=750 cellpadding="0" cellspacing="0">
				<%i = 1
				blnChange = true
				do until rsCategories.eof%>
					<tr><td colspan=4 height="10"></td></tr>
					<tr><td colspan=4 valign="top"><img src="images/doubleLine.gif" width="750" height="3"></td></tr>
					<tr class="rowColor"><td colspan=4>&nbsp;<b><%=rsCategories("category")%></b></td></tr>
					<tr><td colspan=4 valign="top"><img src="images/doubleLine.gif" width="750" height="3"></td></tr>
							
					<%
						Set oCmd = Server.CreateObject("ADODB.Command")

						With oCmd
						   .ActiveConnection = DataConn
						   .CommandText = "spGetQuestionandAnswers"
						   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, rsCategories("categoryID"))
						   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, reportID) 'reportID
						   .CommandType = adCmdStoredProc
						   
						End With
									
						Set rsQuestion = oCmd.Execute
						Set oCmd = nothing%>
						
						<%'blnChange = true
						do until rsQuestion.eof%>
							<tr>
								<td valign=top width="3">&nbsp;<%=i%>.&nbsp;</td>
								<td>
									<%=rsQuestion("question")%>
								</td>
								<td></td>
								<td align=right>
									
									<%	
																
							'		if rsQuestion("addNoteIfAI") = "True" then
										'check to see if there are open items for this question
										'get the percent complete for each question
							'			Set oCmd = Server.CreateObject("ADODB.Command")
							'			With oCmd
							'			   .ActiveConnection = DataConn
							'			   .CommandText = "spGetOpenAIByQuestion"
							'			   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, rsReport("projectID"))
							'			   .parameters.Append .CreateParameter("@questionID", adInteger, adParamInput, 8, rsQuestion("questionID"))
							'			   .CommandType = adCmdStoredProc
							'			   
							'			End With
													
							'			Set rsQuestionComplete = oCmd.Execute
							'			Set oCmd = nothing
							'			if not isnull(rsQuestionComplete("openAI")) then
							'				bNote = "True"%>
											<!--<strong><em>**</em></strong> -->
										<%'end if%>
									<%'end if%>
									<%=rsQuestion("answer")%>
								</td>
							</tr>
							<tr><td colspan=4 height="5"></td></tr>
						<%rsQuestion.movenext
						i = i + 1
						loop%>
						
					<tr><td colspan=4 height="10"></td></tr>
				<%rsCategories.movenext
				loop%>
				<%If bNote = "True" then%>
				
				<tr>
					<td colspan=4>
						<em><strong>** uncompleted items from previous report(s)</strong></em>
					</td>
				</tr>
				<%end if%>
			</table>
		</td>
	</tr>
	<tr><td colspan="2" height="20"></td></tr>
	<tr>
		<td colspan="2">
			<table width="100%">
				<tr>
					<td colspan="3">
						<em>I certify under the penalty of law that this report and all attachments were prepared
						under my direction or supervision in accordance with a system designed to assure that
						certified personnel properly gather and evaluate the information submitted. Based on my
						inquiry of the person or persons who manage the system, or those persons directly
						responsible for gathering the information, the information submitted is, to the best of my
						knowledge and belief, true, accurate, and complete. I am aware that there are significant
						penalties for submitting false information, including the possibility of fine and imprisonment
						for knowing violations</em>
					</td>
				</tr>	
				<tr>
					<td valign="bottom">
						<u><%=rsReport("firstName")%>&nbsp;<%=rsReport("lastName")%></u><br>
						<strong>Name of Inspector Consultant</strong>
					</td>
					<td valign="bottom">
						<%if rsReport("sigFileName") <> "" then%> 
							<u><img src="userUploads/<%=rsReport("userID")%>/<%=rsReport("sigFileName")%>"></u>
						<%else%>
							<u>__________________________</u> 
						<%end if%><br>
						<strong>Signature of Inspector Consultant</strong>
					</td>
					<td valign="bottom">
						<u><%=rsReport("inspectionDate")%></u><br>
						<strong>Date</strong>
					</td>
				</tr>
			</table>
		</td>		
	</tr>
	<tr><td colspan="2" height="20"></td></tr>
	<tr>
		<td colspan="2">
			<strong>Information to Permittee:</strong>  In order to maintain compliance with the terms and conditions of Stormwater General Permit, corrective actions 
			identified in this Inspection Form must be addressed within the timeframe specified by the permit. Please note corrective actions taken on the Stormwater 
			Inspection Form and sign where indicated.
		</td>
	</tr>
</table>
</body>
</html>