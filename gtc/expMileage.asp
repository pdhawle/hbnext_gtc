<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
userID = request("userID")
dtFrom = request("fromDate")
dtTo = request("toDate")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")
	
'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetMileageByUserAndDate"
   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)
   .parameters.Append .CreateParameter("@fromDate", adDBTimeStamp, adParamInput, 8, dtFrom)
   .parameters.Append .CreateParameter("@toDate", adDBTimeStamp, adParamInput, 8, dtTo)   
   .CommandType = adCmdStoredProc   
End With
			
Set rsReport = oCmd.Execute
Set oCmd = nothing


Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetUser"
   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)  
   .CommandType = adCmdStoredProc   
End With
			
Set rsUser = oCmd.Execute
Set oCmd = nothing




Response.ContentType = "application/vnd.ms-excel"
Response.AddHeader "Content-Disposition", "attachment; filename=mileage.xls" 
if rsReport.eof <> true then%>
		<table border=1>
			<tr><td colspan="5"><strong>Employee: <%=rsUser("firstName")%>&nbsp;<%=rsUser("lastName")%></strong></td></tr>
			<tr><td colspan="5"><strong>Date Range: <%=dtFrom%> - <%=dtTo%></strong></td></tr>
			<tr>
				<td><strong>Date</strong></td>
				<td><strong>Odometer Start</strong></td>
				<td><strong>Odometer End</strong></td>
				<td><strong>Miles</strong></td>
				<td><strong>Comments</strong></td>
			</tr>
	<%dim i
	i = 0
	do until rsReport.eof%>
		<tr>
			<td><%=rsReport("date")%></td>
			<td><%=rsReport("odometerStart")%></td>
			<td><%=rsReport("odometerEnd")%></td>
			<td>
				<%=rsReport("miles")%>
				<%iTotalMiles = iTotalMiles + rsReport("miles")%>
			</td>
			<td><%=rsReport("comments")%></td>
		</tr>
	<%
	rsReport.movenext
	if blnChange = true then
		blnChange = false
	else
		blnChange = true
	end if
	i = i + 1
	loop
	
	if iTotalMiles = "" then
		iTotalMiles = 0
	end if%>
	</table>
<%end if

rsReport.close
DataConn.close
Set rsCustomer = nothing
Set rsReport = nothing
Set DataConn = nothing
Set oCmd = nothing%>

