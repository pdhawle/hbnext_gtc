<%
On Error Resume Next

projectID = request("projectID")
primaryFormID = request("id")

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetPrimaryNOI"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, primaryFormID)
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsPrimary = oCmd.Execute
Set oCmd = nothing

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCoverage"
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rs = oCmd.Execute
Set oCmd = nothing

'Create command for state list
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetStates"
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsState = oCmd.Execute
Set oCmd = nothing

'Create command for county list
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCounties"
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsCounty = oCmd.Execute
Set oCmd = nothing


'Create command for secondary permittee list
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetNumSecPermittees"
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsSP = oCmd.Execute
Set oCmd = nothing
%>

<script language="javascript" type="text/javascript">
<!--

function KeepCount() {

	var NewCount = 0
						
	if (document.NOT.typeConstructionCommercial.checked)
	{NewCount = NewCount + 1}
	
	if (document.NOT.typeConstructionIndustrial.checked)
	{NewCount = NewCount + 1}
	
	if (document.NOT.typeConstructionMunicipal.checked)
	{NewCount = NewCount + 1}
	
	if (document.NOT.typeConstructionDOT.checked)
	{NewCount = NewCount + 1}
	
	if (document.NOT.typeConstructionUtility.checked)
	{NewCount = NewCount + 1}
	
	if (document.NOT.typeConstructionResidential.checked)
	{NewCount = NewCount + 1}
	
	if (NewCount == 2)
	{
	alert('Pick Just One Please')
	document.NOT; return false;
	}
} 

function KeepCount2() {

	var NewCount = 0
						
	if (document.NOT.IRWTrout.checked)
	{NewCount = NewCount + 1}
	
	if (document.NOT.IRWWarmWater.checked)
	{NewCount = NewCount + 1}
	
	if (NewCount == 2)
	{
	alert('Pick Just One Please')
	document.NOT; return false;
	}
} 

function KeepCount3() {

	var NewCount = 0
						
	if (document.NOT.RWTrout.checked)
	{NewCount = NewCount + 1}
	
	if (document.NOT.RWWarmWater.checked)
	{NewCount = NewCount + 1}
	
	if (NewCount == 2)
	{
	alert('Pick Just One Please')
	document.NOT; return false;
	}
} 

function KeepCount4() {
  
	var NewCount = 0
						
	if (document.NOT.samplingOfOutfall.checked)
	{NewCount = NewCount + 1}
	
	if (document.NOT.samplingOfRecievingStream.checked)
	{NewCount = NewCount + 1}
	
	if (document.NOT.troutStream.checked)
	{NewCount = NewCount + 1}
	
	if (NewCount == 2)
	{
	alert('Pick Just One Please')
	document.NOT; return false;
	}
} 

function KeepCount5() {
  
	var NewCount = 0
						
	if (document.NOT.typePrimary.checked)
	{NewCount = NewCount + 1}
	
	if (document.NOT.typeSecondary.checked)
	{NewCount = NewCount + 1}
	
	if (document.NOT.typeTertiary.checked)
	{NewCount = NewCount + 1}
	
	if (NewCount == 2)
	{
	alert('Pick Just One Please')
	document.NOT; return false;
	}
} 

function KeepCount6() {

	var NewCount = 0
						
	if (document.NOT.constructionActivityCompleted.checked)
	{NewCount = NewCount + 1}
	
	if (document.NOT.noLongerOwnerOperator.checked)
	{NewCount = NewCount + 1}
	
	if (NewCount == 2)
	{
	alert('Pick Just One Please')
	document.NOT; return false;
	}
}

function KeepCount7() {
  
	var NewCount = 0
						
	if (document.NOT.primaryPermitteeSubdivision.checked)
	{NewCount = NewCount + 1}
	
	if (document.NOT.individualLot.checked)
	{NewCount = NewCount + 1}
	
	if (document.NOT.individualLotWithinSWDA.checked)
	{NewCount = NewCount + 1}
	
	if (NewCount == 2)
	{
	alert('Pick Just One Please')
	document.NOT; return false;
	}
}

//-->
</script>

<form name="NOT" method="post" action="processNOI.asp" onSubmit="return ValidateDate()">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sNOITitle%></span><span class="Header"> - Notice of Termination</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td>
												<strong>VERSION 2008</strong><br />
												<a href="instructions_NOT.asp" target="_blank">Instructions</a><br><br />
												<strong>State of Georgia</strong><br />
												<strong>Department of Natural Resources</strong><br />
												<strong>Environmental Protection Division</strong><br /><br />

												<strong>To Cease Coverage Under the NPDES General Permits</strong><br />
												<strong>To Discharge Storm Water Associated With Construction Activity</strong><br /><br />

												
												<strong>NOTE: Disabled data must be changed on the NOI primary data form.</strong>
											</td>
											<td></td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td>
												<table border="0" cellpadding="0" cellspacing="0">
													<tr>
														<td><strong>I. PERMIT TYPE:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsPrimary("coverageDesiredName")%><input type="hidden" name="coverageDesired" size="30" value="<%=rsPrimary("coverageDesired")%>" />
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
													<tr>
														<td colspan="3"><strong>II. SITE/PERMITTEE INFORMATION</strong></td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td><strong>Project Construction Site Name:</strong><br /><img src="images/pix.gif" width="180" height="1"></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsPrimary("siteProjectName")%><input type="hidden" name="siteProjectName" size="30" value="<%=rsPrimary("siteProjectName")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td valign="top"><strong>GPS Location of Construction Exit (<em>degrees/minutes/seconds</em>):</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<table>
																<tr>
																	<td>
																		Degrees<br /><span class="footer">From 0 to 90</span><br />
																		<%=rsPrimary("GPSDegree1")%><input type="hidden" name="degree1" maxlength="2" size="5" value="<%=rsPrimary("GPSDegree1")%>">
																	</td>
																	<td><img src="images/pix.gif" width="10" height="1"></td>
																	<td>
																		Minutes / Seconds<br /><span class="footer">From 0 to 59</span><br />
																		<%=rsPrimary("GPSMinute1")%>&nbsp;/&nbsp;<%=rsPrimary("GPSSecond1")%>
																		<input type="hidden" name="minute1" maxlength="2" size="5" value="<%=rsPrimary("GPSMinute1")%>">
																		<input type="hidden" name="second1" maxlength="2" size="5" value="<%=rsPrimary("GPSSecond1")%>">
																		<input type="hidden" name="latitude" value="<%=rsPrimary("GPSLat")%>"> North Latitude
																	</td>
																</tr>
																<tr>
																	<td>
																		Degrees<br /><span class="footer">From 0 to 90</span><br />
																		<%=rsPrimary("GPSDegree2")%><input type="hidden" name="degree2" size="5" maxlength="2" value="<%=rsPrimary("GPSDegree2")%>">
																	</td>
																	<td><img src="images/pix.gif" width="10" height="1"></td>
																	<td>
																		Minutes / Seconds<br /><span class="footer">From 0 to 59</span><br />
																		<%=rsPrimary("GPSMinute2")%>&nbsp;/&nbsp;<%=rsPrimary("GPSSecond2")%>
																		<input type="hidden" name="minute2" size="5" maxlength="2" value="<%=rsPrimary("GPSMinute2")%>">									
																		<input type="hidden" name="second2" maxlength="2" size="5" value="<%=rsPrimary("GPSSecond2")%>">
																		<input type="hidden" name="longitude" value="<%=sPrimary("GPSLon")%>"> West Longitude
																	</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Construction Site Location (<em>information must be sufficient <br />to accurately locate the construction site</em>):</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsPrimary("projectAddress")%><input type="hidden" name="projectAddress" size="30" value="<%=rsPrimary("projectAddress")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Subdivision Name (<em>if applicable</em>):</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsPrimary("subdivision")%><input type="hidden" name="subdivision" size="30" value="<%=rsPrimary("subdivision")%>">&nbsp;&nbsp;<strong>Lot Number(s) (<em>if applicable</em>):</strong>&nbsp;&nbsp;<input type="text" name="lotNumber" size="30">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Common Development Name (<em>applicable only to General NPDES Permit No. GAR100003</em>):</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="commonDevelopmentName" size="30">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Construction Site Street Address:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="constructionStreetAddress" size="30">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>City(<em>applicable if the site is located within the jurisdictional boundaries of the city</em>):</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsPrimary("projectCity")%><input type="hidden" name="projectCity" size="30" value="<%=rsPrimary("projectCity")%>">&nbsp;&nbsp;
															<strong>County:</strong>&nbsp;&nbsp;
															<%=rsPrimary("projectCounty")%><input type="hidden" name="projectCounty" size="30" value="<%=rsPrimary("projectCounty")%>">
														</td>
													</tr>
													
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Owner�s Name:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsPrimary("ownerName")%><input type="hidden" name="ownerName" size="30" value="<%=rsPrimary("ownerName")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Address:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsPrimary("ownerAddress")%><input type="hidden" name="ownerAddress" size="30" value="<%=rsPrimary("ownerAddress")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>City:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsPrimary("ownerCity")%><input type="hidden" name="ownerCity" size="20" value="<%=rsPrimary("ownerCity")%>">&nbsp;&nbsp;<strong>State:</strong>&nbsp;&nbsp;
															<%=rsPrimary("ownerState")%><input type="hidden" name="ownerState" size="2" value="<%=rsPrimary("ownerState")%>">&nbsp;&nbsp;
															<strong>Zip:</strong>&nbsp;&nbsp;<%=rsPrimary("ownerZip")%><input type="hidden" name="ownerZip" size="10" value="<%=rsPrimary("ownerZip")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Duly Authorized Representative (<em>optional</em>):</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsPrimary("authorizedRep")%><input type="hidden" name="authorizedRep" size="30" value="<%=rsPrimary("authorizedRep")%>">&nbsp;&nbsp;<strong>Phone:</strong>&nbsp;&nbsp;<%=rsPrimary("authorizedRepPhone")%><input type="hidden" name="authorizedRepPhone" size="30" value="<%=rsPrimary("authorizedRepPhone")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Operator�s Name:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsPrimary("operatorName")%><input type="hidden" name="operatorName" size="30" value="<%=rsPrimary("operatorName")%>">&nbsp;&nbsp;<strong>Phone:</strong>&nbsp;&nbsp;<%=rsPrimary("operatorPhone")%><input type="hidden" name="operatorPhone" size="30" value="<%=rsPrimary("operatorPhone")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Address:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsPrimary("operatorAddress")%><input type="hidden" name="operatorAddress" size="30" value="<%=rsPrimary("operatorAddress")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>City:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsPrimary("operatorCity")%><input type="hidden" name="operatorCity" size="20" value="<%=rsPrimary("operatorCity")%>">&nbsp;&nbsp;<strong>State:</strong>&nbsp;&nbsp;
															<%=rsPrimary("operatorState")%><input type="hidden" name="operatorState" size="2" value="<%=rsPrimary("operatorState")%>">&nbsp;&nbsp;
															<strong>Zip:</strong>&nbsp;&nbsp;<%=rsPrimary("operatorZip")%><input type="hidden" name="operatorZip" size="10" value="<%=rsPrimary("operatorZip")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Facility/Construction Site Contact:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsPrimary("facilityContact")%><input type="hidden" name="facilityContact" size="30" value="<%=rsPrimary("facilityContact")%>">&nbsp;&nbsp;<strong>Phone:</strong>&nbsp;&nbsp;
															<%=rsPrimary("facilityContactPhone")%><input type="hidden" name="facilityContactPhone" size="30" value="<%=rsPrimary("facilityContactPhone")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
													<tr>
														<td colspan="3"><strong>TYPE OF PERMITTEE (<em>Check Only One and Complete</em>):</strong></td>
													</tr>
													<tr>
														<td valign="top"></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="checkbox" name="typePrimary" onClick="return KeepCount5()" />&nbsp;<strong>Primary</strong>&nbsp;&nbsp;
															<!--<input type="checkbox" name="typeSecondary" onClick="return KeepCount5()" />&nbsp;Secondary&nbsp;&nbsp;
															<input type="checkbox" name="typeTertiary" onClick="return KeepCount5()" />&nbsp;Tertiary-->
														</td>
													</tr>
													
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Primary Permittee's Name:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="primaryPermitteeName" size="30">&nbsp;&nbsp;<strong>Phone:</strong>&nbsp;<input type="text" name="primaryPermitteePhone" size="30">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Address:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="primaryPermitteeAddress" size="30">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>City:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="primaryPermitteeCity" size="20">&nbsp;&nbsp;<strong>State:</strong>&nbsp;&nbsp;
															<select name="primaryPermitteeState">
																<%do until rsState.eof%>
																		<option value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
																<%rsState.movenext
																loop
																rsState.moveFirst%>
															</select>
															&nbsp;&nbsp;
															<strong>Zip:</strong>&nbsp;&nbsp;<input type="text" name="primaryPermitteeZip" size="10">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="15"></td></tr>													
													<tr>
														<td colspan="3">
															<input type="checkbox" name="attach1">&nbsp;<strong>Attached to this Notice of Termination</strong> - Listing of the legal name, complete 
															address and telephone number for each secondary permittee at the site for which this NOT is submitted (<em>applicable only 
															to General NPDES Permit No. GAR100003</em>).

														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="15"></td></tr>													
													<tr>
														<td colspan="3">
															<input type="checkbox" name="attach2">&nbsp;<strong>Attached to this Notice of Termination</strong> - Listing of the legal name, complete address 
															and telephone number for the legal title holders for each remaining undeveloped lot(s) at the site for which this NOT is 
															submitted (applicable only to General NPDES Permit No. GAR100003).
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="20"></td></tr>
													
													
													
													<tr>
														<td valign="top"></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<!--<input type="checkbox" name="typePrimary" onClick="return KeepCount5()" />&nbsp;Primary&nbsp;&nbsp;-->
															<input type="checkbox" name="typeSecondary" onClick="return KeepCount5()" />&nbsp;<strong>Secondary Permittee</strong>  (<em>applicable only to General NPDES Permit No. GAR100003</em>)&nbsp;&nbsp;
															<!--<input type="checkbox" name="typeTertiary" onClick="return KeepCount5()" />&nbsp;Tertiary-->
														</td>
													</tr>
													
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Primary Permittee's Name:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="primaryPermitteeName2" size="30">&nbsp;&nbsp;<strong>Phone:</strong>&nbsp;<input type="text" name="primaryPermitteePhone2" size="30">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Address:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="primaryPermitteeAddress2" size="30">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>City:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="primaryPermitteeCity2" size="20">&nbsp;&nbsp;<strong>State:</strong>&nbsp;&nbsp;
															<select name="primaryPermitteeState2">
																<%do until rsState.eof%>
																		<option value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
																<%rsState.movenext
																loop
																rsState.moveFirst%>
															</select>
															&nbsp;&nbsp;
															<strong>Zip:</strong>&nbsp;&nbsp;<input type="text" name="primaryPermitteeZip2" size="10">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="15"></td></tr>
													
													<tr>
														<td valign="top"></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<!--<input type="checkbox" name="typePrimary" onClick="return KeepCount5()" />&nbsp;Primary&nbsp;&nbsp;
															<input type="checkbox" name="typeSecondary" onClick="return KeepCount5()" />&nbsp;Secondary Permittee  (<em>applicable only to General NPDES Permit No. GAR100003</em>)&nbsp;&nbsp;-->
															<input type="checkbox" name="typeTertiary" onClick="return KeepCount5()" />&nbsp;<strong>Tertiary Permittee</strong>  (<em>applicable only to General NPDES Permit No. GAR100003</em>)
														</td>
													</tr>
													
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Primary Permittee's Name:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="primaryPermitteeName3" size="30">&nbsp;&nbsp;<strong>Phone:</strong>&nbsp;<input type="text" name="primaryPermitteePhone3" size="30">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Address:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="primaryPermitteeAddress3" size="30">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>City:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="primaryPermitteeCity3" size="20">&nbsp;&nbsp;<strong>State:</strong>&nbsp;&nbsp;
															<select name="primaryPermitteeState3">
																<%do until rsState.eof%>
																		<option value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
																<%rsState.movenext
																loop
																rsState.moveFirst%>
															</select>
															&nbsp;&nbsp;
															<strong>Zip:</strong>&nbsp;&nbsp;<input type="text" name="primaryPermitteeZip3" size="10">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
													
													<!--<tr>
														<td><strong>Num. of Secondary Permittees:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%'=rsPrimary("secondaryPermittees")%><input type="hidden" name="secondaryPermittees" size="5" value="<%'=rsPrimary("secondaryPermittees")%>">
														</td>
													</tr>
													
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>-->
													<tr>
														<td colspan="3"><strong>III. SITE ACTIVITY INFORMATION</strong></td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td valign="top">
															<strong>Start Date:</strong>&nbsp;<input type="text" name="startDate" size="10" value="<%=rsPrimary("startDate")%>">&nbsp;<a href="javascript:displayDatePicker('startDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>&nbsp;&nbsp;&nbsp;&nbsp;<strong>Completion Date:</strong>&nbsp;&nbsp;<input type="text" name="completionDate" size="10" value="<%=rsPrimary("completionDate")%>">&nbsp;<a href="javascript:displayDatePicker('completionDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Disturbed Acreage:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="estimatedDisturbedAcerage" size="3" value="<%=rsPrimary("estimatedDisturbedAcerage")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td colspan="3">
															<input type="checkbox" name="constructionActivityCompleted" onClick="return KeepCount6()" />&nbsp;Construction Activity Ceased and Final Stabilization Completed&nbsp;&nbsp;
															<input type="checkbox" name="noLongerOwnerOperator" onClick="return KeepCount6()" />&nbsp;No Longer Owner and/or Operator of Facility/Construction Site
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="20"></td></tr>
													<tr>
														<td valign="top"><strong>Construction Activity Type:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="checkbox" name="typeConstructionCommercial" <%=isChecked(rsPrimary("typeConstructionCommercial"))%> onClick="return KeepCount()" />&nbsp;Commercial&nbsp;&nbsp;
															<input type="checkbox" name="typeConstructionIndustrial" <%=isChecked(rsPrimary("typeConstructionIndustrial"))%> onClick="return KeepCount()" />&nbsp;Industrial&nbsp;&nbsp;
															<input type="checkbox" name="typeConstructionMunicipal" <%=isChecked(rsPrimary("typeConstructionMunicipal"))%> onClick="return KeepCount()" />&nbsp;Municipal&nbsp;&nbsp;
															<input type="checkbox" name="typeConstructionDOT" <%=isChecked(rsPrimary("typeConstructionLinear"))%> onClick="return KeepCount()" />&nbsp;DOT<br />
															<input type="checkbox" name="typeConstructionUtility" <%=isChecked(rsPrimary("typeConstructionUtility"))%> onClick="return KeepCount()" />&nbsp;Utility&nbsp;&nbsp;
															<input type="checkbox" name="typeConstructionResidential" <%=isChecked(rsPrimary("typeConstructionResidential"))%> onClick="return KeepCount()" />&nbsp;Residential/Subdivision Development
														</td>
													</tr>
													<!--<tr><td colspan="3"><img src="images/pix.gif" width="1" height="20"></td></tr>
													<tr>
														<td colspan="3">
															<input type="checkbox" name="primaryPermitteeSubdivision" onClick="return KeepCount7()" />&nbsp;Primary Permittee of a Subdivision Development, or<br />
															<input type="checkbox" name="individualLot" onClick="return KeepCount7()" />&nbsp;Individual Lot, or<br />
															<input type="checkbox" name="individualLotWithinSWDA" onClick="return KeepCount7()" />&nbsp;Individual Lot within a Surface Water Drainage Area where the Primary Permittee has ceased Permit Coverage
														</td>
													</tr>-->
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
													<tr>
														<td><strong>Initial Receiving Water(s):</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsPrimary("IRWName")%><input type="hidden" name="IRWName" size="30" value="<%=rsPrimary("IRWName")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Municipal Storm Sewer<br /> System Owner/Operator:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsPrimary("MSSSOwnerOperator")%><input type="hidden" name="MSSSOwnerOperator" size="30" value="<%=rsPrimary("MSSSOwnerOperator")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Name of Receiving Water(s):</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsPrimary("RWName")%><input type="hidden" name="RWName" size="30" value="<%=rsPrimary("RWName")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
													<tr>
														<td colspan="3"><strong>III. CERTIFICATIONS. (Owner or Operator or both to initial as applicable.)</strong></td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td colspan="3">
															<input type="text" name="certify1" size="5">&nbsp;1. I certify under penalty of law that either: (a) all storm water discharges associated
															with construction activity from the portion of the construction activity where I was an Owner or
															Operator have ceased or have been eliminated; (b) all storm water discharges associated with
															construction activity from the identified site that are authorized by General NPDES Permit
															number indicated in Section I of this form have ceased; (c) I am no longer an Owner or
															Operator at the construction site and a new Owner or Operator has assumed operational
															control for those portions of the construction site where I previously had ownership or
															operational control; and/or if I am a primary permittee filing this Notice of Termination under
															Part VI.A.2. of this permit, I will notify by written correspondence to the subsequent legal title
															holder of any remaining lots that these lot Owners and /or Operators will become tertiary
															permittees for purposes of this permit and I will provide these tertiary permittees with the
															primary permittee�s Erosion, Sedimentation and Pollution Control Plan. I understand that by
															submitting this Notice of Termination, that I am no longer authorized to discharge storm water
															associated with construction activity by the general permit, and that discharging pollutants in
															storm water associated with construction activity to waters of Georgia is unlawful under the
															Georgia Water Quality Control Act and the Clean Water Act where the discharge is not
															authorized by a NPDES permit.
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td colspan="3">
															<input type="text" name="certify2" size="5">&nbsp;2. I certify under penalty of law that this document and all attachments were
															prepared under my direction or supervision in accordance with a system designed to assure
															that qualified personnel properly gather and evaluate the information submitted. Based upon
															my inquiry of the person or persons who manage the system, or those persons directly
															responsible for gathering the information, the information submitted is, to the best of my
															knowledge and belief, true, accurate, and complete. I am aware that there are significant
															penalties for submitting false information, including the possibility of fine and imprisonment for
															knowing violations.
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
												</table>
												<table>
													<tr>
														<td valign="top"><strong>Owner�s Printed Name:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsPrimary("ownerPrintedName")%><input type="hidden" name="ownerPrintedName" size="30" value="<%=rsPrimary("ownerPrintedName")%>">&nbsp;&nbsp;&nbsp;&nbsp;<strong>Title:</strong>&nbsp;&nbsp;<%=rsPrimary("ownerTitle")%><input type="hidden" name="ownerTitle" size="30" value="<%=rsPrimary("ownerTitle")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td valign="top" align="right"><strong>Date:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td colspan="3"><input type="text" name="ownerSignDate" size="10" value="<%=date%>">&nbsp;<a href="javascript:displayDatePicker('ownerSignDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a></td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td valign="top"><strong>Operator�s Printed Name:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsPrimary("operatorPrintedName")%><input type="hidden" name="operatorPrintedName" size="30" value="<%=rsPrimary("operatorPrintedName")%>">&nbsp;&nbsp;&nbsp;&nbsp;<strong>Title:</strong>&nbsp;&nbsp;<%=rsPrimary("operatorTitle")%><input type="hidden" name="operatorTitle" size="30" value="<%=rsPrimary("operatorTitle")%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td valign="top" align="right"><strong>Date:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td colspan="3"><input type="text" name="operatorSignDate" size="10" value="<%=date%>">&nbsp;<a href="javascript:displayDatePicker('operatorSignDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a></td>
													</tr>				
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
													<tr>
														<td colspan="3" align="right">
															<input type="hidden" name="projectID" value="<%=projectID%>" />
															<input type="hidden" name="primaryFormID" value="<%=primaryFormID%>" />
															<input type="hidden" name="processType" value="addNOT" />
															<input type="submit" value="Submit Data" class="formButton"/>
														</td>
													</tr>
												</table>
											</td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
										</tr>
										
									</table>
								
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<script language="JavaScript" type="text/javascript">

  var frmvalidator  = new Validator("NOT");
//  frmvalidator.addValidation("lotNumber","req","Please enter the lot number");
//  frmvalidator.addValidation("primaryPermitteeName","req","Please enter the name of the primary permittee");
//  frmvalidator.addValidation("primaryPermitteePhone","req","Please enter the primary permittee's phone number");
//  frmvalidator.addValidation("primaryPermitteeAddress","req","Please enter the street address of the primary permittee");
//  frmvalidator.addValidation("primaryPermitteeCity","req","Please enter the primary permittee's city");
//  frmvalidator.addValidation("primaryPermitteeZip","req","Please enter the primary permittee's zip code");    
//  frmvalidator.addValidation("certify1","req","Please enter initials")
//  frmvalidator.addValidation("certify2","req","Please enter initials")
//  frmvalidator.addValidation("ownerSignDate","req","Please enter the date the owner signs the form");
//  frmvalidator.addValidation("operatorSignDate","req","Please enter the date the operator signs the form"); 

</script>
<%
rs.close
rsState.close
set rs = nothing
set rsState = nothing
DataConn.close
set DataConn = nothing
%>