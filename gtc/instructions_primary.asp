<%bLoggedInInfo = "False"%>
<html>
<head>
<title>On-Demand NOI</title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
</head>
<body>
<table bgcolor="#FFFFFF" class="borderTable" width="400" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr>
		<td><img src="images/pix.gif" width="20" height="1"></td>
		<td>
			<table width="800" border="0" cellpadding="0" cellspacing="0">
				<tr align="center">
					<td colspan="3">
						<span class="Header">Instructions</span><br />
						<strong>Notice of Intent - Primary Permittee<br />
						For Coverage Under the 2008 Re-Issuance of the NPDES General Permits <br />
						To Discharge Storm Water Associated With Construction Activity</strong><br />
						
					</td>
				</tr>
				<tr><td><img src="images/pix.gif" width="1" height="30" /></td></tr>
				<tr>
					<td><img src="images/pix.gif" width="10" height="1" /></td>
					<td>
						Please print or type the Notice of Intent (NOI) form..  Any NOI that contains illegible or incomplete information will not be accepted, will be returned and 
						the construction site will not be granted Permit coverage.  All information requested on the NOI must be submitted in order for the NOI to be valid.  
						Any information requested on the NOI that is not applicable to the primary permittee or to the construction site must be marked �N/A.�  Please do not 
						leave any sections blank in the NOI.<br /><br />

						<strong>Who must file a Notice of Intent Form</strong> - The Owner and/or Operator of a facility/construction site that has a discharge of storm water where 
						construction activities occur must apply for a National Pollutant Discharge Elimination System (NPDES) Permit. The Georgia Environmental Protection Division 
						(EPD) re-issued the General NPDES Permits for Storm Water Discharges Associated with Construction Activity on August 1, 2008.  The Permits are available for 
						review at the EPD District Offices and on the EPD website, www.gaepd.org. It is highly recommended that the permittees read and understand the terms and 
						conditions of the Permits prior to submitting a NOI. Please contact the appropriate EPD District Office as listed on the following pages for assistance 
						in completing the NOI.<br /><br />
						
						<strong>Where to file a Notice of Intent Form</strong> - The NOI and the attachments, as applicable, must be submitted to the appropriate EPD District 
						Office as listed on the following pages.  Please submit only the first three pages of this document with the applicable attachments.<br /><br />						
						
						<strong>Section I - Site/Owner/Operator Information</strong><br /><br />
						
						The construction site name and location information (i.e., GPS location of construction exit, street address, city, county) must be sufficient to accurately 
						locate the construction site.  If the construction site does not have a street address, please provide sufficient information to accurately locate the construction 
						site.  If additional space is needed, attach the location information to the NOI.<br /><br />
						
						A duly authorized representative may be either a named individual or any individual occupying a named position that the primary permittee has authorized to 
						sign all reports, certification statements, or other information requested by EPD.<br /><br />
						
						The facility/construction site contact is the person who the primary permittee has assigned the responsibility for the daily on-site operational control.<br /><br /> 
						
						Please do not leave any blanks in this section.  Any information requested on the NOI that is not applicable to the primary permittee or to the construction site 
						must be mark �N/A.�<br /><br />
						
						<strong>Section II � Construction Site Activity Information</strong><br /><br />

						For construction activities that began prior to the effective date of the Permits, the start date (month/date/year) must be the actual start date of construction 
						activities.<br /><br />
						
						Estimated disturbed acreage is the total number of acres, to the nearest tenth (1/10th) acre, that will be disturbed by the primary permittee and all secondary 
						permittees.  <br /><br />
						
						Please do not leave any blanks in this section.  Any information requested on the NOI that is not applicable to the primary permittee or to the construction 
						site must be mark �N/A.�<br /><br />
						
						
						<strong>Section III - Receiving Water Information</strong><br /><br />
						
						If the facility/construction site discharges storm water directly or indirectly to the receiving water(s), and not through a municipal separate storm sewer 
						system (MS4), enter the name of the receiving water(s) and indicate whether the water(s) is a trout stream or a warm water fisheries stream. Attach a written 
						description and location map identifying the receiving water(s). <br /><br />
						
						If the facility/construction site discharges storm water to a municipal separate storm sewer system (MS4), enter the name of the owner/operator of the 
						MS4 (e.g., city name or county name) and the name of the receiving water(s) at the point of discharge from the MS4. A MS4 is defined as a conveyance or system 
						of conveyances (including roads with drainage systems, municipal streets, catch basins, curbs, gutters, ditches, man-made channels or storm drains) that is 
						owned and/or operated by a city or county which is designed or used for collecting or conveying storm water. It may be necessary to contact the city or county 
						that owns and/or operates the MS4 to determine the name of the receiving water(s). Indicate whether the receiving water(s) is a trout stream or a warm water 
						fisheries stream.  Attach a written description and location map identifying the receiving water(s).<br /><br />
						
						Any permittee who intends to obtain coverage under the Permits for storm water discharges associated with construction activity into an Impaired Stream Segment, 
						or within one (1) linear mile upstream of and within the same watershed as, any portion of an Impaired Stream Segment identified as �not supporting� its 
						designated use(s), as shown on Georgia�s 2008 and subsequent �305(b)/303(d) List Documents (Final)� at the time of NOI submittal, must satisfy the requirements 
						of Part III.C. of the Permits if the Impaired Stream Segment has been listed for criteria violated, �Bio F� (Impaired Fish Community) and/or �Bio M� (Impaired 
						Macroinvertebrate Community), within Category 4a, 4b or 5, and the potential cause is either �NP� (nonpoint source) or �UR� (urban runoff).  Those discharges 
						that are located within one (1) linear mile of an Impaired Stream Segment, but are not located within the watershed of any portion of that stream segment, are 
						excluded from this requirement.  Georgia�s 2008 and subsequent 305(b)/303(d) List Documents (Final)� can be viewed on the EPD website, 
						www.gaepd.org/Documents/305b.html.<br /><br />
						
						If a Total Maximum Daily Load (TMDL) Implementation Plan for sediment has been finalized at least six (6) months prior to the permittee�s submittal of the 
						NOI, the Erosion, Sedimentation and Pollution Control Plan (Plan) must address any site-specific conditions or requirements included in the TMDL Implementation 
						Plan that are applicable to the permittee�s discharge(s) to the Impaired Stream Segment within the timeframe specified in the TMDL Implementation Plan.  If the 
						TMDL Implementation Plan establishes a specific numeric wasteload allocation that applies to an permittee�s discharge(s) to the Impaired Stream Segment, then 
						the permittee must incorporate that allocation into the Erosion, Sedimentation and Pollution Control Plan and implement all necessary measures to meet that 
						allocation.  A list of TMDL Implementation Plans can be viewed on the EPD website, www.gaepd.org.<br /><br />
						
						Please do not leave any blanks in this section.  Any information requested on the NOI that is not applicable to the primary permittee or to the construction 
						site must be mark �N/A.�<br /><br />
						
						<strong>Section V � Certifications</strong><br /><br />

						The owner and/or operator must sign the Notice of Intent and initial the certification statements on the lines provided. Federal and State statutes provide 
						specific requirements as to who is authorized to sign the Notice of Intent forms.  A Notice of Intent form signed by an unauthorized person will not be valid.  
						Please be aware that Federal and State statutes provide for severe penalties for submitting false information on this Notice of Intent form. Federal and State 
						regulations require that the Notice of Intent form be signed as follows:<br /><br />
						
						-  For a corporation, by a responsible corporate officer;<br />
						-  For a partnership or sole proprietorship, by a general partner or the proprietor; and<br />
						-  For a municipality, State, Federal or other public facility, by either a principal executive officer or ranking elected official.<br /><br />
						
						
					</td>
					<td><img src="images/pix.gif" width="10" height="1" /></td>
				</tr>
				<tr><td><img src="images/pix.gif" width="1" height="30" /></td></tr>
				<tr align="center">
					<td colspan="3">
						<strong>GEORGIA EPD DISTRICT OFFICES</strong>
					</td>
				</tr>
				<tr><td><img src="images/pix.gif" width="1" height="30" /></td></tr>
				<tr>
					<td><img src="images/pix.gif" width="10" height="1" /></td>
					<td>
						All Notice of Intents, Notice of Terminations, Erosion, Sedimentation and Pollution Control Plans, Comprehensive
						Monitoring Programs, certifications, reports, and any other information shall be sent to the following District offices of
						EPD:<br /><br />
						
						<strong>A. For facilities/sites located in the following counties:</strong> Bibb, Bleckley, Chattahoochee, Crawford, Dooly, Harris,
						Houston, Jones, Lamar, Macon, Marion, Meriwether, Monroe, Muscogee, Peach, Pike, Pulaski, Schley, Talbot, Taylor,
						Troup, Twiggs, Upson<br /><br />
						<table>
							<tr>
								<td valign="top">Information shall be submitted to:</td>
								<td><img src="images/pix.gif" width="10" height="1" /></td>
								<td>
									West Central District Office<br />
									Georgia Environmental Protection Division<br />
									2640 Shurling Drive<br />
									Macon, GA 31211-3576<br />
									(478) 751-6612<br />
								</td>
							</tr>
						</table><br />
						
						<strong>B. For facilities/sites located in the following counties:</strong> Burke, Columbia, Emanuel, Glascock, Jefferson, Jenkins,
						Johnson, Laurens, McDuffie, Montgomery, Richmond, Screven, Treutlen, Warren, Washington, Wheeler, Wilkinson<br /><br />
						<table>
							<tr>
								<td valign="top">Information shall be submitted to:</td>
								<td><img src="images/pix.gif" width="10" height="1" /></td>
								<td>
									East Central District Office<br />
									Georgia Environmental Protection Division<br />
									1885-A Tobacco Road<br />
									Augusta, GA 30906-8825<br />
									(706) 792-7744<br />
								</td>
							</tr>
						</table><br />
						
						<strong>C. For facilities/sites located in the following counties:</strong> Baldwin, Banks, Barrow, Butts, Clarke, Elbert, Franklin,
						Greene, Hall, Hancock, Hart, Jackson, Jasper, Lincoln, Madison, Morgan, Newton, Oconee, Oglethorpe, Putnam,
						Stephens, Taliaferro, Walton, Wilkes<br /><br />
						<table>
							<tr>
								<td valign="top">Information shall be submitted to:</td>
								<td><img src="images/pix.gif" width="10" height="1" /></td>
								<td>
									Northeast District Office<br />
									Georgia Environmental Protection Division<br />
									745 Gaines School Road<br />
									Athens, GA 30605-3129<br />
									(706) 369-6376<br />
								</td>
							</tr>
						</table><br />
						
						<strong>D. For facilities/sites located in the following counties:</strong> Clayton, Coweta, DeKalb, Fayette, Gwinnett, Heard,
						Henry, Rockdale, Spalding<br /><br />
						<table>
							<tr>
								<td valign="top">Information shall be submitted to:</td>
								<td><img src="images/pix.gif" width="10" height="1" /></td>
								<td>
									Mountain District - Atlanta Satellite<br />
									Georgia Environmental Protection Division<br />
									4244 International Parkway, Suite 114<br />
									Atlanta, GA 30354-3906<br />
									(404) 362-2671<br />
								</td>
							</tr>
						</table><br />
						
						<strong>E. For facilities/sites located in the following counties:</strong> Bartow, Carroll, Catoosa, Chattooga, Cherokee, Cobb,
						Dade, Dawson, Douglas, Fannin, Floyd, Forsyth, Fulton, Gilmer, Gordon, Habersham, Haralson, Lumpkin, Murray,
						Paulding, Pickens, Polk, Rabun, Towns, Union, Walker, White, Whitfield<br /><br />
						<table>
							<tr>
								<td valign="top">Information shall be submitted to:</td>
								<td><img src="images/pix.gif" width="10" height="1" /></td>
								<td>
									Mountain District - Cartersville Office<br />
									Georgia Environmental Protection Division<br />
									P.O. Box 3250<br />
									Cartersville, GA 30120-1705<br />
									(770) 387-4900<br />
								</td>
							</tr>
						</table><br />
						
						<strong>F. For facilities/sites located in the following counties:</strong> Appling, Atkinson, Bacon, Brantley, Bryan, Bulloch,
						Camden, Candler, Charlton, Chatham, Clinch, Coffee, Effingham, Evans, Glynn, Jeff Davis, Liberty, Long, McIntosh,
						Pierce, Tattnall, Toombs, Ware, Wayne<br /><br />
						<table>
							<tr>
								<td valign="top">Information shall be submitted to:</td>
								<td><img src="images/pix.gif" width="10" height="1" /></td>
								<td>
									Coastal District- Brunswick Office<br />
									Georgia Environmental Protection Division<br />
									One Conservation Way<br />
									Brunswick, GA 31520-8687<br />
									(912) 264-7284<br />
								</td>
							</tr>
						</table><br />
						
						<strong>G. For facilities/sites located in the following counties:</strong> Baker, Ben Hill, Berrien, Brooks, Calhoun, Clay, Colquitt,
						Cook, Crisp, Decatur, Dodge, Dougherty, Early, Echols, Grady, Irwin, Lanier, Lee, Lowndes, Miller, Mitchell, Quitman,
						Randolph, Seminole, Stewart, Sumter, Telfair, Terrell, Thomas, Tift, Turner, Webster, Wilcox, Worth<br /><br />
						<table>
							<tr>
								<td valign="top">Information shall be submitted to:</td>
								<td><img src="images/pix.gif" width="10" height="1" /></td>
								<td>
									Southwest District Office<br />
									Georgia Environmental Protection Division<br />
									2024 Newton Road<br />
									Albany, GA 31701-3576<br />
									(912) 430-4144<br />
								</td>
							</tr>
						</table><br />
					</td>
					<td><img src="images/pix.gif" width="10" height="1" /></td>
				</tr>
			</table>
		</td>
		<td><img src="images/pix.gif" width="20" height="1"></td>
	</tr>
</table>
</body>
</html>
