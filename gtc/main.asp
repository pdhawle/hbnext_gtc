<!--#include file="includes/constants.inc"-->
<%
If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

clientID=Session("clientID")

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

Set oCmd = Server.CreateObject("ADODB.Command")		
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetClient"
   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
   .CommandType = adCmdStoredProc
	   
End With
		
Set rsClient = oCmd.Execute
Set oCmd = nothing
%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script language="javascript" src="includes/mm.js"></script>
<script type="text/javascript">
    var GB_ROOT_DIR = "<%=sPDFPath%>/greybox/";
</script>
<script type="text/javascript" src="greybox/AJS.js"></script>
<script type="text/javascript" src="greybox/AJS_fx.js"></script>
<script type="text/javascript" src="greybox/gb_scripts.js"></script>
<link href="greybox/gb_styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="10">
						<table>
							<tr>
								<td><img src="images/pix.gif" width="1" height="100"></td>
								<td valign="middle"><span class="grayHeader"><%=sLoginTitle%></span></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<%if Session("Admin") = "True" then%>
					<td>
						<table bgcolor="#E6E6E6" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td colspan="3">
									<div id="footer" style="position:relative; z-index:2; width:165px; height:31px; left: 0px; top: 0px; visibility: visible;">
									<img src="images/headerBar.gif" width="165" height="31">
									<div id="loggedIn" style="position: absolute; z-index:2; width:165px; height:20px; left: 10px; top: 10px; visibility: visible;">
										<span class="homeHeader"><strong>Sequence Dashboard</strong></span>
									</div>
									</div>
								</td>
							</tr>
							
							<tr>
								<td><img src="images/pix.gif" width="5" height="1"></td>
								<td><br>
									<a href="userList.asp">Users</a> <br>
									<a href="divisionList.asp?id=1">Divisions</a> <br>
									<a href="contractorList.asp">Manage Contractors</a> <br>
									<!--<a href="MaterialsList.asp?clientID=<'%=session("clientID")%>">Products</a> <br>-->
									<a href="adminReports.asp">Administrative Reports</a> <br>
									<!--<a href="toolboxTopics.asp">Toolbox Topics</a> <br>
									<a href="classTypeList.asp">Manage Class Types</a> <br>
									<a href="classCalendar.asp">Open Classes</a> <br>-->
									<%if session("superAdmin") = "True" then 'only for super admins%>
										<a href="emailMembers.asp">Email Active Users</a> <br>
									<%else%>
										<br>
									<%end if%>
									<a href="newsList.asp?clientID=<%=session("clientID")%>">News and Events</a><br>
									<%if session("superAdmin") = "True" then 'only for super admins%>
									<a href="supportTicketList.asp" class="mainLink">Support Tickets</a><br>
									<%else%>
										<br>
									<%end if%>
									
									<br><br>
								</td>
								<td><img src="images/pix.gif" width="2" height="1"></td>
							</tr>
							<tr bgcolor="#000000"><td colspan="3" height="3"></td></tr>
						</table>
					</td>
					<td><img src="images/pix.gif" width="3" height="1"></td>
					<%end if%>
				
				
					<%if Session("inspector") = "True" then%>
					<td>
						<table bgcolor="#E6E6E6" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td colspan="3">
									<div id="footer" style="position:relative; z-index:2; width:165px; height:31px; left: 0px; top: 0px; visibility: visible;">
									<img src="images/headerBar.gif" width="165" height="31">
									<div id="loggedIn" style="position: absolute; z-index:2; width:700px; height:20px; left: 10px; top: 10px; visibility: visible;">
										<span class="homeHeader"><strong>GTC Manager</strong></span>
									</div>
									</div>
								</td>
							</tr>
							
							<tr>
								<td><img src="images/pix.gif" width="5" height="1"></td>
								<td><br>
									 <a href="form.asp?formType=addReport">Add New Report</a> <br>
									 <a href="reportList.asp">View Reports</a> <br>
									 <a href="docManager.asp">Project Documents</a><br>
									 <a href="chooseProj.asp">Open Items Punch List</a> <br>
									 <a href="chooseCorrProj.asp">Open/Corrected Items Log</a> <br>
									 <a href="form.asp?formType=primary">Add NOI</a> <br>
									  <a href="primaryList.asp">View/Print NOIs</a> <br>
									 <a href="locationList.asp?path=Home&divisionID=1">Manage Locations</a> <br>
									 <!--<a href="workOrdersList.asp?clientID=<%'=session("clientID")%>">Work Orders</a> <br>									 
									<a href="templates.asp">Print Templates</a>-->
									<br>

								</td>
								<td><img src="images/pix.gif" width="5" height="1"></td>
							</tr>
							<tr bgcolor="#000000"><td colspan="3" height="3"></td></tr>
						</table>
					</td>
					<td><img src="images/pix.gif" width="3" height="1"></td>
					<%end if%>
					<%'if Session("noiUser") = "True" then%>
					<!--<td>
						<table bgcolor="#E6E6E6" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td colspan="3">
									<div id="footer" style="position:relative; z-index:2; width:165px; height:31px; left: 0px; top: 0px; visibility: visible;">
									<img src="images/headerBar.gif" width="165" height="31">
									<div id="loggedIn" style="position: absolute; z-index:2; width:700px; height:20px; left: 10px; top: 10px; visibility: visible;">
										<span class="homeHeader"><strong>NOI Manager</strong></span>
									</div>
									</div>
								</td>
							</tr>
							
							<tr>
								<td><img src="images/pix.gif" width="5" height="1"></td>
								<td><br>
									  <a href="form.asp?formType=primary">Add New NOI Primary</a> <br>
									  <a href="form.asp?formType=secondary">Add New NOI Secondary</a> <br>
									  <a href="primaryList.asp">NOI Primary List</a> <br>
									  <a href="secondaryList.asp?primaryID=0">NOI Secondary List</a> <br><br><br>
									  <br><br><br><br><br>
									 </td>
								<td><img src="images/pix.gif" width="5" height="1"></td>
							</tr>
							<tr bgcolor="#000000"><td colspan="3" height="3"></td></tr>
						</table>
					</td>
					<td><img src="images/pix.gif" width="3" height="1"></td>-->
					<%'end if%>
					<%if Session("CustomerAdmin") = "True" then%>
					<td>
						<table bgcolor="#E6E6E6" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td colspan="3">
									<div id="footer" style="position:relative; z-index:2; width:165px; height:31px; left: 0px; top: 0px; visibility: visible;">
									<img src="images/headerBar.gif" width="165" height="31">
									<div id="loggedIn" style="position: absolute; z-index:2; width:700px; height:20px; left: 10px; top: 10px; visibility: visible;">
										<span class="homeHeader"><strong>Customer Manager</strong></span>
									</div>
									</div>
								</td>
							</tr>
							
							<tr>
								<td><img src="images/pix.gif" width="5" height="1"></td>
								<td><br>
									 <a href="reportList.asp?man=cust">View Reports</a> <br>
									 <%'if Session("projectDocuments") = "True" then%>
										 <a href="docManager.asp">Project Documents</a><br>
									 <%'else%>
										<!--<br>-->
									 <%'end if%>
									 <!--<a href="chooseProj.asp">Open Items Punch List</a> <br>-->
									 <%'if Session("openCorrectedLog") = "True" then%>
									  <a href="chooseCorrProj.asp">Open/Corrected Items Log</a><br>
									 <%'else%>
										<!--<br>-->
									 <%'end if%>
									 <a href="custReports.asp">Customer Reports</a> <br>									 
									 <!--<a href="workOrdersList.asp?clientID=<%'=session("clientID")%>">Work Orders</a> <br>-->
									 
									 									  
									  
									 <br><br><br><br><br>
								</td>
								<td><img src="images/pix.gif" width="5" height="1"></td>
							</tr>
							<tr bgcolor="#000000"><td colspan="3" height="3"></td></tr>
						</table>
					</td>
					<td><img src="images/pix.gif" width="3" height="1"></td>
					<%end if%>
					
					<td>
						<table bgcolor="#E6E6E6" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td colspan="3">
									<div id="footer" style="position:relative; z-index:2; width:165px; height:31px; left: 0px; top: 0px; visibility: visible;">
									<img src="images/headerBar.gif" width="165" height="31">
									<div id="loggedIn" style="position: absolute; z-index:2; width:700px; height:20px; left: 10px; top: 10px; visibility: visible;">
										<span class="homeHeader"><strong>Support Manager</strong></span>
									</div>
									</div>
								</td>
							</tr>
							
							<tr>
								<td><img src="images/pix.gif" width="5" height="1"></td>
								<td><br>
									 <a href="supportTicket.asp">Support Request</a> <br>
									  <br><br><br><br><br><br><br><br>
								</td>
								<td><img src="images/pix.gif" width="5" height="1"></td>
							</tr>
							<tr bgcolor="#000000"><td colspan="3" height="3"></td></tr>
						</table>
					</td>
					
					<%'if Session("viewQuotes") = "True" then%>
					<!--<td valign="top">
						
						<table bgcolor="#E6E6E6" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td colspan="3">
									<div id="footer" style="position:relative; z-index:2; width:165px; height:31px; left: 0px; top: 0px; visibility: visible;">
									<img src="images/headerBar.gif" width="165" height="31">
									<div id="loggedIn" style="position: absolute; z-index:2; width:700px; height:20px; left: 10px; top: 10px; visibility: visible;">
										<span class="homeHeader"><strong>Sales Manager</strong></span>
									</div>
									</div>
								</td>
							</tr>
							
							<tr>
								<td><img src="images/pix.gif" width="5" height="1"></td>
								<td><br>
									 <a href="form.asp?formType=addQuote&clientID=<%'=session("clientID")%>">Add New Quote</a> <br>
									 <a href="quoteList.asp?clientID=<%'=session("clientID")%>&init=true">View Quotes</a><br>
									 <a href="form.asp?formType=addInfoPack&clientID=<%'=session("clientID")%>">Add Info Pack</a> <br>
									 <a href="infoPackList.asp?clientID=<%'=session("clientID")%>">View Info Packs</a><br>
									 
									 <%'if session("Admin") = "True" then%>
									 	<a href="infoPackList.asp?clientID=<%'=session("clientID")%>&v=True">Verify Info Packs</a><br>
									<%'else%>
										<br>
									 <%'end if%>
									 
									 <br><br><br><br><br><br>
								</td>
								<td><img src="images/pix.gif" width="5" height="1"></td>
							</tr>
							<tr bgcolor="#000000"><td colspan="3" height="3"></td></tr>
						</table>	
										
					</td>-->
					
					<%'end if%>
					
					
					
					
				<!--</tr>
				<tr><td colspan="10"><img src="images/pix.gif" width="1" height="20"></td></tr>	
				<tr>
					<%'if Session("OSHAManager") = "True" then
					'change later if they buy the software%>
					<td valign="top">
						
						<table bgcolor="#E6E6E6" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td colspan="3">
									<div id="footer" style="position:relative; z-index:2; width:165px; height:31px; left: 0px; top: 0px; visibility: visible;">
									<img src="images/headerBar.gif" width="165" height="31">
									<div id="loggedIn" style="position: absolute; z-index:2; width:700px; height:20px; left: 10px; top: 10px; visibility: visible;">
										<span class="homeHeader"><strong>OSHA Manager</strong></span>
									</div>
									</div>
								</td>
							</tr>
							
							<tr>
								<td><img src="images/pix.gif" width="5" height="1"></td>
								<td><br>
									<a href="OSHAReportList.asp">View Mock OSHA Reports</a> <br>
									 <a href="form.asp?formType=addOSHAReport">Add New Mock OSHA</a> <br><br>
									 
									 <a href="form.asp?formType=addOSHA301">Add OSHA 301 Report</a> <br>
									 <a href="OSHA301Log.asp">View OSHA 300 & 300A</a> <br>
									<br><br><br><br>
								</td>
								<td><img src="images/pix.gif" width="5" height="1"></td>
							</tr>
							<tr bgcolor="#000000"><td colspan="3" height="3"></td></tr>
						</table>
						
					
					</td>
					<td><img src="images/pix.gif" width="3" height="1"></td>
					<%'end if%>
					<%'if Session("superAdmin") = "True" then%>						
					<td valign="top">
											
						<table bgcolor="#E6E6E6" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td colspan="3">
									<div id="footer" style="position:relative; z-index:2; width:165px; height:31px; left: 0px; top: 0px; visibility: visible;">
									<img src="images/headerBar.gif" width="165" height="31">
									<div id="loggedIn" style="position: absolute; z-index:2; width:165px; height:20px; left: 10px; top: 10px; visibility: visible;">
										<span class="homeHeader"><strong>HR Manager</strong></span>
									</div>
									</div>
								</td>
							</tr>
							
							<tr>
								<td><img src="images/pix.gif" width="5" height="1"></td>
								<td><br>
									
									 <a href="form.asp?formType=addWorkComp&clientID=<%'=session("clientID")%>">Add Workers Comp Report</a> <br>
									 <%'if Session("hrAdmin") = "True" then%>
									 <a href="workersCompReportList.asp?clientID=<%'=session("clientID")%>">View Workers Comp Reports</a><br>
									 <%'else%>
									 <br>
									 <%'end if%>
									 <a href="form.asp?formType=addNoticeOfSeparation&clientID=<%'=session("clientID")%>">Add Notice Of Separation</a><br>
									 <%'if Session("hrAdmin") = "True" then%>
									 <a href="noticeOfSeparationList.asp?clientID=<%'=session("clientID")%>">View Notices Of Separation</a><br>
									 <%'else%>
									 <br>
									 <%'end if%>
									 
									 <a href="form.asp?formType=addCompliance" class="mainLink">Add Subcontractor/<br>Vendor Compliance Form</a> <br>
									 <%'if Session("Admin") = "True" then%>
									 	<a href="subVendorList.asp" class="mainLink">View Compliance List</a><br>
									 <%'end if%>
									 
									 <br><br>
								</td>
								<td><img src="images/pix.gif" width="2" height="1"></td>
							</tr>
							<tr bgcolor="#000000"><td colspan="3" height="3"></td></tr>
						</table>					
					</td>
					<td><img src="images/pix.gif" width="3" height="1"></td>
					<%'end if%>
					<%'if Session("superAdmin") = "True" then%>
					<td valign="top">						
						<table bgcolor="#E6E6E6" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td colspan="3">
									<div id="footer" style="position:relative; z-index:2; width:165px; height:31px; left: 0px; top: 0px; visibility: visible;">
									<img src="images/headerBar.gif" width="165" height="31">
									<div id="loggedIn" style="position: absolute; z-index:2; width:165px; height:20px; left: 10px; top: 10px; visibility: visible;">
										<span class="homeHeader"><strong>Risk Manager</strong></span>
									</div>
									</div>
								</td>
							</tr>
							
							<tr>
								<td><img src="images/pix.gif" width="5" height="1"></td>
								<td><br>
									 <a href="form.asp?formType=addAccidentReport&clientID=<%'=session("clientID")%>">Add Accident Report</a> <br>
									 <%'if Session("hrAdmin") = "True" then%>
										 <a href="accidentReportList.asp?clientID=<%'=session("clientID")%>">View Accident Reports</a> 
									 <%'end if%>
									
									 <br><br><br><br><br><br><br><br>
								</td>
								<td><img src="images/pix.gif" width="2" height="1"></td>
							</tr>
							<tr bgcolor="#000000"><td colspan="3" height="3"></td></tr>
						</table>
										
					</td>
					<td><img src="images/pix.gif" width="3" height="1"></td>
					<%'end if%>
					<%'if Session("Admin") = "True" then
					'change later if they buy the software%>
					<td valign="top">
						
						<table bgcolor="#E6E6E6" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td colspan="3">
									<div id="footer" style="position:relative; z-index:2; width:165px; height:31px; left: 0px; top: 0px; visibility: visible;">
									<img src="images/headerBar.gif" width="165" height="31">
									<div id="loggedIn" style="position: absolute; z-index:2; width:700px; height:20px; left: 10px; top: 10px; visibility: visible;">
										<span class="homeHeader"><strong>Safety Manager</strong></span>
									</div>
									</div>
								</td>
							</tr>
							
							<tr>
								<td><img src="images/pix.gif" width="5" height="1"></td>
								<td><br>
									 <a href="form.asp?formType=toolboxSession" class="mainLink">Conduct Toolbox Session</a> <br>
									 <a href="toolboxSessionList.asp" class="mainLink">Toolbox Session List</a> <br>
									 <br><br><br><br><br><br>								 
									 <br>
								</td>
								<td><img src="images/pix.gif" width="5" height="1"></td>
							</tr>
							<tr bgcolor="#000000"><td colspan="3" height="3"></td></tr>
						</table>
						
					
					</td>
					<td><img src="images/pix.gif" width="3" height="1"></td>
					<%'end if%>-->
					
				<tr>
					<td valign="top" colspan="30">
						<br>
						<!--get the news for the client and all public news-->
						<%
						Set oCmd = Server.CreateObject("ADODB.Command")	
						With oCmd
						   .ActiveConnection = DataConn
						   .CommandText = "spGetSiteNewsForHome"
						   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, clientID)
						   .CommandType = adCmdStoredProc   
						End With
						Set rsNews = oCmd.Execute
						Set oCmd = nothing
						%>
						<table width="100%" cellpadding="0" cellspacing="0">
							<tr><td colspan="2" height="20"></td></tr>
							<tr class="colorBars">
								<td width="10"></td>
								<td valign="middle"><span class="Header">Sequence News</span></td>
							</tr>
							<tr><td colspan="2" height="10"></td></tr>
							<%do until rsNews.eof%>
								<tr>
									<td></td>
									<td><a href="siteNewsDetails.asp?siteNewsID=<%=rsNews("siteNewsID")%>" title="Sequence News" rel="gb_page_center[820, 500]"><%=rsNews("headline")%></a></td>
								</tr>
							<%rsNews.movenext
							loop%>
						</table>
					
					</td>
				
				</tr>

				<tr><td colspan="10"><img src="images/pix.gif" width="1" height="10"></td></tr>		
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>