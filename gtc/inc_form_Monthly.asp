<%
'get the latest monthly report for this project
Set oCmd = Server.CreateObject("ADODB.Command")
		
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetLatestMonthlyReport"
   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, iProjectID)
   .CommandType = adCmdStoredProc
   
End With

Set rsLatestReport = oCmd.Execute
Set oCmd = nothing

'response.Write rsLatestReport("reportID")

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetMonthlyRainReport"
    .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, rsLatestReport("reportID")) 'reportID
   .CommandType = adCmdStoredProc
   
End With
			
Set rsMonthly = oCmd.Execute
Set oCmd = nothing


'response.Write rsLatestReport("reportID")

%>
<table cellpadding="0" cellspacing="0" border="0">
	
	<tr>
		<td>
			<strong>Perimeter Control BMP's Installation Date</strong>
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			<input type="text" name="perimControlBMPInstallDate" size="5" value="<%=rsMonthly("perimControlBMPInstallDate")%>" />&nbsp;<a href="javascript:displayDatePicker('perimControlBMPInstallDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
		</td>
	</tr>
	<tr><td height="10"></td></tr>
	<tr>
		<td>
			<strong>Initial Sediment Storage Requirements Installation Date</strong>
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			<input type="text" name="initSedStorageInstallDate" size="5" value="<%=rsMonthly("initSedStorageInstallDate")%>" />&nbsp;<a href="javascript:displayDatePicker('initSedStorageInstallDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
		</td>
	</tr>
	<tr><td height="10"></td></tr>
	<tr>
		<td>
			<strong>Sediment Storage/Perimeter Control BMP'sfor T/L "Initial Segment" Installation Date</strong>
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			<input type="text" name="sedStorageBMPInstallDate" size="5" value="<%=rsMonthly("sedStorageBMPInstallDate")%>" />&nbsp;<a href="javascript:displayDatePicker('sedStorageBMPInstallDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
		</td>
	</tr>
	<tr><td height="20"></td></tr>	
	
	
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			<select name="landDisturbanceWeekendHoliday">
				<option></option>
				<option>Yes</option>
				<option>No</option>
			</select>
			&nbsp;Did land disturbance occur on a weekend or holiday?
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			If yes - list dates<br />
			<textarea name="landDisturbanceWeekendHolidayListDates" cols="30" rows="3"></textarea>
		</td>
	</tr>
	<tr><td height="20"></td></tr>
	
	
	
	<tr>
		<td>
			<strong>Daily Inspection Forms</strong>
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			<input type="text" name="tlNumConstructionDays" size="1" />&nbsp;# of construction days
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			<select name="tlDailyReportsAllDays">
				<option></option>
				<option>Yes</option>
				<option>No</option>
				<option selected="selected">N/A</option>
			</select>
			&nbsp;Daily Reports for all Construction Days?
		</td>
	</tr>
	<!--<tr><td height="3"></td></tr>
	<tr>
		<td>
			<input type="text" name="ssNumConstructionDays" size="1" />&nbsp;S/S # of construction days
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			<select name="ssDailyReportsAllDays">
				<option></option>
				<option>Yes</option>
				<option>No</option>
			</select>
			&nbsp;Daily Reports for all Construction Days?
		</td>
	</tr>-->
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			<select name="complianceGTCPolicy">
				<option></option>
				<option>Yes</option>
				<option>No</option>
			</select>
			&nbsp;Compliance with GTC policy and procedures for Daily Inspection Forms?
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			If no - why?<br />
			<textarea name="complianceGTCPolicyWhyNot" cols="30" rows="3"></textarea>
		</td>
	</tr>
	
	
	<tr><td height="20"></td></tr>
	<tr>
		<td>
			<strong>Stabilization</strong>
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			<select name="stabilizationAchieved">
				<option></option>
				<option <%=isSelected(rsMonthly("stabilizationAchieved"),"Yes with temporary vegetation")%>>Yes with temporary vegetation</option>
				<option <%=isSelected(rsMonthly("stabilizationAchieved"),"Yes with permanent vegetation")%>>Yes with permanent vegetation</option>
				<option <%=isSelected(rsMonthly("stabilizationAchieved"),"No")%>>No</option>
			</select>
			&nbsp;Is stabilization achieved?
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			If yes, then provide specifics on stabilization<br />
			<textarea name="explainStabilizationAchieved" cols="30" rows="3"><%=rsMonthly("explainStabilizationAchieved")%></textarea>
		</td>
	</tr>
	<tr><td height="10"></td></tr>
	
	<tr>
		<td>
			<strong>Site Stabilization Date</strong>
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			<input type="text" name="siteStabilizationDate" size="5" value="<%=rsMonthly("siteStabilizationDate")%>" />&nbsp;<a href="javascript:displayDatePicker('siteStabilizationDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
		</td>
	</tr>
	
	
	
	
	<tr><td height="20"></td></tr>
	<tr>
		<td>
			<strong>BMP Inspection Forms</strong>
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			<select name="inspectionOccurEvery14Days">
				<option></option>
				<option>Yes</option>
				<option>No</option>
			</select>
			&nbsp;Did an inspection occur every 14 days during the month?
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			If no - why?<br />
			<textarea name="inspectionOccurEvery14DaysWhyNot" cols="30" rows="3"></textarea>
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			<select name="inspectionOccur24HourRain">
				<option></option>
				<option>Yes</option>
				<option>No</option>
			</select>
			&nbsp;Did inspections occur within 24 hours of a "= or >" 0.5 inch rainfall event or the next business day if the rainfall ended during non-business hours?
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			If no - why?<br />
			<textarea name="inspectionOccur24HourRainWhyNot" cols="30" rows="3"></textarea>
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			<select name="monthlyInspectionOccur">
				<option></option>
				<option>Yes</option>
				<option>No</option>
			</select>
			&nbsp;If stabilization has been achieved, did a monthly inspection occur?
		</td>
	</tr>
	<tr><td height="20"></td></tr>
	<tr>
		<td>
			<strong>BMP Installation and Maintenance</strong>
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			<select name="bmpComplianceGTCPolicy">
				<option></option>
				<option>Yes</option>
				<option>No</option>
			</select>
			&nbsp;Compliance with GTC policy and procedures for BMP Installation and Maintenance?
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			If no - why?<br />
			<textarea name="bmpComplianceGTCPolicyWhyNot" cols="30" rows="3"></textarea>
		</td>
	</tr>
	
	
	
	
	
	
	
	
	
	<tr><td height="20"></td></tr>
	<tr>
		<td>
			<strong>Water Quality Monitoring</strong>
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td align="left">
			<%'bring forth the data from the previous report
			Set oCmd = Server.CreateObject("ADODB.Command")
	
			With oCmd
			   .ActiveConnection = DataConn
			   .CommandText = "spGetMonthlyRainData"
				.parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, rsLatestReport("reportID")) 'reportID
			   .CommandType = adCmdStoredProc
			   
			End With
						
			Set rsData = oCmd.Execute
			Set oCmd = nothing
			
			'create arrays for each item
			i = 0
			Dim arrSamplerID(10)
		'	Dim arrGCSamplerID(10)
		'	Dim arrSamplerType(10)
			Dim arrDateEndClearingGrubbing(10)
			Dim arrDateSamplerInstalled(10)			
			Dim arrDateSampleTakenAfterClearGrub(10)
			Dim arrDateEndGrading(10)
			Dim arrDateAfterEndGradingorNinety(10)
			
			
			Dim arrSamplingRequired(10)
			Dim arrSamplingComplete(10)
			
			'Dim arrBMPInstalledMaintRetrieved(10)
			'Dim arrDateSamplerRemoved(10)
			Dim arrRainDateEvent(10)
			Dim arrRainAmount(10)
			Dim arrCodeCol1(10)
			Dim arrCodeCol2(10)
			Dim arrCodeCol3(10)
			Dim arrCodeCol4(10)
			Dim arrCodeCol5(10)
			Dim arrCodeCol6(10)
			Dim arrCodeCol7(10)
			Dim arrCodeCol8(10)
			Dim arrCodeCol9(10)
			Dim arrCodeCol10(10)
			
			do until rsData.eof
			
				'response.Write  "Samp ID " & rsData("samplerID") & "<br>"
			
				arrSamplerID(i) = rsData("samplerID")
		'		arrGCSamplerID(i) = rsData("GCSamplerID")
		'		arrSamplerType(i) = rsData("samplerType")
				arrDateEndClearingGrubbing(i) = rsData("dateEndClearingGrubbing")
				arrDateSamplerInstalled(i) = rsData("dateSamplerInstalled")
				arrDateSampleTakenAfterClearGrub(i) = rsData("dateSampleTakenAfterClearGrub")
				arrDateEndGrading(i) = rsData("dateEndGrading")
				arrDateAfterEndGradingorNinety(i) = rsData("dateAfterEndGradingorNinety")
				
				arrSamplingRequired(i) = rsData("samplingRequired")
				arrSamplingComplete(i) = rsData("samplingComplete")
				
				
				'arrBMPInstalledMaintRetrieved(i) = rsData("BMPInstalledMaintRetrieved")
				'arrDateSamplerRemoved(i) = rsData("dateSamplerRemoved")
				arrRainDateEvent(i) = rsData("rainDateEvent")
				arrRainAmount(i) = rsData("rainAmount")
				arrCodeCol1(i) = rsData("codeCol1")
				arrCodeCol2(i) = rsData("codeCol2")
				arrCodeCol3(i) = rsData("codeCol3")
				arrCodeCol4(i) = rsData("codeCol4")
				arrCodeCol5(i) = rsData("codeCol5")
				arrCodeCol6(i) = rsData("codeCol6")
				arrCodeCol7(i) = rsData("codeCol7")
				arrCodeCol8(i) = rsData("codeCol8")
				arrCodeCol9(i) = rsData("codeCol9")
				arrCodeCol10(i) = rsData("codeCol10")
			rsData.movenext
			i=i+1
			loop
			%>
			<table width="1400" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td align="right">
						Sampler ID (See CMP for ID)&nbsp;			
					</td>
					<%ii = 0
					
					for i=1 to 10
						Set oCmd = Server.CreateObject("ADODB.Command")

						With oCmd
						   .ActiveConnection = DataConn
						   .CommandText = "spGetLocationsByProjectWS"
						   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, iProjectID) 'reportID
						   .CommandType = adCmdStoredProc
						   
						End With
									
						Set rsLoc = oCmd.Execute
						Set oCmd = nothing%>
						<td>
							<select class="graySelect" name="samplerID<%=i%>">
								<option></option>
								<%do until rsLoc.eof
									if trim(arrSamplerID(ii)) = trim(rsLoc("location")) then%>
										<option selected="selected" value="<%=handleApostropheDisplay(rsLoc("location"))%>"><%=handleApostropheDisplay(rsLoc("location"))%></option>
									<%else%>
										<option value="<%=handleApostropheDisplay(rsLoc("location"))%>"><%=handleApostropheDisplay(rsLoc("location"))%></option>
								<%end if
								rsLoc.movenext
								loop%>
							</select>
							<!--<input type="text" size="10" class="grayTextBox" name="samplerID<%'=i%>" value="<%'=arrSamplerID(i - 1)%>" />-->
						</td>
					<%ii = ii + 1
					next%>
				</tr>
				<tr><td colspan="20" height="3"></td></tr>
				<!--<tr>
					<td align="right" colspan="2">
						GTC Sampler ID&nbsp;			
					</td>
					<%'for i=1 to 10%>
						<td>
							<input type="text" size="10" class="grayTextBox" name="GCSamplerID<%'=i%>" value="<%'=arrGCSamplerID(i - 1)%>" />
						</td>
					<%'next%>
				</tr>
				<tr><td colspan="20" height="3"></td></tr>
				<tr>
					<td align="right" colspan="2">
						Type (Upstream, Downstream, Outfall)&nbsp;			
					</td>
					<%'for i=1 to 10%>
						<td>
							<select class="graySelect" name="samplerType<%=i%>">
								<option></option>
								<option <%'=isSelected("Upstream",trim(arrSamplerType(i - 1)))%>>Upstream</option>
								<option <%'=isSelected("Downstream",trim(arrSamplerType(i - 1)))%>>Downstream</option>
								<option <%'=isSelected("Outfall",trim(arrSamplerType(i - 1)))%>>Outfall</option>	
							</select>
						</td>
					<%'next%>
				</tr>
				<tr><td colspan="20" height="3"></td></tr>-->
				<tr>
					<td align="right">
						Date end clearing and grubbing&nbsp;			
					</td>
					<%ii = 0
					for i=1 to 10%>
						<td width="120">
							<input type="text" size="15" class="grayTextBox" name="dateEndClearingGrubbing<%=i%>" value="<%=arrDateEndClearingGrubbing(ii)%>" />&nbsp;<a href="javascript:displayDatePicker('dateEndClearingGrubbing<%=i%>')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
						</td>
					<%ii = ii + 1
					next%>
				</tr>
				<!--<tr><td colspan="20" height="3"></td></tr>
				<tr>
					<td align="right">
						Date Sampler Installed&nbsp;			
					</td>
					<%'ii = 0
					'for i=1 to 10%>
						<td>
							<input type="text" size="15" class="grayTextBox" name="dateSamplerInstalled<%'=i%>" value="<%'=arrDateSamplerInstalled(ii)%>" />&nbsp;<a href="javascript:displayDatePicker('dateSamplerInstalled<%'=i%>')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
						</td>
					<%'ii = ii + 1
					'next%>
				</tr>-->
				<tr><td colspan="20" height="3"></td></tr>
				<tr>
					<td align="right">
						Date sample taken after clearing/grubbing&nbsp;			
					</td>
					<%ii = 0
					for i=1 to 10%>
						<td>
							<input type="text" size="15" class="grayTextBox" name="dateSampleTakenAfterClearGrub<%=i%>" value="<%=arrDateSampleTakenAfterClearGrub(ii)%>" />&nbsp;<a href="javascript:displayDatePicker('dateSampleTakenAfterClearGrub<%=i%>')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
						</td>
					<%ii = ii + 1
					next%>
				</tr>
				<tr><td colspan="20" height="3"></td></tr>
				<tr>
					<td align="right">
						Date of end of grading&nbsp;			
					</td>
					<%ii = 0
					for i=1 to 10%>
						<td>
							<input type="text" size="15" class="grayTextBox" name="dateEndGrading<%=i%>" value="<%=arrDateEndGrading(ii)%>" />&nbsp;<a href="javascript:displayDatePicker('dateEndGrading<%=i%>')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
						</td>
					<%ii = ii + 1
					next%>
				</tr>
				<tr><td colspan="20" height="3"></td></tr>
				<tr>
					<td align="right" valign="top">
						Date sample taken after end of grading<br /> or within 90 days of first sampling event&nbsp;			
					</td>
					<%ii = 0
					for i=1 to 10%>
						<td>
							<input type="text" size="15" class="grayTextBox" name="dateAfterEndGradingorNinety<%=i%>" value="<%=arrDateAfterEndGradingorNinety(ii)%>" />&nbsp;<a href="javascript:displayDatePicker('dateAfterEndGradingorNinety<%=i%>')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
						</td>
					<%ii = ii + 1
					next%>
				</tr>
				<tr><td colspan="20" height="3"></td></tr>
				<tr>
					<td align="right" valign="top">
						Is sampling required for this<br />project per the ES&PC Plans?&nbsp;			
					</td>
					<%ii = 0
					for i=1 to 10%>
						<td>
							<select class="graySelect" name="samplingRequired<%=i%>">
								<option></option>
								<option <%=isSelected("Yes",trim(arrSamplingRequired(i - 1)))%>>Yes</option>
								<option <%=isSelected("No",trim(arrSamplingRequired(i - 1)))%>>No</option>
							</select>
						</td>
					<%ii = ii + 1
					next%>
				</tr>
				<tr><td colspan="20" height="3"></td></tr>
				<tr>
					<td align="right" valign="top">
						Sampling Complete?&nbsp;			
					</td>
					<%ii = 0
					for i=1 to 10%>
						<td>
							<select class="graySelect" name="samplingComplete<%=i%>">
								<option></option>
								<option <%=isSelected("Yes",trim(arrSamplingComplete(i - 1)))%>>Yes</option>
								<option <%=isSelected("No",trim(arrSamplingComplete(i - 1)))%>>No</option>
							</select>
						</td>
					<%ii = ii + 1
					next%>
				</tr>
				
				<!--<tr><td colspan="20" height="3"></td></tr>
				<tr>
					<td align="right" valign="top" colspan="2">
						Were BMP's designed, installed and maintained<br />when sample retrieved?&nbsp;			
					</td>
					<%'for i=1 to 10%>
						<td>
							<select class="graySelect" name="BMPInstalledMaintRetrieved<%'=i%>">
								<option></option>
								<option <%'=isSelected("Yes",trim(arrBMPInstalledMaintRetrieved(i - 1)))%>>Yes</option>
								<option <%'=isSelected("No",trim(arrBMPInstalledMaintRetrieved(i - 1)))%>>No</option>
								<option <%'=isSelected("N/A",trim(arrBMPInstalledMaintRetrieved(i - 1)))%>>N/A</option>	
							</select>
						</td>
					<%'next%>
				</tr>-->
				<!--<tr><td colspan="20" height="3"></td></tr>
				<tr>
					<td align="right" colspan="2">
						Date Sampler Removed & reason&nbsp;			
					</td>
					<%'ii = 0
					'for i=1 to 10%>
						<td>
							<input type="text" size="10" class="grayTextBox" name="arrDateSamplerRemoved<%'=i%>" value="<%'=arrDateAfterEndGradingorNinety(ii)%>" />&nbsp;<a href="javascript:displayDatePicker('dateSamplerRemoved<%=i%>')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
						</td>
					<%'ii = ii + 1
					'next%>
				</tr>-->
			</table>
			<table width="1300" cellpadding="0" cellspacing="0">
				<tr><td height="20" colspan="3"></td></tr>
				<tr>
					<td colspan="3">
						<strong>Monthly Rainfall Events</strong>
					</td>
				</tr>
				<tr>
					<td>Date of >= 0.5<br /> inch event</td>
					<td>Amt. Of<br />Rainfall</td>
					<td colspan="10" bgcolor="#000000">&nbsp;</td>
				</tr>
				<%ii = 0
				for i=1 to 10%>
				<tr>
					<td><input type="text" size="15" class="grayTextBox" name="rainDateEvent<%=i%>" value="<%'=arrRainDateEvent(ii)%>" />&nbsp;<a href="javascript:displayDatePicker('rainDateEvent<%=i%>')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a></td>
					<td><input type="text" size="10" class="grayTextBox" name="rainAmount<%=i%>" value="<%'=arrRainAmount(ii)%>" /></td>
					<td>
						<select class="blueSelect" name="codeCol1<%=i%>">
							<option></option>
							<option <%'=isSelected(trim(arrCodeCol1(ii)),"E")%>>ST-E</option>
							<option <%'=isSelected(trim(arrCodeCol1(ii)),"ST")%>>ST</option>
							<option <%'=isSelected(trim(arrCodeCol1(ii)),"N/A")%>>N/A</option>	
							<option <%'=isSelected(trim(arrCodeCol1(ii)),"Q-NO")%>>Q-NO</option>
							<option <%'=isSelected(trim(arrCodeCol1(ii)),"Q-ND")%>>Q-ND</option>
							<option <%'=isSelected(trim(arrCodeCol1(ii)),"Q-DB")%>>Q-DB</option>
							<option <%'=isSelected(trim(arrCodeCol10(ii)),"Q-DB")%>>Q-MS</option>
							<option <%'=isSelected(trim(arrCodeCol1(ii)),"Q-SM")%>>Q-SM</option>
							<option <%'=isSelected(trim(arrCodeCol1(ii)),"Q-IY")%>>Q-IU</option>
							<option <%'=isSelected(trim(arrCodeCol1(ii)),"NBH")%>>NBH</option>
						</select>						
					</td>
					<td>
						<select class="blueSelect" name="codeCol2<%=i%>">
							<option></option>
							<option <%'=isSelected(trim(arrCodeCol2(ii)),"E")%>>ST-E</option>
							<option <%'=isSelected(trim(arrCodeCol2(ii)),"ST")%>>ST</option>
							<option <%'=isSelected(trim(arrCodeCol2(ii)),"N/A")%>>N/A</option>	
							<option <%'=isSelected(trim(arrCodeCol2(ii)),"Q-NO")%>>Q-NO</option>
							<option <%'=isSelected(trim(arrCodeCol2(ii)),"Q-ND")%>>Q-ND</option>
							<option <%'=isSelected(trim(arrCodeCol2(ii)),"Q-DB")%>>Q-DB</option>
							<option <%'=isSelected(trim(arrCodeCol10(ii)),"Q-DB")%>>Q-MS</option>
							<option <%'=isSelected(trim(arrCodeCol2(ii)),"Q-SM")%>>Q-SM</option>
							<option <%'=isSelected(trim(arrCodeCol1(ii)),"Q-IY")%>>Q-IU</option>
							<option <%'=isSelected(trim(arrCodeCol2(ii)),"NBH")%>>NBH</option>
						</select>						
					</td>
					<td>
						<select class="blueSelect" name="codeCol3<%=i%>">
							<option></option>
							<option <%'=isSelected(trim(arrCodeCol3(ii)),"E")%>>ST-E</option>
							<option <%'=isSelected(trim(arrCodeCol3(ii)),"ST")%>>ST</option>
							<option <%'=isSelected(trim(arrCodeCol3(ii)),"N/A")%>>N/A</option>	
							<option <%'=isSelected(trim(arrCodeCol3(ii)),"Q-NO")%>>Q-NO</option>
							<option <%'=isSelected(trim(arrCodeCol3(ii)),"Q-ND")%>>Q-ND</option>
							<option <%'=isSelected(trim(arrCodeCol3(ii)),"Q-DB")%>>Q-DB</option>
							<option <%'=isSelected(trim(arrCodeCol10(ii)),"Q-DB")%>>Q-MS</option>
							<option <%'=isSelected(trim(arrCodeCol3(ii)),"Q-SM")%>>Q-SM</option>
							<option <%'=isSelected(trim(arrCodeCol1(ii)),"Q-IY")%>>Q-IU</option>
							<option <%'=isSelected(trim(arrCodeCol3(ii)),"NBH")%>>NBH</option>
						</select>						
					</td>
					<td>
						<select class="blueSelect" name="codeCol4<%=i%>">
							<option></option>
							<option <%'=isSelected(trim(arrCodeCol4(ii)),"E")%>>ST-E</option>
							<option <%'=isSelected(trim(arrCodeCol4(ii)),"ST")%>>ST</option>
							<option <%'=isSelected(trim(arrCodeCol4(ii)),"N/A")%>>N/A</option>	
							<option <%'=isSelected(trim(arrCodeCol4(ii)),"Q-NO")%>>Q-NO</option>
							<option <%'=isSelected(trim(arrCodeCol4(ii)),"Q-ND")%>>Q-ND</option>
							<option <%'=isSelected(trim(arrCodeCol4(ii)),"Q-DB")%>>Q-DB</option>
							<option <%'=isSelected(trim(arrCodeCol10(ii)),"Q-DB")%>>Q-MS</option>
							<option <%'=isSelected(trim(arrCodeCol4(ii)),"Q-SM")%>>Q-SM</option>
							<option <%'=isSelected(trim(arrCodeCol1(ii)),"Q-IY")%>>Q-IU</option>
							<option <%'=isSelected(trim(arrCodeCol4(ii)),"NBH")%>>NBH</option>
						</select>						
					</td>
					<td>
						<select class="blueSelect" name="codeCol5<%=i%>">
							<option></option>
							<option <%'=isSelected(trim(arrCodeCol5(ii)),"E")%>>ST-E</option>
							<option <%'=isSelected(trim(arrCodeCol5(ii)),"ST")%>>ST</option>
							<option <%'=isSelected(trim(arrCodeCol5(ii)),"N/A")%>>N/A</option>	
							<option <%'=isSelected(trim(arrCodeCol5(ii)),"Q-NO")%>>Q-NO</option>
							<option <%'=isSelected(trim(arrCodeCol5(ii)),"Q-ND")%>>Q-ND</option>
							<option <%'=isSelected(trim(arrCodeCol5(ii)),"Q-DB")%>>Q-DB</option>
							<option <%'=isSelected(trim(arrCodeCol10(ii)),"Q-DB")%>>Q-MS</option>
							<option <%'=isSelected(trim(arrCodeCol5(ii)),"Q-SM")%>>Q-SM</option>
							<option <%'=isSelected(trim(arrCodeCol1(ii)),"Q-IY")%>>Q-IU</option>
							<option <%'=isSelected(trim(arrCodeCol5(ii)),"NBH")%>>NBH</option>
						</select>						
					</td>
					<td>
						<select class="blueSelect" name="codeCol6<%=i%>">
							<option></option>
							<option <%'=isSelected(trim(arrCodeCol6(ii)),"E")%>>ST-E</option>
							<option <%'=isSelected(trim(arrCodeCol6(ii)),"ST")%>>ST</option>
							<option <%'=isSelected(trim(arrCodeCol6(ii)),"N/A")%>>N/A</option>	
							<option <%'=isSelected(trim(arrCodeCol6(ii)),"Q-NO")%>>Q-NO</option>
							<option <%'=isSelected(trim(arrCodeCol6(ii)),"Q-ND")%>>Q-ND</option>
							<option <%'=isSelected(trim(arrCodeCol6(ii)),"Q-DB")%>>Q-DB</option>
							<option <%'=isSelected(trim(arrCodeCol10(ii)),"Q-DB")%>>Q-MS</option>
							<option <%'=isSelected(trim(arrCodeCol6(ii)),"Q-SM")%>>Q-SM</option>
							<option <%'=isSelected(trim(arrCodeCol1(ii)),"Q-IY")%>>Q-IU</option>
							<option <%'=isSelected(trim(arrCodeCol6(ii)),"NBH")%>>NBH</option>
						</select>						
					</td>
					<td>
						<select class="blueSelect" name="codeCol7<%=i%>">
							<option></option>
							<option <%'=isSelected(trim(arrCodeCol7(ii)),"E")%>>ST-E</option>
							<option <%'=isSelected(trim(arrCodeCol7(ii)),"ST")%>>ST</option>
							<option <%'=isSelected(trim(arrCodeCol7(ii)),"N/A")%>>N/A</option>	
							<option <%'=isSelected(trim(arrCodeCol7(ii)),"Q-NO")%>>Q-NO</option>
							<option <%'=isSelected(trim(arrCodeCol7(ii)),"Q-ND")%>>Q-ND</option>
							<option <%'=isSelected(trim(arrCodeCol7(ii)),"Q-DB")%>>Q-DB</option>
							<option <%'=isSelected(trim(arrCodeCol10(ii)),"Q-DB")%>>Q-MS</option>
							<option <%'=isSelected(trim(arrCodeCol7(ii)),"Q-SM")%>>Q-SM</option>
							<option <%'=isSelected(trim(arrCodeCol1(ii)),"Q-IY")%>>Q-IU</option>
							<option <%'=isSelected(trim(arrCodeCol7(ii)),"NBH")%>>NBH</option>
						</select>						
					</td>
					<td>
						<select class="blueSelect" name="codeCol8<%=i%>">
							<option></option>
							<option <%'=isSelected(trim(arrCodeCol8(ii)),"E")%>>ST-E</option>
							<option <%'=isSelected(trim(arrCodeCol8(ii)),"ST")%>>ST</option>
							<option <%'=isSelected(trim(arrCodeCol8(ii)),"N/A")%>>N/A</option>	
							<option <%'=isSelected(trim(arrCodeCol8(ii)),"Q-NO")%>>Q-NO</option>
							<option <%'=isSelected(trim(arrCodeCol8(ii)),"Q-ND")%>>Q-ND</option>
							<option <%'=isSelected(trim(arrCodeCol8(ii)),"Q-DB")%>>Q-DB</option>
							<option <%'=isSelected(trim(arrCodeCol10(ii)),"Q-DB")%>>Q-MS</option>
							<option <%'=isSelected(trim(arrCodeCol8(ii)),"Q-SM")%>>Q-SM</option>
							<option <%'=isSelected(trim(arrCodeCol1(ii)),"Q-IY")%>>Q-IU</option>
							<option <%'=isSelected(trim(arrCodeCol8(ii)),"NBH")%>>NBH</option>
						</select>						
					</td>
					<td>
						<select class="blueSelect" name="codeCol9<%=i%>">
							<option></option>
							<option <%'=isSelected(trim(arrCodeCol9(ii)),"E")%>>ST-E</option>
							<option <%'=isSelected(trim(arrCodeCol9(ii)),"ST")%>>ST</option>
							<option <%'=isSelected(trim(arrCodeCol9(ii)),"N/A")%>>N/A</option>	
							<option <%'=isSelected(trim(arrCodeCol9(ii)),"Q-NO")%>>Q-NO</option>
							<option <%'=isSelected(trim(arrCodeCol9(ii)),"Q-ND")%>>Q-ND</option>
							<option <%'=isSelected(trim(arrCodeCol9(ii)),"Q-DB")%>>Q-DB</option>
							<option <%'=isSelected(trim(arrCodeCol10(ii)),"Q-DB")%>>Q-MS</option>
							<option <%'=isSelected(trim(arrCodeCol9(ii)),"Q-SM")%>>Q-SM</option>
							<option <%'=isSelected(trim(arrCodeCol1(ii)),"Q-IY")%>>Q-IU</option>
							<option <%'=isSelected(trim(arrCodeCol9(ii)),"NBH")%>>NBH</option>
						</select>						
					</td>
					<td>
						<select class="blueSelect" name="codeCol10<%=i%>">
							<option></option>
							<option <%'=isSelected(trim(arrCodeCol10(ii)),"E")%>>ST-E</option>
							<option <%'=isSelected(trim(arrCodeCol10(ii)),"ST")%>>ST</option>
							<option <%'=isSelected(trim(arrCodeCol10(ii)),"N/A")%>>N/A</option>	
							<option <%'=isSelected(trim(arrCodeCol10(ii)),"Q-NO")%>>Q-NO</option>
							<option <%'=isSelected(trim(arrCodeCol10(ii)),"Q-ND")%>>Q-ND</option>
							<option <%'=isSelected(trim(arrCodeCol10(ii)),"Q-DB")%>>Q-DB</option>
							<option <%'=isSelected(trim(arrCodeCol10(ii)),"Q-DB")%>>Q-MS</option>
							<option <%'=isSelected(trim(arrCodeCol10(ii)),"Q-SM")%>>Q-SM</option>
							<option <%'=isSelected(trim(arrCodeCol1(ii)),"Q-IY")%>>Q-IU</option>
							<option <%'=isSelected(trim(arrCodeCol10(ii)),"NBH")%>>NBH</option>
						</select>						
					</td>
				</tr>
				<%ii = ii + 1
				next%>
				
			</table>
		</td>
	</tr>

	<tr><td height="20"></td></tr>
	<tr>
		<td>
			<select name="samplesCollectedInMonth">
				<option></option>
				<option>Yes</option>
				<option>No</option>
			</select>
			&nbsp;Sample(s) collected during this month?
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			<select name="exceedNTULimit">
				<option></option>
				<option>Yes</option>
				<option>No</option>
			</select>
			&nbsp;Exceedence of NTU limit
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			<select name="waterQualityMonitoringCompliance">
				<option></option>
				<option>Yes</option>
				<option>No</option>
			</select>
			&nbsp;Compliance with GTC policy and procedures for Water Quality Monitoring?
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			If no - why?<br />
			<textarea name="waterQualityMonitoringComplianceWhyNot" cols="30" rows="3"></textarea>
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			<select name="BMPInstalledMaintRetrieved">
				<option></option>
				<option>Yes</option>
				<option>No</option>
				<option selected="selected">N/A</option>
			</select>
			&nbsp;Were BMP's designed, installed and maintained when sample retrieved?
		</td>
	</tr>
	
	
	<tr><td height="20"></td></tr>
	<tr>
		<td>
			<strong>Impaired Streams</strong>
		</td>
	</tr>
	<tr><td height="10"></td></tr>
	<tr>
		<td>
			<select name="projectDrainUpstream">
				<option></option>
				<option <%=isSelected(rsMonthly("projectDrainUpstream"),"Yes")%>>Yes</option>
				<option <%=isSelected(rsMonthly("projectDrainUpstream"),"No")%>>No</option>
			</select>
			&nbsp;Does this project or a portion of this project drain within one mile upstream of an impaired stream segment?
		</td>
	</tr>
	
	<tr><td height="10"></td></tr>
	<tr>
		<td>
			<select name="projectDrainUpstreamYesOutlined">
				<option></option>
				<option <%=isSelected(rsMonthly("projectDrainUpstreamYesOutlined"),"Yes")%>>Yes</option>
				<option <%=isSelected(rsMonthly("projectDrainUpstreamYesOutlined"),"No")%>>No</option>
			</select>
			&nbsp;If Yes, were the additional BMPs and/or requirements implemented as outlined in the certified ES&PC Plan?
		</td>
	</tr>
	<tr><td height="10"></td></tr>
	<tr>
		<td>
			If no - Explain<br />
			<textarea name="projectDrainUpstreamExplain" cols="30" rows="3"><%=rsMonthly("projectDrainUpstreamExplain")%></textarea>
		</td>
	</tr>
	
	
	
	
	<tr><td height="20"></td></tr>
	<tr>
		<td>
			<strong>Daily Rainfall Data</strong>
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			<select name="hobo">
				<option></option>
				<option>Yes</option>
				<option>No</option>
			</select>
			&nbsp;iNet
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			<!--<select name="dataTable">
				<option></option>
				<option>Yes</option>
				<option>No</option>
			</select>
			&nbsp;Data Table-->
			<strong>Other</strong><br />
			<input type="text" name="dataTable" maxlength="50"/>
		</td>
	</tr>
	<tr><td height="3"></td></tr>
	<tr>
		<td>
			<select name="rainfallCollectedDaily">
				<option></option>
				<option>Yes</option>
				<option>No</option>
			</select>
			&nbsp;Was rainfall data collected daily?
		</td>
	</tr>
	<tr><td height="20"></td></tr>
	<tr>
		<td>
			1) Enter data in gray areas.<br />
			2) In blue boxes use the following codes.  Can use multiple codes to convey information.<br /><br />
			
			<table cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td align="right">
						<strong>ST-E</strong>&nbsp;
					</td>
					<td>
						Exceedence of NTU limit
					</td>
					<td width="30"></td>
					<td align="right">
						<strong>Q-NO</strong>&nbsp;
					</td>
					<td>
						Qualifying event- no sample due to no outlet 
					</td>
				</tr>
				<tr>
					<td align="right">
						<strong>ST</strong>&nbsp;
					</td>
					<td>
						Sample taken
					</td>
					<td width="30"></td>
					<td align="right">
						<strong>Q-ND</strong>&nbsp;
					</td>
					<td>
						Qualifying event - no discharge  
					</td>
				</tr>
				<tr>
					<td align="right">
						<strong>N/A </strong>&nbsp;
					</td>
					<td>
						Not a qualifying event for sampling
					</td>
					<td width="30"></td>
					<td align="right">
						<strong>Q-DB</strong>&nbsp;
					</td>
					<td>
						Qualifying event- no sample due to dead battery
					</td>
				</tr>
				<tr>
					<td align="right">&nbsp;
						<strong>NBH</strong>&nbsp;
					</td>
					<td>
						Non Business Hours / Holidays
					</td>
					<td width="30"></td>
					<td align="right">
						<strong>Q-SM</strong>&nbsp;
					</td>
					<td>
						Qualifying event- no sample due to sampler malfunction
					</td>
				</tr>
				<tr>
					<td align="right">&nbsp;
						<strong>Q-MS</strong>&nbsp;
					</td>
					<td>
						Qualifying event, missed sample
					</td>
					<td width="30"></td>
					<td align="right">
						<strong>Q-IU</strong>&nbsp;
					</td>
					<td>
						Qualifying event- no sample, area unsafe/impossible
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
