<%
On Error Resume Next
'to dos
'get the reportanswers
'get the responsiveactions
'update the report on the process.asp page

iReportID = request("reportID")


Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
'Create command for report
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetReport"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iReportID)
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsReport = oCmd.Execute
Set oCmd = nothing


'Create command for question category list
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCategories"'ByDivision
   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, rsReport("divisionID"))
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsCategories = oCmd.Execute
Set oCmd = nothing

'get the number of open action items
'Create command for question category list
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetOpenAIByProject"
   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, rsReport("projectID"))
   .CommandType = adCmdStoredProc
   
End With

Set rsAllOpen = oCmd.Execute
Set oCmd = nothing

%>
<script type="text/javascript">
<!--

function addEvent(ival,ques)
{
//alert (ival)
//ival = the letter
//ques = the question number
var ni = document.getElementById("myDiv"+ival);
//alert (ni)
var numi = document.getElementById("theValue"+ival);
var num = (document.getElementById("theValue"+ival).value -1)+ 2;
//alert (num)
numi.value = num;
var divIdName = "answer"+ival+num;
var newdiv = document.createElement('div');
newdiv.setAttribute("id",divIdName);
//newdiv.innerHTML = "Element Number "+num+" has been added! <a href=\"javascript:;\" onclick=\"removeEvent(\'"+divIdName+"\')\">Remove</a>";
//newdiv.innerHTML = "<table><tr><td>Corrective action "+ival+ "-" +num+"&nbsp;&nbsp;<a href=javascript:displayAnswerPicker('answer_R"+ival+ "-" +num+"@"+ques+"')>select action</a><br><input type=text name=answer_R"+ival+ "-" +num+"@"+ques+" size=40></td><td>Location"+ival+ "-" +num+"<br><input type=text name=location_"+ival+ "-" +num+" size=30></td><td> <a href=\"javascript:;\" onclick=\"removeEvent(\'"+divIdName+"\','"+ival+"\')\">Remove action</a></td></tr></table>";
newdiv.innerHTML = "<table><tr><td>Action item "+ival+ "-" +num+"&nbsp;&nbsp;<a href=javascript:displayAnswerPicker('answer_R"+ival+ "-" +num+"@"+ques+"')>select action</a><br><input type=text name=answer_R"+ival+ "-" +num+"@"+ques+" size=40></td><td>Location"+ival+ "-" +num+"<br><input type=text name=location_"+ival+ "-" +num+" size=30></td><!--<td><input type=checkbox name=installed"+ival+ "-" +num+"@"+ques+" />&nbsp;In Compliance</td>--><!--<td valign=bottom> <a href=\"javascript:;\" onclick=\"removeEvent(\'"+divIdName+"\','"+ival+"\')\">Remove action</a></td>--></tr><tr><td colspan=3>Proposed Activity<br><input type=text name=proposedActivity"+ival+ "-" +num+" size=60 /></td></tr><tr><td colspan=3>Comments<br><input type=text name=observations"+ival+ "-" +num+" size=60 /></td></tr><tr><td colspan=3>Date Installed<br><input type=text name=dateInstalled"+ival+ "-" +num+" size=10 />&nbsp;<a href=javascript:displayDatePicker('dateInstalled"+ival+ "-" +num+"')><img alt='Pick a date' src='images/date.gif' border=0 width=17 height=16></a></td></tr></table>";
//newdiv.innerHTML = "Responsive action needed "+ival+num+" <input type=text name=answer_"+ival+num+"@"+ques+" size=40> <a href=\"javascript:;\" onclick=\"removeEvent(\'"+divIdName+"\','"+ival+"\')\">Remove action</a>";
//alert (newdiv.innerHTML)
ni.appendChild(newdiv);

document.getElementById("answer_&"+ques+"2").checked = true

}

function removeEvent(divNum,ival)
{
//alert (divNum)
//alert (ival)
var d = document.getElementById("myDiv"+ival);
//alert (d)
var olddiv = document.getElementById(divNum);
//alert (document.getElementById(divNum))
d.removeChild(olddiv);
ival = ival - 1
}

function rept_onchange(addReport,repID) {
   window.location.href = 'form.asp?formType=addReport&reportType='+repID;
}

function dept_onchange(addReport,repID,divID) {
   window.location.href = 'form.asp?formType=addReport&division='+divID+'&reportType='+repID;
}

function proj_onchange(addReport,repID,divID,projID) {
   window.location.href = 'form.asp?formType=addReport&division='+divID+'&project='+projID+'&reportType='+repID;
}


function formControl(submitted) 
{
   if(submitted=="1") 
    {
   addReport.Submit.disabled=true
   document.addReport.submit();
  // alert("Thanks for your comments!")
    }
}
// -->
</script>
<form name="addReport" method="post" action="process.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sNPDESTitle%></span><span class="Header"> - Edit Report</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="800" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td>
												<table border="0" cellpadding="0" cellspacing="0">
													<tr>
														<td><img src="images/pix.gif" width="10" height="1"></td>
														<td valign="top"><strong>Report Type:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="hidden" name="reportType" value="<%=rsReport("reportTypeID")%>" />
															<%=rsReport("reportType")%>
														</td>
													</tr>
													<tr><td colspan="4"><img src="images/pix.gif" width="1" height="5"></td></tr>			
													<tr>
														<td><img src="images/pix.gif" width="10" height="1"></td>
														<td valign="top"><strong>Customer/Division:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="hidden" name="division" value="<%=rsReport("divisionID")%>" />
															<%=rsReport("division")%>
														</td>
													</tr>
													<tr><td colspan="4"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td></td>
														<td valign="top"><strong>Site/Proj. Name:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="hidden" name="project" value="<%=rsReport("projectID")%>" />
															<%=rsReport("projectName")%>
														</td>
													</tr>
													<tr><td colspan="4"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td></td>
														<td valign="top"><strong>Inspector:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%=rsReport("firstName") & " " & rsReport("lastName")%>
															<input type="hidden" name="inspector" value="<%=rsReport("userID")%>" />
														</td>
													</tr>
													<%if rsReport("reportTypeID") <> 25 then%>
													<!--<tr><td colspan="4"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td></td>
														<td valign="top"><strong>Contact Number:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="hidden" name="contactNumber" value="<%'=rsReport("contactNumber")%>"/>
															<%'=rsReport("contactNumber")%>
														</td>
													</tr>-->
													<tr><td colspan="4"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td></td>
														<td valign="top"><strong>Inspection Type:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="hidden" name="inspectionType" value="<%=rsReport("inspectionTypeID")%>" />
															<%=rsReport("inspectionType")%>
														</td>
													</tr>
													<!--<tr><td colspan="4"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td></td>
														<td valign="top"><strong>Project Status:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="hidden" name="projectStatus" value="<%'=rsReport("projectStatusID")%>" />
															<'%=rsReport("projectStatus")%>
														</td>
													</tr>-->
													<%end if%>
													
													
													<tr><td colspan="4"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td></td>
														<td valign="top"><strong>Inspection Date:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
														
														
															<%if rsReport("reportTypeID") = 25 or rsReport("reportTypeID") = 3 then%>
																
																<input type="text" name="inspectionDate" maxlength="10" size="10" value="<%=rsReport("inspectionDate")%>"/>&nbsp;<a href="javascript:displayDatePicker('inspectionDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
														
															<%else%>
																
																<script type="text/javascript">
																<!--
																
																function checkBackdate() {
																   
																   //Set the two dates
																	var currentTime = new Date();
																	var month = currentTime.getMonth() + 1;
																	var day = currentTime.getDate();
																	var year = currentTime.getFullYear();
																	var currDate = month + "/" + day + "/" + year;
																
																	
																	//get the date from the textbox
																	var dateEntered = document.addReport.inspectionDate.value;
																	
																	
																	var date1 = new Date(currDate);
																	var date2 = new Date(dateEntered);
																	
																	//var numDays = DateDiff(currDate, dateEntered);
																	//alert(date1);
																	//alert(date2);
																	
																	
																	var timeDiff = Math.abs(date1.getTime() - date2.getTime());
																	var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
																	//alert(diffDays);
																	
																	
																	if (diffDays > 3){
																		alert("You are not allowed to back date or post date a report more than 3 days.");
																		return false;
																	}
																	
																	//Math.ceil((christmas.getTime()-today.getTime())/(one_day)
	
																																
																}
																// -->
																</script>
														
																<input type="text" name="inspectionDate" maxlength="10" size="10" value="<%=rsReport("inspectionDate")%>" onblur="checkBackdate()"/>&nbsp;<a href="javascript:displayDatePicker('inspectionDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
															<%end if%>
														</td>
													</tr>
													
													
													
													<%if rsReport("reportTypeID") <> 2 then
														if rsReport("reportTypeID") <> 3 then
														if rsReport("reportTypeID") <> 7 then
														if rsReport("reportTypeID") <> 25 then%>
															<tr><td colspan="4"><img src="images/pix.gif" width="1" height="5"></td></tr>
															<tr>
																<td></td>
																<td valign="top"><strong>Weather Type:</strong></td>
																<td><img src="images/pix.gif" width="5" height="1"></td>
																<td>
																	<input type="hidden" name="weatherType" value="<%=rsReport("weatherID")%>" />
																	<%=rsReport("weatherType")%>
																</td>
															</tr>	
														<%end if
														end if
														if rsReport("reportTypeID") <> 25 then%>			
															<tr><td colspan="4"><img src="images/pix.gif" width="1" height="5"></td></tr>
															<tr>
																<td></td>
																<td valign="top"><span class="required">*</span> <strong>Rainfall Amount:</strong></td>
																<td><img src="images/pix.gif" width="5" height="1"></td>
																<td>
																	<input type="text" name="rainfallAmount" size="2" value="<%=formatnumber(rsReport("rainfallAmount"),1)%>"/> inches (please enter numbers only)
																</td>
															</tr>
														<%end if%>
														<%if rsReport("reportTypeID") = 7 then%>
														<tr><td colspan="4"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td></td>
															<td valign="top"><span class="required">*</span> <strong>Rainfall Source:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td>
																<%if request("rainfallSource") <> "" then
																	rainfallSource = trim(request("rainfallSource"))
																else
																	rainfallSource = trim(rsReport("rainfallSource"))
																end if%>
																<select name="rainfallSource">
																	<option value=""></option>
																	<option value="iNet" <%=isSelected(rainfallSource,"iNet")%>>iNet</option>
																	<option value="Sampler" <%=isSelected(rainfallSource,"Sampler")%>>Sampler</option>
																	<option value="Rain gauge" <%=isSelected(rainfallSource,"Rain gauge")%>>Rain gauge</option>
																</select>
															</td>
														</tr>
														<tr><td colspan="4"><img src="images/pix.gif" width="1" height="5"></td></tr>
														<tr>
															<td></td>
															<td valign="top"><strong>Soil Stabilization<br />Comments:</strong></td>
															<td><img src="images/pix.gif" width="5" height="1"></td>
															<td valign="top">
																<%if request("soilStabilizationComments") <> "" then
																	soilStabilizationComments = trim(request("soilStabilizationComments"))
																else
																	soilStabilizationComments = trim(rsReport("soilStabilizationComments"))
																end if%>
																<textarea name="soilStabilizationComments" rows="3" cols="30"><%=soilStabilizationComments%></textarea>
															</td>
														</tr>
														<%end if%>
														
														<%if rsReport("reportTypeID") = 14 or rsReport("reportTypeID") = 15 then%>
															<!--<tr><td colspan="4"><img src="images/pix.gif" width="1" height="5"></td></tr>
															<tr>
																<td></td>
																<td valign="top"><strong>Personnel Present:</strong></td>
																<td><img src="images/pix.gif" width="5" height="1"></td>
																<td>
																	<input type="hidden" name="personnelPresent" value="<%'=rsReport("personnelPresent")%>" />
																	<%'=rsReport("personnelPresent")%>
																</td>
															</tr>	-->
														<%end if%>
														<%if rsReport("reportTypeID") <> 7 then
														if rsReport("reportTypeID") <> 25 then%>
															<tr><td colspan="4"><img src="images/pix.gif" width="1" height="5"></td></tr>
															<tr>
																<td></td>
																<td valign="top"><strong>% compliant:</strong></td>
																<td><img src="images/pix.gif" width="5" height="1"></td>
																<td>
																	<a href="#" onClick="MM_openBrWindow('openItems.asp?projectID=<%=rsReport("projectID")%>','popPrint','scrollbars=yes,resizable=yes,width=1000,height=600')">view open items</a>&nbsp;&nbsp;<br />
																	<%=rsAllOpen("openAI")%> total open items<br />
																	<%=rsAllOpen("closedAI")%> total corrected items<br />
																	<%
																		iTotalItems = rsAllOpen("closedAI") + rsAllOpen("openAI")
																		iTotalComplete = (rsAllOpen("closedAI")/iTotalItems) * 100
																	if iTotalComplete <> "" then
																		response.Write "<b>" & round(iTotalComplete) & "% corrected</b>"
																	end if
																	%>
																</td>
															</tr>
															<!--<tr><td colspan="4"><img src="images/pix.gif" width="1" height="5"></td></tr>
																<tr>
																	<td></td>
																	<td valign="top"><strong>Lot management:</strong></td>
																	<td><img src="images/pix.gif" width="5" height="1"></td>
																	<td>
																		<a href="#" onClick="MM_openBrWindow('openLots.asp?ID=<%'=rsReport("projectID")%>','popPrint','scrollbars=yes,resizable=yes,width=1000,height=600')">view open/closed lots</a>&nbsp;&nbsp;<br />
																	</td>
																</tr>-->
													<%end if
													end if
													end if
													end if%>
													<tr><td colspan="4"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td></td>
														<td valign="top"><strong>Comments:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<textarea name="comments" rows="3" cols="30"><%=rsReport("comments")%></textarea>
														</td>
													</tr>
													<tr><td colspan="4"><img src="images/pix.gif" width="1" height="30"></td></tr>				
												</table>
												<table width="100%" border="0" cellpadding="0" cellspacing="0">
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td valign="top" colspan="3">
																<%
																'get the report type to pass in
																'make dynamic?
																iReportTypeID = rsReport("reportTypeID")
																
																Select Case iReportTypeID
																	Case 1
																		sType = "spGetWeeklyQuestion"
																	'Case 3
																	'	sType = "spGetMonthlyQuestionandAnswers"
																	Case 4
																		sType = "spGetPostRainQuestion"
																	Case 8
																		sType = "spGetBiWeeklyQuestion"	
																	Case 9
																		sType = "spGetAnnualQuestion"
																	Case 13
																		sType = "spGetPetroleumQuestionandAnswers"	
																	Case 14 
																		sType = "spGetUtilBWPRQuestion"
																	Case 15
																		sType = "spGetUtilBWPRQuestion"	
																	Case 21
																		sType = "spGetWeeklyQuestion"	
																	Case 25
																		sType = ""	
																	Case else
																		sType = "spGetWeeklyQuestion"					
																end Select
										
													
																'display the report type
																'if it is daily display the monthly log
																'else display the questions
																Select case iReportTypeID
																	case 2%>
																		<!--#include file="inc_edit_form_daily.asp"-->
																		
																	<%Case 3 'monthly report%>
																		<!--#include file="inc_edit_form_monthly.asp"-->
																	
																	<% case 7 'water sampling%>
																		<!--#include file="inc_edit_form_WaterSampling.asp"-->
																		
																	<%case 8 'bi weekly report%>	
																		<!--#include file="inc_edit_form_biWeekly.asp"-->
																			
																	<%case 10 'erosion control checklist%>	
																		<!--#include file="inc_edit_form_ecChecklist.asp"-->
																		
																	<%Case 12 'GDOT daily report%>
																		<!--#include file="inc_edit_form_GDOTDaily.asp"-->
																		
																	<%case 13 'petroleum%>	
																		<!--#include file="inc_edit_form_Petroleum.asp"-->
																	
																	<%case 14 'utility bi weekly%>	
																		<!--#include file="inc_edit_form_UtilBWPR.asp"-->
																		
																	<%case 15 'utility post-rain%>	
																		<!--#include file="inc_edit_form_UtilBWPR.asp"-->
																		
																	<%case 16 'utility daily%>	
																		<!--#include file="inc_edit_form_UtilDaily.asp"-->
																	<%case 21 'GDOT monthly%>	
																		<!--#include file="inc_edit_form_Weekly.asp"-->
																	<%case 25 'EPD monthly%>	
																		<!--#include file="inc_edit_form_FinalMonthly.asp"-->
																										
																	<%case else%>
																		<!--#include file="inc_edit_form_Weekly.asp"-->
																		
									
																<%end select%>
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td colspan="3" align="right">
															<input type="hidden" name="reportID" value="<%=iReportID%>" />
															<input type="hidden" name="projectID" value="<%=rsReport("projectID")%>" />
															<!--<input type="hidden" name="customer" value="<%'=rsReport("customerID")%>" />-->
															<%select case iReportTypeID
																case 2 'daily%>
																	<input type="hidden" name="processType" value="editDaily" />
																<%case 3 'monthly%>
																	<input type="hidden" name="processType" value="editMonthly" />
																<%case 7 'water sampling%>
																	<input type="hidden" name="processType" value="editWaterSampling" />
																<%case 8 'bi weekly%>
																	<input type="hidden" name="processType" value="editBiWeekly" />
																<%case 10 'EC checklist%>
																	<input type="hidden" name="processType" value="editECChecklist" />
																<%case 12 'GDOT Daily%>
																	<input type="hidden" name="processType" value="editGDOTDaily" />
																<%case 13 'Petroleum%>
																	<input type="hidden" name="processType" value="editPetroleum" />
																<%case 14 'utility bi weekly%>
																	<input type="hidden" name="processType" value="editUtilBWPR" />
																<%case 15 'utility post rain%>
																	<input type="hidden" name="processType" value="editUtilBWPR" />
																<%case 16 'utility post rain%>
																	<input type="hidden" name="processType" value="editUtilDaily" />
																<%case 21 'GDOT Monthly%>
																	<input type="hidden" name="processType" value="editReport" />
																<%case 25 'monthly Final%>
																		<input type="hidden" name="processType" value="editMonthlyFinal" />
																<%case else 'others%>
																	<input type="hidden" name="processType" value="editReport" />
															<%end select%>
															
															
															<%if rsReport("reportTypeID") <> 25 then%> 
																<input type="submit" name="Submit" value="  Save  " class="formButton" onclick="return checkBackdate();"/>&nbsp;&nbsp;
															<%else%>
																<input type="submit" name="Submit" value="  Save  " class="formButton" onClick="formControl(1)"/>&nbsp;&nbsp;
															<%end if%>
														</td>
													</tr>
												</table>
											</td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
										</tr>
									</table>
									
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<script language="JavaScript" type="text/javascript">

  var frmvalidator  = new Validator("addReport");
  
  
  frmvalidator.addValidation("inspectionDate","req","Please enter the inspection date");
  <%if iReportTypeID <> 2 then
	  if iReportTypeID <> 3 then
	  	if iReportTypeID <> 7 then%>
		  frmvalidator.addValidation("rainfallAmount","req","Please enter the rainfall amount");
		  //frmvalidator.addValidation("rainfallAmount","numeric");
	  <%end if
	  end if
	  end if
	  if iReportTypeID = 7 then%>
	  	//  frmvalidator.addValidation("sampleNumber","req","Please enter the sample number");
	    // frmvalidator.addValidation("sampleNumber","numeric");
	  	//  frmvalidator.addValidation("dateSampleTaken","req","Please enter the date the sample was taken");
	  	//  frmvalidator.addValidation("exactLocation","req","Please enter the exact location the sample was taken");
		//  frmvalidator.addValidation("city","req","Please enter the city the sample was taken");
		//  frmvalidator.addValidation("county","req","Please enter the county the sample was taken");
		//  frmvalidator.addValidation("sampledBy","req","Please enter the person who took the sample");
	<%end if
	
	if iReportTypeID = 8 then%>
	  	  
	  	  frmvalidator.addValidation("districtOffice","req","Please enter the district office");
	  	  frmvalidator.addValidation("EPDDivision","req","Please enter the EPD division");
		  frmvalidator.addValidation("address","req","Please enter the EPD division address");		  
		  frmvalidator.addValidation("city","req","Please enter the EPD division city");
		  frmvalidator.addValidation("zip","req","Please enter the EPD division zip code");
		  frmvalidator.addValidation("reportCovering","req","Please enter the dates the report covers");		  
		  frmvalidator.addValidation("county","req","Please enter the county");
		  frmvalidator.addValidation("observation1","req","Please enter your observations");
		  frmvalidator.addValidation("observation2","req","Please enter your observations");
	<%end if%>
</script>
<%
rsReport.close
set rsReport = nothing
rsCategories.close
set rsCategories = nothing
rsQuestion.close
set rsQuestion = nothing

DataConn.close
set DataConn = nothing
%>
