<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->

<%
projectID = request("projectID")
Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetAssignedProjectsAdmin"
   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, session("ID"))
   .CommandType = adCmdStoredProc   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsProjects = oCmd.Execute
Set oCmd = nothing

If projectID <> "" then
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
	
	sState = request("state")
	
	'If Session("Admin") = "True" Then
	'	if sState = "Archived" then
	'		sp = "spGetPrimaryListArchiveAll" 
	'	else
	'		sp = "spGetPrimaryListAll"
	'	end if
	'else
		if sState = "Archived" then
			sp = "spGetPrimaryListArchive" 
		else
			sp = "spGetPrimaryList"
		end if
	'End If
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = sp
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, projectID)
	   .CommandType = adCmdStoredProc
	   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
				
	Set rs = oCmd.Execute
	Set oCmd = nothing
end if
%>


<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script language="javascript" src="includes/mm.js"></script>
<script language="JavaScript">
<!--
function confirmDelete(delUrl) {
 if (confirm("Are you sure you wish to delete this form and the corresponding PDF? This will also delete the fee form associated with it.")) {
    document.location = delUrl;
  }


}

function dept_onchange(primaryList) {
   primaryList.submit(); 
}
//-->
</script>
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sNOITitle%></span><span class="Header"> - Primary Permittee List</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<%if sState = "Archived" then%>
							<a href="primaryList.asp?projectID=<%=projectID%>" class="footerLink">Active</a><br>
						<%else%>
							<a href="primaryList.asp?state=Archived&projectID=<%=projectID%>" class="footerLink">Archived</a><br>
						<%end if%>			
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td colspan="11">
												<!--get a dropdown of all of the assigned projects for the user-->
													Please select a project to view NOTICE OF INTENTS.<br>
													<form name="primaryList" method="post" action="primaryList.asp">
														<select name="projectID" onChange="return dept_onchange(primaryList)">
															<option value="">--Select Project--</option>
															<%do until rsProjects.eof
															if trim(rsProjects("projectID")) = trim(ProjectID) then%>
																<option selected="selected" value="<%=rsProjects("projectID")%>"><%=rsProjects("projectName")%></option>
															<%else%>
																<option value="<%=rsProjects("projectID")%>"><%=rsProjects("projectName")%></option>
															<%end if
															rsProjects.movenext
															loop
															rsProjects.close
															set rsProjects = nothing%>
														</select><br /><br />
													</form>
											</td>
										</tr>
										<tr bgcolor="#666666">
											<td>&nbsp;&nbsp;<span class="searchText">Site Project Name</span></strong></td>
											<td align="center"><span class="searchText">Primary NOI</span></td>					
											<td align="center"><span class="searchText">NPDES Fee Form</span></strong></td>
											<td align="center"><span class="searchText">EPD Office</span></td>
											<!--<td align="center"><span class="searchText">Secondary NOI</span></td>-->										
											<td align="center"><span class="searchText">NOT Form</span></td>
											<td align="center"><span class="searchText">Email</span></td>
											<%if sState = "" Then%>
											<td><span class="searchText">Archive</span></td>
											<%else%>
											<td><span class="searchText">Activate</span></td>
											<%end if%>
										</tr>
										<tr><td colspan="11"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<%If projectID <> "" then
											If rs.eof then%>
												<tr><td colspan="11">there are no records to display</td></tr>
											<%else
												blnChange = true
												Do until rs.eof
													If blnChange = true then%>
														<tr class="rowColor">
													<%else%>
														<tr>
													<%end if
													
													docID = rs("primaryFormID")
													
													%>
														<td>&nbsp;&nbsp;<%=rs("siteProjectName")%></td>
														<td align="center"><a href="form.asp?id=<%=rs("primaryFormID")%>&formType=editPrimary&projectID=<%=projectID%>"><img src="images/edit.gif" width="19" height="19" alt="edit" border="0"></a>&nbsp;&nbsp;<a href="downloads/NOI/NOIPrimary_<%=docID%>.pdf" target="_blank"><img src="images/pdf.gif" width="16" height="16" alt="pdf" border="0"></a></td>
														<%if rs("feeFormID") <> "" Then%>
														<td align="center"><a href="form.asp?formType=editFeeForm&id=<%=rs("primaryFormID")%>&projectID=<%=projectID%>"><img src="images/edit.gif" width="19" height="19" alt="edit" border="0"></a>&nbsp;&nbsp;<a href="downloads/NOI/npdesFeeForm_<%=docID%>.pdf" target="_blank"><img src="images/pdf.gif" width="16" height="16" alt="pdf" border="0"></a></td>
														<%else%>
														<td align="center"><a href="form.asp?formType=addFeeForm&id=<%=rs("primaryFormID")%>&projectID=<%=projectID%>">Add Form</a></td>
														<%end if%>
														<td align="center"><a href="#" onClick="MM_openBrWindow('officeLocation.asp?id=<%=rs("officeID")%>','popPrint','scrollbars=yes,resizable=yes,width=440,height=380')"><img src="images/open.gif" width="20" height="20" border="0" alt="open office location"></a></td>
														<!--<td align="center"><a href="secondaryList.asp?primaryID=<%'=rs("primaryFormID")%>&projectID=<%'=projectID%>">list</a></td>-->
														<%if rs("notFormID") <> "" Then%>
														<td align="center"><a href="form.asp?formType=editNOTForm&id=<%=rs("primaryFormID")%>&notid=<%=rs("notFormID")%>&projectID=<%=projectID%>"><img src="images/edit.gif" width="19" height="19" alt="edit" border="0"></a>&nbsp;&nbsp;<a href="downloads/NOI/noticeOfTermination_<%=docID%>.pdf" target="_blank"><img src="images/pdf.gif" width="16" height="16" alt="pdf" border="0"></a></td>
														<%else%>
														<td align="center"><a href="form.asp?formType=addNOTForm&id=<%=rs("primaryFormID")%>&projectID=<%=projectID%>">Add Form</a></td>
														<%end if%>
														<td align="center"><a href="form.asp?id=<%=rs("primaryFormID")%>&formType=sendNOIEmail&type=Primary&docID=<%=docID%>&projName=<%=rs("siteProjectName")%>&projectID=<%=projectID%>"><img src="images/email.gif" width="20" height="20" alt="email" border="0"></a></td>
														<!--<td align="center"><input type="image" src="images/remove.gif" width="11" height="13" alt="delete" border="0" onClick="return confirmDelete('process.asp?id=<%'=rs("primaryFormID")%>&processType=deletePrimary')"></td>-->
														<%if rs("notFormID") <> "" Then%>
															<%if sState = "" Then%>
															<td><a href="processNOI.asp?primaryID=<%=rs("primaryFormID")%>&set=0&fType=primary&processType=setArchive&projectID=<%=projectID%>">archive</a></td>
															<%else%>
															<td><a href="processNOI.asp?primaryID=<%=rs("primaryFormID")%>&set=1&fType=primary&processType=setArchive&projectID=<%=projectID%>">activate</a></td>
															<%end if%>
														<%else%>
															<td>&nbsp;</td>
														<%end if%>
													</tr>
												<%rs.movenext
												if blnChange = true then
													blnChange = false
												else
													blnChange = true
												end if
												loop
											end if
										else%>
											<tr><td colspan="11">please choose a project from the above list</td></tr>
										<%end if%>
									</table>
									
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>