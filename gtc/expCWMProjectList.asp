<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
customerID = Request("customerID")
divisionID = Request("divisionID")

Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If


On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")
	
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCWMProjectsByDivision"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, divisionID)
   .CommandType = adCmdStoredProc   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rs = oCmd.Execute
Set oCmd = nothing


Response.ContentType = "application/vnd.ms-excel"
Response.AddHeader "Content-Disposition", "attachment; filename=CWMProjectList.xls" 
if rs.eof <> true then%>
		<table border=1>
			<tr>
				<td><strong>Project Name</strong></td>				
				<td><strong>Last Report Date</strong></td>
			</tr>
	<%do until rs.eof%>
		<tr>
			<td><%=rs("projectName")%></td>
			<td><%=rs("lastInspectionDate")%></td>
		</tr>
	<%
	rs.movenext
	loop%>
	</table>
<%end if

rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing%>

