<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
clientID = request("clientID")
smonth = request("month")
syear = request("year")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

if clientID = 1 then 'get NEXT and McBride Accounts. this is a special circumstance
	'get a list of the customers based on the clientID
	Set oCmd = Server.CreateObject("ADODB.Command")
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetCustomersByClientSpecial"
	   '.parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
	   .CommandType = adCmdStoredProc   
	End With
else
	'get a list of the customers based on the clientID
	Set oCmd = Server.CreateObject("ADODB.Command")
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetCustomersByClient"
	   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
	   .CommandType = adCmdStoredProc   
	End With
end if
Set rsReport = oCmd.Execute
Set oCmd = nothing

Response.ContentType = "application/vnd.ms-excel"
Response.AddHeader "Content-Disposition", "attachment; filename=billingReconciliationReport.xls" 
%>

<!--start of content for the page-->
<table border="1">
	<tr>
		<td colspan="7">
			<table width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td colspan="7">BILLING RECONCILIATION REPORT</td>
				</tr>
				<tr bgcolor="#666666">
					<td><font color="#FFFFFF">Customer</font></td>
					<td><font color="#FFFFFF">Project</font></td>
					<td align="center"><font color="#FFFFFF">Bill Rate</font></td>
					<td align="center"><font color="#FFFFFF">1st Inspection Date</font></td>
					<td align="center"><font color="#FFFFFF">*Last Inspection Date</font></td>
					<td align="center"><font color="#FFFFFF">Project End Date</font></td>
					<td align="center"><font color="#FFFFFF">Active</font></td>
				</tr>
				<%if rsReport.eof then%>
					<tr bgcolor="#DDDDDD"><td colspan="7">There are no result for the items you chose. Please select again.</td></tr>
				<%else
				iBillTotal = 0
				do until rsReport.eof%>
						<tr bgcolor="#DDDDDD">
							<td colspan="7"><b><%=rsReport("customerName")%></b></td>
						</tr>
						<tr bgcolor="#DDDDDD">
							<td colspan="7">
								<%if rsReport("advanceBilling") = "True" then%>
									<em>***Billing in Advance***</em><br>
								<%else%>
									<font color="#EE0000"><em>***Billing in Arrears***</em></font><br>
								<%end if%>
								<%if rsReport("billName") <> "" then%>
									<%=rsReport("billName")%><br>
								<%end if%>
								<%if rsReport("billEmail") <> "" then%>
									<a href="mailto:<%=rsReport("billEmail")%>"><%=rsReport("billEmail")%></a><br>
								<%end if%>
								<%if rsReport("billPhone") <> "" then%>
									<%=rsReport("billPhone")%><br>
								<%end if%>
								<%if rsReport("billStreet") <> "" then%>
									<%=rsReport("billStreet")%><br>
								<%end if%>
								<%if rsReport("billSuite") <> "" then%>
									Suite&nbsp;<%=rsReport("billSuite")%><br>
								<%end if%>
								<%if rsReport("billCity") <> "" then%>
									<%=rsReport("billCity")%>,&nbsp;<%=rsReport("billState")%>&nbsp;<%=rsReport("billZip")%><br>
								<%end if%>																	
								<%if rsReport("billMethodFax") = "True" then%>
									<strong>Fax</strong><br>
								<%end if%>
								<%if rsReport("billMethodEmail") = "True" then%>
									<strong>Email</strong><br>
								<%end if%>
								<%if rsReport("billMethodMail") = "True" then%>
									<strong>Mail</strong><br>
								<%end if%>
								<%if rsReport("acctManager") <> "" then%>
									<strong>Acct Mgr:</strong> <%=rsReport("acctManager")%><br>
								<%end if%>
							</td>
						</tr>
						<%
						'get the projects associated with this customer
						Set oCmd = Server.CreateObject("ADODB.Command")
						With oCmd
						   .ActiveConnection = DataConn
						   .CommandText = "spGetProjectandDivisionByCustomer"
						   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, rsReport("customerID"))
						   .CommandType = adCmdStoredProc   
						End With
						
						Set rsProject = oCmd.Execute
						Set oCmd = nothing
						
						
						if rsProject.eof then%>
							<tr bgcolor="#C4E0FF"><td colspan="7">There are no projects listed for this customer</td></tr>
						<%else
							do until rsProject.eof%>
								<tr bgcolor="#C4E0FF">
									<td></td>
									<td><%=rsProject("projectName")%></td>
									<td align="center">
										<%
										
											if rsProject("endDate") <> "" then
												'zero out the bill rate
												iBillRate = 0
											else
												iBillRate = rsProject("billRate")
											end if
											response.Write formatCurrency(iBillRate,2)										
											
											if isnull(iBillRate) = false then
												if iBillRate <> "" then
													'if the project is inactive, don't add to the total
													If rsProject("isActive") = "True" then
														iBillTotal = iBillTotal + iBillRate
													end if
												end if
											end if
										%>
									</td>
									<td align="center"><%=rsProject("firstInspectionDate")%></td>
									<td align="center">
										<%If datediff("d",rsProject("lastInspectionDate"),date()) > 10 then																			
											response.Write "<b><font color=red>* " & rsProject("lastInspectionDate") & "</font>"
										end if
										%>
									</td>
									<td align="center"><%=rsProject("endDate")%></td>
									<td align="center">
										<%'=rsProject("isActive")%>
										<%if rsProject("isActive") = "True" then%>
											Yes
										<%end if%>
									</td>
								</tr>
							<%
							rsProject.movenext
							loop
						end if%>
						
						<tr><td colspan="7"></td></tr>
						
						<%
				
				rsReport.movenext
							
				loop
				rsReport.close
				set rsReport = nothing
				end if%>
				
				<tr>
					<td></td>
					<td align="right"><strong>Total:</strong>&nbsp;</td>
					<td align="left" colspan="5"><%=formatcurrency(iBillTotal,2)%></td>
				</tr>
			</table>
		</td>
	</tr>
	
</table>
						
<!--end of content for the page-->

<%
'rsCustomer.close
rsReport.close
DataConn.close
Set rsCustomer = nothing
Set rsReport = nothing
Set DataConn = nothing
Set oCmd = nothing
%>