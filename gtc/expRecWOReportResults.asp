<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
iDivisionID = request("division")
iProjectID = request("projectID")
openClosed = request("openClosed")
clientID = request("clientID")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

if iProjectID = "All" then
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetWorkOrdersByDivision" 'by division
	   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, iDivisionID)
	   .parameters.Append .CreateParameter("@isClosed", adBoolean, adParamInput, 1, openClosed)
	   .CommandType = adCmdStoredProc   
	End With
				
	Set rsReport = oCmd.Execute
	Set oCmd = nothing
	
else
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetWorkOrdersByProject" 'by project
	   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, iProjectID)
	   .parameters.Append .CreateParameter("@isClosed", adBoolean, adParamInput, 1, openClosed)
	   .CommandType = adCmdStoredProc   
	End With
				
	Set rsReport = oCmd.Execute
	Set oCmd = nothing
end if


Response.ContentType = "application/vnd.ms-excel"
Response.AddHeader "Content-Disposition", "attachment; filename=workOrderReconciliationReport.xls" 
if rsReport.eof <> true then%>
		<table border=1>
			<tr>
				<td><strong>Work Order ID</strong></td>
				<td><strong>Created By</strong></td>
				<td><strong>Division</strong></td>
				<td><strong>Project</strong></td>
				<td><strong>Work Order Date</strong></td>
				<td><strong>Work Order Amount</strong></td>
			</tr>
	<%dim i
	i = 0
	do until rsReport.eof%>
		<tr>
			<td><%=rsReport("workOrderID")%></td>
			<td><%=rsReport("firstName")%>&nbsp;<%=rsReport("lastName")%></td>
			<td><%=rsReport("division")%></td>
			<td><%=rsReport("projectName")%></td>
			<td><%=rsReport("dateAdded")%></td>
			<td>
				<%
				'get the total amount of the work order for this field
				Set oCmd = Server.CreateObject("ADODB.Command")
				With oCmd
				   .ActiveConnection = DataConn
				   .CommandText = "spGetWorkCost"
				   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, rsReport("workOrderID"))
				   .CommandType = adCmdStoredProc   
				End With
							
				Set rsCost = oCmd.Execute
				Set oCmd = nothing
				
				response.Write formatcurrency(rsCost("cost"),2)
				%>
			</td>
		</tr>
	<%
	rsReport.movenext
	i = i + 1
	loop%>
	</table>
<%end if

rsReport.close
DataConn.close
Set rsCustomer = nothing
Set rsReport = nothing
Set DataConn = nothing
Set oCmd = nothing%>

