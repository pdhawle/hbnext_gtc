<script language="javascript">
<!--
function addLines() {
   document.addReport.action = "form.asp?formType=addReport#jumpto";
   addReport.submit(); 
}

//-->
</script>
<%
iDCount = request("count")
if iDCount = "" then
	iDCount = 5
end if


'get the division based on the project
Set oCmd = Server.CreateObject("ADODB.Command")
		
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetDivisionByProject"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iProjectID)
   .CommandType = adCmdStoredProc
   
End With
	
Set rsDivision = oCmd.Execute
Set oCmd = nothing

'get the latest EC report
Set oCmd = Server.CreateObject("ADODB.Command")
		
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetLatestECReport"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iProjectID)
   .CommandType = adCmdStoredProc
   
End With
	
Set rsLast = oCmd.Execute
Set oCmd = nothing

'get the ECChecklist 
'from all the other reports for this project
Set oCmd = Server.CreateObject("ADODB.Command")
		
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetECChecklists"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iProjectID)
   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, rsLast("reportID"))
   .CommandType = adCmdStoredProc
   
End With
	
Set rsECChecklist = oCmd.Execute
Set oCmd = nothing



%>
<table class="borderTable" width="1200" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td><br />
			<%
			If request("reportNumber") <> "" then
				iRepNum = request("reportNumber")
			else
				'if there is no report number in the previous report, get the total number and post here
				'get the report number from the previous report
				
				Set oCmd = Server.CreateObject("ADODB.Command")
			
				With oCmd
				   .ActiveConnection = DataConn
				   .CommandText = "spGetLatestReportNumber"
				   .parameters.Append .CreateParameter("@reportTypeID", adInteger, adParamInput, 8, iReportTypeID)
				   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, iProjectID)
				   .CommandType = adCmdStoredProc
				   
				End With
					
				Set rsReportNumber = oCmd.Execute
				Set oCmd = nothing
				
				if isnull(rsReportNumber) then
					'get the count of the report previously entered
					Set oCmd = Server.CreateObject("ADODB.Command")
		
					With oCmd
					   .ActiveConnection = DataConn
					   .CommandText = "spGetECChecklistCount"
						.parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, iProjectID) 'reportID
					   .CommandType = adCmdStoredProc
					   
					End With							
					Set rsCount = oCmd.Execute
					Set oCmd = nothing
					
					iRepNum = rsCount("reportNumber")
				else
					'set the report number to be the last in the DB
					iRepNum = rsReportNumber("reportNumber")
				end if
			end if
			%>
			&nbsp;&nbsp;<strong>Report Number:</strong>&nbsp;<input type="text" name="reportNumber" size="3" value="<%=iRepNum%>"/>&nbsp;this is the number that will be displayed at the top of the report
		</td>
	</tr>
	<tr>
		<td><br />
			<%if request("disturbedAcreage") <> "" then
				dAcreage = request("disturbedAcreage")
			else
				dAcreage = 0
			end if%>
			&nbsp;&nbsp;<strong>Disturbed Acreage:</strong>&nbsp;<input type="text" name="disturbedAcreage" size="3" value="<%=dAcreage%>"/>
		</td>
	</tr>
	<tr>
		<td><br />
			&nbsp;&nbsp;Number of rows to add: <input type="text" name="count" size="5" value="<%=iDCount%>" />&nbsp;&nbsp;<input type="button" name="refresh" value="Refresh" class="formButton" onclick="return addLines()"/><br />
			&nbsp;&nbsp;If you need more lines, add to the number displayed.
		</td>
	</tr>
	<tr><td><img src="images/pix.gif" width="1" height="20"></td>
	<tr>
		<td>
		&nbsp;<select name="checklistType">
			<%if trim(request("checklistType")) = "Weekly Inspection" then%>
				<option selected="selected" value="Weekly Inspection">Regular Inspection</option>
			<%else%>
				<option value="Weekly Inspection">Weekly Inspection</option>
			<%end if%>
			<%if trim(request("checklistType")) = "Post Storm Event Inspection" then%>
				<option selected="selected" value="Post Storm Event Inspection">Post Storm Event Inspection</option>
			<%else%>
				<option value="Post Storm Event Inspection">Post Storm Event Inspection</option>
			<%end if%>
		</select>
		<!--<input type="radio" name="period" <%'=requestIsChecked(request("periodWeekly"))%> />&nbsp;Regelar Inspection</td>-->
	</tr>
	<!--<tr>
		<td><input type="radio" name="period" <%'=requestIsChecked(request("periodHour"))%> />&nbsp;Post</td>
	</tr>-->
	<tr><td><img src="images/pix.gif" width="1" height="20"></td>	
	<tr>
		<td>
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<a name="jumpto"></a><tr bgcolor="#666666">
						<!--<td><span class="searchText">&nbsp;Report#</span></td>-->
						<td align="left"><span class="searchText">Control Device</span></td>
						<td align="left"><span class="searchText">Location</span></td>
						<td align="left"><span class="searchText">Date of install</span></td>
						<td align="left"><span class="searchText">%<br />Filled</span></td>
						<td align="center"><span class="searchText">Maintenance<br />Required</span></td>
						<td align="left"><span class="searchText">Maintenance<br />Needed</span></td>
						<td align="center"><span class="searchText">Add/Rem<br />BMPs</span></td>
						<td align="left"><span class="searchText">Comments <br />WECS/DOT Engineer</span></td>
						<td align="left"><span class="searchText">Date Fixed</span></td>
						<!--<td></td>-->
					</tr>
					<tr><td colspan="7"><img src="images/pix.gif" width="1" height="10"></td></tr>
				<%blnChange = true
				iCount = 0
				iNum = 0
				iRep = 0
				do until rsECChecklist.eof
				iOldRepID = iRep
				if rsECChecklist("reportID") <> repID then
					'
					iRep = iRep + 1
				end if
				iCount = iCount + 1
				iNum = iNum + 1
				'for iNum = 1 to iDCount
					'get the devices from database based on division
					Set oCmd = Server.CreateObject("ADODB.Command")
		
					With oCmd
					   .ActiveConnection = DataConn
					   .CommandText = "spGetECDevicesByDivision"
					   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, rsDivision("divisionID"))
					   .CommandType = adCmdStoredProc
					   
					End With
						
						If Err Then
					%>
							<!--#include file="includes/FatalError.inc"-->
					<%
						End If
						
					Set rsECDevice = oCmd.Execute
					Set oCmd = nothing
					
					if iRep <> iOldRepID then							
						'add a row to seperate the reports%>
						<tr><td colspan="15" valign="top"><img src="images/pix.gif" width="1" height="20"></td></tr>
					<%end if
					
					If blnChange = true then%>
						<tr class="rowColor">
					<%else%>
						<tr>
					<%end if%>
						<td>
							<input type="hidden" name="ECChecklistID<%=iNum%>" value="<%=rsECChecklist("ECChecklistID")%>" />
							&nbsp;<select name="device<%=iNum%>">
								<option value="">--select--</option>
								<%do until rsECDevice.eof
									if trim(rsECChecklist("deviceID")) = trim(rsECDevice("deviceID")) then%>
										<option selected="selected" value="<%=rsECDevice("deviceID")%>"><%=rsECDevice("device")%></option>
									<%else%>
										<option value="<%=rsECDevice("deviceID")%>"><%=rsECDevice("device")%></option>
								<%end if
								rsECDevice.movenext
								loop%>
							</select>
						</td>
						<td>
							<input type="text" name="location<%=iNum%>" size="15" value="<%=rsECChecklist("location")%>"/>
						</td>
						<td>
							<input type="text" name="installDate<%=iNum%>" size="6" maxlength="10" value="<%=rsECChecklist("installDate")%>"/>&nbsp;<a href="javascript:displayDatePicker('installDate<%=iNum%>')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
						</td>
						<td>
							<input type="text" name="percentFilled<%=iNum%>" size="2" value="<%=rsECChecklist("percentFilled")%>"/>&nbsp;&nbsp;
						</td>
						<td align="center">
							<select name="maintReq<%=iNum%>">
								<%if trim(request("maintReq" & iNum)) = "No" then%>
									<option selected="selected" value="No">No</option>
								<%else%>
									<option value="No">No</option>
								<%end if%>
								<%if trim(request("maintReq" & iNum)) = "Yes" then%>
									<option selected="selected" value="Yes">Yes</option>
								<%else%>
									<option value="Yes">Yes</option>
								<%end if%>
							</select>

						</td>
						<td>
							<input type="text" name="maintenanceRequired<%=iNum%>" size="30" value="<%=rsECChecklist("maintenanceRequired")%>" maxlength="50"/>
							<!--<select name="maintenanceRequired<%'=iNum%>">
								<%'if trim(rsECChecklist("maintenanceRequired")) = "No" then%>
									<option selected="selected" value="No">No</option>
								<%'else%>
									<option value="No">No</option>
								<%'end if%>
								
								<%'if trim(rsECChecklist("maintenanceRequired")) = "Yes" then%>
									<option selected="selected" value="Yes">Yes</option>
								<%'else%>
									<option value="Yes">Yes</option>
								<%'end if%>
								
							</select>-->
						</td>
						<td align="center">
							<input type="text" name="addRemBMP<%=iNum%>" size="5" value="<%=rsECChecklist("addRemBMP")%>" maxlength="100"/>
							<!--<select name="addRemBMP<%'=iNum%>">
								<option value=""></option>
								<%'if trim(rsECChecklist("addRemBMP")) = "New" then%>
									<option selected="selected" value="New">New</option>
								<%'else%>
									<option value="New">New</option>
								<%'end if%>
								
								<%'if trim(rsECChecklist("addRemBMP")) = "Add" then%>
									<option selected="selected" value="Add">Add</option>
								<%'else%>
									<option value="Add">Add</option>
								<%'end if%>
								
								<%'if trim(rsECChecklist("addRemBMP")) = "Rem" then%>
									<option selected="selected" value="Rem">Rem</option>
								<%'else%>
									<option value="Rem">Rem</option>
								<%'end if%>
								
							</select>-->
						</td>
						<td>
							<input type="text" name="comments<%=iNum%>" size="30" value="<%=rsECChecklist("comments")%>"/>
						</td>
						<td>
							<input type="text" name="dateFixed<%=iNum%>" size="6" maxlength="10" value="<%=rsECChecklist("dateFixed")%>"/>&nbsp;<a href="javascript:displayDatePicker('dateFixed<%=iNum%>')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
						</td>
						<!--<td><a href="">remove item</a></td>-->
					</tr>
					<tr><td colspan="7"><img src="images/pix.gif" width="1" height="5"></td>
				<%
				if blnChange = true then
					blnChange = false
				else
					blnChange = true
				end if
				
				repID = rsECChecklist("reportID")
				
				rsECChecklist.movenext
				
				loop
				'next
				
				iDCount = iNum + iDCount
				iNum = iNum + 1
				
				'add a row to seperate
				%>
				<tr><td colspan="15"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<tr><td colspan="15">&nbsp;<strong>New Report</strong><br /><br /></td></tr>
				<tr bgcolor="#666666">
					<!--<td><span class="searchText">&nbsp;Report#</span></td>-->
					<td align="left"><span class="searchText">Control Device</span></td>
					<td align="left"><span class="searchText">Location</span></td>
					<td align="left"><span class="searchText">Date of install</span></td>
					<td align="left"><span class="searchText">%<br />Filled</span></td>
					<td align="center"><span class="searchText">Maintenance<br />Required</span></td>
					<td align="left"><span class="searchText">Maintenance<br />Needed</span></td>
					<td align="center"><span class="searchText">Add/Rem<br />BMPs</span></td>
					<td align="left"><span class="searchText">Comments <br />WECS/DOT Engineer</span></td>
					<td align="left"><span class="searchText">Date Fixed</span></td>
					<!--<td></td>-->
				</tr>
				<tr><td colspan="7"><img src="images/pix.gif" width="1" height="10"></td></tr>
				<%
				
				'add the new lines here
				for iNum = iNum to iDCount
					'get the devices from database based on division
					Set oCmd = Server.CreateObject("ADODB.Command")
		
					With oCmd
					   .ActiveConnection = DataConn
					   .CommandText = "spGetECDevicesByDivision"
					   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, rsDivision("divisionID"))
					   .CommandType = adCmdStoredProc
					   
					End With
						
						If Err Then
					%>
							<!--#include file="includes/FatalError.inc"-->
					<%
						End If
						
					Set rsECDevice = oCmd.Execute
					Set oCmd = nothing
				
					If blnChange = true then%>
						<tr class="rowColor">
					<%else%>
						<tr>
					<%end if%>
						<!--<td>&nbsp;</td>-->
						<td>
							<input type="hidden" name="ECChecklistID<%=iNum%>" value="" />
							&nbsp;<select name="device<%=iNum%>">
								<option value="">--select--</option>
								<%do until rsECDevice.eof
									if trim(request("device" & iNum)) = trim(rsECDevice("deviceID")) then%>
										<option selected="selected" value="<%=rsECDevice("deviceID")%>"><%=rsECDevice("device")%></option>
									<%else%>
										<option value="<%=rsECDevice("deviceID")%>"><%=rsECDevice("device")%></option>
								<%end if
								rsECDevice.movenext
								loop%>
							</select>
						</td>
						<td>
							<input type="text" name="location<%=iNum%>" size="15" value="<%=request("location" & iNum)%>"/>
						</td>
						<td>
							<input type="text" name="installDate<%=iNum%>" size="6" maxlength="10" value="<%=request("installDate" & iNum)%>"/>&nbsp;<a href="javascript:displayDatePicker('installDate<%=iNum%>')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
						</td>
						<td>
							<input type="text" name="percentFilled<%=iNum%>" size="2" value="<%=request("percentFilled" & iNum)%>"/>&nbsp;&nbsp;
						</td>
						<td align="center">
							<select name="maintReq<%=iNum%>">
								<%if trim(request("maintReq" & iNum)) = "No" then%>
									<option selected="selected" value="No">No</option>
								<%else%>
									<option value="No">No</option>
								<%end if%>
								<%if trim(request("maintReq" & iNum)) = "Yes" then%>
									<option selected="selected" value="Yes">Yes</option>
								<%else%>
									<option value="Yes">Yes</option>
								<%end if%>
							</select>

						</td>
						<td>
							<input type="text" name="maintenanceRequired<%=iNum%>" size="30" value="<%=trim(request("maintenanceRequired" & iNum))%>" maxlength="50"/>
							<!--<select name="maintenanceRequired<%'=iNum%>">
								<%'if trim(request("maintenanceRequired" & iNum)) = "No" then%>
									<option selected="selected" value="No">No</option>
								<%'else%>
									<option value="No">No</option>
								<%'end if%>
								
								<%'if trim(request("maintenanceRequired" & iNum)) = "Yes" then%>
									<option selected="selected" value="Yes">Yes</option>
								<%'else%>
									<option value="Yes">Yes</option>
								<%'end if%>
								
							</select>-->
						</td>
						<td align="center">
							<input type="text" name="addRemBMP<%=iNum%>" size="5" value="<%=request("addRemBMP" & iNum)%>" maxlength="100"/>
							<!--<select name="addRemBMP<%'=iNum%>">
								<option value=""></option>
								<%'if trim(request("addRemBMP" & iNum)) = "New" then%>
									<option selected="selected" value="New">New</option>
								<%'else%>
									<option value="New">New</option>
								<%'end if%>
								
								<%'if trim(request("addRemBMP" & iNum)) = "Add" then%>
									<option selected="selected" value="Add">Add</option>
								<%'else%>
									<option value="Add">Add</option>
								<%'end if%>
								
								<%'if trim(request("addRemBMP" & iNum)) = "Rem" then%>
									<option selected="selected" value="Rem">Rem</option>
								<%'else%>
									<option value="Rem">Rem</option>
								<%'end if%>
								
							</select>-->
						</td>
						<td>
							<input type="text" name="comments<%=iNum%>" size="30" value="<%=request("comments" & iNum)%>"/>
						</td>
						<td>
							<input type="text" name="dateFixed<%=iNum%>" size="6" maxlength="10" value="<%=request("dateFixed" & iNum)%>"/>&nbsp;<a href="javascript:displayDatePicker('dateFixed<%=iNum%>')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>
						</td>
						<!--<td><a href="">remove item</a></td>-->
					</tr>
					<tr><td colspan="7"><img src="images/pix.gif" width="1" height="5"></td>
				<%
				if blnChange = true then
					blnChange = false
				else
					blnChange = true
				end if
				next%>
				<input type="hidden" name="dataCount" value="<%=iNum - 1%>" />
			</table>
		</td>
	</tr>
</table>	
