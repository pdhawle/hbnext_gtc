<%
reportID = request("reportID")
projName = request("projName")
iProjectID = trim(request("projectID"))
reportType = request("repType")
custName = request("custName")
divisionID=request("divisionID")
quoteID=request("quoteID")
sEmail = request("contactEmail")
clientID = request("clientID")


workOrderID = request("workOrderID")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

If workOrderID <> "" then
	sAttach = "workOrder_" & workOrderID & ".pdf"
else
	if quoteID <> "" then
		sAttach = "quote_" & quoteID & ".pdf"
		emailType = "quote"
	else
		sAttach = "swsir_" & reportID & ".pdf"
		
		'generate open items log PDF to send
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "printLogs.asp?projectID=" & iProjectID & "&type=Open&from=&to=&divisionID=" & divisionID, "landscape=true"
		Filename = Doc.Save( Server.MapPath("downloads/openItems/OpenItems_" & iProjectID & ".pdf"), true )
		
		sAttach2 = "OpenItems_" & iProjectID & ".pdf"
		Set Pdf = nothing
		
		'generate closed items log PDF to send
		Set Pdf = Server.CreateObject("Persits.Pdf")
		Pdf.RegKey = sRegKey
		Set Doc = Pdf.CreateDocument
		Doc.ImportFromUrl sPDFPath & "printLogs.asp?projectID=" & iProjectID & "&type=Corrected&from=&to=&divisionID=" & divisionID, "landscape=true"
		Filename = Doc.Save( Server.MapPath("downloads/openItems/CorrectedItems_" & iProjectID & ".pdf"), true )
		
		sAttach3 = "CorrectedItems_" & iProjectID & ".pdf"
		Set Pdf = nothing
	end if
end if

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")
'get the user's signature, if available
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetUser"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, Session("ID"))
   .CommandType = adCmdStoredProc
   
End With
			
Set rsUser = oCmd.Execute
Set oCmd = nothing

%>
<SCRIPT LANGUAGE="JavaScript">
var myWind = ""
function doNew() {
	if (myWind == "" || myWind.closed || myWind.name == undefined) {
		myWind = window.open("addressBook.asp?project=<%=iProjectID%>","subWindow","HEIGHT=600,WIDTH=1000,scrollbars=yes")
	} else{
		myWind.focus();
	}
}
</SCRIPT>
<form action="process.asp" method="post" name="myForm">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sNPDESTitle%></span><span class="Header"> - Send Email</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td>
												<strong>Project Name:</strong> <%=projName%>
											</td>
											<td></td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td><img src="images/pix.gif" width="10" height="1"></td>
											<td>
												<table border="0" cellpadding="0" cellspacing="0">
													<tr>
														<td colspan="3">
															<strong>Check below to send as an attachment or click to print.</strong> <br />
															<%If workOrderID <> "" then%>
																<input type="checkbox" name="workOrderPDF" checked="checked"/> <strong><a href="downloads/<%=sAttach%>" target="_blank">Work Order for project #<%=iProjectID%> (<%=projName%>)</a></strong><br /><br />
															<%else%>
																<%if quoteID <> "" then%>
																	<input type="checkbox" name="quotePDF" checked="checked"/> <strong><a href="downloads/<%=sAttach%>" target="_blank">Quote #<%=quoteID%>  for <%=projName%></a></strong><br /><br />
																<%else%>
																	<input type="checkbox" name="openItemsPDF" checked="checked"/> <strong><a href="downloads/openItems/<%=sAttach2%>" target="_blank">Current open items for project #<%=iProjectID%> (<%=projName%>)</a></strong><br />
																	<input type="checkbox" name="correctedItemsPDF"/> <strong><a href="downloads/openItems/<%=sAttach3%>" target="_blank">Corrected items for project #<%=iProjectID%> (<%=projName%>)</a></strong><br />
																	<input type="checkbox" name="reportPDF"/> <strong><a href="downloads/<%=sAttach%>" target="_blank">Report #<%=reportID%>  (<%=reportType%>)</a></strong><br /><br />
																<%end if%>
															<%end if%>
															Separate multiple recipients' email addresses with a semi-colon (;). 
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<tr>
														<td><input type="button" name="storage" value="To:.." onClick="doNew()" class="formButton"></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="emailTo" size="50" value="<%=sEmail%>">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>CC:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="emailCC" size="50">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td><strong>Bcc:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<input type="text" name="emailBCC" size="50">
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>	
													<tr>
														<td><strong>Subject:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%If workOrderID <> "" then%>
																<input type="text" name="subject" size="50" value="<%=custName & " - " & projName & " - Work Order - " & date()%>">
															<%else%>
																<%if quoteID <> "" then%>
																	<input type="text" name="subject" size="50" value="<%=projName & " - NPDES Inspection Quote - " & date()%>">
																<%else%>
																	<input type="text" name="subject" size="50" value="<%=custName & " - " & projName & " - " & reportType & " - " & date()%>">
																<%end if%>
															<%end if%>
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>			
													<tr>
														<td valign="top"><strong>Message:</strong></td>
														<td><img src="images/pix.gif" width="5" height="1"></td>
														<td>
															<%
																Dim strFormName
																Dim strTextAreaName
																strFormName = "myForm"
																strTextAreaName = "smessage"
															%>
															<%'if reportType <> "Daily" then response.Write sOIBody end if%>
															<textarea name="smessage" cols="70" rows="10"><br /><br><%=rsUser("emailSignature")%></textarea>
															<script>
																//STEP 2: Replace the textarea (txtContent)
																oEdit1 = new InnovaEditor("oEdit1");
																oEdit1.features=["FullScreen","Preview","Print","Search",
																	"Cut","Copy","Paste","PasteWord","PasteText","|","Undo","Redo","|",
																	"ForeColor","BackColor","|","Bookmark","Hyperlink","XHTMLSource","BRK",
																	"Numbering","Bullets","|","Indent","Outdent","LTR","RTL","|",
																	"Image","Flash","Media","|","Table","Guidelines","Absolute","|",
																	"Characters","Line","Form","RemoveFormat","ClearAll","BRK",
																	"StyleAndFormatting","TextFormatting","ListFormatting","BoxFormatting",
																	"ParagraphFormatting","CssText","|",
																	"Paragraph","FontName","FontSize","|",
																	"Bold","Italic","Underline","Strikethrough","Superscript","Subscript","|",
																	"JustifyLeft","JustifyCenter","JustifyRight","JustifyFull"];
																oEdit1.REPLACE("smessage");
															</script>
														</td>
													</tr>
													<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
													<tr>
														<td colspan="3" align="right">
															<input type="hidden" name="emailType" value="<%=emailType%>" />
															<input type="hidden" name="clientID" value="<%=clientID%>" />
															<input type="hidden" name="quoteID" value="<%=quoteID%>" />
															<input type="hidden" name="reportID" value="<%=reportID%>" />
															<input type="hidden" name="processType" value="sendEmail" />
															<input type="hidden" name="attachment1" value="<%=sAttach%>" />
															<input type="hidden" name="attachment2" value="<%=sAttach2%>" />
															<input type="hidden" name="attachment3" value="<%=sAttach3%>" />
															<input type="hidden" name="projName" value="<%=projName%>" />
															<input type="submit" value="Send Email" class="formButton"/>
														</td>
													</tr>
												</table>
											</td>
											<td><img src="images/pix.gif" width="10" height="1"></td>
										</tr>
									</table>
									
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>