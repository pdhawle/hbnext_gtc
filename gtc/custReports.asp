<!--#include file="includes/constants.inc"-->
<%
If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If
%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sCustomerTitle%></span><span class="Header"> - Customer Reports</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td>
												<a href="custReportsOpenAI.asp"><b>Report Status</b></a> - Select and view reports with open versus corrected items.<br>
												<!--<a href="">Query Closed Action Items</a>  Select and view reports with resolved <em>Action Required</em> items.<br>-->
												<a href="custReportsArchive.asp"><b>Archive Reports</b></a> - Select reports to download for archival purposes.  <br>
												<a href="monthlySummary.asp"><b>Site Inspection Monthly Summary Report</b></a> - View and download a monthy snapshot of a given project.  <br>
												<a href="custReportsWOStatus.asp"><b>Work Order Status</b></a> - Select and view work orders with the current status.<br>
											</td>
										</tr>
									</table>
															
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>