		<div id="myform">
			<tr>
				<td colspan="2">
					<!--<strong>Attachment "A"</strong><br />
					<strong>Major Land Disturbance</strong><br />
					<strong>Special Inspector Approval Application Form</strong><br />-->
					<%Select Case iReportTypeID
						Case 1 'weekly*************************************************%>
							<strong>MAJOR LAND DISTURBANCE</strong><br />
							<strong>SPECIAL INSPECTOR�S WEEKLY INSPECTION REPORT</strong><br />									
						<%case 4'post-rain%>
							<strong>MAJOR LAND DISTURBANCE</strong><br />
							<strong>SPECIAL INSPECTOR�S HEAVY RAIN � COMPLAINT INSPECTION REPORT</strong><br />
					<%end select%>
					
					<input type="checkbox" name="addToReport" checked="checked" />&nbsp;add form to report<br />
					<input type="radio" name="AttachAForm" value="Yes" onclick="javascript: $('#AttachA').show('slow');"  />&nbsp;Show&nbsp;&nbsp;
					<input checked="checked" type="radio" name="AttachAForm" value="No" onclick="javascript: $('#AttachA').hide('slow');"  />&nbsp;Hide<br /><br />
					
					<div id="AttachA">
						<table cellpadding="0" cellspacing="0" border="0">
							<!--<tr><td colspan="2"><strong>Requested Approval</strong></td></tr>
							<tr>
								<td valign="top" width="5"><input type="checkbox" name="requestApproval" />&nbsp;</td>
								<td>
									I/we hereby request approval to perform the duties and responsibilities of the Major Land Disturbance Special 
									Inspector for the major land disturbance known as&nbsp;<input type="text" name="landDisturbanceName" maxlength="50" value="<%=rsProjInfo("projectName")%>" />
								</td>
							</tr>
							<tr><td colspan="2" height="5"></td></tr>
							<tr>
								<td valign="top"><input type="checkbox" name="requestPreApproval" />&nbsp;</td>
								<td>
									I/we hereby request pre-approval and registration to perform the duties and responsibilities of a Major Land 
									Disturbance Special Inspector on projects regulated by St. Louis County. I have enclosed the $ 22.00 processing fee.
								</td>
							</tr>
							<tr><td colspan="2" height="10"></td></tr>
							<tr><td colspan="2"><strong>Statement of Qualifications</strong></td></tr>
							<tr>
								<td valign="top"><input type="checkbox" name="licensedEngineer" />&nbsp;</td>
								<td>
									I am a Missouri Licensed/Registered Professional Engineer or Land Surveyor, experienced and knowledgeable in the principals 
									and practices of erosion and sediment control, including BMP�s as well as the duties & responsibilities of Major Land Disturbance 
									Special Inspectors described in the St. Louis County Land Disturbance Code.
								</td>
							</tr>
							<tr><td colspan="2" height="5"></td></tr>
							<tr>
								<td valign="top"><input type="checkbox" name="licensedEngineeringFirm" />&nbsp;</td>
								<td>
									We are a Missouri Licensed/Registered Professional Engineering or Land Surveying firm that is experienced and knowledgeable in the 
									principles and practices of erosion & sediment control, including BMP�s as well as the duties & responsibilities of Major Land Disturbance 
									Special Inspector described in the St. Louis County Land Disturbance Code.
								</td>
							</tr>
							<tr><td colspan="2" height="5"></td></tr>
							<tr>
								<td valign="top"><input type="checkbox" name="experiencedPrinciplesPractices" />&nbsp;</td>
								<td>
									I am experienced and knowledgeable in the principals and practices of erosion and sediment control, including BMPs as well as the duties 
									and responsibilities of Major Land Disturbance Special Inspectors described in the St. Louis County Land Disturbance Code.
								</td>
							</tr>
							<tr><td colspan="2" height="5"></td></tr>
							<tr>
								<td valign="top"><input type="checkbox" name="experiencedPrinciplesPracticesFirm" />&nbsp;</td>
								<td>
									We are a firm that is experienced and knowledgeable in the principals and practices of erosion and sediment control, including BMP�s 
									as well as the duties and responsibilities of the Major Land Disturbance Special Inspector described in the St. Louis County Land 
									Disturbance Code.
								</td>
							</tr>
							<tr><td colspan="2" height="10"></td></tr>
							<tr><td colspan="2"><strong>Experience References</strong></td></tr>
							<tr>
								<td colspan="2">
									Enclosed are project descriptions, dates and names, telephone numbers and <strong>fax numbers</strong> of reference individuals who have knowledge of 
									my/our field inspection and/or supervisory functions related to erosion and sediment control including Best Management Practices for 
									three (3) large land disturbance projects.
								</td>
							</tr>
							<tr><td colspan="2" height="10"></td></tr>
							<tr><td colspan="2"><strong>Signatures & Data Concerning Applicant(s)</strong></td></tr>
							<tr>
								<td colspan="2">
									<table cellpadding="0" cellspacing="0">
										<tr>
											<td align="right"><strong>Company Name:</strong>&nbsp;</td>
											<td><input type="text" name="attachACompanyName" value="<%'=Session("Company")%>" maxlength="50" /></td>
										</tr>
										<tr>
											<td align="right"><strong>Individual Name:</strong>&nbsp;</td>
											<td><input type="text" name="attachAIndividualName" value="<%'=Session("Name")%>"  maxlength="50" /></td>
										</tr>
										<tr>
											<td align="right"><strong>Title:</strong>&nbsp;</td>
											<td><input type="text" name="attachATitle" value="<%'=Session("JobTitle")%>" maxlength="50" /></td>
										</tr>
										<tr>
											<td align="right"><strong>Signature:</strong>&nbsp;</td>
											<td><em>Attached to form if available</em></td>
										</tr>
										<tr>
											<td align="right"><strong>Date:</strong>&nbsp;</td>
											
											<td><input type="text" name="attachADate" maxlength="10" size="10" value="<%'=formatdatetime(now(),2)%>"/>&nbsp;<a href="javascript:displayDatePicker('attachADate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a></td>
										</tr>
										<tr>
											<td align="right"><strong>Address:</strong>&nbsp;</td>
											<td><input type="text" name="attachAAddress" value="" size="30" maxlength="100" /></td>
										</tr>
										<tr>
											<td align="right"><strong>City:</strong>&nbsp;</td>
											<td><input type="text" name="attachACity" value="" maxlength="50" /></td>
										</tr>
										<tr>
											<td align="right"><strong>State:</strong>&nbsp;</td>
											<td><input type="text" name="attachAState" value="" maxlength="50"/>&nbsp;<strong>Zip:</strong>&nbsp;<input type="text" name="attachAZip" value="" size="3" maxlength="15"/></td>
										</tr>
										<tr>
											<td align="right"><strong>Telephome(Normal Hours):</strong>&nbsp;</td>
											<td><input type="text" name="attachATelephoneNormal" value="<%'=Session("cellPhone")%>" maxlength="15" /></td>
										</tr>
										<tr>
											<td align="right"><strong>Emergency(24 hours):</strong>&nbsp;</td>
											<td><input type="text" name="attachATelephoneEmergency" value="" maxlength="15" /></td>
										</tr>
										<tr>
											<td align="right"><strong>Email:</strong>&nbsp;</td>
											<td><input type="text" name="attachAEmail" value="<%'=Session("Email")%>" size="30" maxlength="100" /></td>
										</tr>
										<tr>
											<td align="right"><strong>Fax:</strong>&nbsp;</td>
											<td><input type="text" name="attachAFax" value="" maxlength="15" /></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr><td colspan="2" height="10"></td></tr>
							<tr><td colspan="2"><strong>Approval/Disapproval - Department of Public Works</strong></td></tr>
							<tr><td colspan="2">Reviewed and:</td></tr>							
							<tr>
								<td valign="top"><input type="checkbox" name="approved" />&nbsp;</td>
								<td>
									Approved by:
								</td>
							</tr>
							<tr>
								<td valign="top"><input type="checkbox" name="disApproved" />&nbsp;</td>
								<td>
									Disapproved by:
								</td>
							</tr>
							<tr>
								<td valign="top" colspan="2"><strong>Printed:</strong>&nbsp;<input type="text" name="approvalPrintedName" value="" maxlength="50" /></td>
							</tr>
							<tr>
								<td valign="top" colspan="2"><strong>Signed:</strong>&nbsp;<em>will need to be signed after form is printed</em></td>
							</tr>
							<tr>
								<td valign="top"><input type="checkbox" name="addComments" />&nbsp;</td>
								<td>
									<strong>Comments</strong><br />
									<textarea name="attachAComments" rows="3" cols="40"></textarea>
								</td>
							</tr><tr><td colspan="2" height="10"></td></tr>
							<tr>
								<td colspan="2" align="center">
									<strong>Submit original signed copy to the Public Works:</strong><br />
									Residential Inspect. Section for Resid. Land Disturbances or Commercial Inspect. Section for Comm. Land Disturbances<br />
									Lawrence K. Roos&nbsp;&nbsp;&nbsp;&nbsp;Building 41 S. Central Avenue (6th Floor)&nbsp;&nbsp;&nbsp;&nbsp;St. Louis, MO 63105<br />
									
								</td>
							</tr>
							<tr><td colspan="2" height="30"></td></tr>-->
							
							<%Select Case iReportTypeID
							Case 1 'weekly*************************************************%>
							
							
							<!--<tr>
								<td colspan="2" align="center">
									<strong>MAJOR LAND DISTURBANCE</strong><br />
									<strong>SPECIAL INSPECTOR�S WEEKLY INSPECTION REPORT</strong>									
								</td>
							</tr>-->
							<tr><td colspan="2" height="10"></td></tr>
							<tr>
								<td colspan="2">
									<table cellpadding="0" cellspacing="0">
										<tr>
											<td align="right"><strong>Project Name:</strong>&nbsp;</td>
											<td><input type="text" name="attachAProjectName" value="<%=rsProjInfo("projectName")%>" maxlength="50" /></td>
										</tr>
										<tr>
											<td align="right"><strong>Public Works Inspection Permit Number:</strong>&nbsp;</td>
											<td><input type="text" name="attachAPermitNumber" value="" maxlength="50" />&nbsp;<em>(report will be returned if seven digit permit number is not included)</em></td>
										</tr>
										<tr>
											<td align="right"><strong>Inspection Date(s):</strong>&nbsp;</td>
											<td><input type="text" name="attachAInspectionDate" maxlength="10" size="10" value="<%=formatdatetime(now(),2)%>"/>&nbsp;<a href="javascript:displayDatePicker('attachAInspectionDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a></td>
										</tr>
										<tr>
											<td align="right"><strong>Inspected By(Company):</strong>&nbsp;</td>
											<td><input type="text" name="attachAInspectedByCompanyName" value="<%=Session("Company")%>" maxlength="50" /></td>
										</tr>
										<tr>
											<td align="right"><strong>Inspected By(Individual):</strong>&nbsp;</td>
											<td><input type="text" name="attachAInspectedByIndividualName" value="<%=Session("Name")%>" maxlength="50" /></td>
										</tr>
										<tr>
											<td align="right"><strong>Phone #:</strong>&nbsp;</td>
											<td><input type="text" name="attachAInspectedByPhoneNumber" value="<%=Session("cellPhone")%>" maxlength="15" /></td>
										</tr>
										<tr>
											<td align="right"><strong>Emergency Phone(24 hour) #:</strong>&nbsp;</td>
											<td><input type="text" name="attachAInspectedByPhoneEmergency" value="" maxlength="50" /></td>
										</tr>
										<tr>
											<td align="right"><strong>Email:</strong>&nbsp;</td>
											<td><input type="text" name="attachAInspectedByEmail" value="<%=Session("Email")%>" size="30" maxlength="100" /></td>
										</tr>
										<tr>
											<td align="right"><strong>Fax:</strong>&nbsp;</td>
											<td><input type="text" name="attachAInspectedByFax" value="" maxlength="15" /></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr><td colspan="2" height="10"></td></tr>	
							<tr>
								<td colspan="2">
									<strong>Check Those Items that are Applicable to this Phase of the Land Disturbance	</strong>	
								</td>
							</tr>
							<tr><td colspan="2" height="10"></td></tr>	
							<tr>
								<td colspan="2">
									<table cellpadding="0" cellpadding="0">
										<tr>
											<td valign="top">
												<strong>Items of Concern</strong><br />
												<table cellpadding="0" cellspacing="0">
													<tr>
														<td valign="top"><input type="checkbox" name="itemSedimentLeaving" />&nbsp;</td>
														<td>
															Sediment leaving the land disturbance site.
														</td>
													</tr>
												</table>
											</td>
											<td width="20"></td>
											<td valign="top">
												<strong>Inspection Results</strong><br />
												<table cellpadding="0" cellspacing="0">
													<tr>
														<td valign="top"><input type="checkbox" name="itemSedimentLeavingSiteInCompliance" />&nbsp;</td>
														<td>
															Site in compliance with Land Disturbance Code/Approved Plans
														</td>
													</tr>
													<tr>
														<td valign="top"><input type="checkbox" name="itemSedimentLeavingCorrectionRequired" />&nbsp;</td>
														<td>
															Correction of deficiencies required. See attached comments/descriptions
														</td>
													</tr>
													<tr>
														<td valign="top"><input type="checkbox" name="itemSedimentLeavingDeficienciesCorrectedNotCorrected" />&nbsp;</td>
														<td>
															Deficiencies from previous report <input type="checkbox" name="itemSedimentLeavingDeficienciesCorrected" /> Corrected; <input type="checkbox" name="itemSedimentLeavingDeficienciesNotCorrected" /> Not corrected.	See attached comments/descriptions
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr><td colspan="3" height="20"></td></tr>
										<tr>
											<td valign="top">
												<table cellpadding="0" cellspacing="0">
													<tr>
														<td valign="top"><input type="checkbox" name="itemOffSiteErosion" />&nbsp;</td>
														<td>
															Off-site erosion caused by land disturbance activity.
														</td>
													</tr>
												</table>
											</td>
											<td width="20"></td>
											<td valign="top">
												<table cellpadding="0" cellspacing="0">
													<tr>
														<td valign="top"><input type="checkbox" name="itemOffSiteErosionInCompliance" />&nbsp;</td>
														<td>
															Site in compliance with Land Disturbance Code/Approved Plans
														</td>
													</tr>
													<tr>
														<td valign="top"><input type="checkbox" name="itemOffSiteErosionCorrectionRequired" />&nbsp;</td>
														<td>
															Correction of deficiencies required. See attached comments/descriptions
														</td>
													</tr>
													<tr>
														<td valign="top"><input type="checkbox" name="itemOffSiteErosionDeficienciesCorrectedNotCorrected" />&nbsp;</td>
														<td>
															Deficiencies from previous report <input type="checkbox" name="itemOffSiteErosionDeficienciesCorrected" /> Corrected; <input type="checkbox" name="itemOffSiteErosionDeficienciesNotCorrected" /> Not corrected. See attached comments/descriptions
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr><td colspan="3" height="20"></td></tr>
										<tr>
											<td valign="top">
												<table cellpadding="0" cellspacing="0">
													<tr>
														<td valign="top"><input type="checkbox" name="itemProtection" />&nbsp;</td>
														<td>
															Protection of operational storm drain inlet(s). Protection from sediment entering storm drain system.
														</td>
													</tr>
												</table>
											</td>
											<td width="20"></td>
											<td valign="top">
												<table cellpadding="0" cellspacing="0">
													<tr>
														<td valign="top"><input type="checkbox" name="itemProtectionInCompliance" />&nbsp;</td>
														<td>
															Site in compliance with Land Disturbance Code/Approved Plans
														</td>
													</tr>
													<tr>
														<td valign="top"><input type="checkbox" name="itemProtectionCorrectionRequired" />&nbsp;</td>
														<td>
															Correction of deficiencies required. See attached comments/descriptions
														</td>
													</tr>
													<tr>
														<td valign="top"><input type="checkbox" name="itemProtectionDeficienciesCorrectedNotCorrected" />&nbsp;</td>
														<td>
															Deficiencies from previous report <input type="checkbox" name="itemProtectionDeficienciesCorrected" /> Corrected; <input type="checkbox" name="itemProtectionDeficienciesNotCorrected" /> Not corrected. See attached comments/descriptions
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr><td colspan="3" height="20"></td></tr>
										<tr>
											<td valign="top">
												<table cellpadding="0" cellspacing="0">
													<tr>
														<td valign="top"><input type="checkbox" name="itemMud" />&nbsp;</td>
														<td>
															Mud on paved roadways. Installation, maintenance & protection of vehicle wash down areas.
														</td>
													</tr>
												</table>
											</td>
											<td width="20"></td>
											<td valign="top">
												<table cellpadding="0" cellspacing="0">
													<tr>
														<td valign="top"><input type="checkbox" name="itemMudInCompliance" />&nbsp;</td>
														<td>
															Site in compliance with Land Disturbance Code/Approved Plans
														</td>
													</tr>
													<tr>
														<td valign="top"><input type="checkbox" name="itemMudCorrectionRequired" />&nbsp;</td>
														<td>
															Correction of deficiencies required. See attached comments/descriptions
														</td>
													</tr>
													<tr>
														<td valign="top"><input type="checkbox" name="itemMudDeficienciesCorrectedNotCorrected" />&nbsp;</td>
														<td>
															Deficiencies from previous report <input type="checkbox" name="itemMudDeficienciesCorrected" /> Corrected; <input type="checkbox" name="itemMudDeficienciesNotCorrected" /> Not corrected.	See attached comments/descriptions
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr><td colspan="3" height="20"></td></tr>
										<tr>
											<td valign="top">
												<table cellpadding="0" cellspacing="0">
													<tr>
														<td valign="top"><input type="checkbox" name="itemInstallation" />&nbsp;</td>
														<td>
															Installation & maintenance of other necessary BMP�s.
														</td>
													</tr>
												</table>
											</td>
											<td width="20"></td>
											<td valign="top">
												<table cellpadding="0" cellspacing="0">
													<tr>
														<td valign="top"><input type="checkbox" name="itemInstallationInCompliance" />&nbsp;</td>
														<td>
															Site in compliance with Land Disturbance Code/Approved Plans
														</td>
													</tr>
													<tr>
														<td valign="top"><input type="checkbox" name="itemInstallationCorrectionRequired" />&nbsp;</td>
														<td>
															Correction of deficiencies required. See attached comments/descriptions
														</td>
													</tr>
													<tr>
														<td valign="top"><input type="checkbox" name="itemInstallationDeficienciesCorrectedNotCorrected" />&nbsp;</td>
														<td>
															Deficiencies from previous report <input type="checkbox" name="itemInstallationDeficienciesCorrected" /> Corrected; <input type="checkbox" name="itemInstallationDeficienciesNotCorrected" /> Not corrected. See attached comments/descriptions
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>	
								</td>
							</tr>
							<tr><td colspan="2" height="10"></td></tr>	
							<tr>
								<td colspan="2">
									It is my professional opinion that the land disturbance work being conducted except as specifically identified above and attached is in 
									compliance with the St. Louis County Land Disturbance Code as further identified on the approved plans.<br />
									I have also attached listings of areas where land disturbance operations have permanently or temporarily stopped.<br />
									
									<input type="checkbox" name="furtherConcerns" />&nbsp;<strong>Attached are Further Comments/Descriptions of Deficiencies/Special Problems or Concerns</strong>
								</td>
							</tr>
							<tr><td colspan="2" height="10"></td></tr>
							<tr>
								<td colspan="2">
									<table border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td align="right"><strong>Printed Name:</strong>&nbsp;</td>											
											<td><input type="text" name="weeklyPrintedName" value="<%=Session("Name")%>" /></td>
										</tr>
										<tr>
											<td align="right"><strong>Company:</strong>&nbsp;</td>											
											<td><input type="text" name="weeklyCompany" value="<%=Session("Company")%>" /></td>
										</tr>
										<tr>
											<td align="right"><strong>Inspector Signature:</strong>&nbsp;</td>		
											<td><em>Attached to form if available</em></td>											
										</tr>	
										<tr>
											<td align="right"><strong>Date:</strong>&nbsp;</td>											
											<td><input type="text" name="weeklyDate" maxlength="10" size="10" value="<%=formatdatetime(now(),2)%>"/>&nbsp;<a href="javascript:displayDatePicker('weeklyDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td colspan="2">
									Submit original signed copy NLT Friday of the week following the week of inspection to the Public Works:<br />
									Attn: Chief Residential for Resid. Land Disturbances or Chief Commercial Inspector for Comm. Land Disturbances<br />
									Lawrence K. Roos&nbsp;&nbsp;&nbsp;&nbsp;Building 41 S. Central Avenue (6th Floor)&nbsp;&nbsp;&nbsp;&nbsp;St. Louis, MO 63105
								</td>
							</tr>	
									
									
									
									
							<%Case 4 'post-rain**************************************************************%>
							<!--<tr>
								<td colspan="2" align="center">
									<strong>MAJOR LAND DISTURBANCE</strong><br />
									<strong>SPECIAL INSPECTOR�S HEAVY RAIN � COMPLAINT INSPECTION REPORT</strong>									
								</td>
							</tr>-->
							<tr><td colspan="2" height="10"></td></tr>
							<tr>
								<td colspan="2">
									<table cellpadding="0" cellspacing="0">
										<tr>
											<td align="right"><strong>Project Name:</strong>&nbsp;</td>
											<td><input type="text" name="attachAProjectName" value="<%=rsProjInfo("projectName")%>" maxlength="50" /></td>
										</tr>
										<tr>
											<td align="right"><strong>Public Works Inspection Permit Number:</strong>&nbsp;</td>
											<td><input type="text" name="attachAPermitNumber" value="" maxlength="50" />&nbsp;<em>(report will be returned if seven digit permit number is not included)</em></td>
										</tr>
										<tr>
											<td align="right"><strong>Inspection Date(s):</strong>&nbsp;</td>
											<td><input type="text" name="attachAInspectionDate" maxlength="10" size="10" value="<%=formatdatetime(now(),2)%>"/>&nbsp;<a href="javascript:displayDatePicker('attachAInspectionDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a></td>
										</tr>
										<tr>
											<td align="right"><strong>Inspected By(Company):</strong>&nbsp;</td>
											<td><input type="text" name="attachAInspectedByCompanyName" value="<%=Session("Company")%>" maxlength="50" /></td>
										</tr>
										<tr>
											<td align="right"><strong>Inspected By(Individual):</strong>&nbsp;</td>
											<td><input type="text" name="attachAInspectedByIndividualName" value="<%=Session("Name")%>" maxlength="50" /></td>
										</tr>
										<tr>
											<td align="right"><strong>Phone #:</strong>&nbsp;</td>
											<td><input type="text" name="attachAInspectedByPhoneNumber" value="<%=Session("cellPhone")%>" maxlength="15" /></td>
										</tr>
										<tr>
											<td align="right"><strong>Emergency Phone(24 hour) #:</strong>&nbsp;</td>
											<td><input type="text" name="attachAInspectedByPhoneEmergency" value="" maxlength="15" /></td>
										</tr>
										<tr>
											<td align="right"><strong>Email:</strong>&nbsp;</td>
											<td><input type="text" name="attachAInspectedByEmail" value="<%=Session("Email")%>" size="30" maxlength="100" /></td>
										</tr>
										<tr>
											<td align="right"><strong>Fax:</strong>&nbsp;</td>
											<td><input type="text" name="attachAInspectedByFax" value="" maxlength="15" /></td>
										</tr>
										<tr><td colspan="2" height="20"></td></tr>
										<tr>
											<td colspan="2">
												I have made an inspection of the above referenced land disturbance site as related to:<br /> 
												<input type="checkbox" name="relatedToHeavyRain" /> the heavy rain that occurred on or about 
												<input type="text" name="heavyRainDate" maxlength="10" size="10" value="<%=formatdatetime(now(),2)%>"/>&nbsp;<a href="javascript:displayDatePicker('heavyRainDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a><br />
												<input type="checkbox" name="relatedToCompliant" /> the complaint concerning matters described below that were referred to me by the Dept. of Public Works and I submit the following report<br /><br />
												
												The siltation control devices, Best Management Practices or Land Disturbance Activities <input type="checkbox" name="wereInCompliance" /> <strong>were</strong> <input type="checkbox" name="wereNotInCompliance" /> <strong>were not</strong> in compliance with the Land Disturbance Code/Approved Plans. I directed the correction of the following described deficiencies on or before 
												<input type="text" name="correctionDate" maxlength="10" size="10" value="<%=formatdatetime(now(),2)%>"/>&nbsp;<a href="javascript:displayDatePicker('correctionDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>. <br />
												I will re-inspect the site on or about this date, to verify that such deficiencies were corrected.<br /><br />
												
												Describe deficiencies & specific individuals & companies directed to correct same<br />
												<textarea name="describeDeficiencies" rows="4" cols="50"></textarea><br /><br />
												
												In my previous report dated 
												<input type="text" name="previousReportDate" maxlength="10" size="10" value="<%=formatdatetime(now(),2)%>"/>&nbsp;<a href="javascript:displayDatePicker('previousReportDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a><br />
												I identified Land Disturbance Code/Approved Plans deficiencies that need to be corrected. I have re-inspected the land disturbance site and find that all 
												the previously identified deficiencies	<input type="checkbox" name="wereProperlyCorrected" /> <strong>were</strong> <input type="checkbox" name="wereNotProperlyCorrected" /> <strong>were not</strong> properly corrected. (If deficiencies were not all properly corrected describe additional actions taken to assure proper correction of all deficiencies in an attachment).<br /><br />
												
												<input type="checkbox" name="furtherConcerns" />&nbsp;<strong>Attached are Further Comments/Descriptions of Deficiencies/Special Problems or Concerns</strong>
											</td>
										</tr>
										<tr><td colspan="2" height="10"></td></tr>
										<tr>
											<td colspan="2">
												<table border="0" cellpadding="0" cellspacing="0">
													<tr>
														<td align="right"><strong>Printed Name:</strong>&nbsp;</td>											
														<td><input type="text" name="weeklyPrintedName" value="<%=Session("Name")%>" /></td>
													</tr>
													<tr>
														<td align="right"><strong>Company:</strong>&nbsp;</td>											
														<td><input type="text" name="weeklyCompany" value="<%=Session("Company")%>" /></td>
													</tr>
													<tr>
														<td align="right"><strong>Inspector Signature:</strong>&nbsp;</td>		
														<td><em>Attached to form if available</em></td>											
													</tr>	
													<tr>
														<td align="right"><strong>Date:</strong>&nbsp;</td>											
														<td><input type="text" name="weeklyDate" maxlength="10" size="10" value="<%=formatdatetime(now(),2)%>"/>&nbsp;<a href="javascript:displayDatePicker('weeklyDate')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a></td>
													</tr>
												</table>
											</td>
										</tr>													
										
										
										<tr>
											<td colspan="2">
												Submit original signed copy within 72 hrs. of the heavy rain event to the Public Works:<br />
												Attn: Chief Residential or Chief Commercial Inspector<br />
												Lawrence K. Roos&nbsp;&nbsp;&nbsp;&nbsp;Building 41 S. Central Avenue (6th Floor)&nbsp;&nbsp;&nbsp;&nbsp;St. Louis, MO 63105
											</td>
										</tr>
									</table>
								</td>
							</tr>
							
							<%end select%>
							
						</table>
					</div>				
				</td>
			</tr>
		</div>