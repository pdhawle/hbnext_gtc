<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->
<%
sPage = "customerList"
sSearchString = request("search")

sLetter = request("id")

Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

if request("fp") <> "" then
	Session("fp") = request("fp")
else
	Session("fp") = ""
end if

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")

If sLetter = "" then
	sLetter = "@"
end if


if sSearchString <> "" then
	Set oCmd = Server.CreateObject("ADODB.Command")
			
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spSearchCustomers"
		   .parameters.Append .CreateParameter("@id", adVarchar, adParamInput, 100, sSearchString)
		   .CommandType = adCmdStoredProc   
		End With
			
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
					
		Set rs = oCmd.Execute
		Set oCmd = nothing
		
		sLetter = "Search results for: " & sSearchString
		
else


	If session("superAdmin") = "True" Then
	'get all of the customers
		Select Case sLetter
			Case "ALL"
				With oCmd
				   .ActiveConnection = DataConn
				   .CommandText = "spGetAllCustomers"
				   .CommandType = adCmdStoredProc   
				End With
				
			Case "INACTIVE"
				With oCmd
				   .ActiveConnection = DataConn
				   .CommandText = "spGetInactiveCustomers"
				   .CommandType = adCmdStoredProc   
				End With
				
			Case else
				With oCmd
				   .ActiveConnection = DataConn
				   .CommandText = "spGetCustomersByLetter"
				   .parameters.Append .CreateParameter("@letterID", adVarchar, adParamInput, 50, sLetter)
				   .CommandType = adCmdStoredProc   
				End With
		end select	
	else
	'get only the customers that are assigned to the client
		Select Case sLetter
			Case "ALL"
				With oCmd
				   .ActiveConnection = DataConn
				   .CommandText = "spGetAllCustomersForClient"
				   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, session("clientID"))
				   .CommandType = adCmdStoredProc   
				End With
				
			Case "INACTIVE"
				With oCmd
				   .ActiveConnection = DataConn
				   .CommandText = "spGetInactiveCustomersForClient"
				   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, session("clientID"))
				   .CommandType = adCmdStoredProc   
				End With
				
			Case else
				With oCmd
				   .ActiveConnection = DataConn
				   .CommandText = "spGetCustomersByLetterForClient"
				   .parameters.Append .CreateParameter("@letterID", adVarchar, adParamInput, 50, sLetter)
				   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, session("clientID"))
				   .CommandType = adCmdStoredProc   
				End With
		end select
	
	end if
			
	Set rs = oCmd.Execute
	Set oCmd = nothing
	
end if
	

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script type="text/javascript">
    var GB_ROOT_DIR = "<%=sPDFPath%>/greybox/";
</script>
<script type="text/javascript" src="greybox/AJS.js"></script>
<script type="text/javascript" src="greybox/AJS_fx.js"></script>
<script type="text/javascript" src="greybox/gb_scripts.js"></script>
<link href="greybox/gb_styles.css" rel="stylesheet" type="text/css" />
<script language="JavaScript">
<!--
function confirmDelete(delUrl) {
 if (confirm("Are you sure you wish to delete this customer?")) {
    document.location = delUrl;
  }


}
//-->
</script>
<link rel="stylesheet" type="text/css" href="menu/popupmenu.css" />

<script type="text/javascript" src="menu/jquery.min.js"></script>
<script type="text/javascript" src="menu/popupmenu.js"></script>
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Customer List</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<a href="form.asp?formType=addCustomer" class="footerLink">add customer</a>&nbsp;&nbsp;
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="expCustomerList.asp?clientID=<%=session("clientID")%>" class="footerLink">export customers to excel</a>&nbsp;&nbsp;			
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr><td></td><td colspan="20"><!--#include file="includes/custNav.asp"--></td></tr>
										<tr><td colspan="20"><img src="images/pix.gif" width="1" height="20"></td></tr>
										
										<tr>
											<td></td>
											<td colspan="8">
												<form name="customerList" method="post" action="customerList.asp">
													<input type="text" name="search" value="<%=sSearchString%>">&nbsp;&nbsp;<input type="submit" name="searchBtn" value="Search" class="formButton">
												</form>										
											</td>
										</tr>
								
										<tr><td></td><td colspan="20"><strong><%if sLetter <> "@" then response.Write sLetter%></strong></td></tr>
										<tr><td colspan="20"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr><td></td><td colspan="20"><strong>NOTE:</strong> hover over a customer name to do a number of tasks related to the customer</td></tr>
										<tr bgcolor="#666666">
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td width="25%"><span class="searchText">Customer Name</span></td>
											<td>&nbsp;&nbsp;</td>
											<td width="15%"><span class="searchText">Client Name</span></td>
											<td>&nbsp;&nbsp;</td>
											<%if session("Admin") = "True" Then%>
											<td><span class="searchText">Hierarchy</span></td>
											<%end if%>
											<td align="center"><span class="searchText">HB/S Client</span></td>
											<td align="center"><span class="searchText">HBTC Client</span></td>
											<!--<td align="center"><span class="searchText">Contacts</span></td>
											<%'if session("Admin") = "True" Then%>
											<td align="center"><span class="searchText">Sub-Contractors</span></td>
											<td align="center"><span class="searchText">Divisions</span></td>																						
											<td align="center"><span class="searchText">Billing Info</span></td>
											<%'end if%>
											<td align="center"><span class="searchText">Quotes</span></td>
											<%if session("Admin") = "True" Then%>
											<td align="center"><span class="searchText">Documents</span></td>-->
											<td align="center"><span class="searchText">Active</span></td>
											<!--<td align="center"><span class="searchText">Edit</span></td>-->
											<td align="center"><span class="searchText">Delete</span></td>
											<%end if%>
										</tr>
										<tr><td colspan="20"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<%If rs.eof then%>
											<tr><td></td><td colspan="20">there are no customer that start with this letter</td></tr>
										<%else
											blnChange = true
											i = 1
											Do until rs.eof
											
												If blnChange = true then%>
													<tr class="rowColor">
												<%else%>
													<tr>
												<%end if%>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<!--<td><a href="activeProjectList.asp?customerID=<%'=rs("customerID")%>" title="<%'=rs("customerName")%> - Active Projects" rel="gb_page_center[820, 500]"><%'=rs("customerName")%></a></td>-->
													<td>
														<a href="#" data-popupmenu="popmenu<%=i%>"><%=rs("customerName")%></a>
														
														<!--popup menu here-->
														<ul id="popmenu<%=i%>" class="jqpopupmenu">
															<li><a href="form.asp?id=<%=rs("customerID")%>&formType=editCustomer">Edit Customer</a></li>
															<li><a href="customerContactList.asp?id=<%=rs("customerID")%>">Contacts</a>
																<!--<ul>
																	<li><a href="form.asp?formType=addCustomerContact&customerID=<%'=rs("customerID")%>">Add Contact</a></li>
																	<li><a href="form.asp?formType=addInfoPack&clientID=<%'=session("clientID")%>&customer=<%'=rs("customerID")%>">Add Info Pack</a></li>
																</ul>-->
															</li>
															<li><a href="divisionList.asp?id=<%=rs("customerID")%>">Divisions</a>
																<%'get a list of the divisions
																Set oCmd = Server.CreateObject("ADODB.Command")	
																With oCmd
																   .ActiveConnection = DataConn
																   .CommandText = "spGetDivisionsByCustomer"
																   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, rs("customerID"))
																   .CommandType = adCmdStoredProc																   
																End With
																Set rsDiv = oCmd.Execute
																Set oCmd = nothing
																%>
																<ul>
																	<%do until rsDiv.eof%>
																		<li><a href="divisionList.asp?id=<%=rs("customerID")%>"><%=rsDiv("division")%></a></li>
																			<ul>
																				<li><a href="projectList.asp?id=<%=rsDiv("divisionID")%>&customerID=<%=rs("customerID")%>">Projects</a></li>
																					<!--<ul>
																						<li><a href="form.asp?formType=addCustomerContact&customerID=<%'=rs("customerID")%>">Add Contact</a></li>
																						<li><a href="form.asp?formType=addInfoPack&clientID=<%'=session("clientID")%>&customer=<%'=rs("customerID")%>">Add Info Pack</a></li>
																					</ul>-->
																				
																				<li><a href="questionList.asp?id=<%=rsDiv("divisionID")%>&customerID=<%=rs("customerID")%>">NPDES Questions</a></li>
																				<li><a href="OSHAquestionList.asp?id=<%=rsDiv("divisionID")%>&customerID=<%=rs("customerID")%>">OSHA Questions</a></li>
																				<li><a href="craneQuestionList.asp?id=<%=rsDiv("divisionID")%>&customerID=<%=rs("customerID")%>">Crane Questions</a></li>
																				<li><a href="inspectionTypeList.asp?id=<%=rsDiv("divisionID")%>&customerID=<%=rs("customerID")%>">Inspection Types</a></li>
																				<li><a href="projectStatusList.asp?id=<%=rsDiv("divisionID")%>&customerID=<%=rs("customerID")%>">Project Status</a></li>
																				<li><a href="weatherList.asp?id=<%=rsDiv("divisionID")%>&customerID=<%=rs("customerID")%>">Weather Conditions</a></li>
																				<li><a href="ecDeviceList.asp?id=<%=rsDiv("divisionID")%>&customerID=<%=rs("customerID")%>">Erosion Control Devices</a></li>
																				<li><a href="form.asp?formType=addDivision&customerID=<%=rs("customerID")%>">Add Division</a></li>
																				<li><a href="form.asp?id=<%=rsDiv("divisionID")%>&formType=editDivision&customerID=<%=rs("customerID")%>">Edit Division</a></li>
																			</ul>
																	<%rsDiv.movenext
																	loop%>
																</ul>
															</li>
															<li><a href="#">Activities</a>
																<ul>
																	<%if Session("viewQuotes") = "True" then%>
																	<li><a href="form.asp?formType=addInfoPack&clientID=<%=session("clientID")%>&customer=<%=rs("customerID")%>">Add Info Pack</a></li>
																	<li><a href="infoPackList.asp?clientID=<%=session("clientID")%>&customer=<%=rs("customerID")%>">View Info Packs</a></li>
																	<li><a href="customerQuoteList.asp?id=<%=rs("customerID")%>">View Quotes</a></li>
																	<li><a href="form.asp?formType=addQuote&clientID=<%=session("clientID")%>">Add Quote</a></li>
																	<%end if%>
																	<li><a href="studentList.asp?clientID=<%=session("clientID")%>&customerID=<%=rs("customerID")%>">Students</a></li>
																	<li><a href="classList.asp?clientID=<%=session("clientID")%>&customerID=<%=rs("customerID")%>">Classes</a></li>
																	<li><a href="">Inspection</a></li>
																	<li><a href="">Software</a></li>
																	<li><a href="">Consulting</a></li>
																	<li><a href="form.asp?formType=editConversations&id=<%=rs("customerID")%>">Conversations</a></li>
																	<li><a href="form.asp?id=<%=rs("customerID")%>&formType=editCustomerBilling">Billing</a></li>
																</ul>
															</li>
															<li><a href="#">Documents</a>
																<ul>
																	<li><a href="docManagerCust.asp?customerID=<%=rs("customerID")%>">Customer Documents</a></li>
																	<li><a href="docManager.asp">Project Documents</a></li>
																</ul>
															</li>
															<li><a href="#">Users</a>
																<%'get a list of the divisions
																Set oCmd = Server.CreateObject("ADODB.Command")	
																With oCmd
																   .ActiveConnection = DataConn
																   .CommandText = "spGetUsersByCustomer"
																   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, rs("customerID"))
																   .CommandType = adCmdStoredProc																   
																End With
																Set rsUser = oCmd.Execute
																Set oCmd = nothing
																%>
																<ul>
																	<%do until rsUser.eof%>
																	<li><a href="form.asp?id=<%=rsUser("userID")%>&formType=editUser"><%=rsUser("firstName")%>&nbsp;<%=rsUser("lastName")%></a></li>
																	<%rsUser.movenext
																	loop%>
																</ul>
														</ul>
														<!--end of popup menu-->
														
													</td>											
													
													<td>&nbsp;</td>
													<td><%=rs("clientName")%></td>
													<td>&nbsp;&nbsp;</td>
													<%if session("Admin") = "True" Then%>
													<td><a href="hierarchyList.asp?id=<%=rs("customerID")%>&parent=<%=rs("parentCustomerID")%>">View</a></td>
													<%end if%>
													<td align="center">
														<%if rs("HBSClient") = "True" then%>
															<img src="images/check.gif">
														<%end if%>
													</td>
													<td align="center">
														<%if rs("HBTCClient") = "True" then%>
															<img src="images/check.gif">
														<%end if%>
													</td>
													<!--<td align="center"><a href="customerContactList.asp?id=<%'=rs("customerID")%>">List Contacts</a></td>
													<%'if session("Admin") = "True" Then%>
													<td align="center"><a href="subList.asp?id=<%'=rs("customerID")%>">List Subs</a></td>
													<td align="center"><a href="divisionList.asp?id=<%'=rs("customerID")%>">List Divisions</a></td>
													<td align="center"><a href="form.asp?id=<%'=rs("customerID")%>&formType=editCustomerBilling">view/edit</a></td>
													<%'end if%>
													<td align="center"><a href="customerQuoteList.asp?id=<%'=rs("customerID")%>">List Quotes</a></td>
													<%if session("Admin") = "True" Then%>
													<td align="center"><a href="docManagerCust.asp?customerID=<%'=rs("customerID")%>">view docs</a></td>-->
													<td align="center">
														<%if rs("isActive") = "True" then%>
															<img src="images/check.gif">
														<%end if%>
													</td>
													<!--<td align="center"><a href="form.asp?id=<%'=rs("customerID")%>&formType=editCustomer"><img src="images/edit.gif" width="19" height="19" alt="edit" border="0"></a></td>-->
													<td align="center"><input type="image" src="images/remove.gif" width="11" height="13" alt="delete" border="0" onClick="return confirmDelete('process.asp?id=<%=rs("customerID")%>&processType=deleteCustomer')"></td>
													<%end if%>
												</tr>
											<%rs.movenext
											i = i+1
											if blnChange = true then
												blnChange = false
											else
												blnChange = true
											end if
											loop
										end if%>
									</table>
									
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>