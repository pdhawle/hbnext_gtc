<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->
<!--#include virtual="/ASPSpellCheck/ASPSpellInclude.inc"-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<head>
<title><%=sPageTitle%></title>
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script language="javascript" src="includes/datePicker.js"></script>
<link rel="stylesheet" href="includes/datePicker.css" type="text/css">
<script language="javascript" src="includes/answerPicker.js"></script>
<link rel="stylesheet" href="includes/answerPicker.css" type="text/css">
<script language="JavaScript" src="includes/gen_validatorv31.js" type="text/javascript"></script>
<script language="javascript" src="includes/mm.js"></script>
<script language=JavaScript src='scripts/innovaeditor.js'></script>
<SCRIPT LANGUAGE="JavaScript" SRC="includes/timepicker.js"></SCRIPT>
<script type="text/javascript">
    var GB_ROOT_DIR = "<%=sPDFPath%>/greybox/";
</script>
<script type="text/javascript" src="greybox/AJS.js"></script>
<script type="text/javascript" src="greybox/AJS_fx.js"></script>
<script type="text/javascript" src="greybox/gb_scripts.js"></script>
<link href="greybox/gb_styles.css" rel="stylesheet" type="text/css" />

<script language="javascript" src="includes/OSHACommentPicker.js"></script>

<script type="text/javascript" src="jquery/jquery.js"></script>
</head>

<%
Dim rs, rsState, oCmd, DataConn

'Response.Buffer = False

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If
%>
<body>
	<%
		dim sFormType
		sFormType = request("formType")
		
		Select case sFormType
			Case "editUser"%>
				<!--#include file="edit_form_User.asp"-->
				
			<%case "editQuestion"%>
				<!--#include file="edit_form_Question.asp"-->
				
			<%case "addQuestion"%>
				<!--#include file="form_Question.asp"-->
			
			<%case "editCategory"%>
				<!--#include file="edit_form_Category.asp"-->	
				
			<%case "addCategory"%>
				<!--#include file="form_Category.asp"-->
				
			<%case "addCustomer"%>
				<!--#include file="form_Customer.asp"-->
				
			<%case "editCustomer"%>
				<!--#include file="edit_form_Customer.asp"-->
				
			<%case "addInspectionType"%>
				<!--#include file="form_InspectionType.asp"-->
				
			<%case "editInspectionType"%>
				<!--#include file="edit_form_InspectionType.asp"-->
				
			<%case "addWeatherType"%>
				<!--#include file="form_Weather.asp"-->
				
			<%case "editWeatherType"%>
				<!--#include file="edit_form_Weather.asp"-->
				
			<%case "addReport"%>
				<!--#include file="form_Report.asp"-->
				
			<%case "editReport"%>
				<!--#include file="edit_form_Report.asp"-->
				
			<%case "addDivision"%>
				<!--#include file="form_Division.asp"-->
				
			<%case "editDivision"%>
				<!--#include file="edit_form_Division.asp"-->
				
			<%case "addProject"%>
				<!--#include file="form_Project.asp"-->
				
			<%case "editProject"%>
				<!--#include file="edit_form_Project.asp"-->
			
			<%Case "sendEmail"%>
				<!--#include file="form_email.asp"-->
				
			<%Case "addContact"%>
				<!--#include file="form_Contact.asp"-->
				
			<%Case "editContact"%>
				<!--#include file="edit_form_Contact.asp"-->
				
			<%Case "addProjectStatus"%>
				<!--#include file="form_ProjectStatus.asp"-->
				
			<%Case "editProjectStatus"%>
				<!--#include file="edit_form_ProjectStatus.asp"-->
				
			<%Case "addReportType"%>
				<!--#include file="form_ReportType.asp"-->
				
			<%Case "editReportType"%>
				<!--#include file="edit_form_ReportType.asp"-->
				
			<%Case "addLots"%>
				<!--#include file="form_Lots.asp"-->
				
			<%Case "addUtility"%>
				<!--#include file="form_Utility.asp"-->
				
			<%Case "editUtility"%>
				<!--#include file="edit_form_Utility.asp"-->
				
			<%Case "addStreet"%>
				<!--#include file="form_Street.asp"-->
			
			<%Case "editStreet"%>
				<!--#include file="edit_form_Street.asp"-->
				
			<%Case "editLot"%>
				<!--#include file="edit_form_Lot.asp"-->
				
			<%Case "primary"%>
				<!--#include file="form_primary.asp"-->
				
			<%Case "editPrimary"%>
				<!--#include file="edit_form_primary.asp"-->
				
			<%Case "addFeeForm"%>
				<!--#include file="form_Fee.asp"-->
				
			<%Case "editFeeForm"%>
				<!--#include file="edit_form_Fee.asp"-->
				
			<%Case "secondary"%>
				<!--#include file="form_secondary.asp"-->
				
			<%Case "editSecondary"%>
				<!--#include file="edit_form_secondary.asp"-->				
			
			<%Case "addSecNOTForm"%>
				<!--#include file="form_secNOT.asp"-->
				
			<%Case "editSecNOTForm"%>
				<!--#include file="edit_form_secNOT.asp"-->
				
			<%Case "addNOTForm"%>
				<!--#include file="form_NOT.asp"-->
				
			<%Case "editNOTForm"%>
				<!--#include file="edit_form_NOT.asp"-->				
			
			<%Case "sendNOIEmail"%>
				<!--#include file="form_NOI_email.asp"-->

			<%Case "addECDevice"%>
				<!--#include file="form_ECDevice.asp"-->
				
			<%Case "editECDevice"%>
				<!--#include file="edit_form_ECDevice.asp"-->
				
			<%Case "editRates"%>
				<!--#include file="edit_form_Rates.asp"-->
				
			<%Case "editAlerts"%>
				<!--#include file="edit_form_Alerts.asp"-->
				
			<%Case "addEvent"%>
				<!--#include file="form_Event.asp"-->
				
			<%Case "editEvent"%>
				<!--#include file="edit_form_Event.asp"-->
				
			<%Case "shareCalendar"%>
				<!--#include file="form_shareCalendar.asp"-->
				
			<%Case "addMaterial"%>
				<!--#include file="form_Material.asp"-->
				
			<%Case "editMaterial"%>
				<!--#include file="edit_form_Material.asp"-->
				
			<%Case "addWorkOrder"%>
				<!--#include file="form_WorkOrder.asp"-->
			
			<%Case "addJournal"%>
				<!--#include file="form_Journal.asp"-->	
				
			<%Case "editJournal"%>
				<!--#include file="edit_form_Journal.asp"-->	
				
			<%Case "shareJournal"%>
				<!--#include file="form_shareJournal.asp"-->
				
			<%Case "editCustomerBilling"%>
				<!--#include file="edit_form_CustomerBilling.asp"-->
				
			<%Case "addPerformance"%>
				<!--#include file="form_Performance.asp"-->	
				
			<%Case "addSiteNews"%>
				<!--#include file="form_SiteNews.asp"-->
				
			<%Case "editSiteNews"%>
				<!--#include file="edit_form_SiteNews.asp"-->

			<%Case "addEquipReport"%>
				<!--#include file="form_EquipReport.asp"-->	
				
			<%Case "addQuote"%>
				<!--#include file="form_Quote.asp"-->

			<%Case "editQuote"%>
				<!--#include file="edit_form_Quote.asp"-->	
				
			<%Case "assignCustomer"%>
				<!--#include file="form_AssignCustomer.asp"-->

			<%Case "editAssignCustomer"%>
				<!--#include file="edit_form_AssignCustomer.asp"-->
				
			<%Case "sendFax"%>
				<!--#include file="form_SendFax.asp"-->
				
			<%Case "addQuoteComments"%>
				<!--#include file="form_QuoteComments.asp"-->
				
			<%case "addCustomerContact"%>
				<!--#include file="form_CustomerContact.asp"-->
				
			<%case "editCustomerContact"%>
				<!--#include file="edit_form_CustomerContact.asp"-->

			<%case "addAccidentReport"%>
				<!--#include file="form_AccidentReport.asp"-->

			<%case "addWorkComp"%>
				<!--#include file="form_WorkComp.asp"-->

			<%case "addNoticeOfSeparation"%>
				<!--#include file="form_NoticeOfSeparation.asp"-->

			<%case "addPLS"%>
				<!--#include file="form_PLS.asp"-->

			<%case "editPLS"%>
				<!--#include file="edit_form_PLS.asp"-->

			<%case "activatePLS"%>
				<!--#include file="form_ActivatePLS.asp"-->
			
			<%case "editAccidentReport"%>
				<!--#include file="edit_form_AccidentReport.asp"-->

			<%case "assignSupervisor"%>
				<!--#include file="form_AssignSupervisor.asp"-->

			<%Case "addTime"%>
				<!--#include file="form_Time.asp"-->
				
			<%Case "editTime"%>
				<!--#include file="edit_form_Time.asp"-->

			<%Case "addInfoPack"%>
				<!--#include file="form_InfoPack.asp"-->

			<%Case "previewInfoPack"%>
				<!--#include file="form_PreviewInfoPack.asp"-->

			<%Case "sendFaxInfo"%>
				<!--#include file="form_SendFaxInfo.asp"-->

			<%Case "editInfoPack"%>
				<!--#include file="edit_form_InfoPack.asp"-->

			<%Case "addOSHA301"%>
				<!--#include file="form_OSHA301.asp"-->

			<%Case "editOSHA301"%>
				<!--#include file="edit_form_OSHA301.asp"-->

			<%case "addOSHASub"%>
				<!--#include file="form_OSHASub.asp"-->

			<%case "editOSHASub"%>
				<!--#include file="edit_form_OSHASub.asp"-->

			<%Case "sendOSHAEmail"%>
				<!--#include file="form_OSHAEmail.asp"-->

			<%case "addOSHAReport"%>
				<!--#include file="form_OSHAReport.asp"-->

			<%case "editOSHAReport"%>
				<!--#include file="edit_form_OSHAReport.asp"-->

			<%case "addSub"%>
				<!--#include file="form_Sub.asp"-->

			<%case "editSub"%>
				<!--#include file="edit_form_Sub.asp"-->

			<%case "addOSHAQuestion"%>
				<!--#include file="form_OSHAQuestion.asp"-->

			<%case "editOSHAQuestion"%>
				<!--#include file="edit_form_OSHAQuestion.asp"-->

			<%case "editConversations"%>
				<!--#include file="edit_form_Conversations.asp"-->

			<%Case "addCompliance"%>
				<!--#include file="form_Compliance.asp"-->

			<%case "addCraneCategory"%>
				<!--#include file="form_CraneCategory.asp"-->

			<%case "editCraneCategory"%>
				<!--#include file="edit_form_CraneCategory.asp"-->

			<%case "addCraneQuestion"%>
				<!--#include file="form_CraneQuestion.asp"-->

			<%case "editCraneQuestion"%>
				<!--#include file="edit_form_CraneQuestion.asp"-->

			<%case "addCrane"%>
				<!--#include file="form_Crane.asp"-->

			<%case "editCrane"%>
				<!--#include file="edit_form_Crane.asp"-->

			<%Case "addTopic"%>
				<!--#include file="form_Topic.asp"-->

			<%Case "editTopic"%>
				<!--#include file="edit_form_Topic.asp"-->

			<%Case "toolboxSession"%>
				<!--#include file="form_ToolboxSession.asp"-->

			<%Case "addClass"%>
				<!--#include file="form_Class.asp"-->

			<%Case "editClass"%>
				<!--#include file="edit_form_Class.asp"-->

			<%Case "addStudent"%>
				<!--#include file="form_Student.asp"-->

			<%Case "editStudent"%>
				<!--#include file="edit_form_Student.asp"-->

			<%Case "addClassType"%>
				<!--#include file="form_ClassType.asp"-->

			<%Case "editClassType"%>
				<!--#include file="edit_form_ClassType.asp"-->

			<%Case "addMiles"%>
				<!--#include file="form_Miles.asp"-->

			<%Case "editMiles"%>
				<!--#include file="edit_form_Miles.asp"-->
				
			<%Case "addLocation"%>
				<!--#include file="form_Location.asp"-->
				
			<%Case "editLocation"%>
				<!--#include file="edit_form_Location.asp"-->

			<%Case "addContractor"%>
				<!--#include file="form_Contractor.asp"-->

			<%Case "editContractor"%>
				<!--#include file="edit_form_Contractor.asp"-->

			<%Case "addContractorCert"%>
				<!--#include file="form_ContractorCert.asp"-->

			<%Case "editContractorCert"%>
				<!--#include file="edit_form_ContractorCert.asp"-->
				
		<%End select
	%>
</body>
</html>
