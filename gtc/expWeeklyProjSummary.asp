<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
projectID = request("projectID")
dtFrom = request("fromDate")
dtTo = request("toDate")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

If projectID = "all" then

	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetWeeklySummaryListByDateAll"
	   .parameters.Append .CreateParameter("@fromDate", adDBTimeStamp, adParamInput, 8, dtFrom)
	   .parameters.Append .CreateParameter("@toDate", adDBTimeStamp, adParamInput, 8, dtTo)
	   .CommandType = adCmdStoredProc   
	End With
				
	Set rs = oCmd.Execute
	Set oCmd = nothing
	
else

	Set oCmd = Server.CreateObject("ADODB.Command")
		
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetWeeklySummaryListByDate"
	   .parameters.Append .CreateParameter("@id", adInteger, adParamInput, 8, projectID)
	   .parameters.Append .CreateParameter("@fromDate", adDBTimeStamp, adParamInput, 8, dtFrom)
	   .parameters.Append .CreateParameter("@toDate", adDBTimeStamp, adParamInput, 8, dtTo)
	   .CommandType = adCmdStoredProc   
	End With
				
	Set rs = oCmd.Execute
	Set oCmd = nothing
	
end if


Response.ContentType = "application/vnd.ms-excel"
Response.AddHeader "Content-Disposition", "attachment; filename=weeklyProjectSummary.xls" %>
		<table border=1>
			<tr bgcolor="#666666"><td colspan="5"><font color="#EEEEEE"><strong>Date Range: <%=dtFrom%> - <%=dtTo%></strong></font></td></tr>
			<tr bgcolor="#666666">
				<td><font color="#EEEEEE"><strong>Customer</strong></font></td>
				<td><font color="#EEEEEE"><strong>Project</strong></font></td>
				<td><font color="#EEEEEE"><strong>Entered By</strong></font></td>
				<td><font color="#EEEEEE"><strong>Date Entered</strong></font></td>
				<td><font color="#EEEEEE"><strong>Summary Entry</strong></font></td>
			</tr>
	<%do until rs.eof%>
		<tr>
			<td valign="top"><%=rs("customerName")%></td>
			<td valign="top"><%=rs("projectName")%></td>
			<td valign="top"><%=rs("firstName")%>&nbsp;<%=rs("lastName")%></td>
			<td valign="top"><%=formatdatetime(rs("dateEntered"),2)%></td>
			<td valign="top"><%=rs("summary")%></td>
		</tr>
	<%
	rs.movenext
	loop%>
	</table>
<%rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing%>

