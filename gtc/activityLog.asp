<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
Dim rs, oCmd, DataConn

iReportID = request("reportID")
t = request("t")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

Select Case t
	Case "al"
		sp = "spGetActivityLog"
	Case else
		sp = "spGetReport"
end select
'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = sp
   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, iReportID)
   .CommandType = adCmdStoredProc   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rs = oCmd.Execute
Set oCmd = nothing


Set oCmd = Server.CreateObject("ADODB.Command")	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomerByReportID"
   .parameters.Append .CreateParameter("@reportID", adInteger, adParamInput, 8, iReportID)
   .CommandType = adCmdStoredProc   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsInfo = oCmd.Execute
Set oCmd = nothing

%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sNPDESTitle%></span><span class="Header"> - 
								<%select case t
									case "al"%>
										Report Activity Log
									<%case else%>
										Report Information
								<%end select%></span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<a href="activityLog.asp?reportID=<%=iReportID%>" class="footerLink">Report Information</a>&nbsp;&nbsp;
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="activityLog.asp?reportID=<%=iReportID%>&t=al" class="footerLink">Report Activity Log</a>&nbsp;&nbsp;
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="downloads/swsir_<%=iReportID%>.pdf" target="_blank" class="footerLink">View Report</a>			
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<%select case t
											case "al"%>
												<tr><td colspan="5">Customer: <strong><%=rsInfo("customerName")%></strong></td></tr>
												<tr><td colspan="5">Division: <strong><%=rsInfo("division")%></strong></td></tr>
												<tr><td colspan="5">Project: <strong><%=rsInfo("projectName")%></strong></td></tr>
												<tr><td colspan="15"><img src="images/pix.gif" width="1" height="3"></td></tr>
												<tr bgcolor="#666666">
													<td>&nbsp;&nbsp;<span class="searchText">Date/Time</span></td>
													<td><span class="searchText">User</span></td>
													<td><span class="searchText">Event Type</span></td>
												</tr>
												<tr><td colspan="7"><img src="images/pix.gif" width="1" height="10"></td></tr>
												<%If rs.eof then%>
													<tr><td colspan="7">there is no activity to display</td></tr>
												<%else
													blnChange = true
													Do until rs.eof
														If blnChange = true then%>
															<tr class="rowColor">
														<%else%>
															<tr>
														<%end if%>
															<td>			
																&nbsp;&nbsp;<%=rs("activityDate")%>								
															</td>
															<td>
																<%=rs("firstName") & " " & rs("lastName")%>
															</td>
															<td>			
																<%=rs("eventType")%>								
															</td>
														</tr>
														<%If blnChange = true then%>
															<tr class="rowColor">
														<%else%>
															<tr>
														<%end if%>
															<td colspan="5">			
																&nbsp;&nbsp;<strong>Details:</strong> <%=rs("eventDetails")%><br>								
															</td>
														</tr>
														<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>
													<%rs.movenext
													if blnChange = true then
														blnChange = false
													else
														blnChange = true
													end if
													loop
												end if
												
											case else%>
												<tr><td></td><td colspan="5">Customer: <strong><%=rsInfo("customerName")%></strong></td></tr>
												<tr><td colspan="15"><img src="images/pix.gif" width="1" height="3"></td></tr>
												<tr>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td bgcolor="#666666"><span class="searchText">&nbsp;ID:</span></td>
													<td>&nbsp;</td>
													<td class="rowColor">&nbsp;<%=rs("reportID")%></td>
												</tr>
												<tr><td colspan="7"><img src="images/pix.gif" width="1" height="1"></td></tr>
												<tr>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td bgcolor="#666666"><span class="searchText">&nbsp;Division:</span></td>
													<td>&nbsp;</td>
													<td>&nbsp;<%=rs("division")%></td>
												</tr>
												<tr><td colspan="7"><img src="images/pix.gif" width="1" height="1"></td></tr>
												<tr>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td bgcolor="#666666"><span class="searchText">&nbsp;Project:</span></td>
													<td>&nbsp;</td>
													<td class="rowColor">&nbsp;<%=rs("projectName")%></td>
												</tr>
												<tr><td colspan="7"><img src="images/pix.gif" width="1" height="1"></td></tr>
												<tr>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td bgcolor="#666666"><span class="searchText">&nbsp;Inspector:</span></td>
													<td>&nbsp;</td>
													<td>&nbsp;<%=rs("firstName") & " " & rs("lastName")%></td>
												</tr>
												<tr><td colspan="7"><img src="images/pix.gif" width="1" height="1"></td></tr>
												<tr>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td bgcolor="#666666"><span class="searchText">&nbsp;Report Type:</span></td>
													<td>&nbsp;</td>
													<td class="rowColor">&nbsp;<%=rs("reportType")%></td>
												</tr>
												<tr><td colspan="7"><img src="images/pix.gif" width="1" height="1"></td></tr>
												<tr>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td bgcolor="#666666"><span class="searchText">&nbsp;Project Status:</span></td>
													<td>&nbsp;</td>
													<td>&nbsp;<%=rs("projectStatus")%></td>
												</tr>
												<tr><td colspan="7"><img src="images/pix.gif" width="1" height="1"></td></tr>
												<tr>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td bgcolor="#666666"><span class="searchText">&nbsp;Inspection Type:</span></td>
													<td>&nbsp;</td>
													<td class="rowColor">&nbsp;<%=rs("inspectionType")%></td>
												</tr>
												<tr><td colspan="7"><img src="images/pix.gif" width="1" height="1"></td></tr>
												<tr>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td bgcolor="#666666"><span class="searchText">&nbsp;Date of inspection:</span></td>
													<td>&nbsp;</td>
													<td>&nbsp;<%=rs("inspectionDate")%></td>
												</tr>
												<tr><td colspan="7"><img src="images/pix.gif" width="1" height="1"></td></tr>
												<tr>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td bgcolor="#666666"><span class="searchText">&nbsp;Date Added:</span></td>
													<td>&nbsp;</td>
													<td class="rowColor">&nbsp;<%=rs("dateAdded")%></td>
												</tr>
												<tr><td colspan="7"><img src="images/pix.gif" width="1" height="1"></td></tr>
												<tr>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td bgcolor="#666666"><span class="searchText">&nbsp;Date Modified:</span></td>
													<td>&nbsp;</td>
													<td>&nbsp;<%=rs("dateModified")%></td>
												</tr>
												<tr><td colspan="7"><img src="images/pix.gif" width="1" height="1"></td></tr>
												<tr>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td bgcolor="#666666"><span class="searchText">&nbsp;Comments:</span></td>
													<td>&nbsp;</td>
													<td class="rowColor">&nbsp;<%=rs("comments")%></td>
												</tr>
												<tr><td></td><td></td><td></td><td><img src="images/pix.gif" width="600" height="10"></td></tr>
											<%end select%>
									</table>
									
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>