<%
'lotID = request("lotID")
'lotNumber = request("lotNumber")
'projectName = request("projectName")
divisionID = request("divisionID")
customerID = request("customerID")
'projectID = request("projectID")
pg = request("pg")
locateLogID = request("locateLogID")

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetUtility"
	.parameters.Append .CreateParameter("@locateLogID", adInteger, adParamInput, 8, locateLogID)
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rs = oCmd.Execute
Set oCmd = nothing
%>
<form name="editUtility" method="post" action="process.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Edit Utility Locate Log</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td valign="top"><strong>Project:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%=rs("projectName")%>					
											</td>
										</tr>	
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top"><strong>Lot Number:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%=rs("lotNumber")%>
												<input type="hidden" name="lotNumber" value="<%=lotNumber%>" />							
											</td>
										</tr>	
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top"><span class="required">*</span> <strong>Address:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="address" size="30" maxlength="150" value="<%=rs("address")%>" />						
											</td>
										</tr>				
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top"><strong>Confirmation #:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="confirmationNumber" size="30" maxlength="100" value="<%=rs("confirmationNumber")%>" />						
											</td>
										</tr>				
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top"><strong>Contractor:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="contractor" size="30" maxlength="100" value="<%=rs("contractor")%>" />						
											</td>
										</tr>				
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top"><strong>Date Begins:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="dateBegins" maxlength="10" size="10" value="<%=rs("dateBegins")%>"/>&nbsp;<a href="javascript:displayDatePicker('dateBegins')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>				
											</td>
										</tr>				
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top"><strong>Renew By:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="renewBy" maxlength="10" size="10" value="<%=rs("renewBy")%>"/>&nbsp;<a href="javascript:displayDatePicker('renewBy')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>				
											</td>
										</tr>				
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top"><strong>Date Ends:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="dateEnds" maxlength="10" size="10" value="<%=rs("dateEnds")%>"/>&nbsp;<a href="javascript:displayDatePicker('dateEnds')"><img alt="Pick a date" src="images/date.gif" border="0" width="17" height="16"></a>				
											</td>
										</tr>				
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td colspan="3" align="right">
												<input type="hidden" name="locateLogID" value="<%=locateLogID%>"/>
												<input type="hidden" name="lotID" value="<%=rs("lotID")%>"/>
												<input type="hidden" name="projectID" value="<%=rs("projectID")%>"/>
												<input type="hidden" name="divisionID" value="<%=divisionID%>"/>
												<input type="hidden" name="customerID" value="<%=customerID%>"/>
												<input type="hidden" name="pg" value="<%=pg%>"/>
												<input type="hidden" name="processType" value="editUtility" />
												<input type="submit" value="  Save  " class="formButton"/>
											</td>
										</tr>
									</table>
								
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<script language="JavaScript" type="text/javascript">

  var frmvalidator  = new Validator("editUtility");
  frmvalidator.addValidation("address","req","Please enter the address");
 // frmvalidator.addValidation("confirmationNumber","req","Please enter the confirmation number");
  //frmvalidator.addValidation("contractor","req","Please enter the contractor");
 // frmvalidator.addValidation("dateBegins","req","Please enter the begin date");
 // frmvalidator.addValidation("renewBy","req","Please enter the renew by date");
  //frmvalidator.addValidation("dateEnds","req","Please enter the end date");
</script>