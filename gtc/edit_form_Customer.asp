<%
On Error Resume Next

iCustomerID = request("id")
sState = request("state")


Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

'Create command for state list
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetStates"
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsState = oCmd.Execute
Set oCmd = nothing

'Create command for customer
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomer"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iCustomerID)
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsCustomer = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomers"
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsCustomerList = oCmd.Execute
Set oCmd = nothing

'Create command for state list
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomerTypes"
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsTypes = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetUserListForClient"
   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, session("clientID"))
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsAM = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetUserListForClient"
   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, session("clientID"))
   .CommandType = adCmdStoredProc
   
End With

Set rsArea = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetUserListForClient"
   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, session("clientID"))
   .CommandType = adCmdStoredProc
   
End With

Set rsEst = oCmd.Execute
Set oCmd = nothing


'check th value of the state
if rsCustomer("state") <> "" then
	if sState = "" then
		sState = rsCustomer("state")
	end if
else
	sState = sState							
end if

'response.Write "s " & sState

If sState <> "" then

	Set oCmd = Server.CreateObject("ADODB.Command")
	
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetCountiesByState"
	   .parameters.Append .CreateParameter("@ID", adVarchar, adParamInput, 50, trim(sState))
	   .CommandType = adCmdStoredProc   
	End With
		
	Set rsCounty = oCmd.Execute
	Set oCmd = nothing

end if

%>
<script type="text/javascript">
<!--
//function proj_onchange(addProject,stateID,customerID,divisionID) {
 //  window.location.href = 'form.asp?formType=addProject&state='+stateID+'&id='+customerID+'&divisionID='+divisionID;
//}

function cust_onchange(editCustomer) {
   document.editCustomer.action = "form.asp?formType=editCustomer";
   editCustomer.submit(); 
}

var isNN = (navigator.appName.indexOf("Netscape")!=-1);
function autoTab(input,len, e) {
var keyCode = (isNN) ? e.which : e.keyCode; 
var filter = (isNN) ? [0,8,9] : [0,8,9,16,17,18,37,38,39,40,46];
if(input.value.length >= len && !containsElement(filter,keyCode)) {
input.value = input.value.slice(0, len);
input.form[(getIndex(input)+1) % input.form.length].focus();
}
function containsElement(arr, ele) {
var found = false, index = 0;
while(!found && index < arr.length)
if(arr[index] == ele)
found = true;
else
index++;
return found;
}
function getIndex(input) {
var index = -1, i = 0, found = false;
while (i < input.form.length && index == -1)
if (input.form[i] == input)index = i;
else i++;
return index;
}
return true;
}
// -->
</script>
<!--<li class="current">
	<a href="#ab">menu item</a>
	<ul>
		<li class="current"><a href="#">menu item</a></li>
		<li><a href="#aba">menu item</a></li>
		<li><a href="#abb">menu item</a></li>
		<li><a href="#abc">menu item</a></li>
		<li><a href="#abd">menu item</a></li>
	</ul>
</li>
-->
<form name="editCustomer" method="post" action="process.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Edit Customer</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td valign="top" align="right"><strong>Customer Name:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="customerName" size="50" value="<%=rsCustomer("customerName")%>" tooltipText="Enter the customer name in this box." />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Established By:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%if trim(request("acctEstBy")) = "" then
													acctEstBy = trim(rsCustomer("acctEstBy"))
												else
													acctEstBy = trim(request("acctEstBy"))
												end if%>
												<select name="acctEstBy">
													<option value="0"></option>
													<%do until rsEst.eof
														if trim(acctEstBy) = trim(rsEst("userID")) then%>
															<option selected="selected" value="<%=rsEst("userID")%>"><%=rsEst("firstName")%>&nbsp;<%=rsEst("lastName")%></option>
														<%else%>
															<option value="<%=rsEst("userID")%>"><%=rsEst("firstName")%>&nbsp;<%=rsEst("lastName")%></option>
													<%end if
													rsEst.movenext
													loop%>
												</select>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Customer Type:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="customerTypeID">
													<%do until rsTypes.eof
														if rsCustomer("customerTypeID") = rsTypes("customerTypeID") then%>
															<option selected="selected" value="<%=rsTypes("customerTypeID")%>"><%=rsTypes("customerType")%></option>
														<%else%>
															<option value="<%=rsTypes("customerTypeID")%>"><%=rsTypes("customerType")%></option>
													<%end if
													rsTypes.movenext
													loop%>
												</select>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Parent Account:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="parentCustomerID" tooltipText="If this is company has a parent company in our database, please select it here.">
													<option value="">--Select Parent Account---</option>
													<%do until rsCustomerList.eof
														if rsCustomer("parentCustomerID") = rsCustomerList("customerID") then%>
															<option selected="selected" value="<%=rsCustomerList("customerID")%>"><%=rsCustomerList("customerName")%></option>
														<%else%>
															<option value="<%=rsCustomerList("customerID")%>"><%=rsCustomerList("customerName")%></option>
													<%end if
													rsCustomerList.movenext
													loop%>
												</select>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Primary Consultant:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="areaManager">
													<option value="">--Select Area Manager---</option>
													<%do until rsArea.eof
														if rsCustomer("areaManager") = rsArea("userID") then%>
															<option selected="selected" value="<%=rsArea("userID")%>"><%=rsArea("firstName")%>&nbsp;<%=rsArea("lastName")%></option>
														<%else%>
															<option value="<%=rsArea("userID")%>"><%=rsArea("firstName")%>&nbsp;<%=rsArea("lastName")%></option>
													<%end if
													rsArea.movenext
													loop%>
												</select>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Secondary Consultant:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="accountManagerID">
													<option value="">--Select Account Manager---</option>
													<%do until rsAM.eof
														if rsCustomer("accountManagerID") = rsAM("userID") then%>
															<option selected="selected" value="<%=rsAM("userID")%>"><%=rsAM("firstName")%>&nbsp;<%=rsAM("lastName")%></option>
														<%else%>
															<option value="<%=rsAM("userID")%>"><%=rsAM("firstName")%>&nbsp;<%=rsAM("lastName")%></option>
													<%end if
													rsAM.movenext
													loop%>
												</select>
											</td>
										</tr>
										<!--<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Contact Name:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%'if isnull(rsCustomer("contactName")) then
													'contactName = request("contactName")
												'else
												'	contactName = rsCustomer("contactName")
												'end if%>
												<input type="text" name="contactName" size="30" value="<%'=contactName%>" tooltipText="Enter the customer contact name in this box." />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Contact Email:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%'if isnull(rsCustomer("contactEmail")) then
												'	contactEmail = request("contactEmail")
												'else
												'	contactEmail = rsCustomer("contactEmail")
												'end if%>
												<input type="text" name="contactEmail" size="30" value="<%'=contactEmail%>" tooltipText="Enter the customer contact email in this box." />
											</td>
										</tr>-->
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>				
										<tr>
											<td valign="top" align="right"><strong>Address 1:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%if isnull(rsCustomer("address1")) then
													address1 = request("address1")
												else
													address1 = rsCustomer("address1")
												end if%>
												<input type="text" name="address1" size="30" value="<%=address1%>" tooltipText="Enter the customer's mailing address in this box."/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Address 2:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%if isnull(rsCustomer("address2")) then
													address2 = request("address2")
												else
													address2 = rsCustomer("address2")
												end if%>
												<input type="text" name="address2" size="30" value="<%=address2%>" />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>City:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%if isnull(rsCustomer("city")) then
													city = request("city")
												else
													city = rsCustomer("city")
												end if%>
												<input type="text" name="city" size="30" value="<%=city%>" tooltipText="Enter the customer's city in this box." />
											</td>
										</tr>				
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>State:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td><!-- onChange="return cust_onchange(editCustomer)"-->
												<select name="state">
													<%do until rsState.eof
														if trim(sstate) = trim(rsState("stateID")) then%>
															<option selected="selected" value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
														<%else%>
															<option value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
														<%end if
													rsState.movenext
													loop%>
												</select>&nbsp;&nbsp;<strong>Zip:</strong>&nbsp;<input type="text" name="zip" size="5" value="<%=rsCustomer("zip")%>" tooltipText="Enter the customer's zip code in this box." />
											</td>
										</tr>
										<!--<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>County:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%'if sState = "" then%>
													Please select a state above.
												<%'else%>
													<select name="county">
														<%'do until rsCounty.eof
															'if trim(rsCustomer("countyID")) = trim(rsCounty("countyID")) then%>
																<option selected="selected" value="<%'=rsCounty("countyID")%>"><%'=rsCounty("county")%></option>
															<%'else%>
																<option value="<%'=rsCounty("countyID")%>"><%'=rsCounty("county")%></option>
														<%'end if
														'rsCounty.movenext
														'loop%>
													</select>
												<%'end if%>
												
											</td>
										</tr>-->
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Office Phone:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%'if isnull(rsCustomer("phone")) then
													'phone = request("phone")
												'else
													'phone = rsCustomer("phone")
												'end if
												
												'parse the phone number
												phone = stripPhone(rsCustomer("phone"))
												
												phone1 = firstThree(phone)
												phone2 = secondThree(phone)
												phone3 = lastFour(phone)%>
												
												<input type="text" name="phone1" size="3" value="<%=phone1%>" maxlength="3" onKeyUp="return autoTab(this, 3, event);"/>&nbsp;
												<input type="text" name="phone2" size="3" value="<%=phone2%>" maxlength="3" onKeyUp="return autoTab(this, 3, event);"/>&nbsp;
												<input type="text" name="phone3" size="3" value="<%=phone3%>" maxlength="4" onKeyUp="return autoTab(this, 4, event);"/>&nbsp;
												ext.&nbsp;<input type="text" name="ext" size="3" value="<%=rsCustomer("ext")%>"/>
												<!--<input type="text" name="phone" size="30" value="<%'=phone%>" tooltipText="Enter the customers contact's phone number in this box." />-->
											</td>
										</tr>
										<!--<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Cell Phone:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%'if isnull(rsCustomer("cellPhone")) then
												'	cellPhone = request("cellPhone")
												'else
												'	cellPhone = rsCustomer("cellPhone")
												'end if
										'		cellPhone = stripPhone(rsCustomer("cellPhone"))
												
										'		cellPhone1 = firstThree(cellPhone)
										'		cellPhone2 = secondThree(cellPhone)
										'		cellPhone3 = lastFour(cellPhone)
												%>
												<input type="text" name="cellPhone1" size="3" value="<%'=cellPhone1%>" maxlength="3" onKeyUp="return autoTab(this, 3, event);"/>&nbsp;
												<input type="text" name="cellPhone2" size="3" value="<%'=cellPhone2%>" maxlength="3" onKeyUp="return autoTab(this, 3, event);"/>&nbsp;
												<input type="text" name="cellPhone3" size="3" value="<%'=cellPhone3%>" maxlength="4" onKeyUp="return autoTab(this, 4, event);"/>
											</td>
										</tr>-->
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Fax:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<%'if isnull(rsCustomer("fax")) then
												'	fax = request("fax")
												'else
												'	fax = rsCustomer("fax")
												'end if
												fax = stripPhone(rsCustomer("fax"))
												
												fax1 = firstThree(fax)
												fax2 = secondThree(fax)
												fax3 = lastFour(fax)
												%>
												<input type="text" name="fax1" size="3" value="<%=fax1%>" maxlength="3" onKeyUp="return autoTab(this, 3, event);"/>&nbsp;
												<input type="text" name="fax2" size="3" value="<%=fax2%>" maxlength="3" onKeyUp="return autoTab(this, 3, event);"/>&nbsp;
												<input type="text" name="fax3" size="3" value="<%=fax3%>" maxlength="4" onKeyUp="return autoTab(this, 4, event);"/>
												<!--<input type="text" name="fax" size="30" value="<%'=fax%>" />-->
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Website:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="website" size="30" value="<%=rsCustomer("website")%>" maxlength="150"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>SIC:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="SIC1" size="1" maxlength="1" value="<%=rsCustomer("SIC1")%>" />&nbsp;<input type="text" name="SIC2" size="1" maxlength="1" value="<%=rsCustomer("SIC2")%>" />&nbsp;<input type="text" name="SIC3" size="1" maxlength="1" value="<%=rsCustomer("SIC3")%>" />&nbsp;<input type="text" name="SIC4" size="1" maxlength="1" value="<%=rsCustomer("SIC4")%>" />&nbsp;<em>(Standard Industrial Classification)</em>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>NAICS:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="NAICS1" size="1" maxlength="1" value="<%=rsCustomer("NAICS1")%>" />&nbsp;<input type="text" name="NAICS2" size="1" maxlength="1" value="<%=rsCustomer("NAICS2")%>" />&nbsp;<input type="text" name="NAICS3" size="1" maxlength="1" value="<%=rsCustomer("NAICS3")%>" />&nbsp;<input type="text" name="NAICS4" size="1" maxlength="1" value="<%=rsCustomer("NAICS4")%>" />&nbsp;<input type="text" name="NAICS5" size="1" maxlength="1" value="<%=rsCustomer("NAICS5")%>" />&nbsp;<input type="text" name="NAICS6" size="1" maxlength="1" value="<%=rsCustomer("NAICS6")%>" />&nbsp;<em>(North American Industrial Classification)</em>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Annual Average Employees:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="annualAverageEmployees" size="3" value="<%=rsCustomer("annualAverageEmployees")%>" />&nbsp;<em>enter a number</em>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Total Hours Worked:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="totalHours" size="3" value="<%=rsCustomer("totalHours")%>" />&nbsp;<em>enter the total hours worked last year by all employees</em>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Report Logo Path:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="logo" size="30" value="<%=rsCustomer("logo")%>" tooltipText="Enter the path of the logo you want displayed on each report that is created for this customer. The NEXT logo is displayed by default." />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Active Customer:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="checkbox" name="isActive"  <%=isChecked(rsCustomer("isActive"))%> />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>HB/S Client:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="checkbox" name="HBSClient"  <%=isChecked(rsCustomer("HBSClient"))%> />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>HBTC Client:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="checkbox" name="HBTCClient"  <%=isChecked(rsCustomer("HBTCClient"))%> />
											</td>
										</tr>			
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top" align="right"><strong>Software Client:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="checkbox" name="softwareClient"  <%=isChecked(rsCustomer("softwareClient"))%> />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td colspan="3" align="right">
												<input type="hidden" name="id" value="<%=iCustomerID%>" />
												<input type="hidden" name="processType" value="editCustomer" />
												<input type="submit" value="  Save  " class="formButton"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
									</table>
								
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<script language="JavaScript" type="text/javascript">

  var frmvalidator  = new Validator("editCustomer");
  frmvalidator.addValidation("customerName","req","Please enter the customer's name");
  //frmvalidator.addValidation("contactName","req","Please enter the contact's name");
  //frmvalidator.addValidation("contactEmail","req","Please enter the cantact's email address");
  //frmvalidator.addValidation("contactEmail","email");
 // frmvalidator.addValidation("address1","req","Please enter the customer's address");
 // frmvalidator.addValidation("city","req","Please enter the customer's city");
  //frmvalidator.addValidation("zip","req","Please enter the customer's zip code");
 // frmvalidator.addValidation("phone","req","Please enter the customer's phone number");
</script>

<%
rsCustomer.close
rsState.close
set rsCustomer = nothing
set rsState = nothing
DataConn.close
set DataConn = nothing
%>