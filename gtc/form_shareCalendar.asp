<%
clientID = request("clientID")
userID = request("userID")

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
'Create command for customer list
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetUnsharedUsers"
   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)
   .parameters.Append .CreateParameter("@clientID", adInteger, adParamInput, 8, clientID)
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsUnShared = oCmd.Execute
Set oCmd = nothing



'Create command for customer list
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCalendarUsers"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, userID)
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsShared = oCmd.Execute
Set oCmd = nothing
%>


<script language="javascript" SRC="includes/common.js"></script>
<script LANGUAGE="javascript">
<!--
// determine browser type
var browser = (navigator.userAgent.toLowerCase().indexOf("msie") != -1)?"ie":"ns";

function MoveUp()
{
	ShiftListSelections(document.shareCalendar.unsharedUsers, false);
}

function MoveDown()
{
	ShiftListSelections(document.shareCalendar.unsharedUsers, true);
}

/* 
** Add the selected fields from the freefields listbox into the
** usedfields listbox, then remove them from the freefields listbox
*/
function AddFields() {
	MoveSelectedListItems(document.shareCalendar.sharedUsers,document.shareCalendar.unsharedUsers,false);
	//Force a refresh to keeps netscape happy.
	if (browser=="ns") history.go(0);
}

/* This function does the opposite of Addfields, but ensures the free list is alphabetical */
function RemoveFields() {
	MoveSelectedListItems(document.shareCalendar.unsharedUsers,document.shareCalendar.sharedUsers,true);
	//Force a refresh to keeps netscape happy.
	if (browser=="ns") history.go(0);
}

function SelectAll(sel)
{
	for (var x=0; x<sel.length; x++){sel.options[x].selected=true;}
}

function selectAll(box) 
{     
	for(var i=0; i<box.length; i++) 
	{     
		box[i].selected = true;     
	}     
}
//-->
</script>
<form name="shareCalendar" method="post" action="process.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sLoginTitle%></span><span class="Header"> - Share Calendar</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td valign="top"><strong>Users:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
																		
												<table width="100%" border="0" cellspacing="0" cellpadding="0"> 
													<tr valign="top"> 
														<td> 
															<table class="rowColor" border="0"> 
																<tr><td colspan="3">&nbsp;&nbsp;<strong>Note:</strong> To select multiple users hold down the Ctrl key and select the items you want.&nbsp;&nbsp;<br /></td></tr>
																<tr> 
																	<td> 
																		&nbsp;&nbsp;<strong>Available Users</strong><br />
																		&nbsp;&nbsp;These are the users that are available for you to share your calendar with.<br />
																		&nbsp;&nbsp;<select name="unsharedUsers" multiple="multiple" size="12">
																			<%do until rsUnShared.eof%>
																				<option value="<%=rsUnShared("userID")%>"><%=rsUnShared("firstName")%>&nbsp;<%=rsUnShared("lastName")%> - <%=rsUnShared("company")%></option>
																			<%rsUnShared.movenext
																			loop
																			rsUnShared.moveFirst%>
																		</select>
																		<br /><br />
																		<div align="center">
																			<a href="#" onClick="AddFields();return false;"><img src="images/arrowup.gif" alt="Move Selected List 2 Fields to List 1" border="0" width="22" height="12"></a>&nbsp;&nbsp;&nbsp; 
																			<a href="#" onClick="RemoveFields();return false;"><img src="images/arrowdown.gif" alt="Move Selected List 1 Fields to List 2" border="0" width="22" height="12"></a> 
																		</div>
																		<br /><br />
																		&nbsp;&nbsp;<strong>Shared Users</strong><br />
																		&nbsp;&nbsp;These are the users that you have shared your calendar with.<br />
																		&nbsp;&nbsp;If they are in this list, they will be able to see your calendar.<br />
																		&nbsp;&nbsp;<select name="sharedUsers" multiple="multiple" size="12">
																			<%do until rsShared.eof%>
																					<option value="<%=rsShared("userID")%>"><%=rsShared("firstName")%>&nbsp;<%=rsShared("lastName")%> - <%=rsShared("company")%></option>
																			<%rsShared.movenext
																			loop
																			rsShared.moveFirst%>
																		</select><br /><br />
																	</td>
																</tr> 
															</table>
														</td>
													</tr> 
												</table>
												
											</td>
										</tr>				
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td colspan="3" align="right">
												<input type="hidden" name="userID" value="<%=userID%>" />
												<input type="hidden" name="processType" value="shareCalendar" />
												<input type="submit" value="  Save  " class="formButton" onClick="selectAll(document.shareCalendar.sharedUsers)"/>
											</td>
										</tr>
									</table>
								
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<%
rsAssigned.close
set rsAssigned = nothing
rsUnassigned.close
set rsUnassigned = nothing
DataConn.close
set DataConn = nothing
%>