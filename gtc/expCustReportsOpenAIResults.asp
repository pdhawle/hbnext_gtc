<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<% 
iCustomerID = request("customer")
iDivisionID = request("division")
dtFrom = request("fromDate")
dtTo = request("toDate")

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If


if iCustomerID = "all" then
	'Create command
	Set oCmd = Server.CreateObject("ADODB.Command")
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetOpenAIByDate" 'all customers and divisions
	   .parameters.Append .CreateParameter("@fromDate", adDBTimeStamp, adParamInput, 8, dtFrom)
	   .parameters.Append .CreateParameter("@toDate", adDBTimeStamp, adParamInput, 8, dtTo)
	   .CommandType = adCmdStoredProc   
	End With
		
		If Err Then
	%>
			<!--#include file="includes/FatalError.inc"-->
	<%
		End If
				
	Set rsReport = oCmd.Execute
	Set oCmd = nothing
	
else
	if iDivisionID <> "all" then
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetOpenAIByDateandDivision" 'specific customer and division
		   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iCustomerID)
		   .parameters.Append .CreateParameter("@divisionID", adInteger, adParamInput, 8, iDivisionID)
		   .parameters.Append .CreateParameter("@fromDate", adDBTimeStamp, adParamInput, 8, dtFrom)
		   .parameters.Append .CreateParameter("@toDate", adDBTimeStamp, adParamInput, 8, dtTo)
		   .CommandType = adCmdStoredProc   
		End With
			
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
					
		Set rsReport = oCmd.Execute
		Set oCmd = nothing
		
	else
		'Create command
		Set oCmd = Server.CreateObject("ADODB.Command")
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetOpenAIByDateandCustomer" 'specific customer and all divisions
		   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iCustomerID)
		   .parameters.Append .CreateParameter("@fromDate", adDBTimeStamp, adParamInput, 8, dtFrom)
		   .parameters.Append .CreateParameter("@toDate", adDBTimeStamp, adParamInput, 8, dtTo)
		   .CommandType = adCmdStoredProc   
		End With
			
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
					
		Set rsReport = oCmd.Execute
		Set oCmd = nothing
	
	end if
	
end if

Response.ContentType = "application/vnd.ms-excel"
Response.AddHeader "Content-Disposition", "attachment; filename=OpenAIResults.xls" 
if rsReport.eof <> true then%>
		<table border=1>
			<tr>
				<td><b>Report ID</b></td>
				<td><b>Customer</b></td>
				<td><b>Division</b></td>
				<td><b>Project</b></td>
				<td><b>Report Date</b></td>
				<td><b>Inspection Type</b></td>
				<td><b>Open Items</b></td>
				<td><b>Corrected Items</b></td>
				<td><b>% Corrected</b></td>
			</tr>
	<%do until rsReport.eof%>
		<tr>
			<td><%=rsReport("reportID")%></td>
			<td><%=rsReport("customerName")%></td>
			<td><%=rsReport("division")%></td>
			<td><%=rsReport("projectName")%></td>
			<td><%=rsReport("inspectionDate")%></td>
			<td><%=rsReport("inspectionType")%></td>
			<td><%=rsReport("openAI")%></td>
			<td><%=rsReport("closedAI")%></td>
			<td>
				<%iTotalItems = rsReport("closedAI") + rsReport("openAI")
				iTotalComplete = (rsReport("closedAI")/iTotalItems) * 100
				
				if iTotalItems = 0 then
					iTotalComplete = 100
				end if
				
				if iTotalComplete <> "" then
					response.Write round(iTotalComplete) & "%"
				end if%>											
			</td>
		</tr>
	<%
		iTotOpenAI = iTotOpenAI + rsReport("openAI")
		iTotClosedAI = iTotClosedAI + rsReport("closedAI")
	rsReport.movenext
	loop%>
		<tr>
			<td colspan="6" align="right"><strong>Totals:</strong>&nbsp;&nbsp;&nbsp;</td>
			<td><%=iTotOpenAI%></td>
			<td><%=iTotClosedAI%></td>
			<td>
				<%iTotal = iTotOpenAI + iTotClosedAI
				iTotalComp = (iTotClosedAI/iTotal) * 100											
				
				if iTotalComp <> "" then
					response.Write round(iTotalComp) & "%"
				end if%>
				
			</td>
		</tr>
	</table>
<%end if

rsReport.close
set rsReport=nothing
DataConn.close
set DataConn = nothing
%>