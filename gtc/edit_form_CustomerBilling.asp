<%
On Error Resume Next

iCustomerID = request("id")
sMsgBill=request("sMsgBill")

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

'Create command for state list
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetStates"
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsState = oCmd.Execute
Set oCmd = nothing

'Create command for customer
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetCustomer"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iCustomerID)
   .CommandType = adCmdStoredProc
   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
Set rsCustomer = oCmd.Execute
Set oCmd = nothing

Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetBillingComments"
   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, iCustomerID)
   .CommandType = adCmdStoredProc
   
End With

	
Set rsBillComments = oCmd.Execute
Set oCmd = nothing


%>
<form name="editCustomerBilling" method="post" action="process.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Edit Customer Billing</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<a href="customerList.asp?id=<%=left(rsCustomer("customerName"),1)%>" class="footerLink">customer list</a>			
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td colspan="3">
												<%if sMsgBill = "True" then%>
													<font color="#EE0000"><strong>Your changes have been made</font></span>
												<%else%>
													<span class="redText">&nbsp;</span>
												<%end if%>
											</td>
										</tr>				
										<tr><td colspan="3" height="5"></td></tr>
										<tr><td colspan="3"><strong>Customer:</strong>&nbsp;<%=rsCustomer("customerName")%></td></tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr><td colspan="3"><strong>Billing Address Info</strong></td></tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top"><strong>Street:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="billStreet" size="30" value="<%=rsCustomer("billStreet")%>" />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top"><strong>Suite #:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="billSuite" size="3" value="<%=rsCustomer("billSuite")%>" />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top"><strong>City:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="billCity" size="30" value="<%=rsCustomer("billCity")%>"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top"><strong>State:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<select name="billState">
													<%do until rsState.eof
														if rsCustomer("billState") = rsState("stateID") then%>
															<option selected="selected" value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
														<%else%>
															<option value="<%=rsState("stateID")%>"><%=rsState("stateID")%></option>
														<%end if
													rsState.movenext
													loop%>
												</select>&nbsp;&nbsp;<strong>Zip:</strong>&nbsp;<input type="text" name="billZip" size="5" value="<%=rsCustomer("billZip")%>" />
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr><td colspan="3"><strong>Billing Contact Info</strong></td></tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top"><strong>Name:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="billName" size="30" value="<%=rsCustomer("billName")%>"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top"><strong>Email:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="billEmail" size="30" value="<%=rsCustomer("billEmail")%>"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top"><strong>Office Phone:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="billPhone" size="10" value="<%=rsCustomer("billPhone")%>"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top"><strong>Mobile Phone:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="billMobile" size="10" value="<%=rsCustomer("billMobile")%>"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top"><strong>Fax:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="billFax" size="10" value="<%=rsCustomer("billFax")%>"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top"><strong>Other Phone:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="billOtherPhone" size="10" value="<%=rsCustomer("billOtherPhone")%>"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top"><strong>Account Manager:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="text" name="acctManager" size="30" value="<%=rsCustomer("acctManager")%>"/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top"><strong>Advanced Billing:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="checkbox" name="advanceBilling" <%=isChecked(rsCustomer("advanceBilling"))%>/>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr><td colspan="3"><strong>Billing Delivery Method</strong></td></tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top"><strong>Method:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<input type="checkbox" name="billMethodEmail" <%=isChecked(rsCustomer("billMethodEmail"))%> />&nbsp;Email<br />
												<input type="checkbox" name="billMethodFax" <%=isChecked(rsCustomer("billMethodFax"))%> />&nbsp;Fax<br />
												<input type="checkbox" name="billMethodMail" <%=isChecked(rsCustomer("billMethodMail"))%> />&nbsp;Standard Mail
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr>
											<td valign="top"><strong>* Comments:</strong></td>
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td>
												<textarea name="billComments" rows="5" cols="30"></textarea>
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td colspan="3" align="right">
												<input type="hidden" name="commentTypeID" value="1" />
												<input type="hidden" name="customerName" value="<%=rsCustomer("customerName")%>" />
												<input type="hidden" name="customerID" value="<%=iCustomerID%>" />
												<input type="hidden" name="userID" value="<%=session("ID")%>" />
												<input type="hidden" name="processType" value="editCustomerBilling" />
												<input type="submit" value="  Save  " class="formButton"/>
												
											</td>
										</tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="5"></td></tr>
										<tr><td colspan="3">* comments will automatically be added to the journal/blog for the day<br /> entered  
												and will also be listed below for your convenience.</td></tr>
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="10"></td></tr>
										<tr>
											<td colspan="3">
												
												<strong>All Comments:</strong><br />
												<%do until rsBillComments.eof
													response.Write "<b>" & rsBillComments("dateAdded") & "</b> (" & rsBillComments("firstName")& " " & rsBillComments("lastName") & ") - " & rsBillComments("comments") & "<br><br>"
												rsBillComments.movenext
												loop%>
											</td>
										</tr>
										
										<tr><td colspan="3"><img src="images/pix.gif" width="1" height="30"></td></tr>
									</table>
								
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
<script language="JavaScript" type="text/javascript">

//  var frmvalidator  = new Validator("editCustomerBilling");
 // frmvalidator.addValidation("customerName","req","Please enter the customer's name");
//  frmvalidator.addValidation("contactName","req","Please enter the contact's name");
//  frmvalidator.addValidation("contactEmail","req","Please enter the cantact's email address");
//  frmvalidator.addValidation("contactEmail","email");
//  frmvalidator.addValidation("address1","req","Please enter the customer's address");
//  frmvalidator.addValidation("city","req","Please enter the customer's city");
//  frmvalidator.addValidation("zip","req","Please enter the customer's zip code");
//  frmvalidator.addValidation("phone","req","Please enter the customer's phone number");
</script>

<%
rsCustomer.close
rsState.close
set rsCustomer = nothing
set rsState = nothing
DataConn.close
set DataConn = nothing
%>