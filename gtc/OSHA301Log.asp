<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
customerID = Request("customerID")
sYear = request("year")

Dim rs, oCmd, DataConn

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If


On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

Set oCmd = Server.CreateObject("ADODB.Command")	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetAllCustomers"
   .CommandType = adCmdStoredProc   
End With

Set rsCustomer = oCmd.Execute
Set oCmd = nothing

if customerID <> "" then
		
	'get the customer
	Set oCmd = Server.CreateObject("ADODB.Command")	
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetCustomer"
	   .parameters.Append .CreateParameter("@ID", adInteger, adParamInput, 8, customerID)
	   .CommandType = adCmdStoredProc
	   
	End With		
	Set rsCust = oCmd.Execute
	Set oCmd = nothing

end if	
	
if customerID <> "" and sYear <> "" then	
	'************PDF LOG CREATION HERE***************************************************

	Set PDF = Server.CreateObject("Persits.PdfManager")
	PDF.RegKey = sRegKey
	Set Doc = PDF.OpenDocument( Server.MapPath( "pdfTemplates/OSHA300Log.pdf" ) )
	Set Page = Doc.Pages(1)	
	Set Param = PDF.CreateParam
	Set Font = Doc.Fonts("Helvetica-Bold")
	
	'year
	Param.Add("x=914, y=577, size=13, html=true")
	Page.Canvas.SaveState
	Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
	Page.Canvas.DrawText right(sYear,2), Param, Font
	Page.Canvas.RestoreState
	
	'customer name
	Param.Add("x=850, y=508, size=7, html=true")
	Page.Canvas.SaveState
	Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
	Page.Canvas.DrawText rsCust("customerName"), Param, Font
	Page.Canvas.RestoreState
	
	'customer city
	Param.Add("x=803, y=490, size=7, html=true")
	Page.Canvas.SaveState
	Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
	Page.Canvas.DrawText rsCust("city"), Param, Font
	Page.Canvas.RestoreState
	
	'customer state
	Param.Add("x=930, y=490, size=7, html=true")
	Page.Canvas.SaveState
	Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
	Page.Canvas.DrawText rsCust("state"), Param, Font
	Page.Canvas.RestoreState
	
	'this is where we loop the recordset and
	'write on the pdf the data
	'get the list of osha 301 reports	
	y = "366"
	
	i = 1
	
	Set oCmd = Server.CreateObject("ADODB.Command")	
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetOSHA301ByYearAndCustomer"
	   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)
	   .parameters.Append .CreateParameter("@Year", adVarchar, adParamInput, 50, sYear)
	   .CommandType = adCmdStoredProc   
	End With
	
	Set rsOSHA = oCmd.Execute
	Set oCmd = nothing
	
	do until rsOSHA.eof
	
		' case number
		Param.Add("x=22, y=" & y & ", size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsOSHA("caseNumber"), Param, Font
		Page.Canvas.RestoreState
		
		' employee name
		Param.Add("x=55, y=" & y & ", size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsOSHA("name"), Param, Font
		Page.Canvas.RestoreState
		
		' job title
		Param.Add("x=178, y=" & y & ", size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsOSHA("jobTitle"), Param, Font
		Page.Canvas.RestoreState
		
		' date of injury month
		Param.Add("x=246, y=" & y & ", size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsOSHA("dateOfInjuryMonth"), Param, Font
		Page.Canvas.RestoreState
		
		' date of injury day
		Param.Add("x=262, y=" & y & ", size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsOSHA("dateOfInjuryDay"), Param, Font
		Page.Canvas.RestoreState
		
		' event occured
		Param.Add("x=302, y=" & y & ", size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsOSHA("doingBefore"), Param, Font
		Page.Canvas.RestoreState
		
		' describe injury
		Param.Add("x=403, y=" & y & ", size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText rsOSHA("whatWasInjury"), Param, Font
		Page.Canvas.RestoreState
		
		'classify case
		select case rsOSHA("classifyCase")
			case "1"
				Param.Add("x=617, y=" & y & ", size=7, html=true")
				Page.Canvas.SaveState
				Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
				Page.Canvas.DrawText "X", Param, Font
				Page.Canvas.RestoreState
							
			Case "2"
				Param.Add("x=653, y=" & y & ", size=7, html=true")
				Page.Canvas.SaveState
				Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
				Page.Canvas.DrawText "X", Param, Font
				Page.Canvas.RestoreState
				
			Case "3"
				Param.Add("x=697, y=" & y & ", size=7, html=true")
				Page.Canvas.SaveState
				Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
				Page.Canvas.DrawText "X", Param, Font
				Page.Canvas.RestoreState
			
			Case "4"
				Param.Add("x=743, y=" & y & ", size=7, html=true")
				Page.Canvas.SaveState
				Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
				Page.Canvas.DrawText "X", Param, Font
				Page.Canvas.RestoreState
			
			Case else
			
		end select
		
		'num days away from work
		daysAwayFromWork = rsOSHA("daysAwayFromWork")
		if daysAwayFromWork = "" then
			daysAwayFromWork = 0
		end if
		if isnull(daysAwayFromWork) then
			daysAwayFromWork = 0
		end if
		Param.Add("x=788, y=" & y & ", size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText daysAwayFromWork, Param, Font
		Page.Canvas.RestoreState
		
		'num on job transfer
		daysOnJobTransfer = rsOSHA("daysOnJobTransfer")
		if daysOnJobTransfer = "" then
			daysOnJobTransfer = 0
		end if
		if isnull(daysOnJobTransfer) then
			daysOnJobTransfer = 0
		end if
		Param.Add("x=828, y=" & y & ", size=7, html=true")
		Page.Canvas.SaveState
		Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
		Page.Canvas.DrawText daysOnJobTransfer, Param, Font
		Page.Canvas.RestoreState
		
		'injury
		select case rsOSHA("injury")
			case "1"
				Param.Add("x=881, y=" & y & ", size=7, html=true")
				Page.Canvas.SaveState
				Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
				Page.Canvas.DrawText "X", Param, Font
				Page.Canvas.RestoreState
							
			Case "2"
				Param.Add("x=901, y=" & y & ", size=7, html=true")
				Page.Canvas.SaveState
				Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
				Page.Canvas.DrawText "X", Param, Font
				Page.Canvas.RestoreState
				
			Case "3"
				Param.Add("x=921, y=" & y & ", size=7, html=true")
				Page.Canvas.SaveState
				Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
				Page.Canvas.DrawText "X", Param, Font
				Page.Canvas.RestoreState
			
			Case "4"
				Param.Add("x=941, y=" & y & ", size=7, html=true")
				Page.Canvas.SaveState
				Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
				Page.Canvas.DrawText "X", Param, Font
				Page.Canvas.RestoreState
				
			Case "5"
				Param.Add("x=961, y=" & y & ", size=7, html=true")
				Page.Canvas.SaveState
				Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
				Page.Canvas.DrawText "X", Param, Font
				Page.Canvas.RestoreState
				
			Case "6"
				Param.Add("x=981, y=" & y & ", size=7, html=true")
				Page.Canvas.SaveState
				Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
				Page.Canvas.DrawText "X", Param, Font
				Page.Canvas.RestoreState
			
			Case else
			
		end select
	
	rsOSHA.movenext
	
		'decrease the Y corordinate 
		y = y - 21
		
	loop
	
	FileName = Doc.Save( Server.MapPath("downloads/OSHA300Log_" & sYear & "_" & customerID & ".pdf"), True)
	
	Set PDF = nothing
	
	'************END OFPDF LOG CREATION HERE***************************************************
	
	'************PDF 300A CREATION HERE***************************************************
	Set PDF = Server.CreateObject("Persits.PdfManager")
	PDF.RegKey = sRegKey
	Set Doc = PDF.OpenDocument( Server.MapPath( "pdfTemplates/OSHA300A.pdf" ) )
	Set Page = Doc.Pages(1)	
	Set Param = PDF.CreateParam
	Set Font = Doc.Fonts("Helvetica-Bold")
	
	'year
	Param.Add("x=919, y=577, size=13, html=true")
	Page.Canvas.SaveState
	Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
	Page.Canvas.DrawText right(sYear,2), Param, Font
	Page.Canvas.RestoreState
	
	'customer/establishment name
	Param.Add("x=745, y=470, size=7, html=true")
	Page.Canvas.SaveState
	Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
	Page.Canvas.DrawText rsCust("customerName"), Param, Font
	Page.Canvas.RestoreState
	
	'street
	Param.Add("x=690, y=449, size=7, html=true")
	Page.Canvas.SaveState
	Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
	Page.Canvas.DrawText rsCust("address1"), Param, Font
	Page.Canvas.RestoreState
	
	'customer city
	Param.Add("x=690, y=432, size=7, html=true")
	Page.Canvas.SaveState
	Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
	Page.Canvas.DrawText rsCust("city"), Param, Font
	Page.Canvas.RestoreState
	
	'customer state
	Param.Add("x=826, y=432, size=7, html=true")
	Page.Canvas.SaveState
	Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
	Page.Canvas.DrawText rsCust("state"), Param, Font
	Page.Canvas.RestoreState
	
	'customer zip
	Param.Add("x=869, y=432, size=7, html=true")
	Page.Canvas.SaveState
	Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
	Page.Canvas.DrawText rsCust("zip"), Param, Font
	Page.Canvas.RestoreState
	
	'customer/industry type
	Param.Add("x=690, y=384, size=7, html=true")
	Page.Canvas.SaveState
	Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
	Page.Canvas.DrawText rsCust("customerType"), Param, Font
	Page.Canvas.RestoreState
	
	'SIC 1
	Param.Add("x=697, y=356, size=7, html=true")
	Page.Canvas.SaveState
	Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
	Page.Canvas.DrawText rsCust("SIC1"), Param, Font
	Page.Canvas.RestoreState

	'SIC 2
	Param.Add("x=716, y=356, size=7, html=true")
	Page.Canvas.SaveState
	Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
	Page.Canvas.DrawText rsCust("SIC2"), Param, Font
	Page.Canvas.RestoreState
	
	'SIC 3
	Param.Add("x=736, y=356, size=7, html=true")
	Page.Canvas.SaveState
	Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
	Page.Canvas.DrawText rsCust("SIC3"), Param, Font
	Page.Canvas.RestoreState
	
	'SIC 4
	Param.Add("x=756, y=356, size=7, html=true")
	Page.Canvas.SaveState
	Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
	Page.Canvas.DrawText rsCust("SIC4"), Param, Font
	Page.Canvas.RestoreState
	
	'NAICS 1
	Param.Add("x=697, y=308, size=7, html=true")
	Page.Canvas.SaveState
	Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
	Page.Canvas.DrawText rsCust("NAICS1"), Param, Font
	Page.Canvas.RestoreState

	'NAICS 2
	Param.Add("x=716, y=308, size=7, html=true")
	Page.Canvas.SaveState
	Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
	Page.Canvas.DrawText rsCust("NAICS2"), Param, Font
	Page.Canvas.RestoreState
	
	'NAICS 3
	Param.Add("x=736, y=308, size=7, html=true")
	Page.Canvas.SaveState
	Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
	Page.Canvas.DrawText rsCust("NAICS3"), Param, Font
	Page.Canvas.RestoreState
	
	'NAICS 4
	Param.Add("x=756, y=308, size=7, html=true")
	Page.Canvas.SaveState
	Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
	Page.Canvas.DrawText rsCust("NAICS4"), Param, Font
	Page.Canvas.RestoreState
	
	'NAICS 5
	Param.Add("x=776, y=308, size=7, html=true")
	Page.Canvas.SaveState
	Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
	Page.Canvas.DrawText rsCust("NAICS5"), Param, Font
	Page.Canvas.RestoreState
	
	'NAICS 6
	Param.Add("x=796, y=308, size=7, html=true")
	Page.Canvas.SaveState
	Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
	Page.Canvas.DrawText rsCust("NAICS6"), Param, Font
	Page.Canvas.RestoreState
	
	'Annual num employees
	Param.Add("x=826, y=252, size=7, html=true")
	Page.Canvas.SaveState
	Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
	Page.Canvas.DrawText rsCust("annualAverageEmployees"), Param, Font
	Page.Canvas.RestoreState
	
	'total hours
	Param.Add("x=826, y=231, size=7, html=true")
	Page.Canvas.SaveState
	Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
	Page.Canvas.DrawText rsCust("totalHours"), Param, Font
	Page.Canvas.RestoreState
	
	'********enter totals here*****************
	'will need to get totals from the database
	iTotDeaths = 0
	iTotCasesAway = 0
	iTotTransfer = 0
	iTotOtherCases = 0
	daysAwayFromWork = 0
	daysOnJobTransfer = 0
	iTotInjury = 0
	iTotSkinDisorders = 0
	iTotRespitory = 0
	iTotPoison = 0
	iTotHearing = 0
	iTotOtherIllness = 0
	
	Set oCmd = Server.CreateObject("ADODB.Command")	
	With oCmd
	   .ActiveConnection = DataConn
	   .CommandText = "spGetOSHA301ByYearAndCustomer"
	   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)
	   .parameters.Append .CreateParameter("@Year", adVarchar, adParamInput, 50, sYear)
	   .CommandType = adCmdStoredProc   
	End With
	
	Set rsOSHA = oCmd.Execute
	Set oCmd = nothing
	
	do until rsOSHA.eof
		select case rsOSHA("classifyCase")
			Case "1"
				iTotDeaths = iTotDeaths + 1
			Case "2"
				iTotCasesAway = iTotCasesAway + 1
			Case "3"
				iTotTransfer = iTotTransfer + 1
			Case "4"
				iTotOtherCases = iTotOtherCases + 1
		end select
		
		daysAwayFromWork = daysAwayFromWork + cint(rsOSHA("daysAwayFromWork"))
		daysOnJobTransfer = daysOnJobTransfer + cint(rsOSHA("daysOnJobTransfer"))
		
		select case rsOSHA("injury")
			Case "1"
				iTotInjury = iTotInjury + 1
			Case "2"
				iTotSkinDisorders = iTotSkinDisorders + 1
			Case "3"
				iTotRespitory = iTotRespitory + 1
			Case "4"
				iTotPoison = iTotPoison + 1
			Case "5"
				iTotHearing = iTotHearing + 1
			Case "6"
				iTotOtherIllness = iTotOtherIllness + 1
		end select
	rsOSHA.movenext
	loop
	
	'total deaths
	Param.Add("x=57, y=353, size=7, html=true")
	Page.Canvas.SaveState
	Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
	Page.Canvas.DrawText iTotDeaths, Param, Font
	Page.Canvas.RestoreState
	
	'total cases away
	Param.Add("x=151, y=353, size=7, html=true")
	Page.Canvas.SaveState
	Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
	Page.Canvas.DrawText iTotCasesAway, Param, Font
	Page.Canvas.RestoreState
	
	'total transfer
	Param.Add("x=241, y=353, size=7, html=true")
	Page.Canvas.SaveState
	Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
	Page.Canvas.DrawText iTotTransfer, Param, Font
	Page.Canvas.RestoreState
	
	'total other cases
	Param.Add("x=351, y=353, size=7, html=true")
	Page.Canvas.SaveState
	Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
	Page.Canvas.DrawText iTotOtherCases, Param, Font
	Page.Canvas.RestoreState
	
	'numdays away from work
	Param.Add("x=60, y=244, size=7, html=true")
	Page.Canvas.SaveState
	Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
	Page.Canvas.DrawText daysAwayFromWork, Param, Font
	Page.Canvas.RestoreState
	
	'numdays on job transfer
	Param.Add("x=220, y=244, size=7, html=true")
	Page.Canvas.SaveState
	Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
	Page.Canvas.DrawText daysOnJobTransfer, Param, Font
	Page.Canvas.RestoreState
	
	
	'injury
	Param.Add("x=168, y=157, size=7, html=true")
	Page.Canvas.SaveState
	Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
	Page.Canvas.DrawText iTotInjury, Param, Font
	Page.Canvas.RestoreState
	
	'skin disorders
	Param.Add("x=168, y=129, size=7, html=true")
	Page.Canvas.SaveState
	Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
	Page.Canvas.DrawText iTotSkinDisorders, Param, Font
	Page.Canvas.RestoreState
	
	'respitory
	Param.Add("x=168, y=112, size=7, html=true")
	Page.Canvas.SaveState
	Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
	Page.Canvas.DrawText iTotRespitory, Param, Font
	Page.Canvas.RestoreState
	
	'poison
	Param.Add("x=376, y=157, size=7, html=true")
	Page.Canvas.SaveState
	Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
	Page.Canvas.DrawText iTotPoison, Param, Font
	Page.Canvas.RestoreState
	
	'hearing
	Param.Add("x=376, y=141, size=7, html=true")
	Page.Canvas.SaveState
	Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
	Page.Canvas.DrawText iTotHearing, Param, Font
	Page.Canvas.RestoreState
	
	'other illnesses
	Param.Add("x=376, y=125, size=7, html=true")
	Page.Canvas.SaveState
	Page.Canvas.SetCTM 0, 1, -1, 0, Page.Width, 0
	Page.Canvas.DrawText iTotOtherIllness, Param, Font
	Page.Canvas.RestoreState
	

	
	FileName = Doc.Save( Server.MapPath("downloads/OSHA300A_" & sYear & "_" & customerID & ".pdf"), True)
	
	Set PDF = nothing
	
	'************END OFPDF 300A CREATION HERE***************************************************
end if
%>

<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script type="text/javascript">
    var GB_ROOT_DIR = "<%=sPDFPath%>/greybox/";
</script>
<script type="text/javascript" src="greybox/AJS.js"></script>
<script type="text/javascript" src="greybox/AJS_fx.js"></script>
<script type="text/javascript" src="greybox/gb_scripts.js"></script>
<link href="greybox/gb_styles.css" rel="stylesheet" type="text/css" />
<script language="JavaScript">
<!--
function confirmDelete(delUrl) {
 if (confirm("Are you sure you wish to delete this report?")) {
    document.location = delUrl;
  }


}

function rep_onchange(addOSHA301) {
   document.OSHA301Log.action = "OSHA301Log.asp";
   OSHA301Log.submit(); 
}
//-->
</script>
</head>
<body>
<form name="OSHA301Log" method="post" action="OSHA301Log.asp.asp">
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Log of Work-Related Injuries and Illnesses</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<%if customerID <> "" and sYear <> "" then%>
				<tr class="colorBars">
					<td colspan="5"><!--printOSHA300Log.asp?customerID=<%'=customerID%>&year=<%'=sYear%>-->
						
						&nbsp;&nbsp;<a href="downloads/OSHA300Log_<%=sYear%>_<%=customerID%>.pdf" class="footerLink" target="_blank">view/print OSHA Form 300</a>&nbsp;&nbsp;
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="downloads/OSHA300A_<%=sYear%>_<%=customerID%>.pdf" class="footerLink" target="_blank">view/print OSHA Form 300A</a>&nbsp;&nbsp;		
						
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<%end if%>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<select name="customerID" onChange="return rep_onchange(OSHA301Log)">
										<option>--select a customer--</option>
									<%do until rsCustomer.eof
										if trim(rsCustomer("customerID")) = trim(customerID) then%>
											<option selected="selected" value="<%=rsCustomer("customerID")%>"><%=rsCustomer("customerName")%></option>
										<%else%>
											<option value="<%=rsCustomer("customerID")%>"><%=rsCustomer("customerName")%></option>
									<%end if
									rsCustomer.movenext
									loop%>
									</select><br><br>
									
									<%if customerID <> "" then%>
										<select name="year" onChange="return rep_onchange(OSHA301Log)">
											<option>--select a year--</option>
											<option value="<%=year(now())%>" <%=isSelected(year(now()),trim(sYear))%>><%=year(now())%></option>
											<option value="<%=year(now()) - 1%>" <%=isSelected(year(now()) - 1,trim(sYear))%>><%=year(now()) - 1%></option>
											<option value="<%=year(now()) - 2%>" <%=isSelected(year(now()) - 2,trim(sYear))%>><%=year(now()) - 2%></option>
										</select><br><br>
									<%end if%>
									
									<%if customerID <> "" and sYear <> "" then%>
									
										<table width="100%" cellpadding="0" cellspacing="0" border="0">
											<tr bgcolor="#666666">
												<td><img src="images/pix.gif" width="5" height="1"></td>
												<td><span class="searchText">Case No.</span></td>
												<td><span class="searchText">Employee's Name</span></td>
												<td><span class="searchText">Job Title</span></td>
												<td><span class="searchText">Date of Injury</span></td>
												<td><span class="searchText">Where Event Occurred</span></td>
												<td><span class="searchText">Describe Injury</span></td>
												<td><span class="searchText">Action</span></td>
											</tr>
											<tr><td colspan="10"><img src="images/pix.gif" width="1" height="10"></td></tr>
											<%
											Set oCmd = Server.CreateObject("ADODB.Command")	
											With oCmd
											   .ActiveConnection = DataConn
											   .CommandText = "spGetOSHA301ByYearAndCustomer"
											   .parameters.Append .CreateParameter("@customerID", adInteger, adParamInput, 8, customerID)
											   .parameters.Append .CreateParameter("@Year", adVarchar, adParamInput, 50, sYear)
											   .CommandType = adCmdStoredProc   
											End With
											
											Set rsOSHA = oCmd.Execute
											Set oCmd = nothing
											
											blnChange = true
											do until rsOSHA.eof
												If blnChange = true then%>
													<tr class="rowColor">
												<%else%>
													<tr>
												<%end if%>
													<td><img src="images/pix.gif" width="5" height="1"></td>
													<td><%=rsOSHA("caseNumber")%></td>
													<td><%=rsOSHA("name")%></td>
													<td><%=rsOSHA("jobTitle")%></td>
													<td><%=rsOSHA("dateOfInjuryMonth")%>/<%=rsOSHA("dateOfInjuryDay")%>/<%=rsOSHA("dateOfInjuryYear")%></td>
													<td><%=rsOSHA("doingBefore")%></td>
													<td><%=rsOSHA("whatWasInjury")%></td>
													<td><a href="form.asp?formType=editOSHA301&reportID=<%=rsOSHA("reportID")%>">edit</a>&nbsp;|&nbsp;<a href="downloads/OSHA301_<%=rsOSHA("reportID")%>.pdf" target="_blank">view form 301</a></td>
												</tr>
											<%rsOSHA.movenext
											if blnChange = true then
												blnChange = false
											else
												blnChange = true
											end if
											loop%>
										</table>
									<%end if%>
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
</body>
</html>
<%
rs.close
DataConn.close
Set rs = nothing
Set DataConn = nothing
Set oCmd = nothing
%>