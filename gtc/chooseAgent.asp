<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%

projectID = request("projectID")

sMonth = request("month")
sYear = request("year")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
	
DataConn.Open Session("Connection"), Session("UserID")
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If

'Create command
Set oCmd = Server.CreateObject("ADODB.Command")
	
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetAssignedProjectsAdmin"
   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, session("ID"))
   .CommandType = adCmdStoredProc   
End With
	
	If Err Then
%>
		<!--#include file="includes/FatalError.inc"-->
<%
	End If
			
Set rsProjects = oCmd.Execute
Set oCmd = nothing


If projectID <> "" then
	If projectID = "All" Then
		'get all of the users
		
		Set oCmd = Server.CreateObject("ADODB.Command")
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetUserListByName" 'get all of the users
		   .CommandType = adCmdStoredProc   
		End With
			
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
					
		Set rsUsers = oCmd.Execute
		Set oCmd = nothing
	else
		'get the agents that are assigned to the project
		'getUsersByProject
		Set oCmd = Server.CreateObject("ADODB.Command")
		With oCmd
		   .ActiveConnection = DataConn
		   .CommandText = "spGetUsersByProject" 'get the users based on the project selected
		   .parameters.Append .CreateParameter("@projectID", adInteger, adParamInput, 8, projectID)
		   .CommandType = adCmdStoredProc   
		End With
			
			If Err Then
		%>
				<!--#include file="includes/FatalError.inc"-->
		<%
			End If
					
		Set rsUsers = oCmd.Execute
		Set oCmd = nothing
	end if
end if
%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
<script language="javascript" src="includes/datePicker.js"></script>
<link rel="stylesheet" href="includes/datePicker.css" type="text/css">
<script language="javascript">
<!--
function dept_onchange(reportList) {
   document.reportList.action = "chooseAgent.asp";
   reportList.submit(); 
}

function sendme() 
{ 
    window.open("","myNewWin","scrollbars=yes,resizable=yes,width=1000,height=800"); 
    var a = window.setTimeout("document.reportList.submit();",100); 
} 

//-->
</script>
</head>
<body>
<form name="reportList" method="post" action="agentPayReport.asp" >
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Agent Pay Report</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0" border="0">
										<tr>
											<td>
												Select customer/project to view the pay rate for a given agent/inspector.<br>
												<select name="projectID" onChange="return dept_onchange(reportList)">
													<option value="">--Select Project--</option>
													<!--when admin is added make this a super admin item-->
													<%'if trim(projectID) = "All" then%>
														<!--<option selected="selected" value="All">All Projects</option>
													<%'else%>
														<option value="All">All Projects</option>-->
													<%'end if%>
													
													<%do until rsProjects.eof
														If trim(rsProjects("projectID")) = trim(projectID) then%>
															<option selected="selected" value="<%=rsProjects("projectID")%>"><%=rsProjects("customerName")%> - <%=rsProjects("projectName")%></option>
														<%else%>
															<option value="<%=rsProjects("projectID")%>"><%=rsProjects("customerName")%> - <%=rsProjects("projectName")%></option>
													<%end if
													rsProjects.movenext
													loop%>
												</select><br><br>
												
												<select name="inspector">
													<%'If projectID <> "" then%>
														<option value="All">All Inspectors/Agents</option>
														<%'do until rsUsers.eof%>
															<!--<option value="<%'=rsUsers("userID")%>"><%'=rsUsers("firstName") & " " & rsUsers("lastName")%></option>-->
														<%'rsUsers.movenext
														'loop
													'end if%>
												</select><br><br>
												
												Month: <select name="month">
													<%if sMonth = 1 then%>
														<option selected="selected" value="1">January</option>
													<%else%>
														<option value="1">January</option>
													<%end if%>
													
													<%if sMonth = 2 then%>
														<option selected="selected" value="2">February</option>
													<%else%>
														<option value="2">February</option>
													<%end if%>
													
													<%if sMonth = 3 then%>
														<option selected="selected" value="3">March</option>
													<%else%>
														<option value="3">March</option>
													<%end if%>
													
													<%if sMonth = 4 then%>
														<option selected="selected" value="4">April</option>
													<%else%>
														<option value="4">April</option>
													<%end if%>
													
													<%if sMonth = 5 then%>
														<option selected="selected" value="5">May</option>
													<%else%>
														<option value="5">May</option>
													<%end if%>
													
													<%if sMonth = 6 then%>
														<option selected="selected" value="6">June</option>
													<%else%>
														<option value="6">June</option>
													<%end if%>
													
													<%if sMonth = 7 then%>
														<option selected="selected" value="7">July</option>
													<%else%>
														<option value="7">July</option>
													<%end if%>
													
													<%if sMonth = 8 then%>
														<option selected="selected" value="8">August</option>
													<%else%>
														<option value="8">August</option>
													<%end if%>
													
													<%if sMonth = 9 then%>
														<option selected="selected" value="9">September</option>
													<%else%>
														<option value="9">September</option>
													<%end if%>
													
													<%if sMonth = 10 then%>
														<option selected="selected" value="10">October</option>
													<%else%>
														<option value="10">October</option>
													<%end if%>
													
													<%if sMonth = 11 then%>
														<option selected="selected" value="11">November</option>
													<%else%>
														<option value="11">November</option>
													<%end if%>
													
													<%if sMonth = 12 then%>
														<option selected="selected" value="12">December</option>
													<%else%>
														<option value="12">December</option>
													<%end if%>						
														
												</select> &nbsp;&nbsp;&nbsp;&nbsp;
												
												Year: <select name="year">
													<%if sYear = year(now()) then%>
														<option selected="selected" value="<%=year(now())%>"><%=year(now())%></option>
													<%else%>
														<option value="<%=year(now())%>"><%=year(now())%></option>
													<%end if%>
													
													<%if sYear = year(now()) + 1 then%>
														<option selected="selected" value="<%=year(now()) + 1%>"><%=year(now()) + 1%></option>
													<%else%>
														<option value="<%=year(now()) + 1%>"><%=year(now()) + 1%></option>
													<%end if%>
													
													<%if sYear = year(now()) - 1 then%>
														<option selected="selected" value="<%=year(now()) - 1%>"><%=year(now()) - 1%></option>
													<%else%>
														<option value="<%=year(now()) - 1%>"><%=year(now()) - 1%></option>
													<%end if%>
												</select> &nbsp;&nbsp;&nbsp;&nbsp;
											
												<%If projectID <> "" then%>
													<input type="submit" value=" Select " class="formButton"> 
												<%else%>
													<input type="submit" value=" Select " class="formButton" disabled="disabled"> 
												<%end if%>
											</td>
										</tr>
									</table>
															
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</form>
</body>
</html>
<%
rsCustomer.close
DataConn.close
Set rsCustomer = nothing
Set DataConn = nothing
Set oCmd = nothing
%>