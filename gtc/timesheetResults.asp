<!--#include file="includes/constants.inc"-->
<!--#include file="includes/InitializeVariables.inc"-->
<!--#include file="includes/functions.asp"-->

<%
clientID = request("clientID")
userID = request("userID")
dtFrom = request("fromDate")
dtTo = request("toDate")

If Session("LoggedIn") <> "True" Then
	Response.Redirect("index.asp")
End If

On Error Resume Next

Set DataConn = Server.CreateObject("ADODB.Connection") 
DataConn.Open Session("Connection"), Session("UserID")

Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetTimesheetByUserAndDate"
   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)
   .parameters.Append .CreateParameter("@fromDate", adDBTimeStamp, adParamInput, 8, dtFrom)
   .parameters.Append .CreateParameter("@toDate", adDBTimeStamp, adParamInput, 8, dtTo)   
   .CommandType = adCmdStoredProc   
End With
			
Set rsReport = oCmd.Execute
Set oCmd = nothing


Set oCmd = Server.CreateObject("ADODB.Command")
With oCmd
   .ActiveConnection = DataConn
   .CommandText = "spGetUser"
   .parameters.Append .CreateParameter("@userID", adInteger, adParamInput, 8, userID)  
   .CommandType = adCmdStoredProc   
End With
			
Set rsUser = oCmd.Execute
Set oCmd = nothing

%>
<html>
<head>
<title><%=sPageTitle%></title>
<meta http-equiv="Content-Type" content="text/html;">
<link rel="stylesheet" href="includes/main.css" type="text/css">
</head>
<body>
<table width="848" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">
			<!--#include file="includes/header.asp"-->
		</td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td><img src="images/pix.gif" width="5" height="1"></td>
		<td>	
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="5">
						<table>
							<tr>								
								<td valign="middle" align="left"><span class="grayHeader"><%=sDashboardTitle%></span><span class="Header"> - Timesheet Report</span></td>
								<td><img src="images/pix.gif" width="1" height="100"></td>
							</tr>
						</table>						
					</td>
				</tr>
				<!--this is the nav bar for the sub pages-->
				<tr class="colorBars">
					<td colspan="5">
						&nbsp;&nbsp;<a href="expTimesheet.asp?userID=<%=userID%>&fromDate=<%=dtFrom%>&toDate=<%=dtTo%>" class="footerLink">Export Data to Excel</a>&nbsp;&nbsp;
						<span class="footerLink">|</span>&nbsp;&nbsp;<a href="chooseTimesheet.asp?clientID=<%=clientID%>" class="footerLink">back to selections</a>			
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="20"></td></tr>
				<!--end of nav bars for the sub pages-->
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>						
									<!--start of content for the page-->
									<table width="100%" cellpadding="0" cellspacing="0">
										<tr><td></td><td colspan="5">Employee: <strong><%=rsUser("firstName")%>&nbsp;<%=rsUser("lastName")%></strong></td></tr>
										<tr><td></td><td colspan="5">Date Range: <strong><%=dtFrom%> - <%=dtTo%></strong></td></tr>
										<tr bgcolor="#666666">
											<td><img src="images/pix.gif" width="5" height="1"></td>
											<td><span class="searchText">Date</span></td>
											<td><span class="searchText">Project</span></td>
											<td><span class="searchText">Comments</span></td>
											<td><span class="searchText">Code</span></td>
											<td><span class="searchText">Code Description</span></td>
											<td><span class="searchText">Billable</span></td>
											<td><span class="searchText">Hours</span></td>											
										</tr>
								
										<%If rsReport.eof then %>
												<tr><td></td><td colspan="9">there are no items to display</td></tr>
											<%else
												blnChange = false
												dim i
												i = 0
												'irate = 0
												'iFee = 0
												Do until rsReport.eof
													If blnChange = true then%>
														<tr class="rowColor">
													<%else%>
														<tr>
													<%end if%>
														<td></td>
														<td><%=rsReport("date")%></td>
														<td><%=rsReport("customerName")%> - <%=rsReport("projectName")%></td>
														<td><%=rsReport("comments")%></td>
														<td><%=rsReport("code")%></td>
														<td><%=rsReport("type")%></td>
														<td>
															<%if rsReport("billable") = "True" then
																iTotalBill = iTotalBill + rsReport("hours")%>
																<img src="images/check.gif">
															<%else
																iNTotalBill = iNTotalBill + rsReport("hours")%>
															<%end if%>
														</td>
														<td><%=rsReport("hours")%></td>
													</tr>
												<%
												iTotal = iTotal + rsReport("hours")
												rsReport.movenext
												if blnChange = true then
													blnChange = false
												else
													blnChange = true
												end if
												i = i + 1
												loop
												
												if iTotalBill = "" then
													iTotalBill = 0
												end if
												if iNTotalBill = "" then
													iNTotalBill = 0
												end if
												%>
												
												<tr><td colspan="10"><img src="images/pix.gif" width="1" height="10"></td></tr>
												<tr>
													<td colspan="6" align="right"><strong>Billable Hours:</strong>&nbsp;&nbsp;</td>
													<td><%=iTotalBill%></td>
												</tr>
												<tr>
													<td colspan="6" align="right"><strong>Non-Billable Hours:</strong>&nbsp;&nbsp;</td>
													<td><%=iNTotalBill%></td>
												</tr>
												<tr>
													<td colspan="6" align="right"><strong>Total Hours:</strong>&nbsp;&nbsp;</td>
													<td><%=iTotal%></td>
												</tr>
												
											<%end if%>
										
									</table>
															
									<!--end of content for the page-->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="5"><img src="images/pix.gif" width="1" height="10"></td></tr>			
			</table>
		</td>
		<td><img src="images/pix.gif" width="5" height="1"></td>
	</tr>
	<!--#include file="includes/footer.asp"-->
</table>
</body>
</html>
<%
'rsCustomer.close
rsReport.close
DataConn.close
Set rsCustomer = nothing
Set rsReport = nothing
Set DataConn = nothing
Set oCmd = nothing
%>